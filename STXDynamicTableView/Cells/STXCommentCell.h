//
//  STXCommentCell.h
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

//@import UIKit;
#import "STXCommentItem.h"
#import "STXUserItem.h"
#import "Comment_share.h"
#import "STXAttributedLabel.h"
#import "Constants.h"

@class STXCommentCell;

@protocol STXCommentCellDelegate <NSObject>

@optional

- (void)commentCellWillShowAllComments:(STXCommentCell *)commentCell;
//- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(id<STXUserItem>)commenter;

//- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(Comment_share *)commenter;

- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter;

- (void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url;
- (void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag;
- (void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention;
- (void)userWillComment:(STXCommentCell *)userActionCell;

@end

@interface STXCommentCell : UITableViewCell {
    Comment_share *comment;
}

//@property (copy, nonatomic) id <STXCommentItem> comment;

@property (nonatomic) NSInteger totalComments;
@property (strong, nonatomic) STXAttributedLabel *commentLabel;
@property (strong, nonatomic) Comment_share *comment;
@property (weak, nonatomic) id <STXCommentCellDelegate> delegate;

- (instancetype)initWithStyle:(STXCommentCellStyle)style comment:(Comment_share *)comment reuseIdentifier:(NSString *)reuseIdentifier;

- (instancetype)initWithStyle:(STXCommentCellStyle)style totalComments:(NSInteger)totalComments reuseIdentifier:(NSString *)reuseIdentifier;

@end
