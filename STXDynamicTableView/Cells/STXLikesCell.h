//
//  STXLikesCell.h
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

//@import UIKit;
#import "TTTAttributedLabel.h"
#import "NSObject+MTJSONUtils.h"
#import "UIView+AutoLayout.h"

typedef NS_ENUM(NSUInteger, STXLikesCellStyle) {
    STXLikesCellStyleLikesCount,
    STXLikesCellStyleLikers
};

@class STXLikesCell;

@protocol STXLikesCellDelegate <NSObject>

@optional

- (void)likesCellWillShowLikes:(STXLikesCell *)likesCell;
- (void)likesCellDidSelectLiker:(NSString *)liker;

@end

@interface STXLikesCell : UITableViewCell <TTTAttributedLabelDelegate> {
    TTTAttributedLabel *likesLabel;
}

@property (copy, nonatomic) NSDictionary *likes;

@property (weak, nonatomic) id <STXLikesCellDelegate> delegate;

@property (strong, nonatomic) TTTAttributedLabel *likesLabel;

- (instancetype)initWithStyle:(STXLikesCellStyle)style likes:(NSMutableArray *)likes reuseIdentifier:(NSString *)reuseIdentifier;

-(void)setCustomLabel:(STXLikesCellStyle)style likes:(NSMutableArray *)likes;

- (void)setLikesLabel:(TTTAttributedLabel *)likesLabels count:(NSInteger)count;
- (void)setLikersLabel:(TTTAttributedLabel *)likersLabel likers:(NSMutableArray *)likers;

@end
