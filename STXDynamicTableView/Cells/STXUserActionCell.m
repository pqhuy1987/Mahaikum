//
//  STXUserActionCell.m
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

#import "STXUserActionCell.h"
#import "STXPostItem.h"
#import "UIButton+STXButton.h"

@interface STXUserActionCell ()

@end

@implementation STXUserActionCell
@synthesize dlstarObj;
@synthesize likeButton,commentButton,shareButton;

-(void)awakeFromNib {
    [super awakeFromNib];
    self.buttonDividerLeft.backgroundColor = [UIColor blackColor];
    self.buttonDividerRight.backgroundColor = [UIColor blackColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)layoutSubviews {
    [super layoutSubviews];
//    if ([self.postItem liked]) {
//        [self setButton:self.likeButton toLoved:YES];
//    } else {
//        [self setButton:self.likeButton toLoved:NO];
//    }
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setPostItem:(id<STXPostItem>)postItem
{
    _postItem = postItem;
    
    NSString *likeButtonTitle = postItem.liked ? NSLocalizedString(@"Loved", nil) : NSLocalizedString(@"Love", nil);
    [self.likeButton setTitle:likeButtonTitle forState:UIControlStateNormal];
    
    
    if (postItem.liked) {
        [self.likeButton stx_setSelectedAppearance];
    } else {
        [self.likeButton stx_setNormalAppearance];
    }
}

#pragma mark - Action

- (void)setButton:(UIButton *)button toLoved:(BOOL)loved
{
    if (loved) {
        [button setTitle:NSLocalizedString(@"Loved", nil) forState:UIControlStateNormal];
        [button stx_setSelectedAppearance];
    } else {
        [button setTitle:NSLocalizedString(@"Love", nil) forState:UIControlStateNormal];
        [button stx_setNormalAppearance];
    }
}

- (IBAction)like:(id)sender {
    if ([self.delegate respondsToSelector:@selector(userDidLike:)]) {
        [self.delegate userDidLike:self];
    }
}

- (IBAction)comment:(id)sender {
    if ([self.delegate respondsToSelector:@selector(userWillComment:)]) {
        [self.delegate userWillComment:self];
    }
}

- (IBAction)share:(id)sender {
    if ([self.delegate respondsToSelector:@selector(userWillShare:)]) {
        [self.delegate userWillShare:self];
    }
}


@end
