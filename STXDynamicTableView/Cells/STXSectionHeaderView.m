
#import "STXSectionHeaderView.h"

#import "UIImageView+Masking.h"
#import "UIImageView+AFNetworking.h"

@implementation STXSectionHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.view.frame = frame;
        [[NSBundle mainBundle] loadNibNamed:@"STXSectionHeaderView" owner:self options:nil];
        [self addSubview:self.view];
        
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.profileImageView.clipsToBounds = YES;
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addSubview:self.view];
}

- (void)setPostItem:(id<STXPostItem>)postItem {
    if (_postItem != postItem) {
        _postItem = postItem;
        
        self.dateLabel.textColor = [UIColor blackColor];
        
        id<STXUserItem> userItem = [postItem user];
        NSString *name = [userItem fullname];
        self.profileLabel.text = name;
        NSURL *profilePhotoURL = [userItem profilePictureURL];
        
        [self.profileImageView setCircleImageWithURL:profilePhotoURL placeholderImage:[UIImage imageNamed:@"ProfilePlaceholder"] borderWidth:2];
    }
}

- (IBAction)btnProfileClicked:(id)sender {
    if ([self.delegate respondsToSelector:@selector(btnProfileClicked:)]) {
        [self.delegate btnProfileClicked:sender];
    }
}

@end
