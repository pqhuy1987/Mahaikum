//
//  STXCommentCell.m
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

#import "STXCommentCell.h"
#import "Comment_share.h"

static CGFloat STXCommentViewLeadingEdgeInset = 10.f;
static CGFloat STXCommentViewTrailingEdgeInset = 10.f;

//static NSString *HashTagAndMentionRegex = @"(#|@)(\\w+)";
static NSString *HashTagAndMentionRegex = @"(#|@|http://+.+.+.|https://+.+.+.|Http://+.+.+.|Https://+.+.+.)(\\w+)";

@interface STXCommentCell () <TTTAttributedLabelDelegate>

@property (nonatomic) STXCommentCellStyle cellStyle;
//@property (strong, nonatomic) STXAttributedLabel *commentLabel;

@property (nonatomic) BOOL didSetupConstraints;

@end

@implementation STXCommentCell
@synthesize comment;

+ (Class)STXCommentCell {
    return [STXCommentCell class];
}

//- (id)initWithStyle:(STXCommentCellStyle)style comment:(id<STXCommentItem>)comment totalComments:(NSInteger)totalComments reuseIdentifier:(NSString *)reuseIdentifier
- (id)initWithStyle:(STXCommentCellStyle)style comment:(Comment_share *)commentss totalComments:(NSInteger)totalComments reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _cellStyle = style;
        
        if (style == STXCommentCellStyleShowAllComments) {
            NSString *title = [NSString stringWithFormat:NSLocalizedString(@"view all %d comments", nil), totalComments];
            _commentLabel = [self allCommentsLabelWithTitle:title];
            _commentLabel.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture = \
            [[UITapGestureRecognizer alloc]
             initWithTarget:self action:@selector(didCickViewAllCommentWithGesture:)];
            [_commentLabel addGestureRecognizer:tapGesture];
            
        } else {
            _commentLabel = [self commentLabelWithText:commentss.comment_desc commenter:commentss.username];
        }
        
        [self.contentView addSubview:_commentLabel];
        _commentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        // nazima changed //
        // self.contentView.bounds = CGRectMake(0, 0, 99999, 100000);
        //              //
        
        [self.commentLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXCommentViewLeadingEdgeInset, 0, STXCommentViewTrailingEdgeInset)];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
   }
    return self;
}
- (void)didCickViewAllCommentWithGesture:(UITapGestureRecognizer *)tapGesture {
    if ([self.delegate respondsToSelector:@selector(userWillComment:)]) {
        [self.delegate userWillComment:self];
    }
}
//- (instancetype)initWithStyle:(STXCommentCellStyle)style comment:(id<STXCommentItem>)comment reuseIdentifier:(NSString *)reuseIdentifier
- (instancetype)initWithStyle:(STXCommentCellStyle)style comment:(Comment_share *)commentsa reuseIdentifier:(NSString *)reuseIdentifier
{
    return [self initWithStyle:style comment:commentsa totalComments:0 reuseIdentifier:reuseIdentifier];
}

- (instancetype)initWithStyle:(STXCommentCellStyle)style totalComments:(NSInteger)totalComments reuseIdentifier:(NSString *)reuseIdentifier
{
    return [self initWithStyle:style comment:nil totalComments:totalComments reuseIdentifier:reuseIdentifier];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.commentLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.frame) - (STXCommentViewLeadingEdgeInset + STXCommentViewTrailingEdgeInset);
    
    // self.commentLabel.textColor=[UIColor blackColor];//[UIColor whiteColor];
}

- (void)updateConstraints {
    [super updateConstraints];
    if (!self.didSetupConstraints) {
        
        // nazima changed //
        // self.contentView.bounds = CGRectMake(0, 0, 99999, 100000);
        //              //
        
        [self.commentLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXCommentViewLeadingEdgeInset, 0, STXCommentViewTrailingEdgeInset)];
        
        //self.didSetupConstraints = YES;
    }
}

- (void)setComment:(Comment_share *)commentsd {    
        [self setCommentLabel:self.commentLabel text:commentsd.comment_desc commenter:[StaticClass urlDecode:commentsd.username]];
}

- (void)setTotalComments:(NSInteger)totalComments
{
    if (_totalComments != totalComments) {
        NSString *title = [NSString stringWithFormat:NSLocalizedString(@"view all %d comments", nil), totalComments];
        [self setAllCommentsLabel:self.commentLabel title:title];
    }
}

#pragma mark - Attributed Label

- (void)setAllCommentsLabel:(STXAttributedLabel *)commentLabel title:(NSString *)title
{
    [commentLabel setText:title];
}

- (void)setCommentLabel:(STXAttributedLabel *)commentLabel text:(NSString *)text commenter:(NSString *)commenter
{
    NSMutableArray *textCheckingResults = [NSMutableArray array];
    
    text =[NSString stringWithFormat:@"%@ %@",commenter,text];
    
    [commentLabel setText:text afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        NSRange searchRange = NSMakeRange(0, [mutableAttributedString length]);
        
        NSRange currentRange = [[mutableAttributedString string] rangeOfString:commenter options:NSLiteralSearch range:searchRange];
        NSTextCheckingResult *textCheckingResult = [NSTextCheckingResult linkCheckingResultWithRange:currentRange URL:nil];
        [textCheckingResults addObject:textCheckingResult];
        
        NSError *error;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:HashTagAndMentionRegex options:NSRegularExpressionCaseInsensitive error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
        
        [regex enumerateMatchesInString:[mutableAttributedString string] options:0 range:NSMakeRange(0, [mutableAttributedString length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
            [textCheckingResults addObject:result];
        }];
        
        return mutableAttributedString;
    }];
   
    for (NSTextCheckingResult *result in textCheckingResults) {
        [commentLabel addLinkWithTextCheckingResult:result];
    }
}

- (STXAttributedLabel *)allCommentsLabelWithTitle:(NSString *)title
{
    STXAttributedLabel *allCommentsLabel = [STXAttributedLabel newAutoLayoutView];
    allCommentsLabel.delegate = self;
    allCommentsLabel.textColor = [UIColor colorWithRed:45.0f/255.0f green:165.0f/255.0f blue:218.0f/255.0f alpha:1];//[UIColor whiteColor];
    allCommentsLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    allCommentsLabel.font=[UIFont fontWithName:@"Helvetica neue" size:14.0];
    allCommentsLabel.linkAttributes = @{ (NSString *)kCTFontAttributeName: allCommentsLabel.font,(NSString *)kCTForegroundColorAttributeName: allCommentsLabel.textColor};
    allCommentsLabel.activeLinkAttributes = allCommentsLabel.linkAttributes;
    allCommentsLabel.inactiveLinkAttributes = allCommentsLabel.linkAttributes;
    [allCommentsLabel setText:title];
    
    NSTextCheckingResult *textCheckingResult = [NSTextCheckingResult linkCheckingResultWithRange:NSMakeRange(0, [title length]) URL:nil];
    [allCommentsLabel addLinkWithTextCheckingResult:textCheckingResult];
    
    return allCommentsLabel;
}

- (STXAttributedLabel *)commentLabelWithText:(NSString *)text commenter:(NSString *)commenter
{
    NSString *trimmedText = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *commentText = [[commenter stringByAppendingString:@" "] stringByAppendingString:trimmedText];
    
    STXAttributedLabel *commentLabel = [[STXAttributedLabel alloc] initForParagraphStyleWithText:commentText];
    commentLabel.delegate = self;
    commentLabel.numberOfLines = 0;
    commentLabel.font=[UIFont fontWithName:@"Helvetica neue" size:12.0];
    commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    commentLabel.textColor=[UIColor blackColor];//[UIColor whiteColor];
    //commentLabel.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor blueColor] };
    commentLabel.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor colorWithRed:0.0f/255.0f green:35.0f/255.0f blue:102.0f/255.0f alpha:1],(NSString *)kCTFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:12.0],(NSString *)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleNone) };
    
    commentLabel.activeLinkAttributes = commentLabel.linkAttributes;
    commentLabel.inactiveLinkAttributes = commentLabel.linkAttributes;
    
    [self setCommentLabel:commentLabel text:text commenter:commenter];
    
    return commentLabel;
}

- (void)showAllComments:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(commentCellWillShowAllComments:)])
        [self.delegate commentCellWillShowAllComments:self];
}

#pragma mark - Attributed Label

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{
    NSString *selectedText = [[label.attributedText string] substringWithRange:result.range];
    NSLog(@"%@", selectedText);
    
    if ([selectedText hasPrefix:@"Http://"]||[selectedText hasPrefix:@"Https://"]||[selectedText hasPrefix:@"http://"]|| [selectedText hasPrefix:@"https://"]|| [selectedText hasPrefix:@"www."]|| [selectedText hasPrefix:@"WWW."]|| [selectedText hasPrefix:@"Www."]) {
        NSURL *selectedURL = [NSURL URLWithString:selectedText];
        
        if (selectedURL) {
            if ([self.delegate respondsToSelector:@selector(commentCell:didSelectURL:)]) {
                [self.delegate commentCell:self didSelectURL:selectedURL];
            }
            return;
        }
    } else if ([selectedText hasPrefix:@"#"]) {
        NSString *hashtag = [selectedText substringFromIndex:1];
        
        if ([self.delegate respondsToSelector:@selector(commentCell:didSelectHashtag:)]) {
            [self.delegate commentCell:self didSelectHashtag:hashtag];
            return;
        }
    } else if ([selectedText hasPrefix:@"@"]) {
        NSString *mention = [selectedText substringFromIndex:1];
        
        if ([self.delegate respondsToSelector:@selector(commentCell:didSelectMention:)]) {
            [self.delegate commentCell:self didSelectMention:mention];
            return;
        }
    }
    else {
        //NSLog(@"d54565ds4f56ad4s4f56d4");
    }
    
    if (self.cellStyle == STXCommentCellStyleShowAllComments) {
        [self showAllComments:label];
    } else {
        if ([self.delegate respondsToSelector:@selector(commentCell:willShowCommenter:)]) {
            //[self.delegate commentCell:self willShowCommenter:comment];
            [self.delegate commentCell:self willShowCommenter:selectedText];
        }
    }
}

@end
