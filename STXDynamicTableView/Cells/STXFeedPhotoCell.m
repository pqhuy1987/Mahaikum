//
//  STXFeedPhotoCell.m
//  STXDynamicTableView
//
//  Created by Triệu Khang on 24/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

#import "STXFeedPhotoCell.h"

#import "UIImageView+Masking.h"
#import "UIImageView+AFNetworking.h"

@interface STXFeedPhotoCell ()

@end

@implementation STXFeedPhotoCell
@synthesize postImageView;
@synthesize lblavgrating,lbltotrating,ratingview,ratingimg;
@synthesize dlstarAvgObj;

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.postImageView.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *likeGestures=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(likeGesturesTap:)];
    [likeGestures setNumberOfTapsRequired:2];
    [self.postImageView addGestureRecognizer:likeGestures];
    
    self.postImageView.clipsToBounds = YES;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setPostItem:(id<STXPostItem>)postItem {
    if (_postItem != postItem) {
        _postItem = postItem;
        
        [self.postImageView sd_setImageWithURL:postItem.photoURL];
      //  [self.postImageView setImageWithURL:postItem.photoURL placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
}

- (UIImage *)photoImage {
    _photoImage = self.postImageView.image;
    return _photoImage;
}

- (void)cancelImageLoading {
    [self.postImageView cancelImageRequestOperation];
    self.postImageView.image = nil;
}

#pragma mark - Actions

- (void)profileTapped:(UIGestureRecognizer *)recognizer
{
    if ([self.delegate respondsToSelector:@selector(feedCellWillShowPoster:)])
        [self.delegate feedCellWillShowPoster:[self.postItem user]];
}

-(void)likeGesturesTap:(id)recognizer {
    if ([self.delegate respondsToSelector:@selector(feedCellWillShowLikeUnlike:)])
        [self.delegate feedCellWillShowLikeUnlike:self];
}

- (void)dealloc {
//    [super dealloc];
}
@end
