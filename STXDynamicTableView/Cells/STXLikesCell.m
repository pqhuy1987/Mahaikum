//
//  STXLikesCell.m
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

#import "STXLikesCell.h"

static CGFloat STXLikesViewLeadingEdgeInset = 25.f;
static CGFloat STXLikesViewTrailingEdgeInset = 10.f;

@interface STXLikesCell () <TTTAttributedLabelDelegate>

@property (nonatomic) BOOL didSetupConstraints;

@property (nonatomic) STXLikesCellStyle cellStyle;

@end

@implementation STXLikesCell
@synthesize likesLabel;


//+ (Class)STXLikesCell {
//    return [STXLikesCell class];
//}

-(void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)initWithStyle:(STXLikesCellStyle)style likes:(NSMutableArray *)likes reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _cellStyle = style;
        
        if (style == STXLikesCellStyleLikesCount) {
            NSInteger count = [likes count];
            likesLabel = [self likesLabelForCount:count];
        } else {
            likesLabel = [self likersLabelForLikers:likes];
        }
        
        [self.contentView addSubview:likesLabel];
        likesLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        // nazima changed   //
        // self.contentView.bounds = CGRectMake(0, 0, 99999, 99999);
        //                  //
        
        [self.likesLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXLikesViewLeadingEdgeInset, 0, STXLikesViewTrailingEdgeInset)];
    }
    return self;
}

-(void)setCustomLabel:(STXLikesCellStyle)style likes:(NSMutableArray *)likes {
    if (style == STXLikesCellStyleLikesCount) {
        NSInteger count = [likes count];
        likesLabel = [self likesLabelForCount:count];
    } else {
        likesLabel = [self likersLabelForLikers:likes];
    }
    
    [self.contentView addSubview:likesLabel];
    likesLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.likesLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.frame) - (STXLikesViewLeadingEdgeInset + STXLikesViewTrailingEdgeInset);
}

- (void)updateConstraints
{
    [super updateConstraints];
    if (!self.didSetupConstraints) {
        // Note: if the constraints you add below require a larger cell size
        // than the current size (which is likely to be the default size {320,
        // 44}), you'll get an exception.  As a fix, you can temporarily
        // increase the size of the cell's contentView so that this does not
        // occur using code similar to the line below.  See here for further
        // discussion:
        // https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
        
        // nazima changed   //
        // self.contentView.bounds = CGRectMake(0, 0, 99999, 99999);
        //                  //
        
        [self.likesLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXLikesViewLeadingEdgeInset, 0, STXLikesViewTrailingEdgeInset)];
        
        self.didSetupConstraints = YES;
    }
}

- (void)setLikes:(NSDictionary *)likes {
    if (_likes != likes) {
        _likes = likes;
        
        NSInteger count = [[likes valueForComplexKeyPath:@"count"] integerValue];
        if (count > 2) {
            [self setLikesLabel:self.likesLabel count:count];
        }
        else {
            //NSArray *likers = [[likes valueForComplexKeyPath:@"data"] objectWithJSONSafeObjects];
            //[self setLikersLabel:self.likesLabel likers:likers];
        }
    }
}

#pragma mark - Attributed Label

- (void)setLikesLabel:(TTTAttributedLabel *)likesLabels count:(NSInteger)count
{
    NSString *countString = [@(count) stringValue];
    NSString *title = [NSString stringWithFormat:NSLocalizedString(@"%@ likes", nil), countString];
    likesLabels.text = title;
    likesLabel.textColor=[UIColor blackColor];//[UIColor whiteColor];
}

- (void)setLikersLabel:(TTTAttributedLabel *)likersLabel likers:(NSMutableArray *)likers
{
    NSMutableString *likersString = [NSMutableString stringWithCapacity:0];
    NSUInteger likerIndex = 0;
    NSArray *likerNames =(NSArray *)likers;// [likers valueForKey:@"name"];
    
    for (NSString *likerName in likers) {
        if ([likerName length] > 0) {
            if (likerIndex == 0)
                [likersString setString:likerName];
            else
                [likersString appendFormat:NSLocalizedString(@" and %@", nil), likerName];
        }
        
        ++likerIndex;
    }
    
    if ([likersString length] > 0) {
        if ([likers count] == 1) {
            [likersString appendString:NSLocalizedString(@" likes this", nil)];
        } else {
            [likersString appendString:NSLocalizedString(@" like this", nil)];
        }
    }
    
    NSString *coverString = [likersString copy];
    
    NSMutableArray *textCheckingResults = [NSMutableArray array];
    
    NSString *likersText = [NSString stringWithFormat:@"\u200E %@", coverString];
    
    [likersLabel setText:likersText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        NSRange searchRange = NSMakeRange(0, [mutableAttributedString length]);
        for (NSString *likerName in likerNames) {
            NSRange currentRange = [[mutableAttributedString string] rangeOfString:likerName options:NSLiteralSearch range:searchRange];
            
            NSTextCheckingResult *result = [NSTextCheckingResult linkCheckingResultWithRange:currentRange URL:nil];
            [textCheckingResults addObject:result];
            
            searchRange = NSMakeRange(currentRange.length, [mutableAttributedString length] - currentRange.length);
        }
        
        return mutableAttributedString;
    }];
    
    
    for (NSTextCheckingResult *result in textCheckingResults)
        [likersLabel addLinkWithTextCheckingResult:result];
}

- (TTTAttributedLabel *)likesLabelForCount:(NSInteger)count {
    
    TTTAttributedLabel *likesLabels = [TTTAttributedLabel newAutoLayoutView];
    likesLabels.textColor =[UIColor blackColor];// [UIColor colorWithRed:45.0f/255.0f green:165.0f/255.0f blue:218.0f/255.0f alpha:1];//[UIColor blueColor];
    
    likesLabels.delegate = self;
    likesLabels.font=[UIFont fontWithName:@"Helvetica neue" size:14.0];
    [self setLikesLabel:likesLabels count:count];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showLikers:)];
    [likesLabels addGestureRecognizer:recognizer];
    
    return likesLabels;
}

- (TTTAttributedLabel *)likersLabelForLikers:(NSMutableArray *)likers {
    TTTAttributedLabel *likersLabels = [TTTAttributedLabel newAutoLayoutView];
    likersLabels.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    likersLabels.numberOfLines = 0;
    likersLabels.lineBreakMode = NSLineBreakByWordWrapping;
    likersLabels.delegate = self;
    likersLabels.textColor=[UIColor blackColor];//[UIColor whiteColor];
    likersLabels.font=[UIFont fontWithName:@"Helvetica neue" size:12.0];
    
    //likersLabels.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor blueColor],(NSString *)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleNone) };
    
    likersLabels.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor colorWithRed:0.0f/255.0f green:35.0f/255.0f blue:102.0f/255.0f alpha:1],(NSString *)kCTFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:12.0],(NSString *)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleNone) };
    
    likersLabels.activeLinkAttributes = likersLabels.linkAttributes;
    likersLabels.inactiveLinkAttributes = likersLabels.linkAttributes;
    
    [self setLikersLabel:likersLabels likers:likers];
    
    return likersLabels;
}

#pragma mark - Actions

- (void)showLikers:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(likesCellWillShowLikes:)])
        [self.delegate likesCellWillShowLikes:self];
}

#pragma mark - Attributed Label

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result
{
    NSString *string = [[label.attributedText string] substringWithRange:result.range];
    NSLog(@"%@", string);
    
    if ([self.delegate respondsToSelector:@selector(likesCellDidSelectLiker:)])
        [self.delegate likesCellDidSelectLiker:string];
}

@end
