//
//  STXCaptionCell.m
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

#import "STXCaptionCell.h"

static CGFloat STXCaptionViewLeadingEdgeInset = 25.f;
static CGFloat STXCaptionViewTrailingEdgeInset = 100.0f;

static NSString *HashTagAndMentionRegex = @"(#|@)(\\w+)";

@interface STXCaptionCell () <TTTAttributedLabelDelegate>

@property (nonatomic) BOOL didSetupConstraints;

@end

@implementation STXCaptionCell

+(Class)STXCaptionCell {
    return [STXCaptionCell class];
}

-(id)initWithCaption:(NSString *)caption reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
       // NSString *text = [caption stringValueForComplexKeyPath:@"text"];
        _captionLabel = [self captionLabelWithText:caption];
        
        // price label
        _priceLabel = [UILabel newAutoLayoutView];
        _priceLabel.textColor = [GlobalDefine colorWithHexString:@"F52E3A"];
        _priceLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0];
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _priceLabel.numberOfLines = 0;
        [self.contentView addSubview:_captionLabel];
        [self.contentView addSubview:_priceLabel];
        
        _captionLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.captionLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXCaptionViewLeadingEdgeInset, 0, STXCaptionViewTrailingEdgeInset)];
        
        [_priceLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_captionLabel withOffset:2.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:10.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0];
    }
    
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.captionLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.frame) - (STXCaptionViewLeadingEdgeInset + STXCaptionViewTrailingEdgeInset);
    
    [super layoutSubviews];
}

-(void)updateConstraints {
    if (!self.didSetupConstraints) {
        // Note: if the constraints you add below require a larger cell size
        // than the current size (which is likely to be the default size {320,
        // 44}), you'll get an exception.  As a fix, you can temporarily
        // increase the size of the cell's contentView so that this does not
        // occur using code similar to the line below.  See here for further
        // discussion:
        // https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
        
        // nazima changed   //
        // self.contentView.bounds = CGRectMake(0, 0, 99999, 99999);
        //                  //
        
        [self.captionLabel autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(0, STXCaptionViewLeadingEdgeInset, 0, STXCaptionViewTrailingEdgeInset)];
        
        [_priceLabel autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:_captionLabel withOffset:5.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:10.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0.0];
        [_priceLabel autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0];
        
        self.didSetupConstraints = YES;
    }
    
    [super updateConstraints];
}

-(void)setCaption:(NSString *)caption {
    if (_caption != caption) {
        _caption = caption;
        
        //NSString *text = [caption stringValueForComplexKeyPath:@"text"];
        [self setCaptionLabel:self.captionLabel text:caption];
    }
}

#pragma mark - Attributed Label

-(void)setCaptionLabel:(STXAttributedLabel *)captionLabel text:(NSString *)text {
    NSString *trimmedText = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSMutableArray *textCheckingResults = [NSMutableArray array];
    [captionLabel setText:trimmedText afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        NSError *error;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:HashTagAndMentionRegex
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
        
        [regex enumerateMatchesInString:[mutableAttributedString string] options:0 range:NSMakeRange(0, [mutableAttributedString length]) usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
            [textCheckingResults addObject:result];
        }];
        
        return mutableAttributedString;
    }];
    
    for (NSTextCheckingResult *result in textCheckingResults) {
        [captionLabel addLinkWithTextCheckingResult:result];
    }
}

-(STXAttributedLabel *)captionLabelWithText:(NSString *)text {
    STXAttributedLabel *captionLabel = [[STXAttributedLabel alloc] initForParagraphStyleWithText:text];
    captionLabel.textColor = [UIColor blackColor];
    captionLabel.font = [UIFont fontWithName:@"Helvetica neue" size:12.0];
    captionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    captionLabel.numberOfLines = 0;
    captionLabel.delegate = self;
    captionLabel.userInteractionEnabled = YES;
    
   // captionLabel.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor blueColor],(NSString *)kCTFontAttributeName: [UIFont boldSystemFontOfSize:14],(NSString *)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleNone) };
    
    captionLabel.linkAttributes = @{ (NSString *)kCTForegroundColorAttributeName: [UIColor colorWithRed:0.0f/255.0f green:35.0f/255.0f blue:102.0f/255.0f alpha:1],(NSString *)kCTFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:12.0],(NSString *)kCTUnderlineStyleAttributeName : @(kCTUnderlineStyleNone) };
    
    
    
    captionLabel.activeLinkAttributes = captionLabel.linkAttributes;
    captionLabel.inactiveLinkAttributes = captionLabel.linkAttributes;
    [self setCaptionLabel:captionLabel text:text];
    
    return captionLabel;
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithTextCheckingResult:(NSTextCheckingResult *)result {
    NSString *hashtagOrMention = [[label.attributedText string] substringWithRange:result.range];
    NSLog(@"%@", hashtagOrMention);
    
    if ([hashtagOrMention hasPrefix:@"#"]) {
        NSString *hashtag = [hashtagOrMention substringFromIndex:1];
        
        if ([self.delegate respondsToSelector:@selector(captionCell:didSelectHashtag:)]) {
            [self.delegate captionCell:self didSelectHashtag:hashtag];
        }
    } else if ([hashtagOrMention hasPrefix:@"@"]) {
        NSString *mention = [hashtagOrMention substringFromIndex:1];
        
        if ([self.delegate respondsToSelector:@selector(captionCell:didSelectMention:)]) {
            [self.delegate captionCell:self didSelectMention:mention];
        }
    }
}

@end