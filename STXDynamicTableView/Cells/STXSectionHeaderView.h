
#import <UIKit/UIKit.h>

@class STXSectionHeaderView;

@protocol STXSectionHeaderViewDelegate <NSObject>

@optional
- (void)btnProfileClicked:(id)sender;

@end

@interface STXSectionHeaderView : UIView

@property (nonatomic, strong) IBOutlet UIView *view;

@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhotoBG;
@property (weak, nonatomic) IBOutlet UILabel *profileLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UIImageView *imgLocationIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;

@property (strong, nonatomic) id <STXPostItem> postItem;
@property (weak, nonatomic) id <STXSectionHeaderViewDelegate> delegate;

@end
