//
//  STXUserActionCell.h
//  STXDynamicTableViewExample
//
//  Created by Jesse Armand on 10/4/14.
//  Copyright (c) 2014 2359 Media Pte Ltd. All rights reserved.
//

//@import UIKit;
#import "STXPostItem.h"
#import "DLStarRatingControl.h"

@class STXUserActionCell;

@protocol STXUserActionDelegate <NSObject,DLStarRatingDelegate>

- (void)userWillComment:(STXUserActionCell *)userActionCell;
- (void)userWillShare:(STXUserActionCell *)userActionCell;
- (void)userDidLike:(STXUserActionCell *)userActionCell;
- (void)userDidUnlike:(STXUserActionCell *)userActionCell;

@end

@interface STXUserActionCell : UITableViewCell {
    IBOutlet DLStarRatingControl *dlstarObj;
    
    UIButton *likeButton;
    UIButton *commentButton;
    UIButton *shareButton;
}

@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *commentButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIView *buttonDividerLeft;
@property (weak, nonatomic) IBOutlet UIView *buttonDividerRight;

@property (weak, nonatomic) id <STXUserActionDelegate> delegate;

@property (copy, nonatomic) id <STXPostItem> postItem;
@property (copy, nonatomic) NSIndexPath *indexPath;
@property (retain, nonatomic) IBOutlet DLStarRatingControl *dlstarObj;

@end
