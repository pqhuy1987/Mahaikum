//
//  STXFeedPhotoCell.h
//  STXDynamicTableView
//
//  Created by Triệu Khang on 24/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

//@import UIKit;

#import "STXUserItem.h"
//#import "UIImageView+AFNetworking.h"
#import "DLStarRatingControl.h"

@class STXFeedPhotoCell;

@protocol STXFeedPhotoCellDelegate <NSObject>

@optional

- (void)feedCellWillShowPoster:(id <STXUserItem>)poster;
- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike;

@end

@interface STXFeedPhotoCell : UITableViewCell {
    IBOutlet DLStarRatingControl *dlstarAvgObj;
    UILabel *lblavgrating,*lbltotrating;
    UIView *ratingview;
    UIImageView *ratingimg;
    UIImageView *imgUserPhotoBG;
    UIImageView *imgLocationIcon;
    UIButton *btnProfile;
}

@property (strong, nonatomic) NSIndexPath *indexPath;

@property (strong, nonatomic) id <STXPostItem> postItem;
@property (strong, nonatomic) UIImage *photoImage;

@property (weak, nonatomic) id <STXFeedPhotoCellDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *lblavgrating,*lbltotrating;
@property (strong, nonatomic) IBOutlet UIView *ratingview;
@property (strong, nonatomic) IBOutlet UIImageView *ratingimg;
@property (strong, nonatomic) IBOutlet DLStarRatingControl *dlstarAvgObj;

@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (strong, nonatomic) IBOutlet UIImageView *soldImageView;

- (void)cancelImageLoading;

@end
