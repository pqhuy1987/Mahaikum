//
//  STXFeedTableViewDataSource.m
//  STXDynamicTableView
//
//  Created by Jesse Armand on 27/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

#import "STXFeedTableViewDataSource.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"
#import "Singleton.h"

#define PHOTO_CELL_ROW 0
#define LIKES_CELL_ROW 1
#define CAPTION_CELL_ROW 2

#define NUMBER_OF_STATIC_ROWS 4
#define MAX_NUMBER_OF_COMMENTS 4

static CGFloat const PhotoCellRowHeight1 = 360;

@interface STXFeedTableViewDataSource ()
{
    NSMutableDictionary *_heightDictionary;
}

@property (weak, nonatomic) id<STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate> controller;
@end

@implementation STXFeedTableViewDataSource

-(instancetype)initWithController:(id<STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate>)controller tableView:(UITableView *)tableView {
    self = [super init];
    _heightDictionary = [NSMutableDictionary dictionary];
    if (self) {
        _controller = controller;
        
//        NSString *feedPhotoCellIdentifier = NSStringFromClass([STXFeedPhotoCell class]);
//        UINib *feedPhotoCellNib = [UINib nibWithNibName:feedPhotoCellIdentifier bundle:nil];
//        [tableView registerNib:feedPhotoCellNib forCellReuseIdentifier:feedPhotoCellIdentifier];
        
//        NSString *userActionCellIdentifier = NSStringFromClass([STXUserActionCell class]);
  //      UINib *userActionCellNib = [UINib nibWithNibName:userActionCellIdentifier bundle:nil];
    //    [tableView registerNib:userActionCellNib forCellReuseIdentifier:userActionCellIdentifier];
    }
    return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.posts count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rowCount = 2;
    
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:section];
    
    if ([shareObjs.likes integerValue]>0) {
        rowCount++;
    }
    
    NSInteger commentsCount=0;
    if ([shareObjs.array_comments_no_description count]>0) {
         commentsCount = MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count]);
        rowCount = rowCount+commentsCount;
    }
    
//    if (!self.promotion)
        rowCount=rowCount+1;
    return rowCount;
}

-(STXFeedPhotoCell *)photoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"STXFeedPhotoCell";
	STXFeedPhotoCell *cell = (STXFeedPhotoCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"STXFeedPhotoCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
	}
    
    
//    NSString *CellIdentifier = NSStringFromClass([STXFeedPhotoCell class]);
//    STXFeedPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    if (cell.indexPath != nil && cell.indexPath.section != indexPath.section) {
        [cell cancelImageLoading];
    }
    
    cell.indexPath = indexPath;
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    cell.delegate = self.controller;
    
    [cell.postImageView startLoaderWithTintColor:[UIColor colorWithRed:255.0f/255.0f green:72.0f/255.0f blue:74.0f/255.0f alpha:1.0f]];
    
//    __weak UIImageView *cellWeakPostImageView;
    [cell.postImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:shareObjs.image_path] placeholderImage:nil options:SDWebImageCacheMemoryOnly | SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        [cell.postImageView updateImageDownloadProgress:(CGFloat)receivedSize/expectedSize];

//        MBProgressHUD *hud;
//        
//        hud = (MBProgressHUD *)[cell.postImageView viewWithTag:100];
//        if (hud == nil)
//        {
//            hud = [MBProgressHUD showHUDAddedTo:cell.postImageView animated:YES];
//            hud.tag = 100;
//        }
////        hud.backgroundColor = [UIColor blackColor];
//        hud.mode = MBProgressHUDModeAnnularDeterminate;
//        float progress = (float)receivedSize / (float)expectedSize;
//        progress = progress < 0.0 ? 0.0 : progress;
//        hud.progress = progress;
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [cell.postImageView reveal];

//        MBProgressHUD *hud;
//        hud = (MBProgressHUD *)[cell.postImageView viewWithTag:100];
//        if (hud != nil)
//        {
//            [hud hide:NO];
//        }
        if (!error)
        {
//            cell.postImageView.image = image;
//            NSLog(@"photoCellSize=(%f %f %f)", image.size.width, image.size.height, cell.frame.size.width );
            [_heightDictionary setObject:[NSNumber numberWithFloat:tableView.frame.size.width*image.size.height/image.size.width] forKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
//            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
//    [cell.postImageView sd_setImageWithURL:[NSURL URLWithString:shareObjs.image_path]];
    
//    cell.postImageView.contentMode=UIViewContentModeScaleAspectFit;
    cell.postImageView.contentMode=UIViewContentModeScaleToFill;
    cell.postImageView.clipsToBounds=YES;
    
    if (shareObjs.sold)
    {
        cell.soldImageView.hidden = NO;
    }
    else
    {
        cell.soldImageView.hidden = YES;
    }
    
    if (self.promotion)
        cell.ratingview.hidden = YES;
    else
    {
        if ([shareObjs.avgrating integerValue] == 1) {
            cell.ratingview.hidden = NO;
            cell.ratingimg.image = [UIImage imageNamed:@"1star2.png"];
        }
        else if ([shareObjs.avgrating integerValue] == 2){
            cell.ratingview.hidden = NO;
            cell.ratingimg.image = [UIImage imageNamed:@"2star2.png"];
        }
        else if ([shareObjs.avgrating integerValue] == 3){
            cell.ratingview.hidden = NO;
            cell.ratingimg.image = [UIImage imageNamed:@"3star2.png"];
        }
        else if ([shareObjs.avgrating integerValue] == 4){
            cell.ratingview.hidden = NO;
            cell.ratingimg.image = [UIImage imageNamed:@"4star2.png"];
        }
        else if ([shareObjs.avgrating integerValue] == 5){
            cell.ratingview.hidden = NO;
            cell.ratingimg.image = [UIImage imageNamed:@"5star2.png"];
        }
        else{
            cell.ratingview.hidden = YES;
        }
        cell.ratingview.hidden = YES;
    }
    
    cell.dlstarAvgObj.userInteractionEnabled=NO;
    
    cell.lblavgrating.text=[shareObjs avgrating];
    cell.lbltotrating.text=shareObjs.totalUser;
    
    cell.tag=indexPath.section;
    
    return cell;
}

-(NSString *)get_time_different:(NSString *)datestring {
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60) {
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24) {
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31) {
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12) {
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else {
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

-(STXLikesCell *)likesCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    /*
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    static NSString *cellIdentifier = @"STXLikesCell";
    STXLikesCell *cell = (STXLikesCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"STXLikesCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        cell.showsReorderControl = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor =[UIColor clearColor];
        
        if ([shareObjs.likes integerValue] > 2) {
            [cell setCustomLabel:STXLikesCellStyleLikesCount likes:shareObjs.array_liked_by];
        }
        else {
            [cell setCustomLabel:STXLikesCellStyleLikers likes:shareObjs.array_liked_by];
        }
    }
    
    if ([shareObjs.likes integerValue] > 2) {
        [cell setLikesLabel:cell.likesLabel count:[shareObjs.array_liked_by count]];
    }
    else {
        
        [cell setLikersLabel:cell.likesLabel likers:shareObjs.array_liked_by];
    }
    
    cell.delegate = self.controller;
    cell.backgroundColor= [UIColor colorWithRed:236/255.0 green:223/255.0 blue:226/255.0 alpha:1.0];
    cell.tag=indexPath.section;
    
    return cell;
    */
    
    STXLikesCell *cell;
    
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    if (cell == nil) {
        if ([shareObjs.likes integerValue] > 2) {
            static NSString *CellIdentifier = @"STXLikesCountCell";
          //  cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = [[STXLikesCell alloc] initWithStyle:STXLikesCellStyleLikesCount likes:shareObjs.array_liked_by reuseIdentifier:CellIdentifier];
        }
        else {
            static NSString *CellIdentifier = @"STXLikersCell";
          //  cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = [[STXLikesCell alloc] initWithStyle:STXLikesCellStyleLikers likes:shareObjs.array_liked_by reuseIdentifier:CellIdentifier];
        }
    }
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 15, 15)];
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"likes-cell.png"]];
    [cell.contentView addSubview:imageView];
    cell.delegate = self.controller;
    cell.backgroundColor= [UIColor whiteColor];
    cell.tag=indexPath.section;
    return cell;
    
}

-(CGFloat)heightOfPhotoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = [_heightDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.section]];
    
//    NSLog(@"heightOfPhotoCellForTableView=%d, %f", height.intValue, tableView.frame.size.width);
    
    if ( height == nil )
        return tableView.frame.size.width;
    else
        return height.floatValue;
}

-(STXCaptionCell *)captionCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    NSString *strComments=[NSString stringWithFormat:@"%@",[StaticClass urlDecode:shareObjs.description]];
    
    NSString *strPrice = @"";
    
    if ( shareObjs.price.length != 0 )
        strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObjs.currency], shareObjs.price];
    
    NSString *CellIdentifier = NSStringFromClass([STXCaptionCell class]);
    STXCaptionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[STXCaptionCell alloc] initWithCaption:strComments reuseIdentifier:CellIdentifier];
        cell.delegate = self.controller;
    }
    
    if (strComments.length != 0)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 15, 15)];
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"caption-cell.png"]];
        [cell.contentView addSubview:imageView];
    }
    
    cell.backgroundColor= [UIColor whiteColor];
    cell.caption = strComments;// [post caption];
    cell.priceLabel.text = strPrice;
    
    if (self.promotion)
        cell.priceLabel.text = @"";
    
    if ([strPrice isEqualToString:@"(null)(null)"])
        cell.priceLabel.text = @"";
    
    cell.tag=indexPath.section;
    return cell;
}

-(STXCommentCell *)commentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    int values=3;//2
    if ([shareObjs.likes integerValue]==0) {
        values=2;//1
    }
    
    STXCommentCell *cell;
   
    if (indexPath.row==values && [shareObjs.array_comments_no_description count] > MAX_NUMBER_OF_COMMENTS) {
        static NSString *AllCommentsCellIdentifier = @"STXAllCommentsCell";
        cell = [tableView dequeueReusableCellWithIdentifier:AllCommentsCellIdentifier];
    
        if (cell == nil) {
            cell = [[STXCommentCell alloc] initWithStyle:STXCommentCellStyleShowAllComments totalComments:[shareObjs.array_comments_no_description count] reuseIdentifier:AllCommentsCellIdentifier];
        }
        else {
            
            cell.totalComments = [shareObjs.array_comments_no_description count];
        }

        cell.totalComments = [shareObjs.array_comments_no_description count];
    }
    else {
        
        // nazima changed this code //
        // static NSString *CellIdentifier = @"STXSingleCommentCell";
        // cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //                          //
        
        int ij=0;
        if (shareObjs.array_comments_no_description.count > MAX_NUMBER_OF_COMMENTS) {
            ij=1;
        }
        NSMutableArray* reversedArray = shareObjs.array_comments_no_description;
        
        // NSMutableArray* reversedArray = [[NSMutableArray alloc] initWithArray:[[[[NSArray alloc] initWithArray:shareObjs.array_comments_no_description ] reverseObjectEnumerator] allObjects]];
        NSMutableArray *comments = (NSMutableArray *)reversedArray;
        if (indexPath.row-values < [comments count]) {
            
            // nazima changed   //
            // Comment_share *obj=[comments objectAtIndex:indexPath.row-values-ij];
            NSInteger commentsCount = MIN(MAX_NUMBER_OF_COMMENTS, comments.count);
            NSInteger index = comments.count - (commentsCount - (indexPath.row - values - ij));
            if (comments.count > MAX_NUMBER_OF_COMMENTS)
                index ++;
            
            Comment_share *obj = [comments objectAtIndex:index];
            //                  //
            
            if (cell == nil) {
                cell = [[STXCommentCell alloc] initWithStyle:STXCommentCellStyleSingleComment comment:obj reuseIdentifier:nil];
            }
            else {
                cell.comment=obj;
            }
            cell.comment=obj;
            
            /// nazima added this code ///
            // NSInteger index = comments.count - (indexPath.row - values - ij) - 1;
            // CGFloat alpha = (float)((float)(index + 1) / (float)(comments.count + 1));
            // NSLog(@"alpha = %lf comments = %@", alpha, obj.comment_desc);
            // cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:alpha];
            /////////////////
        }
    }
    
    cell.delegate = self.controller;
    cell.tag=indexPath.section;
    cell.backgroundColor= [UIColor whiteColor];
    return cell;
}

-(STXUserActionCell *)userActionCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"STXUserActionCell";
	STXUserActionCell *cell = (STXUserActionCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"STXUserActionCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //cell.backgroundColor = [UIColor clearColor];
	}
    
    
    //STXUserActionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([STXUserActionCell class]) forIndexPath:indexPath];
    cell.delegate = self.controller;
    cell.dlstarObj.delegate = self.controller;
    cell.dlstarObj.tag=indexPath.section;
    
    cell.commentButton.tag=indexPath.section;
    cell.likeButton.tag=indexPath.section;
    cell.shareButton.tag=indexPath.section;
    
    if (self.promotion)
    {
        cell.commentButton.hidden = YES;
    }
    else
    {
        cell.commentButton.hidden = NO;
    }
    
    cell.tag=indexPath.section;
    
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    [cell.dlstarObj setTag:indexPath.section];
    [cell.dlstarObj setRating:[shareObjs.my_rating intValue]];
    
    cell.dlstarObj.hidden = YES;
    if ([shareObjs.liked isEqualToString:@"no"]) {
        [cell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else {
        [cell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
    //cell.backgroundColor= [UIColor whiteColor];
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;

    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    NSInteger rowCount=2;//1
    
//    if (![shareObjs.description isEqualToString:@""]&&shareObjs.description!=nil) {
//        rowCount=rowCount+1;
//    }
    if ([shareObjs.likes integerValue]>0) {
        rowCount++;
    }
   // NSInteger indCount=[self.array_comments_no_description count] - 1 - indexPath.row;
    //Comment_share *obj=[self.array_comments_no_description objectAtIndex:indCount];

    NSInteger commentsCount=0;
    if ([shareObjs.array_comments_no_description count]>0) {
        
        commentsCount= MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count]);
        rowCount=rowCount+commentsCount;
    }
    rowCount=rowCount+1;

    if (indexPath.row == PHOTO_CELL_ROW) {
        cell = [self photoCellForTableView:tableView atIndexPath:indexPath];
    }
    else if(indexPath.row==1) {
        cell = [self captionCellForTableView:tableView atIndexPath:indexPath];
    }
    else if ([shareObjs.likes integerValue]>0 && indexPath.row==2) {
        cell = [self likesCellForTableView:tableView atIndexPath:indexPath];
    }
    else if ([shareObjs.likes integerValue]>0 &&[shareObjs.array_comments_no_description count]>0&& indexPath.row>=3 && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+3) {
        cell = [self commentCellForTableView:tableView atIndexPath:indexPath];
    }
    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=2 && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
        cell = [self commentCellForTableView:tableView atIndexPath:indexPath];
    }
    else {
        cell = [self userActionCellForTableView:tableView atIndexPath:indexPath];
    }
    
    //    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=2 &&rowCount-2!=indexPath.row && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
    
    
//    if (indexPath.row == PHOTO_CELL_ROW) {
//        cell = [self photoCellForTableView:tableView atIndexPath:indexPath];
//    }
//    else if ([shareObjs.likes integerValue]>0 && indexPath.row==1) {
//        cell = [self likesCellForTableView:tableView atIndexPath:indexPath];
//    }
//    else if ([shareObjs.likes integerValue]>0 &&[shareObjs.array_comments_no_description count]>0&& indexPath.row>=2 && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
//        cell = [self commentCellForTableView:tableView atIndexPath:indexPath];
//    }
//    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=1 &&rowCount-1!=indexPath.row && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+1) {
//        cell = [self commentCellForTableView:tableView atIndexPath:indexPath];
//        cell = [self commentCellForTableView:tableView atIndexPath:indexPath];
//    }
//    else {
//         cell = [self userActionCellForTableView:tableView atIndexPath:indexPath];
//    }
    return cell;
}

@end
