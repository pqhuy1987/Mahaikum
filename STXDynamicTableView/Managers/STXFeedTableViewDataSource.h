//
//  STXFeedTableViewDataSource.h
//  STXDynamicTableView
//
//  Created by Jesse Armand on 27/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

//@import Foundation;
//@import UIKit;

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol STXFeedPhotoCellDelegate;
@protocol STXLikesCellDelegate;
@protocol STXCaptionCellDelegate;
@protocol STXCommentCellDelegate;
@protocol STXUserActionDelegate;
@protocol STXSectionHeaderViewDelegate;

@class STXLikesCell;
@class STXCaptionCell;
@class STXCommentCell;

@interface STXFeedTableViewDataSource : NSObject <UITableViewDataSource>

@property (copy, nonatomic) NSMutableArray *posts;
@property (nonatomic, assign) BOOL promotion;

- (instancetype)initWithController:(id<STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate>)controller tableView:(UITableView *)tableView;

- (STXLikesCell *)likesCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (STXCaptionCell *)captionCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (STXCommentCell *)commentCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
-(CGFloat)heightOfPhotoCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;

@end
