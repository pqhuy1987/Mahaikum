//
//  STXFeedTableViewDelegate.m
//  STXDynamicTableView
//
//  Created by Jesse Armand on 27/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

#import "STXFeedTableViewDelegate.h"
#import "STXFeedTableViewDataSource.h"

#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXSectionHeaderView.h"
#import "Home_tableview_data_share.h"

#import "STXPostItem.h"

#define PHOTO_CELL_ROW 0
#define LIKES_CELL_ROW 1
#define CAPTION_CELL_ROW 2

#define MAX_NUMBER_OF_COMMENTS 4

static CGFloat const PhotoCellRowHeight = 360;
static CGFloat const UserActionCellHeight = 44;

@interface STXFeedTableViewDelegate () <UIScrollViewDelegate>

@property (weak, nonatomic) id <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate> controller;

@end

@implementation STXFeedTableViewDelegate

-(instancetype)initWithController:(id<STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate>)controller {
    self = [super init];
    if (self) {
        _controller = controller;
    }
    
    return self;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 44)];
    
    STXSectionHeaderView *headerView = [[STXSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 44)];
    [view addSubview:headerView];
    
    headerView.view.frame = CGRectMake(0, 0, kViewWidth, 44);
    [headerView setNeedsUpdateConstraints];
    [headerView updateConstraintsIfNeeded];
    
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:section];
    
    [headerView.profileImageView sd_setImageWithURL:[NSURL URLWithString:shareObjs.user_image]];
    
    headerView.profileImageView.layer.cornerRadius=16.0f;
    headerView.profileImageView.layer.masksToBounds=YES;
    
    headerView.profileImageView.contentMode=UIViewContentModeScaleAspectFill;
    headerView.profileImageView.clipsToBounds=YES;
    
    headerView.dateLabel.text=[self get_time_different:shareObjs.datecreated];
    headerView.profileLabel.text=shareObjs.username;
    
    headerView.locationLabel.hidden=NO;
    headerView.imgLocationIcon.hidden=NO;
    headerView.profileLabel.frame=CGRectMake(46,2,213,21);
    headerView.locationLabel.text=shareObjs.location;
    
    if ([shareObjs.location isEqualToString:@""] || shareObjs.location==nil || self.promotion) {
        headerView.locationLabel.hidden=YES;
        headerView.imgLocationIcon.hidden=YES;
        headerView.profileLabel.frame=CGRectMake(46,2,213,42);
    }
    
    headerView.btnProfile.tag = section;
    
    headerView.delegate = self.controller;
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0;
    //NSInteger captionRowOffset = 3;
    //NSInteger commentsRowLimit = captionRowOffset + MAX_NUMBER_OF_COMMENTS;
    
    UITableViewCell *cell;
    STXFeedTableViewDataSource *dataSource = tableView.dataSource;
    
    Home_tableview_data_share *shareObjs=[self.posts objectAtIndex:indexPath.section];
    
    NSInteger rowCount=2;
    if ([shareObjs.likes integerValue]>0) {
        rowCount++;
    }
    
    if ([shareObjs.array_comments_no_description count]>0) {
        NSInteger commentsCount = MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count]);
        rowCount=rowCount+commentsCount;
    }
    rowCount=rowCount+1;
    
    if (indexPath.row == PHOTO_CELL_ROW) {
//        return PhotoCellRowHeight - 44.0;
        return [dataSource heightOfPhotoCellForTableView:tableView atIndexPath:indexPath];
    }
    else if(indexPath.row==1) {
        if (shareObjs.description==nil||[shareObjs.description isEqualToString:@""]) {
            return 0;
        }
        else {
            cell =[dataSource captionCellForTableView:tableView atIndexPath:indexPath];
        }
    }
    else if ([shareObjs.likes integerValue]>0 && indexPath.row==2) {
        cell = [dataSource likesCellForTableView:tableView atIndexPath:indexPath];

    }
    else if ([shareObjs.likes integerValue]>0 &&[shareObjs.array_comments_no_description count]>0&& indexPath.row>=3 && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+3) {
        NSIndexPath *commentIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        cell = [dataSource commentCellForTableView:tableView atIndexPath:commentIndexPath];
    }
    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=2&& indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
        NSIndexPath *commentIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        cell = [dataSource commentCellForTableView:tableView atIndexPath:commentIndexPath];
    }
    else {
        return UserActionCellHeight;
    }
    
    
//    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=2&&rowCount-2!=indexPath.row && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
    
//    if (indexPath.row == PHOTO_CELL_ROW) {
//        return PhotoCellRowHeight;
//    }
//    else if ([shareObjs.likes integerValue]>0 && indexPath.row==1) {
//        if (indexPath.row == LIKES_CELL_ROW && indexPath.row==1) {
//            cell = [dataSource likesCellForTableView:tableView atIndexPath:indexPath];
//        }
//    }
//    else if ([shareObjs.likes integerValue]>0 &&[shareObjs.array_comments_no_description count]>0&& indexPath.row>=2 && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+2) {
//        NSIndexPath *commentIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
//        cell = [dataSource commentCellForTableView:tableView atIndexPath:commentIndexPath];
//    }
//    else if ([shareObjs.likes integerValue]==0 && [shareObjs.array_comments_no_description count]>0 && indexPath.row>=1&&rowCount-1!=indexPath.row && indexPath.row<MIN(MAX_NUMBER_OF_COMMENTS, [shareObjs.array_comments_no_description count])+1) {
//        NSIndexPath *commentIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
//        cell = [dataSource commentCellForTableView:tableView atIndexPath:commentIndexPath];
//    }
//    else {
//        return UserActionCellHeight;
//    }
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    height = [self heightForTableView:tableView cell:cell atIndexPath:indexPath];
    if (height<22) {
        height=22;
    }
    
//    if (shareObjs.description==nil||[shareObjs.description isEqualToString:@""]) {
//        height=0;
//    }
    
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

-(CGFloat)heightForTableView:(UITableView *)tableView cell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.bounds = CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGSize cellSize = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    // Add extra padding
    CGFloat height = cellSize.height + 1;
    return height;
}

-(NSString *)get_time_different:(NSString *)datestring {
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60) {
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24) {
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31) {
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12) {
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else {
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Row Updates
-(void)reloadAtIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView {
    self.insertingRow = YES;
    
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Scroll View
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    id<UIScrollViewDelegate> scrollViewDelegate = (id<UIScrollViewDelegate>)self.controller;
    if ([scrollViewDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [scrollViewDelegate scrollViewDidScroll:scrollView];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    id<UIScrollViewDelegate> scrollViewDelegate = (id<UIScrollViewDelegate>)self.controller;
    if ([scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]) {
        [scrollViewDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

@end
