//
//  STXFeedTableViewDelegate.h
//  STXDynamicTableView
//
//  Created by Jesse Armand on 27/3/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

//@import Foundation;

#import <Foundation/Foundation.h>

@protocol STXFeedPhotoCellDelegate;
@protocol STXLikesCellDelegate;
@protocol STXCaptionCellDelegate;
@protocol STXCommentCellDelegate;
@protocol STXUserActionDelegate;
@protocol STXSectionHeaderViewDelegate;

@interface STXFeedTableViewDelegate : NSObject <UITableViewDelegate,UIScrollViewDelegate>

@property (nonatomic) BOOL insertingRow;
@property (copy, nonatomic) NSMutableArray *posts;
@property (nonatomic, assign) BOOL promotion;

- (instancetype)initWithController:(id<STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate>)controller;

- (void)reloadAtIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView;

@end
