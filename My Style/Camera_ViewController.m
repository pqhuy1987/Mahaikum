//
//  Camera_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Camera_ViewController.h"
#import "TWPhotoPickerController.h"

@interface Camera_ViewController ()

@end

@implementation Camera_ViewController
//@synthesize editorViewController;
@synthesize btn_camera,btn_laibray;
@synthesize photoPickerController,img_photo,share_photo_viewObj,image_effect_viewObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(IBAction)btn_cancle_click:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"BG.png"]];
    
    self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(edit_photo:) name:@"edit_photo" object:nil];
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    //    [self displayEditorForImage:[UIImage imageNamed:@"test.png"]];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_homepage:) name:@"go_to_homepage" object:nil];
    self.image_effect_viewObj=[[aCameraMoreViewController alloc]initWithNibName:@"aCameraMoreViewController" bundle:nil];
}

-(void)go_to_homepage:(NSNotification *)notification {
    self.tabBarController.selectedIndex = 0;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}

-(IBAction)btn_camera_click:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        photoPickerController.allowsEditing = YES;
        [self presentViewController:photoPickerController animated:YES completion:nil];
    }
    //  [self displayEditorForImage:self.img_photo];
}

-(IBAction)btn_libary_click:(id)sender {
    //    photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //    [self presentViewController:photoPickerController animated:YES completion:nil];
    
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    
    photoPicker.cropBlock = ^(UIImage *image) {
        //do something
        self.img_photo=image;
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(gotoEditView) userInfo:self repeats:NO];
        
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"0"]) {
            
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }
    };
    
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    [navCon setNavigationBarHidden:YES];
    
    [self presentViewController:navCon animated:YES completion:NULL];
    
}

-(void)edit_photo:(NSNotification *)notification {
    //   [self gotoEditView];
    self.image_effect_viewObj.workingImage =self.img_photo;
    //[self.navigationController pushViewController:self.image_effect_viewObj animated:YES];
}

-(IBAction)btn_post_click:(id)sender{
    [self postImage];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    self.img_photo=baseImage;
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(gotoEditView) userInfo:self repeats:NO];
    
    if (![[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"0"]) {
        
        UIImageWriteToSavedPhotosAlbum(baseImage, nil, nil, nil);
    }
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"edit_photo" object:nil];
}

-(void)gotoEditView{
    self.image_effect_viewObj.workingImage =self.img_photo;
    [self presentViewController:self.image_effect_viewObj animated:NO completion:nil];
    // [self.navigationController pushViewController:self.image_effect_viewObj animated:YES];
    // [self displayEditorForImage:self.img_photo];
}


#pragma mark Aviary SDK Delegate

//- (void)displayEditorForImage:(UIImage *)imageToEdit
//{
//    //kAFStickers
//
//    NSArray *tools =[NSArray arrayWithObjects:kAFEffects,kAFOrientation,kAFCrop,kAFBrightness,kAFContrast,kAFSaturation,kAFSharpness,kAFDraw,kAFText,kAFRedeye,kAFWhiten,kAFBlemish,nil];
//
//  //      NSArray *tools = [NSArray arrayWithObjects:kAFEnhance, kAFEffects,  kAFOrientation,  kAFBrightness, kAFContrast, kAFSaturation, kAFSharpness,  nil];
// //   NSArray *tools = [NSArray arrayWithObjects:kAFEffects,  kAFOrientation, nil];
//
//    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit options:[NSDictionary dictionaryWithObject:tools forKey:kAFPhotoEditorControllerToolsKey]];
//
//    self.editorViewController = editorController;////////////
//
//    [editorController setDelegate:self];
//
//    AFPhotoEditorStyle *style = [editorController style];
//    UIColor *accentColor = [UIColor colorWithRed:87/255.0 green:165/255.0 blue:232/255.0 alpha:1.0];
//
//    [style setAccentColor:accentColor];
//
//    style.topBarLeftButtonBackgroundColor = accentColor;
//    style.topBarBackgroundColor = accentColor;
//    style.topBarLeftButtonTextColor = [UIColor whiteColor];
//    style.topBarTextColor = [UIColor whiteColor];
//
//    editorViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//    editorViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
//
//    for (id obj in editorController.view.subviews)
//    {
//
//        NSLog(@"%@",NSStringFromClass([obj class]));
//        NSString    *className = NSStringFromClass([obj class]);
//
//        if ([className isEqualToString:@"AFNavigationBar"])
//        {
//            NSLog(@"Find");
//            UINavigationBar *nav = (UINavigationBar*)obj;
//
//            //            nav.hidden = YES;
//            nav.topItem.title = @"My Style";
//
//            //            UIImageView *imgVIew = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,146,32)];
//            //            imgVIew.image = [UIImage imageNamed:@"Nevigation_Logo_5G.png"];
//            //
//            //            nav.topItem.titleView = imgVIew;
//
//        }
//
//        if ([obj isKindOfClass:[UIView class]])
//        {
//            UIView* vc = (UIView*)obj;
//
//            for (id sub_obj in vc.subviews)
//            {
//                if ([sub_obj isKindOfClass:[UIImageView class]])
//                {
//                    UIImageView *imgView = (UIImageView*)sub_obj;
//                    imgView.image = [UIImage imageNamed:@""];
//                }
//
//                //                if ([sub_obj isKindOfClass:[UIScrollView class]])
//                //                {
//                //                    UIScrollView *scrView = (UIScrollView*)sub_obj;
//                //
//                //                    scrView.contentSize = CGSizeMake(640,scrView.contentSize.height);
//                //                }
//            }
//
//        }
//    }
//
//    [self presentViewController:editorController animated:NO completion:nil];
//}

//-(void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image {
//    // result image
//    [editor dismissViewControllerAnimated:NO completion:^{
//        dispatch_async( dispatch_get_main_queue(),^{
//                           share_photo_viewObj.img_photo =image;
//                          UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:share_photo_viewObj];
//                           [self presentViewController:nav animated:YES completion:nil];
//
//                       });
//    }];
//
//}
//
//-(void)photoEditorCanceled:(AFPhotoEditorController *)editor {
//    /*
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//
//                       });
//    }];
//     */
//}


#pragma mark Post Image To server
-(void)postImage {
    //   http://www.techintegrity.in/mystyle/post_images.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&lat=12.2222&lng=12.454545&location=kkvhall&uid=2&description=test&image=sarju.png
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    UIImage *image=[UIImage imageNamed:@"test2.png"];
    NSData *img_data  = UIImageJPEGRepresentation(image,1);
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14202" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postImagesAPIResponce:) name:@"14202" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14202" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostImagesAPIResponce:) name:@"-14202" object:nil];
    
    
    //    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&user_id=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_images.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    //[request setData:img_data withFileName:@"1.png" andContentType:@"image/png" forKey:@"image"];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",
                            @"test", @"description",nil];
    //NSLog(@"params:%@",params);
    //[networkQueue uploadImageAndData:urlString :createStoryAPI :params :self.cameraImageData];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue uploadImageAndData:requestStr :@"14202" :params :img_data];
}

-(void)postImagesAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14202" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14202" object:nil];
    
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-4"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"A user with that email already exists." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"-3"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"A user with that username already exists." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"Success");
        
    }
}

-(void)FailpostImagesAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14202" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14202" object:nil];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
