//
//  Favourite_feed_following_collection_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collectionview_delegate.h"
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"


static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";

@interface Favourite_feed_following_collection_cell : UITableViewCell {
    UIImageView *btn_user_img;
    UILabel *lbl_time;
    LORichTextLabel *lbl_dec;
    UIImageView *imgBottomImage;
}

@property(nonatomic,strong) LORichTextLabel *lbl_dec;
@property(nonatomic,strong)IBOutlet UIImageView *btn_user_img;
@property(nonatomic,strong)IBOutlet UILabel *lbl_time;
@property(nonatomic,strong)IBOutlet UIImageView *imgBottomImage;

-(void)draw_collectionview_in_cell;

@property(nonatomic,strong)IBOutlet Collectionview_delegate *collectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

@end
