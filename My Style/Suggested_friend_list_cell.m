//
//  Suggested_friend_list_cell.m
//  My Style
//
//  Created by Tis Macmini on 6/21/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Suggested_friend_list_cell.h"

@implementation Suggested_friend_list_cell
@synthesize btn_user_img,lbl_name,lbl_username,lbl_dec,btn_follow;

-(void)draw_collectionview_in_cell{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    layout.minimumLineSpacing = 0.1f;
    layout.minimumInteritemSpacing =0.1f;
    layout.itemSize = CGSizeMake(50,50);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[Collectionview_delegate alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier1];
    self.collectionView.backgroundColor = [UIColor clearColor];
    //  self.collectionView.showsHorizontalScrollIndicator = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    [self.contentView addSubview:self.collectionView];
}
/*
 -(void)layoutSubviews
 {
 [super layoutSubviews];
 
 self.collectionView.frame = CGRectMake(0, 50, 320, 100);
 }
 */
-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.index = index;
    
    [self.collectionView reloadData];
}


@end
