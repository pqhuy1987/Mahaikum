//
//  News_feed_like_Share.h
//  My Style
//
//  Created by Tis Macmini on 6/10/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Home_tableview_data_share.h"
@interface News_feed_like_Share : NSObject{

    NSString *feed;
    NSString *userimage;
    NSString *name;
    NSString *username;
    
    NSString *user_id;
    NSString *owner_id;
    NSString *image;
    Home_tableview_data_share *image_data;
    NSString *status_date;
    NSString *read_status;
    NSString *userAction;
}
@property(nonatomic,strong) NSString *status_date;
@property(nonatomic,strong) NSString *feed;
@property(nonatomic,strong) NSString *userimage;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) NSString *owner_id;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) Home_tableview_data_share *image_data;

@property(nonatomic,strong) NSString *read_status;
@property(nonatomic,strong) NSString *entity_id;
@property(nonatomic,strong) NSString *userAction;

@end
