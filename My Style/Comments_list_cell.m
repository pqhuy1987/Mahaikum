
#import "Comments_list_cell.h"



@implementation Comments_list_cell

@synthesize img_user,btn_user_name,lbl_desc;
@synthesize lbl_date,view_line,img_user1,img_back;


-(void)draw_in_cell {
    //self.lbl_desc.lineBreakMode = NSLineBreakByCharWrapping;
    //self.lbl_desc.numberOfLines = 0;
    //self.lbl_desc.userInteractionEnabled=YES;
    self.lbl_desc=[[UITextView alloc] initWithFrame:CGRectMake(58,30, 255,24)];
    self.lbl_desc.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
   	[self.lbl_desc setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
    self.lbl_desc.textColor=[UIColor blackColor];
    [self.lbl_desc setBackgroundColor:[UIColor clearColor]];
    self.lbl_desc.tag=100;
    self.lbl_desc.userInteractionEnabled = YES;
    [self.lbl_desc positionAtX:55.0 andY:30.0];
    self.lbl_desc.delegate = self;
    self.lbl_desc.editable = NO;
    self.lbl_desc.selectable = YES;
    self.lbl_desc.scrollEnabled=NO;
    self.lbl_desc.dataDetectorTypes = UIDataDetectorTypeLink;
    [self.contentView addSubview:self.lbl_desc];

}
- (void)didClickComment_listWithGesture:(UITapGestureRecognizer *)tapGesture {
     [self.customdelegate didSlectComment:self];
    
} 

@end
