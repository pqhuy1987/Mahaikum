//
//  Image_detail_url.m
//  My Style
//
//  Created by Tis Macmini on 6/19/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Image_detail_url.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"

@interface Image_detail_url () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate>{
    Hash_tag_ViewController *hash_tag_viewObj;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;

}

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation Image_detail_url

@synthesize image_id;
@synthesize shareObj,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view;
@synthesize tbl_news;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.img_like_heart setHidden:YES];
    
    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
    self.tbl_news.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tbl_news.delegate = delegate;
    self.tableViewDelegate = delegate;
    
    self.shareObj =[[Home_tableview_data_share alloc]init];
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"400" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-400" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"401" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-401" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"402" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-402" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"403" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-403" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"404" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-404" object:nil];
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    [self.tbl_news setContentOffset:CGPointMake(0, 0)];

    if (self.shareObj.image_id.length != 0) {
        NSLog(@"not 0 :%@",shareObj.image_id);
    }else{
        NSLog(@"%@",shareObj.image_id);
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_image_detail];
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObjs=[arrayFeedArray objectAtIndex:currentSelectedIndex];
    which_image_liked = userActionCell.tag;
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}

-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}
-(void)userWillComment:(STXUserActionCell *)userActionCell {
    self.comments_list_viewObj.image_id=self.shareObj.image_id;
    self.comments_list_viewObj.array_comments=self.shareObj.array_comments_tmp;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}
-(void)userWillShare:(STXUserActionCell *)userActionCell {
    
    NSInteger tag =userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    if ([self.shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = self.shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];

        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = self.shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =self.shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        //report_id : 1= i dont like this photo,  2=this photo is spam or a scam,  3=this photo puts people at risk,  4=this photo shouldn't be on mystyle
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = self.shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =self.shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
            //  [controller release];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag =likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked = (int)tag;
    //Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
   // pushPopFlg=1;
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
   // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}


- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
   // pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(IBAction)btnProfileClicked:(id)sender {
    user_info_view.user_id=shareObj.uid; //obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld", (long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    NSInteger selectedIndex = posterLike.tag;
    if (selectedIndex != currentSelectedIndex)
        selectedIndex = currentSelectedIndex;
    
    Home_tableview_data_share *shareObjs=[arrayFeedArray objectAtIndex:selectedIndex];
    which_image_liked=posterLike.tag;
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    
}

#pragma mark - Get Image Detail
-(void)get_image_detail{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14218" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getImageDetailResponce:) name:@"14218" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14218" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetImageDetailResponce:) name:@"-14218" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_image_detail.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            self.image_id,@"id",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14218" :params];
}

-(void)getImageDetailResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14218" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14218" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        for (NSDictionary *dict in [result valueForKey:@"data"]){
            self.shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            
            self.shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            self.shareObj.image_id =[dict valueForKey:@"id"];
            self.shareObj.image_owner=[dict valueForKey:@"image_owner"];
            self.shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            self.shareObj.lat =[dict valueForKey:@"lat"];
            self.shareObj.lng=[dict valueForKey:@"lng"];
            self.shareObj.likes=[dict valueForKey:@"likes"];
            self.shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
            self.shareObj.uid=[dict valueForKey:@"uid"];
            self.shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            
            self.shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            self.shareObj.category_id = [dict valueForKey:@"category_id"];
            
            self.shareObj.lat=[dict valueForKey:@"lat"];
            self.shareObj.lng=[dict valueForKey:@"lng"];
            
            self.shareObj.liked=[dict valueForKey:@"user_liked"];
            self.shareObj.comment_count =[dict valueForKey:@"total_comment"];
            
            self.shareObj.price = dict[@"price"];
            self.shareObj.currency = dict[@"currency"];
            self.shareObj.sold = [dict[@"sold"] integerValue];
            
            self.shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [self.shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            shareObj.array_comments_tmp=[[NSMutableArray alloc]init];
            
            if (![[dict valueForKey:@"description"] isEqualToString:@""])
            {
                Comment_share *obj =[[Comment_share alloc]init];
                obj.uid =shareObj.uid;
                obj.username =shareObj.username;
                obj.name =shareObj.name;
                obj.image_url=shareObj.user_image;
                obj.comment_desc=shareObj.description;
                obj.datecreated=shareObj.datecreated;
                [shareObj.array_comments_tmp addObject:obj];
            }
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    
                    [shareObj.array_comments addObject:obj];
                    [shareObj.array_comments_tmp addObject:obj];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            self.shareObj.rating=[dict valueForKey:@"rating"];
            self.shareObj.my_rating=[dict valueForKey:@"my_rating"];
            self.shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            self.shareObj.totalUser=[dict valueForKey:@"tot_user"];
            
        }
    
        NSMutableArray *tempArray=[[NSMutableArray alloc] init];
        [tempArray addObject:self.shareObj];
        
        self.tableViewDataSource.posts = tempArray;// copy];
        self.tableViewDelegate.posts = tempArray;// copy];
        
        [self.tbl_news setContentOffset:CGPointMake(0, 0)];
        [self.tbl_news reloadData];
    }
}

-(void)FailgetImageDetailResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14218" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14218" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.shareObj.image_id.length != 0) {
       return 1;
    }
	return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 41.0f;//60
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"strip.png"]];
    strip.alpha = 1.0;
    strip.frame =CGRectMake(0,0,kViewWidth,41);
    
    UIImageView *userimg_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_photobg.png"]];
    userimg_bg.alpha = 1.0;
    userimg_bg.frame =CGRectMake(5,1,40,40);
    
    UIImageView *img_user =[[UIImageView alloc]initWithFrame:CGRectMake(9, 5, 32, 32)];
    [img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    img_user.layer.cornerRadius = 16.0;
    img_user.layer.masksToBounds=YES;
    
    EGOImageButton *img_userimage=[[EGOImageButton alloc]initWithFrame:CGRectMake(5,1, 40, 40)];
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    img_userimage.layer.cornerRadius = 20.0f;
    img_userimage.layer.masksToBounds=YES;
    
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:[StaticClass urlDecode:self.shareObj.username] forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(50,0,150,21)];
    [btn_username setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"category.png"]];
    img_locationicon.frame = CGRectMake(50,27,8,11);
    
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:self.shareObj.location forState:UIControlStateNormal];
    [btn_location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_location.titleLabel.font = [UIFont systemFontOfSize:15];
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon.png"]];
    [img_clock setFrame:CGRectMake(240,10,62,21)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:shareObj.datecreated];
    lbl_time.font = [UIFont boldSystemFontOfSize:15];
    lbl_time.textColor =[UIColor grayColor];
    lbl_time.frame =CGRectMake(265,14,40,16);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =1.0;
    [view addSubview:strip];
    [view addSubview:userimg_bg];
    [view addSubview:img_user];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    [view addSubview:lbl_time];
    
    if (self.shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(50,2,150,20)];
        [btn_location setFrame:CGRectMake(63,23,175,20)];
        img_locationicon.hidden=NO;
        strip.frame =CGRectMake(0,0,kViewWidth,46);//320,70
        
    }else{
        img_locationicon.hidden=YES;
        strip.frame =CGRectMake(0, 0,kViewWidth,46);
        //[btn_username setFrame:CGRectMake(55, 0, 150, 40)];
        [btn_username setFrame:CGRectMake(50,0,150,41)];
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [Home_tableview_cell get_tableview_hight:self.shareObj];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Home_tableview_cell";
	Home_tableview_cell *cell = (Home_tableview_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor whiteColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
         
        [cell.img_big addGestureRecognizer:tapImage];
        
        //
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"www."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"WWW."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Www."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"https://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Https://"];
        
//        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
//        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"www."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"WWW."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Www."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"https://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Https://"];
        
//        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
//        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
	}
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    [cell redraw_cell:self.shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    
    cell.dlstarObj.delegate = self;
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Method for Stop Header scrolling in UITableview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    
    
    if ([self.shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            
        }];
        
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            NSLog(@"");
            self.share_photo_view.str_img_url = self.shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            NSLog(@"");
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =self.shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            NSLog(@"");
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
                NSLog(@"%@",htmlStr);
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        NSLog(@"");
        self.share_photo_view.str_img_url = self.shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        NSLog(@"");
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =self.shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        NSLog(@"");
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
            NSLog(@"%@",htmlStr);
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}

-(void)report_inappropriate:(int )index {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this post if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark - List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    self.liker_list_viewObj.image_id =self.shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark - Go to Profile

-(IBAction)btn_profile_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld", (long)tag);
    
    user_info_view.user_id=self.shareObj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark - UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:(int)buttonIndex];
        
    }
}

#pragma mark - Delete Photo
-(void)delete_photo{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,self.shareObj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);

    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"402":nil];
}

-(void)getdeleteResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        //  [self get_news_feed];
    }
}

#pragma mark - Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"403":nil];
    self.shareObj.my_rating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
}

-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld",(long)[[dataDict objectForKey:@"rate"] integerValue]];
        [self.tbl_news reloadData];
    }
    
}

#pragma mark - like unlike call
-(IBAction)btn_like_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_liked = (int)tag;
    
    if (![self.shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //[SVProgressHUD show];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr;
    if (self.promotion == NO)
        requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    else
        requestStr = [NSString stringWithFormat:@"%@promotion_post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"401":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"%@", self.shareObj.liked);
        //        if ([self.shareObj.liked isEqualToString:@"no"]) {
        //            self.shareObj.liked=@"yes";
        //            int count =[self.shareObj.likes intValue];
        //            count++;
        //            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        //
        //
        //
        //            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        //
        //        }else{
        //
        //            self.shareObj.liked=@"no";
        //            int count =[self.shareObj.likes intValue];
        //            count--;
        //            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        //
        //
        //            if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
        //                [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        //            }
        //        }
        //
        //        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        //        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    // NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"404":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        if ([self.shareObj.liked isEqualToString:@"no"]) {
            self.shareObj.liked=@"yes";
            int count =[self.shareObj.likes intValue];
            count++;
            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            
            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            NSLog(@"%@",self.shareObj.array_liked_by);
            NSLog(@"%@",self.shareObj.likes);
            
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
            
        }
        
    }
    
}

#pragma mark - Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked = (int)tappedRow.section;
    
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"like"];
    }
    
    
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
    
    
    
}


#pragma mark - Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}
- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mark - Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    self.comments_list_viewObj.image_id=self.shareObj.image_id;
    self.comments_list_viewObj.array_comments=self.shareObj.array_comments_tmp;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)FailNewsReson:(NSNotification *)notification {
	//[SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
