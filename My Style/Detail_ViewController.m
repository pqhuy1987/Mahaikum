//
//  Detail_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Detail_ViewController.h"

@interface Detail_ViewController ()

@end

@implementation Detail_ViewController
@synthesize title1,desc,date,complete_flag,tbl_detail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - UITableview Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
       
    UILabel *lbl_header=[[UILabel alloc]initWithFrame:CGRectMake(30,5, 290,25)];
    lbl_header.text =@"Title";
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320,30)];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =0.9;
    [view addSubview:lbl_header];
    
    return view;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        CGSize size = [self.title sizeWithFont:[UIFont systemFontOfSize:17]
                           constrainedToSize:CGSizeMake(300, HUGE_VALL)
                               lineBreakMode:NSLineBreakByWordWrapping];
        if (size.height<30) {
            return 44;
        }else{
        
            return size.height+14.0f+14.0f;
        }
        
    }
    if (indexPath.section==1) {
        CGSize size = [self.desc sizeWithFont:[UIFont systemFontOfSize:17]
                             constrainedToSize:CGSizeMake(300, HUGE_VALL)
                                 lineBreakMode:NSLineBreakByWordWrapping];
        if (size.height<30) {
            return 44;
        }else{
            
            return size.height+14.0f+14.0f;
        }
    }
    
    return 44.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==0 || indexPath.section ==1 || indexPath.section ==2) {
        
    
    static NSString *identifier =@"Detail_view_cell";
    Detail_view_cell *cell=(Detail_view_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Detail_view_cell" owner:self options:nil];
        cell =[nib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.showsReorderControl = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lbl_border_line.layer.masksToBounds = YES;
        cell.lbl_border_line.layer.cornerRadius = 5.0;
        cell.lbl_border_line.layer.borderColor = [UIColor colorWithRed:(1.0*81.0)/255.0 green:(1.0*169.0)/255.0 blue:(1.0*222.0)/255.0 alpha:1].CGColor;
        cell.lbl_border_line.layer.borderWidth = 2.0;
    }
    
    if (indexPath.section==0) {
        CGSize size = [self.title sizeWithFont:[UIFont systemFontOfSize:17]
                             constrainedToSize:CGSizeMake(300, HUGE_VALL)
                                 lineBreakMode:NSLineBreakByWordWrapping];
        if (size.height<30) {
            cell.lbl_title.frame = CGRectMake(10, 7, 300, 30);
        }else{
            cell.lbl_title.frame = CGRectMake(10, 7, 300,size.height+14.0f);
        }
    cell.lbl_title.text =self.title1;
    }
    if (indexPath.section==1) {
        CGSize size = [self.desc sizeWithFont:[UIFont systemFontOfSize:17]
                             constrainedToSize:CGSizeMake(300, HUGE_VALL)
                                 lineBreakMode:NSLineBreakByWordWrapping];
        if (size.height<30) {
            cell.lbl_title.frame = CGRectMake(10, 7, 300, 30);
        }else{
            cell.lbl_title.frame = CGRectMake(10, 7, 300,size.height+14.0f);
        }
        cell.lbl_title.text =self.desc;
    }
    cell.lbl_border_line.frame =CGRectMake(7, 5, 305, cell.lbl_title.frame.size.height+4);
    cell.lbl_title.backgroundColor = [UIColor redColor];
    
        return cell;
    }else{
        static NSString *identifier =@"Detail_view_complete_cell";
        Detail_view_complete_cell *cell=(Detail_view_complete_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Detail_view_complete_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.lbl_border_line.layer.masksToBounds = YES;
            cell.lbl_border_line.layer.cornerRadius = 5.0;
            cell.lbl_border_line.layer.borderColor = [UIColor colorWithRed:(1.0*81.0)/255.0 green:(1.0*169.0)/255.0 blue:(1.0*222.0)/255.0 alpha:1].CGColor;
            cell.lbl_border_line.layer.borderWidth = 2.0;
        }
        return cell;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
