//
//  News_feed_like_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "EGOImageButton.h"

@interface News_feed_like_cell : UITableViewCell{
    LORichTextLabel *lbl_dec;
    UIImageView *btn_user_img;
    UIImageView *btn_img;
    
    UILabel *lbl_time;
    UIButton *btn_user_img1;
    UIButton *btn_img1;
    UIImageView *imgRateImage;
    UIImageView *imgBottomLine;
}
@property(nonatomic,strong) LORichTextLabel *lbl_dec;
@property(nonatomic,strong)IBOutlet UIImageView *btn_user_img;
@property(nonatomic,strong)IBOutlet UIImageView *btn_img;
@property(nonatomic,strong)IBOutlet UILabel *lbl_time;
@property(nonatomic,strong)IBOutlet UIButton *btn_user_img1;
@property(nonatomic,strong)IBOutlet UIButton *btn_img1;
@property(nonatomic,strong)IBOutlet UIImageView *imgRateImage;
@property(nonatomic,strong)IBOutlet UIImageView *imgBottomLine;

-(void)draw_dec_lbl;
@end
