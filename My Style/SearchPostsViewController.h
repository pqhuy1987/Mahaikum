
#import <UIKit/UIKit.h>
#import "image_collection_cell.h"
#import "Image_detail.h"
#import "Comments_list_ViewController.h"

@protocol SearchPostsViewControllerDelegate;

@interface SearchPostsViewController : UIViewController

@property int getFeedKind;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *viewTitle;
@property(nonatomic,strong) Comments_list_ViewController *comments_list_viewObj;

@property (nonatomic, strong) NSString *viewTitleString;
@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic,strong) Image_detail *image_detail_viewObj;

@property (strong, nonatomic) IBOutlet UIView *bodyView;
@property (nonatomic, strong) id<SearchPostsViewControllerDelegate> delegate;

@end

@protocol SearchPostsViewControllerDelegate <NSObject>

- (void)backedFromView:(SearchPostsViewController *)viewController;

@end
