//
//  Facebook_follow_frnd_list.m
//  My Style
//
//  Created by Tis Macmini on 5/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Facebook_follow_frnd_list.h"
#import "AppDelegate.h"

@interface Facebook_follow_frnd_list ()

@end

@implementation Facebook_follow_frnd_list
@synthesize array_friend,tbl_friend,suggested_viewObj;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)btn_skip_click:(id)sender{
    [self.navigationController pushViewController:self.suggested_viewObj animated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.suggested_viewObj =[[Suggested_friend_list_reg alloc]initWithNibName:@"Suggested_friend_list_reg" bundle:nil];

    // Do any additional setup after loading the view from its nib.
    self.array_friend =[[NSMutableArray alloc]init];
    
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    if (!appDelegate.session.isOpen) {
//        // appDelegate.session = [[FBSession alloc] init];
//        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
//        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
//            
//            
//            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
//                                                             FBSessionState status,
//                                                             NSError *error) {
//            }];
//        }
//    }
    
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    //[self start_activity];
    
/*    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self get_friend_list];
    }];*/
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if ( self.loadFriends )
    {
        self.loadFriends = NO;
        [self start_activity];
        
        if ( [FBSDKAccessToken currentAccessToken] == nil )
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login logOut];
            [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
             {
                 if (error) {
                     NSLog(@"Process error");
                 } else if (result.isCancelled) {
                     NSLog(@"Cancelled");
                 } else {
                     if ([result.grantedPermissions containsObject:@"email"])
                     {
                         [self get_friend_list];
                     }
                 }
             }];
        }
        else
        {
            [self get_friend_list];
        }
    }
}

-(void)get_friend_list{
    
 /*
    NSString *query=@"SELECT uid,name,first_name,last_name,pic_square FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(reloadTable) withObject:self afterDelay:0.3];
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
 */
    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"id, name, picture, devices" forKey:@"fields"];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends" parameters:parameters]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         NSLog(@"facebook friends");
         
         [array_friend removeAllObjects];
         
         NSArray *friends = [result objectForKey:@"data"];
         NSLog(@"Found %d facebook friends", friends.count);
         for ( NSDictionary *friend in friends )
         {
             NSLog(@"Friend %@ with id %@", friend[@"name"], friend[@"id"]);
             [array_friend addObject:friend[@"id"]];
         }
         
         [self reloadTable];
     }];

    
}

- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    
    
    if (error) {
        
        
        NSLog(@"%@", error.localizedDescription);
    } else {
        FBGraphObject *dictionary = (FBGraphObject *)result;
        // NSString* userId = (NSString *)[dictionary objectForKey:@"id"];
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            
            [self.array_friend removeAllObjects];
            
            for (id result in tempResult) {
                [self.array_friend addObject:[result objectForKey:@"uid"]];
            }
            
        });
    }
}

-(void)reloadTable{
    
    NSLog(@"%@",self.array_friend);
    //  http://www.techintegrity.in/mystyle/get_facebook_users.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&fbId=array
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14212" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFacebookUsersResponce:) name:@"14212" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14212" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetFacebookUsersResponce:) name:@"-14212" object:nil];
    
    NSMutableString *fb_list=[[NSMutableString alloc] init];
    
    [fb_list appendString:[self.array_friend componentsJoinedByString:@","]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_facebook_users.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [NSString stringWithFormat:@"%@",fb_list],@"fbId",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14212" :params];
}

-(void)getFacebookUsersResponce:(NSNotification *)notification {
    //  [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14212" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14212" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    [self.array_friend removeAllObjects];
    
    for(NSString *dict in result){
        //   NSLog(@"%@",[dict valueForKey:@"email"]);
        Faceabook_find_friend_Share *shareObj =[[Faceabook_find_friend_Share alloc]init];
        shareObj.email =[dict valueForKey:@"email"];
        shareObj.facebook_id=[dict valueForKey:@"facebook_id"];
        shareObj.user_id=[dict valueForKey:@"id"];
        shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
        shareObj.name=[dict valueForKey:@"name"];
        shareObj.url_image=[dict valueForKey:@"image"];
        shareObj.is_following=@"no";
        [self.array_friend addObject:shareObj];
        
    }
    self.tbl_friend.hidden =NO;
    
    [self.tbl_friend reloadData];
    [self stop_activity];
    
}

-(void)FailgetFacebookUsersResponce:(NSNotification *)notification {
    //  [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14212" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14212" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return 60.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_friend.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Faceabook_find_friend_cell";
	Faceabook_find_friend_cell *cell = (Faceabook_find_friend_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Faceabook_find_friend_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.btn_follow addTarget:self action:@selector(btn_follow_click:) forControlEvents:UIControlEventTouchUpInside];
	}
    
    Faceabook_find_friend_Share *shareObj =[self.array_friend objectAtIndex:indexPath.row];
    cell.lbl_name.text = shareObj.name;
    cell.lbl_username.text =shareObj.username;
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.url_image] placeholderImage:nil];
//    cell.img_user.imageURL =[NSURL URLWithString:shareObj.url_image];
    cell.btn_follow.tag =indexPath.row;
    if ([shareObj.is_following isEqualToString:@"no"]) {
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
    }else{
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
    }
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark Follow Click
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    
    
    NSInteger tag =btn.tag;
    
    Faceabook_find_friend_Share *shareObj =[self.array_friend objectAtIndex:tag];
    //post user follow
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    if ([shareObj.is_following isEqualToString:@"no"]) {
        shareObj.is_following =@"yes";
        url=@"post_user_following.php";
    }else{
        shareObj.is_following =@"no";
        url=@"post_user_unfollow.php";
    }

    [self.array_friend replaceObjectAtIndex:tag withObject:shareObj];
    [self.tbl_friend reloadData];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14247" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingUnFollowingAPIResponce:) name:@"14247" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14247" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingUnFollowingAPIResponce:) name:@"-14247" object:nil];
    
    
    NSString *requestStr = [NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            shareObj.user_id,@"following_id",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14247" :params];
}

-(void)postUserFollowingUnFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14247" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14247" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    NSLog(@"result:%@",result);
    //akshay
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }
}

-(void)FailpostUserFollowingUnFollowingAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14247" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14247" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];

}

#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}
-(void)stop_activity{
    [DejalBezelActivityView removeView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
