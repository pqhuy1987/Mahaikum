//
//  Share_photo_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Share_photo_ViewController.h"
//#import "AFPhotoEditorControllerOptions.h"
//#import "AFPhotoEditorController.h"
#import "UIImage+Resize.h"
#import "NSString+AVTagAdditions.h"

#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "CustomCamera.h"
#import "DBCameraGridView.h"

NSInteger btnswitchsharephotoflg = 0;

@interface Share_photo_ViewController () <DBCameraViewControllerDelegate>

@end

@implementation Share_photo_ViewController
@synthesize img_photo,scrollview,img_bg_firstcell;
@synthesize img_show_photo,view_bg_show_photo,txt_desc,img_bg_secondcell,btnswitch,imgswitchon,imgswitchoff,lblSpinnerBg;

@synthesize btn_location,lbl_optional,view_second_cell;
@synthesize view_third_cell,img_bg_third_cell;
@synthesize show_location_listViewObj,switch_map,lbladdtomap,lblheader,lblshareto,lblsubheader1,lblsubheader2,activity;

@synthesize tblSearch,searchArray,tagsArray;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    [self dismissViewControllerAnimated:YES completion:nil];
   
/*
    [self dismissViewControllerAnimated:NO completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view_from_share" object:self];
    }];
 */
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    searchArray=[[NSMutableArray alloc] init];
    tagsArray=[[NSMutableArray alloc] init];
    txt_desc.hashTagsDelegate = self;
    txt_desc.hashTagsTableViewHeight = 120.0f;
    
    
    self.s3 = [[[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] autorelease];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:16.0];
    lblsubheader1.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    lblsubheader2.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    lbladdtomap.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    lblshareto.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    btn_location.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    txt_desc.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.show_location_listViewObj=[[Show_location_list alloc]initWithNibName:@"Show_location_list" bundle:nil];
    
    self.img_bg_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell.layer.cornerRadius =4.0f;
    
//    self.img_bg_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
//    self.img_bg_secondcell.layer.cornerRadius =4.0f;
    
//    self.img_bg_third_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
//    self.img_bg_third_cell.layer.cornerRadius =4.0f;
//    self.view_bg_show_photo.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
  //  self.view_bg_show_photo.layer.shadowOffset = CGSizeMake(0,0.1);
  //  self.view_bg_show_photo.layer.shadowColor = [UIColor whiteColor].CGColor;
 //   self.view_bg_show_photo.layer.shadowOpacity = 0.4;
    
    [switch_map setOn:YES];
    switch_map.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
   // [[UIApplication sharedApplication]setStatusBarHidden:YES];
    
    self.img_show_photo.layer.cornerRadius = 42.0;
    self.img_show_photo.layer.masksToBounds=YES;
    
    self.img_show_photo.image=self.img_photo;
    
    if (is_iPhone_5) {
        self.scrollview.contentSize=CGSizeMake(320,500);
    }
    else{
        self.scrollview.contentSize=CGSizeMake(320,500);
        self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.scrollview.frame = CGRectMake(0,71, 320, 498);
    }
    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }
    else {
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
    
    
    NSMutableArray *tempFollowing=[[Singleton sharedSingleton] getCurrentDictFollowing];
    
    [tagsArray removeAllObjects];
    for (int i=0;i<[tempFollowing count];i++) {
        NSString *strName=[StaticClass urlDecode:[tempFollowing objectAtIndex:i]];
        [tagsArray addObject:strName];
    }
}

- (NSArray *)tagsForQuery:(NSString *)query{
    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", query];
    NSArray *array =[tagsArray filteredArrayUsingPredicate:bPredicate];
    
    [searchArray removeAllObjects];
    for ( int i=0;i<[array count];i++) {
        [searchArray addObject:[array objectAtIndex:i]];
    }
    
    if ([searchArray count]==0) {
        tblSearch.hidden=YES;
    }
    else {
        tblSearch.hidden=NO;
    }
    
    [tblSearch reloadData];
    return array;
}

- (BOOL)textView:(UITextView *)textViewsss shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [txt_desc resignFirstResponder];
        //list all the contained tags:
        NSLog(@"Entered tags: %@", [txt_desc.hashTags componentsJoinedByString:@" "]);
    }
    return YES;
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 41.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    strip.frame =CGRectMake(0,0,320,41);
        
    UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,0,320,41)];
    lbl.textColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    lbl.backgroundColor=[UIColor clearColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.text=@"Suggestion";
        
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,41)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    [view addSubview:strip];
    [view addSubview:lbl];
    return view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
        
    NSString *tag = [searchArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"@%@", tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:14.0f];
    cell.backgroundColor=[UIColor whiteColor];
    cell.textLabel.textColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==tblSearch) {
        NSString *str=[searchArray objectAtIndex:indexPath.row];
        
        NSString *replaceString = [NSString stringWithFormat:@"@%@ ", str];
        NSMutableString *mutableText = [NSMutableString stringWithString:txt_desc.text];
        
        [[NSString endOfStringHashtagRegex] replaceMatchesInString:mutableText options:0 range:[txt_desc.text wholeStringRange] withTemplate:replaceString];
        txt_desc.text = mutableText;
        tblSearch.hidden=YES;
    }
}

-(IBAction)btn_location_list_click:(id)sender {
    [self presentViewController:self.show_location_listViewObj animated:YES completion:^{
        
    }];
    
//    [self.navigationController pushViewController:self.show_location_listViewObj animated:YES];
}

-(IBAction)editBtnClick :(id)sender {
//    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:self.img_photo];
//    [editorController setDelegate:self];
//    [self presentViewController:editorController animated:YES completion:nil];
    
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
}

- (void) dismissCamera:(id)cameraViewController{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
    [cameraViewController restoreFullScreenMode];
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    [[Singleton sharedSingleton] setCurrentCapturedImage:image];
    self.img_photo=image;
    self.img_show_photo.image=image;
    [self.img_show_photo setImage:self.img_photo];
    
    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//-(void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image {
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                           self.img_photo = image;
//                           self.img_show_photo.image=self.img_photo;
//                           
//                       });
//    }];
//    
//}
//
//-(void)photoEditorCanceled:(AFPhotoEditorController *)editor {
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                       });
//    }];
//}


#pragma mark - ToggleViewDelegate
-(IBAction)btnswitchClick:(id)sender{
    if (btnswitchsharephotoflg == 0) {
        
        btnswitchsharephotoflg = 1;
        imgswitchoff.hidden = YES;
        imgswitchon.hidden = NO;
        
        [self performSelector:@selector(selectRightButton) withObject:self afterDelay:0.1];
    }
    else{
        btnswitchsharephotoflg = 0;
        imgswitchoff.hidden = NO;
        imgswitchon.hidden = YES;
        
        [self performSelector:@selector(selectLeftButton) withObject:self afterDelay:0.1];
    }
}

-(IBAction)btn_switch_map_click:(id)sender {
    if (!switch_map.isOn) {
        [self performSelector:@selector(selectLeftButton) withObject:self afterDelay:0.1];
    }else{
        [self performSelector:@selector(selectRightButton) withObject:self afterDelay:0.1];
    }
}

-(void)selectLeftButton {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(5,125+25,310,52);
    self.view_second_cell.frame = CGRectMake(5,177+25,310,0);
    self.view_third_cell.frame =CGRectMake(5, 235-52+25, 310, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=YES;
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
}

-(void)selectRightButton {
    self.view_second_cell.layer.masksToBounds = YES;
    
    self.view_second_cell.frame = CGRectMake(5,177+25,310,0);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(5,125+25,310,104);
    self.view_second_cell.frame = CGRectMake(5,177+25,310,51);
    self.view_third_cell.frame =CGRectMake(5, 235+25, 310, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=NO;
    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }else{
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
    
    
}

#pragma mark UIScrollview Delegate 
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
   [self.txt_desc resignFirstResponder];
}

#pragma mark UITextview Delegate

//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    return TRUE;
//}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
        self.txt_desc.text=@"";
    }
    
    return TRUE;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if (self.txt_desc.text.length==0) {
        self.txt_desc.text=@"Write a caption...";
    }
}

-(IBAction)btn_share_click:(id)sender {
    [self postImage];
}

#pragma mark Post Image To server
-(void)postImage {
 //      http://www.techintegrity.in/mystyle/post_images.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&lat=12.2222&lng=12.454545&location=kkvhall&uid=2&description=test&image=sarju.png
    
    
    
//    if(self.img_photo.size.height>2048&&self.img_photo.size.width>1536) {
//        UIImage *newImage = [self.img_photo resizedImage:CGSizeMake(1536,2048) interpolationQuality:3];
//    }
//    else if(self.img_photo.size.height>2048&&self.img_photo.size.width<=1536) {
//        UIImage *newImage = [self.img_photo resizedImage:CGSizeMake(self.img_photo.size.width,2048) interpolationQuality:3];
//    }
//    else if(self.img_photo.size.height<=2048&&self.img_photo.size.width>1536) {
//        
//    }
//    else {
//        
//    }
    
    NSLog(@"s3 Start Time:%@",[NSDate date]);
    UIImage *newImage = [self.img_photo resizedImage:CGSizeMake(640,640) interpolationQuality:3];
    NSData *img_data = UIImageJPEGRepresentation(newImage,1);

/*
    CGSize destinationSize;
    destinationSize = CGSizeMake(640,640);

    UIGraphicsBeginImageContext(destinationSize);
    [self.img_photo drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *img_data = UIImageJPEGRepresentation(newImage,1);
    */
    
    //NSData *img_data  = UIImageJPEGRepresentation(self.img_photo,1);
    [self startSpinner];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSDate *dt=[NSDate date];
        //double timePassed_ms = [dt timeIntervalSinceNow];
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *dateString1 = [DateFormatter stringFromDate:dt];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        __block NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]] autorelease];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                [self stopSpinner];
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else {
                NSLog(@"s3 End Time:%@",[NSDate date]);
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postImagesAPIResponce:) name:@"14234" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostImagesAPIResponce:) name:@"-14234" object:nil];
                
                NSString *requestStr =[NSString stringWithFormat:@"%@post_images.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSString *strDesc=@"";
                if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
                    //[request setPostValue:@"" forKey:@"description"];
                    strDesc=@"";
                    
                }else{
                    //[request setPostValue:self.txt_desc.text forKey:@"description"];
                    strDesc=self.txt_desc.text;
                }
                
                NSString *strLocation=@"";
                NSString *strLatitude=@"";
                NSString *strLongitude=@"";
                
                if (!self.view_second_cell.hidden) {
                    if ([[Singleton sharedSingleton]get_location_name].length !=0) {
                        NSLog(@"Share");
//                        [request setPostValue:[[Singleton sharedSingleton]get_location_name] forKey:@"location"];
//                        [request setPostValue:[[Singleton sharedSingleton]get_location_lat] forKey:@"lat"];
//                        [request setPostValue:[[Singleton sharedSingleton]get_location_lng] forKey:@"lng"];
                        
                        strLocation=[[Singleton sharedSingleton]get_location_name];
                        strLatitude=[[Singleton sharedSingleton]get_location_lat];
                        strLongitude=[[Singleton sharedSingleton]get_location_lng];
                    }
                }
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"1"]) {
                    UIImageWriteToSavedPhotosAlbum(newImage, 1, nil, nil);
                }
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        strDesc,@"description",
                                        imageKeys,@"image",
                                        strLocation,@"location",
                                        strLatitude,@"lat",
                                        strLongitude,@"lng",
                                        nil];
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14234" :params];
            }
            
        });
    });
}

-(void)postImagesAPIResponce :(NSNotification *)notification {
    NSLog(@"API EndTime:%@",[NSDate date]);
    [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"Success");
       // [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeBlue title:@"Photo upload successfully." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        self.txt_desc.text=@"Write a caption...";
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Photo upload successfully." hideAfter:2];
        
        [StaticClass saveToUserDefaults:@"1" :@"IMAGEUPLOADEDSUCCESSFULLY"];
        [self dismissViewControllerAnimated:NO completion:^{
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view_from_share" object:self];
            
        }];
    }
}

-(void)FailpostImagesAPIResponce :(NSNotification *)notification {
    [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark - Facebook Sharing
-(IBAction)btn_facebook_share_click:(id)sender{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark - Twitter Sharing
-(IBAction)btn_twitter_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark - Messaage Sharing
-(IBAction)btn_message_share_click:(id)sender{
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = self.img_show_photo.image;
    
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    
    if([MFMessageComposeViewController canSendText]) {
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Your Message Body"];
        picker.messageComposeDelegate = self;
        [picker setBody:emailBody];// your recipient number or self for testing
        picker.body = emailBody;
        NSLog(@"Picker -- %@",picker.body);
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    /*    MessageComposeResultCancelled,
     MessageComposeResultSent,
     MessageComposeResultFailed*/
    switch (result) {
        case MessageComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MessageComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent failed!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MessageComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
    }
}

#pragma mark - Email Sharing
-(IBAction)btn_email_share_click:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        NSData *photoData=UIImagePNGRepresentation(self.img_show_photo.image);
        [controller addAttachmentData:photoData mimeType:@"image/png" fileName:[NSString stringWithFormat:@"photo.png"]];
        controller.mailComposeDelegate = self;
        if (controller)
            [self presentViewController:controller animated:YES completion:nil];
        [controller release];
    }
    else {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)stopSpinner {
    activity.hidden=YES;
    [activity stopAnimating];
    [activity removeFromSuperview];
    [lblSpinnerBg removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

-(void)startSpinner {
    [self.view setUserInteractionEnabled:NO];
    
    lblSpinnerBg = [[UILabel alloc] initWithFrame:CGRectMake(120,190,80,80)];
    lblSpinnerBg.backgroundColor = [UIColor blackColor];
    lblSpinnerBg.alpha = 0.8;
    
    lblSpinnerBg.layer.cornerRadius = 8.0;
    lblSpinnerBg.layer.masksToBounds=YES;
    
    [self.view addSubview:lblSpinnerBg];
    
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activity.frame=CGRectMake(135,205,50,50);
    activity.color=[UIColor colorWithRed:(1.0*177)/255 green:(1.0*174)/255 blue:(1.0*174)/255 alpha:1.0];
    [activity startAnimating];
    [self.view addSubview:activity];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [switch_map release];
    [show_location_listViewObj release];
    [view_third_cell release];
    [img_bg_third_cell release];

    [btn_location release];
    [lbl_optional release];
    [view_second_cell release];
    
    [img_bg_secondcell release];
    [txt_desc release];
    [view_bg_show_photo release];
    [img_show_photo release];
    [img_bg_firstcell release];
    [scrollview release];
    [img_photo release];
    [super dealloc];
}

@end
