
#import "ThirdAdsViewController.h"
#import "CarbonKit.h"

@interface ThirdAdsViewController () <UIScrollViewDelegate> {
    CarbonSwipeRefresh *refresh;
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet UILabel *ptitle;
@end

@implementation ThirdAdsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.ptitle.text = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    NSString *urlNameInString = @"http://mahalkum.com/blog/?page_id=16";
    NSURL *url = [NSURL URLWithString:urlNameInString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
    // We need to set self as delegate of the inner scrollview of the webview to override scrollViewShouldScrollToTop
    self.webView.scrollView.delegate = self;
    
    refresh = [[CarbonSwipeRefresh alloc] initWithScrollView:self.webView.scrollView];
    [refresh setMarginTop:10];
    [refresh setColors:@[[UIColor blueColor], [UIColor redColor], [UIColor orangeColor], [UIColor greenColor]]];
    [self.view addSubview:refresh];
    
    [refresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}

-(void)viewWillAppear:(BOOL)animated
{
    _webView.delegate = self;
}
- (void)updateBackButton {
    if ([self.webView canGoBack]) {
        if (!self.navigationItem.leftBarButtonItem) {
            [self.navigationItem setHidesBackButton:YES animated:YES];
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backWasClicked:)];
            [self.navigationItem setLeftBarButtonItem:backItem animated:YES];
        }
    }
    else {
        [self.navigationItem setLeftBarButtonItem:nil animated:YES];
        [self.navigationItem setHidesBackButton:YES animated:YES];
    }
}

- (void)backWasClicked:(id)sender {
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self updateBackButton];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.ptitle.text = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self updateBackButton];
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [ProgressHUD showError:@"Connection Error"];
}

- (void)refresh:(id)sender {
    NSLog(@"REFRESH");
    [self.webView reload];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self endRefreshing];
    });
}

- (void)endRefreshing {
    [refresh endRefreshing];
    [SVProgressHUD dismiss];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // Open PDF files, Specific Pages, and External Links (not beginning with http://www.example.com) in Safari.app
    if (
        (navigationType == UIWebViewNavigationTypeLinkClicked) &&
        ([[[request URL] absoluteString] hasPrefix:@"http://mahalkum.com/blog/"])
        ) {
        SVWebViewController *webViewController = [[SVWebViewController alloc] initWithURL:[request URL]];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:nil action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
        [self.navigationController pushViewController:webViewController animated:YES];
        return NO;
    }else if (
              (navigationType == UIWebViewNavigationTypeLinkClicked) &&
              (![[[request URL] absoluteString] hasPrefix:@"http://mahalkum.com/blog/"])
              ) {
        SVModalWebViewController *exwebViewController = [[SVModalWebViewController alloc] initWithURL:[request URL]];
        [self presentViewController:exwebViewController animated:YES completion:NULL];
        return NO;
    }
    return YES;
}
@end
