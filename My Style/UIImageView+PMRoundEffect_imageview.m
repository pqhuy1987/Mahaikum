//
//  UIImageView+PMRoundEffect_imageview.m
//  My Style
//
//  Created by Tis Macmini on 4/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "UIImageView+PMRoundEffect_imageview.h"

@implementation UIImageView (PMRoundEffect_imageview)


-(UIImageView *)addinnerglowto:(UIImageView *)imageview shadowRadius:(float)shadowRadius{
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [UIColor blueColor].CGColor;
    sublayer.shadowOffset = CGSizeMake(0, 3);
    sublayer.shadowRadius = 5.0;
    sublayer.shadowColor = [UIColor blackColor].CGColor;
    sublayer.shadowOpacity = 0.8;
    sublayer.frame = self.bounds;
    sublayer.borderColor = [UIColor blackColor].CGColor;
    sublayer.borderWidth = 2.0;
    sublayer.cornerRadius = 10.0;
   // [self.layer addSublayer:sublayer];
    [imageview.layer addSublayer:sublayer];
    return imageview;
}
-(void)addinnerglowtoImageViewshadowRadius:(float)shadowRadius{
    CALayer *sublayer = [self  layer];
    sublayer.backgroundColor = [UIColor clearColor].CGColor;
    sublayer.shadowOffset = CGSizeMake(0, 2);
    sublayer.shadowRadius = 5.0;
    sublayer.shadowColor = [UIColor blackColor].CGColor;
    sublayer.shadowOpacity = 0.8;
    sublayer.frame = self.frame;
    sublayer.cornerRadius =5.0f;
  //  sublayer.masksToBounds =YES;
   // sublayer.borderColor = [UIColor blackColor].CGColor;
   // sublayer.borderWidth = 1.0;
    
   
    

    
}

-(void)addinnerglowfor_shadow_shadowRadius:(float)shadowRadius{
    CALayer *sublayer = [self  layer];
    sublayer.backgroundColor = [UIColor clearColor].CGColor;
    sublayer.shadowOffset = CGSizeMake(0, 2);
    sublayer.shadowRadius = 5.0;
    sublayer.shadowColor = [UIColor blackColor].CGColor;
    sublayer.shadowOpacity = 0.8;
    sublayer.frame = self.frame;
    sublayer.cornerRadius =5.0f;
  //  sublayer.masksToBounds =YES;

}

@end
