//
//  Forgot_password_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Forgot_password_ViewController.h"
#import "AppDelegate.h"

@interface Forgot_password_ViewController ()

@end

@implementation Forgot_password_ViewController

@synthesize img_cell_bg,btnUserName,btnFacebook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_cancel_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
     self.view.backgroundColor =[UIColor whiteColor];
    
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =4.0f;
    
    _reset_email_viewObj =[[Reset_password_from_username_email alloc]initWithNibName:@"Reset_password_from_username_email" bundle:nil];
    _reset_fb_viewObj =[[Rest_password_throw_fb alloc]initWithNibName:@"Rest_password_throw_fb" bundle:nil];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    if (isiPhone) {
        btnUserName.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    }else{
        btnUserName.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30.0];
    }
}

-(IBAction)btn_reset_username_throw:(id)sender{

    [self.navigationController pushViewController:_reset_email_viewObj animated:YES];
}

-(IBAction)btn_reset_fb_throw:(id)sender{
    [self start_activity];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
 [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self get_user_data_from_fb];
    }];
}

-(void)get_user_data_from_fb{
    
    NSLog(@"login facebook click");
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];

    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"me/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
}
- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.debugDescription);
        [self stop_activity];
    } else {
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        [StaticClass saveToUserDefaults:@"1" :BY_FACEBOOK_LOGIN];
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSLog(@"%@",[dictionary valueForKey:@"email"]);
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14217" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAccountDetailsResponce:) name:@"14217" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14217" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetAccountDetailsResponce:) name:@"-14217" object:nil];
        
        
        NSString *requestStr = [NSString stringWithFormat:@"%@get_account_details.php",[[Singleton sharedSingleton] getBaseURL]];
        NSLog(@"requestStr:%@",requestStr);
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                [dictionary valueForKey:@"email"],@"key",
                                nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14217" :params];
    }
}

-(void)getAccountDetailsResponce:(NSNotification *)notification {
    [self stop_activity];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14217" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14217" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-1"]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Account Found" message:@"Sorry, we could not find any matching accounts." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"reset throw email");
        
        NSDictionary *dict =[result valueForKey:@"data"];
        
        _reset_fb_viewObj.str_username =[StaticClass urlDecode:[dict valueForKey:@"user_name"]];
        _reset_fb_viewObj.str_image_url =[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
        _reset_fb_viewObj.str_email =[StaticClass urlDecode:[dict valueForKey:@"email"]];
        [self.navigationController pushViewController:_reset_fb_viewObj animated:YES];
    }
}

-(void)FailgetAccountDetailsResponce:(NSNotification *)notification {
    [self stop_activity];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14217" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14217" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}
-(void)stop_activity{
    [DejalBezelActivityView removeView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
