//
//  Venue.h
//  CoffeeShop
//
//  Created by Scott McAlister on 5/7/12.
//  Copyright (c) 2012 4 Arrows Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import "Stats.h"

@interface Venue : NSObject

@property (strong, nonatomic) NSString *venueID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) Location *location;
@property (strong, nonatomic) Stats    *stats;
@end
