//
//  VenueCell.h
//  My Style
//
//  Created by Tis Macmini on 5/20/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueCell : UITableViewCell {

    UILabel *nameLabel;
    UILabel *distanceLabel;
    UILabel *checkinsLabel;
    
    UILabel *lbl_dict;
}   
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong)IBOutlet UILabel *lbl_dict;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *checkinsLabel;
 
@end
