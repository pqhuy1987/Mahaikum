//
//  Venue.m
//  CoffeeShop
//
//  Created by Scott McAlister on 5/7/12.
//  Copyright (c) 2012 4 Arrows Media. All rights reserved.
//

#import "Venue.h"

@implementation Venue

@synthesize venueID, name, location, stats;

@end
