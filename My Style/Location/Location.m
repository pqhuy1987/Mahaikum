//
//  Location.m
//  CoffeeShop
//
//  Created by Scott McAlister on 5/16/12.
//  Copyright (c) 2012 4 Arrows Media. All rights reserved.
//

#import "Location.h"

@implementation Location
@synthesize address, city, country, crossStreet, postalCode, state, distance, lat, lng;

@end
