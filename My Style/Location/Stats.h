//
//  Stats.h
//  CoffeeShop
//
//  Created by Scott McAlister on 5/17/12.
//  Copyright (c) 2012 4 Arrows Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stats : NSObject

@property (nonatomic, strong) NSNumber *checkins;
@property (nonatomic, strong) NSNumber *tips;
@property (nonatomic, strong) NSNumber *users;

@end
