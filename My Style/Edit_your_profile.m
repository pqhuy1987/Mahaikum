//
//  Edit_your_profile.m
//  My Style
//
//  Created by Tis Macmini on 5/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Edit_your_profile.h"
#import "AppDelegate.h"
#import "TWPhotoPickerController.h"
#import "TGCameraViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UIView+Layout.h"

@interface Edit_your_profile () <FindLocationViewControllerDelegate,GMSMapViewDelegate>
{
    FindLocationViewController *findLocationView;
    CLLocationCoordinate2D backedLocation;
    GMSMapView  *googlemapview;
    NSInteger btnswitchgenderflg;
}

@end

@implementation Edit_your_profile
@synthesize img_first_cell,img_photo,scrollview,img_second_cell,img_third_cell,img_fourth_cell,imgswitchgenderfemale,imgswitchgendermale,lblgender,lblheader,btnchangepassword,lblsubheaders,lblSpinnerBg;

@synthesize txt_name,txt_username,txt_weburl,txt_bio,txt_email,txt_phone;
@synthesize photoPickerController,image_url,btn_gender;
@synthesize profile_shareObj;
@synthesize is_reload_data,is_show_alert,switch_gender,switch_private;
@synthesize btn_switch_private,activity;
@synthesize txt_CountryCode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_name.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_username.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_weburl.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_phone.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_bio.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
    btnswitchgenderflg = 0;
    
    // you might have to play around a little with numbers in CGRectMake method
    // they work fine with my settings
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(4.5, 11.0, txt_bio.frame.size.width - 20.0, 14.0)];
    [placeholderLabel setText:@"Bio"];
    // placeholderLabel is instance variable retained by view controller
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    [placeholderLabel setTextColor:[UIColor lightGrayColor]];
    placeholderLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
    // textView is UITextView object you want add placeholder text to
    [txt_bio addSubview:placeholderLabel];
  
    self.txt_facebook_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.txt_twitter_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.txt_pinterest_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.txt_google_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.txt_instagram_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    self.txt_youtube_url.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_email.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    btnchangepassword.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
    lblsubheaders.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    lblgender.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    
    self.profile_shareObj=[[Edit_your_profile_share alloc]init];
    
    self.img_first_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_first_cell.layer.cornerRadius =5.0f;
 
    
    self.img_second_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_second_cell.layer.cornerRadius =5.0f;
    
    self.img_third_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_third_cell.layer.cornerRadius =5.0f;
    
    self.img_fourth_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_fourth_cell.layer.cornerRadius =5.0f;
    
//    if (is_iPhone_5) {
//        self.scrollview.contentSize=CGSizeMake(kViewWidth, 1110);
//        self.scrollview.frame = CGRectMake(0,32,kViewWidth,448);
//    }else{
//        self.scrollview.contentSize=CGSizeMake(kViewWidth, 1110);
//        self.scrollview.frame = CGRectMake(0,32,kViewWidth,448);
//    }
    
//    UIToolbar *toolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    toolbar.barStyle= UIBarStyleBlackTranslucent;
//    toolbar.items =@[[[UIBarButtonItem alloc]initWithTitle:@"Cancel"style:UIBarButtonItemStyleBordered target:self
//                     action:@selector(btn_cancel_click:)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                     [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btn_done_click:)]];
//    [toolbar sizeToFit];
//    
//     self.txt_name.inputAccessoryView=toolbar;
//     self.txt_username.inputAccessoryView=toolbar;
//     self.txt_weburl.inputAccessoryView=toolbar;
//     self.txt_bio.inputAccessoryView=toolbar;
//     self.txt_email.inputAccessoryView=toolbar;
//     self.txt_phone.inputAccessoryView=toolbar;
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.switch_gender.onText = NSLocalizedString(@"Male", @"");
	self.switch_gender.offText = NSLocalizedString(@"Female", @"");
    self.switch_gender.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    
    self.switch_private.onText = NSLocalizedString(@"ON", @"");
	self.switch_private.offText = NSLocalizedString(@"OFF", @"");
    self.switch_private.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    [self.switch_private addTarget:self action:@selector(btn_private_valuechange:) forControlEvents:UIControlEventValueChanged];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    backedLocation = delegate.locationManager.location.coordinate;
    [self selectGooglemapLocation:backedLocation name:@"Current Location"];
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(![txt_bio hasText]) {
        [txt_bio addSubview:placeholderLabel];
    } else if ([[txt_bio subviews] containsObject:placeholderLabel]) {
        [placeholderLabel removeFromSuperview];
    }
}
- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![txt_bio hasText]) {
        [txt_bio addSubview:placeholderLabel];
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    is_show_alert=0;

    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];

    if (is_iPhone_5) {
        self.scrollview.contentSize=CGSizeMake(kViewWidth, 1110);
        self.scrollview.frame = CGRectMake(0, 32, kViewWidth, 448);
    }else{
        self.scrollview.contentSize=CGSizeMake(kViewWidth, 1150);
        self.scrollview.frame = CGRectMake(0, 32, kViewWidth, kViewHeight);
    }
    
    if (is_reload_data==1) {
        [self get_user_info_for_edit];
        is_reload_data=0;
    }
    
    [GlobalDefine setRounded:img_photo color:[UIColor blackColor] rate:2.0f];
}

#pragma mark Change Password
-(IBAction)btn_change_password_click:(id)sender{

    Change_password *obj =[[Change_password alloc]initWithNibName:@"Change_password" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark UITextfield Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField==self.txt_username || textField==self.txt_email) {
        
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    NSString *fullString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([textField isEqual:self.txt_facebook_url] && ![fullString hasPrefix:@"www.facebook.com/"])
    {
        if (fullString.length < @"www.facebook.com/".length)
            textField.text = @"www.facebook.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_twitter_url] && ![fullString hasPrefix:@"www.twitter.com/"])
    {
        if (fullString.length < @"www.twitter.com/".length)
            textField.text = @"www.twitter.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_pinterest_url] && ![fullString hasPrefix:@"www.pinterest.com/"])
    {
        if (fullString.length < @"www.pinterest.com/".length)
            textField.text = @"www.pinterest.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_google_url] && ![fullString hasPrefix:@"plus.google.com/"])
    {
        if (fullString.length < @"plus.google.com/".length)
            textField.text = @"plus.google.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_instagram_url] && ![fullString hasPrefix:@"www.instagram.com/"])
    {
        if (fullString.length < @"www.instagram.com/".length)
            textField.text = @"www.instagram.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_youtube_url] && ![fullString hasPrefix:@"www.youtube.com/"])
    {
        if (fullString.length < @"www.youtube.com/".length)
            textField.text = @"www.youtube.com/";
        return NO;
    }
    
    return YES;
}

-(IBAction)btn_cancel_click:(id)sender{
    [self.view endEditing:YES];
    self.scrollview.contentOffset=CGPointMake(0,0);
}
-(IBAction)btn_done_click:(id)sender{
    [self.view endEditing:YES];
    self.scrollview.contentOffset=CGPointMake(0,0);
    
    if (self.txt_username.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if (self.txt_name.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a name." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if (![StaticClass validateEmail:self.txt_email.text]) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
}

#pragma mark - Change Profile pic
-(IBAction)btn_profile_btn_click:(id)sender{
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Change Profile Picture"];
    
//    [sheet setDestructiveButtonWithTitle:@"Remove Current Photo" block:^{
//        
//        
//    }];
    
    [sheet addButtonWithTitle:@"Import from Facebook" block:^{
        [self get_user_photo_facebok];
    }];
    
//    [sheet addButtonWithTitle:@"Import from Twitter" block:^{
//        
//    }];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        [sheet addButtonWithTitle:@"Choose/Take Photo" block:^{
            TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
            [self presentViewController:navigationController animated:YES completion:nil];

//            photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
//            photoPickerController.allowsEditing = YES;
//            [self presentViewController:photoPickerController animated:YES completion:nil];
        }];
    }
    else
    {
        [sheet addButtonWithTitle:@"Choose from Library" block:^{
            //        photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            //        [self presentViewController:photoPickerController animated:YES completion:nil];
            
            TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
            
            photoPicker.cropBlock = ^(UIImage *image) {
                //do something
                if (image == nil) return;
                [self.img_photo setImage:image forState:UIControlStateNormal];
            };
            
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
            [navCon setNavigationBarHidden:YES];
            
            [self presentViewController:navCon animated:YES completion:NULL];
            
        }];
    }
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
	if (baseImage == nil) return;
    [self.img_photo setImage:baseImage forState:UIControlStateNormal];
    
}


#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        //  appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}
-(void)apiFQLI_pic_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    //NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(set_btn_add_photo_image) withObject:self afterDelay:0.1];
        
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
    
}
- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    
    
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    } else {
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
        
        self.image_url=imgU;
        
//        NSLog(@"%@",dictionary);
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            for (NSString *str in [tempResult valueForKey:@"pic"]) {
//                self.image_url=str;
//            }
//            
//        });
        
    }
    
}

- (void)set_btn_add_photo_image{
    [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.image_url] forState:UIControlStateNormal];
//    [self.img_photo.imageView sd_setImageWithURL:[NSURL URLWithString:self.image_url]];
}

#pragma mark - Gender selected
-(IBAction)btnswitchgenderClick:(id)sender{
//    if (btnswitchgenderflg == 0) {
//        btnswitchgenderflg = 1;
//        lblgender.text = @"Male";
//        imgswitchgendermale.hidden = NO;
//        imgswitchgenderfemale.hidden = YES;
//    }
//    else{
//        btnswitchgenderflg = 0;
//        lblgender.text = @"Female";
//        imgswitchgendermale.hidden = YES;
//        imgswitchgenderfemale.hidden = NO;
//    }
}

-(IBAction)btn_gender_click:(id)sender{
    if (!switch_gender.isOn) {
         lblgender.text = @"Female";
    }else{
         lblgender.text = @"Male";
    }
}
- (void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}
- (void)genderSelected:(NSNumber *)selectedIndex element:(id)element {

    //NSArray *array =@[@"-",@"Male",@"Female"];
   // [self.btn_gender setTitle:[array objectAtIndex:[selectedIndex integerValue]] forState:UIControlStateNormal];
    
}

-(IBAction)btn_private_valuechange:(id)sender{

    NSLog(@"value Changed");
    if (is_show_alert!=0) {
        is_show_alert=0;
        if (self.switch_private.isOn) {
            NSLog(@"is ON");
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy on. This means from now on only people you approve will be able to follow you. Are you sure you want to turn privacy on?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
        }else{
            NSLog(@"is OFF");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy off. This means from now on. anyone will be able to follow/see your ads. Are you sure you want to turn privacy off?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
            
        }
       
    }
    else{
        is_show_alert=1;
    }
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{

    if (self.switch_private.isOn) {
        if (buttonIndex==1) {
            [self.switch_private setOn:YES animated:NO];
            is_show_alert=1;
        }else{
            [self.switch_private setOn:NO animated:YES];
            is_show_alert=0;
        }
    }else{
        if (buttonIndex==1) {
            [self.switch_private setOn:NO animated:NO];
            is_show_alert=1;
        }else{
            [self.switch_private setOn:YES animated:YES];
            is_show_alert=0;
        }
    }
}

#pragma mark - Get user info for Edit

-(void)get_user_info_for_edit{
    [SVProgressHUD dismiss];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disUserProfileAPIResponce:) name:@"14209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildisUserProfileAPIResponce:) name:@"-14209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@dis_user_profile.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14209" :params];
}

-(void)disUserProfileAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        NSDictionary *dict =[result valueForKey:@"data"];
        self.profile_shareObj.bio =[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        self.profile_shareObj.email =[StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_shareObj.facebook_id =[dict valueForKey:@"facebook_id"];
        self.profile_shareObj.gender =[dict valueForKey:@"gender"];
        self.profile_shareObj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_shareObj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_shareObj.photos_status =[StaticClass urlDecode:[dict valueForKey:@"photos_status"]];
        self.profile_shareObj.user_id =[dict valueForKey:@"user_id"];
        self.profile_shareObj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
        self.profile_shareObj.weburl =[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        self.profile_shareObj.phone =[StaticClass urlDecode:[dict valueForKey:@"phone"]];
        self.profile_shareObj.countryCode=[StaticClass urlDecode:[dict valueForKey:@"country_code"]];
        self.profile_shareObj.facebook_url =[StaticClass urlDecode:[dict valueForKey:@"facebook_url"]];
        self.profile_shareObj.twitter_url =[StaticClass urlDecode:[dict valueForKey:@"twitter_url"]];
        self.profile_shareObj.pinterest_url =[StaticClass urlDecode:[dict valueForKey:@"pinterest_url"]];
        self.profile_shareObj.google_url =[StaticClass urlDecoding:[dict valueForKey:@"google_url"]];
        self.profile_shareObj.instagram_url =[StaticClass urlDecode:[dict valueForKey:@"instagram_url"]];
        self.profile_shareObj.youtube_url =[StaticClass urlDecode:[dict valueForKey:@"youtube_url"]];
        self.profile_shareObj.latitude =[StaticClass urlDecode:[dict valueForKey:@"latitude"]];
        self.profile_shareObj.longitude =[StaticClass urlDecode:[dict valueForKey:@"longitude"]];
        self.profile_shareObj.delivery_time =[StaticClass urlDecode:[dict valueForKey:@"delivery_time"]];
        self.profile_shareObj.working_hours =[StaticClass urlDecode:[dict valueForKey:@"working_hours"]];
        self.profile_shareObj.shipping_fee =[StaticClass urlDecode:[dict valueForKey:@"shipping_fee"]];
        
        self.txt_name.text =self.profile_shareObj.name;
        self.txt_username.text = self.profile_shareObj.username;
        self.txt_weburl.text = self.profile_shareObj.weburl;
        self.txt_bio.text = self.profile_shareObj.bio;
        self.txt_email.text = self.profile_shareObj.email;
        self.txt_phone.text = self.profile_shareObj.phone;
        self.txt_CountryCode.text=self.profile_shareObj.countryCode;
        self.txt_facebook_url.text = self.profile_shareObj.facebook_url;
        self.txt_twitter_url.text = self.profile_shareObj.twitter_url;
        self.txt_pinterest_url.text = self.profile_shareObj.pinterest_url;
        self.txt_google_url.text = self.profile_shareObj.google_url;
        self.txt_instagram_url.text = self.profile_shareObj.instagram_url;
        self.txt_youtube_url.text = self.profile_shareObj.youtube_url;
        
        self.txt_delivery_time.text=self.profile_shareObj.delivery_time;
        self.txt_working_hours.text=self.profile_shareObj.working_hours;
        self.txt_shipping_fee.text=self.profile_shareObj.shipping_fee;
        
        if(![txt_bio hasText]) {
            [txt_bio addSubview:placeholderLabel];
        } else if ([[txt_bio subviews] containsObject:placeholderLabel]) {
            [placeholderLabel removeFromSuperview];
        }
        
        if ([self.profile_shareObj.photos_status isEqualToString:@"1"]) {
            [self.switch_private setOn:YES animated:YES];
        } else {
            [self.switch_private setOn:NO animated:YES];
        }
        
        if ([self.profile_shareObj.gender isEqualToString:@"f"]) {
            [switch_gender setOn:NO];
        } else {
            [switch_gender setOn:YES];
        }
        
//        if ([self.profile_shareObj.photos_status isEqualToString:@"1"]){
//            imgswitchgenderfemale.hidden = YES;
//            imgswitchgendermale.hidden = NO;
//        }
//        else {
//            imgswitchgenderfemale.hidden = NO;
//            imgswitchgendermale.hidden = YES;
//        }
//        
//        if ([self.profile_shareObj.gender isEqualToString:@"f"]){
//            lblgender.text = @"Female";
//            imgswitchgenderfemale.hidden = NO;
//            imgswitchgendermale.hidden = YES;
//        }
//        else{
//            lblgender.text = @"Male";
//            imgswitchgendermale.hidden = NO;
//            imgswitchgenderfemale.hidden = YES;
//        }
        
        [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.profile_shareObj.image] forState:UIControlStateNormal];
    }
}

-(void)FaildisUserProfileAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
}

#pragma mark - Save

-(IBAction)btn_save_click:(id)sender {
    if (self.txt_username.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if (self.txt_name.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a name." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }

    if (![StaticClass validateEmail:self.txt_email.text]) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    [self update_call];
}

-(void)update_call{
    [SVProgressHUD dismiss];
    
    [txt_name resignFirstResponder];
    [txt_username resignFirstResponder];
    [txt_weburl resignFirstResponder];
    [txt_bio resignFirstResponder];
    [txt_email resignFirstResponder];
    [txt_phone resignFirstResponder];

    
    [SVProgressHUD show];
    
    
    UIImage *newImage = [self.img_photo.imageView.image resizedImage:CGSizeMake(200,200) interpolationQuality:3];
    NSData *img_data = UIImageJPEGRepresentation(newImage,1);
    
    //NSData *img_data  = UIImageJPEGRepresentation(self.img_photo.imageView.image,1);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSDate *dt=[NSDate date];
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *dateString1 = [DateFormatter stringFromDate:dt];
        
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
       // double timePassed_ms = [dt timeIntervalSinceNow];
        
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
//        S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:@"myStyleImg"] autorelease];
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket: [[Singleton sharedSingleton] getBucketName]];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(putObjectResponse.error != nil) {
                NSLog(@"Error: %@", putObjectResponse.error);
                [self upDateAPICalling:imageKeys];
            }
            else {
                NSLog(@"KEY:%@",imageKeys);
                [self upDateAPICalling:imageKeys];
            }            
        });
    });
}

-(void)upDateAPICalling:(NSString *)strky {
    NSString *imageKeys=strky;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUpdateProfileResponce:) name:@"14210" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUpdateProfileResponce:) name:@"-14210" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_profile.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSString *strPhotoStatus=@"";
    if (self.switch_private.isOn) {
        strPhotoStatus=@"1";
    }
    else{
        strPhotoStatus=@"0";
    }
    
    NSString *strGenderString=@"";
    if ([lblgender.text isEqualToString:@"Male"]) {
        strGenderString=@"m";
    }
    else if([lblgender.text isEqualToString:@"Female"]) {
        strGenderString=@"f";
    }
    else {
        strGenderString=@"-";
    }
    
    if (btnswitchgenderflg == 0) {
        btnswitchgenderflg = 1;
        strPhotoStatus=@"1";
    }
    else{
        btnswitchgenderflg = 0;
        strPhotoStatus=@"0";
    }
    
    NSString *uid = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];
    NSString *latitude = [NSString stringWithFormat:@"%f", backedLocation.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", backedLocation.longitude];
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"uid": uid, @"username": self.txt_username.text, @"name": self.txt_name.text, @"email": self.txt_email.text, @"phone": self.txt_phone.text, @"image": imageKeys, @"weburl": self.txt_weburl.text, @"bio": self.txt_bio.text, @"photos_status": strPhotoStatus, @"gender": strGenderString, @"country_code": self.txt_CountryCode.text, @"facebook_url": self.txt_facebook_url.text, @"twitter_url": self.txt_twitter_url.text, @"pinterest_url": self.txt_pinterest_url.text, @"google_url": [StaticClass urlEncoding:self.txt_google_url.text], @"instagram_url": self.txt_instagram_url.text, @"youtube_url": self.txt_youtube_url.text, @"latitude": latitude, @"longitude": longitude, @"delivery_time":self.txt_delivery_time.text, @"working_hours":self.txt_working_hours.text, @"shipping_fee":self.txt_shipping_fee.text};
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14210" :params];
}

-(void)postUpdateProfileResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    NSString *strString=[[NSString alloc] initWithData:[response responseData ] encoding:NSUTF8StringEncoding];
    NSLog(@"strString:%@",strString);
    
    NSString *strResult=[result valueForKey:@"success"];
    if ([strResult isEqualToString:@"false"]) {
        NSString *msg=[StaticClass urlDecode:[result valueForKey:@"message"]];
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:msg hideAfter:2];
        [self get_user_info_for_edit];
    }
    else {
        NSLog(@"Success");
        
        NSDictionary *dataDict=[result objectForKey:@"data"];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dataDict valueForKey:@"image"]] :USER_IMAGE];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dataDict valueForKey:@"username"]] :@"STOREUSERNAME"];
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Profile updated sucessfully" hideAfter:2];
    }
}
-(void)FailpostUpdateProfileResponc:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chooseLocationClicked:(id)sender
{
    findLocationView =[[FindLocationViewController alloc]initWithNibName:@"FindLocationViewController" bundle:nil];
    findLocationView.delegate = self;
    [self.navigationController pushViewController:findLocationView animated:YES];
}

- (void)selectGooglemapLocation:(CLLocationCoordinate2D)location name:(NSString *)locationname
{
    if ( googlemapview == nil )
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:location zoom:13];
        GMSMapView *view = [GMSMapView mapWithFrame:CGRectMake(0, 0, kViewWidth, self.mapView.frame.size.height) camera:camera];
        //    view.myLocationEnabled = YES;
        view.delegate = self;
        [self.mapView removeSubviews];
        [self.mapView addSubview:view];
        GMSMarker   *marker = [GMSMarker markerWithPosition:location];
        marker.title = locationname;
        marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
        marker.map = view;
        marker.userData = marker;
        googlemapview = view;
    }
    else
    {
        [googlemapview clear];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:location zoom:13];
        
        googlemapview.camera = camera;

        GMSMarker   *marker = [GMSMarker markerWithPosition:location];
        marker.title = locationname;
        marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
        marker.map = googlemapview;
        marker.userData = marker;
    }
    
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    
    [googlemapview clear];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:coordinate zoom:13];
    
    googlemapview.camera = camera;
    
    CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    [reverseGeocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        // NSLog(@"Received placemarks: %@", placemarks);
        
        CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
        NSString *countryCode = myPlacemark.ISOcountryCode;
        NSString *countryName = myPlacemark.country;
        NSString *cityName= myPlacemark.subAdministrativeArea;
        NSLog(@"My country code: %@ and countryName: %@ MyCity: %@", countryCode, countryName, cityName);
        
        GMSMarker   *marker = [GMSMarker markerWithPosition:coordinate];
        marker.title = cityName;
        marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
        marker.map = googlemapview;
        marker.userData = marker;
    }];
    
    backedLocation = coordinate;
}

- (void)mapItemSelected:(FindLocationViewController *)locationViewController mapItem:(MKMapItem *)mapItem selectedLocationKind:(NSString *)selectedLocationKind
{
    locationViewController.delegate = nil;
    backedLocation = CLLocationCoordinate2DMake([(CLCircularRegion *)mapItem.placemark.region center].latitude, [(CLCircularRegion *)mapItem.placemark.region center].longitude);
    NSString *locationName = mapItem.placemark.name;
//    formattedAddress = [mapItem.placemark.addressDictionary[@"FormattedAddressLines"] componentsJoinedByString:@", "];
    [self.navigationController popViewControllerAnimated:YES];

//    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
//    MKCoordinateRegion regionToDisplay = MKCoordinateRegionMake(backedLocation, span);
//    [self.mapView setRegion:regionToDisplay];
//    
//    MyAnnotation *ann = [[MyAnnotation alloc] init];
//    ann.title = locationName;
//    ann.coordinate = backedLocation;
//    [self.mapView addAnnotation:ann];
    
    [self selectGooglemapLocation:backedLocation name:locationName];
}

- (void)mapItemSelected:(FindLocationViewController *)locationViewController place:(GMSPlace *)place selectedLocationKind:(NSString *)selectedLocationKind
{
    locationViewController.delegate = nil;
    backedLocation = place.coordinate;
    NSString *locationName = place.name;

    [self.navigationController popViewControllerAnimated:YES];
    
    [self selectGooglemapLocation:backedLocation name:locationName];
}


#pragma mark -
#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *baseImage = image;
    
    if (baseImage == nil) return;
    [self.img_photo setImage:baseImage forState:UIControlStateNormal];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *baseImage = image;
    
    if (baseImage == nil) return;
    [self.img_photo setImage:baseImage forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark - TGCameraDelegate optional

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidSavePhotoAtPath:(NSURL *)assetURL
{
    NSLog(@"%s album path: %@", __PRETTY_FUNCTION__, assetURL);
}

- (void)cameraDidSavePhotoWithError:(NSError *)error
{
    NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
}

#pragma mark Google Map Delegate
- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidStartTileRendering");
}

- (void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidFinishTileRendering");
}

@end
