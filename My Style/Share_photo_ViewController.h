//
//  Share_photo_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "DCRoundSwitch.h"
//#import "AFPhotoEditorController.h"
#import "UITextView+AVTagTextView.h"
#import "Home_tableview_data_share.h"

@protocol Share_photo_ViewControllerDelegate;

@interface Share_photo_ViewController : UIViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,AmazonServiceRequestDelegate,AVTagTextViewDelegate>{

    UIImage *img_photo;
    UIScrollView *scrollview;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_secondcell;
    
    UILabel *lblheader,*lblsubheader1,*lblsubheader2,*lbladdtomap,*lblshareto;
    
    UIImageView *img_show_photo;
    UIView *view_bg_show_photo;
    UITextView *txt_desc;
    
    UIButton *btn_location;
    UILabel *lbl_optional;
    UIView *view_second_cell;
    
    UIButton *btnswitch;
    UIImageView *imgswitchon,*imgswitchoff;
    
    UIView *view_third_cell;
    UIImageView *img_bg_third_cell;
    
    UIActivityIndicatorView *activity;
    
    UILabel *lblSpinnerBg;
    UITableView *tblSearch;
    NSMutableArray *searchArray;
}

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong) IBOutlet DCRoundSwitch *switch_map;

@property(nonatomic,strong)IBOutlet UITextView *txt_desc;
@property(nonatomic,strong)IBOutlet UIImageView *img_show_photo;
@property(nonatomic,strong)IBOutlet UIView *view_bg_show_photo;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblsubheader1,*lblsubheader2,*lbladdtomap,*lblshareto;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_secondcell;
@property(nonatomic,strong)IBOutlet UIButton *btnswitch;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchon,*imgswitchoff;

@property(nonatomic,strong)UIImage *img_photo;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollview;

@property(nonatomic,strong)IBOutlet UIButton *btn_location;
@property(nonatomic,strong)IBOutlet UILabel *lbl_optional;
@property(nonatomic,strong)IBOutlet UIView *view_second_cell;
@property (strong, nonatomic) IBOutlet UIButton *btn_category;
@property (strong, nonatomic) IBOutlet UITextField *itemPriceEdit;
@property (strong, nonatomic) IBOutlet UIButton *selectCurrencyButton;

@property (nonatomic, strong) Home_tableview_data_share *shareObjs;
@property (nonatomic) BOOL editing;

@property (nonatomic, strong) id<Share_photo_ViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *markAsSoldView;
@property (strong, nonatomic) IBOutlet UISwitch *soldSwitch;

@property(nonatomic,strong)IBOutlet UIView *view_third_cell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_third_cell;

@property (nonatomic, strong) AmazonS3Client *s3;
@property (nonatomic, strong) UIActivityIndicatorView *activity;

@property (nonatomic, strong) IBOutlet UITableView *tblSearch;
@property (nonatomic, strong) NSMutableArray *searchArray;
@property (nonatomic, strong) NSMutableArray *tagsArray;

-(IBAction)editBtnClick :(id)sender ;
-(IBAction)btnswitchClick:(id)sender;

@end

@protocol Share_photo_ViewControllerDelegate <NSObject>
@optional
- (void)shareObjsChanged:(Share_photo_ViewController *)viewController shareObjs:(Home_tableview_data_share *)shareObjs;

@end
