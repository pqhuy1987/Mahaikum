//
//  UIImageView+PMRoundEffect_imageview.h
//  My Style
//
//  Created by Tis Macmini on 4/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (PMRoundEffect_imageview)
-(UIImageView *)addinnerglowto:(UIImageView *)imageview shadowRadius:(float)shadowRadius;
-(void)addinnerglowtoImageViewshadowRadius:(float)shadowRadius;
-(void)addinnerglowfor_shadow_shadowRadius:(float)shadowRadius;
@end
