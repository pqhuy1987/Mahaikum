//
//  CustomPin.h
//  Cruze
//
//  Created by TISMobile on 3/24/11.
//  Copyright 2011 SDA Software Associates Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomPin : MKAnnotationView {

    UIImageView *egoImageObj;
}

@property (nonatomic, strong) UIImageView *egoImageObj;

- (id)initWithAnnotation:(id <MKAnnotation>) annotation image: (NSString *) imageName;

- (id)initWithAnnotation:(id <MKAnnotation>) annotation imageURL: (NSString *) imageName;

@end
