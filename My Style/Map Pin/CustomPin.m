//
//  CustomPin.m
//  Cruze
//
//  Created by TISMobile on 3/24/11.
//  Copyright 2011 SDA Software Associates Inc. All rights reserved.
//

#import "CustomPin.h"
#import "StaticClass.h"

@implementation CustomPin

@synthesize egoImageObj;

-(id)initWithAnnotation:(id <MKAnnotation>)annotation image:  (NSString * ) imageName  {
    self = [super initWithAnnotation:annotation reuseIdentifier:@"CustomId"];	
    if (self) {
        UIImage *theImage = [UIImage imageNamed:imageName];		
        if (!theImage)
            return nil;		
        self.image = theImage;
    }    
    return self;
}


- (id)initWithAnnotation:(id <MKAnnotation>) annotation imageURL: (NSString *) imageName {
    self = [super initWithAnnotation:annotation reuseIdentifier:@"CustomId"];	
    if (self) {
        UIImageView *wallIconBg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wallIconBg.png"]];
        wallIconBg.frame=CGRectMake(0,0,43,43);
        [self  addSubview:wallIconBg];
        
        //egoImageObj = [[UIImageView alloc] initWithPlaceholderImage:[UIImage imageNamed:@"ThumbImg.png"]];
        
        egoImageObj=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ThumbImg.png"]];
        egoImageObj.frame = CGRectMake(4,4,35,35);
        [self addSubview:egoImageObj];
        if (!egoImageObj.image)
            return nil;		
    }
    return self;
}

@end
