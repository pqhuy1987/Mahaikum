//
//  MyAnnotation.h
//  DrinkonsApp
//
//  Created by Test on 17/02/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface MyAnnotation:NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString* title;
	NSInteger annotaionListID;
	NSInteger replacedMapId;
	NSString *availableMapId ;
    UIImageView *egoImageObj;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic) NSInteger annotaionListID;
@property (nonatomic) NSInteger replacedMapId;
@property (nonatomic, strong) NSString *availableMapId ;
@property (nonatomic, strong) UIImageView *egoImageObj;

@end
