//
//  Detail_view_complete_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Detail_view_complete_cell : UITableViewCell{

    UILabel *lbl_border_line;
    UISwitch *switch_complete;
}
@property(nonatomic,strong)IBOutlet  UILabel *lbl_border_line;
@property(nonatomic,strong)IBOutlet  UISwitch *switch_complete;
@end
