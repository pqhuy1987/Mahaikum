//
//  News_feed_following_cell.m
//  My Style
//
//  Created by Tis Macmini on 6/10/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "News_feed_following_cell.h"


@implementation News_feed_following_cell
@synthesize btn_user_img,lbl_time,lbl_dec,imgRateImage;
@synthesize imgBottomLine;

-(void)draw_in_cell{
    self.lbl_dec = [[LORichTextLabel alloc] initWithWidth:kViewWidth];
	[self.lbl_dec setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
    
    [self.lbl_dec setTextColor:[UIColor blackColor]];
	[self.lbl_dec setBackgroundColor:[UIColor clearColor]];
	[self.lbl_dec positionAtX:55.0 andY:5.0];
    [self.contentView addSubview:self.lbl_dec];
}


@end
