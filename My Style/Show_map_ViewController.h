//
//  Show_map_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <CoreLocation/CoreLocation.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Home_tableview_data_share.h"
#import "CustomAlertView_map.h"

#import "image_collection_cell.h"
#import "Image_detail.h"
#import "MKMapView+ZoomLevel.h"

@interface Show_map_ViewController : UIViewController<MKMapViewDelegate>{
    NSMutableArray *array_data;
    
    UIView *view_image;
    
    UICollectionView *collectionViewObj;
    
    
}
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_collection;
@property(nonatomic,strong)IBOutlet UIView *view_zoom;
@property(nonatomic,strong)IBOutlet UIImageView *img_zoom;

@property(nonatomic,strong)IBOutlet UIView *view_zoom_controller;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo;
@property(nonatomic,strong)IBOutlet UIButton *btn_info;
@property(nonatomic,strong)IBOutlet  UIView *view_show_image;
@property(nonatomic,strong)NSMutableArray *array_data;
@property(nonatomic,strong)IBOutlet UIView *view_image;
@property(nonatomic,strong)IBOutlet UICollectionView *collectionViewObj;
@property(nonatomic,strong) Image_detail *image_detail_viewObj;
@end
