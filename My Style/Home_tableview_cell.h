//
//  Home_tableview_cell.h
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOCache.h"
#import "DLStarRatingControl.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Home_tableview_data_share.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Comment_share.h"

@protocol home_tableview_cell <NSObject>

- (void)userViewAllCommend;
@end



@interface Home_tableview_cell : UITableViewCell{
    

    
    
    IBOutlet DLStarRatingControl *dlstarObj;
    
    
    IBOutlet DLStarRatingControl *dlstarAvgObj;
    
    UIImageView *img_big;
    UIButton *btn_likes_count;
    UIImageView *img_likes;
    UIButton *btn_like;
    UIButton *btn_comment;
    UIButton *btn_photo_option;
    UILabel *lblavgrating,*lbltotrating;
    UIView *ratingview;
    UIImageView *ratingimg;
    
    LORichTextLabel *lbl_desc;
    UIImageView *img_desc;
    LORichTextLabel *view_like_btn;
    UIButton *btn_view_all_comments;
    
    LORichTextLabel *lbl_comments;
    UIImageView *imgDetailBG;
    UIImageView *imgWhiteImage;
    
    
}

@property (weak, nonatomic, nonatomic) id <home_tableview_cell> delegate;

@property(nonatomic,strong)IBOutlet UIImageView *ratingimg;
@property(nonatomic,strong)IBOutlet UIImageView *img_likes;
@property(nonatomic,strong)IBOutlet LORichTextLabel *lbl_desc;
@property(nonatomic,strong)IBOutlet UIImageView *img_big;
@property(nonatomic,strong)IBOutlet UIButton *btn_likes_count;
@property(nonatomic,strong)IBOutlet UIButton *btn_like;
@property(nonatomic,strong)IBOutlet UIButton *btn_comment;
@property(nonatomic,strong)IBOutlet UIButton *btn_photo_option;
@property(nonatomic,strong)IBOutlet LORichTextLabel *view_like_btn;
@property(nonatomic,strong)IBOutlet UIImageView *img_desc;
@property(nonatomic,strong)IBOutlet DLStarRatingControl *dlstarObj;
@property(nonatomic,strong)IBOutlet DLStarRatingControl *dlstarAvgObj;
@property(nonatomic,strong)IBOutlet UIButton *btn_view_all_comments;
@property(nonatomic,strong)LORichTextLabel *lbl_comments;
@property(nonatomic,strong)IBOutlet UIImageView *imgDetailBG;
@property(nonatomic,strong)IBOutlet UILabel *lblavgrating,*lbltotrating;
@property(nonatomic,strong)IBOutlet UIView *ratingview;

@property(nonatomic,strong)IBOutlet UIImageView *imgWhiteImage;
-(void)draw_desc_in_cell;
-(void)draw_like_button_in_cell;

- (void)setImageURL:(NSURL *)imageURL placeholderImage:(UIImage *)placeholderImage;

+(float)get_tableview_hight:(Home_tableview_data_share *)shareObj;
-(void)redraw_cell:(Home_tableview_data_share *)shareObj andUserStyle:(LORichTextLabelStyle *)userStyle AtIndexPath:(NSIndexPath *)indexPath;
@end
