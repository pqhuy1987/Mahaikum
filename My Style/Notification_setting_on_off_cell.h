//
//  Notification_setting_on_off_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/29/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCRoundSwitch.h"

@interface Notification_setting_on_off_cell : UITableViewCell{
    DCRoundSwitch *switch_on_off;
    UIButton *btnswitch;
    UIImageView *imgswitchon,*imgswitchoff;
    
}
@property(nonatomic,strong)IBOutlet  DCRoundSwitch *switch_on_off;
@property(nonatomic,strong)IBOutlet UIButton *btnswitch;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchon,*imgswitchoff;
@end
