//
//  Home_tableview_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Home_tableview_cell_iPad.h"
#import "NPRImageView.h"

#define CGRectSetPos( r, x, y ) CGRectMake( x, y, r.size.width, r.size.height )

@implementation Home_tableview_cell_iPad
@synthesize img_big,btn_likes_count,btn_like,btn_comment,btn_photo_option;
@synthesize lbl_desc,img_likes,view_like_btn,img_desc,dlstarObj,btn_view_all_comments;
@synthesize lbl_comments;

-(void)draw_desc_in_cell{
    
    self.lbl_desc = [[LORichTextLabel alloc] initWithWidth:616];
	[self.lbl_desc setFont:[UIFont fontWithName:@"Helvetica" size:28.0]];
	[self.lbl_desc setTextColor:[UIColor whiteColor]];
	[self.lbl_desc setBackgroundColor:[UIColor clearColor]];
	[self.lbl_desc positionAtX:25.0 andY:338.0];
    [self.contentView addSubview:self.lbl_desc];
    
    // Comments view
    self.lbl_comments = [[LORichTextLabel alloc] initWithWidth:616];
    [self.lbl_comments setFont:[UIFont fontWithName:@"Helvetica" size:28.0]];
	[self.lbl_comments setTextColor:[UIColor whiteColor]];
	[self.lbl_comments setBackgroundColor:[UIColor clearColor]];
	[self.lbl_comments positionAtX:25.0 andY:338.0];
    [self.contentView addSubview:self.lbl_comments];

    self.img_big = [[NPRImageView alloc] initWithFrame:CGRectMake(22,20,725,560)];
    [self.img_big setBackgroundColor:[UIColor clearColor]];
    [self.img_big setContentMode:UIViewContentModeScaleAspectFill];
    [self.contentView addSubview:self.img_big];
}

- (void)setImageURL:(NSURL *)imageURL placeholderImage:(UIImage *)placeholderImage{
    [self.img_big setImageWithContentsOfURL:imageURL placeholderImage:placeholderImage];
}

-(void)draw_like_button_in_cell{

    self.view_like_btn = [[LORichTextLabel alloc] initWithWidth:616];
    [self.view_like_btn setFont:[UIFont fontWithName:@"Helvetica" size:28.0]];
	[self.view_like_btn setTextColor:[UIColor whiteColor]];
	[self.view_like_btn setBackgroundColor:[UIColor clearColor]];
	[self.view_like_btn positionAtX:76.0 andY:630.0f];
    [self.contentView addSubview:self.view_like_btn];
}

+(float)get_tableview_hight:(Home_tableview_data_share *)shareObj{

    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0/255.0 green:112.0/255.0 blue:112.0/255.0 alpha:1.0]];
    
    LORichTextLabel *lbl_temp = [[LORichTextLabel alloc] initWithWidth:616];
	[lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:28.0]];
    
    [lbl_temp addStyle:urlStyle forPrefix:@"#"];
    [lbl_temp addStyle:urlStyle forPrefix:@"@"];
    [lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    
    [lbl_temp addStyle:urlStyle forPrefix:shareObj.username];
    
    float height = 630.0f;
    if ([shareObj.likes intValue]==0){

    }
    else{
        
        if ([shareObj.likes intValue]<6) {
            
            lbl_temp.text =[shareObj.array_liked_by componentsJoinedByString:@" "];
            
            for (NSString *temusername in shareObj.array_liked_by) {
                [lbl_temp addStyle:urlStyle forPrefix:temusername];
            }
            
            height = height +lbl_temp.height;
            
        }
        else{
            height=height+33.0f;
        }
    }
    
    [lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.description]];
    
    if (shareObj.description.length==0) {
        height =height+8;
    }
    else{
        height =height + lbl_temp.height +8;
        
    }
    if (shareObj.array_comments.count>3) {
        height =height +25.0f;
    }
    if (shareObj.array_comments.count >0) {

        NSMutableString *coments =[[NSMutableString alloc]init];
        
        int comment_count_flag =0;
        for (Comment_share *obj in shareObj.array_comments) {
            NSString *tem =[NSString stringWithFormat:@"%@ %@",obj.username,obj.comment_desc];
            
            NSArray *temarray =[tem componentsSeparatedByString:@" "];
            NSMutableArray *array =[NSMutableArray arrayWithArray:temarray];
            
            if ([[array lastObject] isEqualToString:@""]) {
                [array removeLastObject];
            }
            
            [coments appendFormat:@"\n %@",[array componentsJoinedByString:@" "]];
            [lbl_temp addStyle:urlStyle forPrefix:obj.username];
            comment_count_flag++;
            if (comment_count_flag>2) {
                break;
            }
        }
        NSMutableString * newString = [NSMutableString stringWithString:coments];
        NSRange foundRange = [newString rangeOfString:@"\n"];
        if (foundRange.location != NSNotFound)
        {
            [newString replaceCharactersInRange:foundRange
                                     withString:@""];
        }
        
        lbl_temp.text =[NSString stringWithFormat:@"%@",newString];
        if ([[NSString stringWithFormat:@"%@",coments]length]==0) {
            
        }
        else{
            if (shareObj.description.length==0) {
            }
            height =height + lbl_temp.height+8;
        }
    }
    return height+33.0f+95.0f+35.0f;
}

-(void)redraw_cell:(Home_tableview_data_share *)shareObj andUserStyle:(LORichTextLabelStyle *)userStyle AtIndexPath:(NSIndexPath *)indexPath{

    [self.lbl_desc addStyle:userStyle forPrefix:shareObj.username];
    
    [self setImageURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    
    if ([shareObj.liked isEqualToString:@"no"]) {
        [self.btn_like setImage:[UIImage imageNamed:@"likebtn@2x.png"] forState:UIControlStateNormal];
    }else{
        [self.btn_like setImage:[UIImage imageNamed:@"likedbtn@2x.png"] forState:UIControlStateNormal];
    }
    
    self.btn_photo_option.tag=indexPath.section;
    self.btn_like.tag=indexPath.section;
    self.btn_likes_count.tag =indexPath.section;
    self.btn_comment.tag = indexPath.section;
    self.btn_view_all_comments.tag = indexPath.section;
    
    [self.dlstarObj setTag:indexPath.section];
    [self.dlstarObj setRating:[shareObj.my_rating intValue]];
    
    float height = 630.0f;
    if ([shareObj.likes intValue]==0){
        self.btn_likes_count.hidden=YES;
        self.img_likes.hidden = YES;
        self.view_like_btn.hidden =YES;
    }
    else{
        self.img_likes.hidden = NO;
        
        if ([shareObj.likes intValue]<6) {
            
            self.view_like_btn.text =[shareObj.array_liked_by componentsJoinedByString:@" "];
            self.view_like_btn.hidden=NO;
            self.btn_likes_count.hidden=YES;
            self.view_like_btn.hidden =NO;
            
            for (NSString *temusername in shareObj.array_liked_by) {
                [self.view_like_btn addStyle:userStyle forPrefix:temusername];
            }
            self.view_like_btn.frame = CGRectSetPos(self.lbl_desc.frame, 76, height+5);
            height = height + self.view_like_btn.height;
        }
        else{
            self.view_like_btn.hidden=YES;
            self.btn_likes_count.hidden=NO;
            [self.btn_likes_count setTitle:[NSString stringWithFormat:@"%@ likes",shareObj.likes] forState:UIControlStateNormal];
            [self.btn_likes_count setFrame:CGRectMake(76,630,616,52)];
            height=height+33.0f;
        }
    }
    
    [self.lbl_desc setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.description]];
    
    if (shareObj.description.length==0) {
        self.lbl_desc.hidden=YES;
        self.img_desc.hidden=YES;
        height =height+8;
    }
    else{
        self.lbl_desc.hidden=NO;
        self.img_desc.hidden=NO;
                
        self.img_desc.frame =CGRectMake(self.img_desc.frame.origin.x, height+10, self.img_desc.frame.size.width, self.img_desc.frame.size.height);
        
        self.lbl_desc.frame = CGRectSetPos(self.lbl_desc.frame, 76, height+8 );
        height =height + self.lbl_desc.height +8;
    }
    if (shareObj.array_comments.count>3) {
        self.btn_view_all_comments.hidden =NO;
        self.btn_view_all_comments.frame = CGRectMake(76,height, 616, 25);
        [self.btn_view_all_comments setTitle:[NSString stringWithFormat:@"view all %@ comments",shareObj.comment_count] forState:UIControlStateNormal];
        height =height +25.0f;
    }
    else{
        self.btn_view_all_comments.hidden=YES;
    }
    
    if (shareObj.array_comments.count >0) {
        self.lbl_comments.hidden =NO;
        NSMutableString *coments =[[NSMutableString alloc]init];
        
        int comment_count_flag =0;
        
        for (Comment_share *obj in shareObj.array_comments) {
            NSString *tem =[NSString stringWithFormat:@"%@ %@",obj.username,obj.comment_desc];
            
            NSArray *temarray =[tem componentsSeparatedByString:@" "];
            NSMutableArray *array =[NSMutableArray arrayWithArray:temarray];
            
            if ([[array lastObject] isEqualToString:@""]) {
                [array removeLastObject];
            }
            
            [coments appendFormat:@"\n %@",[array componentsJoinedByString:@" "]];
            [self.lbl_comments addStyle:userStyle forPrefix:obj.username];
            comment_count_flag++;
            if (comment_count_flag>2) {
                break;
            }
        }
        NSMutableString * newString = [NSMutableString stringWithString:coments];
        NSRange foundRange = [newString rangeOfString:@"\n"];
        if (foundRange.location != NSNotFound)
        {
            [newString replaceCharactersInRange:foundRange
                                     withString:@""];
        }
        
        self.lbl_comments.text =[NSString stringWithFormat:@"%@",newString];
        if ([[NSString stringWithFormat:@"%@",coments]length]==0) {
            
        }else{
                       
            self.lbl_comments.frame = CGRectSetPos(self.lbl_desc.frame, 76, height);
            self.lbl_comments.layer.masksToBounds =YES;
            
            if (shareObj.description.length==0) {
                self.img_desc.hidden =NO;
                self.img_desc.frame =CGRectMake(self.img_desc.frame.origin.x, height+2, self.img_desc.frame.size.width, self.img_desc.frame.size.height);
            }
            
            height =height +  self.lbl_comments.height+8;
        }
    }else{
        self.lbl_comments.hidden =YES;
    }
    
    self.dlstarObj.frame =CGRectMake(self.dlstarObj.frame.origin.x,height, self.dlstarObj.frame.size.width, self.dlstarObj.frame.size.height);
    
    self.btn_like.frame = CGRectMake(self.btn_like.frame.origin.x,height+70, self.btn_like.frame.size.width, self.btn_like.frame.size.height);
    self.btn_comment.frame = CGRectMake(self.btn_comment.frame.origin.x,height+70, self.btn_comment.frame.size.width, self.btn_comment.frame.size.height);
    self.btn_photo_option.frame = CGRectMake(self.btn_photo_option.frame.origin.x,height+70, self.btn_photo_option.frame.size.width, self.btn_photo_option.frame.size.height);
    
}

-(void)dealloc{
    [lbl_comments release];
    [btn_view_all_comments release];
    [dlstarObj release];
    [img_desc release];
    [view_like_btn release];
    [img_likes release];
    [lbl_desc release];
    [img_big release];
    [btn_likes_count release];
    [btn_like release];
    [btn_comment release];
    [btn_photo_option release];
    [super dealloc];
}

@end
