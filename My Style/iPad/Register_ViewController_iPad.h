//
//  Register_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BVUnderlineButton.h"
#import "EGOImageButton.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "MKInfoPanel.h"
#import <Twitter/Twitter.h>
#import "Twitter_login_ViewController.h"
#import "Facebook_Find_Friends_ViewController.h"
#import "webview_viewcontroller_iPad.h"


@interface Register_ViewController_iPad : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,AmazonServiceRequestDelegate>{
    
    UIPopoverController *popover;
    
    webview_viewcontroller_iPad *webviewObj;
    BVUnderlineButton *btn_privacy_policy;
    BVUnderlineButton *btn_terms_of_service;
    EGOImageButton *btn_add_photo;
    
    UIImagePickerController *photoPickerController;
    
    NSString *image_url;
    
    UITextField *txt_username;
    UITextField *txt_password;
    UITextField *txt_email;
    UITextField *txt_name;
    UITextField *txt_phone_no;
    UIView *view_error;
    
    UIButton *btn_facebook_info;
    
    UIScrollView *scrollview;
    Twitter_login_ViewController *twitter_login;
    
    UIImageView *img_first_cell_bg;
    UIImageView *img_second_cell_bg;
}
@property(nonatomic,retain)UIPopoverController *popover;
@property(nonatomic,retain)IBOutlet UIImageView *img_first_cell_bg;
@property(nonatomic,retain)IBOutlet UIImageView *img_second_cell_bg;
@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;
@property(nonatomic,retain)IBOutlet  UIButton *btn_facebook_info;
@property(nonatomic,retain)IBOutlet UIView *view_error;
@property(nonatomic,retain) NSString *image_url;
@property(nonatomic,retain)UIImagePickerController *photoPickerController;

@property(nonatomic,retain)IBOutlet BVUnderlineButton *btn_privacy_policy;
@property(nonatomic,retain)IBOutlet BVUnderlineButton *btn_terms_of_service;
@property(nonatomic,retain)IBOutlet EGOImageButton *btn_add_photo;

@property(nonatomic,retain)IBOutlet UITextField *txt_username;
@property(nonatomic,retain)IBOutlet UITextField *txt_password;
@property(nonatomic,retain)IBOutlet UITextField *txt_email;
@property(nonatomic,retain)IBOutlet UITextField *txt_name;
@property(nonatomic,retain)IBOutlet UITextField *txt_phone_no;
@property(nonatomic,retain)webview_viewcontroller_iPad *webviewObj;
@property (nonatomic, retain) AmazonS3Client *s3;

@end
