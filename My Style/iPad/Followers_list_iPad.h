//
//  Followers_list_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Followers_list_cell_iPad.h"
#import "Faceabook_find_friend_Share.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"

@interface Followers_list_iPad : UIViewController{

    NSString *user_id;
    NSString *nav_title;
    UILabel *lbl_title;
    
    UITableView *tbl_followers;
    NSMutableArray *array_folloewrs;
    // User_info_ViewController *user_info_view;
}
//@property(nonatomic,retain)User_info_ViewController *user_info_view;
@property(nonatomic,retain)NSString *nav_title;
@property(nonatomic,retain)NSString *user_id;
@property(nonatomic,retain)IBOutlet  UILabel *lbl_title;
@property(nonatomic,retain)IBOutlet UITableView *tbl_followers;
@property(nonatomic,retain) NSMutableArray *array_folloewrs;

@end
