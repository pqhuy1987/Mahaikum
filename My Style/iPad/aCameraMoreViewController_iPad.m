//
//  aCameraMoreViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "aCameraMoreViewController_iPad.h"
#import "Share_photo_ViewController.h"

#import "GPUImage.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "DemoImageEditor_iPad.h"

#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"
#import "AFPhotoEditorControllerOptions.h"
#import "AFPhotoEditorController.h"

@interface aCameraMoreViewController_iPad (){

    ELCAlbumPickerController *albumController;
    ELCImagePickerController *elcPicker;
}
@property(nonatomic,retain) DemoImageEditor_iPad *imageEditor;
@property(nonatomic,retain) ALAssetsLibrary *library;

@property (nonatomic, retain) ALAssetsLibrary *specialLibrary;


@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *btn_take_photo;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *btn_cancel;;
@property (nonatomic,retain) IBOutlet UIBarButtonItem *btn_photo_library;
@property (nonatomic,retain) IBOutlet UIButton *btn_photo_library1;

@end

static int count=0;

@implementation aCameraMoreViewController_iPad

@synthesize collectionViewObj;

@synthesize imageView;
@synthesize workingImage;
@synthesize sourceImage;
@synthesize scrollView,share_photo_viewObj;
@synthesize btn_done,btn_camera,btn_photo_libary,btn_photo_library1,imagepicker;

@synthesize editorViewController;

@synthesize library = _library;
@synthesize imageEditor = _imageEditor;

-(IBAction)btn_back_clike:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)gotohome {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)go_to_home_view1:(NSNotification *)notification {
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
//    [self dismissViewControllerAnimated:NO completion:nil];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(gotohome) userInfo:self repeats:NO];
}

-(void)viewDidLoad {
    flag=0;
     _view_photo_lib.hidden =YES;
    //    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"transprent.png"]];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    _view_photo_lib.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.share_photo_viewObj=[[Share_photo_ViewController_iPad alloc]initWithNibName:@"Share_photo_ViewController_iPad" bundle:nil];
    [self createMenuWithButtonSize:CGSizeMake(60.0, 55.0) withOffset:10.0f noOfButtons:19];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"go_to_home_view_from_share" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_home_view1:) name:@"go_to_home_view_from_share" object:nil];
    
    
    
    albumController = [[ELCAlbumPickerController alloc] initWithNibName: nil bundle: nil];
	elcPicker = [[ELCImagePickerController alloc] initWithRootViewController:albumController];
    [albumController setParent:elcPicker];
	[elcPicker setDelegate:self];
    
    self.imagepicker = [[UIImagePickerController alloc] init];
    self.imagepicker.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        self.imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [[NSBundle mainBundle] loadNibNamed:@"OverlayView" owner:self options:nil];
        self.overlayView.frame = self.imagepicker.cameraOverlayView.frame;
        
        self.imagepicker.cameraOverlayView = self.overlayView;
        self.overlayView = nil;
        
        self.imagepicker.allowsEditing = NO;
        self.imagepicker.showsCameraControls = NO;
        self.imagepicker.wantsFullScreenLayout = NO;
    }else{
        self.imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    
    self.library = [[[ALAssetsLibrary alloc] init] autorelease];
    self.imageEditor = [[[DemoImageEditor_iPad alloc] initWithNibName:@"DemoImageEditor_iPad" bundle:nil] autorelease];
    self.imageEditor.checkBounds = YES;
    
    self.imageEditor.doneCallback = ^(UIImage *editedImage, BOOL canceled){
        if (canceled) {
          //  [self.navigationController popViewControllerAnimated:YES];
           // [elcPicker popViewControllerAnimated:YES];
          //  [elcPicker popToRootViewControllerAnimated:YES];
            
        }else{
            self.sourceImage = editedImage;
            self.workingImage = editedImage;
            [self.imageView setImage:self.sourceImage];
            // [self dismissViewControllerAnimated:YES completion:nil];
            [self dismissViewControllerAnimated:NO
                                     completion:^{
                                         [self displayEditorForImage:self.workingImage];
                                     }];
        }
    };
    
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
}

#pragma mark - new image picker
-(void)get_image_from_custom_photo_picker {

    
    @try {
        [self presentViewController:elcPicker animated:NO completion:nil];
    }
    @catch (NSException * ex) {
        //“Pushing the same view controller instance more than once is not supported”
        //NSInvalidArgumentException
        NSLog(@"Exception: [%@]:%@",[ex  class], ex );
        NSLog(@"ex.name:'%@'", ex.name);
        NSLog(@"ex.reason:'%@'", ex.reason);
        //Full error includes class pointer address so only care if it starts with this error
        NSRange range = [ex.reason rangeOfString:@"Pushing the same view controller instance more than once is not supported"];
        
        if([ex.name isEqualToString:@"NSInvalidArgumentException"] &&
           range.location != NSNotFound)
        {
            //view controller already exists in the stack - just pop back to it
          //  [self.navigationController popToViewController:tabbar animated:NO];
           // [self presentViewController:elcPicker animated:NO completion:nil];
        }else{
            NSLog(@"ERROR:UNHANDLED EXCEPTION TYPE:%@", ex);
        }
    }
    @finally {
        NSLog(@"finally");

    }
    
     

  //  [elcPicker release];
  //  [albumController release];
}

#pragma mark ELCImagePickerControllerDelegate Methods
-(void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info {
   
    
	
	for(NSDictionary *dict in info) {
        
        self.imageEditor.sourceImage = [dict objectForKey:UIImagePickerControllerOriginalImage];
        self.imageEditor.previewImage = [dict objectForKey:UIImagePickerControllerOriginalImage];
        [self.imageEditor reset:NO];
        
        [picker setNavigationBarHidden:YES animated:NO];
        
        @try {
            [picker pushViewController:self.imageEditor animated:YES];
        }
        @catch (NSException * ex) {
            NSLog(@"Exception: [%@]:%@",[ex  class], ex );
            NSLog(@"ex.name:'%@'", ex.name);
            NSLog(@"ex.reason:'%@'", ex.reason);
            
            NSRange range = [ex.reason rangeOfString:@"Pushing the same view controller instance more than once is not supported"];
            
            if([ex.name isEqualToString:@"NSInvalidArgumentException"] &&
               range.location != NSNotFound)
            {
                //view controller already exists in the stack - just pop back to it
                  [picker popToViewController:self.imageEditor animated:NO];
            }else{
                NSLog(@"ERROR:UNHANDLED EXCEPTION TYPE:%@", ex);
            }
        }
        @finally {
            NSLog(@"finally");
            
        }
	}

}

-(void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [imageView setImage:workingImage];
    self.sourceImage =self.workingImage;
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:YES];
    if (flag==0) {
        flag =1;
        
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self get_image_from_custom_photo_picker];
        }else{
            
            [self presentViewController:self.imagepicker animated:NO completion:nil];
            
        }
    }
}

#pragma mark - get all Photos from Lib

-(void)getAllPictures {
    [self start_activity];
    
    mutableArray =[[NSMutableArray alloc]init];
    NSMutableArray* assetURLDictionaries = [[NSMutableArray alloc] init];
    
    library = [[ALAssetsLibrary alloc] init];
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
                [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url= (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url
                         resultBlock:^(ALAsset *asset) {
                             [mutableArray addObject:[UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]]];
                             
                             if ([mutableArray count]==count)
                             {
                                 [self allPhotosCollected];
                             }
                         }
                        failureBlock:^(NSError *error){ NSLog(@"operation was not successfull!"); } ];
                
            }
        }
    };
    
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            count=[group numberOfAssets];
        }
    };
    
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) {NSLog(@"There is an error");}];
}

-(void)allPhotosCollected {
    _view_photo_lib.hidden =NO;
    [self.collectionViewObj reloadData];
    [self stop_activity];
}

#pragma mark - UICollectionView
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section; {
    return mutableArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {            return CGSizeMake(180,180);
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath; {
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    

    cell.img_photo.image =[mutableArray objectAtIndex:indexPath.row];
    cell.tag = indexPath.row;
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    _view_photo_lib.hidden =YES;

    self.imageEditor.sourceImage = [mutableArray objectAtIndex:indexPath.row];
    self.imageEditor.previewImage = [mutableArray objectAtIndex:indexPath.row];
    [self.imageEditor reset:NO];
    
    
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.imageEditor];
    [self presentViewController:nav animated:NO completion:nil];
}

//////////// SHAREKIT ////////////
- (IBAction) SendingImage:(id) sender {
    
    if(self.workingImage){
        
        self.share_photo_viewObj.img_photo =self.workingImage;
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.share_photo_viewObj];
        [self presentViewController:nav animated:YES completion:nil];
        
    }
}

//////////// TAKE PICTURE ////////////
#pragma mark - UIImagePicker delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
//    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
//    NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
//    
//    [self.library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
//        UIImage *preview = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
//        
//        self.imageEditor.sourceImage = image;
//        self.imageEditor.previewImage = preview;
//        [self.imageEditor reset:NO];
//        
//        
//        [picker pushViewController:self.imageEditor animated:YES];
//        [picker setNavigationBarHidden:YES animated:NO];
//        
//    } failureBlock:^(NSError *error) {
//        NSLog(@"Failed to get asset from library");
//    }];
    
    // if you use no Edited controller then uncomment next line
    self.sourceImage = info[UIImagePickerControllerEditedImage];
    self.workingImage = info[UIImagePickerControllerEditedImage];
    if (!info[UIImagePickerControllerEditedImage]) {
        self.sourceImage = info[UIImagePickerControllerOriginalImage];
        self.workingImage = info[UIImagePickerControllerOriginalImage];
    }
    // if use Edited image .. uncomment next 2 lines / comment previous line
    //self.sourceImage = [info objectForKey:UIImagePickerControllerEditedImage];  // i put sourceImage
    //self.workingImage = [info objectForKey:UIImagePickerControllerEditedImage];  // i put workingImage
    
    
    
    [self.imageView setImage:self.sourceImage];
    [picker dismissViewControllerAnimated:YES completion:^ {
        [self displayEditorForImage:self.sourceImage];
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

-(IBAction)takeImageFromCamera:(id)sender {
	// i get image from iphone Camera -> Dont work in simulator, only in iphone
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        self.imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagepicker animated:YES completion:nil];
        // [self presentModalViewController:imgPicker animated:YES];
    }
    
}

-(IBAction)chooseImageFromAlbum:(id) sender {
    
    [self get_image_from_custom_photo_picker];
    /*
    self.imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagepicker animated:YES completion:nil];
     */
}

//////////// FILTERS ////////////
-(void)MakeFilter:(id)sender {
    
    
    NSLog(@"MakeFilter called");
    UIButton *button=(UIButton *)sender;
    if (!self.sourceImage==0){           // only i have a image loaded (camera or album)
        
        // Original image
        if (button.tag==0){
            if (!self.sourceImage==0){  // only i have a image loaded (camera or album)
                [self.imageView setImage:self.sourceImage];
                self.workingImage=self.sourceImage;
            }
        }
        
        
        // Grey Scale
        if (button.tag == 1) {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageGrayscaleFilter *stillImageFilter2 = [[GPUImageGrayscaleFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
        }
        // RED Filter
        if (button.tag == 2)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"red"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
        }
        // GREEN Filter
        if (button.tag == 3)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"green"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
            
        }
        // BLUE Filter
        if (button.tag == 4)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"blue"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
        }
        // AMARO Filter
        if (button.tag == 5)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"amaro"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
        }
        // AQUA Filter
        if (button.tag == 6)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"aqua"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
        }
        // DOUBLECROSS Filter
        if (button.tag == 7)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"doublecross"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
            
        }
        // GREENPURPLE Filter
        if (button.tag == 8)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"greenpurple"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
        }
        // RISE Filter
        if (button.tag == 9)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"rise"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
        }
        // VINTAGE Filter
        if (button.tag == 10)
        {
            
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToneCurveFilter *stillImageFilter2 = [[GPUImageToneCurveFilter alloc] initWithACV:@"vintage"];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
            
        }
        // GPUImageSepiaFilter
        if (button.tag == 11)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageSepiaFilter *stillImageFilter2 = [[GPUImageSepiaFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImagePixellateFilter
        if (button.tag == 12)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImagePixellateFilter *stillImageFilter2 = [[GPUImagePixellateFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        // GPUImagePolarPixellateFilter
        if (button.tag == 13)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImagePolarPixellateFilter *stillImageFilter2 = [[GPUImagePolarPixellateFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        // GPUImageCrosshatchFilter
        if (button.tag == 14)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageCrosshatchFilter *stillImageFilter2 = [[GPUImageCrosshatchFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        
        // GPUImageSketchFilter
        if (button.tag == 15)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageSketchFilter *stillImageFilter2 = [[GPUImageSketchFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageToonFilter
        if (button.tag == 16)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageToonFilter *stillImageFilter2 = [[GPUImageToonFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageSmoothToonFilter
        if (button.tag == 17)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageSmoothToonFilter *stillImageFilter2 = [[GPUImageSmoothToonFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageEmbossFilter
        if (button.tag == 18)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageEmbossFilter *stillImageFilter2 = [[GPUImageEmbossFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImagePosterizeFilter
        if (button.tag == 19)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImagePosterizeFilter *stillImageFilter2 = [[GPUImagePosterizeFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageSwirlFilter
        if (button.tag == 20)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageSwirlFilter *stillImageFilter2 = [[GPUImageSwirlFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageBulgeDistortionFilter
        if (button.tag == 21)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageBulgeDistortionFilter *stillImageFilter2 = [[GPUImageBulgeDistortionFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImagePinchDistortionFilter
        if (button.tag == 22)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImagePinchDistortionFilter *stillImageFilter2 = [[GPUImagePinchDistortionFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageStretchDistortionFilter
        if (button.tag == 23)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageStretchDistortionFilter *stillImageFilter2 = [[GPUImageStretchDistortionFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
        // GPUImageVignetteFilter
        if (button.tag == 24)
        {
            self.workingImage=self.sourceImage; // copy sourceImage to workingImage
            GPUImageVignetteFilter *stillImageFilter2 = [[GPUImageVignetteFilter alloc] init];
            UIImage *quickFilteredImage = [stillImageFilter2 imageByFilteringImage:self.workingImage];
            self.workingImage = quickFilteredImage;
            // setImage to UIImage
            [self.imageView setImage:self.workingImage];
        }
        
    } // if (!self.sourceImage==nil){
    NSLog(@"button clicked is : %iBut \n\n",button.tag);
    
    
    
}

//////////// SLIDER MENU ////////////
-(void)createMenuWithButtonSize:(CGSize)buttonSize withOffset:(CGFloat)offset noOfButtons:(int)totalNoOfButtons {
    for (int i = 0; i < totalNoOfButtons; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(MakeFilter:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.cornerRadius =3.0f;
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"f%i.JPG",i]] forState:UIControlStateNormal];//with image
        
        //OR
        //[button setTitle:[NSString stringWithFormat:@"%iBut",i] forState:UIControlStateNormal];//with title
        button.frame = CGRectMake(i*(offset+buttonSize.width), 8.0, buttonSize.width, buttonSize.height);
        button.clipsToBounds = YES;
        button.showsTouchWhenHighlighted=YES;
        button.tag=i;
        [self.scrollView addSubview:button];
    }
    self.scrollView.contentSize=CGSizeMake((buttonSize.width + offset) * totalNoOfButtons, buttonSize.height);
}

#pragma mark - Camera
-(void)open_photo_library{
    [self get_image_from_custom_photo_picker];
}

-(IBAction)btn_photo_library_click:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(open_photo_library) userInfo:self repeats:NO];
}

-(IBAction)btn_cancle_click:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }];
}

-(IBAction)takePhoto:(id)sender {
    [self.imagepicker takePicture];
    
    CustomImageEditor *obj =[[CustomImageEditor alloc]initWithNibName:@"CustomImageEditor" bundle:nil];
    obj.image=self.sourceImage;
    [self presentViewController:obj animated:YES completion:^{
        
    }];
}

//////////// OTHER ////////////
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
    return (orientation == UIInterfaceOrientationLandscapeLeft ||
            orientation == UIInterfaceOrientationLandscapeRight);
}

#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}

-(void)stop_activity{
    [DejalBezelActivityView removeView];
}

#pragma mark Aviary SDK Delegate
- (void)displayEditorForImage:(UIImage *)imageToEdit {
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit];
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:nil];
//    //kAFStickers
//    //kAFCrop
//    NSArray *tools =[NSArray arrayWithObjects:kAFEffects,kAFOrientation,kAFBrightness,kAFContrast,kAFSaturation,kAFSharpness,kAFDraw,kAFText,kAFRedeye,kAFWhiten,kAFBlemish,nil];
//    
//    //      NSArray *tools = [NSArray arrayWithObjects:kAFEnhance, kAFEffects,  kAFOrientation,  kAFBrightness, kAFContrast, kAFSaturation, kAFSharpness,  nil];
//    //   NSArray *tools = [NSArray arrayWithObjects:kAFEffects,  kAFOrientation, nil];
//    
//    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit options:[NSDictionary dictionaryWithObject:tools forKey:kAFPhotoEditorControllerToolsKey]];
//    
//    self.editorViewController = editorController;////////////
//    
//    [editorController setDelegate:self];
//    
//    AFPhotoEditorStyle *style = [editorController style];
//    //  UIColor *accentColor = [UIColor colorWithRed:87/255.0 green:165/255.0 blue:232/255.0 alpha:1.0];
//    UIColor *accentColor = [UIColor colorWithRed:37.0f/255.0f green:90.0f/255.0f blue:130.0f/255.0f alpha:1.0];
//    
//    [style setAccentColor:accentColor];
//    
//    style.topBarLeftButtonBackgroundColor = accentColor;
//    style.topBarBackgroundColor = accentColor;
//    style.topBarLeftButtonTextColor = [UIColor whiteColor];
//    style.topBarTextColor = [UIColor whiteColor];
//    
//    editorViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//    editorViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
//    
//    //  NSLog(@"%@", editorController.view.subviews);
//    for (id obj in editorController.view.subviews)
//    {
//        
//        NSLog(@"%@",NSStringFromClass([obj class]));
//        NSString    *className = NSStringFromClass([obj class]);
//        /*
//         if ([className isEqualToString:@"UIView"]) {
//         
//         UIView *view =(UIView *)obj;
//         for (id obj1 in view.subviews) {
//         NSString    *className1 = NSStringFromClass([obj1 class]);
//         if ([className1 isEqualToString:@"AFHomePageControl"]) {
//         
//         UIPageControl *page =(UIPageControl *)obj1;
//         page.numberOfPages =2;
//         if ([page isKindOfClass:[UIPageControl class]] ) {
//         NSLog(@"we also ");
//         }
//         if ([page isMemberOfClass:[UIPageControl class]] ) {
//         NSLog(@"we also too");
//         }
//         }
//         }
//         
//         NSLog(@"%@",view.subviews);
//         }
//         */
//        if ([className isEqualToString:@"AFNavigationBar"])
//        {
//            
//            UINavigationBar *nav = (UINavigationBar*)obj;
//            
//            nav.topItem.title = @"My Style";
//            
//            UIImageView *imgVIew = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,146,32)];
//            imgVIew.image = [UIImage imageNamed:@"bg_shaare.png"];
//            
//            nav.topItem.titleView = imgVIew;
//            
//        }
//        
//        if ([obj isKindOfClass:[UIView class]])
//        {
//            UIView* vc = (UIView*)obj;
//            
//            for (id sub_obj in vc.subviews)
//            {
//                if ([sub_obj isKindOfClass:[UIImageView class]])
//                {
//                    UIImageView *imgView = (UIImageView*)sub_obj;
//                    imgView.image = [UIImage imageNamed:@""];
//                }
//                
//                
//                //                if ([sub_obj isKindOfClass:[UIScrollView class]])
//                //                {
//                //                    UIScrollView *scrView = (UIScrollView*)sub_obj;
//                //
//                //                    scrView.contentSize = CGSizeMake(640,scrView.contentSize.height);
//                //
//                //
//                //                }
//            }
//            
//        }
//    }
//    
//    // [self presentViewController:editorController animated:NO completion:nil];
//    [self presentViewController:editorController animated:NO completion:^{
//        
//    }];
}

-(void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image {
    [editor dismissViewControllerAnimated:YES completion:^{
        dispatch_async( dispatch_get_main_queue(),
                       ^{
                         
                           if(self.workingImage){
                               
                               self.share_photo_viewObj.img_photo =self.workingImage;
                               UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.share_photo_viewObj];
                               [self presentViewController:nav animated:YES completion:nil];
                               
                           }

                           self.imageView.image=image;
                           self.workingImage=image;
                       });
    }];
    
}

-(void)photoEditorCanceled:(AFPhotoEditorController *)editor {
    [editor dismissViewControllerAnimated:YES completion:^{
        dispatch_async( dispatch_get_main_queue(),
                       ^{
                           if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                               self.imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                               [self presentViewController:self.imagepicker animated:NO completion:nil];
                           }
                           else {
                               [self presentViewController:self.imagepicker animated:NO completion:nil];
                           }
                       });
    }];
    
}

//- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image
//{
//    // result image
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                           //   share_photo_viewObj.img_photo =image;
//                           flag=0;
//                           self.workingImage =image;
//                           [imageView setImage:workingImage];
//                           self.sourceImage =self.workingImage;
//                           self.share_photo_viewObj.img_photo =self.workingImage;
//                           UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:share_photo_viewObj];
//                           [self presentViewController:nav animated:YES completion:nil];
//                           
//                           
//                           
//                           
//                       });
//    }];
//    
//}
//
//- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
//{
//    
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                           if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//                           {
//                               [self get_image_from_custom_photo_picker];
//                           }else{
//                               
//                               [self presentViewController:self.imagepicker animated:NO completion:nil];
//                               
//                           }
//                       });
//    }];
//    
//}


#pragma mark - Edit image click
-(IBAction)btn_edit_click:(id)sender{
    if(self.workingImage) {
        [self displayEditorForImage:self.workingImage];
        
    }
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	self.imageView = nil;
}


- (void)dealloc {
    [_library release];
    [_imageEditor release];
    
    [imagepicker release];
    [btn_done release];
    [btn_camera release];
    [btn_photo_libary release];
    
    [share_photo_viewObj release];
	[workingImage release];
    [sourceImage release];
	[imageView release];
    [super dealloc];
}

@end
