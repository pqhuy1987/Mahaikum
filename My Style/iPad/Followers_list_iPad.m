//
//  Followers_list_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Followers_list_iPad.h"
#import "User_info_ViewController_iPad.h"


@interface Followers_list_iPad (){

    User_info_ViewController_iPad *user_info_view;
}

@end

@implementation Followers_list_iPad

@synthesize user_id,nav_title,lbl_title;
@synthesize  tbl_followers,array_folloewrs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.array_folloewrs =[[NSMutableArray alloc]init];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"60" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(get_followers_list_responce:) name:@"60" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-60" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-60" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"61" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFollowResponce:) name:@"61" object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"-61" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailReson:) name:@"-61" object:nil];
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.lbl_title.text = self.nav_title;
    [self get_followers_list];
    
}

-(void)get_followers_list{
    
    //   http://www.techintegrity.in/mystyle/get_user_followers.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=11
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&profile_handle=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],self.user_id];
    
    if ([self.nav_title isEqualToString:@"FOLLOWERS"] || [self.nav_title isEqualToString:@"Followers"]||[self.nav_title isEqualToString:@"followers"]) {
        NSString *requestStr = [NSString stringWithFormat:@"%@get_user_followers.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems :requestStr:@"60":nil];

    }else{
        
        NSString *requestStr = [NSString stringWithFormat:@"%@get_user_following.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems :requestStr:@"60":nil];
        
    }
    
    
}

-(void)get_followers_list_responce:(NSNotification *)notification{
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_folloewrs removeAllObjects];
        
        NSMutableArray *array =[result valueForKey:@"data"];
        NSLog(@"%@",NSStringFromClass([[array objectAtIndex:0] class]));
        for(NSDictionary *dict in array){
            
            Faceabook_find_friend_Share *shareObj =[[Faceabook_find_friend_Share alloc]init];
            shareObj.user_id =[dict valueForKey:@"id"];
            shareObj.username =[dict valueForKey:@"username"];
            shareObj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            shareObj.facebook_id =[dict valueForKey:@"facebook_id"];
            shareObj.url_image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
            shareObj.is_following =[dict valueForKey:@"user_following"];
            [self.array_folloewrs addObject:shareObj];
            [shareObj release];
            
            NSLog(@"%@",[dict valueForKey:@"username"]);
        }
        [self.tbl_followers reloadData];
    }
    
}

-(void)FailNewsReson:(NSNotification *)notification {
	//[self stopSpinner];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_folloewrs.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Followers_list_cell_iPad";
	Followers_list_cell_iPad *cell = (Followers_list_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Followers_list_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        cell.img_user.layer.cornerRadius = 4.0f;
        cell.img_user.layer.masksToBounds = YES;
        
        
        cell.img_user.
        [cell.btn_follow addTarget:self action:@selector(btn_follow_click:) forControlEvents:UIControlEventTouchUpInside];
        
	}
    cell.btn_follow.tag=indexPath.row;
    
    Faceabook_find_friend_Share *shareObj =[self.array_folloewrs objectAtIndex:indexPath.row];

    
    if(shareObj.url_image.length !=0){
        [cell.img_user setImageWithURL:[NSURL URLWithString:shareObj.url_image] placeholderImage:[UIImage imageNamed:@"default_user_image.jpg"]];
    }
    
    cell.lbl_username.text = shareObj.username;
    
    if (shareObj.name.length !=0) {
        cell.lbl_name.text =shareObj.name;
    }else{
        cell.lbl_name.text=@"";
    }
    
    if ([shareObj.is_following isEqualToString:@"yes"]) {
        [cell.btn_follow setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
    }else{
        [cell.btn_follow setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
    }
    

    if([shareObj.user_id isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID]]){
        [cell.btn_follow setHidden:YES];

    }else{
        [cell.btn_follow setHidden:NO];

    }
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Faceabook_find_friend_Share *shareObj =[self.array_folloewrs objectAtIndex:indexPath.row];
    user_info_view.user_id=shareObj.user_id;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark Follow Click
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    
    
    
    int tag =btn.tag;
    
    Faceabook_find_friend_Share *shareObj =[self.array_folloewrs objectAtIndex:tag];
    //post user follow
    //http://www.techintegrity.in/mystyle/post_user_following.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=1&following_id=2
    // http://www.techintegrity.in/mystyle/post_user_unfollow.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=1&following_id=2
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&following_id=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],shareObj.user_id];
    if ([shareObj.is_following isEqualToString:@"no"]) {
        
        NSString *requestStr = [NSString stringWithFormat:@"%@post_user_following.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems :requestStr:@"61":nil];
        shareObj.is_following=@"yes";
    }else{
        NSString *requestStr = [NSString stringWithFormat:@"%@post_user_unfollow.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        NSLog(@"%@",requestStr);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems :requestStr:@"61":nil];
        shareObj.is_following=@"no";
    }
    [self.array_folloewrs replaceObjectAtIndex:tag withObject:shareObj];
    [self.tbl_followers reloadData];
    
    
}

-(void)getFollowResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    //akshay
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }
    
}
-(void)FailReson:(NSNotification *)notification {
	//[self stopSpinner];
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"There was a problem with the Internet connection. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [lbl_title release];
    [nav_title release];
    [tbl_followers release];
    [array_folloewrs release];
    [user_id release];
    [super dealloc];
}
@end
