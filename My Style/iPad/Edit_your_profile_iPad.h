//
//  Edit_your_profile_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "BlockActionSheet.h"
#import "Singleton.h"
#import "EGOImageButton.h"
#import "ActionSheetStringPicker.h"
#import "Edit_your_profile_share.h"
#import "Change_password_iPad.h"

#import "DCRoundSwitch.h"

@interface Edit_your_profile_iPad : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,AmazonServiceRequestDelegate>{
    
    
    
    UIPopoverController *popover;

    UIScrollView *scrollview;
    
    UIImageView *img_first_cell;
    UIImageView *img_second_cell;
    UIImageView *img_third_cell;
    UIImageView *img_fourth_cell;
    EGOImageButton *img_photo;
    
    UITextField *txt_name;
    UITextField *txt_username;
    UITextField *txt_weburl;
    UITextField *txt_bio;
    UITextField *txt_email;
    UITextField *txt_phone;
    UIButton *btn_gender;
    
    UIImagePickerController *photoPickerController;
    NSString *image_url;
    
    Edit_your_profile_share *profile_shareObj;
    
    int is_reload_data;
    DCRoundSwitch *switch_private;
    int is_show_alert;
}
@property(nonatomic,assign)int is_reload_data;
@property(nonatomic,assign)int is_show_alert;

@property(nonatomic,retain)UIImagePickerController *photoPickerController;
@property(nonatomic,retain) NSString *image_url;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;
@property(nonatomic,retain)IBOutlet UIImageView *img_first_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_second_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_third_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_fourth_cell;

@property(nonatomic,retain)IBOutlet  EGOImageButton *img_photo;

@property(nonatomic,retain)IBOutlet UITextField *txt_name;
@property(nonatomic,retain)IBOutlet UITextField *txt_username;
@property(nonatomic,retain)IBOutlet UITextField *txt_weburl;
@property(nonatomic,retain)IBOutlet UITextField *txt_bio;
@property(nonatomic,retain)IBOutlet UITextField *txt_email;
@property(nonatomic,retain)IBOutlet UITextField *txt_phone;
@property(nonatomic,retain)IBOutlet UIButton *btn_gender;
@property(nonatomic,retain)Edit_your_profile_share *profile_shareObj;

@property (nonatomic, retain) IBOutlet DCRoundSwitch *switch_gender;
@property (nonatomic, retain) IBOutlet DCRoundSwitch *switch_private;
@property (nonatomic,retain)UIPopoverController *popover;
@property (nonatomic, retain) AmazonS3Client *s3;
@end
