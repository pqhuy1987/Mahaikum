//
//  Show_location_list_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Show_location_list_iPad.h"
#import <RestKit/RestKit.h>
#import "Venue.h"
#import "VenueCell.h"

 #define kCLIENTID "4W5PKA2BSZEBSMMRVVWIIYGBJ1UC0QOVTP2OJ3A2FOCIVGMD"
 #define kCLIENTSECRET "EE4EME2HIYHO5R0VWE3CMCF3N4ISBWSJRDSKARROERKZAGYS"
 
/*
#define kCLIENTID "UYEZ03WPSC1R4G5O0TLU3YPO1X44H1AUIM5L0PTZL11DMINT"
#define kCLIENTSECRET "W1LCWSMOEJ5KIQFY1Q3530G3G3B5HUC0MABLX03UFVEQJ41J"
*/

@interface Show_location_list_iPad ()
@property (strong, nonatomic) NSArray *data;
@end

@implementation Show_location_list_iPad
@synthesize data,tbl_location;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    RKURL *baseURL = [RKURL URLWithBaseURLString:@"https://api.foursquare.com/v2"];
    RKObjectManager *objectManager = [RKObjectManager objectManagerWithBaseURL:baseURL];
    objectManager.client.baseURL = baseURL;
    
    RKObjectMapping *venueMapping = [RKObjectMapping mappingForClass:[Venue class]];
    [venueMapping mapKeyPathsToAttributes:@"id", @"venueID", @"name", @"name", nil];
    [objectManager.mappingProvider setMapping:venueMapping forKeyPath:@"response.venues"];
    
    RKObjectMapping *locationMapping = [RKObjectMapping mappingForClass:[Location class]];
    [locationMapping mapKeyPathsToAttributes:@"address", @"address", @"city", @"city", @"country", @"country", @"crossStreet", @"crossStreet", @"postalCode", @"postalCode", @"state", @"state", @"distance", @"distance", @"lat", @"lat", @"lng", @"lng", nil];
    
    [venueMapping mapRelationship:@"location" withMapping:locationMapping];
    [objectManager.mappingProvider setMapping:locationMapping forKeyPath:@"location"];
    
    RKObjectMapping *statsMapping = [RKObjectMapping mappingForClass:[Stats class]];
    [statsMapping mapKeyPathsToAttributes:@"checkinsCount", @"checkins", @"tipCount", @"tips", @"usersCount", @"users", nil];
    
    [venueMapping mapRelationship:@"stats" withMapping:statsMapping];
    [objectManager.mappingProvider setMapping:statsMapping forKeyPath:@"stats"];
    
    [self sendRequest];
    
    UIView *tblheader =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768,112)];
    tblheader.backgroundColor =[UIColor clearColor];
    UIImageView *img_header =[[UIImageView alloc]initWithFrame:CGRectMake(165, 0, 438, 110)];
    img_header.image = [UIImage imageNamed:@"poweredByFoursquare_gray.png"];
    [tblheader addSubview:img_header];
    
    UIView *tbl_header_line =[[UIView alloc]initWithFrame:CGRectMake(0,110,768,1)];
    tbl_header_line.backgroundColor =[UIColor colorWithRed:37.0f/255.0f green:90.0f/255.0f blue:130.0f/255.0f alpha:1];
    [tblheader addSubview:tbl_header_line];
    self.tbl_location.tableHeaderView = tblheader;
}

-(IBAction)btn_refresh_click:(id)sender{
    [self sendRequest];
}
- (void)sendRequest
{
    [self start_activity];
    
    // NSString *theDate = @"20130507";
    //  NSString *latLon = @"37.33,-122.03";
    NSString *theDate = @"20130702";
    NSString *latLon = [NSString stringWithFormat:@"%f, %f",[[Singleton sharedSingleton]getCurrentLat],[[Singleton sharedSingleton]getCurrentLong]];
    //  NSString *latLon = @"22.3000,70.7800";
    NSString *client_id = [NSString stringWithUTF8String:kCLIENTID];
    NSString *client_secret = [NSString stringWithUTF8String:kCLIENTSECRET];
    //  NSString *category_id = @"4bf58dd8d48988d1e0931735";
    NSString *category_id = @"";
    
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:latLon, @"ll", client_id, @"client_id", client_secret, @"client_secret", category_id, @"categoryId", theDate, @"v", nil];
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    
    RKURL *URL = [RKURL URLWithBaseURL:[objectManager baseURL] resourcePath:@"/venues/search" queryParameters:queryParams];
    
//    NSLog(@"URL: %@", [URL absoluteString]);
//    NSLog(@"resourcePath: %@", [URL resourcePath]);
//    NSLog(@"query: %@", [URL query]);
    
    [objectManager loadObjectsAtResourcePath:[NSString stringWithFormat:@"%@?%@", [URL resourcePath], [URL query]] delegate:self];
}

#pragma mark - RKObjectLoaderDelegate

- (void)request:(RKRequest*)request didLoadResponse:(RKResponse*)response {
  //  NSLog(@"response code: %d", [response statusCode]);
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", [error localizedDescription]);
    [self stop_activity];
}

- (void)objectLoader:(RKObjectLoader *)objectLoader didLoadObjects:(NSArray *)objects
{
 //   NSLog(@"objects[%d]", [objects count]);
    data = objects;
    
    [self.tbl_location reloadData];
    [self stop_activity];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%d",data.count);
    
    return data.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return 100.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier =@"VenueCell_iPad";
    VenueCell *cell=(VenueCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"VenueCell_iPad" owner:self options:nil];
        cell=[nib objectAtIndex:0];
        cell.showsReorderControl = NO;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
    }
    NSLog(@"%d",data.count);
    
    Venue *venue = [data objectAtIndex:indexPath.row];
    cell.nameLabel.text = [venue.name length] > 25 ? [venue.name substringToIndex:25] : venue.name;
    cell.lbl_dict.text = [venue.location.address length]>0 ? venue.location.address : @"";
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Venue *venue = [data objectAtIndex:indexPath.row];
    
    NSLog(@"%@",venue.location.lat);
    NSLog(@"%@",venue.location.lng);
    
    [[Singleton sharedSingleton]setLocationname:venue.name];
    [[Singleton sharedSingleton]setLocation_lat:[NSString stringWithFormat:@"%@",venue.location.lat]];
    [[Singleton sharedSingleton]setLocation_lng:[NSString stringWithFormat:@"%@",venue.location.lng]];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}
-(void)stop_activity{
    [DejalBezelActivityView removeView];
}

/*-(void)dealloc{
 
 [tbl_location release];
 [super dealloc];
 }
 */
@end
