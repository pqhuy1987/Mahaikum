//
//  webview_viewcontroller_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DejalActivityView.h"
#import "BlockActionSheet.h"

@interface webview_viewcontroller_iPad : UIViewController{
    
    NSString *web_url;
    UIWebView *webview;
}
@property(nonatomic,retain) NSString *web_url;
@property(nonatomic,retain)IBOutlet  UIWebView *webview;

@end
