//
//  Show_map_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import <CoreLocation/CoreLocation.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "EGOImageView.h"
#import "Home_tableview_data_share.h"
#import "CustomAlertView_map.h"

#import "image_collection_cell.h"
#import "Image_detail_iPad.h"
#import "MKMapView+ZoomLevel.h"

@interface Show_map_ViewController_iPad : UIViewController<MKMapViewDelegate>{
    NSMutableArray *array_data;
    
    UIView *view_image;
    
    UICollectionView *collectionViewObj;
    
    
}
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_collection;
@property(nonatomic,retain)IBOutlet UIView *view_zoom;
@property(nonatomic,retain)IBOutlet UIImageView *img_zoom;

@property(nonatomic,retain)IBOutlet UIView *view_zoom_controller;
@property(nonatomic,retain)IBOutlet EGOImageView *img_photo;
@property(nonatomic,retain)IBOutlet UIButton *btn_info;
@property(nonatomic,retain)IBOutlet  UIView *view_show_image;
@property(nonatomic,retain)NSMutableArray *array_data;
@property(nonatomic,retain)IBOutlet UIView *view_image;
@property(nonatomic,retain)IBOutlet UICollectionView *collectionViewObj;
@property(nonatomic,retain) Image_detail_iPad *image_detail_viewObj;

@end
