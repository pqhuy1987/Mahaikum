//
//  Favourite_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Favourite_ViewController_iPad.h"
#import "User_info_ViewController_iPad.h"
#import "webview_viewcontroller_iPad.h"
#import "Hash_tag_ViewController_iPad.h"

@interface Favourite_ViewController_iPad ()
{
    
    User_info_ViewController_iPad *user_info_view;
    webview_viewcontroller_iPad *web_viewObj;
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
}
@end

@implementation Favourite_ViewController_iPad

@synthesize array_news_feed,array_follow,tbl_feed,segment,image_detailViewObj,image_detail_viewObj,tbl_news,lbl_temp,lbl_temp2,userStyle1;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tbl_news setHidden:YES];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.array_follow =[[NSMutableArray alloc]init];
    self.array_news_feed =[[NSMutableArray alloc]init];
    hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    web_viewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    self.image_detailViewObj=[[Image_detail_url_iPad alloc]initWithNibName:@"Image_detail_url_iPad" bundle:nil];
    self.image_detail_viewObj=[[Image_detail_iPad alloc]initWithNibName:@"Image_detail_iPad" bundle:nil];
  
//    self.segment.frame = CGRectMake(134.0f, 5.0f,500.0f, 40.0f);
//    self.segment.font = [UIFont boldSystemFontOfSize:26.0f];
//    self.segment.selectedItemShadowColor =[UIColor clearColor];
//    self.segment.unselectedItemShadowColor =[UIColor clearColor];
//    self.segment.selectedItemColor = [UIColor whiteColor];
//    self.segment.unselectedItemColor=[UIColor whiteColor];
//    
//    self.segment.unSelectedItemBackgroundGradientColors =[NSArray arrayWithObjects:[UIColor colorWithRed:71.0f/255.0f green:148.0f/255.0f blue:232.0f/255.0f alpha:1],[UIColor colorWithRed:71.0f/255.0f green:148.0f/255.0f blue:232.0f/255.0f alpha:1], nil];
    
    
    self.lbl_temp = [[LORichTextLabel alloc] initWithWidth:545];
    [self.lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:32.0]];
    
    self.lbl_temp2 = [[LORichTextLabel alloc] initWithWidth:650];
    [self.lbl_temp2 setFont:[UIFont fontWithName:@"Helvetica" size:32.0]];
    
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
    
    self.userStyle1 = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [self.userStyle1 addTarget:self action:@selector(userSelected:)];
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [urlStyle addTarget:self action:@selector(urlSelected:)];
    [self.lbl_temp addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp addStyle:atStyle forPrefix:@"@"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    
    [self.lbl_temp2 addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp2 addStyle:atStyle forPrefix:@"@"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Http://"];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self get_news_feed];
    [self get_following_feed];
}

-(void)get_following_feed {
    //ttp://www.techintegrity.in/mystyle/get_following_feeds.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=3&off=0
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14216" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFollowingFeedsResponce:) name:@"14216" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14216" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetFollowingFeedsResponce:) name:@"-14216" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_following_feeds.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",@"0",@"off",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14216" :params];
}


-(void)getFollowingFeedsResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14216" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14216" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_follow removeAllObjects];
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        
        NSArray *array =[result valueForKey:@"data"];
        for (NSDictionary *dict in array) {
            Favourite_news_following_share *shareobj =[[Favourite_news_following_share alloc]init];
            shareobj.user_action =[dict valueForKey:@"user_action"];
            
            if ([shareobj.user_action isEqualToString:@"follow"]) {
                
                shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                [self.array_follow addObject:shareobj];
                [shareobj release];
                
            }else if ([shareobj.user_action isEqualToString:@"comment"]) {
                
                shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                shareobj.img_url =[StaticClass urlDecode:[dict valueForKey:@"comment_on_image"]];
                shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"comment_on_username"]];
                shareobj.image_id =[StaticClass urlDecode:[dict valueForKey:@"image_id"]];
                [self.array_follow addObject:shareobj];
                [shareobj release];
                
            }else if ([shareobj.user_action isEqualToString:@"like"]) {
                
                shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"liked_image_owner"]];
                shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                shareobj.img_count =[dict valueForKey:@"image_likes"];
                shareobj.array_img =[dict valueForKey:@"image"];
                [self.array_follow addObject:shareobj];
                [shareobj release];
            }
        }
    }
    [self.tbl_feed reloadData];
}

-(void)FailgetFollowingFeedsResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14216" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14216" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(void)get_news_feed {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14215" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNewsFeedResponce:) name:@"14215" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14215" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetNewsFeedResponce:) name:@"-14215" object:nil];
    
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_news_feed.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",@"0",@"off",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14215" :params];
}

-(void)getNewsFeedResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14215" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14215" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        [self.array_news_feed removeAllObjects];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict_main in array) {
            News_feed_like_Share *shareObj_main =[[News_feed_like_Share alloc]init];
            shareObj_main.user_id = [dict_main valueForKey:@"user_id"];
            shareObj_main.username =[StaticClass urlDecode:[dict_main valueForKey:@"username"]];
            shareObj_main.feed =[StaticClass urlDecode:[dict_main valueForKey:@"feed"]];
            shareObj_main.userimage =[StaticClass urlDecode:[dict_main valueForKey:@"userimage"]];
            shareObj_main.name =[StaticClass urlDecode:[dict_main valueForKey:@"name"]];
            shareObj_main.image =[StaticClass urlDecode:[dict_main valueForKey:@"image"]];
            shareObj_main.status_date=[StaticClass urlDecode:[dict_main valueForKey:@"status_date"]];
            shareObj_main.userAction=[StaticClass urlDecode:[dict_main valueForKey:@"user_action"]];
            
            NSDictionary *dict =[dict_main valueForKey:@"image_data"];
            
            if ([dict isKindOfClass:[NSDictionary class] ]){
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[dict valueForKey:@"username"];
                
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
                shareObj.lat=[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[tempdict valueForKey:@"username"]];
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[tempdict valueForKey:@"username"];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        [shareObj.array_comments addObject:obj];
                        [obj release];
                        
                        
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                shareObj_main.image_data =shareObj;
                //  [shareObj release];
            }
            [self.array_news_feed addObject:shareObj_main];
            
        }
        
        
    }
    [self.tbl_news reloadData];
}

-(void)FailgetNewsFeedResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14215" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14215" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(IBAction)btn_segment_value_change:(id)sender{
    if (self.segment.selectedSegmentIndex==0) {
        [self.tbl_news setHidden:YES];
        [self.tbl_feed setHidden:NO];
        [self.tbl_feed reloadData];
    } else {
        [self.tbl_news setHidden:NO];
        [self.tbl_feed setHidden:YES];
        [self.tbl_news reloadData];
    }
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.segment.selectedSegmentIndex==0) {
        return self.array_follow.count;
    }else{
        return self.array_news_feed.count;
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.segment.selectedSegmentIndex==0) {
        
        Favourite_news_following_share *shareobj =[self.array_follow objectAtIndex:indexPath.row];
        if ([shareobj.user_action isEqualToString:@"follow"]) {
            [self.lbl_temp2 setText:[NSString stringWithFormat:@"%@ %@ %@",shareobj.username,shareobj.feed,shareobj.following_name]];
            if (shareobj.following_name.length !=0) {
                [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.following_name];
            }
            [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.username];
            
            if (self.lbl_temp2.height <42) {
                return 120.0f;
            }else{
                return self.lbl_temp2.height +78.0f;
            }
        }else if([shareobj.user_action isEqualToString:@"comment"]){
            
            
            [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
            
            [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];
            
            
            if (self.lbl_temp.height <58) {
                return 120.0f;
            }else{
                return self.lbl_temp.height +66.0f;
            }
        }else if([shareobj.user_action isEqualToString:@"like"]){
            if ([shareobj.img_count isEqualToString:@"1"]) {
                [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
                
                [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];
                
                
                if (self.lbl_temp.height <58) {
                    return 120.0f;
                }else{
                    return self.lbl_temp.height +66.0f;
                }
                
            }else{
                int count =(([shareobj.img_count intValue])/3);
                int height =count*210;
                if((([shareobj.img_count intValue])%3) >0){
                    
                    height+=210;
                }
                return 120.0f + height;
            }
        }
        return 120.0f;
        
    }
    else{
        News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];
        if (![shareObj.feed isEqualToString:@"started following you."]) {
            
            [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            
            if (self.lbl_temp.height <58) {
                return 120.0f+15;
            }else{
                return self.lbl_temp.height +68.0f+15;
            }
        }
        
        return 120.0f+15;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.segment.selectedSegmentIndex==0) {
        
        Favourite_news_following_share *shareObj =[self.array_follow objectAtIndex:indexPath.row];
        if ([shareObj.user_action isEqualToString:@"follow"]) {
            
            static NSString *identifier =@"News_feed_following_cell_iPad";
            News_feed_following_cell_iPad *cell=(News_feed_following_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell_iPad" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell draw_in_cell];
            }
            cell.imgRateImage.hidden=YES;
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@ %@",shareObj.username,shareObj.feed,shareObj.following_name]];
         //   cell.btn_user_img.imageURL = [NSURL URLWithString:shareObj.user_img_url];
            [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            
            if (cell.lbl_dec.height <42) {
                cell.lbl_time.frame =CGRectMake(116,66,210,42);
                cell.lbl_dec.frame = CGRectMake(110,10,650,40);
            }else{
                cell.lbl_time.frame =CGRectMake(116,cell.lbl_dec.height+6,210,42);//26
                cell.lbl_dec.frame = CGRectMake(110,10,650,cell.lbl_dec.height);
            }
            
            return cell;
            
        }else if([shareObj.user_action isEqualToString:@"comment"]){
            
            static NSString *identifier =@"News_feed_comment_cell_iPad";
            News_feed_comment_cell_iPad *cell=(News_feed_comment_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_comment_cell_iPad" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
                [cell draw_dec_lbl];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:32.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            }
            cell.btn_img1.tag=indexPath.row;
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil];
            [cell.btn_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.img_url] placeholderImage:nil];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            
            if (cell.lbl_dec.height <58) {
                cell.lbl_dec.frame = CGRectMake(110,10,545,58);
                cell.lbl_time.frame = CGRectMake(116, 66, 210, 42);
            }else{
                cell.lbl_dec.frame = CGRectMake(110,10,545,cell.lbl_dec.height);
                cell.lbl_time.frame = CGRectMake(116,cell.lbl_dec.height+14, 210, 42);
                
            }
            
            return cell;
        }else if([shareObj.user_action isEqualToString:@"like"]){
            
            
            if ([shareObj.img_count isEqualToString:@"1"]) {
                
                static NSString *identifier =@"News_feed_like_following_cell_iPad";
                News_feed_like_following_cell_iPad *cell=(News_feed_like_following_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (cell==nil) {
                    NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_following_cell_iPad" owner:self options:nil];
                    cell =[nib objectAtIndex:0];
                    cell.backgroundColor =[UIColor clearColor];
                    cell.showsReorderControl = NO;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
                    [cell draw_dec_lbl];
                    
                    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:32.0];
                    
                    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [hashStyle addTarget:self action:@selector(hashSelected:)];
                    
                    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [atStyle addTarget:self action:@selector(atSelected:)];
                    
                    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [urlStyle addTarget:self action:@selector(urlSelected:)];
                    [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                    [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                }
                cell.btn_img1.tag = indexPath.row;
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
                LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [userStyle addTarget:self action:@selector(userSelected:)];
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
                
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
                
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];

                [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil];
                for (NSDictionary *dict in shareObj.array_img) {
                    
                    [cell.btn_img setImageWithContentsOfURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil];
                }
                ///   cell.btn_img.imageURL = [NSURL URLWithString:shareObj.img_url];
                cell.lbl_time.text =[self get_time_different:shareObj.status_date];
                               
                if (cell.lbl_dec.height <58) {
                    cell.lbl_dec.frame = CGRectMake(110,10,545,58);
                    cell.lbl_time.frame = CGRectMake(116, 66, 210, 42);
                }else{
                    cell.lbl_dec.frame = CGRectMake(110,10,545,cell.lbl_dec.height);
                    cell.lbl_time.frame = CGRectMake(116,cell.lbl_dec.height+6, 210, 42);
                    
                }
                
                return cell;
                
            }else{
                
                
                static NSString *identifier =@"Favourite_feed_following_collection_cell_iPad";
                Favourite_feed_following_collection_cell_iPad *cell=(Favourite_feed_following_collection_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (cell==nil) {
                    NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Favourite_feed_following_collection_cell_iPad" owner:self options:nil];
                    cell =[nib objectAtIndex:0];
                    cell.backgroundColor =[UIColor clearColor];
                    cell.showsReorderControl = NO;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell draw_collectionview_in_cell];
                    
                    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:32.0];
                    
                    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [hashStyle addTarget:self action:@selector(hashSelected:)];
                    
                    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [atStyle addTarget:self action:@selector(atSelected:)];
                    
                    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                    [urlStyle addTarget:self action:@selector(urlSelected:)];
                    [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                    [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                }
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
                LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [userStyle addTarget:self action:@selector(userSelected:)];
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
                //   [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
                
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
              //  cell.btn_user_img.imageURL = [NSURL URLWithString:shareObj.user_img_url];
                [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil];
                cell.lbl_time.text =[self get_time_different:shareObj.status_date];

                int count =(([shareObj.img_count intValue])/3);
                int height =count*210;
                if((([shareObj.img_count intValue])%3) >0){
                    
                    height+=210;
                }
                cell.collectionView.frame = CGRectMake(0,110,768,height);
                [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
                cell.collectionView.delegate = self;
                cell.collectionView.dataSource = self;
                
                return cell;
            }
            
        }
        return nil;
        
    }
    else{
        News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];
        
        if (![shareObj.feed isEqualToString:@"started following you."]) {
            
            static NSString *identifier =@"News_feed_like_cell_iPad";
            News_feed_like_cell_iPad *cell=(News_feed_like_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_cell_iPad" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_click:) forControlEvents:UIControlEventTouchUpInside];
                [cell draw_dec_lbl];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:32.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            }
            cell.btn_img1.tag = indexPath.row;
            if (![shareObj.userAction isEqualToString:@"rating"]) {
                cell.imgRateImage.hidden=YES;
            }
            else {
                cell.imgRateImage.hidden=NO;
                Home_tableview_data_share *homShr=shareObj.image_data;
                if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                    NSString *imgName=@"0star.png";
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
                else {
                    NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                    NSLog(@"imgName3:%@",imgName);
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
            }
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            
            [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil];
            [cell.btn_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.image] placeholderImage:nil];
            
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            if (cell.lbl_dec.height <58) {
                cell.lbl_dec.frame = CGRectMake(110,10,545,58);
                cell.lbl_time.frame = CGRectMake(116,66,210,42);
                cell.imgRateImage.frame = CGRectMake(116,66+5+42,70,15);
                
            }else{
                cell.lbl_dec.frame = CGRectMake(110,10,545,cell.lbl_dec.height);
                cell.lbl_time.frame = CGRectMake(116,cell.lbl_dec.height+10, 210, 42);//14
                cell.imgRateImage.frame = CGRectMake(116,cell.lbl_dec.height+10+5,70,15);
            }
            
            return cell;
        }else{
            
            static NSString *identifier =@"News_feed_following_cell_iPad";
            News_feed_following_cell_iPad *cell=(News_feed_following_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell_iPad" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                [cell draw_in_cell];
            }
            if (![shareObj.userAction isEqualToString:@"rating"]) {
                cell.imgRateImage.hidden=YES;
            }
            else {
                cell.imgRateImage.hidden=NO;
                Home_tableview_data_share *homShr=shareObj.image_data;
                if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                    NSString *imgName=@"0star.png";
                    NSLog(@"imgName3:%@",imgName);
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
                else {
                    NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                    NSLog(@"imgName3:%@",imgName);
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
            }
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:32.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
           // cell.btn_user_img.imageURL = [NSURL URLWithString:shareObj.userimage];
            [cell.btn_user_img setImageWithContentsOfURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            cell.lbl_time.frame = CGRectMake(116,66,216, 42);//14
            cell.imgRateImage.frame = CGRectMake(53,66+42+5,70,15)
            ;
            return cell;
        }
    }
}



#pragma mark - UICollectionViewDataSource Methods


- (NSInteger)numberOfSectionsInCollectionView:(Collectionview_delegate *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(Collectionview_delegate *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    
    return [obj.img_count integerValue];
}



- (CGSize)collectionView:(Collectionview_delegate *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(200, 200);
    
}

-(UICollectionViewCell *)collectionView:(Collectionview_delegate *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];
 //   cell.img_photo.imageURL =[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]];
    [cell.img_photo setImageWithContentsOfURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil];
    // cell.img_photo.image =[UIImage imageNamed:@"test1.png"];
    cell.tag = indexPath.row;
    
    return cell;
}

- (void)collectionView:(Collectionview_delegate *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];
    
    self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
}


#pragma mark - Image Detail
-(IBAction)btn_image_detail_like_click:(id)sender{
    News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:((UIButton *)sender).tag];
    
    self.image_detail_viewObj.shareObj =shareObj.image_data;
    
    self.image_detail_viewObj.arrayFeedArray=self.array_news_feed;
    self.image_detail_viewObj.currentSelectedIndex=((UIButton *)sender).tag;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

-(IBAction)btn_image_detail_like_following_click:(id)sender{
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:((UIButton *)sender).tag];
    if ([obj.user_action isEqualToString:@"comment"]){
        self.image_detailViewObj.image_id =obj.image_id;
    }else{
        NSDictionary *dict =[obj.array_img objectAtIndex:0];
        
        self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    }
    
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
    
}
#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    [dateFormatter release];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"just now"];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.f minutes ago",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.f hours ago",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.f days ago",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.f months ago",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.f year ago",diff/(3600*24*30*12)];
    }
}




#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    //  NSLog(@"%@",[self tagFromSender:sender ]);
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender] stringByReplacingOccurrencesOfString:@"'s" withString:@""];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [lbl_temp2 release];
    [lbl_temp release];
    [tbl_news release];
    [image_detail_viewObj release];
    [image_detailViewObj release];
    [segment release];
    [array_news_feed release];
    [array_follow release];
    [tbl_feed release];
    [super dealloc];
}

@end
