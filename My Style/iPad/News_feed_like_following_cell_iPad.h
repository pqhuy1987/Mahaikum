//
//  News_feed_like_following_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "EGOImageButton.h"
@class NPRImageView;

@interface News_feed_like_following_cell_iPad : UITableViewCell{
    
    LORichTextLabel *lbl_dec;
    NPRImageView *btn_user_img;
    NPRImageView *btn_img;
    
    UILabel *lbl_time;
    
    UIButton *btn_user_img1;
    UIButton *btn_img1;

}
@property(nonatomic,retain) LORichTextLabel *lbl_dec;
@property(nonatomic,retain)IBOutlet  NPRImageView *btn_user_img;
@property(nonatomic,retain)IBOutlet  NPRImageView *btn_img;
@property(nonatomic,retain)IBOutlet  UILabel *lbl_time;

@property(nonatomic,retain)IBOutlet UIButton *btn_user_img1;
@property(nonatomic,retain)IBOutlet UIButton *btn_img1;
-(void)draw_dec_lbl;

@end
