//
//  Rest_password_throw_fb_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Rest_password_throw_fb_iPad.h"

@interface Rest_password_throw_fb_iPad ()

@end

@implementation Rest_password_throw_fb_iPad

@synthesize txt_password,txt_password_again,str_email;
@synthesize  str_username,str_image_url;
@synthesize img_cell_bg,lbl_username,lbl_dec,img_image;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =8.0f;
    self.img_image.layer.cornerRadius = 8.0f;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    NSLog(@"%@",self.str_username);
    self.lbl_username.text =self.str_username;
    self.lbl_dec.text =[NSString stringWithFormat:@"Welcome back %@! Enter your new password twice.",self.str_username];
    self.img_image.imageURL =[NSURL URLWithString:self.str_image_url];
}

-(IBAction)btn_reset_password:(id)sender{
    
    if (self.txt_password.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please create a password." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if (self.txt_password.text.length<6) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Password must be at least 6 characters." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    
    if (![self.txt_password.text isEqualToString:self.txt_password_again.text]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Passwords do not match." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14229" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetPasswordAPIResponce:) name:@"14229" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14229" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailresetPasswordAPIResponce:) name:@"-14229" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@reset_password.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            self.str_email,@"email",
                            self.txt_password.text,@"password",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14229" :params];
}

-(void)resetPasswordAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14229" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14229" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Password change successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

-(void)FailresetPasswordAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14229" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14229" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UIA;ertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [img_image release];
    [lbl_username release];
    [lbl_dec release];
    [img_cell_bg release];
    [str_username release];
    [str_image_url release];
    [str_email release];
    [txt_password release];
    [txt_password_again release];
    [super dealloc];
}
@end
