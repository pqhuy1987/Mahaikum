//
//  Suggested_friend_list_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Suggested_friend_list_cell_iPad.h"

@interface Suggested_friend_list_cell_iPad ()

@end

@implementation Suggested_friend_list_cell_iPad

@synthesize btn_user_img,lbl_name,lbl_username,lbl_dec,btn_follow;

-(void)dealloc{
    [btn_follow release];
    [lbl_name release];
    [lbl_username release];
    [btn_user_img release];
    [super dealloc];
}

-(void)draw_collectionview_in_cell{
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 42, 0, 42);
    layout.minimumLineSpacing = 42.0f;
    layout.minimumInteritemSpacing =1.0f;
    layout.itemSize = CGSizeMake(100,100);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[Collectionview_delegate alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier2];
    self.collectionView.backgroundColor = [UIColor clearColor];
    //  self.collectionView.showsHorizontalScrollIndicator = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    [self.contentView addSubview:self.collectionView];
}
/*
 -(void)layoutSubviews
 {
 [super layoutSubviews];
 
 self.collectionView.frame = CGRectMake(0, 50, 320, 100);
 }
 */
-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.index = index;
    
    [self.collectionView reloadData];
}


@end
