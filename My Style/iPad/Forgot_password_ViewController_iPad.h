//
//  Forgot_password_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reset_password_from_username_email_iPad.h"
#import "Rest_password_throw_fb_iPad.h"
#import "DejalActivityView.h"

@interface Forgot_password_ViewController_iPad : UIViewController{

    UIImageView *img_cell_bg;
}
@property(nonatomic,retain)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,retain) Reset_password_from_username_email_iPad *reset_email_viewObj;
@property(nonatomic,retain)  Rest_password_throw_fb_iPad *reset_fb_viewObj;
@end
