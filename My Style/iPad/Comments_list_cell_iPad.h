//
//  Comments_list_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
@class NPRImageView;

@interface Comments_list_cell_iPad :  UITableViewCell{
    
    NPRImageView *img_user;
    UIButton *btn_user_name;
    LORichTextLabel *lbl_desc;
    
    UILabel *lbl_date;
    
    UIView *view_line;
    
    UIButton *img_user1;
}
@property(nonatomic,retain)IBOutlet UIView *view_line;
@property(nonatomic,retain)IBOutlet  UILabel *lbl_date;

@property(nonatomic,retain)IBOutlet NPRImageView *img_user;
@property(nonatomic,retain)IBOutlet UIButton *btn_user_name;
@property(nonatomic,retain) LORichTextLabel *lbl_desc;
@property(nonatomic,retain)IBOutlet UIButton *img_user1;
-(void)draw_in_cell;@end
