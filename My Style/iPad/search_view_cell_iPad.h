//
//  search_view_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface search_view_cell_iPad : UITableViewCell{
    EGOImageView *img_user;
    UILabel *lbl_name;
    UILabel *lbl_username;
}
@property(nonatomic,retain)IBOutlet EGOImageView *img_user;
@property(nonatomic,retain)IBOutlet UILabel *lbl_name;
@property(nonatomic,retain)IBOutlet UILabel *lbl_username;

@end
