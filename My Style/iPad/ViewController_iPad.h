//
//  ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sign_ViewController_iPad.h"
@interface ViewController_iPad : UIViewController{
    
    UIImageView *img_bg;
    UIImageView *img_logo;
    Sign_ViewController_iPad *sign_in_ViewObj;
    
}

@property(nonatomic,retain)IBOutlet UIImageView *img_bg;
@property(nonatomic,retain)IBOutlet UIImageView *img_logo;

@end
