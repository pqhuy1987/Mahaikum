//
//  Reset_password_throw_email_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "DejalActivityView.h"
#import "Rest_password_throw_fb_iPad.h"

@interface Reset_password_throw_email_iPad : UIViewController
{
    
    NSString *str_username;
    NSString *str_email;
    NSString *str_image_url;
    
    UIImageView *img_cell_bg;
    EGOImageView *img_image;
    
    UILabel *lbl_username;
    UILabel *lbl_dec;
    
}
@property(nonatomic,retain)  Rest_password_throw_fb_iPad *reset_fb_viewObj;
@property(nonatomic,retain) NSString *str_username;
@property(nonatomic,retain) NSString *str_email;
@property(nonatomic,retain) NSString *str_image_url;

@property(nonatomic,retain)IBOutlet  UIImageView *img_cell_bg;
@property(nonatomic,retain)IBOutlet EGOImageView *img_image;
@property(nonatomic,retain)IBOutlet UILabel *lbl_username;
@property(nonatomic,retain)IBOutlet UILabel *lbl_dec;

@end
