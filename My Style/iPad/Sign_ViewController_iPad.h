//
//  Sign_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"
#import "Register_ViewController_iPad.h"
#import "Forgot_password_ViewController_iPad.h"
#import "Facebook_Find_Friends_ViewController_iPad.h"
#import "ALTabBarController.h"

#import "Detail_ViewController.h"

@interface Sign_ViewController_iPad : UIViewController<UITabBarControllerDelegate>
{
    
    ALTabBarController *tabbar;

    
    UIImageView *img_login_bg;
    
    UIImageView *img_StartLoing;
    UILabel *lblLogin;
    UIActivityIndicatorView *activityViewObj;
}
@property(nonatomic,retain)IBOutlet UIImageView *img_login_bg;
@property(nonatomic,retain)IBOutlet  ALTabBarController *tabbar;


@property(nonatomic,retain) Register_ViewController_iPad *register_viewObj;
@property(nonatomic,retain) Forgot_password_ViewController_iPad *forgot_password_viewObj;

@property(nonatomic,retain)IBOutlet UIImageView *img_StartLoing;
@property(nonatomic,retain)IBOutlet UILabel *lblLogin;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activityViewObj;

@end
