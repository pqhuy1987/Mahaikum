//
//  Find_invite_friend_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Find_invite_friend_iPad.h"
#import "AppDelegate.h"
@interface Find_invite_friend_iPad ()

@end

@implementation Find_invite_friend_iPad

@synthesize img_cell_bg,facebook_friendViewObj,contact_friendViewObj,suggested_friend_list;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =4.0f;
    
    self.facebook_friendViewObj =[[Facebook_friend_list_iPad alloc]initWithNibName:@"Facebook_friend_list_iPad" bundle:nil];
    
    self.contact_friendViewObj =[[Contact_friend_list_iPad alloc]initWithNibName:@"Contact_friend_list_iPad" bundle:nil];
    self.suggested_friend_list=[[Suggested_friend_list_iPad alloc]initWithNibName:@"Suggested_friend_list_iPad" bundle:nil];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)btn_facebook_friend_click:(id)sender{
    
    AppDelegate *appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate.session closeAndClearTokenInformation];
    [self.navigationController pushViewController:self.facebook_friendViewObj animated:YES];
}

-(IBAction)btn_contact_click:(id)sender{
    
    [self.navigationController pushViewController:self.contact_friendViewObj animated:YES];
}

-(IBAction)btn_suggested_click:(id)sender{
    [self.navigationController pushViewController:self.suggested_friend_list animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [suggested_friend_list release];
    [contact_friendViewObj release];
    [facebook_friendViewObj release];
    [img_cell_bg release];
    [super dealloc];
    
}
@end
