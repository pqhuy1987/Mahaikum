//
//  News_feed_following_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"

@class NPRImageView;

@interface News_feed_following_cell_iPad : UITableViewCell{
    
    NPRImageView *btn_user_img;
    UILabel *lbl_time;
    LORichTextLabel *lbl_dec;
    UIButton *btn_user_img1;
    UIImageView *imgRateImage;
    
}
-(void)draw_in_cell;
@property(nonatomic,retain) LORichTextLabel *lbl_dec;

@property(nonatomic,retain)IBOutlet NPRImageView *btn_user_img;
@property(nonatomic,retain)IBOutlet UILabel *lbl_time;
@property(nonatomic,retain)IBOutlet UIButton *btn_user_img1;
@property(nonatomic,retain)IBOutlet UIImageView *imgRateImage;

@end
