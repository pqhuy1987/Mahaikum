//
//  Suggested_friend_list_reg_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Suggested_friend_list_cell_iPad.h"
#import "image_collection_cell.h"
#import "Suggested_friend_list_share.h"

@interface Suggested_friend_list_reg_iPad : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{
    
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
}
@property(nonatomic,retain)NSMutableArray *array_friend;
@property(nonatomic,retain)IBOutlet  UITableView *tbl_friend;

@end
