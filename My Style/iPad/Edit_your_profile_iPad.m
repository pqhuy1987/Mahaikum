//
//  Edit_your_profile_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Edit_your_profile_iPad.h"
#import "AppDelegate.h"
@interface Edit_your_profile_iPad ()

@end

@implementation Edit_your_profile_iPad

@synthesize img_first_cell,img_photo,scrollview,img_second_cell,img_third_cell,img_fourth_cell;

@synthesize txt_name,txt_username,txt_weburl,txt_bio,txt_email,txt_phone;
@synthesize photoPickerController,image_url,btn_gender;
@synthesize profile_shareObj;
@synthesize is_reload_data,is_show_alert,switch_gender,switch_private;
@synthesize popover;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.s3 = [[[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] autorelease];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.profile_shareObj=[[Edit_your_profile_share alloc]init];
    
    self.img_first_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_first_cell.layer.cornerRadius =5.0f;

    
    self.img_second_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_second_cell.layer.cornerRadius =5.0f;

    
    self.img_third_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_third_cell.layer.cornerRadius =5.0f;

    
    self.img_fourth_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_fourth_cell.layer.cornerRadius =5.0f;

    
    
    self.img_photo.layer.cornerRadius =4.0f;
    self.img_photo.layer.masksToBounds = YES;
    
    

    
    
    
//    UIToolbar *toolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 768, 50)];
//    toolbar.barStyle= UIBarStyleBlackTranslucent;
//    toolbar.items =@[[[UIBarButtonItem alloc]initWithTitle:@"Cancel"style:UIBarButtonItemStyleBordered target:self
//                                                    action:@selector(btn_cancel_click:)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                     [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btn_done_click:)]];
//    [toolbar sizeToFit];
//    
//    self.txt_name.inputAccessoryView=toolbar;
//    self.txt_username.inputAccessoryView=toolbar;
//    self.txt_weburl.inputAccessoryView=toolbar;
//    self.txt_bio.inputAccessoryView=toolbar;
//    self.txt_email.inputAccessoryView=toolbar;
//    self.txt_phone.inputAccessoryView=toolbar;
    
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    
    
    
    
    self.switch_gender.onText = NSLocalizedString(@"Male", @"");
	self.switch_gender.offText = NSLocalizedString(@"Female", @"");
    self.switch_gender.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    
    self.switch_private.onText = NSLocalizedString(@"ON", @"");
	self.switch_private.offText = NSLocalizedString(@"OFF", @"");
    self.switch_private.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    [self.switch_private addTarget:self action:@selector(btn_private_valuechange:) forControlEvents:UIControlEventValueChanged];


    self.popover = [UIPopoverController alloc];
    self.popover.delegate = self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    is_show_alert=0;
    self.navigationController.navigationBarHidden = YES;
    
    
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    
    

    
    if (is_reload_data==1) {
        [self get_user_info_for_edit];
        is_reload_data=0;
    }
}

#pragma mark Change Password
-(IBAction)btn_change_password_click:(id)sender{
    
    Change_password_iPad *obj =[[Change_password_iPad alloc]initWithNibName:@"Change_password_iPad" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark UITextfield Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField==txt_name) {
        self.scrollview.contentOffset=CGPointMake(0,0);
    }else if(textField==txt_username){
        self.scrollview.contentOffset=CGPointMake(0,0);
        
    }else if(textField==txt_weburl){

            self.scrollview.contentOffset=CGPointMake(0,0);
       
    }else if(textField==txt_bio){
            self.scrollview.contentOffset=CGPointMake(0,0);
       
    
    }else if(textField==txt_email){
            self.scrollview.contentOffset=CGPointMake(0, 0);
        
    }else if(textField==txt_phone){

            self.scrollview.contentOffset=CGPointMake(0,88);
 
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    //  self.scrollview.contentOffset=CGPointMake(0,0);
    
    if (textField==txt_name) {
        
        [txt_username becomeFirstResponder];
        
    }else if(textField==txt_username){
        
        [txt_weburl becomeFirstResponder];
        
    }else if(textField==txt_weburl){
        
        [txt_bio becomeFirstResponder];
        
    }else if(textField==txt_bio){
        
        [txt_email becomeFirstResponder];
        
    }else if(textField==txt_email){
        [txt_phone becomeFirstResponder];
    }
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField==self.txt_username || textField==self.txt_email) {
        
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    
    return YES;
    
}

-(IBAction)btn_cancel_click:(id)sender{
    [self.view endEditing:YES];
    self.scrollview.contentOffset=CGPointMake(0,0);
}
-(IBAction)btn_done_click:(id)sender{
    [self.view endEditing:YES];
    self.scrollview.contentOffset=CGPointMake(0,0);
    
    if (self.txt_name.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a name" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    if (self.txt_username.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a Username" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    if (![StaticClass validateEmail:self.txt_email.text]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
}


#pragma mark - Change Profile pic
-(IBAction)btn_profile_btn_click:(id)sender{
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Change Profile Picture"];
    
    [sheet setDestructiveButtonWithTitle:@"Remove Current Photo" block:^{
        
        
    }];
    
    [sheet addButtonWithTitle:@"Import from Facebook" block:^{
        [self get_user_photo_facebok];
    }];
    
    [sheet addButtonWithTitle:@"Import from Twitter" block:^{
        
    }];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        [sheet addButtonWithTitle:@"Take Photo" block:^{
            photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:photoPickerController animated:YES completion:nil];
        }];
    }
    
    [sheet addButtonWithTitle:@"Choose from Library" block:^{
      //  photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
      //  [self presentViewController:photoPickerController animated:YES completion:nil];
        
        if ([self.popover isPopoverVisible]) {
            [self.popover dismissPopoverAnimated:YES];
            [popover release];
        } else {
            photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            UIPopoverController *popOver = [[UIPopoverController alloc] initWithContentViewController:photoPickerController];
            
            
            [popOver presentPopoverFromRect:self.img_photo.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            self.popover = popOver;
        }
    }];
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.popover dismissPopoverAnimated:YES];
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
	if (baseImage == nil) return;
    [self.img_photo setImage:baseImage forState:UIControlStateNormal];
    
    
}


#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        //  appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}
-(void)apiFQLI_pic_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    //NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(set_btn_add_photo_image) withObject:self afterDelay:0.1];
        
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
    
}
- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    
    
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    } else {
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
        
        self.image_url=imgU;
        
//        NSLog(@"%@",dictionary);
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            
//            for (NSString *str in [tempResult valueForKey:@"pic"]) {
//                self.image_url=str;
//            }
//            
//        });
        
    }
    
}

-(void)set_btn_add_photo_image{
    
    self.img_photo.imageURL =[NSURL URLWithString:self.image_url];
}

#pragma mark - Gender selected
-(IBAction)btn_gender_click:(id)sender{
    
    
    if (!switch_gender.isOn) {
        [self.btn_gender setTitle:@"Female" forState:UIControlStateNormal];
    }else{
        [self.btn_gender setTitle:@"Male" forState:UIControlStateNormal];
    }
}
- (void)actionPickerCancelled:(id)sender {
    NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}
- (void)genderSelected:(NSNumber *)selectedIndex element:(id)element {
    
    NSArray *array =@[@"-",@"Male",@"Female"];
    [self.btn_gender setTitle:[array objectAtIndex:[selectedIndex integerValue]] forState:UIControlStateNormal];
    
}

-(IBAction)btn_private_valuechange:(id)sender{
    
    NSLog(@"value Changed");
    if (is_show_alert!=0) {
        is_show_alert=0;
        if (self.switch_private.isOn) {
            NSLog(@"is ON");
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy on. This means from now on only people you approve will be able to follow your photos. Are you sure you want to turn privacy on?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
            [alert release];
        }else{
            NSLog(@"is OFF");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy off. This means from now on. anyone will be able to follow your photos. Are you sure you want to turn privacy off?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
            [alert release];
            
        }
        
    }
    else{
        is_show_alert=1;
    }
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (self.switch_private.isOn) {
        if (buttonIndex==1) {
            [self.switch_private setOn:YES animated:NO];
            is_show_alert=1;
        }else{
            [self.switch_private setOn:NO animated:YES];
            is_show_alert=0;
        }
    }else{
        if (buttonIndex==1) {
            [self.switch_private setOn:NO animated:NO];
            is_show_alert=1;
        }else{
            [self.switch_private setOn:YES animated:YES];
            is_show_alert=0;
        }
    }
}

#pragma mark - Get user info for Edit

-(void)get_user_info_for_edit{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disUserProfileAPIResponce:) name:@"14209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildisUserProfileAPIResponce:) name:@"-14209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@dis_user_profile.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14209" :params];
}


-(void)disUserProfileAPIResponce:(NSNotification *)notification {
  //  [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        NSDictionary *dict =[result valueForKey:@"data"];
        self.profile_shareObj.bio =[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        self.profile_shareObj.email =[StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_shareObj.facebook_id =[dict valueForKey:@"facebook_id"];
        self.profile_shareObj.gender =[dict valueForKey:@"gender"];
        self.profile_shareObj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_shareObj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_shareObj.photos_status =[StaticClass urlDecode:[dict valueForKey:@"photos_status"]];
        self.profile_shareObj.user_id =[dict valueForKey:@"user_id"];
        self.profile_shareObj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
        self.profile_shareObj.weburl =[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        self.profile_shareObj.phone =[StaticClass urlDecode:[dict valueForKey:@"phone"]];
        self.txt_name.text =self.profile_shareObj.name;
        self.txt_username.text = self.profile_shareObj.username;
        self.txt_weburl.text = self.profile_shareObj.weburl;
        self.txt_bio.text = self.profile_shareObj.bio;
        self.txt_email.text = self.profile_shareObj.email;
        self.txt_phone.text = self.profile_shareObj.phone;
        if ([self.profile_shareObj.photos_status isEqualToString:@"1"]) {
            [self.switch_private setOn:YES animated:YES];
        }else{
            [self.switch_private setOn:NO animated:YES];
        }
        if ([self.profile_shareObj.gender isEqualToString:@"f"]) {
            [self.btn_gender setTitle:@"Female" forState:UIControlStateNormal];
            [switch_gender setOn:NO];
        }else{
            [self.btn_gender setTitle:@"Male" forState:UIControlStateNormal];
            [switch_gender setOn:YES];
        }
        self.img_photo.imageURL =[NSURL URLWithString:self.profile_shareObj.image];
        
    }
    
}

-(void)FaildisUserProfileAPIResponce:(NSNotification *)notification {
  //  [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark - Save

-(IBAction)btn_save_click:(id)sender{
    
    if (self.txt_username.text.length==0) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please choose a username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        
        return;
    }
    
    if (![StaticClass validateEmail:self.txt_email.text]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    [self update_call];
}

-(void)update_call {
    NSData *img_data  = UIImageJPEGRepresentation(self.img_photo.imageView.image,1);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSDate *dt=[NSDate date];
       // double timePassed_ms = [dt timeIntervalSinceNow];
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *dateString1 = [DateFormatter stringFromDate:dt];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        __block NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]] autorelease];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else
            {
                
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = @"coco";
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUpdateProfileResponce:) name:@"14210" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUpdateProfileResponce:) name:@"-14210" object:nil];
                
                NSString *requestStr = [NSString stringWithFormat:@"%@post_update_profile.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSString *strPhotoStatus=@"";
                if (self.switch_private.isOn) {
                    strPhotoStatus=@"1";
                }
                else{
                    strPhotoStatus=@"0";
                }
                
                NSString *strGenderString=@"";
                
                if ([[btn_gender currentTitle] isEqualToString:@"Male"]) {
                    strGenderString=@"m";
                }else if([[btn_gender currentTitle] isEqualToString:@"Female"]){
                    strGenderString=@"f";
                }else{
                    strGenderString=@"-";
                }
                
                if (self.switch_private.isOn) {
                    
                    strPhotoStatus=@"1";
                }else{
                    strPhotoStatus=@"0";
                }
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        self.txt_username.text,@"username",
                                        self.txt_name.text,@"name",
                                        self.txt_email.text,@"email",
                                        self.txt_phone.text,@"phone",
                                        imageKeys,@"image",
                                        self.txt_weburl.text,@"weburl",
                                        self.txt_bio.text,@"bio",
                                        strPhotoStatus,@"photos_status",
                                        strGenderString,@"gender",
                                        nil];
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14210" :params];
            }
        });
    });
}

-(void)postUpdateProfileResponce:(NSNotification *)notification {
 //   [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"Success");
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeBlue title:@"Profile update sucessfully" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }else  if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Duplicate username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }else  if ([[result valueForKey:@"success"] isEqualToString:@"-3"]) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Duplicate email ID." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
}
-(void)FailpostUpdateProfileResponc:(NSNotification *)notification {
  //  [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14210" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [popover release];
    [switch_private release];
    [switch_gender release];
    [profile_shareObj release];
    [btn_gender release];
    [photoPickerController release];
    [image_url release];
    [txt_name release];
    [txt_username release];
    [txt_weburl release];
    [txt_bio release];
    [txt_email release];
    [txt_phone release];
    
    [img_fourth_cell release];
    [img_third_cell release];
    [img_second_cell release];
    [scrollview release];
    [img_first_cell release];
    [img_photo release];
    [super dealloc];
}

@end
