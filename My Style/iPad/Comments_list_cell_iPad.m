//
//  Comments_list_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Comments_list_cell_iPad.h"



@implementation Comments_list_cell_iPad


@synthesize img_user,btn_user_name,lbl_desc;
@synthesize lbl_date,view_line,img_user1;

-(void)draw_in_cell{
    self.lbl_desc = [[LORichTextLabel alloc] initWithWidth:655];
	[self.lbl_desc setFont:[UIFont fontWithName:@"Helvetica" size:28.0f]];
	[self.lbl_desc setTextColor:[UIColor colorWithRed:30.0f/255.0f green:48.0f/255.0f blue:62.0f/255.0f alpha:1]];
	[self.lbl_desc setBackgroundColor:[UIColor clearColor]];
	[self.lbl_desc positionAtX:110.0 andY:65.0];
    [self.contentView addSubview:self.lbl_desc];
}

-(void)dealloc{
    [img_user1 release];
    [view_line release];
    [lbl_date release];
    [img_user release];
    [btn_user_name release];
    [lbl_desc release];
    
    [super dealloc];
}@end
