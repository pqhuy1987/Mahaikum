//
//  Setting_profile_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "webview_viewcontroller_iPad.h"
#import "Notification_setting_ViewController_iPad.h"
#import "Photos_you_liked_iPad.h"
#import "DCRoundSwitch.h"
#import "Find_invite_friend_iPad.h"

@interface Setting_profile_iPad : UIViewController{

    UIImageView *img_firstcell;
    UIImageView *img_secondcell;
    UIImageView *img_thirdcell;
    UIImageView *img_fourthcell;
    
    UIScrollView *scrollview;
    
    Notification_setting_ViewController_iPad *notification_settingViewObj;
    int is_private_flag;
    
    Find_invite_friend_iPad *find_inviteViewObj;
}
@property(nonatomic,retain) Find_invite_friend_iPad *find_inviteViewObj;
@property(nonatomic,retain) Notification_setting_ViewController_iPad *notification_settingViewObj;

@property(nonatomic,retain)IBOutlet UIImageView *img_firstcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_secondcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_thirdcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_fourthcell;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;
@property (nonatomic, retain) IBOutlet DCRoundSwitch *switch_private;
@property (nonatomic,retain)  IBOutlet DCRoundSwitch *swith_photos_save;


@end
