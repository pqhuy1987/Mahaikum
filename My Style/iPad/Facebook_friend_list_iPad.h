//
//  Facebook_friend_list_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "DejalActivityView.h"

@interface Facebook_friend_list_iPad : UIViewController{
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
}

@property(nonatomic,retain)NSMutableArray *array_friend;
@property(nonatomic,retain)IBOutlet  UITableView *tbl_friend;
@property(nonatomic,retain)NSMutableArray *array_fb_id;
@end
