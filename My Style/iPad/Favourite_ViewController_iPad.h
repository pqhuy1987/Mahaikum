//
//  Favourite_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"
#import "News_feed_like_Share.h"
#import "Favourite_news_following_share.h"
#import "Collectionview_delegate.h"
#import "Image_detail_url_iPad.h"
#import "Image_detail_iPad.h"
#import "News_feed_like_following_cell_iPad.h"
#import "News_feed_comment_cell_iPad.h"
#import "Favourite_feed_following_collection_cell_iPad.h"
#import "News_feed_following_cell_iPad.h"
#import "News_feed_like_cell_iPad.h"
#import "MCSegmentedControl.h"

@interface Favourite_ViewController_iPad : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{
    
    Image_detail_url_iPad *image_detailViewObj;
    
    NSMutableArray *array_news_feed;
    NSMutableArray *array_follow;
    UITableView *tbl_feed;
    UITableView *tbl_news;
//    MCSegmentedControl *segment;
    UISegmentedControl *segment;
    Image_detail_iPad  *image_detail_viewObj;
    
    LORichTextLabel *lbl_temp;
    LORichTextLabel *lbl_temp2;
    LORichTextLabelStyle *userStyle1;
}
@property(nonatomic,retain) NSMutableArray *array_news_feed;
@property(nonatomic,retain) NSMutableArray *array_follow;
@property(nonatomic,retain) IBOutlet UITableView *tbl_feed;
@property(nonatomic,retain) IBOutlet UITableView *tbl_news;
//@property(nonatomic,retain) IBOutlet MCSegmentedControl *segment;
@property(nonatomic,retain) IBOutlet UISegmentedControl *segment;
@property(nonatomic,retain) Image_detail_url_iPad *image_detailViewObj;
@property(nonatomic,retain) Image_detail_iPad  *image_detail_viewObj;


@property(nonatomic,retain) LORichTextLabel *lbl_temp;
@property(nonatomic,retain) LORichTextLabel *lbl_temp2;
@property(nonatomic,retain) LORichTextLabelStyle *userStyle1;

@end
