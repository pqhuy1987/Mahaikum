//
//  Show_location_list_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "DejalActivityView.h"
#import "Singleton.h"
@interface Show_location_list_iPad : UIViewController <RKObjectLoaderDelegate>{
    
    UITableView *tbl_location;
}
@property(nonatomic,retain)IBOutlet UITableView *tbl_location;

@end
