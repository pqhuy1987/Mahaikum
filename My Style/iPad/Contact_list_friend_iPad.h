//
//  Contact_list_friend_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"

#import "DejalActivityView.h"
#import <AddressBook/AddressBook.h>
#import "AJNotificationView.h"
#import "Suggested_friend_list_reg_iPad.h"

@interface Contact_list_friend_iPad : UIViewController{
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
    Suggested_friend_list_reg_iPad *suggested_viewObj;
    NSMutableArray *array_email;
}

@property(nonatomic,retain) Suggested_friend_list_reg_iPad *suggested_viewObj;
@property(nonatomic,retain)NSMutableArray *array_friend;
@property(nonatomic,retain)IBOutlet  UITableView *tbl_friend;
@property(nonatomic,retain)NSMutableArray *array_email;

@end