//
//  Sign_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Sign_ViewController_iPad.h"

#import "AppDelegate.h"

@interface Sign_ViewController_iPad ()

@end

@implementation Sign_ViewController_iPad



@synthesize tabbar,img_login_bg,activityViewObj;
@synthesize img_StartLoing,lblLogin;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    self.tabbar.delegate = self;
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.img_login_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_login_bg.layer.cornerRadius =4.0f;
    
    _register_viewObj=[[Register_ViewController_iPad alloc]initWithNibName:@"Register_ViewController_iPad" bundle:nil];
    _forgot_password_viewObj=[[Forgot_password_ViewController_iPad alloc]initWithNibName:@"Forgot_password_ViewController_iPad" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"111011" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_login_allData:) name:@"111011" object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"-111011" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailReson:) name:@"-1" object:nil];
    
    //forgot_password_click
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"forgot_password_click" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forgot_password_click:) name:@"forgot_password_click" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"go_to_home_view" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_home_view:) name:@"go_to_home_view" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"login_with_facebook_click" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login_with_facebook_click:) name:@"login_with_facebook_click" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"login_with_twitter_click" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login_with_twitter_click:) name:@"login_with_twitter_click" object:nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    if ([[StaticClass retrieveFromUserDefaults:USER_IS_LOGIN] isEqualToString:@"0"]) {
        [activityViewObj stopAnimating];
        activityViewObj.hidden = YES;
        img_StartLoing.hidden = YES;
        lblLogin.hidden = YES;
    }
    else {
        NSString *strName=[StaticClass retrieveFromUserDefaults:@"STOREUSERNAME"];
        NSString *strPassword=[StaticClass retrieveFromUserDefaults:@"STOREPASSWORD"];
        
        if (![strName isEqualToString:@""]&&strName!=nil&&![strPassword isEqualToString:@""]&&strPassword!=nil) {
            [activityViewObj startAnimating];
            activityViewObj.hidden = NO;
            img_StartLoing.hidden = NO;
            lblLogin.hidden = NO;

            self.view.userInteractionEnabled = NO;
            
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = @"coco";
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            NSString *myRequestString = [NSString stringWithFormat:@"salt=%@&sign=%@&username=%@&password=%@&device_token=%@",salt,sig,strName,strPassword,[StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"]];
            NSString *requestStr = [NSString stringWithFormat:@"%@get_login.php?%@",[[Singleton sharedSingleton] getBaseURL],[myRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"%@",requestStr);
            AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
            [networkQueue queueItems :requestStr:@"111011":nil];
        }
        else {
            [activityViewObj stopAnimating];
            activityViewObj.hidden = YES;
            img_StartLoing.hidden = YES;
            lblLogin.hidden = YES;
        }

    }    
}

-(void)login_with_facebook_click:(NSNotification *)notification {
    NSLog(@"Login in with facebook");
    [self get_user_photo_facebok];
}

-(void)login_with_twitter_click:(NSNotification *)notification {
    NSLog(@"Login in with twitter");
}

#pragma mark Response

-(void)get_login_allData:(NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];

    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSDictionary *data =[result valueForKey:@"data"];
    NSLog(@"%@",data);
    if ([[result valueForKey:@"success"]isEqualToString:@"-3" ]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"invalid login credentails" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    
    if ([[result valueForKey:@"success"]isEqualToString:@"-2" ]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Your account is suspended" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    if ([[result valueForKey:@"success"]isEqualToString:@"1" ]) {
        //close_popup
        [StaticClass saveToUserDefaults:[data valueForKey:@"user_id"] :USER_ID];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"username"]] :USERNAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"name"]] :USER_NAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"image"]] :USER_IMAGE];
        
        [StaticClass saveToUserDefaults:[data valueForKey:@"is_private"] :@"ISPRIVATEPHOTOORNOT"];
        
        NSLog(@"%@",data);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"close_popup" object:nil];
        [StaticClass saveToUserDefaults:@"1" :USER_IS_LOGIN];
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(goTOHOMEPUSH) userInfo:self repeats:NO];
    }
}

-(void)goTOHOMEPUSH {
    [self go_to_home_view];
}

-(void)go_to_home_view {
    
    int sure;
    @try {
        [tabbar setSelectedIndex:0];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"REVMOBCHARTBOOSTADDNOTIFICATION" object:self];
        [self.navigationController pushViewController:tabbar animated:YES];
        sure =0;
    }
    @catch (NSException * ex) {
        //“Pushing the same view controller instance more than once is not supported”
        //NSInvalidArgumentException
        NSLog(@"Exception: [%@]:%@",[ex  class], ex );
        NSLog(@"ex.name:'%@'", ex.name);
        NSLog(@"ex.reason:'%@'", ex.reason);
        //Full error includes class pointer address so only care if it starts with this error
        NSRange range = [ex.reason rangeOfString:@"Pushing the same view controller instance more than once is not supported"];
        
        if([ex.name isEqualToString:@"NSInvalidArgumentException"] &&
           range.location != NSNotFound)
        {
            //view controller already exists in the stack - just pop back to it
            [StaticClass saveToUserDefaults:@"1" :IS_HOME_PAGE_REFRESH];
            [tabbar setSelectedIndex:0];
            [self.navigationController popToViewController:tabbar animated:NO];
            sure =1;
        }else{
            NSLog(@"ERROR:UNHANDLED EXCEPTION TYPE:%@", ex);
        }
    }
    @finally {
        NSLog(@"finally");
        if (sure==1) {
            //    [self.navigationController popToViewController:tabbar animated:NO];
            sure=0;
        }
    }
}

-(void)FailReson:(NSNotification *)notification {
	//[self stopSpinner];
    [activityViewObj stopAnimating];
    activityViewObj.hidden = YES;
    img_StartLoing.hidden = YES;
    lblLogin.hidden = YES;
    
    self.view.userInteractionEnabled = YES;
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"There was a problem with the Internet connection. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(IBAction)btn_sign_click:(id)sender {
    CustomAlertView *ddAlert =[[CustomAlertView alloc]initWithDelegate:self theme:DDSocialDialogThemePlurk alertImage:nil];
    [ddAlert show];
    //  [ddAlert release];
}

-(IBAction)btn_register_click:(id)sender {
  /*  //   [self.navigationController pushViewController:register_viewObj animated:YES];
      Facebook_Find_Friends_ViewController_iPad *tempObj =[[Facebook_Find_Friends_ViewController_iPad alloc]initWithNibName:@"Facebook_Find_Friends_ViewController_iPad" bundle:nil];
       [self.navigationController pushViewController:tempObj animated:YES];
    
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:tempObj ];
    [self presentViewController:nav animated:YES completion:nil];
    */
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:_register_viewObj ];
    [self presentViewController:nav animated:YES completion:nil];
    
    //    Detail_ViewController *obj =[[Detail_ViewController alloc]initWithNibName:@"Detail_ViewController" bundle:nil];
    //    [self.navigationController pushViewController:obj animated:YES];
}

-(void)forgot_password_click:(NSNotification *)notification {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:_forgot_password_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    
}

-(void)go_to_home_view:(NSNotification *)notification {
    [self go_to_home_view];
}

#pragma mark Facebook Login
#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok {
    activityViewObj.hidden=NO;
    [activityViewObj startAnimating];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        //  appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}

-(void)apiFQLI_pic_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex,username FROM user WHERE uid=me()";
    //   NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        } else {
            
            FBGraphObject *dictionary = (FBGraphObject *)result;
            NSLog(@"%@",[dictionary valueForKey:@"data"]);
            NSArray *tempArray =[dictionary valueForKey:@"data"];
            
            
            
            //ttp://techintegrity.in/mystyle/get_facebook_login.php?salt=123&sign=cf59c04c88841a4e5e0d0c9b60230cbb&name=hitesh%20parekh&username=hitesh&facebook_id=123456&email=hitesh.bece@gmail.com
            
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = @"coco";
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14235" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFacebookLoginAPIResponce:) name:@"14235" object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14235" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetFacebookLoginAPIResponce:) name:@"-14235" object:nil];
            
            
            NSString *requestStr =[NSString stringWithFormat:@"%@get_facebook_login.php",[[Singleton sharedSingleton] getBaseURL]];
            NSLog(@"requestStr:%@",requestStr);
            
            NSString *strName=@"";
            NSString *strEmail=@"";
            NSString *strUid=@"";
            NSString *strUserName=@"";
            NSString *strGender=@"";
            
            for (FBGraphObject *fbobj in tempArray) {
                
                strName=[fbobj valueForKey:@"name"];
                strEmail=[fbobj valueForKey:@"email"];
                strUid=[fbobj valueForKey:@"uid"];
                
                if ([[fbobj valueForKey:@"username"] isEqualToString:@""]) {
                    strUserName=[fbobj valueForKey:@"email"];
                }else{
                    strUserName=[fbobj valueForKey:@"username"];
                }
                
                if ([[fbobj valueForKey:@"sex"] isEqualToString:@"male"]) {
                    strGender=@"m";
                }else if([[fbobj valueForKey:@"sex"] isEqualToString:@"female"]){
                    strGender=@"f";
                }
            }
            
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sig, @"sign",
                                    salt, @"salt",
                                    strName,@"name",
                                    strEmail,@"email",
                                    strUid,@"facebook_id",
                                    strUserName,@"username",
                                    strGender,@"gender",
                                    [StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"],@"device_token=%@",
                                    nil];
            NSLog(@"params:%@",params);
            AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
            [networkQueue queueItems:requestStr :@"14235" :params];
        }
    };
    [requester addRequest:request completionHandler:handler];
    [requester start];
}


-(void)getFacebookLoginAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14235" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14235" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if([[result valueForKey:@"success"] isEqualToString:@"1"]){
        
        NSDictionary *data =[result valueForKey:@"data"];
        
        [StaticClass saveToUserDefaults:[data valueForKey:@"uid"] :USER_ID];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"username"]] :USERNAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"name"]] :USER_NAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"pic"]] :USER_IMAGE];
        [StaticClass saveToUserDefaults:@"1" :USER_IS_LOGIN];
        [self go_to_home_view];
    }
}

-(void)FailgetFacebookLoginAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14235" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14235" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't login" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [img_login_bg release];
    [tabbar release];
    
    [super dealloc];
}

@end
