//
//  Likers_list_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/25/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
@class NPRImageView;

@interface Likers_list_cell_iPad : UITableViewCell{
    NPRImageView *img_user;
    UILabel *lbl_name;
    UILabel *lbl_decs;
    
}
@property(nonatomic,retain)IBOutlet NPRImageView *img_user;
@property(nonatomic,retain)IBOutlet UILabel *lbl_name;
@property(nonatomic,retain)IBOutlet UILabel *lbl_decs;@end
