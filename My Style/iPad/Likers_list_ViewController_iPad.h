//
//  Likers_list_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Likers_list_cell_iPad.h"
#import "UIImageView+PMRoundEffect_imageview.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "like_share.h"

@interface Likers_list_ViewController_iPad : UIViewController {
    UITableView *tbl_likers;
    NSMutableArray *array_likers;
    NSString *image_id;
}

@property(nonatomic,retain)IBOutlet UITableView *tbl_likers;
@property(nonatomic,retain)NSMutableArray *array_likers;
@property(nonatomic,retain)NSString *image_id;

@end
