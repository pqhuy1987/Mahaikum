//
//  Change_password_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"

@interface Change_password_iPad : UIViewController{

    UITextField *txt_old_password;
    UITextField *txt_new_password;
    UITextField *txt_new_password_again;
    
    UIImageView *img_bd_firstcell;
    UIImageView *img_bd_secondcell;
}
@property(nonatomic,retain)IBOutlet UITextField *txt_old_password;
@property(nonatomic,retain)IBOutlet UITextField *txt_new_password;
@property(nonatomic,retain)IBOutlet UITextField *txt_new_password_again;

@property(nonatomic,retain)IBOutlet UIImageView *img_bd_firstcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bd_secondcell;


@end
