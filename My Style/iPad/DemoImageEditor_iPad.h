//
//  DemoImageEditor_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFImageEditorViewController.h"

@interface DemoImageEditor_iPad : HFImageEditorViewController
@property(nonatomic,retain) IBOutlet UIBarButtonItem *saveButton;

@end
