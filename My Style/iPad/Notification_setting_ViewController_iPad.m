//
//  Notification_setting_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Notification_setting_ViewController_iPad.h"

@interface Notification_setting_ViewController_iPad ()

@end

@implementation Notification_setting_ViewController_iPad
@synthesize tbl_notification,array_notification,dict_notification;
@synthesize tbl_footer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.array_notification =[[NSMutableArray alloc]init];
    self.dict_notification =[[NSMutableDictionary alloc]init];


    
    self.tbl_notification.tableFooterView=self.tbl_footer;
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];

    [self get_notification_info];
}
-(void)get_notification_info{
    //ttp://techintegrity.in/mystyle/get_notification.php?uid=6
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationAPIResponce:) name:@"14220" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetNotificationAPIResponce:) name:@"-14220" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_notification.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14220" :params];
}

-(void)getNotificationAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_notification removeAllObjects];
        
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        NSDictionary *dict =[result valueForKey:@"data"];
        NSLog(@"%d",[[dict valueForKey:@"like"] integerValue]);
        NSMutableArray *array =[[NSMutableArray alloc]initWithCapacity:3];
        for (int i =1; i<4; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"like"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==1) {
                obj.title=@"Off";
            }else if(i==2){
                obj.title=@"From People I Follow";
            }else{
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"Like Notifications";
            [array addObject:obj];
        }
        [self.array_notification addObject:array];
        
        NSMutableArray *array1 =[[NSMutableArray alloc]initWithCapacity:3];
        for (int i =1; i<4; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"comment"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==1) {
                obj.title=@"Off";
            }else if(i==2){
                obj.title=@"From People I Follow";
            }else{
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"Comment Notifications";
            [array1 addObject:obj];
        }
        [self.array_notification addObject:array1];
        
        NSMutableArray *array2=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =1; i<3; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"contact"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==1) {
                obj.title=@"Off";
            }else {
                obj.title=@"All New Contacts";
            }
            obj.notification_type=@"Contact Notifications";
            [array2 addObject:obj];
        }
        [self.array_notification addObject:array2];
        //contact
        
    }
}

-(void)FailgetNotificationAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.array_notification.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 100.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSMutableArray *array =[self.array_notification objectAtIndex:section];
    Notification_setting_share *obj =[array objectAtIndex:0];
    //    notification_type first;
    //    [[array objectAtIndex:section] getBytes:&first length:sizeof(first)];
    
    UILabel *lbl_title =[[UILabel alloc]initWithFrame:CGRectMake(94,30,500, 60)];
    lbl_title.text=obj.notification_type;
    lbl_title.backgroundColor =[UIColor clearColor];
    [lbl_title setFont:[UIFont boldSystemFontOfSize:30.0f]];
    // lbl_title.textColor =[UIColor colorWithRed:193.0f/255.0f green:209.0f/255.0f blue:222.0f/255.0f alpha:1];
    lbl_title.textColor =[UIColor whiteColor];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,768 , 100)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =0.9;
    [view addSubview:lbl_title];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return 88.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==2) {
        return 2;
    }
    return 3;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row!=0) {
        
        static NSString *CellIdentifier = @"Notification_setting_cell_iPad";
        Notification_setting_cell_iPad *cell = (Notification_setting_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)	{
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Notification_setting_cell_iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png.png"]];
        }
        
        NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
        Notification_setting_share *obj =[array objectAtIndex:indexPath.row];
        cell.lbl_title.text =obj.title;
        if ([obj.is_checked isEqualToString:@"1"]) {
            [cell.img_checkmark setHidden:NO];
        }else{
            [cell.img_checkmark setHidden:YES];
        }
        return cell;
    }
    else{
        static NSString *CellIdentifier = @"Notification_setting_on_off_cell_iPad";
        Notification_setting_on_off_cell_iPad *cell = (Notification_setting_on_off_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)	{
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Notification_setting_on_off_cell_iPad" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
            
            [cell.switch_on_off addTarget:self action:@selector(btn_switch_on_off_click:) forControlEvents:UIControlEventValueChanged];
            cell.switch_on_off.onText = NSLocalizedString(@"ON", @"");
            cell.switch_on_off.offText = NSLocalizedString(@"OFF", @"");
            cell.switch_on_off.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
        }
        cell.switch_on_off.tag =indexPath.section;
        NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
        Notification_setting_share *obj =[array objectAtIndex:indexPath.row];
        
        if ([obj.is_checked isEqualToString:@"1"]) {
            
            [cell.switch_on_off setOn:NO animated:NO];
        }else{
            
            [cell.switch_on_off setOn:YES animated:NO];
        }
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row !=0) {
        
        NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
        Notification_setting_share *temObj =[array objectAtIndex:indexPath.row];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:indexPath.section withObject:temparray];
        [self.tbl_notification reloadData];
        [self update_notification_seting:indexPath];
    }
}

#pragma mark - Update notification setting
-(void)update_notification_seting:(NSIndexPath *)indexpath{
    
    
    NSString *type;
    
    switch (indexpath.section) {
        case 0:{
            type=@"like";
            break;
        }
        case 1:{
            type=@"comment";
            break;
        }
        default:{
            type=@"contact";
            break;
        }
            
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNotificationAPIResponce:) name:@"14221" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsetNotificationAPIResponce:) name:@"-14221" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@set_notification.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            [NSString stringWithFormat:@"%d",indexpath.row+1], @"value",
                            type,@"ntype",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14221" :params];
}

-(void)setNotificationAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"success");
    }
    [self.tbl_notification reloadData];
}

-(void)FailsetNotificationAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}


-(IBAction)btn_switch_on_off_click:(id)sender{
    UISwitch *btn_switch =(UISwitch *)sender;
    int tag =btn_switch.tag;
    if (!btn_switch.on) {
        NSMutableArray *array =[self.array_notification objectAtIndex:tag];
        Notification_setting_share *temObj =[array objectAtIndex:0];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
        [self.tbl_notification reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:tag];
        [self update_notification_seting:indexPath];
    }else{
        
        NSMutableArray *array =[self.array_notification objectAtIndex:tag];
        Notification_setting_share *temObj =[array objectAtIndex:0];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"0";
            }else{
                if ([obj isEqual:[array lastObject]]) {
                    obj.is_checked=@"1";
                }else{
                    obj.is_checked=@"0";
                }
                
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
        [self.tbl_notification reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(array.count-1) inSection:tag];
        [self update_notification_seting:indexPath];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [tbl_footer release];
    [dict_notification release];
    [tbl_notification release];
    [array_notification release];
    [super dealloc];
}
@end
