//
//  Favourite_feed_following_collection_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Favourite_feed_following_collection_cell_iPad.h"

@interface Favourite_feed_following_collection_cell_iPad ()

@end

@implementation Favourite_feed_following_collection_cell_iPad

@synthesize btn_user_img,lbl_time,lbl_dec,btn_user_img1;

-(void)dealloc{
    [btn_user_img1 release];
    [lbl_time release];
    [btn_user_img release];
    [super dealloc];
}

-(void)draw_collectionview_in_cell{
    
    
    
    self.lbl_dec = [[LORichTextLabel alloc] initWithWidth:650];
	[self.lbl_dec setFont:[UIFont fontWithName:@"Helvetica" size:32.0]];
	//[self.lbl_dec setTextColor:[UIColor colorWithRed:30.0f/255.0f green:48.0f/255.0f blue:62.0f/255.0f alpha:1]];
    [self.lbl_dec setTextColor:[UIColor whiteColor]];
	[self.lbl_dec setBackgroundColor:[UIColor clearColor]];
	[self.lbl_dec positionAtX:110.0 andY:10.0];
    [self.contentView addSubview:self.lbl_dec];
    
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    layout.minimumLineSpacing = 10.0f;
    layout.minimumInteritemSpacing =1.0f;
    layout.itemSize = CGSizeMake(100,100);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[Collectionview_delegate alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    self.collectionView.backgroundColor = [UIColor clearColor];
    //  self.collectionView.showsHorizontalScrollIndicator = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    [self.contentView addSubview:self.collectionView];
}
/*
 -(void)layoutSubviews
 {
 [super layoutSubviews];
 
 self.collectionView.frame = CGRectMake(0, 50, 320, 100);
 }
 */
-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.index = index;
    
    [self.collectionView reloadData];
}



@end
