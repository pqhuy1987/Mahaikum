//
//  Likers_list_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Likers_list_ViewController_iPad.h"
#import "User_info_ViewController_iPad.h"

@interface Likers_list_ViewController_iPad (){
    User_info_ViewController_iPad *user_info_view;
}

@end



@implementation Likers_list_ViewController_iPad

@synthesize tbl_likers,array_likers,image_id;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.array_likers =[[NSMutableArray alloc]init];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)get_user_liker_list {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //ttp://mystyleapp.ca/api/get_image_likes.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&id=42
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14219" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getImageLikesResponce:) name:@"14219" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14219" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetImageLikesResponce:) name:@"-14219" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_image_likes.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            @"like",@"action",
                            self.image_id,@"id",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14219" :params];
}

-(void)getImageLikesResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14219" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14219" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_likers removeAllObjects];
        for (NSDictionary *obj in [result valueForKey:@"data"]) {
            like_share *shareobj = [[like_share alloc]init];
            shareobj.username =[obj valueForKey:@"username"];
            shareobj.name =[StaticClass urlDecode:[obj valueForKey:@"name"]];
            shareobj.img_path =[StaticClass urlDecode:[obj valueForKey:@"user_image"]];
            shareobj.uid =[obj valueForKey:@"uid"];
            
            [self.array_likers addObject:shareobj];
            [shareobj release];
        }
    }
    [self.tbl_likers reloadData];
}

-(void)FailgetImageLikesResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14219" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14219" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tbl_likers reloadData];
    [self get_user_liker_list];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_likers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Likers_list_cell_iPad";
	Likers_list_cell_iPad *cell = (Likers_list_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Likers_list_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
	}
    like_share  *shareObj =[self.array_likers objectAtIndex:indexPath.row];
    cell.lbl_name.text =shareObj.name;
    cell.lbl_decs.text =shareObj.username;
    [cell.img_user setImageWithURL:[NSURL URLWithString:shareObj.img_path] placeholderImage:nil];
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    like_share *obj =[self.array_likers objectAtIndex:indexPath.row];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [image_id release];
    [tbl_likers release];
    [array_likers release];
    [super dealloc];
}

@end
