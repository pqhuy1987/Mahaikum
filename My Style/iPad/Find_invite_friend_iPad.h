//
//  Find_invite_friend_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook_friend_list_iPad.h"
#import "Contact_friend_list_iPad.h"
#import "Suggested_friend_list_iPad.h"

@interface Find_invite_friend_iPad : UIViewController{

    UIImageView *img_cell_bg;
    Facebook_friend_list_iPad *facebook_friendViewObj;
    Contact_friend_list_iPad *contact_friendViewObj;
    Suggested_friend_list_iPad *suggested_friend_list;
}
@property(nonatomic,retain)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,retain)Facebook_friend_list_iPad *facebook_friendViewObj;
@property(nonatomic,retain)Contact_friend_list_iPad *contact_friendViewObj;
@property(nonatomic,retain) Suggested_friend_list_iPad *suggested_friend_list;


@end
