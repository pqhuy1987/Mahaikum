//
//  Hash_tag_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_cell_iPad.h"
#import "BlockActionSheet.h"
#import "Likers_list_ViewController_iPad.h"
#import "IFTweetLabel.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "Comment_share.h"
#import "Comments_list_ViewController_iPad.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Share_photo_iPad.h"

#import "image_collection_cell.h"
#import "image_collection_header.h"
#import "Image_detail_iPad.h"

@interface Hash_tag_ViewController_iPad : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate>{
    
    UITableView *tbl_news;
    Likers_list_ViewController_iPad *liker_list_viewObj;
    Image_detail_iPad *image_detail_viewObj;
    
    NSMutableArray *array_feeds;
    
    int which_image_liked;
    int which_image_delete;
    UIView *imgfullscreenviewobj;
    
    UIImageView *img_like_heart;
    Comments_list_ViewController_iPad *comments_list_viewObj;
    Share_photo_iPad *share_photo_view;
    
    UICollectionView *collectionViewObj;
    IBOutlet UIView *view_header;
    IBOutlet UIView *view_header_tbl;
    
    UIImageView *img_down_arrow;
    UIImageView *img_down_arrow_tbl;
    
    UIImageView *img_header_bg;
    UIImageView *img_header_bg_tbl;
    
    UILabel *lbl_title;
    NSString *str_title;
    
    UILabel *lbl_photos_count;
    UILabel *lbl_photos_count_tbl;
    
    
}
@property(nonatomic,retain)IBOutlet UILabel *lbl_photos_count;
@property(nonatomic,retain)IBOutlet UILabel *lbl_photos_count_tbl;

@property(nonatomic,retain)IBOutlet UILabel *lbl_title;
@property(nonatomic,retain) NSString *str_title;
@property(nonatomic,retain)IBOutlet UIImageView *img_header_bg;
@property(nonatomic,retain)IBOutlet UIImageView *img_header_bg_tbl;
@property(nonatomic,retain)IBOutlet UIImageView *img_down_arrow;
@property(nonatomic,retain)IBOutlet UIImageView *img_down_arrow_tbl;
@property(nonatomic,retain)IBOutlet UIView *imgfullscreenviewobj;
@property(nonatomic,retain) IBOutlet UIView *view_header;
@property(nonatomic,retain) IBOutlet UIView *view_header_tbl;
@property(nonatomic,retain)IBOutlet UICollectionView *collectionViewObj;

@property(nonatomic,retain)Share_photo_iPad *share_photo_view;

@property(nonatomic,retain)Comments_list_ViewController_iPad *comments_list_viewObj;
@property(nonatomic,retain)Image_detail_iPad *image_detail_viewObj;
@property(nonatomic,retain)IBOutlet  UIImageView *img_like_heart;

@property(nonatomic,retain)IBOutlet UITableView *tbl_news;
@property(nonatomic,retain)Likers_list_ViewController_iPad *liker_list_viewObj;
@property(nonatomic,retain)NSMutableArray *array_feeds;



@end
