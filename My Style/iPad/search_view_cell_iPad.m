//
//  search_view_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "search_view_cell_iPad.h"


@implementation search_view_cell_iPad

@synthesize img_user,lbl_name,lbl_username;
-(void)dealloc{
    [img_user release];
    [lbl_name release];
    [lbl_username release];
    [super dealloc];
}
@end
