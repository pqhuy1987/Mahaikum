//
//  Facebook_Find_Friends_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "Facebook_follow_frnd_list_iPad.h"
#import "Contact_list_friend_iPad.h"
#import "Suggested_friend_list_reg_iPad.h"

@interface Facebook_Find_Friends_ViewController_iPad : UIViewController{

    UIView *view_find_friend;
    UITableView *tbl_friend;
    NSMutableArray *array_friend;
    NSMutableArray *array_frnd;
    
    UIImageView *img_cell_bg;
    
    Suggested_friend_list_reg_iPad *suggested_viewObj;
}

@property(nonatomic,retain) Suggested_friend_list_reg_iPad *suggested_viewObj;
@property(nonatomic,retain) NSMutableArray *array_frnd;
@property(nonatomic,retain)NSMutableArray *array_friend;
@property(nonatomic,retain)IBOutlet  UIView *view_find_friend;
@property(nonatomic,retain)IBOutlet  UITableView *tbl_friend;
@property(nonatomic,retain)IBOutlet  UIImageView *img_cell_bg;

@end
