//
//  Image_detail_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Image_detail_iPad.h"
#import "User_info_ViewController_iPad.h"
#import "webview_viewcontroller_iPad.h"
#import "Hash_tag_ViewController_iPad.h"
#import "NPRImageView.h"

@interface Image_detail_iPad (){
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
    User_info_ViewController_iPad *user_info_view;
    webview_viewcontroller_iPad *web_viewObj;
}

@end

@implementation Image_detail_iPad

@synthesize shareObj,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view;
@synthesize tbl_news,imgfullscreenviewobj;
@synthesize arrayFeedArray,currentSelectedIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.img_like_heart setHidden:YES];
    
    hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    web_viewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.share_photo_view=[[Share_photo_iPad alloc]initWithNibName:@"Share_photo_iPad" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController_iPad alloc]initWithNibName:@"Comments_list_ViewController_iPad" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController_iPad alloc]initWithNibName:@"Likers_list_ViewController_iPad" bundle:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"400" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-400" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"401" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-401" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"402" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-402" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"403" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-403" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"404" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-404" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDecsNotification:) name:IFTweetLabelURLNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.shareObj=[self.arrayFeedArray objectAtIndex:currentSelectedIndex];
    
    [self.tbl_news setContentOffset:CGPointMake(0, 0)];
    [self.tbl_news reloadData];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@",self.shareObj.username,self.shareObj.description]);
}
#pragma mark - UITableview Delegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 120.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
    NPRImageView *img_user =[[[NPRImageView alloc]initWithFrame:CGRectMake(10, 30, 80, 80)]autorelease];
    [img_user setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    
    EGOImageButton *img_userimage=[[[EGOImageButton alloc]initWithFrame:CGRectMake(10,30, 80,80)] autorelease];
   // img_userimage.imageURL =[NSURL URLWithString:self.shareObj.user_image];
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:self.shareObj.username forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(100,45, 550, 50)];
    [btn_username setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"locationicon@2x.png"]];
    img_locationicon.frame = CGRectMake(100,73, 32, 40);
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:self.shareObj.location forState:UIControlStateNormal];
    
    
    if (self.shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(100,25, 550, 50)];
        [btn_location setFrame:CGRectMake(140,70,500, 50)];
        img_locationicon.hidden=NO;
        
    }else{
        img_locationicon.hidden=YES;
    }
    
    [btn_location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_location.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //   [btn_location addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon@2x.png"]];
    
    [img_clock setFrame:CGRectMake(650, 64, 32,32)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:self.shareObj.datecreated];
    lbl_time.font = [UIFont boldSystemFontOfSize:30];
    lbl_time.textColor =[UIColor whiteColor];
    lbl_time.frame =CGRectMake(688, 64,80,32);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,768, 120)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =0.9;
    [view addSubview:img_user];
    [view addSubview:lbl_time];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return [Home_tableview_cell_iPad get_tableview_hight:self.shareObj];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Home_tableview_cell_iPad";
	Home_tableview_cell_iPad *cell = (Home_tableview_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        [cell setBackgroundColor:[UIColor clearColor]];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
        
        [cell.img_big addGestureRecognizer:tapImage];
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
        
	}
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    [cell redraw_cell:self.shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    cell.dlstarObj.delegate = self;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Method for Stop Header scrolling in UITableview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    if ([self.shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            [alert release];
            
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            NSLog(@"");
            self.share_photo_view.str_img_url = self.shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            NSLog(@"");
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =self.shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            NSLog(@"");
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"My Style\"></body></html>",self.shareObj.image_path];
                NSLog(@"%@",htmlStr);
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"I don't like this photo",@"This photo is spam or a scam",@"This photo puts people at risk",@"This photo shouldn't be on mystyle", nil];
        alert.tag =2;
        [alert show];
        [alert release];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    /*  [sheet addButtonWithTitle:@"Show Action Sheet on top" block:^{
     NSLog(@"");
     }];
     [sheet addButtonWithTitle:@"Show another alert" block:^{
     NSLog(@"");
     }];
     */
    [sheet showInView:self.view];
}
-(void)report_inappropriate:(int )index{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //post_report_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&report_id=2&item_id=12&uid=1
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"403":nil];
}

#pragma mark - List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    self.liker_list_viewObj.image_id =self.shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

- (void)handleDecsNotification:(NSNotification *)notification
{
	NSLog(@" %@ Click", [notification object]);
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(void)handlelikeButtonNotification:(NSNotification *)notification{
    
    NSLog(@" %@ Click", [notification object]);
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark - Go to Profile

-(IBAction)btn_profile_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    NSLog(@"%d",tag);
    
    user_info_view.user_id=self.shareObj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark - UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:buttonIndex];
    }
}

#pragma mark - Delete Photo
-(void)delete_photo{
    //     http://www.techintegrity.in/mystyle/post_delete_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,self.shareObj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"402":nil];
}

-(void)getdeleteResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        //  [self get_news_feed];
    }
}

#pragma mark - Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    //  http://www.techintegrity.in/mystyle/post_rating.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&rate=2&item_id=12&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%d&item_id=%@&uid=%@",salt,sig,rating,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"403":nil];
    self.shareObj.my_rating = [NSString stringWithFormat:@"%d",rating];
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    // [dlstarObj  setRating:3];
}

-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
    }
}

#pragma mark - like unlike call
-(IBAction)btn_like_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    
    if (![self.shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"like"];
    }
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"401":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        if ([self.shareObj.liked isEqualToString:@"no"]) {
            self.shareObj.liked=@"yes";
            int count =[self.shareObj.likes intValue];
            count++;
            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }else{
            self.shareObj.liked=@"no";
            int count =[self.shareObj.likes intValue];
            count--;
            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
                [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            }
        }
        
        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
}

// single tap for fullscreen image
-(IBAction)handleSingleTap:(UITapGestureRecognizer *)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    which_image_liked=tappedRow.section;
    
//    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
    //    UIImageView *fullscreenimgview = [[UIImageView alloc] initWithImage:shareObj.image_id];
    NPRImageView *nprfullscreenimg = [[NPRImageView alloc] init];
    
    [nprfullscreenimg setImageWithContentsOfURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:Nil];
    
    nprfullscreenimg.contentMode = UIViewContentModeScaleToFill;
    
    nprfullscreenimg.frame = CGRectMake(0, 20, 320, 418);
    
    UIButton *btnclose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnclose setFrame:CGRectMake(290, 30, 26, 26)];
    [btnclose setImage:[UIImage imageNamed:[NSString stringWithFormat:@"closebtn_pop.png"]] forState:UIControlStateNormal];//with image
    [btnclose addTarget:self action:@selector(btnclosefullscreenClick) forControlEvents:UIControlEventTouchUpInside];
    
    imgfullscreenviewobj = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    [imgfullscreenviewobj autorelease];
    [imgfullscreenviewobj setBackgroundColor:[UIColor blackColor]];
    imgfullscreenviewobj.alpha =1.0;
    
    [self.view addSubview:imgfullscreenviewobj];
    [imgfullscreenviewobj addSubview:nprfullscreenimg];
    [imgfullscreenviewobj addSubview:btnclose];
}

-(IBAction)btnclosefullscreenClick{
    [imgfullscreenviewobj removeFromSuperview];
}


#pragma mark - Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked=tappedRow.section;
    
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"like"];
    }
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    // NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"404":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        if ([self.shareObj.liked isEqualToString:@"no"]) {
            self.shareObj.liked=@"yes";
            int count =[self.shareObj.likes intValue];
            count++;
            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            NSLog(@"%@",self.shareObj.array_liked_by);
            NSLog(@"%@",self.shareObj.likes);
            
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        }
    }
}

#pragma mark - Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}
- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mark - Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
   // self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=self.shareObj.image_id;
    self.comments_list_viewObj.array_comments=self.shareObj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    //  [self.navigationController pushViewController:self.comments_list_viewObj animated:YES];
}

#pragma mark - Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    //    if (result == MFMailComposeResultSent) {
    //
    //    }
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)FailNewsReson:(NSNotification *)notification {
	//[self stopSpinner];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}
#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    [dateFormatter release];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [tbl_news release];
    [liker_list_viewObj release];
    [comments_list_viewObj release];
    [share_photo_view release];
    
    [img_like_heart release];
    [shareObj release];
    [super dealloc];
}
@end
