//
//  aCameraMoreViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "Share_photo_ViewController_iPad.h"
#import "AFPhotoEditorController.h"

#import "CustomImageEditor.h"


//for all photos
#include <AssetsLibrary/AssetsLibrary.h>
#import "image_collection_cell.h"

#import "ELCImagePickerController.h"

//<ELCImagePickerControllerDelegate, UINavigationControllerDelegate
@interface aCameraMoreViewController_iPad : UIViewController
<AFPhotoEditorControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ELCImagePickerControllerDelegate> {
    UICollectionView *collectionViewObj;
    
    
    ALAssetsLibrary *library;
    NSArray *imageArray;
    NSMutableArray *mutableArray;
    
	UIImage *workingImage;              // im use this for filters&save
    UIImage *sourceImage;               // this is a selected image or camera image
	IBOutlet UIImageView *imageView;    // UIImageView
    IBOutlet UIScrollView *scrollView; // scroll buttons
    GPUImageOutput<GPUImageInput> *filter;
    
    Share_photo_ViewController_iPad *share_photo_viewObj;
    int flag;
    
    UIImagePickerController *imagepicker;
}
@property(nonatomic, retain) AFPhotoEditorController *editorViewController;


@property(nonatomic,retain) UIImagePickerController *imagepicker;
@property(nonatomic,retain) Share_photo_ViewController_iPad *share_photo_viewObj;

@property (nonatomic, retain) UIImage *workingImage;             // im use this for filters&save
@property (nonatomic, retain) UIImage *sourceImage;              // this is a selected image or camera image
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property ( nonatomic , retain )  IBOutlet UIScrollView *scrollView; // scroll buttons


- (IBAction) chooseImageFromAlbum:(id) sender;      // activate Album
- (IBAction) takeImageFromCamera:(id) sender;       // activate Camera only works on iphone
- (IBAction) SendingImage:(id)sender;               // activate Share module, remember read readme for configuration


-(void)createMenuWithButtonSize:(CGSize)buttonSize withOffset:(CGFloat)offset noOfButtons:(int)totalNoOfButtons;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *btn_done;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *btn_camera;
@property(nonatomic,retain)IBOutlet UIBarButtonItem *btn_photo_libary;


@property(nonatomic,retain)IBOutlet UIView *view_photo_lib;
@property(nonatomic,retain)IBOutlet UICollectionView *collectionViewObj;

@end
