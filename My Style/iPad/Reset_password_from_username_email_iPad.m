//
//  Reset_password_from_username_email_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Reset_password_from_username_email_iPad.h"

@interface Reset_password_from_username_email_iPad ()

@end

@implementation Reset_password_from_username_email_iPad
@synthesize img_cell_bg,txt_username;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =8.0f;
    
    _thro_email_viewObj=[[Reset_password_throw_email_iPad alloc]initWithNibName:@"Reset_password_throw_email_iPad" bundle:nil];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self.txt_username becomeFirstResponder];
}
-(IBAction)btn_search_click:(id)sender{
    [self search_username];
}

-(void)search_username{
    if ([self.txt_username.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length >0) {
        [self.view endEditing:YES];
        NSLog(@"search call");
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = @"coco";
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14227" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAccountDetailsResponce:) name:@"14227" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14227" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetAccountDetailsResponce:) name:@"-14227" object:nil];
        
        NSString *requestStr = [NSString stringWithFormat:@"%@get_account_details.php",[[Singleton sharedSingleton] getBaseURL]];
        NSLog(@"requestStr:%@",requestStr);
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                self.txt_username.text,@"key",
                                nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14227" :params];
    }
}

-(void)getAccountDetailsResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14227" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14227" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-1"]) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Account Found" message:@"Sorry, we could not find any matching accounts." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"reset throw email");
        NSDictionary *dict =[result valueForKey:@"data"];
        
        self.thro_email_viewObj.str_username =[dict valueForKey:@"user_name"];
        self.thro_email_viewObj.str_image_url =[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
        self.thro_email_viewObj.str_email =[StaticClass urlDecode:[dict valueForKey:@"email"]];
        [self.navigationController pushViewController:self.thro_email_viewObj animated:YES];
    }
}

-(void)FailgetAccountDetailsResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14227" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14227" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self search_username];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [_thro_email_viewObj release];
    [txt_username release];
    [img_cell_bg release];
    [super dealloc];
}
@end