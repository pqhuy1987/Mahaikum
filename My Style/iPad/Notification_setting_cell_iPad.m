//
//  Notification_setting_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Notification_setting_cell_iPad.h"


@implementation Notification_setting_cell_iPad

@synthesize lbl_title,img_checkmark,img_bg;

-(void)dealloc{
    [img_bg release];
    [lbl_title release];
    [img_checkmark release];
    [super dealloc];
}
@end
