//
//  Show_map_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Show_map_ViewController_iPad.h"
#import "CustomMapItem.h"
#import "CustomAnnotationView.h"

@interface Show_map_ViewController_iPad ()

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSMutableArray *mapAnnotations;
@end

@implementation Show_map_ViewController_iPad
@synthesize array_data,view_image,collectionViewObj;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)btn_back_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _img_bg_collection.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    _img_bg_collection.layer.cornerRadius = 4.0f;
    
    [self.view_image setHidden:YES];
    [_view_show_image setHidden:YES];
    
    self.mapAnnotations = [[NSMutableArray alloc] init];
    
    _image_detail_viewObj =[[Image_detail_iPad alloc]initWithNibName:@"Image_detail_iPad" bundle:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    
    
    _img_zoom.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    _img_zoom.layer.cornerRadius =4.0f;
    ;
    
    //  [self gotoLocation];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
        self.view_image.frame = CGRectMake(0, 0,self.view_image.frame.size.width, self.view_image.frame.size.height);

    self.view_show_image.frame = CGRectMake(0, 0,self.view_show_image.frame.size.width, self.view_show_image.frame.size.height);
    // Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    int i =0;
    for (Home_tableview_data_share *shareObj in self.array_data) {
        CustomMapItem *item = [[CustomMapItem alloc] init];
        
        
        if ([shareObj.lat floatValue]==0.0 && [shareObj.lng floatValue]==0.0) {
            i++;
            continue;
            item.imageName = shareObj.image_path;
            item.latitude = [NSNumber numberWithDouble:[[Singleton  sharedSingleton]getCurrentLat]];
            item.longitude = [NSNumber numberWithDouble:[[Singleton  sharedSingleton]getCurrentLong]];
            item.index=[NSString stringWithFormat:@"%d",i];
            
            
        }else{
            
            item.imageName = shareObj.image_path;
            item.latitude = [NSNumber numberWithDouble:[shareObj.lat floatValue]];
            item.longitude = [NSNumber numberWithDouble:[shareObj.lng floatValue]];
            item.index=[NSString stringWithFormat:@"%d",i];
        }
        [self.mapAnnotations addObject:item];
        i++;
    }
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = [[Singleton  sharedSingleton]getCurrentLat];
    newRegion.center.longitude = [[Singleton  sharedSingleton]getCurrentLong];
    newRegion.span.latitudeDelta = 0.112872;
    newRegion.span.longitudeDelta = 0.109863;
    
    if (self.mapAnnotations.count==0) {
        CustomAlertView_map *ddAlert =[[CustomAlertView_map alloc]initWithDelegate:self theme:DDSocialDialogThemePlurk alertImage:nil];
        [ddAlert show];
    }
    [self.mapView setRegion:newRegion animated:YES];
    for (CustomMapItem *obj in self.mapAnnotations) {
        [self.mapView addAnnotation:obj];
    }
    
    
}


-(void)remove_annotations_from_map{
    
    [self.mapView removeAnnotations:self.mapView.annotations];
}


#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // in case it's the user location, we already have an annotation, so just return nil
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    static NSString *TeaGardenAnnotationIdentifier = @"TeaGardenAnnotationIdentifier";
    
    CustomAnnotationView *annotationView =
    (CustomAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:TeaGardenAnnotationIdentifier];
    if (annotationView == nil)
    {
        annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:TeaGardenAnnotationIdentifier];
    }
    CustomMapItem *item =(CustomMapItem *)annotation;
    annotationView.tag=[item.index intValue];
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"Click");
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    // [self.view_show_image setHidden:NO];
    Home_tableview_data_share *shareObj =[self.array_data objectAtIndex:view.tag];
    
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = [shareObj.lat floatValue];
    newRegion.center.longitude = [shareObj.lng floatValue];
    newRegion.span.latitudeDelta = 1.0;
    newRegion.span.longitudeDelta = 1.0;
    [self.mapView setRegion:newRegion animated:YES];
    
    _img_photo.imageURL =[NSURL URLWithString:shareObj.image_path];
    _btn_info.tag =view.tag;
    _view_show_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [UIView setAnimationDelegate:self];
    
    _view_show_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    [UIView commitAnimations];
    
    [_view_show_image setHidden:NO];
    _view_show_image.frame = CGRectMake(0, 0, _view_show_image.frame.size.width, _view_show_image.frame.size.height);
    [_view_show_image bringSubviewToFront:self.view];
    
}

#pragma mark - Hide photo viewer
-(IBAction)btn_hide_photo_viewer:(id)sender{
    
    _view_show_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1,1);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [UIView setAnimationDelegate:self];
    
    _view_show_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView commitAnimations];
    
    //   [_view_show_image setHidden:YES];
    
}
-(IBAction)showDetail:(id)sender {
    NSLog(@"Click");
}

-(IBAction)btn_close_view_image:(id)sender{
    
    [self.view_image setHidden:YES];
}


#pragma mark - UICollectionView




- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return self.array_data.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{            return CGSizeMake(180,180);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    Home_tableview_data_share *shareObj =[self.array_data objectAtIndex:indexPath.row];
    //cell.img_photo.imageURL =[NSURL URLWithString:shareObj.image_path];
    [cell.img_photo setImageWithContentsOfURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    cell.tag = indexPath.row;
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _image_detail_viewObj.shareObj =[self.array_data objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_data;
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    
    [self.navigationController pushViewController:_image_detail_viewObj animated:YES];
}


#pragma mark - Go TO Image Detail
-(IBAction)btn_info_click:(id)sender{
    
    NSLog(@"%d",((UIButton *)sender).tag);
    _image_detail_viewObj.shareObj =[self.array_data objectAtIndex:((UIButton *)sender).tag];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_data;
    self.image_detail_viewObj.currentSelectedIndex=((UIButton *)sender).tag;
    
    [self.navigationController pushViewController:_image_detail_viewObj animated:YES];
}

-(IBAction)btn_zoom_out_click:(id)sender{
    
    
    CLLocationCoordinate2D centre = [self.mapView centerCoordinate];
    
    //    CLLocationCoordinate2D centerCoord = { GEORGIA_TECH_LATITUDE, GEORGIA_TECH_LONGITUDE };
    
    [self.mapView setCenterCoordinate:centre zoomLevel:self.mapView.zoomLevel-1 animated:YES];
    
    //    newRegion.center.latitude = centre.latitude;
    //    newRegion.center.longitude = centre.longitude;
    //
    //    newRegion.span.latitudeDelta = 1.0;
    //    newRegion.span.longitudeDelta = 1.0;
    //    [self.mapView setRegion:newRegion animated:YES];
    
}

-(IBAction)btn_show_all_image:(id)sender{
    
    self.view_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    [UIView setAnimationDelegate:self];
    
    self.view_image.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    [UIView commitAnimations];
    [self.view_image setHidden:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [_view_zoom_controller release];
    [_img_bg_collection release];
    [_view_zoom release];
    [_img_zoom release];
    [_img_photo release];
    [_btn_info release];
    [_view_show_image release];
    [_image_detail_viewObj release];
    [collectionViewObj release];
    [view_image release];
    [array_data release];
    [super dealloc];
}
@end
