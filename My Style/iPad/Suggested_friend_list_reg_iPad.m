//
//  Suggested_friend_list_reg_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Suggested_friend_list_reg_iPad.h"

@interface Suggested_friend_list_reg_iPad ()

@end

@implementation Suggested_friend_list_reg_iPad

@synthesize array_friend,tbl_friend;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btn_done_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.array_friend =[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self get_suggested];
}
-(void)get_suggested{
    
    //http://www.techintegrity.in/mystyle/get_suggest_user_follow.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=3
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14240" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSuggestUserFollowResponce:) name:@"14240" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14240" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetSuggestUserFollowResponce:) name:@"-14240" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@get_suggest_user_follow.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14240" :params];
}

-(void)getSuggestUserFollowResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14240" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14240" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_friend removeAllObjects];
        
        
        for (NSDictionary *dict in [result valueForKey:@"data"]) {
            
            Suggested_friend_list_share *shareobj =[[Suggested_friend_list_share alloc]init];
            shareobj.bio =[StaticClass urlDecode:[dict valueForKey:@"bio"]];
            
            NSLog(@"%@",NSStringFromClass([[dict valueForKey:@"images"] class]));
            if ([[dict valueForKey:@"images"] isKindOfClass:[NSArray class]] ) {
                shareobj.image_data =[dict valueForKey:@"images"];
            }else{
                
                NSLog(@"Stri");
            }
            shareobj.total_img=[dict valueForKey:@"total_images"];
            shareobj.user_img =[StaticClass urlDecode:[dict valueForKey:@"user_image"]];
            shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
            shareobj.is_follow=@"no";
            shareobj.user_id=[dict valueForKey:@"uid"];
            shareobj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            [self.array_friend addObject:shareobj];
            [shareobj release];
            
        }
        [self.tbl_friend reloadData];
    }
}

-(void)FailgetSuggestUserFollowResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14240" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14240" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_friend.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float height = 110.0f;
    Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:indexPath.row];
    
    if (![obj.total_img intValue] ==0) {
        height +=210.0f;
    }
    
    
    CGSize size = [obj.bio  sizeWithFont:[UIFont systemFontOfSize:30.0f]
                       constrainedToSize:CGSizeMake(550, HUGE_VALL)
                           lineBreakMode:NSLineBreakByWordWrapping];
    
    height +=size.height;
    return height +10.0f;
    
    //return 170;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier =@"Suggested_friend_list_cell_iPad";
    Suggested_friend_list_cell_iPad *cell=(Suggested_friend_list_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Suggested_friend_list_cell_iPad" owner:self options:nil];
        cell =[nib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.showsReorderControl = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell draw_collectionview_in_cell];
        [cell.btn_follow addTarget:self action:@selector(btn_follow_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_user_img addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.btn_follow.tag = indexPath.row;
    cell.btn_user_img.tag = indexPath.row;
    
    Suggested_friend_list_share *shareobj =[self.array_friend objectAtIndex:indexPath.row];
    cell.lbl_username.text = shareobj.username;
    cell.lbl_name.text = shareobj.name;
    cell.btn_user_img.imageURL =[NSURL URLWithString:shareobj.user_img];
    if ([shareobj.is_follow isEqualToString:@"no"]) {
        [cell.btn_follow setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
    }else{
        [cell.btn_follow setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
    }
    
    cell.lbl_dec.text = shareobj.bio;
    
    CGSize size = [shareobj.bio  sizeWithFont:[UIFont systemFontOfSize:30.0f]
                            constrainedToSize:CGSizeMake(550, HUGE_VALL)
                                lineBreakMode:NSLineBreakByWordWrapping];
    
    if (![shareobj.total_img intValue] ==0) {
        cell.collectionView.frame = CGRectMake(0, 110, 768, 200);
        [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
        
        cell.lbl_dec.frame = CGRectMake(109,320,550,size.height);
        
    }else{
        cell.collectionView.frame = CGRectMake(0, 110, 320, 0);
        cell.lbl_dec.frame = CGRectMake(109,118,550,size.height);
    }
    
    return cell;
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(Collectionview_delegate *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(Collectionview_delegate *)collectionView numberOfItemsInSection:(NSInteger)section {
    Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:collectionView.index];
    return [obj.total_img integerValue];
}

-(CGSize)collectionView:(Collectionview_delegate *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(200, 200);
    
}

-(UICollectionViewCell *)collectionView:(Collectionview_delegate *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    
    Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.image_data objectAtIndex:indexPath.row];
    //cell.img_photo.imageURL =[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image_path"]]];
            [cell.img_photo setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image_path"]]] placeholderImage:nil];

    
    // cell.img_photo.image =[UIImage imageNamed:@"test1.png"];
    cell.tag = indexPath.row;
    
    return cell;
}


#pragma mark Follow - unfollow
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    int tag =btn.tag;
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    Suggested_friend_list_share *shareobj =[self.array_friend objectAtIndex:tag];
    if ([shareobj.is_follow isEqualToString:@"no"]) {
        shareobj.is_follow =@"yes";
        url=@"post_user_following.php";
    }
    else {
        shareobj.is_follow =@"no";
        url=@"post_user_unfollow.php";
    }
    
    [self.array_friend replaceObjectAtIndex:tag withObject:shareobj];
    [self.tbl_friend reloadData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14241" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingUnfollowAPIResponce:) name:@"14241" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14241" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingUnfollowAPIResponce:) name:@"-14241" object:nil];
    
    
     NSString *strParameter=[NSString  stringWithFormat:@"sign=%@&salt=%@&uid=%@&following_id=%@&",sig,salt,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],shareobj.user_id];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@?%@",[[Singleton sharedSingleton] getBaseURL],url,strParameter];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14241" :nil];
}

-(void)postUserFollowingUnfollowAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14241" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14241" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    //akshay
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }
}

-(void)FailpostUserFollowingUnfollowAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14241" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14241" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}


-(IBAction)btn_user_info_click:(id)sender{
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    
    [array_friend release];
    [tbl_friend release];
    
    [super dealloc];
}

@end
