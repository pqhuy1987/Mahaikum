//
//  Favourite_feed_following_collection_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collectionview_delegate.h"
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
@class NPRImageView;

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";

@interface Favourite_feed_following_collection_cell_iPad :  UITableViewCell{
    
    
    NPRImageView *btn_user_img;
    UIButton *btn_user_img1;
    UILabel *lbl_time;
    LORichTextLabel *lbl_dec;
    
    
}
@property(nonatomic,retain) LORichTextLabel *lbl_dec;
@property(nonatomic,retain)IBOutlet NPRImageView *btn_user_img;
@property(nonatomic,retain)IBOutlet UILabel *lbl_time;
@property(nonatomic,retain)IBOutlet UIButton *btn_user_img1;
-(void)draw_collectionview_in_cell;
@property(nonatomic,retain)IBOutlet Collectionview_delegate *collectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

@end
