//
//  Suggested_friend_list_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/6/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collectionview_delegate.h"
#import "EGOImageButton.h"

static NSString *CollectionViewCellIdentifier2 = @"CollectionViewCellIdentifier";

@interface Suggested_friend_list_cell_iPad : UITableViewCell{
    EGOImageButton *btn_user_img;
    UILabel *lbl_name;
    UILabel *lbl_username;
    UILabel *lbl_dec;
    
    UIButton *btn_follow;
}
@property(nonatomic,retain)IBOutlet UIButton *btn_follow;
@property(nonatomic,retain)IBOutlet UILabel *lbl_dec;
@property(nonatomic,retain)IBOutlet EGOImageButton *btn_user_img;
@property(nonatomic,retain)IBOutlet UILabel *lbl_name;
@property(nonatomic,retain)IBOutlet UILabel *lbl_username;

-(void)draw_collectionview_in_cell;
@property(nonatomic,retain)IBOutlet Collectionview_delegate *collectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;@end
