//
//  ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "ViewController_iPad.h"

@interface ViewController_iPad ()

@end

@implementation ViewController_iPad

@synthesize img_bg,img_logo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    sign_in_ViewObj=[[Sign_ViewController_iPad alloc]initWithNibName:@"Sign_ViewController_iPad" bundle:nil];
    self.img_logo.frame=CGRectMake(0,568, img_logo.frame.size.width, img_logo.frame.size.height);
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [UIView animateWithDuration:1.5 animations:^{
        if (is_iPhone_5) {
            self.img_logo.frame=CGRectMake(0,0, img_logo.frame.size.width, img_logo.frame.size.height);
        }else{
            self.img_logo.frame=CGRectMake(0,0, img_logo.frame.size.width, img_logo.frame.size.height);
        }
        
    } completion:^ (BOOL finished)
     {
         [self.navigationController pushViewController:sign_in_ViewObj animated:YES];
         
     }];
    //    [self.navigationController pushViewController:sign_in_ViewObj animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [img_bg release];
    [img_logo release];
    [super dealloc];
}

@end
