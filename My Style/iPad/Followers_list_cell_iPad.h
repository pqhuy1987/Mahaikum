//
//  Followers_list_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
@class NPRImageView;

@interface Followers_list_cell_iPad : UITableViewCell{
    
    NPRImageView *img_user;
    UILabel *lbl_username;
    UILabel *lbl_name;
    UIButton *btn_follow;
}
@property(nonatomic,retain)IBOutlet NPRImageView *img_user;
@property(nonatomic,retain)IBOutlet UILabel *lbl_username;
@property(nonatomic,retain)IBOutlet UILabel *lbl_name;
@property(nonatomic,retain)IBOutlet UIButton *btn_follow;


@end
