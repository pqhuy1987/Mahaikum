//
//  DemoImageEditor_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "DemoImageEditor_iPad.h"
#import "HFImageEditorViewController+SubclassingHooks.h"


@interface DemoImageEditor_iPad ()

@end

@implementation DemoImageEditor_iPad

@synthesize  saveButton = _saveButton;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        self.cropSize = CGSizeMake(768,768);
        self.minimumScale = 0.2;
        self.maximumScale = 10;
    }
    return self;
}

- (void)dealloc
{
    [_saveButton release];
    [super dealloc];
}
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
   // [[UIApplication sharedApplication]setStatusBarHidden:YES];
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:37.0f/255.0f green:90.0f/255.0f blue:130.0f/255.0f alpha:1];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    self.saveButton = nil;
}

-(IBAction)btn_cancle_click:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)setSquareAction:(id)sender
{
    self.cropSize = CGSizeMake(768, 768);
    [self reset:YES];
}

- (IBAction)setLandscapeAction:(id)sender
{
    self.cropSize = CGSizeMake(768, 768);
    [self reset:YES];
}


- (IBAction)setLPortraitAction:(id)sender
{
    self.cropSize = CGSizeMake(768, 768);
    [self reset:YES];
}

#pragma mark Hooks
- (void)startTransformHook
{
    self.saveButton.tintColor = [UIColor colorWithRed:0 green:49/255.0f blue:98/255.0f alpha:1];
}

- (void)endTransformHook
{
    self.saveButton.tintColor = [UIColor colorWithRed:0 green:128/255.0f blue:1 alpha:1];
}


@end
