//
//  Share_photo_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Show_location_list_iPad.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "DCRoundSwitch.h"

@interface Share_photo_ViewController_iPad : UIViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,AmazonServiceRequestDelegate>{
    
    UIImage *img_photo;
    UIScrollView *scrollview;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_secondcell;
    
    UIImageView *img_show_photo;
    UIView *view_bg_show_photo;
    UITextView *txt_desc;
    
    UIButton *btn_location;
    UILabel *lbl_optional;
    UIView *view_second_cell;
    
    UIView *view_third_cell;
    UIImageView *img_bg_third_cell;
    
    Show_location_list_iPad *show_location_listViewObj;
}

@property (nonatomic, retain) IBOutlet DCRoundSwitch *switch_map;

@property(nonatomic,retain)IBOutlet UITextView *txt_desc;
@property(nonatomic,retain)IBOutlet UIImageView *img_show_photo;
@property(nonatomic,retain)IBOutlet UIView *view_bg_show_photo;

@property(nonatomic,retain)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_secondcell;

@property(nonatomic,retain)UIImage *img_photo;
@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;

@property(nonatomic,retain)IBOutlet UIButton *btn_location;
@property(nonatomic,retain)IBOutlet UILabel *lbl_optional;
@property(nonatomic,retain)IBOutlet UIView *view_second_cell;

@property(nonatomic,retain)IBOutlet UIView *view_third_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_third_cell;

@property(nonatomic,retain)Show_location_list_iPad *show_location_listViewObj;
@property (nonatomic, retain) AmazonS3Client *s3;

@end
