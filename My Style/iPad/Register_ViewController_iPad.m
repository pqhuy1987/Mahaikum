//
//  Register_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Register_ViewController_iPad.h"
#import "AppDelegate.h"
@interface Register_ViewController_iPad ()

@end

@implementation Register_ViewController_iPad
@synthesize btn_privacy_policy,btn_terms_of_service,btn_add_photo;
@synthesize photoPickerController;
@synthesize image_url;
@synthesize txt_username,txt_password,txt_email,txt_name,txt_phone_no;
@synthesize view_error;
@synthesize btn_facebook_info,scrollview;
@synthesize img_first_cell_bg,img_second_cell_bg,webviewObj,popover;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.s3 = [[[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] autorelease];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    self.popover = [UIPopoverController alloc];
    self.popover.delegate = self;
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    
    self.img_first_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_first_cell_bg.layer.cornerRadius =8.0f;
    
    self.img_second_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_second_cell_bg.layer.cornerRadius =8.0f;
    
    self.webviewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    twitter_login=[[Twitter_login_ViewController alloc]initWithNibName:@"Twitter_login_ViewController" bundle:nil];
    

    // self.btn_add_photo.imageURL =[NSURL URLWithString:@"http://profile.ak.fbcdn.net/hprofile-ak-snc6/275141_100004515153095_171887159_s.jpg"];
    
    //Notification for get Image from Facebook
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"register_add_photo" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_photo_facebok:) name:@"register_add_photo" object:nil];
    
    
    //Notification for get info from Facebook
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"register_use_fb_info" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_facebok:) name:@"register_use_fb_info" object:nil];
    
    
    
    self.btn_privacy_policy.underlinePosition=2.0;
    self.btn_terms_of_service.underlinePosition=2.0;
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.btn_add_photo.imageView.contentMode =UIViewContentModeScaleAspectFit;
    
    /*
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelKeyboard)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_click:)],
                           nil];
    [numberToolbar sizeToFit];
    self.txt_username.inputAccessoryView = numberToolbar;
    self.txt_password.inputAccessoryView = numberToolbar;
    self.txt_email.inputAccessoryView = numberToolbar;
    self.txt_name.inputAccessoryView = numberToolbar;
    self.txt_phone_no.inputAccessoryView = numberToolbar;
    */
    
    //    self.txt_username.text = @"Piyush";
    //    self.txt_password.text = @"piyush";
    //    self.txt_email.text = @"piyush@gmail.com";
    //    self.txt_name.text = @"Piyush Patel";
    //    self.txt_phone_no.text = @"9998823969";
    
}
-(IBAction)btn_back_click:(id)sender{
    
    [self.view endEditing:YES];
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    self.txt_username.text = @"";
    self.txt_password.text = @"";
    self.txt_email.text = @"";
    self.txt_name.text = @"";
    self.txt_phone_no.text = @"";
    [self.btn_facebook_info setImage:[UIImage imageNamed:@"reg_fb_btn.png"] forState:UIControlStateNormal];
    [self.btn_facebook_info setFrame:CGRectMake(51,376,384,60)];
    
    [self.btn_add_photo setImage:[UIImage imageNamed:@"btn_add_photo.png"] forState:UIControlStateNormal];
    [StaticClass saveToUserDefaults:@"" :FACEBOOK_ID];
    
    AppDelegate *appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate.session closeAndClearTokenInformation];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    //  [self.navigationController popViewControllerAnimated:YES];
}

#pragma viewWillAppear
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark Add Photo Click
-(IBAction)btn_add_photo_click:(id)sender{
    UIActionSheet *sheet =[[UIActionSheet alloc]init];
    [sheet setTitle:@"Change Profile Picture"];
    [sheet addButtonWithTitle:@"Import from Facebook"];
    [sheet addButtonWithTitle:@"Import from Twitter"];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        [sheet addButtonWithTitle:@"Take Photo"];
    }
    [sheet addButtonWithTitle:@"Choose from Library"];
    sheet.cancelButtonIndex = [sheet addButtonWithTitle: @"Cancel"];
    sheet.delegate=self;
    //   UIActionSheet *sheet =[[UIActionSheet alloc]initWithTitle:@"Change Profile Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Import from Facebook",@"Import from Twitter",@"Take Photo",@"Choose from Library", nil];
    [sheet showInView:self.view];
}

#pragma mark ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
        {
            NSLog(@"Import from Facebook");
            [self get_user_photo_facebok];
        }
        case 1:
        {
            NSLog(@"Import from Twitter");
            [self getImage_from_twitter];
            //http://mobile.tutsplus.com/tutorials/iphone/ios-6-and-the-social-framework-twitter-requests/
            break;
        }
        case 2:
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                NSLog(@"Take Photo");
                photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:photoPickerController animated:YES completion:nil];
                
            }else{
                if ([self.popover isPopoverVisible]) {
                    [self.popover dismissPopoverAnimated:YES];
                    [popover release];
                } else {
                photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                UIPopoverController *popOver = [[UIPopoverController alloc] initWithContentViewController:photoPickerController];
                
                    [popOver presentPopoverFromRect:self.btn_add_photo.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                    self.popover = popOver;
                }
            }
            break;
        }
        case 3:
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                if ([self.popover isPopoverVisible]) {
                    [self.popover dismissPopoverAnimated:YES];
                    [popover release];
                } else {
                    photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    UIPopoverController *popOver = [[UIPopoverController alloc] initWithContentViewController:photoPickerController];
                    
                    [popOver presentPopoverFromRect:self.btn_add_photo.frame inView:self.scrollview permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                    self.popover = popOver;
                }
            }
            else{
                NSLog(@"Cancel");
            }
            
            break;
        }
        case 4:
        {
            NSLog(@"Cancel");
            
            break;
        }
        default:
            break;
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//	[self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.popover dismissPopoverAnimated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
   // [self dismissViewControllerAnimated:YES completion:nil];

    [photoPickerController dismissViewControllerAnimated:YES completion:nil];
    [self.popover dismissPopoverAnimated:YES];
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
	if (baseImage == nil) return;
    self.btn_add_photo.imageView.image=baseImage;
    self.btn_add_photo.imageView.contentMode = UIViewContentModeScaleToFill;
    [self.btn_add_photo setContentMode:UIViewContentModeScaleToFill];
    NSLog(@"%@",NSStringFromCGRect(self.btn_add_photo.frame));
}

#pragma mark Use Your Facebook Info
-(IBAction)btn_use_fb_info_click:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_info_Me];
    }];
}

#pragma Mark Facebook Add Photo
-(void)get_user_info_facebok:(NSNotification *)notification {
    [self apiFQLI_info_Me];
}

-(void)apiFQLI_info_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    // NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        [self info_requestFacebookUserInfoCompleted:connection result:result error:error];
        // [self performSelector:@selector(set_btn_add_photo_image) withObject:self afterDelay:0.1];
        
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
    
}

- (void)info_requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                       result:(id)result
                                        error:(NSError *)error {
    
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    else {
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSLog(@"%@",dictionary);
        NSLog(@"%@",NSStringFromClass([[dictionary objectForKey:@"data"] class]));
        
        for (NSString *str in [tempResult valueForKey:@"name"]) {
            self.txt_name.text =str;
        }
        for (NSString *str in [tempResult valueForKey:@"email"]) {
            self.txt_email.text =str;
        }
//        for (NSString *str in [tempResult valueForKey:@"pic"]) {
//            self.btn_add_photo.imageURL =[NSURL URLWithString:str];
//        }
//        for (NSString *str in [tempResult valueForKey:@"uid"]) {
//            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
//        }
        
        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        for (NSString *str in [tempResult valueForKey:@"pic"]) {
            NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
            
            //self.btn_add_photo.imageURL =[NSURL URLWithString:str];
            self.btn_add_photo.imageURL =[NSURL URLWithString:imgU];
            
        }

        [self.btn_facebook_info setImage:[UIImage imageNamed:@"facebookbtn_selected@2x.png"] forState:UIControlStateNormal];
//        [self.btn_facebook_info setFrame:CGRectMake(51,376,574,55)];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //   self.txt_email.text=[tempResult :@"email"];
            // self.txt_name.text =[tempResult valueForKey:@"name"];
            
        });
    }
}

#pragma mark Done Button Click

-(IBAction)btn_done_click:(id)sender{
    if (self.txt_username.text.length==0) {
        
        //        [AJNotificationView showNoticeInView:self.view
        //                                        type:AJNotificationTypeRed
        //                                       title:@"Please choose a username."
        //                             linedBackground:AJLinedBackgroundTypeAnimated
        //                                   hideAfter:2.5f
        //                                      offset:50.0f
        //                                       delay:0.01f
        //                                    response:^{
        //                                        NSLog(@"Response block");
        //                                    }
        //         ];
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please choose a username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];

        return;
    }
    if (self.txt_password.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please create a password." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if (self.txt_password.text.length<6) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Password must be at least 6 characters." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    if (![StaticClass validateEmail:self.txt_email.text]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Is this correct?" message:[NSString stringWithFormat:@"You entered your email as :\n%@",self.txt_email.text] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag=1;
    [alert show];
    [alert release];
}

-(void)register_call{
    
    NSData *img_data  = UIImageJPEGRepresentation(self.btn_add_photo.imageView.image,1);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSDate *dt=[NSDate date];
        //double timePassed_ms = [dt timeIntervalSinceNow];
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *dateString1 = [DateFormatter stringFromDate:dt];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString* identifierForVendor = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
        __block NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",identifierForVendor,dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]] autorelease];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else
            {
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = @"coco";
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRegisterAPIResponce:) name:@"14223" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetRegisterAPIResponce:) name:@"-14223" object:nil];
                
                NSString *requestStr = [NSString stringWithFormat:@"%@get_register.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        self.txt_username.text,@"username",
                                        self.txt_password.text,@"password",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ID],@"facebook_id",
                                        self.txt_name.text,@"name",
                                        self.txt_email.text,@"email",
                                        self.txt_phone_no.text,@"phone",
                                        imageKeys,@"image",
                                        [StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"],@"device_token",
                                        nil];
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14223" :params];
            }
        });
    });
}

-(void)getRegisterAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-4"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"A user with that email already exists." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"-3"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"A user with that username already exists." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"%@",[result valueForKey:@"data"]);
        NSDictionary *dict =[result valueForKey:@"data"];
        
        [StaticClass saveToUserDefaults:[dict valueForKey:@"uid"] :USER_ID];
        [StaticClass saveToUserDefaults:[dict valueForKey:@"username"] :USERNAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"name"]] :USER_NAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"image"]] :USER_IMAGE];
        Facebook_Find_Friends_ViewController *tempObj =[[Facebook_Find_Friends_ViewController alloc]initWithNibName:@"Facebook_Find_Friends_ViewController" bundle:nil];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"REVMOBCHARTBOOSTADDNOTIFICATION" object:self];
        [self.navigationController pushViewController:tempObj animated:YES];
    }
}

-(void)FailgetRegisterAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        //  appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}

-(void)apiFQLI_pic_Me {
    //    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(set_btn_add_photo_image) withObject:self afterDelay:0.1];
        
    };
    [requester addRequest:request completionHandler:handler];
    [requester start];
}

- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    else {
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSLog(@"%@",dictionary);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            for (NSString *str in [tempResult valueForKey:@"pic"]) {
                self.image_url=str;
            }
        });
    }
}

-(void)set_btn_add_photo_image{
    self.btn_add_photo.imageURL =[NSURL URLWithString:self.image_url];
}

#pragma mark Photo From Twitter

-(void)getImage_from_twitter{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
        if (granted) {
        }
        else{
            // [self.navigationController pushViewController:twitter_login animated:YES];
        }
        if (error) {
            NSLog(@"%@",[error debugDescription]);
            //[self.navigationController pushViewController:twitter_login animated:YES];
            // [self go_to_login_twitter];
            [self performSelector:@selector(go_to_login_twitter) withObject:self afterDelay:0.1];
        }
    }];
}

-(void)go_to_login_twitter{
    [self.navigationController pushViewController:twitter_login animated:YES];
}

#pragma mark KeyBoard Done & Calcel Method

-(void)doneWithKeyboard {
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    [self.view endEditing:YES];
}

-(void)cancelKeyboard {
    
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    NSLog(@"%@",NSStringFromCGRect(self.scrollview.frame));
    [self.view endEditing:YES];
}

#pragma mark UITextfield Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
        return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    //  self.scrollview.contentOffset=CGPointMake(0,0);
    
    if (textField==txt_username) {
        
        [txt_password becomeFirstResponder];
        
    }else if(textField==txt_password){
        
        [txt_email becomeFirstResponder];
        
    }else if(textField==txt_email){
        
        [txt_name becomeFirstResponder];
        
    }else if(textField==txt_name){
        
        [txt_phone_no becomeFirstResponder];
        
    }else if(textField==txt_phone_no){
        
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField==self.txt_username || textField==self.txt_email) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark UIAlert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        if (buttonIndex==1) {
            [self register_call];
        }
    }
}

-(IBAction)btn_privacy_policy_click:(id)sender{
    //  self.webviewObj.web_url =@"http://instagram.com/legal/privacy/";
    self.webviewObj.web_url =[[Singleton sharedSingleton]get_privacy_police_url];
    [self.navigationController pushViewController:self.webviewObj animated:YES];
}

-(IBAction)btn_terms_of_service_click:(id)sender{
    
    // self.webviewObj.web_url =@"http://instagram.com/legal/privacy/";
    self.webviewObj.web_url =[[Singleton sharedSingleton]get_terms_of_use_url];
    [self.navigationController pushViewController:self.webviewObj animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [popover release];
    [webviewObj release];
    [img_first_cell_bg release];
    [img_second_cell_bg release];
    [scrollview release];
    [btn_facebook_info release];
    [view_error release];
    [txt_username release];
    [txt_password release];
    [txt_email release];
    [txt_name release];
    [txt_phone_no release];
    [image_url release];
    [photoPickerController release];
    [btn_add_photo release];
    [btn_terms_of_service release];
    [btn_privacy_policy release];
    [super dealloc];
}

@end
