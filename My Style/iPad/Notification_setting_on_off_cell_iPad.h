//
//  Notification_setting_on_off_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCRoundSwitch.h"

@interface Notification_setting_on_off_cell_iPad : UITableViewCell{
    DCRoundSwitch *switch_on_off;
}
@property(nonatomic,retain)IBOutlet  DCRoundSwitch *switch_on_off;@end
