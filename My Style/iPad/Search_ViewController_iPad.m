//
//  Search_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Search_ViewController_iPad.h"

@interface Search_ViewController_iPad ()

@end

@implementation Search_ViewController_iPad

@synthesize collectionViewObj,view_header,view_footer,img_spinner,image_detail_viewObj,array_search;
@synthesize  txt_search,view_search,segment,tbl_users,array_search_result;
@synthesize user_info_view,array_hashtag_result,tbl_hashtag;
@synthesize hash_tag_viewObj,view_fillter,lbl_header;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.view_fillter.hidden = YES;
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.view_search.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.image_detail_viewObj =[[Image_detail_iPad alloc]initWithNibName:@"Image_detail_iPad" bundle:nil];
    self.user_info_view =[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    
    self.hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    
    self.array_search =[[NSMutableArray alloc]init];
    self.array_search_result =[[NSMutableArray alloc]init];
    self.array_hashtag_result=[[NSMutableArray alloc]init];
    
    //   self.tbl_news.hidden =YES;
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    
    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
    NSArray *array_spinner =@[[UIImage imageNamed:@"feedstate-spinner-frame01.png"],[UIImage imageNamed:@"feedstate-spinner-frame02.png"],[UIImage imageNamed:@"feedstate-spinner-frame03.png"],[UIImage imageNamed:@"feedstate-spinner-frame04.png"],[UIImage imageNamed:@"feedstate-spinner-frame05.png"],[UIImage imageNamed:@"feedstate-spinner-frame06.png"],[UIImage imageNamed:@"feedstate-spinner-frame07.png"],[UIImage imageNamed:@"feedstate-spinner-frame08.png"]];
    
    
    self.img_spinner.animationImages =array_spinner;
    self.img_spinner.animationRepeatCount = HUGE_VAL;
    self.img_spinner.animationDuration=1.0f;
    //    [self get_search_feed];
    
//    self.segment.frame = CGRectMake(134.0f, 98.0f,500.0f, 60.0f);
//    self.segment.font = [UIFont boldSystemFontOfSize:26.0f];
//    self.segment.selectedItemShadowColor =[UIColor clearColor];
//    self.segment.unselectedItemShadowColor =[UIColor clearColor];
//    self.segment.selectedItemColor = [UIColor whiteColor];
//    self.segment.unselectedItemColor=[UIColor whiteColor];
//    
//    self.segment.unSelectedItemBackgroundGradientColors =[NSArray arrayWithObjects:[UIColor colorWithRed:71.0f/255.0f green:148.0f/255.0f blue:232.0f/255.0f alpha:1],[UIColor colorWithRed:71.0f/255.0f green:148.0f/255.0f blue:232.0f/255.0f alpha:1], nil];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;

    [self get_search_feed];
}

-(IBAction)btn_refresh_click:(id)sender{
    
//    for (Home_tableview_data_share *obj in self.array_search) {
//        [obj release];
//        obj=nil;
//    }
    [self.array_search removeAllObjects];
    [self.collectionViewObj reloadData];
    [self get_search_feed];
}

-(IBAction)btn_fillter_clikc:(id)sender{
    
    if (self.view_fillter.hidden) {
        self.view_fillter.hidden =NO;
    }else{
        self.view_fillter.hidden =YES;
    }
    [self.view bringSubviewToFront:self.view_fillter];
}

-(IBAction)btn_out_side_click:(id)sender{
    [self.view_fillter setHidden:YES];
}

-(IBAction)btn_most_recent_click:(id)sender{
    self.lbl_header.text =@"MOST RECENT";
    self.view_fillter.hidden = YES;
//    for (Home_tableview_data_share *obj in self.array_search) {
//        [obj release];
//        obj=nil;
//    }
    [self.array_search removeAllObjects];
    [self.collectionViewObj reloadData];
    [self get_search_feed];
}

-(IBAction)btn_top_rated_click:(id)sender{
    self.lbl_header.text =@"TOP RATED";
    self.view_fillter.hidden = YES;
    
//    for (Home_tableview_data_share *obj in self.array_search) {
//        [obj release];
//        obj=nil;
//    }
    [self.array_search removeAllObjects];
    [self.collectionViewObj reloadData];
    [self get_search_feed];
}

#pragma mark - Get News Feed
-(void)get_search_feed{
    [self start_activity];
    _btn_reload.enabled = NO;

    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14230" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSearchFeedAPIResponce:) name:@"14230" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14230" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetSearchFeedAPIResponce:) name:@"-14230" object:nil];
    
    NSString *requestStr = @"";
    if ([self.lbl_header.text isEqualToString:@"MOST RECENT"]) {
        requestStr =[NSString stringWithFormat:@"%@get_random_images.php",[[Singleton sharedSingleton] getBaseURL]];
    }else{
        requestStr =[NSString stringWithFormat:@"%@get_top_images.php",[[Singleton sharedSingleton] getBaseURL]];
    }
    
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14230" :params];
}

-(void)getSearchFeedAPIResponce:(NSNotification *)notification {
   // [self stopSpinner];
    _btn_reload.enabled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14230" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14230" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        
        [self.array_search removeAllObjects];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict in array) {
            
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[dict valueForKey:@"username"];
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[tempdict valueForKey:@"username"]];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[tempdict valueForKey:@"username"];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    
                    [shareObj.array_comments addObject:obj];
                    [obj release];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            
            [self.array_search addObject:shareObj];
            [shareObj release];
        }
    }
    
    [self.collectionViewObj reloadData];
    [self stop_activity];
}

-(void)FailgetSearchFeedAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14230" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14230" object:nil];
    //[self stopSpinner];
    _btn_reload.enabled = YES;
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UICollectionView
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.view_header.frame;
        [headerView addSubview:self.view_header];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(0, self.view_header.frame.size.height);
        
        reusableview = headerView;
        NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
        return reusableview;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        
        image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
        
        
        footerview.frame =self.view_footer.frame;
        [footerview addSubview:self.view_footer];
        reusableview = footerview;
        
        return reusableview;
    }
    
    return reusableview;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section; {
    return self.array_search.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {            return CGSizeMake(180,180);
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath; {
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    Home_tableview_data_share *shareObj =[self.array_search objectAtIndex:indexPath.row];
    [cell.img_photo setImageWithContentsOfURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    cell.tag = indexPath.row;
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.image_detail_viewObj.shareObj =[self.array_search objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_search;
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
    
}

#pragma mark - Start & Stop Spiner
-(void)start_activity{
    self.img_spinner.hidden=NO;
    [self.img_spinner startAnimating];
}
-(void)stop_activity{
    self.img_spinner.hidden=YES;
    [self.img_spinner stopAnimating];
}

#pragma mark - Search view hide show
-(IBAction)btn_search_show_click:(id)sender{
    self.view_search.hidden=NO;
    [self.txt_search becomeFirstResponder];
}

-(IBAction)btn_search_cancel_click:(id)sender{
    self.view_search.hidden=YES;
    self.txt_search.text=@"";
    [self.view_search endEditing:YES];
}

-(IBAction)segment_value_change:(id)sender {
    if (self.segment.selectedSegmentIndex==0) {
        self.txt_search.placeholder=@"Search for a user";
        self.tbl_users.hidden =NO;
        self.tbl_hashtag.hidden = YES;
        
    }
    else{
        self.txt_search.placeholder=@"Search for Hashtags";
        self.txt_search.text =[self.txt_search.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.tbl_users.hidden =YES;
        self.tbl_hashtag.hidden = NO;
    }
}

#pragma mark - UITextField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.segment.selectedSegmentIndex==1) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self get_search_user];
    return YES;
}

#pragma mark - Get History Data from Database
-(void)get_history_data_from_db {
    
}

#pragma mark - Get Search user call
-(void)get_search_user {
    //[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([self.txt_search.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length >0) {
        
        NSString *requestStr=@"";
        NSString *notificationVal=@"";
        
        if (self.segment.selectedSegmentIndex==1){
            NSLog(@"search for Hash Tag");
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHashUsernameAPIResponce:) name:@"14231" object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetHashUsernameAPIResponce:) name:@"-14231" object:nil];
            
            requestStr = [NSString stringWithFormat:@"%@get_hash_username.php",[[Singleton sharedSingleton] getBaseURL]];
            notificationVal=@"14231";
        }else{
            NSLog(@" search for username");
            requestStr = [NSString stringWithFormat:@"%@search_users.php",[[Singleton sharedSingleton] getBaseURL]];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchUsersResponce:) name:@"14232" object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsearchUsersResponce:) name:@"-14232" object:nil];
            
            notificationVal=@"14232";
        }
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = @"coco";
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                self.txt_search.text,@"key",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :notificationVal :params];
    }
}

-(void)getHashUsernameAPIResponce :(NSNotification *)notification {
    //[self stopSpinner];
    _btn_reload.enabled = YES;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_hashtag_result removeAllObjects];
        
        NSArray *temarray =[result valueForKey:@"data"];
        for (NSString *dict in temarray) {
            NSLog(@"DICT:%@",dict);
            [self.array_hashtag_result addObject:[StaticClass urlDecode:dict]];
        }
        NSLog(@"COUNT:%d",[self.array_hashtag_result count]);
        [self.tbl_hashtag reloadData];
    }
}

-(void)FailgetHashUsernameAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
    
   // [self stopSpinner];
    _btn_reload.enabled = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)searchUsersResponce :(NSNotification *)notification {
 //   [self stopSpinner];
    _btn_reload.enabled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_search_result removeAllObjects];
        NSArray *temarray =[result valueForKey:@"data"];
        for (NSDictionary *dict in temarray) {
            Comment_share *obj =[[Comment_share alloc]init];
            obj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            obj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            obj.image_url=[StaticClass urlDecode:[dict valueForKey:@"image"]];
            obj.uid =[dict valueForKey:@"user_id"];
            [self.array_search_result addObject:obj];
            [obj release];
        }
        [self.tbl_users reloadData];
    }
}

-(void)FailsearchUsersResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    
  //  [self stopSpinner];
    _btn_reload.enabled = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView ==self.tbl_users) {
        return self.array_search_result.count;
    }else{
        
        return self.array_hashtag_result.count;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tbl_users) {
        return 110.0f;
    }else{
        return 88.0f;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==self.tbl_users) {
        
        
        static NSString *identifier =@"search_view_cell_iPad";
        search_view_cell_iPad *cell=(search_view_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"search_view_cell_iPad" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        Comment_share *shareObj =[self.array_search_result objectAtIndex:indexPath.row];
        cell.lbl_username.text =shareObj.username;
        cell.lbl_name.text = shareObj.name;
        cell.img_user.imageURL =[NSURL URLWithString:shareObj.image_url];
        return cell;
    }else{
        
        static NSString *identifier =@"search_view_hashtag_cell_iPad";
        search_view_hashtag_cell_iPad *cell=(search_view_hashtag_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"search_view_hashtag_cell_iPad" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.lbl_title.text =[self.array_hashtag_result objectAtIndex:indexPath.row];
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==self.tbl_users) {
        
        Comment_share *shareObj =[self.array_search_result objectAtIndex:indexPath.row];
        self.user_info_view.user_id=shareObj.uid;
        [self.navigationController pushViewController:self.user_info_view animated:YES];
        
    }else{
        //[[self tagFromSender:sender ] substringFromIndex:1];
        self.hash_tag_viewObj.str_title =[[self.array_hashtag_result objectAtIndex:indexPath.row] substringFromIndex:1];
        [self.navigationController pushViewController:self.hash_tag_viewObj animated:YES];
    }
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    [hash_tag_viewObj release];
    [view_fillter release];
    [tbl_hashtag release];
    [array_hashtag_result release];
    [user_info_view release];
    [array_search_result release];
    [tbl_users release];
    [segment release];
    [txt_search release];
    [view_search release];
    [image_detail_viewObj release];
    [img_spinner release];
    [view_header release];
    [view_footer release];
    [collectionViewObj release];
    [lbl_header release];
    [super dealloc];
}


@end
