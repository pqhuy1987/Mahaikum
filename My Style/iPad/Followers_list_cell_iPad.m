//
//  Followers_list_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Followers_list_cell_iPad.h"



@implementation Followers_list_cell_iPad

@synthesize img_user,lbl_username,lbl_name,btn_follow;

-(void)dealloc{
    
    [img_user release];
    [lbl_username release];
    [lbl_name release];
    [btn_follow release];
    
    [super dealloc];
}@end
