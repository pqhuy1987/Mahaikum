//
//  Home_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "Home_tableview_cell_iPad.h"
#import "BlockActionSheet.h"
#import "Likers_list_ViewController_iPad.h"
#import "IFTweetLabel.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "Comment_share.h"
#import "Comments_list_ViewController_iPad.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Share_photo_iPad.h"

@interface Home_ViewController_iPad : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate,UITabBarControllerDelegate>{
    
    UITableView *tbl_news;
    Likers_list_ViewController_iPad *liker_list_viewObj;
    
    NSMutableArray *array_feeds;
    
    int which_image_liked;
    int which_image_delete;
    
    UIImageView *img_like_heart;
    Comments_list_ViewController_iPad *comments_list_viewObj;
    Share_photo_iPad *share_photo_view;
}
@property(nonatomic,retain)Share_photo_iPad *share_photo_view;

@property(nonatomic,retain)Comments_list_ViewController_iPad *comments_list_viewObj;

@property(nonatomic,retain)IBOutlet  UIImageView *img_like_heart;

@property(nonatomic,retain)IBOutlet UITableView *tbl_news;
@property(nonatomic,retain)Likers_list_ViewController_iPad *liker_list_viewObj;
@property(nonatomic,retain)NSMutableArray *array_feeds;
@property(nonatomic,retain)UIButton *btn_reload;

@end
