//
//  Home_tableview_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "ViewController.h"
#import "DLStarRatingControl.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Home_tableview_data_share.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "Comment_share.h"

@class NPRImageView;
@interface Home_tableview_cell_iPad : UITableViewCell{
    
    IBOutlet DLStarRatingControl *dlstarObj;
    
    NPRImageView *img_big;
    UIButton *btn_likes_count;
    UIImageView *img_likes;
    UIButton *btn_like;
    UIButton *btn_comment;
    UIButton *btn_photo_option;
    
    LORichTextLabel *lbl_desc;
    UIImageView *img_desc;

    LORichTextLabel *view_like_btn;
    UIButton *btn_view_all_comments;
    
    LORichTextLabel *lbl_comments;
    
    
}
@property(nonatomic,retain)IBOutlet UIImageView *img_likes;
@property(nonatomic,retain)IBOutlet LORichTextLabel *lbl_desc;
@property(nonatomic,retain)IBOutlet NPRImageView *img_big;
@property(nonatomic,retain)IBOutlet UIButton *btn_likes_count;
@property(nonatomic,retain)IBOutlet UIButton *btn_like;
@property(nonatomic,retain)IBOutlet UIButton *btn_comment;
@property(nonatomic,retain)IBOutlet UIButton *btn_photo_option;
@property(nonatomic,retain)IBOutlet LORichTextLabel *view_like_btn;
@property(nonatomic,retain)IBOutlet UIImageView *img_desc;
@property(nonatomic,retain) IBOutlet DLStarRatingControl *dlstarObj;
@property(nonatomic,retain)IBOutlet UIButton *btn_view_all_comments;
@property(nonatomic,retain)LORichTextLabel *lbl_comments;
-(void)draw_desc_in_cell;
-(void)draw_like_button_in_cell;

- (void)setImageURL:(NSURL *)imageURL placeholderImage:(UIImage *)placeholderImage;

+(float)get_tableview_hight:(Home_tableview_data_share *)shareObj;
-(void)redraw_cell:(Home_tableview_data_share *)shareObj andUserStyle:(LORichTextLabelStyle *)userStyle AtIndexPath:(NSIndexPath *)indexPath;

@end
