//
//  Rest_password_throw_fb_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "AJNotificationView.h"
#import "Singleton.h"
#import "StaticClass.h"

@interface Rest_password_throw_fb_iPad : UIViewController{
    UITextField *txt_password;
    UITextField *txt_password_again;
    
    NSString *str_email;
    NSString *str_username;
    NSString *str_image_url;
    UIImageView *img_cell_bg;
    
    UILabel *lbl_username;
    UILabel *lbl_dec;
    EGOImageView *img_image;
}
@property(nonatomic,retain)IBOutlet EGOImageView *img_image;

@property(nonatomic,retain)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,retain)IBOutlet UITextField *txt_password;
@property(nonatomic,retain)IBOutlet UITextField *txt_password_again;
@property(nonatomic,retain) NSString *str_username;
@property(nonatomic,retain) NSString *str_email;
@property(nonatomic,retain) NSString *str_image_url;

@property(nonatomic,retain)IBOutlet UILabel *lbl_username;
@property(nonatomic,retain)IBOutlet UILabel *lbl_dec;
@end
