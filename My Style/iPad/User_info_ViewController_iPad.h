//
//  User_info_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "image_collection_cell.h"
#import "image_collection_header.h"
#import "image_collection_footer.h"

#import "Show_map_ViewController_iPad.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"

#import "IFTweetLabel.h"
#import "EGOImageButton.h"
#import "Home_tableview_cell_iPad.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "BlockActionSheet.h"
#import "Likers_list_ViewController_iPad.h"
#import "Comments_list_ViewController_iPad.h"
#import "Share_photo_iPad.h"
#import "Image_detail_iPad.h"

#import "Followers_list_iPad.h"
#import "Profile_share.h"
#import "EGOImageView.h"
#import "Edit_your_profile.h"
#import "Setting_profile.h"
#import "LORichTextLabel.h"

@interface User_info_ViewController_iPad : UIViewController<MFMailComposeViewControllerDelegate,DLStarRatingDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    UICollectionView *collectionViewObj;
    
    //Header view
    IBOutlet UIView *view_header;
    EGOImageView *img_photo;
    UIView *view_img_photo;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_scondcell;
    
    IBOutlet UIView *view_header_tbl;
    EGOImageView *img_photo_tbl;
    UIView *view_img_photo_tbl;
    UIImageView *img_bg_firstcell_tbl;
    UIImageView *img_bg_scondcell_tbl;
    
    //UITableview
    UITableView *tbl_news;
    
    //Map view
    Show_map_ViewController_iPad *mapviewObj;
    
    //Footer view
    
    UIView *view_footer;
    UIImageView *img_spinner;
    NSArray *array_spinner;
    UIView *view_footer_tbl;
    UIImageView *img_spinner_tbl;
    
    IBOutlet UILabel *lbl_no_photo;
    IBOutlet UILabel *lbl_no_photo_tbl;
    
    UILabel *lbl_total_followers;
    UILabel *lbl_total_followings;
    UILabel *lbl_total_photos;
    //  UILabel *lbl_name;
    LORichTextLabel *lbl_name;
    
    
    UILabel *lbl_total_followers_tbl;
    UILabel *lbl_total_followings_tbl;
    UILabel *lbl_total_photos_tbl;
    LORichTextLabel *lbl_name_tbl;
    
    UIView *view_secondcell;
    UIView *view_secondcell_tbl;
    
    NSMutableArray *array_feeds;
    
    //News feed
    int which_image_liked;
    int which_image_delete;
    UIImageView *img_like_heart;
    
    Likers_list_ViewController_iPad *liker_list_viewObj;
    Comments_list_ViewController_iPad *comments_list_viewObj;
    Share_photo_iPad *share_photo_view;
    Edit_your_profile *edit_your_profile_viewObj;
    
    
    Image_detail_iPad *image_detail_viewObj;
    Followers_list_iPad *followers_list_viewObj;
    
    Profile_share *profile_share_obj;
    
    UIImagePickerController *photoPickerController;
    Setting_profile *setting_profileViewObj;
    
    int image_upload_flag;
    
    
    UIImageView *img_down_arrow;
    UIImageView *img_down_arrow_tbl;
    
    NSString *user_id;
    
    UIButton *btn_follow;
    UIButton *btn_follow_tbl;
    
    UIButton *btn_edit;
    UIButton *btn_edit_tbl;
    
    LORichTextLabelStyle *username_style;
    
}
@property(nonatomic,retain)IBOutlet UIButton *btn_edit;
@property(nonatomic,retain)IBOutlet UIButton *btn_edit_tbl;
@property(nonatomic,retain) LORichTextLabelStyle *username_style;

@property(nonatomic,retain) NSString *user_id;
@property(nonatomic,retain)IBOutlet UIImageView *img_down_arrow;
@property(nonatomic,retain)IBOutlet UIImageView *img_down_arrow_tbl;

@property(nonatomic,retain)IBOutlet  UIView *view_secondcell;
@property(nonatomic,retain)IBOutlet  UIView *view_secondcell_tbl;

@property(nonatomic,retain)UIImagePickerController *photoPickerController;

@property(nonatomic,retain) Profile_share *profile_share_obj;
@property(nonatomic,retain) Followers_list_iPad *followers_list_viewObj;
@property(nonatomic,retain) Image_detail_iPad *image_detail_viewObj;
@property(nonatomic,retain) Likers_list_ViewController_iPad *liker_list_viewObj;
@property(nonatomic,retain) Comments_list_ViewController_iPad *comments_list_viewObj;
@property(nonatomic,retain) Edit_your_profile *edit_your_profile_viewObj;
@property(nonatomic,retain) Setting_profile *setting_profileViewObj;

@property(nonatomic,retain) Share_photo_iPad *share_photo_view;
@property(nonatomic,retain)IBOutlet UIImageView *img_like_heart;
@property(nonatomic,retain) NSMutableArray *array_feeds;
//Footer view
@property(nonatomic,retain)IBOutlet UIView *view_footer;
@property(nonatomic,retain)IBOutlet UIImageView *img_spinner;
@property(nonatomic,retain)IBOutlet UIView *view_footer_tbl;
@property(nonatomic,retain)IBOutlet UIImageView *img_spinner_tbl;

@property(nonatomic,retain)Show_map_ViewController_iPad *mapviewObj;

@property(nonatomic,retain)IBOutlet  UICollectionView *collectionViewObj;

//Header view
@property(nonatomic,retain)IBOutlet UIView *view_header;
@property(nonatomic,retain)IBOutlet EGOImageView *img_photo;
@property(nonatomic,retain)IBOutlet UIView *view_img_photo;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_scondcell;

@property(nonatomic,retain)IBOutlet UIView *view_header_tbl;
@property(nonatomic,retain)IBOutlet EGOImageView *img_photo_tbl;
@property(nonatomic,retain)IBOutlet UIView *view_img_photo_tbl;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_firstcell_tbl;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_scondcell_tbl;

@property(nonatomic,retain)IBOutlet UILabel *lbl_total_followers;
@property(nonatomic,retain)IBOutlet UILabel *lbl_total_followings;
@property(nonatomic,retain)IBOutlet UILabel *lbl_total_photos;

@property(nonatomic,retain)IBOutlet UILabel *lbl_total_followers_tbl;
@property(nonatomic,retain)IBOutlet UILabel *lbl_total_followings_tbl;
@property(nonatomic,retain)IBOutlet UILabel *lbl_total_photos_tbl;

@property(nonatomic,retain)IBOutlet LORichTextLabel *lbl_name;
@property(nonatomic,retain)IBOutlet LORichTextLabel *lbl_name_tbl;

//UITableview
@property(nonatomic,retain)IBOutlet UITableView *tbl_news;
@property(nonatomic,retain)IBOutlet UIButton *btn_follow;
@property(nonatomic,retain)IBOutlet UIButton *btn_follow_tbl;

@end
