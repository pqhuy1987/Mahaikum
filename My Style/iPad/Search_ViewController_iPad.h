//
//  Search_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "image_collection_header.h"
#import "image_collection_footer.h"
#import "image_collection_cell.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Home_tableview_data_share.h"
#import "AJNotificationView.h"
#import "Comment_share.h"
#import "Image_detail_iPad.h"

#import "User_info_ViewController_iPad.h"
#import "FMDatabase.h"

#import "search_view_hashtag_cell_iPad.h"
#import "search_view_cell_iPad.h"


#import "Hash_tag_ViewController_iPad.h"
#import "MCSegmentedControl.h"

@interface Search_ViewController_iPad : UIViewController{
    
    User_info_ViewController_iPad *user_info_view;
    
    UICollectionView *collectionViewObj;
    NSMutableArray *array_search;
    UIView *view_header;
    UIView *view_footer;
    UIImageView *img_spinner;
    Image_detail_iPad *image_detail_viewObj;
    
    UIView *view_search;
    UITextField *txt_search;
    
//    MCSegmentedControl *segment;
    UISegmentedControl *segment;
    
    UITableView *tbl_users;
    UITableView *tbl_hashtag;
    NSMutableArray *array_search_result;
    NSMutableArray *array_hashtag_result;
    
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
    
    UIView *view_fillter;
    
    UILabel *lbl_header;
    int which_filter;
}
@property(nonatomic,retain)IBOutlet UILabel *lbl_header;
@property(nonatomic,retain)IBOutlet UIView *view_fillter;
@property(nonatomic,retain) User_info_ViewController_iPad *user_info_view;
@property(nonatomic,retain)IBOutlet UIView *view_search;
@property(nonatomic,retain)IBOutlet UITextField *txt_search;
//@property(nonatomic,retain)IBOutlet MCSegmentedControl *segment;
@property(nonatomic,retain)IBOutlet UISegmentedControl *segment;
@property(nonatomic,retain)IBOutlet  UICollectionView *collectionViewObj;
@property(nonatomic,retain)IBOutlet  NSMutableArray *array_search;
@property(nonatomic,retain)IBOutlet UIView *view_header;
@property(nonatomic,retain)IBOutlet UIView *view_footer;
@property(nonatomic,retain)IBOutlet UIImageView *img_spinner;
@property(nonatomic,retain) Image_detail_iPad *image_detail_viewObj;

@property(nonatomic,retain)IBOutlet  UITableView *tbl_users;
@property(nonatomic,retain)IBOutlet  UITableView *tbl_hashtag;
@property(nonatomic,retain) NSMutableArray *array_search_result;
@property(nonatomic,retain)  NSMutableArray *array_hashtag_result;
@property(nonatomic,retain) Hash_tag_ViewController_iPad *hash_tag_viewObj;

@property(nonatomic,retain)IBOutlet UIButton *btn_reload;

-(IBAction)btn_fillter_clikc:(id)sender;

@end
