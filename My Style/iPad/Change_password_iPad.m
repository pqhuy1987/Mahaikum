//
//  Change_password_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Change_password_iPad.h"

@interface Change_password_iPad ()

@end

@implementation Change_password_iPad

@synthesize txt_old_password,txt_new_password,txt_new_password_again,img_bd_firstcell,img_bd_secondcell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.img_bd_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bd_firstcell.layer.cornerRadius =5.0f;
    //  self.img_bd_firstcell.layer.shadowOffset = CGSizeMake(0,0.1);
    //  self.img_bd_firstcell.layer.shadowRadius = 5.0;
    //  self.img_bd_firstcell.layer.shadowColor = [UIColor whiteColor].CGColor;
    //  self.img_bd_firstcell.layer.shadowOpacity = 0.4;
    
    self.img_bd_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bd_secondcell.layer.cornerRadius =5.0f;
    //  self.img_bd_secondcell.layer.shadowOffset = CGSizeMake(0,0.1);
    //  self.img_bd_secondcell.layer.shadowRadius = 5.0;
    //  self.img_bd_secondcell.layer.shadowColor = [UIColor whiteColor].CGColor;
    // self.img_bd_secondcell.layer.shadowOpacity = 0.4;
    
    
//    UIToolbar *toolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    toolbar.barStyle= UIBarStyleBlackTranslucent;
//    toolbar.items =@[[[UIBarButtonItem alloc]initWithTitle:@"Cancel"style:UIBarButtonItemStyleBordered target:self
//                                                    action:@selector(btn_cancel_click:)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                     [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btn_done_click:)]];
//    [toolbar sizeToFit];
//    
//    self.txt_new_password.inputAccessoryView=toolbar;
//    self.txt_new_password_again.inputAccessoryView=toolbar;
//    self.txt_old_password.inputAccessoryView=toolbar;
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)btn_cancel_click:(id)sender{
    [self.view endEditing:YES];
}
-(IBAction)btn_done_click:(id)sender{
    [self.view endEditing:YES];
    
    if(![self.txt_new_password_again.text isEqualToString:self.txt_new_password.text]){
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Passwords do not match." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    
    if (self.txt_new_password.text.length<6) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Password must be at least 6 characters." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    
    [self change_password];
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if (textField==self.txt_old_password) {
        
        [self.txt_new_password becomeFirstResponder];
        
    }else if(textField==self.txt_new_password){
        
        [self.txt_new_password_again becomeFirstResponder];
        
    }
    return YES;
}

-(void)change_password{
    
    //techintegrity.in/mystyle/post_change_password.php?uid=3&oldpassword=piyush&newpassword=abc123
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = @"coco";
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postPasswordChangeAPIResponce:) name:@"14203" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostPasswordChangeAPIResponce:) name:@"-14203" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_change_password.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",                            self.txt_old_password.text,@"oldpassword",self.txt_new_password.text,@"newpassword",nil];
   // NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14203" :params];
}

-(void)postPasswordChangeAPIResponce:(NSNotification *)notification {
    //[self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]){
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Password change successfully" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-1"]){
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please check your old password" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        return;
    }
}

-(void)FailpostPasswordChangeAPIResponce :(NSNotification *)notification {
    //[self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [img_bd_firstcell release];
    [img_bd_secondcell release];
    
    [txt_old_password release];
    [txt_new_password release];
    [txt_new_password_again release];
    [super dealloc];
    
}
@end
