//
//  Home_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Home_ViewController_iPad.h"
#import "User_info_ViewController_iPad.h"
#import "webview_viewcontroller_iPad.h"
#import "Hash_tag_ViewController_iPad.h"
#import "NPRImageView.h"

@interface Home_ViewController_iPad (){
    User_info_ViewController_iPad *user_info_view;
    webview_viewcontroller_iPad *web_viewObj;
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
}

@end

@implementation Home_ViewController_iPad
@synthesize tbl_news;
@synthesize array_feeds;
@synthesize img_like_heart;
@synthesize comments_list_viewObj,liker_list_viewObj;
@synthesize share_photo_view;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refresh_home_view_data:(NSNotification *)notification{
    
    [self.tbl_news setContentOffset:CGPointZero animated:YES];
    [self get_news_feed];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refresh_home_view_data" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh_home_view_data:) name:@"refresh_home_view_data" object:nil];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen_iPad.png"]];
    
    hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    web_viewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    
    self.share_photo_view=[[Share_photo_iPad alloc]initWithNibName:@"Share_photo_iPad" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController_iPad alloc]initWithNibName:@"Comments_list_ViewController_iPad" bundle:nil];
    
    self.array_feeds=[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"100" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-100" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"101" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"101" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-101" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-101" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"102" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"102" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-102" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-102" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"103" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-103" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-103" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"104" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"104" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-104" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-104" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDecsNotification:) name:IFTweetLabelURLNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlelikeButtonNotification:) name:PMsequenceButtonURLNotification object:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController_iPad alloc]initWithNibName:@"Likers_list_ViewController_iPad" bundle:nil];
    
    UIView *tableview_header =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 61)];
    UIImageView *img_header_bg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 768, 62)];
    [img_header_bg setImage:[UIImage imageNamed:@"header.png"]];
    
    _btn_reload =[UIButton buttonWithType:UIButtonTypeCustom];
    [_btn_reload setImage:[UIImage imageNamed:@"refreshbtn.png"] forState:UIControlStateNormal];
    [_btn_reload addTarget:self action:@selector(btn_refresh_click:) forControlEvents:UIControlEventTouchUpInside];
    [_btn_reload setFrame:CGRectMake(768-50, 10,37,35)];
    
    [tableview_header addSubview:img_header_bg];
    [tableview_header addSubview:_btn_reload];
    self.tbl_news.tableHeaderView =tableview_header;
    
    [self get_news_feed];
    [self.img_like_heart setHidden:YES];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    [self.tbl_news reloadData];

    if ([[[NSUserDefaults standardUserDefaults]objectForKey:IS_HOME_PAGE_REFRESH] isEqualToString:@"1"]) {
        [StaticClass saveToUserDefaults:@"0" :IS_HOME_PAGE_REFRESH];
        [self get_news_feed];
    }
}

#pragma mark Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
}
#pragma mark Get News Feed
-(void)get_news_feed{
    
    _btn_reload.enabled = NO;
    //   http://www.techintegrity.in/mystyle/get_image_detail.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&flag=1",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"100":nil];
}

-(void)getnewsResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        
        [self.array_feeds removeAllObjects];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict in array) {
            
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[dict valueForKey:@"username"];
            
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.lat=[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[tempdict valueForKey:@"username"]];
                }
            }
            else{
                // NSLog(@"NSString");
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[tempdict valueForKey:@"username"];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    [shareObj.array_comments addObject:obj];
                    [obj release];
                }
            }
            else{
                // NSLog(@"NSString");
            }
            
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            
            if (shareObj.image_path.length !=0) {
                [self.array_feeds addObject:shareObj];
            }
            [shareObj release];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadData];
    _btn_reload.enabled = YES;
}

-(void)FailNewsReson:(NSNotification *)notification {
	//[self stopSpinner];
    _btn_reload.enabled = YES;
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    /*
     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"There was a problem with the Internet connection. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     [alert release];
     */
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.array_feeds.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 90.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:section];
    
    UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"strip_iPad.png"]];
    strip.alpha = 1.0;
    strip.frame =CGRectMake(0, 0,768, 90);
    
    NPRImageView *img_user =[[[NPRImageView alloc]initWithFrame:CGRectMake(20, 5, 80, 80)]autorelease];
    [img_user setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    
    img_user.layer.cornerRadius = 40.0;
    img_user.layer.masksToBounds=YES;
    
    EGOImageButton *img_userimage=[[[EGOImageButton alloc]initWithFrame:CGRectMake(20,5, 80,80)] autorelease];
 //   img_userimage.imageURL =[NSURL URLWithString:shareObj.user_image];
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    img_user.layer.cornerRadius = 40.0;
    img_user.layer.masksToBounds=YES;
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:shareObj.username forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(130,10, 550, 50)];
    [btn_username setTitleColor:[UIColor colorWithRed:112.0/255.0 green:112.0/255.0 blue:112.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:26.0];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"locationicon@2x.png"]];
    img_locationicon.frame = CGRectMake(100,73, 32, 40);
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:shareObj.location forState:UIControlStateNormal];
    
    if (shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(100,15, 550, 50)];
        [btn_location setFrame:CGRectMake(140,70,500, 50)];
        img_locationicon.hidden=NO;
    }
    else{
        img_locationicon.hidden=YES;
    }
    
    [btn_location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_location.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //   [btn_location addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon_iPad.png"]];
    
    [img_clock setFrame:CGRectMake(600, 25, 124,41)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:shareObj.datecreated];
    lbl_time.font = [UIFont fontWithName:@"Quicksand" size:30.0];
//    lbl_time.font = [UIFont boldSystemFontOfSize:30];
    lbl_time.textColor =[UIColor whiteColor];
    lbl_time.frame =CGRectMake(650, 30,80,32);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,768, 90)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =0.9;
    
    [view addSubview:strip];
    [view addSubview:img_user];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    [view addSubview:lbl_time];

    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    return [Home_tableview_cell_iPad get_tableview_hight:shareObj];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Home_tableview_cell_iPad";
	Home_tableview_cell_iPad *cell = (Home_tableview_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        [cell setBackgroundColor:[UIColor clearColor]];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
        [cell.img_big addGestureRecognizer:tapImage];
        
        //
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
	}
    
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    
    [cell redraw_cell:shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    cell.dlstarObj.delegate = self;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_delete =tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            [alert release];
            
        }];
        [sheet addButtonWithTitle:@"Share Photo" block:^{

            self.share_photo_view.str_img_url = shareObj.image_path;
            NSLog(@"%@",shareObj.image_path);
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{

            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            NSLog(@"");
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"My Style\"></body></html>",shareObj.image_path];
                NSLog(@"%@",htmlStr);
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"I don't like this photo",@"This photo is spam or a scam",@"This photo puts people at risk",@"This photo shouldn't be on mystyle", nil];
        alert.tag =2;
        [alert show];
        [alert release];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    /*  [sheet addButtonWithTitle:@"Show Action Sheet on top" block:^{
     NSLog(@"");
     }];
     [sheet addButtonWithTitle:@"Show another alert" block:^{
     NSLog(@"");
     }];
     */
    [sheet showInView:self.view];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    
    [self.navigationController pushViewController:liker_list_viewObj animated:YES];
}

- (void)handleDecsNotification:(NSNotification *)notification
{
	NSLog(@" %@ Click", [notification object]);
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(void)handlelikeButtonNotification:(NSNotification *)notification{
    
    NSLog(@" %@ Click", [notification object]);
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:buttonIndex];
    }
}

-(void)report_inappropriate:(int )index{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //post_report_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&report_id=2&item_id=12&uid=1
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"103":nil];
}

#pragma mark Delete Photo
-(void)delete_photo{
    //     http://www.techintegrity.in/mystyle/post_delete_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"102":nil];
}

-(void)getdeleteResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_news_feed];
        
    }
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    //  http://www.techintegrity.in/mystyle/post_rating.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&rate=2&item_id=12&uid=1
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%d&item_id=%@&uid=%@",salt,sig,rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"103":nil];
    obj.my_rating = [NSString stringWithFormat:@"%d",rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    // [dlstarObj  setRating:3];
}

-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
    }
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"101":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
        else{
            shareObj.liked=@"no";
            int count =[shareObj.likes intValue];
            count--;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            }
        }
        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark
#pragma mark Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked=tappedRow.section;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
    
    if ([shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    // NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"104":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        // NSLog(@"%@",shareObj.image_id);
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            // NSLog(@"%@",shareObj.array_liked_by);
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        }
    }
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}
#pragma mark Go to Profile

-(IBAction)btn_profile_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    NSLog(@"%d",tag);
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    //self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    //    if (result == MFMailComposeResultSent) {
    //
    //    }
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    [dateFormatter release];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [share_photo_view release];
    [comments_list_viewObj release];
    [img_like_heart release];
    [array_feeds release];
    [liker_list_viewObj release];
    [tbl_news release];
    [_btn_reload release];
    [super dealloc];
}

@end
