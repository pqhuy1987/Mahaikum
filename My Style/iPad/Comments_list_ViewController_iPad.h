//
//  Comments_list_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "Comments_list_cell_iPad.h"
#import "UIImageView+PMRoundEffect_imageview.h"
#import "AJNotificationView.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Comment_share.h"
#import "BlockActionSheet.h"
#import "Home_tableview_data_share.h"

@interface Comments_list_ViewController_iPad : UIViewController<HPGrowingTextViewDelegate>{


    UIView *containerView;
    HPGrowingTextView *textView;
    UIButton *doneBtn;
    
    UITableView *tbl_comments;
    
    
    //Swipe
    //  IBOutlet UITableView* tableView;
    IBOutlet UIView* sideSwipeView;
    UITableViewCell* sideSwipeCell;
    UISwipeGestureRecognizerDirection sideSwipeDirection;
    BOOL animatingSideSwipe;
    
    UIButton *btn_reply;
    UIButton *btn_delete;
    
    NSString *image_id;
    NSString *str_comments;
    
    NSMutableArray *array_comments;
    
    
    NSString *comment_id;
    
    
}
@property(nonatomic,retain)NSString *comment_id;
@property(nonatomic,retain) NSMutableArray *array_comments;

@property(nonatomic,retain) NSString *image_id;
@property(nonatomic,retain)NSString *str_comments;
@property(nonatomic,retain) UIButton *btn_reply;
@property(nonatomic,retain) UIButton *btn_delete;

@property(nonatomic,retain)IBOutlet UITableView *tbl_comments;
@property(nonatomic,retain) UIButton *doneBtn;
-(void)resignTextView;

//swipe
//@property (nonatomic, retain) IBOutlet UITableView* tableView;
@property (nonatomic, retain) IBOutlet UIView* sideSwipeView;
@property (nonatomic, retain) UITableViewCell* sideSwipeCell;
@property (nonatomic) UISwipeGestureRecognizerDirection sideSwipeDirection;
@property (nonatomic) BOOL animatingSideSwipe;

- (void) removeSideSwipeView:(BOOL)animated;
- (BOOL) gestureRecognizersSupported;


@end
