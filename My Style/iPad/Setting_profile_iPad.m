//
//  Setting_profile_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Setting_profile_iPad.h"

@interface Setting_profile_iPad ()

@end

@implementation Setting_profile_iPad

@synthesize scrollview,switch_private,swith_photos_save;
@synthesize img_firstcell,img_secondcell,img_thirdcell,img_fourthcell;
@synthesize notification_settingViewObj,find_inviteViewObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.notification_settingViewObj= [[Notification_setting_ViewController_iPad alloc]initWithNibName:@"Notification_setting_ViewController_iPad" bundle:nil];
    self.find_inviteViewObj =[[Find_invite_friend_iPad alloc]initWithNibName:@"Find_invite_friend_iPad" bundle:nil];
    
    self.img_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_firstcell.layer.cornerRadius =4.0f;
    
    self.img_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_secondcell.layer.cornerRadius =4.0f;
    
    
    self.img_thirdcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_thirdcell.layer.cornerRadius =4.0f;
    
    
    self.img_fourthcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_fourthcell.layer.cornerRadius =4.0f;
    
    
    
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"0"]) {
        [self.swith_photos_save setOn:NO animated:YES];
        
    }else{
        [self.swith_photos_save setOn:YES animated:YES];
        
    }
    self.swith_photos_save.onText = NSLocalizedString(@"ON", @"");
	self.swith_photos_save.offText = NSLocalizedString(@"OFF", @"");
    self.swith_photos_save.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    [self.swith_photos_save addTarget:self action:@selector(btn_photo_save_valuechange:) forControlEvents:UIControlEventValueChanged];
    
    
    self.switch_private.onText = NSLocalizedString(@"ON", @"");
	self.switch_private.offText = NSLocalizedString(@"OFF", @"");
    self.switch_private.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    [self.switch_private addTarget:self action:@selector(btn_private_valuechange:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    is_private_flag = 0;
    
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
     self.scrollview.contentSize=CGSizeMake(768, 1060);

}
-(IBAction)btn_find_invite_click:(id)sender{
    
    [self.navigationController pushViewController:self.find_inviteViewObj animated:YES];
}

#pragma mark Logout
-(IBAction)btn_logout_click:(id)sender{
    [StaticClass saveToUserDefaults:@"0" :USER_IS_LOGIN];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
}
#pragma mark Push notification setting

-(IBAction)btn_push_notification_btn_click:(id)sender{
    [self.navigationController pushViewController:self.notification_settingViewObj animated:YES];
}

#pragma mark photos you have liked
-(IBAction)btn_photos_you_have_liked:(id)sender{
    Photos_you_liked_iPad *obj =[[Photos_you_liked_iPad alloc]initWithNibName:@"Photos_you_liked_iPad" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
    
}
#pragma mark - private switch click
-(IBAction)btn_private_valuechange:(id)sender{
    
    if (is_private_flag==1) {
        
        
        if (switch_private.isOn) {
            NSLog(@"is ON");
            
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy on. This means from now on only people you approve will be able to follow your photos. Are you sure you want to turn privacy on?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
            [alert release];
        }else{
            NSLog(@"is OFF");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy off. This means from now on. anyone will be able to follow your photos. Are you sure you want to turn privacy off?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
            [alert show];
            [alert release];
        }
        is_private_flag=0;
    }else{
        
        is_private_flag=1;
    }
}
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (switch_private.isOn) {
        if (buttonIndex==1) {
            [switch_private setOn:YES animated:NO];
            is_private_flag=1;
        }else{
            [switch_private setOn:NO animated:YES];
            is_private_flag=0;
        }
    }else{
        if (buttonIndex==1) {
            [switch_private setOn:NO animated:NO];
            is_private_flag=1;
        }else{
            [switch_private setOn:YES animated:YES];
            is_private_flag=0;
        }
    }
}

-(IBAction)btn_privacy_policy_click:(id)sender{
    webview_viewcontroller_iPad *obj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    // obj.web_url =@"http://instagram.com/legal/privacy/";
    obj.web_url =[[Singleton sharedSingleton]get_privacy_police_url];
    [self.navigationController pushViewController:obj animated:YES];
}

-(IBAction)btn_terms_of_service_click:(id)sender{
    webview_viewcontroller_iPad *obj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    // obj.web_url =@"http://instagram.com/legal/privacy/";
    obj.web_url =[[Singleton sharedSingleton]get_terms_of_use_url];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - Photo save


-(IBAction)btn_photo_save_valuechange:(id)sender{
    
    if (swith_photos_save.isOn) {
        [StaticClass saveToUserDefaults:@"1" :PHOTOS_SAVE_LIBRARY];
    }else{
        [StaticClass saveToUserDefaults:@"0" :PHOTOS_SAVE_LIBRARY];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [find_inviteViewObj release];
    [switch_private release];
    [notification_settingViewObj release];
    [swith_photos_save release];
    
    [scrollview release];
    [img_firstcell release];
    [img_secondcell  release];
    [img_thirdcell release];
    [img_fourthcell release];
    [super dealloc];
}@end
