//
//  Image_detail_url_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Comment_share.h"
#import "Home_tableview_cell_iPad.h"
#import "DLStarRatingControl.h"
#import <MessageUI/MFMailComposeViewController.h>

#import "BlockActionSheet.h"
#import "Likers_list_ViewController_iPad.h"
#import "Comments_list_ViewController_iPad.h"
#import "Share_photo_iPad.h"

@interface Image_detail_url_iPad : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate>{
    
    NSString *image_id;
    
    Home_tableview_data_share *shareObj;
    int which_image_liked;
    int which_image_delete;
    UIImageView *img_like_heart;
    
    Likers_list_ViewController_iPad *liker_list_viewObj;
    Comments_list_ViewController_iPad *comments_list_viewObj;
    Share_photo_iPad *share_photo_view;
    
    UITableView *tbl_news;
}
@property(nonatomic,retain) NSString *image_id;

@property(nonatomic,retain)IBOutlet  UITableView *tbl_news;
@property(nonatomic,retain) Likers_list_ViewController_iPad *liker_list_viewObj;
@property(nonatomic,retain) Comments_list_ViewController_iPad *comments_list_viewObj;
@property(nonatomic,retain) Share_photo_iPad *share_photo_view;

@property(nonatomic,retain) Home_tableview_data_share *shareObj;
@property(nonatomic,retain)IBOutlet UIImageView *img_like_heart;

@end
