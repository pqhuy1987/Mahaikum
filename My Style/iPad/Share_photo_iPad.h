//
//  Share_photo_iPad.h
//  My Style
//
//  Created by Tis Macmini on 8/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "EGOImageView.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

@class NPRImageView;

@interface Share_photo_iPad : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>{
    
    
    UIScrollView *scrollview;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_secondcell;
    
    NPRImageView *img_show_photo;
    UIView *view_bg_show_photo;
    UITextView *txt_desc;
    
    
    UIView *view_third_cell;
    UIImageView *img_bg_third_cell;
    
    NSString *str_img_url;
    
}


@property(nonatomic,retain)NSString *str_img_url;

@property(nonatomic,retain)IBOutlet UITextView *txt_desc;
@property(nonatomic,retain)IBOutlet NPRImageView *img_show_photo;
@property(nonatomic,retain)IBOutlet UIView *view_bg_show_photo;

@property(nonatomic,retain)IBOutlet UIImageView *img_bg_firstcell;

@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;


@property(nonatomic,retain)IBOutlet UIView *view_third_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_third_cell;

@end
