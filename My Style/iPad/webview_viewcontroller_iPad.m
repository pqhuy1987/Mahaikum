//
//  webview_viewcontroller_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "webview_viewcontroller_iPad.h"

@interface webview_viewcontroller_iPad ()

@end

@implementation webview_viewcontroller_iPad
@synthesize web_url,webview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.web_url]]];
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [self start_activity];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self stop_activity];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self stop_activity];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    self.web_url =[NSString stringWithFormat:@"%@",request.URL];
    return YES;
}

-(IBAction)btn_reload_click:(id)sender{
    
    [self.webview reload];
}
-(IBAction)btn_next_click:(id)sender{
    [self.webview goForward];
    
}
-(IBAction)btn_previeus_click:(id)sender{
    [self.webview goBack];
}

-(IBAction)btn_open_in_safari_click:(id)sender{
    
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@""];
    
    [sheet setDestructiveButtonWithTitle:@"Open in Safari" block:^{
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.web_url]];
        
    }];
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
    
}
#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}
-(void)stop_activity{
    [DejalBezelActivityView removeView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [webview release];
    [web_url release];
    [super dealloc];
    
}

@end
