//
//  Notification_setting_cell_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Notification_setting_cell_iPad :UITableViewCell{
    
    UILabel *lbl_title;
    UIImageView *img_checkmark;
    UIImageView *img_bg;
}


@property(nonatomic,retain)IBOutlet UILabel *lbl_title;
@property(nonatomic,retain)IBOutlet UIImageView *img_checkmark;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg;

@end
