//
//  Likers_list_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/25/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Likers_list_cell_iPad.h"



@implementation Likers_list_cell_iPad

@synthesize img_user,lbl_name,lbl_decs;

-(void)dealloc{
    [img_user release];
    [lbl_name release];
    [lbl_decs release];
    
    [super dealloc];
}

@end
