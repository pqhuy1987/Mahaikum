//
//  Comments_list_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Comments_list_ViewController_iPad.h"
#import "User_info_ViewController_iPad.h"
#import "webview_viewcontroller_iPad.h"
#import "Hash_tag_ViewController_iPad.h"

#define BOUNCE_PIXELS 5.0
#define PUSH_STYLE_ANIMATION NO
#define USE_GESTURE_RECOGNIZERS YES

@interface Comments_list_ViewController_iPad (){
    
    User_info_ViewController_iPad *user_info_view;
    webview_viewcontroller_iPad *web_viewObj;
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
}


@end
@interface Comments_list_ViewController_iPad (PrivateStuff)
- (void) addSwipeViewTo:(UITableViewCell*)cell direction:(UISwipeGestureRecognizerDirection)direction;
- (void) setupGestureRecognizers;
- (void) swipe:(UISwipeGestureRecognizer *)recognizer direction:(UISwipeGestureRecognizerDirection)direction;
@end

@implementation Comments_list_ViewController_iPad

@synthesize doneBtn,tbl_comments;
@synthesize sideSwipeView, sideSwipeCell, sideSwipeDirection, animatingSideSwipe;
@synthesize btn_reply,btn_delete;
@synthesize image_id,str_comments;
@synthesize array_comments,comment_id;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    /*
     for (Comment_share *obj in self.array_comments) {
     [obj release];
     obj=nil;
     }
     */
    //   [self.array_comments removeAllObjects];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.array_comments=[[NSMutableArray alloc]init];
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    web_viewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"201" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_comment_get:) name:@"201" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-201" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailcommentsReson:) name:@"-201" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"202" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_delete_comment:) name:@"202" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-202" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailcommentsReson:) name:@"-202" object:nil];
    
    animatingSideSwipe = NO;
    [self setupGestureRecognizers];
    
    
    btn_delete=[UIButton buttonWithType:UIButtonTypeCustom];
    btn_delete.frame=CGRectMake(20,10,88,88);
    [btn_delete setImage:[UIImage imageNamed:@"comments-actions-icon-delete@2x.png"] forState:UIControlStateNormal];
    [btn_delete addTarget:self action:@selector(btn_delete_click:) forControlEvents:UIControlEventTouchUpInside];
    [sideSwipeView addSubview:btn_delete];
    
    btn_reply=[UIButton buttonWithType:UIButtonTypeCustom];
    btn_reply.frame=CGRectMake(20,10,88,88);
    [btn_reply setImage:[UIImage imageNamed:@"comments-actions-icon-reply@2x.png"] forState:UIControlStateNormal];
    [btn_reply addTarget:self action:@selector(btn_reply_click:) forControlEvents:UIControlEventTouchUpInside];
    [sideSwipeView addSubview:btn_reply];
    sideSwipeView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 40, 768,80)];
    
	textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(8,16, 606, 80)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 6;
	textView.font = [UIFont systemFontOfSize:30.0f];
	textView.delegate = self;
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
    
    // textView.text = @"test\n\ntest";
	// textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"MessageEntryInputField@2x.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:26 topCapHeight:44];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(5, 0, 612, 80);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground@2x.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:26 topCapHeight:44];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    textView.textColor =[UIColor grayColor];
    textView.text =@"Add a comment...";
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    
	doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	doneBtn.frame = CGRectMake(containerView.frame.size.width - 138,16,126, 54);
    doneBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[doneBtn setTitle:@"Send" forState:UIControlStateNormal];
    
    [doneBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    doneBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    doneBtn.titleLabel.font = [UIFont boldSystemFontOfSize:38.0f];
    
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [doneBtn setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:138.0f/255.0f blue:0.0f/255.0f alpha:1]];
    [self addGradient:doneBtn];
    
	[containerView addSubview:doneBtn];
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    

    containerView.frame = CGRectMake(0, self.view.frame.size.height -80, 768,80);
    [self.tbl_comments reloadData];
    [self get_comments_list];
}

#pragma mark
#pragma mark Get Comments list
-(void)get_comments_list{
    //http://www.techintegrity.in/mystyle/get_comments.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,self.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"201":nil];
}
-(void)response_comment_get:(NSNotification *)notification{
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    [self.array_comments removeAllObjects];
    
    [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *tempArray =[result valueForKey:@"data"];
        for (NSDictionary *dict in tempArray) {
            Comment_share *obj =[[Comment_share alloc]init];
            
            obj.comment_id =[dict valueForKey:@"id"];
            obj.uid =[dict valueForKey:@"uid"];
            obj.username =[dict valueForKey:@"username"];
            obj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            obj.image_url=[StaticClass urlDecode:[dict valueForKey:@"image"]];
            obj.comment_desc=[StaticClass urlDecode:[dict valueForKey:@"comment_desc"]];
            obj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            [self.array_comments addObject:obj];
            [obj release];
        }
        
    }
    
    [self.tbl_comments reloadData];
    
}
-(void)FailcommentsReson:(NSNotification *)notification {
	//[self stopSpinner];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Comment couldn't get. Please check your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    
}



#pragma mark Side Swiping under iOS 4.x
- (BOOL) gestureRecognizersSupported
{
    if (!USE_GESTURE_RECOGNIZERS) return NO;
    
    // Apple's docs: Although this class was publicly available starting with iOS 3.2, it was in development a short period prior to that
    // check if it responds to the selector locationInView:. This method was not added to the class until iOS 3.2.
    return [[[[UISwipeGestureRecognizer alloc] init] autorelease] respondsToSelector:@selector(locationInView:)];
}

- (void) setupGestureRecognizers
{
    // Do nothing under 3.x
    if (![self gestureRecognizersSupported]) return;
    
    // Setup a right swipe gesture recognizer
    UISwipeGestureRecognizer* rightSwipeGestureRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)] autorelease];
    rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.tbl_comments addGestureRecognizer:rightSwipeGestureRecognizer];
    
    //  Setup a left swipe gesture recognizer
    UISwipeGestureRecognizer* leftSwipeGestureRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)] autorelease];
    leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.tbl_comments addGestureRecognizer:leftSwipeGestureRecognizer];
    
    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:) ];
    [tapImage setNumberOfTapsRequired:1];
    [self.tbl_comments addGestureRecognizer:tapImage];
}

// Called when a left swipe occurred

- (void)swipeLeft:(UISwipeGestureRecognizer *)recognizer
{
    [self swipe:recognizer direction:UISwipeGestureRecognizerDirectionRight];
}

// Called when a right swipe ocurred
- (void)swipeRight:(UISwipeGestureRecognizer *)recognizer
{
    [self swipe:recognizer direction:UISwipeGestureRecognizerDirectionRight];
}

// Handle a left or right swipe
- (void)swipe:(UISwipeGestureRecognizer *)recognizer direction:(UISwipeGestureRecognizerDirection)direction
{
    if (recognizer && recognizer.state == UIGestureRecognizerStateEnded)
    {
        // Get the table view cell where the swipe occured
        CGPoint location = [recognizer locationInView:self.tbl_comments ];
        NSIndexPath* indexPath = [self.tbl_comments  indexPathForRowAtPoint:location];
        UITableViewCell* cell = [self.tbl_comments  cellForRowAtIndexPath:indexPath];
        
        // If we are already showing the swipe view, remove it
        if (cell.frame.origin.x != 0)
        {
            [self removeSideSwipeView:YES];
            return;
        }
        
        // Make sure we are starting out with the side swipe view and cell in the proper location
        [self removeSideSwipeView:NO];
        
        // If this isn't the cell that already has thew side swipe view and we aren't in the middle of animating
        // then start animating in the the side swipe view
        if (cell!= sideSwipeCell && !animatingSideSwipe)
            [self addSwipeViewTo:cell direction:direction];
    }
}


#pragma mark UITableview Delegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	Comment_share *obj =[self.array_comments objectAtIndex:indexPath.row];

    CGSize size =[obj.comment_desc sizeWithFont:[UIFont fontWithName:@"Helvetica" size:28.0f] constrainedToSize:CGSizeMake(655, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
    
    //  return 30.0f+size.height;
    if (size.height<50) {
        return 110.0f;
    }
    return 65+size.height+10.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_comments.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Comments_list_cell_iPad";
    
	Comments_list_cell_iPad *cell = (Comments_list_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Comments_list_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_in_cell];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
       // cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_shaare.png"]];
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0f];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.btn_user_name addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.img_user1 addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
	}
    Comment_share *shareObj =[self.array_comments objectAtIndex:indexPath.row];
    
    /*  LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor blueColor]];
     [userStyle addTarget:self action:@selector(userSelected:)];
     [cell.lbl_desc addStyle:userStyle forPrefix:shareObj.username];
     */
    
    cell.img_user1.tag=indexPath.row;
    cell.btn_user_name.tag = indexPath.row;
    
  //  cell.img_user.imageURL=[NSURL URLWithString:shareObj.image_url];
    [cell.img_user setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
    
    [cell.btn_user_name setTitle:shareObj.username forState:UIControlStateNormal];
    [cell.lbl_desc setText:shareObj.comment_desc];
    cell.lbl_date.text =[self get_time_different:shareObj.datecreated];
    
    CGSize size =[shareObj.comment_desc sizeWithFont:[UIFont fontWithName:@"Helvetica" size:28.0f] constrainedToSize:CGSizeMake(655, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
    

 //   cell.view_line.frame = CGRectMake(0,cell.frame.size.height-1, 768,1);
    cell.lbl_desc.frame=CGRectMake(110,65, 655, size.height);
    
    NSLog(@"%f",cell.frame.size.height);
    NSLog(@"%@",NSStringFromCGRect(cell.lbl_desc.frame));
    NSLog(@"%@",NSStringFromCGSize(size));
    cell.tag=indexPath.row;
    
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)tableView:(UITableView *)theTableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If we are using gestures, then don't do anything
    if ([self gestureRecognizersSupported]) return;
    
    // Get the table view cell where the swipe occured
    UITableViewCell* cell = [theTableView cellForRowAtIndexPath:indexPath];
    
    // Make sure we are starting out with the side swipe view and cell in the proper location
    [self removeSideSwipeView:NO];
    
    // If this isn't the cell that already has thew side swipe view and we aren't in the middle of animating
    // then start animating in the the side swipe view. We don't have access to the direction, so we always assume right
    if (cell!= sideSwipeCell && !animatingSideSwipe)
        [self addSwipeViewTo:cell direction:UISwipeGestureRecognizerDirectionRight];
}

// Apple's docs: To enable the swipe-to-delete feature of table views (wherein a user swipes horizontally across a row to display a Delete button), you must implement the tableView:commitEditingStyle:forRowAtIndexPath: method.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If we are using gestures, then don't allow editing
    if ([self gestureRecognizersSupported])
        return NO;
    else
        return YES;
}

#pragma mark Adding the side swipe view
- (void) addSwipeViewTo:(UITableViewCell*)cell direction:(UISwipeGestureRecognizerDirection)direction
{
    // Change the frame of the side swipe view to match the cell
    NSLog(@"%d",cell.tag);
    
    sideSwipeView.hidden = NO;
    CGRect frame =cell.frame;
    frame.size.width=240;
    sideSwipeView.frame = frame;
    sideSwipeView.layer.masksToBounds = YES;
    
    btn_delete.tag=cell.tag;
    btn_reply.tag = cell.tag;
    btn_delete.frame=CGRectMake(10,(frame.size.height/2)-44,88,88);
    btn_reply.frame=CGRectMake(108,(frame.size.height/2)-44,88,88);
    
    
    
    // Add the side swipe view to the table below the cell
    [self.tbl_comments insertSubview:sideSwipeView belowSubview:cell];
    
    // Remember which cell the side swipe view is displayed on and the swipe direction
    self.sideSwipeCell = cell;
    sideSwipeDirection = direction;
    
    // CGRect cellFrame = cell.frame;
    CGRect cellFrame = frame;
    if (PUSH_STYLE_ANIMATION)
    {
        // Move the side swipe view offscreen either to the left or the right depending on the swipe direction
        sideSwipeView.frame = CGRectMake(direction == UISwipeGestureRecognizerDirectionRight ? -cellFrame.size.width : cellFrame.size.width, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
    }
    else
    {
        // Move the side swipe view to offset 0
        sideSwipeView.frame = CGRectMake(0, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
    }
    
    // Animate in the side swipe view
    animatingSideSwipe = YES;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStopAddingSwipeView:finished:context:)];
    if (PUSH_STYLE_ANIMATION)
    {
        // Move the side swipe view to offset 0
        // While simultaneously moving the cell's frame offscreen
        // The net effect is that the side swipe view is pushing the cell offscreen
        sideSwipeView.frame = CGRectMake(0, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
    }
    cell.frame = CGRectMake(direction == UISwipeGestureRecognizerDirectionRight ? cellFrame.size.width : -cellFrame.size.width, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
    [UIView commitAnimations];
}

// Note that the animation is done
- (void)animationDidStopAddingSwipeView:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    animatingSideSwipe = NO;
}

#pragma mark Removing the side swipe view
// UITableViewDelegate
// When a row is selected, animate the removal of the side swipe view
- (NSIndexPath *)tableView:(UITableView *)theTableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeSideSwipeView:YES];
    return indexPath;
}

// UIScrollViewDelegate
// When the table is scrolled, animate the removal of the side swipe view
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeSideSwipeView:YES];
}

// When the table is scrolled to the top, remove the side swipe view
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    
    
    [self removeSideSwipeView:NO];
    return YES;
}

// Remove the side swipe view.
// If animated is YES, then the removal is animated using a bounce effect
- (void) removeSideSwipeView:(BOOL)animated
{
    // Make sure we have a cell where the side swipe view appears and that we aren't in the middle of animating
    if (!sideSwipeCell || animatingSideSwipe) return;
    
    if (animated)
    {
        // The first step in a bounce animation is to move the side swipe view a bit offscreen
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        if (sideSwipeDirection == UISwipeGestureRecognizerDirectionRight)
        {
            if (PUSH_STYLE_ANIMATION)
                sideSwipeView.frame = CGRectMake(-sideSwipeView.frame.size.width + BOUNCE_PIXELS,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
            sideSwipeCell.frame = CGRectMake(BOUNCE_PIXELS, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
        }
        else
        {
            if (PUSH_STYLE_ANIMATION)
                sideSwipeView.frame = CGRectMake(sideSwipeView.frame.size.width - BOUNCE_PIXELS,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
            sideSwipeCell.frame = CGRectMake(-BOUNCE_PIXELS, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
        }
        animatingSideSwipe = YES;
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStopOne:finished:context:)];
        [UIView commitAnimations];
    }
    else
    {
        [sideSwipeView removeFromSuperview];
        sideSwipeCell.frame = CGRectMake(0,sideSwipeCell.frame.origin.y,sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
        self.sideSwipeCell = nil;
    }
}

#pragma mark Bounce animation when removing the side swipe view
// The next step in a bounce animation is to move the side swipe view a bit on screen
- (void)animationDidStopOne:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    if (sideSwipeDirection == UISwipeGestureRecognizerDirectionRight)
    {
        if (PUSH_STYLE_ANIMATION)
            sideSwipeView.frame = CGRectMake(-sideSwipeView.frame.size.width + BOUNCE_PIXELS*2,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
        sideSwipeCell.frame = CGRectMake(BOUNCE_PIXELS*2, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
    }
    else
    {
        if (PUSH_STYLE_ANIMATION)
            sideSwipeView.frame = CGRectMake(sideSwipeView.frame.size.width - BOUNCE_PIXELS*2,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
        sideSwipeCell.frame = CGRectMake(-BOUNCE_PIXELS*2, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
    }
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStopTwo:finished:context:)];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView commitAnimations];
}

// The final step in a bounce animation is to move the side swipe completely offscreen
- (void)animationDidStopTwo:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [UIView commitAnimations];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    if (sideSwipeDirection == UISwipeGestureRecognizerDirectionRight)
    {
        if (PUSH_STYLE_ANIMATION)
            sideSwipeView.frame = CGRectMake(-sideSwipeView.frame.size.width ,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
        sideSwipeCell.frame = CGRectMake(0, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
    }
    else
    {
        if (PUSH_STYLE_ANIMATION)
            sideSwipeView.frame = CGRectMake(sideSwipeView.frame.size.width ,sideSwipeView.frame.origin.y,sideSwipeView.frame.size.width, sideSwipeView.frame.size.height);
        sideSwipeCell.frame = CGRectMake(0, sideSwipeCell.frame.origin.y, sideSwipeCell.frame.size.width, sideSwipeCell.frame.size.height);
    }
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStopThree:finished:context:)];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView commitAnimations];
}

// When the bounce animation is completed, remove the side swipe view and reset some state
- (void)animationDidStopThree:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    animatingSideSwipe = NO;
    self.sideSwipeCell = nil;
    [sideSwipeView removeFromSuperview];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    textView.text =@"Add a comment...";
	[textView resignFirstResponder];
}



#pragma mark
#pragma mark TextView for Comments

-(void)resignTextView
{
    self.str_comments=textView.text;
    
    textView.text =@"Add a comment...";
	[textView resignFirstResponder];
    
    if (![self.str_comments isEqualToString:@"Add a comment..."]) {
        [self post_comments];
        
    }
}

-(void) keyboardWillShow:(NSNotification *)note{
    
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
    
	containerView.frame = containerFrame;
	
	[UIView commitAnimations];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	containerView.frame = containerFrame;
	
	// commit animations
	[UIView commitAnimations];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView{
    
    textView.text =@"Add a comment...";
    return YES;
}
- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    return YES;
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView{
    
    if (growingTextView.text.length==0) {
        doneBtn.enabled=NO;
    }else{
        doneBtn.enabled=YES;
    }
}
- (BOOL)growingTextViewShouldBeginEditing:(HPGrowingTextView *)growingTextView;{
    
    if ([textView.text isEqualToString:@"Add a comment..."]) {
        textView.text =@"";
    }
    return YES;
}
#pragma mark
#pragma mark UIButton Gradiant
-(void) addGradient:(UIButton *) _button {
    
    
    CALayer *layer = _button.layer;
    layer.cornerRadius = 5.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = [UIColor colorWithWhite:0.5f alpha:0.2f].CGColor;
    
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.8f],
                            [NSNumber numberWithFloat:1.0f],
                            nil];
    [layer addSublayer:shineLayer];
}

#pragma mark
#pragma mark Delete Button click
-(IBAction)btn_delete_click:(id)sender{
    
    NSLog(@"%d",((UIButton *)sender).tag);
    [self removeSideSwipeView:YES];
    // http://www.techintegrity.in/mystyle/post_delete_comment.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=12&uid=1
    Comment_share *obj =[self.array_comments objectAtIndex:((UIButton *)sender).tag];
    
    
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@""];
    
    [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
        NSLog(@"Delete");
        
        [self.array_comments removeObjectAtIndex:((UIButton *)sender).tag];
        //  [self.array_comments removeObject:obj];
        [self.tbl_comments beginUpdates];
        [self.tbl_comments deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:((UIButton *)sender).tag inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tbl_comments endUpdates];
        [self.tbl_comments reloadData];
        
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = @"coco";
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@&uid=%@",salt,sig,obj.comment_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
        NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_comment.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        NSLog(@"%@",requestStr);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems :requestStr:@"202":nil];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
}
-(void)response_delete_comment:(NSNotification *)notification{
    
}

#pragma mark
#pragma mark Reply click
-(IBAction)btn_reply_click:(id)sender{
    Comment_share *obj =[self.array_comments objectAtIndex:((UIButton *)sender).tag];
    textView.text=[NSString stringWithFormat:@"@%@ ",obj.username];
    [textView becomeFirstResponder];
    NSLog(@"%d",((UIButton *)sender).tag);
    [self removeSideSwipeView:YES];
}

#pragma mark
#pragma mark Post Comments
-(void)post_comments{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14204" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postCommentAPIResponce:) name:@"14204" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14204" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostCommentAPIResponce:) name:@"-14204" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_comment.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",                            self.image_id,@"image_id",self.str_comments,@"comment_desc",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14204" :params];
}


-(void)postCommentAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14204" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14204" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_comments_list];
    }
}

-(void)FailpostCommentAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14204" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14204" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(IBAction)btn_user_info_click:(id)sender{
    
    
    Comment_share *shareObj =[self.array_comments objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=shareObj.username;

    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    [dateFormatter release];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [comment_id release];
    [array_comments release];
    [image_id release];
    [str_comments release];
    
    [btn_reply release];
    [btn_delete release];
    
    [tbl_comments release];
    [doneBtn release];
    [super dealloc];
}

@end
