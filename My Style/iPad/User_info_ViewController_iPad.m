//
//  User_info_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 7/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "User_info_ViewController_iPad.h"

#import "webview_viewcontroller_iPad.h"
#import "Hash_tag_ViewController_iPad.h"
#import "NPRImageView.h"

@interface User_info_ViewController_iPad (){
    NSString *image_url;
    
    User_info_ViewController_iPad *user_info_view;
    webview_viewcontroller_iPad *web_viewObj;
    Hash_tag_ViewController_iPad *hash_tag_viewObj;
}
@property(nonatomic,retain)NSString *image_url;

@end


@implementation User_info_ViewController_iPad

@synthesize collectionViewObj;
@synthesize view_header,img_photo,img_bg_firstcell,img_bg_scondcell,view_img_photo;
@synthesize view_header_tbl,img_photo_tbl,img_bg_firstcell_tbl,img_bg_scondcell_tbl,view_img_photo_tbl;

@synthesize tbl_news;
@synthesize mapviewObj;

@synthesize view_footer,img_spinner,view_footer_tbl,img_spinner_tbl;
@synthesize array_feeds,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view,image_detail_viewObj,followers_list_viewObj,edit_your_profile_viewObj,setting_profileViewObj;

@synthesize profile_share_obj;
@synthesize lbl_total_followers,lbl_total_followings,lbl_total_photos,lbl_total_followers_tbl,lbl_total_followings_tbl,lbl_total_photos_tbl;
@synthesize lbl_name,lbl_name_tbl,image_url;
@synthesize photoPickerController;
@synthesize view_secondcell,view_secondcell_tbl;
@synthesize img_down_arrow,img_down_arrow_tbl,user_id;
@synthesize btn_follow,btn_follow_tbl;

@synthesize username_style;
@synthesize btn_edit,btn_edit_tbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.btn_edit.hidden = YES;
    self.btn_edit_tbl.hidden = YES;
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    hash_tag_viewObj =[[Hash_tag_ViewController_iPad alloc]initWithNibName:@"Hash_tag_ViewController_iPad" bundle:nil];
    user_info_view=[[User_info_ViewController_iPad alloc]initWithNibName:@"User_info_ViewController_iPad" bundle:nil];
    web_viewObj =[[webview_viewcontroller_iPad alloc]initWithNibName:@"webview_viewcontroller_iPad" bundle:nil];
    
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.profile_share_obj =[[Profile_share alloc]init];
    
    self.array_feeds =[[NSMutableArray alloc]init];
    [self.img_like_heart setHidden:YES];
    
    self.img_down_arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    self.img_down_arrow_tbl.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    
    self.followers_list_viewObj =[[Followers_list_iPad alloc]initWithNibName:@"Followers_list_iPad" bundle:nil];
    self.share_photo_view=[[Share_photo_iPad alloc]initWithNibName:@"Share_photo_iPad" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController_iPad alloc]initWithNibName:@"Comments_list_ViewController_iPad" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController_iPad alloc]initWithNibName:@"Likers_list_ViewController_iPad" bundle:nil];
    
    self.image_detail_viewObj=[[Image_detail_iPad alloc]initWithNibName:@"Image_detail_iPad" bundle:nil];
    
    self.edit_your_profile_viewObj =[[Edit_your_profile alloc]initWithNibName:@"Edit_your_profile" bundle:nil];
    self.setting_profileViewObj=[[Setting_profile alloc]initWithNibName:@"Setting_profile" bundle:nil];
    //  [array_feeds addObject:@"1"];
    
    array_spinner =@[[UIImage imageNamed:@"feedstate-spinner-frame01.png"],[UIImage imageNamed:@"feedstate-spinner-frame02.png"],[UIImage imageNamed:@"feedstate-spinner-frame03.png"],[UIImage imageNamed:@"feedstate-spinner-frame04.png"],[UIImage imageNamed:@"feedstate-spinner-frame05.png"],[UIImage imageNamed:@"feedstate-spinner-frame06.png"],[UIImage imageNamed:@"feedstate-spinner-frame07.png"],[UIImage imageNamed:@"feedstate-spinner-frame08.png"]];
    
    
    self.img_spinner.animationImages =array_spinner;
    self.img_spinner.animationRepeatCount = HUGE_VAL;
    self.img_spinner.animationDuration=1.0f;
    //  [self.img_spinner startAnimating];
    
    self.img_spinner_tbl.animationImages =array_spinner;
    self.img_spinner_tbl.animationRepeatCount = HUGE_VAL;
    self.img_spinner_tbl.animationDuration=1.0f;
    //  [self.img_spinner startAnimating];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    //   self.tbl_news.hidden =YES;
    self.mapviewObj =[[Show_map_ViewController_iPad alloc]initWithNibName:@"Show_map_ViewController_iPad" bundle:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    
    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
    
    
    // header view
    self.img_bg_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell.layer.cornerRadius =4.0f;
    //   self.img_bg_firstcell.layer.shadowOffset = CGSizeMake(0,0.1);
    //   self.img_bg_firstcell.layer.shadowRadius = 4.0;
    //   self.img_bg_firstcell.layer.shadowColor = [UIColor whiteColor].CGColor;
    //  self.img_bg_firstcell.layer.shadowOpacity = 0.4;
    
    self.img_bg_scondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_scondcell.layer.cornerRadius =4.0f;
    //    self.img_bg_scondcell.layer.shadowOffset = CGSizeMake(0,0.1);
    //    self.img_bg_scondcell.layer.shadowRadius = 4.0;
    //    self.img_bg_scondcell.layer.shadowColor = [UIColor whiteColor].CGColor;
    //   self.img_bg_scondcell.layer.shadowOpacity = 0.4;
    
    
    self.img_photo.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_photo.layer.cornerRadius =4.0f;
    self.img_photo.layer.masksToBounds = YES;
    
    self.view_img_photo.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.view_img_photo.layer.cornerRadius =4.0f;
    //  self.view_img_photo.layer.shadowOffset = CGSizeMake(0,0.1);
    //  self.view_img_photo.layer.shadowRadius = 4.0;
    // self.view_img_photo.layer.shadowColor = [UIColor whiteColor].CGColor;
    // self.view_img_photo.layer.shadowOpacity = 0.4;
    
    
    
    ///Header Table
    self.img_bg_firstcell_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell_tbl.layer.cornerRadius =4.0f;
    //   self.img_bg_firstcell_tbl.layer.shadowOffset = CGSizeMake(0,0.1);
    //   self.img_bg_firstcell_tbl.layer.shadowRadius = 4.0;
    //   self.img_bg_firstcell_tbl.layer.shadowColor = [UIColor whiteColor].CGColor;
    //   self.img_bg_firstcell_tbl.layer.shadowOpacity = 0.4;
    
    self.img_bg_scondcell_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_scondcell_tbl.layer.cornerRadius =4.0f;
    //   self.img_bg_scondcell_tbl.layer.shadowOffset = CGSizeMake(0,0.1);
    //   self.img_bg_scondcell_tbl.layer.shadowRadius = 4.0;
    //  self.img_bg_scondcell_tbl.layer.shadowColor = [UIColor whiteColor].CGColor;
    //  self.img_bg_scondcell_tbl.layer.shadowOpacity = 0.4;
    
    
    self.img_photo_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_photo_tbl.layer.cornerRadius =4.0f;
    self.img_photo_tbl.layer.masksToBounds = YES;
    
    self.view_img_photo_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.view_img_photo_tbl.layer.cornerRadius =4.0f;
    //  self.view_img_photo_tbl.layer.shadowOffset = CGSizeMake(0,0.1);
    //  self.view_img_photo_tbl.layer.shadowRadius = 4.0;
    //  self.view_img_photo_tbl.layer.shadowColor = [UIColor whiteColor].CGColor;
    //  self.view_img_photo_tbl.layer.shadowOpacity = 0.4;
    
    self.view_header_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.view_footer_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    self.img_photo.placeholderImage =[UIImage imageNamed:@"default_user_image.jpg"];
    self.img_photo_tbl.placeholderImage =[UIImage imageNamed:@"default_user_image.jpg"];
    
    //Header Name and bio
    self.lbl_name = [[LORichTextLabel alloc] initWithWidth:738];
    [self.lbl_name setFont:[UIFont systemFontOfSize:30.0f]];
    [self.lbl_name setBackgroundColor:[UIColor clearColor]];
    [self.lbl_name setTextColor:[UIColor whiteColor]];
    [self.lbl_name positionAtX:30.0 andY:180.0f];
    [self.view_header addSubview:self.lbl_name];
    
    self.lbl_name_tbl = [[LORichTextLabel alloc] initWithWidth:738];
    [self.lbl_name_tbl setFont:[UIFont systemFontOfSize:30.0f]];
    [self.lbl_name_tbl setBackgroundColor:[UIColor clearColor]];
    [self.lbl_name_tbl setTextColor:[UIColor whiteColor]];
    [self.lbl_name_tbl positionAtX:30.0 andY:180.0f];
    [self.view_header_tbl addSubview:self.lbl_name_tbl];
    self.tbl_news.tableHeaderView =self.view_header_tbl;
    
    self.tbl_news.tableFooterView = self.view_footer_tbl;
    
    //News feed
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"600" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"600" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-600" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-600" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-601" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-602" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-603" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-604" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"605" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_Responce:) name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-605" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-605" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDecsNotification:) name:IFTweetLabelURLNotification object:nil];
    
    image_upload_flag=0;
    
    
    self.username_style = [LORichTextLabelStyle styleWithFont:[UIFont boldSystemFontOfSize:34.0f] color:[UIColor colorWithRed:219.0f/255.0f green:228.0f/255.0f blue:235.0f/255.0f alpha:1]];
    
    LORichTextLabelStyle  *url_style = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:28.0f] color:[UIColor colorWithRed:129.0f/255.0f green:137.0f/255.0f blue:145.0f/255.0f alpha:1]];
    
    [url_style addTarget:self action:@selector(urlSelected:)];
    
    [self.lbl_name addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"www."];
    [self.lbl_name addStyle:url_style forPrefix:@"Http://"];
    
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Http://"];
    
    //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlelikeButtonNotification:) name:PMsequenceButtonURLNotification object:nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self.collectionViewObj setContentOffset:CGPointZero];
    [self.tbl_news setContentOffset:CGPointZero];
    
    [self start_activity];
    [self get_user_info];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    [self.lbl_name setText:@""];
    self.lbl_total_followers.text =@"0";
    self.lbl_total_followings.text = @"0";
    self.lbl_total_photos.text = @"0";
    self.img_photo.image = [UIImage imageNamed:@"default_user_image.jpg"];
    
    [self.lbl_name_tbl setText:@""];
    self.lbl_total_followers_tbl.text =@"0";
    self.lbl_total_followings_tbl.text = @"0";
    self.lbl_total_photos_tbl.text = @"0";
    self.img_photo_tbl.image = [UIImage imageNamed:@"default_user_image.jpg"];
    
    self.btn_follow.hidden =YES;
    self.btn_follow_tbl.hidden =YES;
    
    
    //default_user_image
    
    self.img_bg_firstcell.frame = CGRectMake(10,10,748, 270);
    self.view_secondcell.frame = CGRectMake(0,331,768,120);
    self.view_header.frame = CGRectMake(0,0,768,450);
    
    self.img_bg_firstcell_tbl.frame = CGRectMake(10,10,748, 270);
    self.view_secondcell_tbl.frame = CGRectMake(0,331,768,120);
    self.view_header_tbl.frame = CGRectMake(0,0,768,450);
    [self.tbl_news setTableHeaderView:self.view_header_tbl];
    
    
}
#pragma mark - Photos count click
-(IBAction)btn_photos_count_click:(id)sender{
    
    [self.collectionViewObj setContentOffset:CGPointMake(0, self.view_header.frame.size.height-68)];
    [self.tbl_news setContentOffset:CGPointMake(0, self.view_header.frame.size.height-68)];
}
#pragma mark - Followes count click

-(IBAction)btn_followers_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"FOLLOWERS";
    
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}
#pragma mark - Following count click

-(IBAction)btn_following_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"FOLLOWING";
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}
#pragma mark - Edit your profile click

-(IBAction)btn_edit_your_profile_click:(id)sender{
    
    
   // self.edit_your_profile_viewObj.hidesBottomBarWhenPushed = YES;
    self.edit_your_profile_viewObj.is_reload_data=1;
    
    [self.navigationController pushViewController:self.edit_your_profile_viewObj animated:YES];
    
}
#pragma mark - Setting Button click
-(IBAction)btn_setting_click:(id)sender{
    [self.navigationController pushViewController:self.setting_profileViewObj animated:YES];
}

#pragma mark - Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
}

#pragma mark - Get user Profile Info
-(void)get_user_info{
    
    //  http://www.techintegrity.in/mystyle/get_user_profile.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&profile_handle=1&login=3
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login=%@&flag=1",salt,sig,self.user_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_profile.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"605":nil];
    
    
}
-(void)get_user_info_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if([[result valueForKey:@"success"] isEqualToString:@"1"]){
        
        NSDictionary *dict = [result valueForKey:@"data"];
        
        
        self.profile_share_obj.user_id = [dict valueForKey:@"user_id"];
        self.profile_share_obj.username = [dict valueForKey:@"username"];
        self.profile_share_obj.name = [StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_share_obj.email = [StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_share_obj.facebook_id = [dict valueForKey:@"facebook_id"];
        self.profile_share_obj.total_followers = [dict valueForKey:@"total_followers"];
        self.profile_share_obj.total_following = [dict valueForKey:@"total_following"];
        self.profile_share_obj.total_photos =[dict valueForKey:@"total_images"];
        self.profile_share_obj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_share_obj.bio=[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        self.profile_share_obj.web_url=[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        self.profile_share_obj.is_user_follow=[StaticClass urlDecode:[dict valueForKey:@"user_following"]];
        
        
        self.img_photo.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
        self.img_photo_tbl.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
        
        if (self.profile_share_obj.total_followers.length==0) {
            self.lbl_total_followers.text=@"0";
            self.lbl_total_followers_tbl.text=@"0";
        }else{
            self.lbl_total_followers.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_tbl.text=self.profile_share_obj.total_followers;
        }
        
        if (self.profile_share_obj.total_following.length==0) {
            self.lbl_total_followings.text=@"0";
            self.lbl_total_followings_tbl.text=@"0";
        }else{
            self.lbl_total_followings.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_tbl.text=self.profile_share_obj.total_following;
        }
        if (self.profile_share_obj.total_photos.length==0) {
            self.lbl_total_photos.text=@"0";
            self.lbl_total_photos_tbl.text=@"0";
        }else{
            self.lbl_total_photos.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_tbl.text=self.profile_share_obj.total_photos;
        }
        
        
        NSMutableString *str =[[NSMutableString alloc]init];
        
        if (self.profile_share_obj.name.length==0) {
            
            [self.lbl_name setText:@""];
            [self.lbl_name setText:@""];
        }else{
            
            // [str appendString:self.profile_share_obj.name];
            [str appendFormat:@"%@ \n",self.profile_share_obj.name];
            for (NSString *name in [self.profile_share_obj.name componentsSeparatedByString:@" "]) {
                
                if (name.length>0) {
//                    [self.lbl_name addStyle:self.username_style forPrefix:name];
//                    [self.lbl_name_tbl addStyle:self.username_style forPrefix:name];
                }
                
            }
        }
        if (!self.profile_share_obj.bio.length==0) {
            [str appendFormat:@"%@ \n",self.profile_share_obj.bio];
        }
        if (!self.profile_share_obj.web_url.length==0) {
            [str appendFormat:@"%@",self.profile_share_obj.web_url];
        }
        
        if (![self.profile_share_obj.user_id isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]]) {
            
            
            if ([self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
                [self.btn_follow setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
            }else{
                [self.btn_follow setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
            }
            self.btn_follow_tbl.hidden = NO;
            self.btn_follow.hidden = NO;
            self.btn_edit_tbl.hidden = YES;
            self.btn_edit.hidden = YES;
        }else{
            
            self.btn_follow_tbl.hidden = YES;
            self.btn_follow.hidden = YES;
            self.btn_edit_tbl.hidden = NO;
            self.btn_edit.hidden = NO;
        }
        NSString *tem=[NSString stringWithFormat:@"%@",str];
        [self.lbl_name setText:tem];
        [self.lbl_name_tbl setText:tem];
        CGSize size = [[NSString stringWithFormat:@"%@",tem] sizeWithFont:[UIFont boldSystemFontOfSize:30.0f]
                                                        constrainedToSize:CGSizeMake(708.0f, HUGE_VALL)
                                                            lineBreakMode:NSLineBreakByWordWrapping];
        if (self.lbl_name.height>44) {
            
            
            
            CGRect frame =  self.img_bg_firstcell.frame;
            frame.size.height = 180.0f + self.lbl_name.height ;
            self.img_bg_firstcell.frame =frame;
            self.img_bg_firstcell_tbl.frame =frame;
            
            CGRect secondcell_frame =  self.view_secondcell.frame;
            secondcell_frame.origin.y = 180.0f+50 + self.lbl_name.height;
            self.view_secondcell.frame = secondcell_frame;
            self.view_secondcell_tbl.frame =secondcell_frame;
            
            CGRect frame_header =  self.view_header.frame;
            frame_header.size.height = 120.0f +50+10+180 + self.lbl_name.height;
            self.view_header.frame =frame_header;
            self.view_header_tbl.frame =frame_header;
            [self.tbl_news setTableHeaderView:self.view_header_tbl];
        }else{
            
            self.img_bg_firstcell.frame = CGRectMake(10,10,748, 260);
            self.view_secondcell.frame = CGRectMake(0,315,768,120);
            self.view_header.frame = CGRectMake(0,0,768,450);
            self.img_bg_firstcell_tbl.frame = CGRectMake(10,10,748, 260);
            self.view_secondcell_tbl.frame = CGRectMake(0,315,768,120);
            self.view_header_tbl.frame = CGRectMake(0,0,768,450);
            [self.tbl_news setTableHeaderView:self.view_header_tbl];
            
        }
        
        
        
        
        [self.collectionViewObj reloadData];
        [self.tbl_news reloadData];
        
        
        [self get_news_feed];
    }else{
        
//        for (Home_tableview_data_share *obj in self.array_feeds) {
//            [obj release];
//            obj=nil;
//        }
        [self.array_feeds removeAllObjects];
        [self.collectionViewObj reloadData];
        [self.tbl_news reloadData];
    }
    //  NSLog(@"%@",jsonStr);
    
}
#pragma mark - Get News Feed
-(void)get_news_feed{
    
    //  http://www.techintegrity.in/mystyle/get_user_images.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&profile_handle=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
   // NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@",salt,sig,self.profile_share_obj.user_id];
     NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login_id=%@",salt,sig,self.profile_share_obj.user_id,[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"600":nil];
    
}


-(void)getnewsResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    // NSLog(@"%@",result);
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
//        for (Home_tableview_data_share *obj in self.array_feeds) {
//            [obj release];
//            obj=nil;
//        }
        [self.array_feeds removeAllObjects];
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled    hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        
//        for (Home_tableview_data_share *obj in self.array_feeds) {
//            [obj release];
//            obj=nil;
//        }
        [self.array_feeds removeAllObjects];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict in array) {
            
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[dict valueForKey:@"username"];
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[tempdict valueForKey:@"username"]];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[tempdict valueForKey:@"username"];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    
                    [shareObj.array_comments addObject:obj];
                    [obj release];
                    
                    
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            [self.array_feeds addObject:shareObj];
            [shareObj release];
        }
        
    }
    
    [self.tbl_news reloadData];
    [self.collectionViewObj reloadData];
    [self stop_activity];
    
    
}
-(void)FailNewsReson:(NSNotification *)notification {
	//[self stopSpinner];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    [self stop_activity];
    /*
     UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"There was a problem with the Internet connection. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
     [alert show];
     [alert release];
     */
    
}



#pragma mark - Collectionview & Tableview  choose for show
-(IBAction)btn_collectionview_click:(id)sender{
    
    [self.collectionViewObj setContentOffset:self.tbl_news.contentOffset];
    
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden =NO;
    
    
}
-(IBAction)btn_tableview_click:(id)sender{
    
    [self.tbl_news setContentOffset:self.collectionViewObj.contentOffset];
    
    self.tbl_news.hidden = NO;
    self.collectionViewObj.hidden =YES;
    
}

#pragma mark - UICollectionView


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.view_header.frame;
        
        
        [headerView addSubview:self.view_header];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(0, self.view_header.frame.size.height);
        
        reusableview = headerView;
        NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
        return reusableview;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        
        image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
        
        //reusableview = footerview;
        
        /*  image_collection_footer *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
         */
        footerview.frame =self.view_footer.frame;
        
        
        [footerview addSubview:self.view_footer];
        
        /*   UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
         
         collectionViewLayout.footerReferenceSize = CGSizeMake(0, self.view_footer.frame.size.height);
         */
        reusableview = footerview;
        
        return reusableview;
    }
    
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return self.array_feeds.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{            return CGSizeMake(180,180);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.row];
 //   cell.img_photo.imageURL =[NSURL URLWithString:shareObj.image_path];
    [cell.img_photo setImageWithContentsOfURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    cell.tag = indexPath.row;
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.image_detail_viewObj.shareObj =[self.array_feeds objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_feeds;
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

#pragma mark UITableview Delegate


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.array_feeds.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 120.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:section];
    
    NPRImageView *img_user =[[[NPRImageView alloc]initWithFrame:CGRectMake(10, 30, 80, 80)]autorelease];
    [img_user setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    
    EGOImageButton *img_userimage=[[[EGOImageButton alloc]initWithFrame:CGRectMake(10,30, 80,80)] autorelease];
  //  img_userimage.imageURL =[NSURL URLWithString:shareObj.user_image];
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:shareObj.username forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(100,45, 550, 50)];
    [btn_username setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"locationicon@2x.png"]];
    img_locationicon.frame = CGRectMake(100,73, 32, 40);
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:shareObj.location forState:UIControlStateNormal];
    
    
    if (shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(100,25, 550, 50)];
        [btn_location setFrame:CGRectMake(140,70,500, 50)];
        img_locationicon.hidden=NO;
        
    }else{
        img_locationicon.hidden=YES;
    }
    
    [btn_location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_location.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //   [btn_location addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon@2x.png"]];
    
    [img_clock setFrame:CGRectMake(650, 64, 32,32)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:shareObj.datecreated];
    lbl_time.font = [UIFont boldSystemFontOfSize:30];
    lbl_time.textColor =[UIColor whiteColor];
    lbl_time.frame =CGRectMake(688, 64,80,32);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,768, 120)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =0.9;
    [view addSubview:img_user];
    [view addSubview:lbl_time];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    return view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    return [Home_tableview_cell_iPad get_tableview_hight:shareObj];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Home_tableview_cell_iPad";
	Home_tableview_cell_iPad *cell = (Home_tableview_cell_iPad *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell_iPad" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        [cell setBackgroundColor:[UIColor clearColor]];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
        
        [cell.img_big addGestureRecognizer:tapImage];
        
        //
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
	}
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:28.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    
    [cell redraw_cell:shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    cell.dlstarObj.delegate = self;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
#pragma mark Method for Stop Header scrolling in UITableview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            [alert release];
            
        }];
        
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"My Style\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"I don't like this photo",@"This photo is spam or a scam",@"This photo puts people at risk",@"This photo shouldn't be on mystyle", nil];
        alert.tag =2;
        [alert show];
        [alert release];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    /*  [sheet addButtonWithTitle:@"Show Action Sheet on top" block:^{
     NSLog(@"");
     }];
     [sheet addButtonWithTitle:@"Show another alert" block:^{
     NSLog(@"");
     }];
     */
    [sheet showInView:self.view];
}
-(void)report_inappropriate:(int )index{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    //post_report_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&report_id=2&item_id=12&uid=1
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"603":nil];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    
    int tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}


- (void)handleDecsNotification:(NSNotification *)notification
{
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

-(void)handlelikeButtonNotification:(NSNotification *)notification{
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:[notification object] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}



#pragma mark Go to Profile

-(IBAction)btn_profile_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    NSLog(@"%d",tag);
    /*
     Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
     user_info_view.user_id=obj.uid;
     [self.navigationController pushViewController:user_info_view animated:YES];
     */
}

#pragma mark UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:buttonIndex];
        
    }
}

#pragma mark Delete Photo
-(void)delete_photo{
    //     http://www.techintegrity.in/mystyle/post_delete_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"602":nil];
    
}

-(void)getdeleteResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [self get_news_feed];
    }
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    //  http://www.techintegrity.in/mystyle/post_rating.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&rate=2&item_id=12&uid=1
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%d&item_id=%@&uid=%@",salt,sig,rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"603":nil];
    
    obj.my_rating = [NSString stringWithFormat:@"%d",rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    // [dlstarObj  setRating:3];
}
-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
    }
    
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    int tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    
    
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"601":nil];
    
}

-(void)getlikeResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
        }else{
            
            shareObj.liked=@"no";
            int count =[shareObj.likes intValue];
            count--;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            
            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            }
        }
        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
}

#pragma mark
#pragma mark Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked=tappedRow.section;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
    
    if ([shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
    
    
    
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    // NSLog(@"%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"604":nil];
    
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        // NSLog(@"%@",shareObj.image_id);
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            // NSLog(@"%@",shareObj.array_liked_by);
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
            
        }
        
    }
    
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
   // self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    //  [self.navigationController pushViewController:self.comments_list_viewObj animated:YES];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    //    if (result == MFMailComposeResultSent) {
    //
    //    }
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    // NSLog(@"%@",[[Singleton sharedSingleton]get_current_time]);
    
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    //  NSLog(@"%f",diff);
    [dateFormatter release];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}



#pragma mark - Show Mapview
-(IBAction)btn_mapview_click:(id)sender{
    
    //   self.mapviewObj.array_data =self.array_feeds;
    //  [self.navigationController pushViewController:self.mapviewObj animated:YES];
    
    self.mapviewObj.array_data =self.array_feeds;
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.mapviewObj];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - Follow and Following
-(IBAction)btn_follow_click:(id)sender{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    if (![self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
        [self.btn_follow setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"followbtn@2x.png"] forState:UIControlStateNormal];
        url=@"post_user_unfollow.php";
    }else{
        [self.btn_follow setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"followingbtn@2x.png"] forState:UIControlStateNormal];
        url=@"post_user_following.php";
    }
    NSLog(@"%@%@",[[Singleton sharedSingleton] getBaseURL],url);
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingAPIResponce:) name:@"14242" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingAPIResponce:) name:@"-14242" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            self.profile_share_obj.user_id,@"following_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14242" :params];
}

-(void)postUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        //akshay
     //   if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
            NSDictionary *dataDict=[result objectForKey:@"data"];
            if ([dataDict count]>0) {
                NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
                NSMutableArray *tempArray=[[NSMutableArray alloc] init];
                for (NSDictionary *key in tempFollwingDict) {
                    NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                    [tempArray addObject:str];
                }
                [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
            }
      //  }
        
        [self get_user_info];
    }
}

-(void)FailpostUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - Start-Stop Activityview
-(void)start_activity{
    
    [self.img_spinner_tbl startAnimating];
    [self.img_spinner startAnimating];
    self.img_spinner.tag=1;
    
    if (self.array_feeds.count>0) {
        lbl_no_photo.hidden=YES;
        lbl_no_photo_tbl.hidden=YES;
    }else{
        lbl_no_photo.hidden=YES;
        lbl_no_photo_tbl.hidden=YES;
    }
    
}

-(void)stop_activity{
    [self.img_spinner_tbl stopAnimating];
    [self.img_spinner stopAnimating];
    self.img_spinner.tag=0;
    
    
    if (self.array_feeds.count>0) {
        lbl_no_photo.hidden=YES;
        lbl_no_photo_tbl.hidden=YES;
        [self.img_spinner setImage:[UIImage imageNamed:@"feedstate-refresh-active.png"]];
        [self.img_spinner_tbl setImage:[UIImage imageNamed:@"feedstate-refresh-active.png"]];
        
        
    }else{
        lbl_no_photo.hidden=NO;
        lbl_no_photo_tbl.hidden=NO;
        [self.img_spinner setImage:[UIImage imageNamed:@"photobtn_footer@2x.png"]];
        [self.img_spinner_tbl setImage:[UIImage imageNamed:@"photobtn_footer@2x.png"]];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [btn_edit release];
    [btn_edit_tbl release];
    
    [username_style release];
    
    [btn_follow release];
    [btn_follow_tbl release];
    [user_id release];
    [img_down_arrow_tbl release];
    [img_down_arrow release];
    [view_secondcell release];
    [view_secondcell_tbl release];
    
    [setting_profileViewObj release];
    [photoPickerController release];
    
    [edit_your_profile_viewObj release];
    [lbl_name release];
    [lbl_name_tbl release];
    
    [lbl_total_followers release];
    [lbl_total_followings release];
    [lbl_total_photos release];
    
    [lbl_total_followers_tbl release];
    [lbl_total_followings_tbl release];
    [lbl_total_photos_tbl release];
    
    [profile_share_obj release];
    [followers_list_viewObj release];
    [image_detail_viewObj release];
    [liker_list_viewObj release];
    [comments_list_viewObj release];
    [share_photo_view release];
    
    [img_like_heart release];
    [array_feeds release];
    [view_footer_tbl release];
    [img_spinner_tbl release];
    [img_spinner release];
    [view_footer release];
    [mapviewObj release];
    [tbl_news release];
    
    [view_img_photo release];
    [img_bg_scondcell release];
    [img_bg_firstcell release];
    [img_photo release];
    [view_header release];
    
    [view_img_photo_tbl release];
    [img_bg_scondcell_tbl release];
    [img_bg_firstcell_tbl release];
    [img_photo_tbl release];
    [view_header_tbl release];
    
    [collectionViewObj release];
    [super dealloc];
}
@end
