//
//  Share_photo_ViewController_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Share_photo_ViewController_iPad.h"

@interface Share_photo_ViewController_iPad ()

@end

@implementation Share_photo_ViewController_iPad

@synthesize img_photo,scrollview,img_bg_firstcell;
@synthesize img_show_photo,view_bg_show_photo,txt_desc,img_bg_secondcell;


@synthesize btn_location,lbl_optional,view_second_cell;
@synthesize view_third_cell,img_bg_third_cell;
@synthesize show_location_listViewObj,switch_map;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.s3 = [[[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY] autorelease];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.show_location_listViewObj=[[Show_location_list_iPad alloc]initWithNibName:@"Show_location_list_iPad" bundle:nil];
    
    self.img_bg_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell.layer.cornerRadius =4.0f;
    
    
    self.img_bg_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_secondcell.layer.cornerRadius =4.0f;
    
    
    self.img_bg_third_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    
    self.img_bg_third_cell.layer.cornerRadius =4.0f;
    
    
    self.view_bg_show_photo.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    //  self.view_bg_show_photo.layer.shadowOffset = CGSizeMake(0,0.1);
    //  self.view_bg_show_photo.layer.shadowColor = [UIColor whiteColor].CGColor;
    //   self.view_bg_show_photo.layer.shadowOpacity = 0.4;
    
    
    
    [switch_map setOn:YES];
    switch_map.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    self.img_show_photo.image=self.img_photo;
    

    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }else{
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
}

-(IBAction)btn_location_list_click:(id)sender{
    
    [self.navigationController pushViewController:self.show_location_listViewObj animated:YES];
}
#pragma mark - ToggleViewDelegate

-(IBAction)btn_switch_map_click:(id)sender{
    
    if (!switch_map.isOn) {
        // [self selectLeftButton];
        [self performSelector:@selector(selectLeftButton) withObject:self afterDelay:0.1];
    }else{
        //  [self selectRightButton];
        [self performSelector:@selector(selectRightButton) withObject:self afterDelay:0.1];
    }
}
- (void)selectLeftButton
{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(44,303,680,104);
    self.view_second_cell.frame = CGRectMake(44,409,680,0);
    self.view_third_cell.frame =CGRectMake(0, 535-102, 768, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=YES;
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
}

- (void)selectRightButton
{
    self.view_second_cell.layer.masksToBounds = YES;
    
    self.view_second_cell.frame = CGRectMake(44,409,680,0);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(44,303,680,208);
    self.view_second_cell.frame = CGRectMake(44,409,680,102);
    self.view_third_cell.frame =CGRectMake(0, 535, 768, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=NO;
    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }else{
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
    
    
}




#pragma mark UIScrollview Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [self.txt_desc resignFirstResponder];
}

#pragma mark UITextview Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return TRUE;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
        self.txt_desc.text=@"";
    }
    
    return TRUE;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.txt_desc.text.length==0) {
        self.txt_desc.text=@"Write a caption...";
    }
}

-(IBAction)btn_share_click:(id)sender{
    
    [self postImage];
}
#pragma mark Post Image To server
-(void)postImage{
    //      http://www.techintegrity.in/mystyle/post_images.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&lat=12.2222&lng=12.454545&location=kkvhall&uid=2&description=test&image=sarju.png
    
    NSData *img_data  = UIImageJPEGRepresentation(self.img_photo,1);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        NSDate *dt=[NSDate date];
       // double timePassed_ms = [dt timeIntervalSinceNow];
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSString *dateString1 = [DateFormatter stringFromDate:dt];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        __block NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]] autorelease];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else
            {
                
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = @"coco";
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postImagesAPIResponce:) name:@"14234" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostImagesAPIResponce:) name:@"-14234" object:nil];
                
                NSString *requestStr =[NSString stringWithFormat:@"%@post_images.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSString *strDesc=@"";
                if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
                    //[request setPostValue:@"" forKey:@"description"];
                    strDesc=@"";
                    
                }else{
                    //[request setPostValue:self.txt_desc.text forKey:@"description"];
                    strDesc=self.txt_desc.text;
                }
                
                NSString *strLocation=@"";
                NSString *strLatitude=@"";
                NSString *strLongitude=@"";
                
                if (!self.view_second_cell.hidden) {
                    if ([[Singleton sharedSingleton]get_location_name].length !=0) {
                        NSLog(@"Share");
                        strLocation=[[Singleton sharedSingleton]get_location_name];
                        strLatitude=[[Singleton sharedSingleton]get_location_lat];
                        strLongitude=[[Singleton sharedSingleton]get_location_lng];
                    }
                }
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        strDesc,@"description",
                                        imageKeys,@"image",
                                        strLocation,@"location",
                                        strLatitude,@"lat",
                                        strLongitude,@"lng",
                                        nil];
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14234" :params];
            }
        });
    });
}

-(void)postImagesAPIResponce :(NSNotification *)notification {
   // [self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        NSLog(@"Success");
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeBlue title:@"Photo upload successfully." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        
        
        [self dismissViewControllerAnimated:NO completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view_from_share" object:self];
        }];
    }
    
}

-(void)FailpostImagesAPIResponce :(NSNotification *)notification {
    //[self stopSpinner];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

#pragma mark - Facebook Sharing
-(IBAction)btn_facebook_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark - Twitter Sharing
-(IBAction)btn_twitter_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark - Messaage Sharing
-(IBAction)btn_message_share_click:(id)sender{
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = self.img_show_photo.image;
    
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    
    if([MFMessageComposeViewController canSendText]) {
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Your Message Body"];
        picker.messageComposeDelegate = self;
        //  picker.recipients = [NSArray arrayWithObject:@"123456789"];
        [picker setBody:emailBody];// your recipient number or self for testing
        picker.body = emailBody;
        NSLog(@"Picker -- %@",picker.body);
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    /*    MessageComposeResultCancelled,
     MessageComposeResultSent,
     MessageComposeResultFailed*/
    switch (result) {
        case MessageComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MessageComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent failed!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MessageComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
    }
}
#pragma mark - Email Sharing

-(IBAction)btn_email_share_click:(id)sender{
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        NSData *photoData=UIImagePNGRepresentation(self.img_show_photo.image);
        [controller addAttachmentData:photoData mimeType:@"image/png" fileName:[NSString stringWithFormat:@"photo.png"]];
        controller.mailComposeDelegate = self;
        if (controller)
            [self presentViewController:controller animated:YES completion:nil];
        [controller release];
    }else {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc{
    [switch_map release];
    [show_location_listViewObj release];
    [view_third_cell release];
    [img_bg_third_cell release];
    
    [btn_location release];
    [lbl_optional release];
    [view_second_cell release];
    
    [img_bg_secondcell release];
    [txt_desc release];
    [view_bg_show_photo release];
    [img_show_photo release];
    [img_bg_firstcell release];
    [scrollview release];
    [img_photo release];
    [super dealloc];
}

@end
