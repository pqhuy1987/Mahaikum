//
//  Reset_password_from_username_email_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/4/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "Reset_password_throw_email_iPad.h"

@interface Reset_password_from_username_email_iPad : UIViewController
{
    
    UIImageView *img_cell_bg;
    UITextField *txt_username;
}

@property(nonatomic,retain)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,retain)IBOutlet UITextField *txt_username;
@property(nonatomic,retain)Reset_password_throw_email_iPad *thro_email_viewObj;

@end
