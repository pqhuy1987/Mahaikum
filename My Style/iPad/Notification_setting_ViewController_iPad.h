//
//  Notification_setting_ViewController_iPad.h
//  My Style
//
//  Created by Tis Macmini on 7/31/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification_setting_cell_iPad.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "Notification_setting_share.h"
#import "Notification_setting_on_off_cell_iPad.h"
#import "DCRoundSwitch.h"

@interface Notification_setting_ViewController_iPad : UIViewController{

    UITableView *tbl_notification;
    NSMutableArray *array_notification;
    
    UIView *tbl_footer;
    
    NSMutableDictionary *dict_notification;
}
@property(nonatomic,retain)IBOutlet  UITableView *tbl_notification;
@property(nonatomic,retain) NSMutableArray *array_notification;
@property(nonatomic,retain)IBOutlet UIView *tbl_footer;

@property(nonatomic,retain)  NSMutableDictionary *dict_notification;

@end
