//
//  News_feed_following_cell_iPad.m
//  My Style
//
//  Created by Tis Macmini on 8/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "News_feed_following_cell_iPad.h"



@implementation News_feed_following_cell_iPad

@synthesize btn_user_img,lbl_time,lbl_dec,btn_user_img1;
@synthesize imgRateImage;

-(void)draw_in_cell{
    self.lbl_dec = [[LORichTextLabel alloc] initWithWidth:650];
	[self.lbl_dec setFont:[UIFont fontWithName:@"Helvetica" size:32.0]];
	//[self.lbl_dec setTextColor:[UIColor colorWithRed:30.0f/255.0f green:48.0f/255.0f blue:62.0f/255.0f alpha:1]];
    [self.lbl_dec setTextColor:[UIColor whiteColor]];
	[self.lbl_dec setBackgroundColor:[UIColor clearColor]];
	[self.lbl_dec positionAtX:110.0 andY:10.0];
    [self.contentView addSubview:self.lbl_dec];
}
-(void)dealloc{
    [imgRateImage release];
    [btn_user_img1 release];
    [lbl_time release];
    [btn_user_img release];
    [super dealloc];
}
@end
