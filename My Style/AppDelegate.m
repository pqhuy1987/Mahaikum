//
//  AppDelegate.m
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "OpenInGoogleMapsController.h"
#import "Chat_ViewController.h"

#define REVMOB_ID @"51e533a6c4144155eb00004b"
#define CHARTBOOST_appId  @"540ee0a9c26ee4431bfe1c51"
#define CHARTBOOST_appSignature  @"4195df2a75e02b60ebe4099f48ebc3d2c91db0fe"

@implementation AppDelegate
{
    id _services;
}
@synthesize session = _session;
@synthesize locationManager;
@synthesize navObj;
@synthesize myDefine;
@synthesize transition;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    _services = [GMSServices sharedServices];
    
    [OpenInGoogleMapsController sharedInstance].fallbackStrategy = kGoogleMapsFallbackChromeThenAppleMaps;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
#ifdef DEBUG
    [AmazonLogger verboseLogging];
#else
    [AmazonLogger turnLoggingOff];
#endif
    [AmazonErrorHandler shouldNotThrowExceptions];
    
    //Enabling keyboard manager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
	//Enabling autoToolbar behaviour. If It is set to NO. You have to manually create UIToolbar for keyboard.
	[[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
	[[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    self.myDefine = [[GlobalDefine alloc] init];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
//    NSString *pathsToReources = [[NSBundle mainBundle] resourcePath];
//    NSString *yourOriginalDatabasePath = [pathsToReources stringByAppendingPathComponent:@"MyStyle.sqlite"];
//    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [pathsToDocuments objectAtIndex: 0];
//    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"MyStyle.sqlite"];
//    
//    if (![[NSFileManager defaultManager] isReadableFileAtPath: dbPath]) {
//        if ([[NSFileManager defaultManager] copyItemAtPath: yourOriginalDatabasePath toPath: dbPath error: NULL] != YES)
//        {
//            NSAssert2(0, @"Fail to copy database from %@ to %@", yourOriginalDatabasePath, dbPath);
//        }
//        else{
//            if (SYSTEM_VERSION_LESS_THAN(@"5.0.1")) {
//                NSLog(@"5.0.1");
//                [self addSkipBackupAttributeToItemAtURL_5:[NSURL fileURLWithPath:dbPath]];
//            }
//            
//            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.1")) {
//                NSLog(@"5.1.0");
//                [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:dbPath]];
//            }
//        }
//    }
//    
//    [[Singleton sharedSingleton] setStrdbpath:dbPath];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logout:) name:@"logout" object:nil];

    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [self.locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    if (!self.session.isOpen) {
        self.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (self.session.state == FBSessionStateCreatedTokenLoaded) {
            
            [self.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    //Initialize transition
    // customization
    transition = [[ElasticTransition alloc] init];
    
    transition.sticky           = YES;
    transition.showShadow       = YES;
    transition.panThreshold     = 0.55;
    transition.radiusFactor     = 0.3;
    transition.transformType    = ROTATE;


    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    if (isiPhone) {
        //viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
        
        viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        
        navObj = [[UINavigationController alloc] initWithRootViewController:viewController];
    }
    else{
        //viewController_iPad = [[[ViewController_iPad alloc] initWithNibName:@"ViewController_iPad" bundle:nil] autorelease];
       // viewController_iPad = [[ViewController_iPad alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
       // navObj = [[UINavigationController alloc] initWithRootViewController:viewController_iPad];
        
        
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
    
    // Configure Chartboost
//    cb = [Chartboost sharedChartboost];
//    cb.appId = CHARTBOOST_appId;
//    cb.appSignature = CHARTBOOST_appSignature;
//    cb.delegate = self;
//    [cb startSession];
//    [cb cacheInterstitial];
//    [cb cacheMoreApps];
    
    [Chartboost startWithAppId:CHARTBOOST_appId appSignature:CHARTBOOST_appSignature delegate:self];
    [Chartboost cacheMoreApps:CBLocationHomeScreen];
    [Chartboost cacheInterstitial:CBLocationHomeScreen];
    
    [self performRegisterNotification:application];
    
    //--- your custom code
    
    self.window.rootViewController = navObj;
    [self.window makeKeyAndVisible];
    
    self.device_token=@"";
    self.badge=@"0";
    
    self.push_type = self.fromUser = self.fromUserID = self.fromUserImage = @"";
    
    self.alertView = nil;
    
    return YES;
}

- (void)performRegisterNotification:(UIApplication *)application
{
    //Registering user notification - (Handles user and remote notifications)
//    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
//    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
//    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
//    
//    [application registerForRemoteNotifications];
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
    
    application.applicationIconBadgeNumber = 0;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *tempDeviceToken=[NSString stringWithFormat:@"%@",[deviceToken description]];
    tempDeviceToken=[tempDeviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    tempDeviceToken=[tempDeviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    tempDeviceToken=[tempDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [StaticClass saveToUserDefaults:tempDeviceToken:@"GETDEVICETOKEN"];
    
    self.device_token = tempDeviceToken;
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken:%@", self.device_token);
    
    //[self updateDeviceToken];
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Device Token" message:tempDeviceToken delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alertView show];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Received push message!->%@", userInfo);

    self.push_type = [[userInfo objectForKey:@"aps"] objectForKey: @"type"];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[[userInfo objectForKey:@"aps"] objectForKey: @"badge"] intValue];
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        NSLog(@"What you want to do when your app was active and it got push notification");
        
        if ([self.push_type isEqualToString:@""])
            return;

        NSString *message = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];

        NSString *cancelTitle = @"Close";
        NSString *showTitle = @"Show";
        NSString *title = @"";
        
        if ([self.push_type isEqualToString:@"all push"])
        {
            title = @"A message arrived from the Mahalkum administrator.";

            self.alertView = [[UIAlertView alloc] initWithTitle:@"Mahalkum"
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:cancelTitle 
                                                  otherButtonTitles:showTitle, nil];
        }
        else if ([self.push_type isEqualToString:@"chat"])
        {
            //NSLog(@"-------Receive chat:[%@]----------", message);
            if ( self.isChattingRoom )
            {
                //NSLog(@"didReceiveRemoteNotification->isChattingRoom.");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMessages" object:nil];
            }
            else
            {
                //NSLog(@"didReceiveRemoteNotification->no isChattingRoom.");
                self.fromUserID = [[userInfo objectForKey:@"aps"] objectForKey: @"id"];
                self.fromUser = [[userInfo objectForKey:@"aps"] objectForKey: @"username"];
                self.fromUserImage =[StaticClass urlDecode:[[userInfo objectForKey:@"aps"] objectForKey: @"image"]];
                
                NSString *chat_type = [[userInfo objectForKey:@"aps"] objectForKey: @"chatType"];

                if ([chat_type isEqualToString:@"0"])
                    title = [NSString stringWithFormat: @"A message arrived from the '%@'", self.fromUser];
                else if ([chat_type isEqualToString:@"1"])
                    title = [NSString stringWithFormat: @"The image arrived from the '%@'", self.fromUser];
                
                self.alertView = [[UIAlertView alloc] initWithTitle:title
                                                                    message:message
                                                                   delegate:self
                                                          cancelButtonTitle:cancelTitle
                                                          otherButtonTitles:showTitle, nil];
            }
        }
        else if ([self.push_type isEqualToString:@"push_message"])
        {
            [self.tabbar.badgeView updateAlertStatus];

            title = [NSString stringWithFormat: @"Push notification message for new feeds."];
            
            self.alertView = [[UIAlertView alloc] initWithTitle:title
                                                   message:message
                                                  delegate:self
                                         cancelButtonTitle:cancelTitle
                                         otherButtonTitles:showTitle, nil];
            
        }
        
        if (self.alertView != nil)
        {
            [self.alertView show];
        }

        [self updateBadge];
    }
    else if (state == UIApplicationStateInactive)
    {
        NSLog(@"What you want to do when your app was in background and it got push notification");
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( [self.push_type isEqualToString:@"chat"] && buttonIndex == 1 )
    {
        //goto message room
        Chat_ViewController *controller=(Chat_ViewController *)[[Chat_ViewController alloc]initWithNibName:@"Chat_ViewController" bundle:nil];
        
        controller.toUserID = self.fromUserID;
        controller.toUserImage = self.fromUserImage;

        [self.window.rootViewController presentViewController:controller animated:YES completion:nil];
    }
    else if ( [self.push_type isEqualToString:@"push_message"] && buttonIndex == 1 )
    {
        [self.tabbar setSelectedIndex:3];
    }
}

-(void)showRevmobAndChartBoostAds {
}

-(void)revmobShow {
//    [RevMobAds startSessionWithAppID:REVMOB_ID];
//    [[RevMobAds session] showFullscreen];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self.session close];
}

- (void)updateBadge
{
    if ([self.device_token length] == 0)
        return;
    
    //[SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update_badge:) name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-62" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_badge.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);

    NSString *uid = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];

    if (!uid) uid = @"";
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"uid": uid, @"device_token": self.device_token, @"badge": self.badge};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"62" :params];
}

- (void)update_badge:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
        NSLog(@"Update badge success!");
    else
        NSLog(@"Can not update badge");
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
}

- (void)updateDeviceToken
{
    //[SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"63" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update_device_token:) name:@"63" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-63" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailUpdateDeviceTokenReson:) name:@"-63" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_device_token.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSString *uid = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"uid": uid, @"device_token": self.device_token};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"63" :params];
}

- (void)update_device_token:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"63" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-63" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
        NSLog(@"Update device token success!");
    else
        NSLog(@"Can not update device token");
}

-(void)FailUpdateDeviceTokenReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"63" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-63" object:nil];
}


-(void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"applicationDidBecomeActive!");
    
    [self updateBadge];
    
    if ( self.isChattingRoom )
    {
        //NSLog(@"applicationDidBecomeActive->isChattingRoom.");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMessages" object:nil];
    }
    
    [FBSDKAppEvents activateApp];
}

-(void)RepeateChatBoost {
//        cb.appId = CHARTBOOST_appId;
//        cb.appSignature = CHARTBOOST_appSignature;
//        cb.delegate = self;
//        [cb startSession];
//        [cb cacheInterstitial];
//        [cb showInterstitial];
    
    [Chartboost startWithAppId:CHARTBOOST_appId appSignature:CHARTBOOST_appSignature delegate:self];
    [Chartboost cacheMoreApps:CBLocationHomeScreen];
    [Chartboost cacheInterstitial:CBLocationHomeScreen];
    [Chartboost showInterstitial:CBLocationHomeScreen];
}

#pragma mark - RevMobAdsDelegate methods

-(void)revmobAdDidReceive {
    NSLog(@"[RevMob Sample App] Ad loaded.");
}

-(void)revmobAdDidFailWithError:(NSError *)error {
    NSLog(@"[RevMob Sample App] Ad failed: %@", error);
}

-(void)revmobAdDisplayed {
    NSLog(@"[RevMob Sample App] Ad displayed.");
}

-(void)revmobUserClosedTheAd {
    NSLog(@"[RevMob Sample App] User clicked in the close button.");
}

-(void)revmobUserClickedInTheAd {
    NSLog(@"[RevMob Sample App] User clicked in the Ad.");
}

-(void)installDidReceive {
    NSLog(@"[RevMob Sample App] Install did receive.");
}

-(void)installDidFail {
    NSLog(@"[RevMob Sample App] Install did fail.");
}

+(AppDelegate*) sharedDelegate {
	return (AppDelegate*) [[UIApplication sharedApplication] delegate];
}

-(void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    application.applicationIconBadgeNumber=0;
}

-(void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"REVMOBCHARTBOOSTADDNOTIFICATION" object:self];
    

    
}

/*
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
*/
/*
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
*/
/*
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [FBSession.activeSession handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [FBSession.activeSession handleOpenURL:url];
}
-(void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSession.activeSession handleDidBecomeActive];
}

-(void)applicationWillTerminate:(UIApplication *)application {
    [FBSession.activeSession close];
}
 */

-(void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                
                [StaticClass saveToUserDefaults:session.accessToken :FACEBOOK_ACCESS_TOKEN];
                
                NSString *go_status=[[NSUserDefaults standardUserDefaults]objectForKey:FB_LOGIN ];
                
                if ([go_status isEqualToString:@"register_add_photo"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"register_add_photo" object:nil];
                }else if ([go_status isEqualToString:@"register_use_fb_info"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"register_use_fb_info" object:nil];
                }else if ([go_status isEqualToString:@"register_get_friend_list"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"register_get_friend_list" object:nil];
                }
                
            }
            
            NSLog(@"FBSessionStateOpen");
            break;
        case FBSessionStateClosed:
            [self openSessionWithAllowLoginUI:YES];
            NSLog(@"FBSessionStateClosed");
            if (error) {
                [self openSessionWithAllowLoginUI:YES];
            }
            break;
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            NSLog(@"FBSessionStateClosedLoginFailed");
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        // [self openSessionWithAllowLoginUI:YES];
    }
}

-(BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_location",
                            @"user_birthday",
                            @"user_likes",
                            @"email",
                            @"offline_access",
                            @"publish_stream",
                            @"publish_actions",
                            nil];
    return [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:allowLoginUI completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        [self sessionStateChanged:session state:status error:error];
    }];
    
    return [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:allowLoginUI completionHandler:^(FBSession *session,FBSessionState state,NSError *error) {
        [self sessionStateChanged:session state:state error:error];
    }];
}

#pragma mark Location

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    double degreesLatitude = newLocation.coordinate.latitude;
	double degreesLongitude = newLocation.coordinate.longitude;
    
//    NSLog(@"location updated: (%f,%f)",degreesLatitude,degreesLongitude);
	
	[[Singleton sharedSingleton] setCurrentLat:degreesLatitude];
	[[Singleton sharedSingleton] setCurrentLong:degreesLongitude];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}

#pragma mark - Logout Events

-(void)logout:(NSNotification *)notification{

    self.window.rootViewController = nil;
    UINavigationController *nav;
    if (isiPhone) {
        nav =[[UINavigationController alloc]initWithRootViewController:viewController];
    }else{
       // nav =[[UINavigationController alloc]initWithRootViewController:viewController_iPad];
    }

    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
}

-(BOOL)addSkipBackupAttributeToItemAtURL_5:(NSURL *)URL {
//    const char* filePath = [[URL path] fileSystemRepresentation];
//    
//    const char* attrName = "com.apple.MobileBackup";
//    u_int8_t attrValue = 1;
    
   // int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return 0;//result == 0;
}

-(BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                    
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
    
}

#pragma mark - ChartboostDelegate <NSObject>

// Called before requesting an interstitial from the back-end
- (BOOL)shouldRequestInterstitial:(NSString *)location
{
    NSLog(@"should request Interstitial: %@", location);
    return YES;
}


// Called when an interstitial has failed to come back from the server
// This may be due to network connection or that no interstitial is available for that user
- (void)didFailToLoadInterstitial:(NSString *)location
{
    NSLog(@"failed loading Interstitial: %@", location);
}

// Called when the user dismisses the interstitial
//- (void)didDismissInterstitial:(NSString *)location;

// Same as above, but only called when dismissed for a close
- (void)didCloseInterstitial:(NSString *)location
{
    NSLog(@"did close: %@, cache again!", location);
//    [[Chartboost sharedChartboost] cacheInterstitial: location];
}

// Same as above, but only called when dismissed for a click
//- (void)didClickInterstitial:(NSString *)location;


// Called when an interstitial has been received and cached.
- (void)didCacheInterstitial:(NSString *)location
{
    NSLog(@"didCache: %@", location);
}


- (BOOL)shouldRequestInterstitialsInFirstSession
{
    return NO;
}


// Called when an more apps page has been received, before it is presented on screen
// Return NO if showing the more apps page is currently inappropriate
- (BOOL)shouldDisplayMoreApps
{
    NSLog(@"shouldDisplayMoreApps ?");
    return YES;
}

// Called before requesting the more apps view from the back-end
// Return NO if when showing the loading view is not the desired user experience
- (BOOL)shouldDisplayLoadingViewForMoreApps
{
    NSLog(@"shouldDisplayLoadingViewForMoreApps ?");
    return YES;
}

// Called when the user dismisses the more apps view
//- (void)didDismissMoreApps;

// Same as above, but only called when dismissed for a close
- (void)didCloseMoreApps
{
//    [[Chartboost sharedChartboost] cacheMoreApps];
}

// Same as above, but only called when dismissed for a click
- (void)didClickMoreApps
{
    NSLog(@"DID click more apps");
}

// Called when a more apps page has failed to come back from the server
- (void)didFailToLoadMoreApps
{
    NSLog(@"didFailToLoadMoreApps");
}

// Called when the More Apps page has been received and cached
- (void)didCacheMoreApps
{
    NSLog(@"didCacheMoreApps");
}

@end
