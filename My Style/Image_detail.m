//
//  Image_detail.m
//  My Style
//
//  Created by Tis Macmini on 5/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Image_detail.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "AddPromotionViewController.h"
#import <STPopup/STPopup.h>

@interface Image_detail () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, Share_photo_ViewControllerDelegate, STXSectionHeaderViewDelegate, AddPromotionViewControllerDelegate>{

    Hash_tag_ViewController *hash_tag_viewObj;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    AddPromotionViewController *addPromotionView;
}

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation Image_detail
@synthesize shareObj,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view;
@synthesize tbl_news,imgfullscreenviewobj;
@synthesize arrayFeedArray,currentSelectedIndex;

NSInteger cellClickIndex3=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width-5, self.view.frame.size.height-80);
        self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.img_like_heart setHidden:YES];
    
    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
    self.tbl_news.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tbl_news.delegate = delegate;
    self.tableViewDelegate = delegate;
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"400" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-400" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-400" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"401" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-401" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-401" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"402" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-402" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-402" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"403" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-403" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-403" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"404" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-404" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-404" object:nil];
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
  //  self.shareObj=[self.arrayFeedArray objectAtIndex:currentSelectedIndex];
    
    NSMutableArray *tempArray=[[NSMutableArray alloc] init];
    [tempArray addObject:[self.arrayFeedArray objectAtIndex:currentSelectedIndex]];
    
    NSLog(@"TempArrayCount:%ld", (long)[tempArray count]);
    self.tableViewDataSource.posts = tempArray;// copy];
    self.tableViewDelegate.posts = tempArray;// copy];
    
    ((STXFeedTableViewDataSource *)self.tbl_news.dataSource).promotion = self.promotion;
    ((STXFeedTableViewDelegate *)self.tbl_news.delegate).promotion = self.promotion;
    
    [self.tbl_news setContentOffset:CGPointMake(0, 0)];
    [self.tbl_news reloadData];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@",self.shareObj.username,self.shareObj.image_id]);
    
//    if(is_iPhone_5) {
//        self.tbl_news.frame=CGRectMake(0,61,320,467);
//    }
//    else {
//        self.tbl_news.frame=CGRectMake(0,61,320,379);
//    }
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObjs=[arrayFeedArray objectAtIndex:currentSelectedIndex];
    which_image_liked = userActionCell.tag;
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

    if (![shareObjs.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}

-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}
-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[arrayFeedArray objectAtIndex:currentSelectedIndex];
  //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

// share photo view controller delegate
- (void)shareObjsChanged:(Share_photo_ViewController *)viewController shareObjs:(Home_tableview_data_share *)shareObjz
{
    viewController.delegate = nil;
    NSMutableArray *tempArrayFeedArray = [arrayFeedArray mutableCopy];
    tempArrayFeedArray[currentSelectedIndex] = shareObjz;
    self.arrayFeedArray = tempArrayFeedArray;
}

// add promotion view controller delegate
- (void)shareObjsChangedInPromotion:(AddPromotionViewController *)viewController shareObjs:(Home_tableview_data_share *)shareObjz
{
    viewController.delegate = nil;
    NSMutableArray *tempArrayFeedArray = [arrayFeedArray mutableCopy];
    tempArrayFeedArray[currentSelectedIndex] = shareObjz;
    self.arrayFeedArray = tempArrayFeedArray;
}

-(void)userWillShare:(STXUserActionCell *)userActionCell {
    
    NSInteger tag =userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    Home_tableview_data_share *shareObjs =[arrayFeedArray objectAtIndex:currentSelectedIndex];
    
    if (self.promotion)
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Promotion Options"];
        
//        NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
//        NSString *uid2=[NSString stringWithFormat:@"%ld",(long)[self.profile_share_obj.user_id integerValue]];
//        
//        Home_tableview_data_share *shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        if ([shareObjs.image_owner isEqualToString:@"yes"])
        {
            [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@", salt, sig, shareObjs.image_id];
                NSString *requestStr = [NSString stringWithFormat:@"%@promotion_post_delete.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
                NSLog(@"%@", requestStr);
                
                [SVProgressHUD show];
                [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
                    NSLog(@"%@", responseObject);
                    if ([responseObject[@"success"] isEqualToString:@"1"])
                    {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    [SVProgressHUD dismiss];
                } failure:^(NSString *errorString) {
                    [SVProgressHUD dismiss];
                    NSLog(@"%@", errorString);
                }];
            }];
            
            [sheet addButtonWithTitle:@"Edit" block:^{
                addPromotionView = [[AddPromotionViewController alloc] initWithNibName:@"AddPromotionViewController" bundle:nil];
                addPromotionView.user_id = LOGINEDUSERID;
                addPromotionView.homeData = shareObjs;
                addPromotionView.editing = YES;
                addPromotionView.delegate = self;
                [self.navigationController pushViewController:addPromotionView animated:YES];
            }];
        }
        else
        {
            [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
                
            }];
        }
        
        [sheet addButtonWithTitle:@"Share" block:^{
            NSString *textToShare = shareObjs.description;
            NSString *myWebsite = shareObjs.image_path;
            
            NSArray *objectsToShare = @[textToShare, myWebsite];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo,
                                           UIActivityTypePostToFacebook,
                                           UIActivityTypePostToTwitter,
                                           UIActivityTypePostToWeibo,
                                           UIActivityTypeMessage,
                                           UIActivityTypeMail];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        
        [sheet showInView:self.view];
        
        return;
    }
    
    if ([shareObjs.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObjs;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObjs.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObjs.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObjs.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObjs.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObjs.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObjs.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
            //  [controller release];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[arrayFeedArray objectAtIndex:currentSelectedIndex];
   // self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag =likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObjsss =[arrayFeedArray objectAtIndex:currentSelectedIndex];
    self.liker_list_viewObj.image_id =shareObjsss.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
   // pushPopFlg=1;
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
   // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}

- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
   // pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(IBAction)btnProfileClicked:(id)sender {
    //Home_tableview_data_share *obj =[arrayFeedArray objectAtIndex:((UIButton *)sender).tag];
    Home_tableview_data_share *obj =[arrayFeedArray objectAtIndex:currentSelectedIndex];
    
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld", (long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    NSInteger selectedIndex = posterLike.tag;
    if (selectedIndex != currentSelectedIndex)
        selectedIndex = currentSelectedIndex;
    
    Home_tableview_data_share *shareObjs=[arrayFeedArray objectAtIndex:selectedIndex];
    which_image_liked=posterLike.tag;
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

}

#pragma mark Method for Stop Header scrolling in UITableview
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat sectionHeaderHeight = 50;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
}

#pragma mark - Action sheet
-(IBAction)btn_photo_option_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    
    
    if ([self.shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            
        }];
        
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            NSLog(@"");
            self.share_photo_view.str_img_url = self.shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            NSLog(@"");
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =self.shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            NSLog(@"");
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
                NSLog(@"%@",htmlStr);
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        NSLog(@"");
        self.share_photo_view.str_img_url = self.shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        NSLog(@"");
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =self.shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        NSLog(@"");
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",self.shareObj.image_path];
            NSLog(@"%@",htmlStr);
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    /*  [sheet addButtonWithTitle:@"Show Action Sheet on top" block:^{
     NSLog(@"");
     }];
     [sheet addButtonWithTitle:@"Show another alert" block:^{
     NSLog(@"");
     }];
     */
    [sheet showInView:self.view];
}
-(void)report_inappropriate:(int )index{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    //post_report_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&report_id=2&item_id=12&uid=1
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark - List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    self.liker_list_viewObj.image_id =self.shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark - Go to Profile
-(IBAction)btn_profile_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld", (long)tag);

    user_info_view.user_id=self.shareObj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark - UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:(int)buttonIndex];
        
    }
}

#pragma mark - Delete Photo
-(void)delete_photo{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,self.shareObj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"402":nil];
}

-(void)getdeleteResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.navigationController popViewControllerAnimated:YES];
      //  [self get_news_feed];
    }
}

#pragma mark - Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%ld&item_id=%@&uid=%@",salt,sig, (long)rating,self.shareObj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"403":nil];
    self.shareObj.my_rating = [NSString stringWithFormat:@"%ld", (long)rating];

    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    // [dlstarObj  setRating:3];
}
-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld", (long)[[dataDict objectForKey:@"rate"] integerValue]];
        
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex3] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

#pragma mark - like unlike call
-(IBAction)btn_like_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_liked=tag;
    
    if (![self.shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:self.shareObj.uid imageid:self.shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([self.shareObj.liked isEqualToString:@"no"]) {
        self.shareObj.liked=@"yes";
        int count =[self.shareObj.likes intValue];
        count++;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        
        [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        self.shareObj.liked=@"no";
        int count =[self.shareObj.likes intValue];
        count--;
        self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        
        if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
}

-(IBAction)btnclosefullscreenClick{
    [imgfullscreenviewobj removeFromSuperview];
}


-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //[SVProgressHUD show];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr;
    if (self.promotion == NO)
        requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    else
        requestStr = [NSString stringWithFormat:@"%@promotion_post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"401":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {

        NSLog(@"%@", self.shareObj.liked);
//        if ([self.shareObj.liked isEqualToString:@"no"]) {
//            self.shareObj.liked=@"yes";
//            int count =[self.shareObj.likes intValue];
//            count++;
//            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            
//            
//            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            
//        }else{
//            
//            self.shareObj.liked=@"no";
//            int count =[self.shareObj.likes intValue];
//            count--;
//            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            
//            if ([self.shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                [self.shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//        }
//
//        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    // NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"404":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {

        if ([self.shareObj.liked isEqualToString:@"no"]) {
            self.shareObj.liked=@"yes";
            int count =[self.shareObj.likes intValue];
            count++;
            self.shareObj.likes=[NSString stringWithFormat:@"%d",count];

            
            [self.shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            NSLog(@"%@",self.shareObj.array_liked_by);
            NSLog(@"%@",self.shareObj.likes);
            
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
            
        }
        
    }
    
}

#pragma mark - Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}
- (void)atSelected:(id)sender {

    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mark - Comments Method
-(IBAction)btn_comment_click:(id)sender{

  //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=self.shareObj.image_id;
    self.comments_list_viewObj.array_comments=self.shareObj.array_comments_tmp;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    //  [self.navigationController pushViewController:self.comments_list_viewObj animated:YES];
}

#pragma mark - Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    //    if (result == MFMailComposeResultSent) {
    //
    //    }
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)FailNewsReson:(NSNotification *)notification {
	//[SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
