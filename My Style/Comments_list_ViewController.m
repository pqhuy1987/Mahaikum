//
//  Comments_list_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Comments_list_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextView+AVTagTextView.h"
#import "AVCustomTagTableViewController.h"
#import "NSString+AVTagAdditions.h"
#import "AppDelegate.h"
#import "Profile_ViewController.h"
#import "STXAttributedLabel.h"
#import "BlockUserRecord.h"
#import "search_view_cell.h"

#define BOUNCE_PIXELS 5.0
#define PUSH_STYLE_ANIMATION NO
#define USE_GESTURE_RECOGNIZERS YES

//static NSString *HashTagAndMentionRegex = @"(#|@)(\\w+)";
@interface Comments_list_ViewController()<AVTagTextViewDelegate,Comment_list_CellDelegate> {
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    Comments_list_cell *objComment_list_cell;
    BlockUserRecord*objBlockUserRecord;
    BOOL  userblock;
    CGFloat commentsTblHeight;
    CGFloat lastContentOffset;
}

@end

@interface Comments_list_ViewController (PrivateStuff)

@end

@implementation Comments_list_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

- (IBAction)btn_back_click:(id)sender {
    
    NSString *countStr=[NSString stringWithFormat:@"%lu",(unsigned long)[self.array_comments count]];
    NSDictionary *dictDict=[NSDictionary dictionaryWithObjectsAndKeys:countStr,@"CommentCount",self.array_comments,@"CommentArray", nil];
    
    NSDictionary* dictData = [NSDictionary dictionaryWithObject: dictDict forKey:@"index"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadCommentViewCell" object:nil userInfo:dictData];
    self.tblSearch.hidden=YES;
    textView.text=@"";
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    commentsTblHeight = self.tbl_comments.frame.size.height;
    
    UISwipeGestureRecognizer* swipeDownGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDownFrom:)];
    swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.tbl_comments addGestureRecognizer:swipeDownGestureRecognizer];
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRight:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];

    
    //self.array_comments=[[NSMutableArray alloc]init];
//    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
//    self.imageCaption_lbl.text=self.imageCaption_Tittle;
    self.searchArray=[[NSMutableArray alloc] init];
    self.tagsArray=[[NSMutableArray alloc] init];
    objComment_list_cell.lbl_desc.delegate=self;
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    self.animatingSideSwipe = NO;
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, kViewHeight - 40, kViewWidth, 40)];
    
    UIImage *rawBackground = [UIImage imageNamed:@"Comment Section BG.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [containerView addSubview:imageView];
    
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Comment Box.png"]];
    entryImageView.frame = CGRectMake(5, 5, kViewWidth - 64.0, 30);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [containerView addSubview:entryImageView];
    
    textView=[[UITextView alloc] initWithFrame:CGRectMake(45, 5, kViewWidth - 85.0, 30)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    textView.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    textView.delegate = self;
    textView.backgroundColor = [UIColor clearColor];
    textView.keyboardType=UIKeyboardTypeTwitter;
    textView.hashTagsDelegate = self;
    textView.hashTagsTableViewHeight = 120.0f;
    
    [self.view addSubview:containerView];
    [containerView addSubview:textView];
    
    self.doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.doneBtn.backgroundColor = [UIColor clearColor];
    [self.doneBtn addTarget:self action:@selector(resignTextView) forControlEvents:UIControlEventTouchUpInside];
    [self.doneBtn setImage:[UIImage imageNamed:@"Send Button.png"] forState:UIControlStateNormal];
    self.doneBtn.imageView.image=[UIImage imageNamed:@"Send Button.png"];
    self.doneBtn.frame = CGRectMake(kViewWidth - 53.0, 5, 47, 28);
    [containerView addSubview:self.doneBtn];
    
    [self checkblock_user_list:self.post_user_id];
    
    [self get_search_user];
}

- (void)gestureRight:(id)sender
{
    [self btn_back_click:self];
}

- (void)checkblock_user_list:(NSString*)postuser_id
{
    if (!postuser_id)
        return;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@",salt,sig,postuser_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_list.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"])
        {
            NSMutableArray *tempArray =[responseObject valueForKey:@"data"];
            
            for (BlockUserRecord * obj in tempArray)
            {
                if([LOGINEDUSERID isEqualToString:(NSString *)obj])
                {
                    NSLog(@"comparision successful");
                    //btextView.userInteractionEnabled=NO;
                    userblock=YES;
                }
                else
                {
                    userblock=NO;
                }
            }
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"2"])
        {
            userblock=NO;
        }
    } failure:^(NSString *errorString) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

- (NSArray *)tagsForQuery:(NSString *)query
{
    //NSPredicate *bPredicate =
    //[NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", query];
    //NSArray *array =[self.tagsArray filteredArrayUsingPredicate:bPredicate];
    NSLog(@"%@", query);
    
    NSArray *array =self.tagsArray;
    Comment_share *obj;
    
    [self.searchArray removeAllObjects];
 
    for ( int i=0;i<[array count];i++) {
        //[self.searchArray addObject:[array objectAtIndex:i]];
        obj = [array objectAtIndex:i];
        if ([obj.name containsString:query] || [obj.username containsString:query])
        {
            [self.searchArray addObject:[array objectAtIndex:i]];
            //NSLog(@"Search Result->name:%@, username:%@", obj.name, obj.username);
        }
    }
    
    if ([self.searchArray count]==0) {
        self.tblSearch.hidden=YES;
    }
    else {
        self.tblSearch.hidden=NO;
    }
    
    [self.tblSearch reloadData];
    return array;
}

#pragma mark - Get Search user call
-(void)get_search_user{
//    [textView resignFirstResponder];
    
    [SVProgressHUD dismiss];
     NSString *requestStr=@"";
     NSString *notificationVal=@"";

     NSLog(@" search for username");
     requestStr = [NSString stringWithFormat:@"%@search_users.php",[[Singleton sharedSingleton] getBaseURL]];
            
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchUsersResponce:) name:@"14232" object:nil];
            
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsearchUsersResponce:) name:@"-14232" object:nil];
            
     notificationVal=@"14232";
        
     NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
     NSString *key = SIGNSALTAPIKEY;
     NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
     NSString *sig = [StaticClass returnMD5Hash :tempStr];
     NSLog(@"requestStr:%@",requestStr);
        
     NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                @"", @"key",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                nil];
     NSLog(@"params:%@",params);
     AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
     [networkQueue queueItems:requestStr :notificationVal :params];
}

-(void)searchUsersResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.tagsArray  removeAllObjects];
        NSArray *temarray =[result valueForKey:@"data"];
        for (NSDictionary *dict in temarray) {
            Comment_share *obj =[[Comment_share alloc]init];
            obj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            obj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            obj.image_url=[StaticClass urlDecode:[dict valueForKey:@"image"]];
            obj.uid =[dict valueForKey:@"user_id"];
            
            //NSLog(@"name=%@, username=%@, image_url=%@", obj.name, obj.username, obj.image_url);
            [self.tagsArray addObject:obj];
        }
        [self.tblSearch reloadData];
    }
    else if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        NSLog(@"No records are found.");
    }
}

-(void)FailsearchUsersResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    
    [SVProgressHUD dismiss];
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

- (BOOL)textView:(UITextView *)textViewsss shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]){
        
        [textView resignFirstResponder];
        NSLog(@"Entered tags: %@", [textView.hashTags componentsJoinedByString:@" "]);
        
//        [self get_search_user];
    }
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    lastContentOffset = 0;
    
    //self.imageCaption_lbl.text=self.imageCaption_Tittle;
    
    self.navigationController.navigationBarHidden = YES;
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    containerView.frame = CGRectMake(0, kViewHeight - 40, kViewWidth, 40);
    
    [self.tbl_comments reloadData];
    [textView resignFirstResponder];
    if ([self.array_comments count] > 0){
        [self.tbl_comments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.array_comments.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
/*    NSMutableArray *tempFollowing=[[Singleton sharedSingleton] getCurrentDictFollowing];
    
    [self.tagsArray removeAllObjects];
    for (int i=0;i<[tempFollowing count];i++) {
        NSString *strName=[StaticClass urlDecode:[tempFollowing objectAtIndex:i]];
        [self.tagsArray addObject:strName];
    }
*/
    //tagsArray=[[NSArray alloc] initWithObjects:@"tag1", @"instagram", @"anothertag", @"hmm", @"tag3", @"tag4", @"tag5", @"tag6", @"tag7", nil];
    
    //  [self get_comments_list];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self checkblock_user_list:self.post_user_id];
    [self get_search_user];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [textView resignFirstResponder];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

-(void)btn_Warning_click:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You can only delete your own comments." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark
#pragma mark Get Comments list
- (void)get_comments_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,self.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@", requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            NSMutableArray *tempArray =[responseObject valueForKey:@"data"];
            for (NSDictionary *dict in tempArray) {
                Comment_share *obj =[[Comment_share alloc]init];
                
                obj.comment_id =[dict valueForKey:@"id"];
                obj.uid =[dict valueForKey:@"uid"];
                obj.username =[dict valueForKey:@"username"];
                obj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
                obj.image_url=[StaticClass urlDecode:[dict valueForKey:@"image"]];
                obj.comment_desc=[StaticClass urlDecode:[dict valueForKey:@"comment_desc"]];
                obj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                self.array_comments[self.array_comments.count - 1] = obj;
                return;
            }
        }
        
        [self.tbl_comments reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.array_comments.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        
        NSLog(@"commentsArray:%lu",(unsigned long)[self.array_comments count]);
        NSString *countStr=[NSString stringWithFormat:@"%lu",(unsigned long)[self.array_comments count]];
        NSDictionary *dictDict=[NSDictionary dictionaryWithObjectsAndKeys:countStr,@"CommentCount",self.array_comments,@"CommentArray", nil];
        
        NSDictionary* dictData = [NSDictionary dictionaryWithObject: dictDict forKey:@"index"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadCommentViewCell" object:nil userInfo:dictData];
        
        //[self.tbl_comments reloadData];
        if (self.array_comments.count > 0)
        {
            [self.tbl_comments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.array_comments.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    } failure:^(NSString *errorString) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Comment couldn't get. Please check your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==self.tblSearch) {
        return 55.0f;
    }
    else {
        NSInteger indCount = indexPath.row;
        Comment_share *obj = [self.array_comments objectAtIndex:indCount];
        
        //Comment_share *obj =[self.array_comments objectAtIndex:indexPath.row];
        CGSize size =[obj.comment_desc sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0] constrainedToSize:CGSizeMake(255, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
        
        if (size.height<25) {
            return 58.0f;
        }
        return 30.0f+size.height+20.0f;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==self.tblSearch) {
        return self.searchArray.count;
    }
    else {
        
        return self.array_comments.count;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView==self.tblSearch) {
        //return 41.0f;
        return 0.0f;
    }
    else {
        return 0.0f;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView==self.tblSearch)
    {
        UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
        strip.frame =CGRectMake(0,0,kViewWidth,41);
        
        UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
        lbl.textColor=[UIColor blackColor];
        lbl.backgroundColor=[UIColor clearColor];
        lbl.textAlignment=NSTextAlignmentCenter;
        lbl.text=@"Suggestion";

        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
        [view setBackgroundColor:[UIColor clearColor]];
        [view addSubview:strip];
        [view addSubview:lbl];
        //return view;
        return Nil;
    }
    else
    {
        return Nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (tableView==self.tblSearch) {
        
/*        static NSString *CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        NSString *tag = [self.searchArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"@%@", tag];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:14.0f];
        cell.backgroundColor=[UIColor whiteColor];
        cell.textLabel.textColor=[UIColor blackColor];
*/
        
        static NSString *identifier =@"search_view_cell";
        search_view_cell *cell=(search_view_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"search_view_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        Comment_share *shareObj =[self.searchArray objectAtIndex:indexPath.row];
        cell.lbl_username.text =[StaticClass urlDecode:shareObj.name ];
        cell.lbl_name.text = [StaticClass urlDecode:shareObj.username];
        
        cell.img_user.layer.cornerRadius = 22.0;
        cell.img_user.layer.masksToBounds=YES;
        
        [cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];

        return cell;
        
    }
    else {
        cell = [self comment_list_CellForTableView:tableView atIndexPath:indexPath];
        
        return cell;
    }
}

- (Comments_list_cell *)comment_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Comments_list_cell";
    objComment_list_cell = (Comments_list_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    objComment_list_cell.customdelegate = self;
    if(objComment_list_cell == nil)
    {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Comments_list_cell" owner:self options:nil];
        objComment_list_cell = [nib objectAtIndex:0];
        [objComment_list_cell draw_in_cell];
        objComment_list_cell.showsReorderControl = NO;
        objComment_list_cell.selectionStyle = UITableViewCellSelectionStyleNone;
        objComment_list_cell.backgroundColor=[UIColor whiteColor];
        
        [objComment_list_cell.btn_user_name addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        [objComment_list_cell.img_user1 addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    Comment_share *shareObj = [self.array_comments objectAtIndex:indexPath.row];
    
    MGSwipeButton *btn_Spam = [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"spam.png"] backgroundColor:[UIColor lightGrayColor]];
    [btn_Spam addTarget:self action:@selector(btn_spam_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_Spam.tag = indexPath.row;
    
    MGSwipeButton *btn_Abusive = [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"Abusive.png"] backgroundColor:[UIColor lightGrayColor]];
    [btn_Abusive addTarget:self action:@selector(btn_Abusive_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_Abusive.tag = indexPath.row;
    
    MGSwipeButton *btn_delete = [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"comments-actions-icon-delete"] backgroundColor:[UIColor redColor]];
    [btn_delete addTarget:self action:@selector(btn_delete_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_delete.tag = indexPath.row;
    
    MGSwipeButton *btn_reply = [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"comments-actions-icon-reply"] backgroundColor:[UIColor lightGrayColor]];
    [btn_reply addTarget:self action:@selector(btn_reply_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_reply.tag = indexPath.row;
    
    MGSwipeButton *btn_Warning = [MGSwipeButton buttonWithTitle:nil icon:[UIImage imageNamed:@"comments-actions-icon-warning"] backgroundColor:[UIColor lightGrayColor]];
    [btn_Warning addTarget:self action:@selector(btn_Warning_click:) forControlEvents:UIControlEventTouchUpInside];
    btn_Warning.tag = indexPath.row;
    
    if ([shareObj.OwnPost isEqualToString:@"yes"]||[shareObj.uid isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]])
    {
        objComment_list_cell.rightButtons = @[btn_Spam, btn_Abusive, btn_delete, btn_reply];
        objComment_list_cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    }
    else
    {
        objComment_list_cell.rightButtons = @[btn_Spam, btn_Abusive, btn_Warning, btn_reply];
        objComment_list_cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    }
    
    objComment_list_cell.img_user1.tag = indexPath.row;
    objComment_list_cell.btn_user_name.tag = indexPath.row;
    [objComment_list_cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
    
    objComment_list_cell.img_user.layer.cornerRadius =22.0f;
    objComment_list_cell.img_user.layer.masksToBounds =YES;
    [objComment_list_cell.btn_user_name setTitle:[StaticClass urlDecode:shareObj.username] forState:UIControlStateNormal];
    //[cell.lbl_desc setText:shareObj.comment_desc];
    
    objComment_list_cell.lbl_date.text =[self get_time_different:shareObj.datecreated];
    objComment_list_cell.lbl_desc.text=shareObj.comment_desc;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:objComment_list_cell.lbl_desc.text];
    
    NSArray *words=[objComment_list_cell.lbl_desc.text componentsSeparatedByString:@" "];
    
    for (NSString *word in words) {
        if ([word hasPrefix:@"#"]||[word hasPrefix:@"@"]) {
            NSRange range=[objComment_list_cell.lbl_desc.text rangeOfString:word];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(39.0f/255) green:(105.0f/255) blue:(176.0f /255)alpha:1.0f] range:range];
            [string addAttribute:NSLinkAttributeName value:word range:range];
        }
    }
    
    [objComment_list_cell.lbl_desc setAttributedText:string];
    CGSize size =[shareObj.comment_desc sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0] constrainedToSize:CGSizeMake(255, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    if (size.height<24) {
        objComment_list_cell.view_line.frame = CGRectMake(0,54, kViewWidth,1);
    } else {
        objComment_list_cell.view_line.frame = CGRectMake(0,34.f+size.height,kViewWidth,1);
    }
    
    objComment_list_cell.view_line.hidden=YES;
    
    if (size.height<24)
    {
        objComment_list_cell.lbl_desc.frame=CGRectMake(58,25, 255,size.height+15);
    }
    else
    {
        objComment_list_cell.lbl_desc.frame=CGRectMake(58,25, 255, size.height+15);
    }
    
    objComment_list_cell.tag=indexPath.row;
    objComment_list_cell.lbl_desc.delegate = self;
    
    return objComment_list_cell;
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSString *myString = [URL absoluteString];
    
    if ([myString hasPrefix:@"#"]) {
        NSString *hashtag = [myString substringFromIndex:1];
        
        // [cell comment_list:hashtag];
        if ([self respondsToSelector:@selector(comment_list_Cell:didSelectHashtag:)]) {
            [self comment_list_Cell:objComment_list_cell didSelectHashtag:hashtag];
        }
    }
    else if ([myString hasPrefix:@"@"])
    {
        NSString *mention = [myString substringFromIndex:1];
        if ([self respondsToSelector:@selector(comment_list_Cell:didSelectMention:)]) {
            [self comment_list_Cell:objComment_list_cell didSelectMention:mention];
            
        }
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==self.tblSearch)
    {
        //NSString *str=[self.searchArray objectAtIndex:indexPath.row];
        Comment_share *obj = [self.searchArray objectAtIndex:indexPath.row];
        
        NSString *replaceString = [NSString stringWithFormat:@"@%@ ", obj.username];
        NSMutableString *mutableText = [NSMutableString stringWithString:textView.text];
        
        [[NSString endOfStringHashtagRegex] replaceMatchesInString:mutableText options:0 range:[textView.text wholeStringRange] withTemplate:replaceString];
        textView.text = mutableText;
 
        self.tblSearch.hidden=YES;
    }
}

- (void)didSlectComment:(Comments_list_cell *)cell
{
    // NSIndexPath *indexPath = [self.tbl_comments indexPathForCell:cell];
    // int row = indexPath.row;
}

// Apple's docs: To enable the swipe-to-delete feature of table views (wherein a user swipes horizontally across a row to display a Delete button), you must implement the tableView:commitEditingStyle:forRowAtIndexPath: method.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView!=self.tblSearch) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void)comment_list_Cell:(Comments_list_cell *)comment_list_Cell didSelectHashtag:(NSString *)hashtag
{
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:hash_tag_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    //hash_tag_viewObj.str_title = hashtag;
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    //[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:nav animated:YES];
    
}

- (void)comment_list_Cell:(Comments_list_cell *)comment_list_Cell didSelectMention:(NSString *)mention
{
    user_info_view.user_id=mention;
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:user_info_view];
    [self presentViewController:nav animated:YES completion:nil];
    
    [self.navigationController pushViewController:nav animated:YES];
    
}

// Note that the animation is done
- (void)animationDidStopAddingSwipeView:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    self.animatingSideSwipe = NO;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    if ( ![scrollView isKindOfClass:[UITableView class]] )
//        return;
//    
//    ScrollDirection scrollDirection;
//    if (lastContentOffset > scrollView.contentOffset.y)
//    {
//        scrollDirection = ScrollDirectionDown;
//        [textView resignFirstResponder];
//    }
//    else if (lastContentOffset < scrollView.contentOffset.y)
//    {
//        scrollDirection = ScrollDirectionUp;
//    }
//    
//    lastContentOffset = scrollView.contentOffset.x;
//}

#pragma mark
#pragma mark TextView for Comments

- (void)resignTextView
{
    self.str_comments=textView.text;
    [textView resignFirstResponder];
    
    if(userblock == YES)
    {
        UIAlertView*objBlockUser=[[UIAlertView alloc]initWithTitle:nil message:@"You have been blocked by the author" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [objBlockUser show];
    }
    else
    {
        if (![self.str_comments isEqualToString:@"Add a comment..."] &&![textView.text isEqualToString:@""])
        {
            self.tblSearch.hidden=YES;
            
            //Added by akshay
            Comment_share *obj = [[Comment_share alloc]init];
            obj.uid = [StaticClass retrieveFromUserDefaults:USER_ID];
            obj.username = [StaticClass retrieveFromUserDefaults:USERNAME];
            obj.name = [StaticClass retrieveFromUserDefaults:USER_NAME];
            obj.image_url = [StaticClass retrieveFromUserDefaults:USER_IMAGE];
            obj.comment_desc = textView.text;
            
            NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            NSString *dateString1 = [dateFormatter stringFromDate:[NSDate date]];
            
            obj.datecreated = dateString1;
            [self.array_comments addObject:obj];
            [self.tbl_comments reloadData];
            
            if ([self.array_comments count] > 0)
            {
                [self.tbl_comments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.array_comments.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
            
            textView.text = @"";
            [self post_comments];
        }
    }
}

- (void)keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = kViewHeight - (keyboardBounds.size.height + containerFrame.size.height)+2;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    
    _containerView1.frame = CGRectMake(0, 64, kViewWidth, kViewHeight-64-keyboardBounds.size.height);
    
    // commit animations
    [UIView commitAnimations];
    
//    [self hideRoomTableView];
}

- (void)keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // get a rect for the textView frame
    CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = kViewHeight - containerFrame.size.height;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    containerView.frame = containerFrame;
    _containerView1.frame = CGRectMake(0, 64, kViewWidth, kViewHeight-64);
    
    // commit animations
    [UIView commitAnimations];
    
//    [self showRoomTableView];
}

- (void)handleSwipeDownFrom:(UIGestureRecognizer *)sender
{
    NSLog(@"Swipe Down");
}

- (void)showRoomTableView
{
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionTransitionCurlDown animations:^{
        CGRect frame = self.tbl_comments.frame;
        frame.size.height = commentsTblHeight;
        self.tbl_comments.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideRoomTableView
{
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        CGRect frame = self.tbl_comments.frame;
        frame.size.height = commentsTblHeight - keyboardOffset;
        self.tbl_comments.frame = frame;
        
        if (self.array_comments.count > 0)
            [self.tbl_comments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.array_comments.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark
#pragma mark Delete Button click
- (void)btn_delete_click:(id)sender
{
    NSInteger indCount = ((UIButton *)sender).tag;
    Comment_share *obj=[self.array_comments objectAtIndex:indCount];
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@""];
    
    [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
        NSLog(@"Delete");
        NSInteger tempIndCount = ((UIButton *)sender).tag;
        [self.array_comments removeObjectAtIndex:tempIndCount];
        [self.tbl_comments beginUpdates];
        [self.tbl_comments deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:((UIButton *)sender).tag inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tbl_comments endUpdates];
        [self.tbl_comments reloadData];
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@&uid=%@",salt,sig,obj.comment_id,obj.uid];
        NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_comment.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
        NSLog(@"requestStr:%@",requestStr);
        
        [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
            NSLog(@"result:%@",responseObject);
        } failure:^(NSString *errorString) {
            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Comment couldn't get. Please check your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }];
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}

#pragma mark
#pragma mark Reply click
- (void)btn_reply_click:(id)sender
{
    //[self checkblock_user_list:self.post_user_id];
    if(userblock==NO)
    {
        NSInteger indCount = ((UIButton *)sender).tag;
        Comment_share *obj = [self.array_comments objectAtIndex:indCount];
        
        // Comment_share *obj =[self.array_comments objectAtIndex:((UIButton *)sender).tag];
        textView.text=[NSString stringWithFormat:@"@%@ ",obj.username];
        [textView becomeFirstResponder];
    }
}

- (void)btn_spam_click:(id)sender
{
    UIAlertView*objSpamAlert=[[UIAlertView alloc]initWithTitle:@"Spam" message:@"Are you sure want to report this comment as SPAM?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    [objSpamAlert show];
}

- (void)btn_Abusive_click:(id)sender
{
    UIAlertView*objSpamAlert=[[UIAlertView alloc]initWithTitle:@"Abusive" message:@"Are you sure want to report this comment as Abusive Content?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Cancel", nil];
    [objSpamAlert show];
}

#pragma mark
#pragma mark Post Comments
- (void)post_comments
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_comment.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",salt, @"salt",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",self.image_id,@"image_id",[self.str_comments stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"comment_desc",nil];
    NSLog(@"params:%@",params);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            [self get_comments_list];
            
            //AppDelegate *app = [AppDelegate sharedDelegate];
            //[app RepeateChatBoost];
        }
    } failure:^(NSString *errorString) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }];
    
    textView.text = @"";
}

#pragma mark
#pragma mark Handler Methods
- (NSString *)tagFromSender:(id)sender {
    return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:hash_tag_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:nav animated:YES];
}

- (void)atSelected:(id)sender {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:user_info_view];
    [self presentViewController:nav animated:YES completion:nil];
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:nav animated:YES];
}

- (void)urlSelected:(id)sender {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:web_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:nav animated:YES];
}

- (void)userSelected:(id)sender {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:user_info_view];
    [self presentViewController:nav animated:YES completion:nil];
    
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:nav animated:YES];
}

- (void)btn_user_info_click:(id)sender {
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:user_info_view];
    [self presentViewController:nav animated:YES completion:nil];
    
    Comment_share *shareObj=[self.array_comments objectAtIndex:((UIButton *)sender).tag];
    
    //Comment_share *shareObj =[self.array_comments objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=shareObj.username;
    [self.navigationController pushViewController:nav animated:YES];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
- (NSString *)get_time_different:(NSString *)datestring {
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end