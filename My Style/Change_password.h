//
//  Change_password.h
//  My Style
//
//  Created by Tis Macmini on 5/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
@interface Change_password : UIViewController{

    UITextField *txt_old_password;
    UITextField *txt_new_password;
    UITextField *txt_new_password_again;
    
    UILabel *lblheader,*lblsubheader;
    
    UIImageView *img_bd_firstcell;
    UIImageView *img_bd_secondcell;
    UIActivityIndicatorView *activity;
    
    UILabel *lblSpinnerBg;
}

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong)IBOutlet UITextField *txt_old_password;
@property(nonatomic,strong)IBOutlet UITextField *txt_new_password;
@property(nonatomic,strong)IBOutlet UITextField *txt_new_password_again;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblsubheader;
@property(nonatomic,strong)IBOutlet UIImageView *img_bd_firstcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bd_secondcell;
@property(nonatomic,strong)UIActivityIndicatorView *activity;
@end
