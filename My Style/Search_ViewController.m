//
//  Search_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Search_ViewController.h"
#import "KxMenu.h"
#import "HashtagSearchRecord.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "FirstAdsViewController.h"
#import "SecondAdsViewController.h"
#import "ThirdAdsViewController.h"
#import "SearchPostsViewController.h"
#import "Chameleon.h"
#import <STPopup/STPopup.h>
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

@interface Search_ViewController () <SearchPostsViewControllerDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, STPopupPreviewRecognizerDelegate, UICollectionViewDelegateFlowLayout>
{
    UIButton *topLikesButton, *freshButton, *lblText, *TopLikesText, *verifiedUsers, *lblText1;
    NSInteger getFeedKind;
    FirstAdsViewController *firstAdsView;
    SecondAdsViewController *secondAdsView;
    ThirdAdsViewController *thirdAdsView;
    
    NSMutableArray *array_verified_users;
}

@end

@implementation Search_ViewController
@synthesize collectionViewObj,view_header,view_footer,img_spinner,image_detail_viewObj,array_search,lblSpinnerBg;
@synthesize  txt_search,view_search,segment,tbl_users, array_all_users, array_search_result;
@synthesize user_info_view,array_hashtag_result,tbl_hashtag;
@synthesize hash_tag_viewObj,view_fillter,lbl_header,imgsegmenthashtags,imgsegmentusers,activity;
@synthesize imgArrow;
@synthesize topLikes, freshs;
@synthesize verifyUsersTableView;

static bool enter_mode = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refresh_search_view_data:(NSNotification *)notification {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    UITextField *searchBarTextField = [self.search_bar valueForKey:@"_searchField"];
    
    // Magnifying glass icon.
    UIImageView *leftImageView = (UIImageView *)searchBarTextField.leftView;
    leftImageView.image = [leftImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    leftImageView.tintColor = [UIColor whiteColor];
    
    // Clear button
    UIButton *clearButton = [searchBarTextField valueForKey:@"_clearButton"];
    [clearButton setImage:[clearButton.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    clearButton.tintColor = [UIColor whiteColor];
    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refresh_search_view_data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh_search_view_data:) name:@"refresh_search_view_data" object:nil];
    self.view_fillter.hidden = YES;
    //self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_bg.png"]];
    self.view_search.backgroundColor =[UIColor whiteColor];
    self.image_detail_viewObj =[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    self.user_info_view =[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    self.verifieduser_info_view =[[VerifiedUsersViewController alloc]initWithNibName:@"VerifiedUsersViewController" bundle:nil];

    self.hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    self.array_all_users =[[NSMutableArray alloc]init];
    self.array_search_result =[[NSMutableArray alloc]init];
    self.array_hashtag_result=[[NSMutableArray alloc]init];
    
    firstAdsView = [[FirstAdsViewController alloc] initWithNibName:@"FirstAdsViewController" bundle:nil];
    secondAdsView = [[SecondAdsViewController alloc] initWithNibName:@"SecondAdsViewController" bundle:nil];
    thirdAdsView = [[ThirdAdsViewController alloc] initWithNibName:@"ThirdAdsViewController" bundle:nil];
    
    _search_bar.layer.borderWidth = 0;
    _search_bar.layer.borderColor = _search_bar.barTintColor.CGColor;
    
    [self.view addSubview:self.view_header];
    self.view_header.frame = CGRectMake(0.0, 20.0, self.view.frame.size.width, 80.0);
    JOLImageSlide *slide = [[JOLImageSlide alloc] init];
    slide.image = @"http://mahalkum.com/images/M-ad1.png";
    
    JOLImageSlide *slide2 = [[JOLImageSlide alloc] init];
    slide2.image = @"http://mahalkum.com/images/M-ad2.png";
    
    JOLImageSlide *slide3 = [[JOLImageSlide alloc] init];
    slide3.image = @"http://mahalkum.com/images/M-ad3.png";
    
    NSArray *slideSet = [[NSArray alloc] initWithObjects: slide, slide2, slide3, nil];
    
    
    JOLImageSlider *imageSlider = [[JOLImageSlider alloc] initWithFrame:CGRectMake(0, 93,/*108,*/ self.view.bounds.size.width, 120) andSlides: slideSet];
    
    imageSlider.delegate = self;
    [imageSlider setAutoSlide: YES];
    [imageSlider setPlaceholderImage:@"placeholder.png"];
    [imageSlider setContentMode: UIViewContentModeScaleToFill];
    
    [self.view addSubview: imageSlider];
    
    self.verifyUsersTableView.frame = CGRectMake(0, 215, self.view.bounds.size.width, self.view.bounds.size.height - 265);
    self.verifyUsersTableView.delegate = self;
    self.verifyUsersTableView.dataSource = self;
    [self.view addSubview:self.verifyUsersTableView];
    self.verifyUsersTableView.hidden = YES;
    
    array_verified_users = [[NSMutableArray alloc]init];
    
    self.postCollectionView.frame = CGRectMake(0, 215, self.view.frame.size.width, self.view.bounds.size.height - 265);
    self.postCollectionView.delegate = self;
    self.postCollectionView.dataSource = self;
    [self.postCollectionView registerNib:[UINib nibWithNibName:@"image_collection_cell" bundle:nil] forCellWithReuseIdentifier:@"image_collection_cell"];
    [self.view addSubview:self.postCollectionView];
    self.postCollectionView.hidden = YES;
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    
//    topLikesButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 63.0 + 55.0 + 103.0 - 43, self.view.frame.size.width/2, self.view.frame.size.width/2)];
//    [self.view addSubview:topLikesButton];
//    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView *bluredView = [[UIVisualEffectView alloc] initWithEffect:effect];
//    bluredView.frame = CGRectMake(0.0, 63.0 + 55.0 + 103.0 - 43, self.view.frame.size.width/2, self.view.frame.size.width/2);
//    bluredView.alpha = 0.9;
//    [self.view addSubview:bluredView];
    TopLikesText = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 63.0 + 55.0 + 103.0 - 43, self.view.frame.size.width, self.view.frame.size.width)];
    //    [TopLikesText setTitle:@"Top Likes" forState:UIControlStateNormal];
    //    [TopLikesText addTarget:self action:@selector(topLikesButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [TopLikesText setTitleColor:[UIColor flatBlackColor] forState:UIControlStateNormal];
    TopLikesText.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    TopLikesText.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:TopLikesText];
//    
//    freshButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2)];
//    [self.view addSubview:freshButton];
//    UIBlurEffect *fresheffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView *freshbluredView = [[UIVisualEffectView alloc] initWithEffect:fresheffect];
//    freshbluredView.frame = CGRectMake(self.view.frame.size.width/2, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2);
//    freshbluredView.alpha = 0.9;
//    [self.view addSubview:freshbluredView];
//    lblText = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2)];
//    [lblText setTitle:@"New" forState:UIControlStateNormal];
//    [lblText addTarget:self action:@selector(freshButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [lblText setTitleColor:[UIColor flatBlackColor] forState:UIControlStateNormal];
//    [self.view addSubview:lblText];
//
//    verifiedUsers = [[UIButton alloc] init];
//    [self.view addSubview:verifiedUsers];
//    UIBlurEffect *verfiedeffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView *verifiedbluredView = [[UIVisualEffectView alloc] initWithEffect:verfiedeffect];
//    verifiedbluredView.frame = CGRectMake(0.0, 63.0 + 55.0 + 103.0-43 + self.view.frame.size.width/2, self.view.frame.size.width, 80.0);
//    verifiedbluredView.alpha = 0.9;
//    [self.view addSubview:verifiedbluredView];
//    lblText1 = [[UIButton alloc]initWithFrame:CGRectMake(0.0, 63.0 + 55.0 + 103.0-43 + self.view.frame.size.width/2, self.view.frame.size.width, 80.0)];
//    [lblText1 setTitle:@"Verfied Users" forState:UIControlStateNormal];
//    [lblText1 addTarget:self action:@selector(verifiedButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [lblText1 setTitleColor:[UIColor flatBlackColor] forState:UIControlStateNormal];
//    [self.view addSubview:lblText1];
    
    UIFont *Boldfont = [UIFont systemFontOfSize:12.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:Boldfont,NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil];
    [self.viewSegment setTitleTextAttributes:attributes
                                        forState:UIControlStateNormal];
    Boldfont = [UIFont systemFontOfSize:13.0f];
    attributes = [NSDictionary dictionaryWithObjectsAndKeys:Boldfont,NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil];
    [self.viewSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateSelected];
    
   [self.viewSegment setDividerImage:[self imageWithColor:[UIColor clearColor]] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [self.viewSegment setBackgroundImage:[self imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [self.viewSegment setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:160/255.0 green:31/255.0 blue:57/255.0 alpha:1.0]] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    
    imgArrow.frame=CGRectMake(kViewWidth/2 - 60, 68,15,12);
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
    NSArray *array_spinner =@[[UIImage imageNamed:@"feedstate-spinner-frame01.png"],[UIImage imageNamed:@"feedstate-spinner-frame02.png"],[UIImage imageNamed:@"feedstate-spinner-frame03.png"],[UIImage imageNamed:@"feedstate-spinner-frame04.png"],[UIImage imageNamed:@"feedstate-spinner-frame05.png"],[UIImage imageNamed:@"feedstate-spinner-frame06.png"],[UIImage imageNamed:@"feedstate-spinner-frame07.png"],[UIImage imageNamed:@"feedstate-spinner-frame08.png"]];
    
    
    self.img_spinner.animationImages =array_spinner;
    self.img_spinner.animationRepeatCount = HUGE_VAL;
    self.img_spinner.animationDuration=1.0f;
    
}

- (BOOL)isForceTouchAvailable
{
    return [self respondsToSelector:@selector(traitCollection)] &&
    [self.traitCollection respondsToSelector:@selector(forceTouchCapability)] &&
    self.traitCollection.forceTouchCapability == UIForceTouchCapabilityAvailable;
}

#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
      
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}



- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (void) imagePager:(JOLImageSlider *)imagePager didSelectImageAtIndex:(NSUInteger)index {
    NSLog(@"Selected slide at index: %lu", (unsigned long)index);
    if (index == 0)
    {
        [self.navigationController pushViewController:firstAdsView animated:YES];
    }
    else if (index == 1)
    {
        [self.navigationController pushViewController:secondAdsView animated:YES];
    }
    else
    {
        [self.navigationController pushViewController:thirdAdsView animated:YES];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [self btn_search_show_click:self];
    return NO;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    if (is_iPhone_5) {
        self.tbl_users.frame = CGRectMake(0,88,self.view.frame.size.width,self.view.bounds.size.height - 115);
        self.tbl_hashtag.frame = CGRectMake(0,88,self.view.frame.size.width,self.view.bounds.size.height - 115);
        self.collectionViewObj.frame = CGRectMake(0,61 + 55,self.view.frame.size.width,100);
//        topLikesButton.frame = CGRectMake(0.0, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2);
//        freshButton.frame = CGRectMake(self.view.frame.size.width/2, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2);
    }
    else {
        self.tbl_users.frame = CGRectMake(0, 82, kViewWidth, self.view.bounds.size.height - 60);
        self.tbl_hashtag.frame = CGRectMake(0, 82, kViewWidth, self.view.bounds.size.height - 60);
        self.collectionViewObj.frame = CGRectMake(0,63 + 55,kViewWidth,100);
//        topLikesButton.frame = CGRectMake(0.0, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2);
//        freshButton.frame = CGRectMake(self.view.frame.size.width/2, 63.0 + 55.0 + 103.0-43, self.view.frame.size.width/2, self.view.frame.size.width/2);
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self refreshFeeds];
    
    enter_mode = NO;
    [self get_search_user];
}

//- (void)topLikesButtonClicked:(UIButton *)sender
//{
//    SearchPostsViewController *searchPostsView = [[SearchPostsViewController alloc] initWithNibName:@"SearchPostsViewController" bundle:nil];
//    searchPostsView.posts = topLikes;
//    searchPostsView.getFeedKind = 0;
//    searchPostsView.delegate = self;
//    searchPostsView.viewTitleString = @"Today's Top Likes";
//    [self.navigationController pushViewController:searchPostsView animated:YES];
//}
//
//- (void)freshButtonClicked:(UIButton *)sender
//{
//    SearchPostsViewController *searchPostsView = [[SearchPostsViewController alloc] initWithNibName:@"SearchPostsViewController" bundle:nil];
//    searchPostsView.delegate = self;
//    searchPostsView.posts = freshs;
//    searchPostsView.getFeedKind = 1;
//    searchPostsView.viewTitleString = @"Fresh Posts";
//    [self.navigationController pushViewController:searchPostsView animated:YES];
//}
//
//- (void)verifiedButtonClicked:(UIButton *)sender
//{
//    [self.navigationController pushViewController:self.verifieduser_info_view animated:YES];
//
//    NSLog(@"goto Verified Users");
//}

#pragma search posts view controller delegate

- (void)backedFromView:(SearchPostsViewController *)viewController
{
    viewController.delegate = nil;
    viewController = nil;
}

- (void)refreshFeeds
{
    [self.collectionViewObj reloadData];
    
    topLikesButton.titleLabel.textColor = [UIColor blackColor];
    [topLikesButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    
    freshButton.titleLabel.textColor = [UIColor blackColor];
    [freshButton setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    
    verifiedUsers.titleLabel.textColor = [UIColor blackColor];
    [verifiedUsers setBackgroundImage:[[UIImage alloc] init] forState:UIControlStateNormal];
    
    getFeedKind = 0;// top likes
    [self get_search_feed];
}

- (IBAction)btn_refresh_click:(id)sender
{
    [self refreshFeeds];
}

- (IBAction)btn_click_verified_users:(id)sender {
}

-(IBAction)btn_fillter_clikc:(id)sender{

    if (self.view_fillter.hidden) {
        self.view_fillter.hidden =NO;
    }else{
    self.view_fillter.hidden =YES;
    }
    [self.view bringSubviewToFront:self.view_fillter];
}

-(IBAction)btn_out_side_click:(id)sender{

    [self.view_fillter setHidden:YES];
}

- (IBAction)btn_most_recent_click:(id)sender{
    self.lbl_header.text =@"Most Recent";
    self.view_fillter.hidden = YES;

    [self.array_search removeAllObjects];
    [self.collectionViewObj reloadData];
    [self get_search_feed];
}

- (IBAction)btn_top_rated_click:(id)sender{
    self.lbl_header.text =@"Top Rated";
    self.view_fillter.hidden = YES;

    [self.array_search removeAllObjects];
    [self.collectionViewObj reloadData];
    [self get_search_feed];
}

#pragma mark - Get News Feed
- (void)get_search_feed {
    if (getFeedKind == 0)
    {
        //[SVProgressHUD show];
    }
    _btn_reload.enabled = NO;

    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = @"";
    NSLog(@"TEXT:%@",self.lbl_header.text);
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    
    if (getFeedKind == 1) {
        requestStr =[NSString stringWithFormat:@"%@get_random_images.php?%@",[[Singleton sharedSingleton] getBaseURL], postString];
    }
    else {
        requestStr =[NSString stringWithFormat:@"%@get_top_images.php?%@",[[Singleton sharedSingleton] getBaseURL], postString];
    }
    
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:sig, @"sign",salt, @"salt",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",nil];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
        _btn_reload.enabled = YES;
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            [self performWithEmptyResponse];
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            if (getFeedKind == 0)
                topLikes = [[NSMutableArray alloc] init];
            else
                freshs = [[NSMutableArray alloc] init];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.price = dict[@"price"];
                shareObj.currency = dict[@"currency"];
                shareObj.sold = [dict[@"sold"] integerValue];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        [shareObj.array_comments addObject:obj];
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (getFeedKind == 0)
                    [topLikes addObject:shareObj];
                else
                    [freshs addObject:shareObj];
                
            }
        }
        
        if (getFeedKind == 0)
        {
            if (topLikes.count > 0)
            {
                Home_tableview_data_share *shareObj = [topLikes objectAtIndex:0];
                NSURL *url = [NSURL URLWithString:shareObj.image_path];
                [topLikesButton sd_setBackgroundImageWithURL:url forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    topLikesButton.titleLabel.textColor = [UIColor whiteColor];
                }];
                
                [TopLikesText setTitle:@"Top Likes" forState:UIControlStateNormal];
            }
            else
            {
                [self performWithEmptyResponse];
            }
            
            getFeedKind = 1;
            [self get_search_feed];
        }
        else
        {
            if (freshs.count > 0)
            {
                Home_tableview_data_share *shareObj = [freshs objectAtIndex:0];
                NSURL *url = [NSURL URLWithString:shareObj.image_path];
                [freshButton sd_setBackgroundImageWithURL:url forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    freshButton.titleLabel.textColor = [UIColor whiteColor];
                }];
                
                [lblText setTitle:@"New" forState:UIControlStateNormal];
            }
            else
            {
                [self performWithEmptyResponse];
            }
            
            [SVProgressHUD dismiss];
        }
        
        if (self.view_search.isHidden)
            [self changeContentView:nil];
        
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        _btn_reload.enabled = YES;
        [self performWithEmptyResponse];
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (void)performWithEmptyResponse
{
    if (getFeedKind == 0)
    {
        [TopLikesText setTitle:@"No new posts with likes for today" forState:UIControlStateNormal];
    }
    else
    {
        [lblText setTitle:@"No new" forState:UIControlStateNormal];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == self.postCollectionView)
        return 1;
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (view == self.postCollectionView) {
        return self.posts.count;
    }
    return self.array_search.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.postCollectionView) {
        return CGSizeMake((kViewWidth-2)/3, (kViewWidth-2)/3);
    }
    return CGSizeMake((collectionView.frame.size.width-15)/4, (collectionView.frame.size.width-5)/4);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if (cv == self.postCollectionView) {
        //static NSString *CellIdentifier = @"image_collection_cell";
        //image_collection_cell *cell = [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        image_collection_cell *cell = [cv dequeueReusableCellWithReuseIdentifier:NSStringFromClass([image_collection_cell class]) forIndexPath:indexPath];
        if (!cell.popupPreviewRecognizer) {
            cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
        }
        UIImageView *imageView = (UIImageView *)cell.img_photo;
        
        //    if (imageView == nil)
        //    {
        //        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 105)];
        //        imageView.userInteractionEnabled = NO;
        //        imageView.tag = 10000;
        //    }
    
        Home_tableview_data_share *shareObj = [self.posts objectAtIndex:indexPath.row];
        cell.data = shareObj;
        
        if (!self.image_detail_viewObj.arrayFeedArray.count) {
            self.image_detail_viewObj.arrayFeedArray = [self.posts mutableCopy];
        }
        
        NSLog(@"%@", shareObj.image_path);
        [imageView sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:[UIImage imageNamed:@"default_user_image"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                imageView.image = image;
            }
        }];
        
        NSString *strPrice = @"";
        
        if ( shareObj.price.length != 0 )
            strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
        
        if ([strPrice isEqualToString:@"(null)(null)"])
            cell.lbl_price.text = @"";
        else
            cell.lbl_price.text = strPrice;
        
        if ( [cell.lbl_price.text isEqualToString:@""] )
            cell.lbl_price.hidden = YES;
        else
            cell.lbl_price.hidden = NO;
        
        cell.tag = indexPath.row;
        return cell;
    }
    
    
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.img_photo.layer.borderWidth =1.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
//    Home_tableview_data_share *shareObj =[self.array_search objectAtIndex:indexPath.row];
//    [cell.img_photo setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    cell.img_photo.image = [UIImage imageNamed:self.array_search[indexPath.row]];
    
    cell.lbl_price.hidden = YES;

    cell.tag = indexPath.row;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == self.postCollectionView) {
        self.image_detail_viewObj.shareObj =[self.posts objectAtIndex:indexPath.row];
        
        self.image_detail_viewObj = [[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
        
        Home_tableview_data_share *sh = [self.posts objectAtIndex:indexPath.row];
        NSLog(@"PassedImage:%@", sh.image_id);
        
        self.image_detail_viewObj.arrayFeedArray=[self.posts mutableCopy];
        self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
        self.image_detail_viewObj.promotion = NO;
        
        [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];

    } else {
        if (indexPath.row == 0)
        {
            [self.navigationController pushViewController:firstAdsView animated:YES];
        }
        else if (indexPath.row == 1)
        {
            [self.navigationController pushViewController:secondAdsView animated:YES];
        }
        else
        {
            [self.navigationController pushViewController:thirdAdsView animated:YES];
        }
        //    self.image_detail_viewObj.shareObj =[self.array_search objectAtIndex:indexPath.row];
        //
        //    Home_tableview_data_share *sh=[self.array_search objectAtIndex:indexPath.row];
        //    NSLog(@"PassedImage:%@",sh.image_id);
        //
        //    self.image_detail_viewObj.arrayFeedArray=self.array_search;
        //    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
        //    self.image_detail_viewObj.promotion = NO;
        //    
        //    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
    }
}

#pragma mark - Search view hide show
-(IBAction)btn_search_show_click:(id)sender{
    self.postCollectionView.hidden = YES;
    self.verifyUsersTableView.hidden = YES;
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[JOLImageSlider class]]) { // Condition
            JOLImageSlider *customView = (JOLImageSlider *)subView;
            [customView setHidden:YES];
        }
    }
    for (UIView *subView1 in self.view.subviews) {
        if ([subView1 isKindOfClass:[UIVisualEffectView class]]) { // Condition
            subView1.hidden = YES;
        }
    }
    self.view_search.hidden=NO;
    self.view_header.hidden = YES;
    freshButton.hidden = YES;
    TopLikesText.hidden = YES;
    lblText.hidden = YES;
    topLikesButton.hidden = YES;
    verifiedUsers.hidden = YES;
    lblText1.hidden = YES;
    [self.txt_search becomeFirstResponder];
    
    [self.array_search_result removeAllObjects];
    [tbl_users reloadData];

    [dict_hashposts removeAllObjects];
    [tbl_hashtag reloadData];
}

-(IBAction)btn_search_cancel_click:(id)sender{
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[JOLImageSlider class]]) { // Condition
            JOLImageSlider *customView = (JOLImageSlider *)subView;
            [customView setHidden:NO];
        }
    }
    
    for (UIView *subView1 in self.view.subviews) {
        if ([subView1 isKindOfClass:[UIVisualEffectView class]]) { // Condition
            subView1.hidden = NO;
        }
    }
    self.view_search.hidden=YES;
    self.view_header.hidden = NO;
    freshButton.hidden = NO;
    TopLikesText.hidden = NO;
    lblText.hidden = NO;
    topLikesButton.hidden = NO;
    verifiedUsers.hidden = NO;
    lblText1.hidden = NO;
    self.txt_search.text=@"";
    [self.view_search endEditing:YES];
    [self changeContentView:nil];
}

-(IBAction)segment_value_change:(id)sender{

    if (self.segment.selectedSegmentIndex==0) {
        imgsegmentusers.hidden = NO;
        imgsegmenthashtags.hidden = YES;
        
        self.txt_search.placeholder=@"Search for a user";
        self.tbl_users.hidden =NO;
        self.tbl_hashtag.hidden = YES;
        
        imgArrow.frame=CGRectMake(kViewWidth/2 - 60, 68,15,12);
        
    }else{
        imgsegmentusers.hidden = YES;
        imgsegmenthashtags.hidden = NO;
        
        self.txt_search.placeholder=@"Search for Hashtags";
        self.txt_search.text =[self.txt_search.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.tbl_users.hidden =YES;
        self.tbl_hashtag.hidden = NO;
        
        
        imgArrow.frame=CGRectMake(kViewWidth/2 + 45,68,15,12);
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self updateTextLabelsWithText: newString];
    
   if (self.segment.selectedSegmentIndex==1) {
       if ([string isEqualToString:@" "]) {
           return NO;
       }
    }
    return YES;
}

-(void)updateTextLabelsWithText:(NSString *)string
{
    NSArray *array =self.array_all_users;
    Comment_share *obj;
    
    [self.array_search_result removeAllObjects];
    
    for ( int i=0;i<[array count];i++) {
        obj = [array objectAtIndex:i];
        if ([obj.name containsString:string] || [obj.username containsString:string])
        {
            [self.array_search_result addObject:[array objectAtIndex:i]];
        }
    }
    
    [self.tbl_users reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    enter_mode = YES;
    if (self.segment.selectedSegmentIndex!=0)
        [self get_search_user];
    return YES;
}

#pragma mark - Get History Data from Database
-(void)get_history_data_from_db{

}

#pragma mark - Get Search user call
-(void)get_search_user{
    [txt_search resignFirstResponder];

    //if ([self.txt_search.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length >0) {
        [SVProgressHUD dismiss];
        [SVProgressHUD show];
        NSString *requestStr=@"";
        NSString *notificationVal=@"";
        if (self.segment.selectedSegmentIndex==1){
            NSLog(@"search for Hash Tag");
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getHashUsernameAPIResponce:) name:@"14231" object:nil];
        
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetHashUsernameAPIResponce:) name:@"-14231" object:nil];
        
            requestStr = [NSString stringWithFormat:@"%@get_hash_username.php",[[Singleton sharedSingleton] getBaseURL]];
            notificationVal=@"14231";
    }
    else{
        NSLog(@" search for username");
        requestStr = [NSString stringWithFormat:@"%@search_users.php",[[Singleton sharedSingleton] getBaseURL]];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchUsersResponce:) name:@"14232" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsearchUsersResponce:) name:@"-14232" object:nil];
        
        notificationVal=@"14232";
    }
    
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        NSLog(@"requestStr:%@",requestStr);

        NSDictionary *params;
        if (enter_mode == YES)
            params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                //self.txt_search.text,@"key",
                                @"",@"key",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                nil];
        else
            params = [NSDictionary dictionaryWithObjectsAndKeys:
                      sig, @"sign",
                      salt, @"salt",
                      @"",@"key",
                      [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                      nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :notificationVal :params];
        
        if (enter_mode == YES)
            enter_mode = NO;
    //}
}

-(void)getHashUsernameAPIResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    _btn_reload.enabled = YES;
    

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_hashtag_result removeAllObjects];
        
        NSArray *temarray =[result valueForKey:@"data"];
        dict_hashposts =[NSMutableDictionary dictionary];
        
        if ([temarray count]==0) {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"No records found, Try diffrent words." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        if ([temarray count]>=1) {
            NSUInteger index = 1;
//            if ([[self.txt_search.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
//                 return;
            
            NSString *key = self.txt_search.text;
            key = [key lowercaseString];
            NSMutableDictionary *posts = [NSMutableDictionary dictionary];
            
            for ( NSDictionary *dict in temarray )
            {
                NSString *str = [StaticClass urlDecode:dict[@"desc"]];

                str = [str lowercaseString];
                NSString *imageid = [StaticClass urlDecode:dict[@"imageid"]];
                
                //NSLog(@"%@, [%@]", imageid, str);
                
                NSArray *ary = [str componentsSeparatedByString:@" "];
                
                for ( NSString *str1 in ary )
                {
                    if ( [str1 containsString:key] )
                    {
                        if ( posts[str1] == nil )
                            posts[str1] = [NSMutableDictionary dictionary];
                        
                        posts[str1][imageid] = imageid;
                        
                        //NSLog(@"%@ %d, %@", str1, [posts[str1] allKeys].count, imageid);
                    }
                }
            }
            
            NSMutableDictionary *posts1 = [NSMutableDictionary dictionary];
            
            for ( NSString *str in posts.allKeys )
            {
                NSLog(@"[%@]", str);
                NSString *tagCharacter = [str substringToIndex:1];
                
                if (![tagCharacter isEqualToString:@"#"])
                    continue;

                posts1[str] = [NSMutableDictionary dictionary];

                for ( NSString *str1 in posts.allKeys )
                {
                    NSLog(@"111->%@", str1);
                    
                    if ( [str1 containsString:str] )
                    {
                        for ( NSString *str2 in [posts[str1] allKeys] )
                        {	
                            NSLog(@"222->%@", str2);
                            posts1[str][str2] = str2;
                        }
                    }
                }
            }
            
            dict_hashposts = posts1;
            
//            for (NSDictionary *dict in temarray) {
//              HashtagSearchRecord *hashtagRecord =[[HashtagSearchRecord alloc]init];
//                hashtagRecord.objComment =[StaticClass urlDecode:[dict valueForKey:@"comment"]];
////                hashtagRecord.objImageCaption=[StaticClass urlDecode:[dict valueForKey:@"image_caption"]];
//                hashtagRecord.objImageCaption=[StaticClass urlDecode:[dict valueForKey:@"uid"]];
//                hashtagRecord.objImagePath=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
//                hashtagRecord.objIndex = index;
//                NSLog(@"---------- %lu", index);
//                [self.array_hashtag_result addObject:hashtagRecord];
//                index++;
//            }
        }
        else {
            for (NSString *dict in temarray) {
                NSLog(@"DICT:%@",dict);
                [self.array_hashtag_result addObject:[StaticClass urlDecode:dict]];
            }
        }
        
        NSLog(@"COUNT:%ld", (long)[self.array_hashtag_result count]);
        [self.tbl_hashtag reloadData];
    }
}

-(void)FailgetHashUsernameAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14231" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14231" object:nil];
    
    [SVProgressHUD dismiss];
    _btn_reload.enabled = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)searchUsersResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    _btn_reload.enabled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        //[self.array_search_result removeAllObjects];
        [self.array_all_users removeAllObjects];
        NSArray *temarray =[result valueForKey:@"data"];
        for (NSDictionary *dict in temarray) {
            Comment_share *obj =[[Comment_share alloc]init];
            obj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            obj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            obj.image_url=[StaticClass urlDecode:[dict valueForKey:@"image"]];
            obj.uid =[dict valueForKey:@"user_id"];
            //[self.array_search_result addObject:obj];
            [self.array_all_users addObject:obj];
        }
        [self.tbl_users reloadData];
    }
    else if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"No records are found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)FailsearchUsersResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14232" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14232" object:nil];
    
    [SVProgressHUD dismiss];
    _btn_reload.enabled = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UITableview Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.verifyUsersTableView)
        return 1;
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView ==self.tbl_users) {
        return self.array_search_result.count;
    }else if (tableView == self.verifyUsersTableView){
        return array_verified_users.count;
    }else{

        return dict_hashposts.allKeys.count;
//        return self.array_hashtag_result.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tbl_users) {
        return 55.0f;
    } if (tableView == self.verifyUsersTableView){
        NSInteger indCount = indexPath.row;
        Comment_share *obj = [array_verified_users objectAtIndex:indCount];
        
        CGSize size =[obj.name sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16.0] constrainedToSize:CGSizeMake(255, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
        
        if (size.height<25) {
            return 58.0f;
        }
        return 30.0f+size.height+20.0f;
    }
    else{
        return 44.0f;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView==self.tbl_users) {
        
        static NSString *identifier =@"search_view_cell";
        search_view_cell *cell=(search_view_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"search_view_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        Comment_share *shareObj =[self.array_search_result objectAtIndex:indexPath.row];
        cell.lbl_username.text =shareObj.username;
        cell.lbl_name.text = shareObj.name;
        
        cell.img_user.layer.cornerRadius = 22.0;
        cell.img_user.layer.masksToBounds=YES;
        
        [cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
       // cell.img_user.imageURL =[NSURL URLWithString:shareObj.image_url];
        return cell;
    }else if (tableView == self.verifyUsersTableView) {
        UITableViewCell *cell;
        cell = [self Inbox_list_CellForTableView:tableView atIndexPath:indexPath];
        
        return cell;
    }else{
        
        static NSString *identifier =@"search_view_hashtag_cell";
        search_view_hashtag_cell *cell=(search_view_hashtag_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"search_view_hashtag_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UITapGestureRecognizer *gestureImageCaption=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ImageCaptionGestureAction:)];
            [gestureImageCaption setNumberOfTapsRequired:1];
            [cell.lbl_ImageCaption addGestureRecognizer:gestureImageCaption];
            
            UITapGestureRecognizer *gestureComment=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(CommentGestureAction:)];
            [gestureComment setNumberOfTapsRequired:1];
            [cell.lbl_title addGestureRecognizer:gestureComment];
        }
//        NSLog(@"[self.array_hashtag_result objectAtIndex:indexPath.row]:%@",[self.array_hashtag_result objectAtIndex:indexPath.row]);
//        HashtagSearchRecord *objHashtagRecord =[self.array_hashtag_result objectAtIndex:indexPath.row];
        //cell.lbl_ImageCaption.text = [NSString stringWithFormat: @"%lu. %@", objHashtagRecord.objIndex,objHashtagRecord.objImageCaption];
        
//        cell.lbl_ImageCaption.text=objHashtagRecord.objImageCaption;
//        cell.lbl_title.text =objHashtagRecord.objComment;
        
//        cell.lbl_ImageCaption.text=[NSString stringWithFormat:@"#%@", dict_hashposts.allKeys[indexPath.row]];
        cell.lbl_ImageCaption.text=[NSString stringWithFormat:@"%@", dict_hashposts.allKeys[indexPath.row]];
        cell.lbl_title.text =[NSString stringWithFormat:@"%lu posts", [[dict_hashposts[dict_hashposts.allKeys[indexPath.row]] allKeys] count]];

        
//        NSLog(@"index:%lu, caption:%@, title:%@", objHashtagRecord.objIndex, cell.lbl_ImageCaption.text, cell.lbl_title.text);
        
        cell.image_photo.image = [UIImage imageNamed:@"Hashtag-img.png"];
        
//        if ( [objHashtagRecord.objImagePath isEqualToString:@""] )
//        {
//            cell.image_photo.image = nil;
//        }
//        else
//        {
//            [cell.image_photo sd_setImageWithURL:[NSURL URLWithString:objHashtagRecord.objImagePath]  placeholderImage:nil];
//        }
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==self.verifyUsersTableView) {
        Comment_share *shareObj =[array_verified_users objectAtIndex:indexPath.row];
        self.user_info_view.user_id = shareObj.uid;
        [self.navigationController pushViewController:self.user_info_view animated:YES];
    } else if (tableView==self.tbl_users) {
        Comment_share *shareObj =[self.array_search_result objectAtIndex:indexPath.row];
        self.user_info_view.user_id = shareObj.uid;
        [self.navigationController pushViewController:self.user_info_view animated:YES];
    }
}
-(void)ImageCaptionGestureAction:(UIGestureRecognizer*)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_hashtag];
    NSIndexPath *tappedRow = [self.tbl_hashtag indexPathForRowAtPoint:touchLocation];

    self.hash_tag_viewObj.str_title =[dict_hashposts.allKeys objectAtIndex:tappedRow.row];
    [self.navigationController pushViewController:self.hash_tag_viewObj animated:YES];

//    HashtagSearchRecord*objHashtagRecord=[self.array_hashtag_result objectAtIndex:tappedRow.row];
//    if(![objHashtagRecord.objImageCaption isEqualToString:@""])
//    {
//        self.hash_tag_viewObj.str_title =[objHashtagRecord.objImageCaption substringFromIndex:0];
//        [self.navigationController pushViewController:self.hash_tag_viewObj animated:YES];
//    }
}
-(void)CommentGestureAction:(UIGestureRecognizer*)gesture
{
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_hashtag];
    NSIndexPath *tappedRow = [self.tbl_hashtag indexPathForRowAtPoint:touchLocation];
    
    self.hash_tag_viewObj.str_title =[dict_hashposts.allKeys objectAtIndex:tappedRow.row];

    [self.navigationController pushViewController:self.hash_tag_viewObj animated:YES];

//    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_hashtag];
//    NSIndexPath *tappedRow = [self.tbl_hashtag indexPathForRowAtPoint:touchLocation];
//    HashtagSearchRecord*objHashtagRecord=[self.array_hashtag_result objectAtIndex:tappedRow.row];
//    if(![objHashtagRecord.objImageCaption isEqualToString:@""])
//    {
//     self.hash_tag_viewObj.str_title =[objHashtagRecord.objImageCaption substringFromIndex:0];
//    [self.navigationController pushViewController:self.hash_tag_viewObj animated:YES];
//    }
  
}
- (IBAction)changeContentView:(id)sender {
    self.verifyUsersTableView.hidden = YES;
    self.postCollectionView.hidden = YES;
    if (self.viewSegment.selectedSegmentIndex==0) {
//        SearchPostsViewController *searchPostsView = [[SearchPostsViewController alloc] initWithNibName:@"SearchPostsViewController" bundle:nil];
//        searchPostsView.delegate = self;
//        searchPostsView.posts = freshs;
//        searchPostsView.getFeedKind = 1;
//        searchPostsView.viewTitleString = @"Fresh Posts";
//        [self.navigationController pushViewController:searchPostsView animated:YES];
        self.postCollectionView.hidden = NO;
        TopLikesText.hidden = YES;
        self.posts = freshs;
        [self.postCollectionView reloadData];
        
    } else if (self.viewSegment.selectedSegmentIndex==1) {
//        SearchPostsViewController *searchPostsView = [[SearchPostsViewController alloc] initWithNibName:@"SearchPostsViewController" bundle:nil];
//        searchPostsView.posts = topLikes;
//        searchPostsView.getFeedKind = 0;
//        searchPostsView.delegate = self;
//        searchPostsView.viewTitleString = @"Today's Top Likes";
//        [self.navigationController pushViewController:searchPostsView animated:YES];
        self.postCollectionView.hidden = NO;
        TopLikesText.hidden = NO;
        self.posts = topLikes;
        [self.postCollectionView reloadData];
    } else {
        //[self.navigationController pushViewController:self.verifieduser_info_view animated:YES];
        self.verifyUsersTableView.hidden = NO;
        TopLikesText.hidden = YES;
        [array_verified_users removeAllObjects];
        [self get_verifiedusers_info];
    }
}


- (search_view_cell *)Inbox_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VerifiedUsers_list_cell";
    self.objVerifiedUsers_list_cell = (search_view_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(self.objVerifiedUsers_list_cell == nil)
    {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"search_view_cell" owner:self options:nil];
        self.objVerifiedUsers_list_cell = [nib objectAtIndex:0];
        self.objVerifiedUsers_list_cell.showsReorderControl = NO;
        self.objVerifiedUsers_list_cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.objVerifiedUsers_list_cell.backgroundColor =[UIColor whiteColor];
    }
    
    Comment_share *shareObj = [array_verified_users objectAtIndex:indexPath.row];
    
    self.objVerifiedUsers_list_cell.img_user.tag = indexPath.row;
    [self.objVerifiedUsers_list_cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
    
    self.objVerifiedUsers_list_cell.img_user.layer.cornerRadius =22.0f;
    self.objVerifiedUsers_list_cell.img_user.layer.masksToBounds =YES;
    self.objVerifiedUsers_list_cell.lbl_username.text = shareObj.username;
    self.objVerifiedUsers_list_cell.lbl_name.text=shareObj.name;
    
    self.objVerifiedUsers_list_cell.tag=indexPath.row;
    
    return self.objVerifiedUsers_list_cell;
}

-(void)addVerifiedUsersData:(NSString*)uid username:(NSString*)UserName name:(NSString*)Name image_url:(NSString*)imageURL
{
    Comment_share *user_data = [[Comment_share alloc]init];
    user_data.uid = uid;
    user_data.username = UserName;
    user_data.name = Name;
    user_data.image_url=imageURL;
    
    [array_verified_users addObject:user_data];
}

#pragma mark - Get verified users info from server

-(void)get_verifiedusers_info{
    [SVProgressHUD dismiss];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"29209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getVerifiedUsersAPIResponce:) name:@"29209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-29209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildgetVerifiedUsersAPIResponce:) name:@"-29209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_verified_users.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"29209" :params];
}

-(void)getVerifiedUsersAPIResponce:(NSNotification *)notification {
    
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"29209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-29209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        if ([dict count] == 0 || dict == nil)
        {
            return;
        }
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        
        for (int i = 0; i< [dict count]; i++) {
            NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
            
            NSString *userid = [temp objectForKey:@"id"];
            NSString *UserName = [temp objectForKey:@"username"];
            NSString *Name = [temp objectForKey:@"name"];
            NSString *image=[StaticClass urlDecode:[temp objectForKey:@"image"]];
            
            [self addVerifiedUsersData:userid username:UserName name:Name image_url:image];
        }
        
        [self.verifyUsersTableView reloadData];
    }
}

-(void)FaildgetVerifiedUsersAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19209" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
