//
//  Likers_list_cell.h
//  My Style
//
//  Created by Tis Macmini on 4/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Likers_list_cell : UITableViewCell{
    UIImageView *img_user;
    UILabel *lbl_name;
    UILabel *lbl_decs;
    
}
@property(nonatomic,strong)IBOutlet UIImageView *img_user;
@property(nonatomic,strong)IBOutlet UILabel *lbl_name;
@property(nonatomic,strong)IBOutlet UILabel *lbl_decs;
@end
