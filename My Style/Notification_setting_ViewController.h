//
//  Notification_setting_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification_setting_cell.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "Notification_setting_share.h"
#import "Notification_setting_on_off_cell.h"
#import "DCRoundSwitch.h"

typedef struct
{
   __unsafe_unretained NSString *title;
    int is_checked;
   __unsafe_unretained NSString *notification_type;
}notification_type;

@interface Notification_setting_ViewController : UIViewController{

    UITableView *tbl_notification;
    NSMutableArray *array_notification;
    Notification_setting_on_off_cell *not_on_off_Obj;
    UILabel *lblheader;
    UIView *tbl_footer;
    UIButton *btnSave;
    NSMutableDictionary *dict_notification;
}
@property(nonatomic,strong)IBOutlet  UITableView *tbl_notification;
@property(nonatomic,strong)NSMutableArray *array_notification;
@property(nonatomic,strong)IBOutlet UIView *tbl_footer;
@property(nonatomic,strong)IBOutlet UILabel *lblheader;
@property(nonatomic,strong)NSMutableDictionary *dict_notification;
@property(nonatomic,strong)IBOutlet UIButton *btnSave;

-(IBAction)btnSaveClick:(id)sender;

@end
