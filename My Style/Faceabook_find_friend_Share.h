//
//  Faceabook_find_friend_Share.h
//  My Style
//
//  Created by Tis Macmini on 4/15/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Faceabook_find_friend_Share : NSObject{

    NSString *email;
    NSString *facebook_id;
    NSString *username;
    NSString *user_id;
    NSString *name;
    NSString *url_image;
    NSString *is_following;
}
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *facebook_id;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *url_image;
@property(nonatomic,strong) NSString *is_following;
@end
