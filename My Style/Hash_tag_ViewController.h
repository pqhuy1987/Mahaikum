//
//  Hash_tag_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 6/3/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_cell.h"
#import "BlockActionSheet.h"
#import "Likers_list_ViewController.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "Comment_share.h"
#import "Comments_list_ViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Share_photo.h"

#import "image_collection_cell.h"
#import "image_collection_header.h"
#import "Image_detail.h"

@interface Hash_tag_ViewController : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate>{
    
    UITableView *tbl_news;
    Likers_list_ViewController *liker_list_viewObj;
    Image_detail *image_detail_viewObj;
    
    NSMutableArray *array_feeds;
    
    NSInteger which_image_liked;
    NSInteger which_image_delete;
    
    UIImageView *img_like_heart;
    Comments_list_ViewController *comments_list_viewObj;
    Share_photo *share_photo_view;
    
    UICollectionView *collectionViewObj;
    IBOutlet UIView *view_header;
    IBOutlet UIView *view_header_tbl;
    
    UIImageView *img_down_arrow;
    UIImageView *img_down_arrow_tbl;
    
    UIImageView *img_header_bg;
    UIImageView *img_header_bg_tbl;
    
    UILabel *lbl_title;
    NSString *str_title;
    
    UILabel *lbl_photos_count;
    UILabel *lbl_photos_count_tbl;
    
    
}
@property(nonatomic,strong)IBOutlet UILabel *lbl_photos_count;
@property(nonatomic,strong)IBOutlet UILabel *lbl_photos_count_tbl;

@property(nonatomic,strong)IBOutlet UILabel *lbl_title;
@property(nonatomic,strong) NSString *str_title;
@property(nonatomic,strong)IBOutlet UIImageView *img_header_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_header_bg_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow_tbl;

@property(nonatomic,strong) IBOutlet UIView *view_header;
@property(nonatomic,strong) IBOutlet UIView *view_header_tbl;
@property(nonatomic,strong)IBOutlet UICollectionView *collectionViewObj;

@property(nonatomic,strong)Share_photo *share_photo_view;

@property(nonatomic,strong)Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,strong)Image_detail *image_detail_viewObj;
@property(nonatomic,strong)IBOutlet  UIImageView *img_like_heart;

@property(nonatomic,strong)IBOutlet UITableView *tbl_news;
@property(nonatomic,strong)Likers_list_ViewController *liker_list_viewObj;
@property(nonatomic,strong)NSMutableArray *array_feeds;
@property (nonatomic, strong) Share_photo_ViewController *share_photo_viewObj;

@end
