//
//  Followers_list.h
//  My Style
//
//  Created by Tis Macmini on 5/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Followers_list_cell.h"
#import "Faceabook_find_friend_Share.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"

@interface Followers_list : UIViewController
{
    NSString *user_id;
    NSString *nav_title;
    UILabel *lbl_title;
    
    UITableView *tbl_followers;
    NSMutableArray *array_folloewrs;
    
    UILabel *lblSpinnerBg;
    UIActivityIndicatorView *activity;
}

@property(nonatomic,strong)NSString *nav_title;
@property(nonatomic,strong)NSString *user_id;
@property(nonatomic,strong) IBOutlet  UILabel *lbl_title;
@property(nonatomic,strong) IBOutlet UITableView *tbl_followers;
@property(nonatomic,strong) NSMutableArray *array_folloewrs;
@property(nonatomic) BOOL categoryViewed;
@property(nonatomic,strong) CategoryObject *category;

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong) UIActivityIndicatorView *activity;

@end
