//
//  VerifiedUsersViewController.m
//  Mahalkum
//
//  Created by user on 7/12/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VerifiedUsersViewController.h"
#import "Inbox_info.h"
#import "Inbox_list_cell.h"

@interface VerifiedUsersViewController()<AVTagTextViewDelegate,Inbox_list_CellDelegate> {
    NSMutableArray *array_verified_users;
}

@end

@interface VerifiedUsersViewController (PrivateStuff)

@end

@implementation VerifiedUsersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    array_verified_users = [[NSMutableArray alloc]init];
    
    self.user_info_view =[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
   self.lbl_title.text = @"Verified Users";
    
   if ([array_verified_users count])
        [array_verified_users removeAllObjects];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_verifiedusers_info];
}

#pragma mark - Get verified users info from server

-(void)get_verifiedusers_info{
    [SVProgressHUD dismiss];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"29209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getVerifiedUsersAPIResponce:) name:@"29209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-29209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildgetVerifiedUsersAPIResponce:) name:@"-29209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_verified_users.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"29209" :params];
}

-(void)getVerifiedUsersAPIResponce:(NSNotification *)notification {
    
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"29209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-29209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        if ([dict count] == 0 || dict == nil)
        {
            return;
        }
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        
        for (int i = 0; i< [dict count]; i++) {
            NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
            
            NSString *userid = [temp objectForKey:@"id"];
            NSString *UserName = [temp objectForKey:@"username"];
            NSString *Name = [temp objectForKey:@"name"];
            NSString *image=[StaticClass urlDecode:[temp objectForKey:@"image"]];
            
            [self addVerifiedUsersData:userid username:UserName name:Name image_url:image];
        }
        
        [self.usersTable reloadData];
    }
}

-(void)FaildgetVerifiedUsersAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19209" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}

-(void)addVerifiedUsersData:(NSString*)uid username:(NSString*)UserName name:(NSString*)Name image_url:(NSString*)imageURL
{
    Comment_share *user_data = [[Comment_share alloc]init];
    user_data.uid = uid;
    user_data.username = UserName;
    user_data.name = Name;
    user_data.image_url=imageURL;
    
    [array_verified_users addObject:user_data];
}

#pragma mark UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger indCount = indexPath.row;
    Comment_share *obj = [array_verified_users objectAtIndex:indCount];
    
    CGSize size =[obj.name sizeWithFont:[UIFont fontWithName:@"Helvetica" size:16.0] constrainedToSize:CGSizeMake(255, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
    
    if (size.height<25) {
        return 58.0f;
    }
    return 30.0f+size.height+20.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array_verified_users.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return Nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [self Inbox_list_CellForTableView:tableView atIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==self.usersTable) {
        Comment_share *shareObj =[array_verified_users objectAtIndex:indexPath.row];
        self.user_info_view.user_id = shareObj.uid;
        [self.navigationController pushViewController:self.user_info_view animated:YES];
    }
}

- (search_view_cell *)Inbox_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"VerifiedUsers_list_cell";
    self.objVerifiedUsers_list_cell = (search_view_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if(self.objVerifiedUsers_list_cell == nil)
    {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"search_view_cell" owner:self options:nil];
        self.objVerifiedUsers_list_cell = [nib objectAtIndex:0];
        self.objVerifiedUsers_list_cell.showsReorderControl = NO;
        self.objVerifiedUsers_list_cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.objVerifiedUsers_list_cell.backgroundColor =[UIColor whiteColor];
    }
    
    Comment_share *shareObj = [array_verified_users objectAtIndex:indexPath.row];
    
    self.objVerifiedUsers_list_cell.img_user.tag = indexPath.row;
    [self.objVerifiedUsers_list_cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
    
    self.objVerifiedUsers_list_cell.img_user.layer.cornerRadius =22.0f;
    self.objVerifiedUsers_list_cell.img_user.layer.masksToBounds =YES;
    self.objVerifiedUsers_list_cell.lbl_username.text = shareObj.username;
    self.objVerifiedUsers_list_cell.lbl_name.text=shareObj.name;
    
    self.objVerifiedUsers_list_cell.tag=indexPath.row;
    
    return self.objVerifiedUsers_list_cell;
}

@end
