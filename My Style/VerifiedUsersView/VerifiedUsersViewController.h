//
//  VerifiedUsersViewController.h
//  Mahalkum
//
//  Created by user on 7/12/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#ifndef VerifiedUsersViewController_h
#define VerifiedUsersViewController_h

#import "Comment_share.h"
#import "search_view_cell.h"
#import "User_info_ViewController.h"

#endif /* VerifiedUsersViewController_h */

@class VerifiedUsersViewController;

@protocol VerifiedUsersViewControllerDelegate <NSObject>

@optional


@end

@interface VerifiedUsersViewController : UIViewController<AVTagTextViewDelegate,UITextViewDelegate>
{
}
@property search_view_cell *objVerifiedUsers_list_cell;

@property(nonatomic,strong) User_info_ViewController *user_info_view;

@property (strong, nonatomic) IBOutlet UITableView *usersTable;

@property (strong, nonatomic) IBOutlet UILabel *lbl_title;

- (search_view_cell *)VerifiedUsers_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;

@end
