//
//  Home_tableview_data_share.h
//  My Style
//
//  Created by Tis Macmini on 4/19/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Home_tableview_data_share : NSObject{

    NSString *datecreated;
    NSString *description;
    NSString *image_id;
    NSString *image_owner;
    NSString *image_path;
    NSString *likes;
    NSString *lat;
    NSString *lng;
    NSString *location;
    NSString *uid;
    NSString *username;
    NSString *user_image;
    NSString *liked;
    NSString *liked_by;
    NSMutableArray *array_liked_by;
    NSMutableArray *array_comments;
    NSMutableArray *array_comments_tmp;
    NSMutableArray *array_comments_no_description;
    NSString *name;
    
    NSString *rating;
    NSString *my_rating;
    NSString *comment_count;
    
    NSString *avgrating;
    NSString *totalUser;
    BOOL blockStatus;
    
    NSString *category_id;
}
@property(nonatomic,strong) NSString *comment_count;
@property(nonatomic,strong) NSMutableArray *array_comments;
@property(nonatomic,strong) NSMutableArray *array_comments_tmp;
@property(nonatomic,strong) NSMutableArray *array_comments_no_description;
@property(nonatomic,strong) NSString *user_image;
@property(nonatomic,strong) NSString *datecreated;
@property(nonatomic,strong) NSString *description;
@property(nonatomic,strong) NSString *image_id;
@property(nonatomic,strong) NSString *image_owner;
@property(nonatomic,strong) NSString *image_path;
@property(nonatomic,strong) NSString *likes;
@property(nonatomic,strong) NSString *lat;
@property(nonatomic,strong) NSString *lng;
@property(nonatomic,strong) NSString *location;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic) NSInteger sold;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *liked;
@property(nonatomic,strong) NSString *liked_by;
@property(nonatomic,strong) NSMutableArray *array_liked_by;
@property(nonatomic,strong) NSString*name;

@property(nonatomic,strong) NSString *rating;
@property(nonatomic,strong) NSString *my_rating;

@property(nonatomic,strong) NSString *avgrating;
@property(nonatomic,strong) NSString *totalUser;
@property(nonatomic,assign) BOOL blockStatus;

@property (nonatomic, strong) NSString *category_id;

@end
