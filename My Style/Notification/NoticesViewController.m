//
//  NoticesViewController.m
//  Mahalkum
//
//  Created by user on 8/1/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import "NoticesViewController.h"
#import "PushNotification_info.h"
#import "LORichTextLabel.h"

#import "Hash_tag_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "NoticesTableViewCell.h"

@interface NoticesViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *array_items;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation NoticesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    array_items = [NSMutableArray array];

    [self.tableView registerNib:[UINib nibWithNibName:@"NoticesTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_push_notifications];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)get_push_notifications{
    [SVProgressHUD dismiss];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19210" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getInfoAPIResponce:) name:@"19210" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19210" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildgetInfoAPIResponce:) name:@"-19210" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_push_notifications.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"19210" :params];
}

-(void)getInfoAPIResponce:(NSNotification *)notification {
    
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19210" object:nil];
    
    [array_items removeAllObjects];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        if ([dict count] == 0 || dict == nil)
        {
            [self.tableView reloadData];

            return;
        }
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        
        for (int i = 0; i< [dict count]; i++) {
            NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
            
            NSString *id = [temp objectForKey:@"id"];
            NSString *message = [StaticClass urlDecode:[temp objectForKey:@"message"]];
            
            [self addData:id message:message];
        }
        
        [self.tableView reloadData];
    }
}

-(void)FaildgetInfoAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19210" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19210" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}

-(void)addData:(NSString*)id message:(NSString*)message
{
    PushNotification_info *data = [[PushNotification_info alloc]init];
    data.id = id;
    data.message = message;
    
    [array_items addObject:data];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array_items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PushNotification_info *data = [array_items objectAtIndex:indexPath.row];
    NoticesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    [cell.lbl_message setText:data.message];
    
    [cell.lbl_message sizeToFit];
    
    if ( cell.lbl_message.height < 21 )
        return 40.f;
    else
        return cell.lbl_message.height + 20.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoticesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    PushNotification_info *data = [array_items objectAtIndex:indexPath.row];
    
    [cell.lbl_message setText:data.message];
    
    return cell;
}

@end
