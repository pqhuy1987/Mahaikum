//
//  NoticesTableViewCell.h
//  Mahalkum
//
//  Created by user on 8/1/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LORichTextLabel.h"

@interface NoticesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbl_message;
@end
