//
//  ExamplePageViewController.m
//  LTPageViewController
//
//  Created by Le Thang on 3/15/16.
//  Copyright © 2016 Le Thang. All rights reserved.
//

#import "ExamplePageViewController.h"
#import "PageItemViewController.h"
#import "NewsViewController.h"
#import "FollowingViewController.h"
#import "NoticesViewController.h"

@interface ExamplePageViewController ()

@end

@implementation ExamplePageViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    NewsViewController *viewController1 = [[NewsViewController alloc] initWithNibName:@"NewsViewController" bundle:nil];
    
    FollowingViewController *viewController2 = [[FollowingViewController alloc] initWithNibName:@"FollowingViewController" bundle:nil];
    NoticesViewController *viewController3 = [[NoticesViewController alloc] initWithNibName:@"NoticesViewController" bundle:nil];
    
    NSArray *viewControllers = @[viewController1, viewController2, viewController3];
    
    //Segment titles
    NSArray *segmentTitles = @[@"News", @"Following", @"Notices"];
    
    //Init
    [self initWithListViewController:viewControllers segmentTitle:segmentTitles];

    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 22)];
    statusBarView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"colarge_iPad.png"]];
    [self.view addSubview:statusBarView];
    //    imgsegment1.hidden = NO;
    //    imgsegment2.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
