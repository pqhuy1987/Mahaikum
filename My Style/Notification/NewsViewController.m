//
//  NewsViewController.m
//  Mahalkum
//
//  Created by user on 8/1/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import "NewsViewController.h"

#import "Singleton.h"
#import "StaticClass.h"
#import "News_feed_like_cell.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"
#import "News_feed_like_Share.h"
#import "News_feed_following_cell.h"
#import "Favourite_news_following_share.h"
#import "Favourite_feed_following_collection_cell.h"
#import "Collectionview_delegate.h"
#import "Image_detail_url.h"
#import "Image_detail.h"
#import "News_feed_like_following_cell.h"
#import "News_feed_comment_cell.h"
#import "MCSegmentedControl.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Find_invite_friend.h"
#import "Category_info_ViewController.h"

#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"

@interface NewsViewController ()
{
    UITableView *tbl_news;
    int is_news;
    NSMutableArray *array_news_feed;
    LORichTextLabel *lbl_temp;
    Hash_tag_ViewController *hash_tag_viewObj;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Category_info_ViewController *categoryInfoView;
    
    Image_detail_url *image_detailViewObj;
}

@property (strong, nonatomic) IBOutlet UITableView *tbl_news;
@property(nonatomic,strong) NSMutableArray *array_news_feed;
@property(nonatomic,strong) LORichTextLabel *lbl_temp;
@property(nonatomic,strong) Image_detail_url *image_detailViewObj;
@property (nonatomic, strong) NSMutableArray *array_categories;
@property (strong, nonatomic) IBOutlet UILabel *lbl_noResult;

@end

@implementation NewsViewController

@synthesize tbl_news;
@synthesize array_news_feed, lbl_temp;
@synthesize image_detailViewObj;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];

    self.image_detailViewObj=[[Image_detail_url alloc]initWithNibName:@"Image_detail_url" bundle:nil];

    self.array_news_feed =[[NSMutableArray alloc]init];

    self.lbl_temp = [[LORichTextLabel alloc] initWithWidth:205];
    [self.lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];

    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];

    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [urlStyle addTarget:self action:@selector(urlSelected:)];

    
    [self.lbl_temp addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"WWW."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"https://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Https://"];

    is_news = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)get_news_feed
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_news_feed.php?sign=%@&salt=%@&uid=%@&off=0",[[Singleton sharedSingleton] getBaseURL], sig, salt, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            NSLog(@"There is no news!");
            //            [self.tbl_news setHidden:YES];
            //            [self.tbl_feed setHidden:YES];
            //            [self.lbl_noResult setHidden:NO];
            //            [self.btn_find_following setHidden:YES];
            //            self.lbl_noResult.text=@"Tab on the camera to add your first post and receive you first activity";
            is_news = 0;
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            [self.array_news_feed removeAllObjects];
            
            //[self.tbl_news setHidden:NO];
            //            [self.lbl_noResult setHidden:YES];
            //            [self.btn_find_following setHidden:YES];
            
            is_news = 1;
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict_main in array) {
                
                News_feed_like_Share *shareObj_main =[[News_feed_like_Share alloc]init];
                
                shareObj_main.user_id = [dict_main valueForKey:@"user_id"];
                shareObj_main.owner_id = [dict_main valueForKey:@"owner_id"];
                shareObj_main.username =[StaticClass urlDecode:[dict_main valueForKey:@"username"]];
                
                shareObj_main.feed =[StaticClass urlDecode:[dict_main valueForKey:@"feed"]];
                shareObj_main.userimage =[StaticClass urlDecode:[dict_main valueForKey:@"userimage"]];
                shareObj_main.name =[StaticClass urlDecode:[dict_main valueForKey:@"name"]];
                shareObj_main.image =[StaticClass urlDecode:[dict_main valueForKey:@"image"]];
                shareObj_main.status_date=[StaticClass urlDecode:[dict_main valueForKey:@"status_date"]];
                shareObj_main.read_status=[dict_main valueForKey:@"read_status"];
                shareObj_main.entity_id=[dict_main valueForKey:@"entity_id"];
                
                shareObj_main.userAction=[StaticClass urlDecode:[dict_main valueForKey:@"user_action"]];
                
                NSDictionary *dict =[dict_main valueForKey:@"image_data"];
                
                if ([dict isKindOfClass:[NSDictionary class] ]){
                    Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                    
                    shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                    shareObj.image_id =[dict valueForKey:@"id"];
                    shareObj.image_owner=[dict valueForKey:@"image_owner"];
                    shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                    shareObj.lat =[dict valueForKey:@"lat"];
                    shareObj.lng=[dict valueForKey:@"lng"];
                    shareObj.price = dict[@"price"];
                    shareObj.currency = dict[@"currency"];
                    shareObj.sold = [dict[@"sold"] integerValue];
                    shareObj.likes=[dict valueForKey:@"likes"];
                    shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                    shareObj.uid=[dict valueForKey:@"uid"];
                    shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    
                    shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                    shareObj.category_id = [dict valueForKey:@"category_id"];
                    
                    shareObj.lat=[dict valueForKey:@"lat"];
                    shareObj.lng=[dict valueForKey:@"lng"];
                    
                    shareObj.liked=[dict valueForKey:@"user_liked"];
                    shareObj.comment_count =[dict valueForKey:@"total_comment"];
                    
                    shareObj.array_liked_by=[[NSMutableArray alloc]init];
                    
                    if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                        NSArray *tempArray =[dict valueForKey:@"liked_by"];
                        
                        for (NSDictionary *tempdict in tempArray) {
                            [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                        }
                        
                    }
                    else{
                        // NSLog(@"NSString");
                        
                    }
                    
                    shareObj.array_comments=[[NSMutableArray alloc]init];
                    shareObj.array_comments_tmp=[[NSMutableArray alloc]init];
                    
                    if (![[dict valueForKey:@"description"] isEqualToString:@""])
                    {
                        Comment_share *obj =[[Comment_share alloc]init];
                        obj.uid =shareObj.uid;
                        obj.username =shareObj.username;
                        obj.name =shareObj.name;
                        obj.image_url=shareObj.user_image;
                        obj.comment_desc=shareObj.description;
                        obj.datecreated=shareObj.datecreated;
                        [shareObj.array_comments_tmp addObject:obj];
                    }
                    
                    if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                        NSArray *tempArray =[dict valueForKey:@"comments"];
                        
                        for (NSDictionary *tempdict in tempArray) {
                            Comment_share *obj =[[Comment_share alloc]init];
                            
                            obj.comment_id =[tempdict valueForKey:@"id"];
                            obj.uid =[tempdict valueForKey:@"uid"];
                            obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                            obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                            obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                            obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                            obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                            
                            [shareObj.array_comments addObject:obj];
                            [shareObj.array_comments_tmp addObject:obj];
                            
                        }
                        
                    }else{
                        // NSLog(@"NSString");
                        
                    }
                    
                    shareObj.rating=[dict valueForKey:@"rating"];
                    shareObj.my_rating=[dict valueForKey:@"my_rating"];
                    
                    shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                    shareObj.totalUser=[dict valueForKey:@"tot_user"];
                    
                    shareObj_main.image_data =shareObj;
                    
                    //  [shareObj release];
                }
                [self.array_news_feed addObject:shareObj_main];
            }
            
        }
        
        [SVProgressHUD dismiss];
        
        [self.tbl_news reloadData];
        [self updateNewFeedsReadStatus];
        
        self.lbl_noResult.text=@"Tab on the camera to add your first post and receive you first activity";
        if (is_news == 0)
        {
            [self.tbl_news setHidden:YES];
            
            [self.lbl_noResult setHidden:NO];
        }
        else
        {
            [self.tbl_news setHidden:NO];
            
            [self.lbl_noResult setHidden:YES];
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
    }];
}

- (void)get_categories_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&login_id=%@", salt, sig, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_category_followers.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"])
        {
            self.array_categories = [[NSMutableArray alloc] init];
            
            NSArray *dataArray = responseObject[@"data"];
            for(NSDictionary *data in dataArray)
            {
                CategoryObject *object = [[CategoryObject alloc] init];
                object.categoryId = data[@"id"];
                object.categoryName = data[@"name"];
                object.categoryDescs = data[@"description"];
                object.categoryFollowers = data[@"followers"];
                object.categoryUserFollower = data[@"user_follower"];
                [self.array_categories addObject:object];
            }
        }
        else {
            [self.array_categories removeAllObjects];
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get categories" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_news_feed];
    [self get_categories_list];
}

- (void)updateNewFeedsReadStatus
{
    //[SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update_new_feeds_status:) name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-162" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_new_feeds.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"owner_id": [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"162" :params];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)update_new_feeds_status:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
    {
        NSLog(@"Update new feeds success!");
        //self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)alert_badge];
        ALTabBarController *tabbar = (ALTabBarController *)self.tabBarController;
        tabbar.badgeView.alertCount = 0;
        tabbar.badgeView.hidden = YES;
        
    }
    else
        NSLog(@"Can not update new feeds");
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_news_feed.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];
    
    if (![shareObj.feed isEqualToString:@"started following you."]) {
        [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
        if (self.lbl_temp.height <29) {
            //NSLog(@"row:%d, height:%d",indexPath.row,75);
            return 75.0f;
        }else {
            NSLog(@"row:%ld, height:%f, label height:%f", (long)indexPath.row,self.lbl_temp.height +28.0f+10.0f+15+5,self.lbl_temp.height);
            return self.lbl_temp.height +28.0f+10.0f+15+5;
        }
    }
    //NSLog(@"row:%d, height:%f",indexPath.row,70.0f+5);
    return 70.0f+5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];
    if (![shareObj.feed isEqualToString:@"started following you."]) {
        static NSString *identifier =@"News_feed_like_cell";
        News_feed_like_cell *cell=(News_feed_like_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_click:) forControlEvents:UIControlEventTouchUpInside];
            [cell draw_dec_lbl];
            
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            
            LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color: [UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [hashStyle addTarget:self action:@selector(hashSelected:)];
            
            LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [atStyle addTarget:self action:@selector(atSelected:)];
            
            LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [urlStyle addTarget:self action:@selector(urlSelected:)];
            [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
            [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
            
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
            
            //                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
            //                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
        }
        cell.btn_img1.tag = indexPath.row;
        
        if (![shareObj.userAction isEqualToString:@"rating"]) {
            cell.imgRateImage.hidden=YES;
        }
        else {
            cell.imgRateImage.hidden=NO;
            Home_tableview_data_share *homShr=shareObj.image_data;
            if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                NSString *imgName=@"0star.png";
                cell.imgRateImage.image=[UIImage imageNamed:imgName];
            }
            else {
                NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                cell.imgRateImage.image=[UIImage imageNamed:imgName];
            }
        }
        
        // news table user
        Home_tableview_data_share *homShr=shareObj.image_data;
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
        [userStyle addTarget:self action:@selector(userSelected:)];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
        
        [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
        
        cell.btn_user_img.layer.cornerRadius = 22.0;
        cell.btn_user_img.layer.masksToBounds=YES;
        
        [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_user_img.image = image;
            }
        }];
        
        [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:homShr.image_path] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_img.image = image;
            }
        }];
        
        /* cell.btn_user_img.imageURL = [NSURL URLWithString:shareObj.userimage];
         cell.btn_img.imageURL = [NSURL URLWithString:shareObj.image];
         */
        cell.lbl_time.text =[self get_time_different:shareObj.status_date];
        
        if (cell.lbl_dec.height <29) {
            cell.lbl_dec.frame = CGRectMake(58,13,205,29);
            cell.lbl_time.frame = CGRectMake(58,33,105,21);
            cell.imgRateImage.frame = CGRectMake(58,33+21+2,70,15);
            cell.imgBottomLine.frame=CGRectMake(0,33+21+2+15,kViewWidth,1);
        }
        else{
            cell.lbl_dec.frame = CGRectMake(58,13,205,cell.lbl_dec.height);
            cell.lbl_time.frame = CGRectMake(58,cell.lbl_dec.height+7, 105, 21);
            cell.imgRateImage.frame = CGRectMake(58,cell.lbl_dec.height+7+21,70,15);
            NSLog(@"indexpath row:%ld", (long)indexPath.row);
            NSLog(@"height:%f label:%f",cell.lbl_dec.height+7+21+15,cell.lbl_dec.height);
            cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+7+21+15,kViewWidth,1);
        }
        
        return cell;
    }
    else{
        static NSString *identifier =@"News_feed_following_cell";
        News_feed_following_cell *cell=(News_feed_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell draw_in_cell];
        }
        
        if (![shareObj.userAction isEqualToString:@"rating"]) {
            cell.imgRateImage.hidden=YES;
        }
        else {
            cell.imgRateImage.hidden=NO;
            Home_tableview_data_share *homShr=shareObj.image_data;
            if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                NSString *imgName=@"0star.png";
                cell.imgRateImage.image=[UIImage imageNamed:imgName];
            }
            else {
                NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                cell.imgRateImage.image=[UIImage imageNamed:imgName];
            }
        }
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0/255.0 green:112.0/255.0 blue:112.0/255.0 alpha:1.0]];
        [userStyle addTarget:self action:@selector(userSelected:)];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
        
        [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
        
        cell.btn_user_img.layer.cornerRadius = 22.0;
        cell.btn_user_img.layer.masksToBounds=YES;
        
        [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_user_img.image = image;
            }
        }];
        cell.lbl_time.text =[self get_time_different:shareObj.status_date];
        
        if (cell.lbl_dec.height <21) {
            cell.lbl_dec.frame = CGRectMake(55,5,265,20);
            cell.lbl_time.frame =CGRectMake(58,33,105,21);
            cell.imgRateImage.frame = CGRectMake(53,33+5+21+2,70,15);
            cell.imgBottomLine.frame=CGRectMake(0,33+5+21+2+15,kViewWidth,1);
            
        }
        else{
            cell.lbl_dec.frame = CGRectMake(55,5,265,cell.lbl_dec.height);
            cell.lbl_time.frame =CGRectMake(58,cell.lbl_dec.height+13+5,105,21);
            
            cell.imgRateImage.frame = CGRectMake(53,cell.lbl_dec.height+13+5+21+2,70,15);
            cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+13+5+21+2+15,kViewWidth,1);
        }
        return cell;
    }
}
#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring {
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"just now"];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.f minutes ago",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.f hours ago",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.f days ago",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.f months ago",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.f year ago",diff/(3600*24*30*12)];
    }
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
    return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark - Image Detail
-(IBAction)btn_image_detail_like_click:(id)sender {
    
    News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:((UIButton *)sender).tag];
    NSLog(@"Action=%@, read_status=%@, userid=%@, ownerid=%@, username=%@, following_name=%@, feed=%@",shareObj.userAction, shareObj.read_status, shareObj.user_id, shareObj.owner_id, shareObj.username, shareObj.name, shareObj.feed);
    
    if ([shareObj.userAction isEqualToString:@"recive_request"])
    {
        return;
    }
    
    if ([shareObj.userAction isEqualToString:@"follow_category"])
    {
        categoryInfoView.user_id = shareObj.owner_id;
        for(NSDictionary *data in self.array_categories)
        {
            CategoryObject *object = (CategoryObject*)data;
            
            if (object == nil)
                continue;
            
            if ([object.categoryId isEqualToString:shareObj.entity_id])
            {
                categoryInfoView.category = object;
                break;
            }
        }
        
        [self.navigationController pushViewController:categoryInfoView animated:YES];
        
        return;
    }
    
    //    self.image_detail_viewObj.shareObj =shareObj.image_data;
    //
    //    self.image_detail_viewObj.arrayFeedArray=self.array_news_feed;
    //    self.image_detail_viewObj.currentSelectedIndex=((UIButton *)sender).tag;
    //
    //    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
    
    self.image_detailViewObj.image_id =shareObj.image_data.image_id;
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
}

@end
