//
//  PushNotification_info.h
//  My Style
//
//  Created by Tis Macmini on 4/20/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PushNotification_info : NSObject{

    NSString *id;
    NSString *message;
}
@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSString *message;
@end
