//
//  FollowingViewController.m
//  Mahalkum
//
//  Created by user on 8/1/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import "FollowingViewController.h"

#import "Singleton.h"
#import "StaticClass.h"
#import "News_feed_like_cell.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"
#import "News_feed_like_Share.h"
#import "News_feed_following_cell.h"
#import "Favourite_news_following_share.h"
#import "Favourite_feed_following_collection_cell.h"
#import "Collectionview_delegate.h"
#import "Image_detail_url.h"
#import "Image_detail.h"
#import "News_feed_like_following_cell.h"
#import "News_feed_comment_cell.h"
#import "MCSegmentedControl.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Find_invite_friend.h"
#import "Category_info_ViewController.h"

#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"

@interface FollowingViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UITableView *tbl_feed;
    int is_following;
    NSMutableArray *array_follow;
    LORichTextLabel *lbl_temp;
    LORichTextLabel *lbl_temp2;
    Hash_tag_ViewController *hash_tag_viewObj;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Category_info_ViewController *categoryInfoView;
    
    Image_detail_url *image_detailViewObj;
    Find_invite_friend *find_inviteViewObj;
    LORichTextLabelStyle *userStyle1;
}
@property (strong, nonatomic) IBOutlet UITableView *tbl_feed;
@property(nonatomic,strong) NSMutableArray *array_follow;
@property(nonatomic,strong) LORichTextLabel *lbl_temp;
@property(nonatomic,strong) LORichTextLabel *lbl_temp2;
@property(nonatomic,strong) Image_detail_url *image_detailViewObj;

@property (strong, nonatomic) IBOutlet UILabel *lbl_noResult;
@property (strong, nonatomic) IBOutlet UIButton *btn_find_following;
@property(nonatomic,strong) Find_invite_friend *find_inviteViewObj;
@property(nonatomic,strong) LORichTextLabelStyle *userStyle1;
@end

@implementation FollowingViewController
@synthesize tbl_feed;
@synthesize array_follow, lbl_temp, lbl_temp2;
@synthesize image_detailViewObj, find_inviteViewObj, userStyle1;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];
    
    self.find_inviteViewObj =[[Find_invite_friend alloc]initWithNibName:@"Find_invite_friend" bundle:nil];

    self.image_detailViewObj=[[Image_detail_url alloc]initWithNibName:@"Image_detail_url" bundle:nil];
    
    self.array_follow =[[NSMutableArray alloc]init];
    
    self.lbl_temp = [[LORichTextLabel alloc] initWithWidth:205];
    [self.lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
    
    self.lbl_temp2 = [[LORichTextLabel alloc] initWithWidth:265];
    [self.lbl_temp2 setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
    
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
    
    self.userStyle1 = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [self.userStyle1 addTarget:self action:@selector(userSelected:)];
    
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [urlStyle addTarget:self action:@selector(urlSelected:)];
    
    
    [self.lbl_temp addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"WWW."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"https://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Https://"];

    [self.lbl_temp2 addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp2 addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"www."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"WWW."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Www."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Http://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"https://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Https://"];

    is_following = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self get_following_feed];
}

- (void)get_following_feed
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_following_feeds.php?sign=%@&salt=%@&uid=%@&off=0",[[Singleton sharedSingleton] getBaseURL], sig, salt, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"] || [[responseObject valueForKey:@"success"] isEqualToString:@"-3"]) {
            NSLog(@"There is no following user!");
            //            [self.tbl_feed setHidden:YES];
            //            [self.tbl_news setHidden:YES];
            //            [self.lbl_noResult setHidden:NO];
            //            [self.btn_find_following setHidden:NO];
            //             self.lbl_noResult.text=@"Follow people to see there activities";
            is_following = 0;
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            [self.array_follow removeAllObjects];
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            //[self.tbl_feed setHidden:NO];
            //            [self.lbl_noResult setHidden:YES];
            //            [self.btn_find_following setHidden:YES];
            is_following = 1;
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict in array)
            {
                Favourite_news_following_share *shareobj =[[Favourite_news_following_share alloc]init];
                shareobj.user_action =[dict valueForKey:@"user_action"];
                
                //NSLog(@"========>owner_id=%@, user_id=%@, login_id=%@", [StaticClass urlDecode:[dict valueForKey:@"owner_id"]],[StaticClass urlDecode:[dict valueForKey:@"user_id"]],[StaticClass urlDecode:[dict valueForKey:@"login_id"]]);
                
                if ([shareobj.user_action isEqualToString:@"follow"])
                {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    [self.array_follow addObject:shareobj];
                }
                else if ([shareobj.user_action isEqualToString:@"comment"])
                {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.img_url =[StaticClass urlDecode:[dict valueForKey:@"comment_on_image"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"comment_on_username"]];
                    shareobj.image_id =[StaticClass urlDecode:[dict valueForKey:@"image_id"]];
                    [self.array_follow addObject:shareobj];
                    
                }
                else if ([shareobj.user_action isEqualToString:@"follow_category"])
                {
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"category_on_username"]];
                    [self.array_follow addObject:shareobj];
                    //                    NSLog(@"--------------->%@, %@, %@, %@, %@",shareobj.feed, shareobj.following_name, shareobj.username, shareobj.user_img_url, shareobj.following_name);
                    
                }
                else if ([shareobj.user_action isEqualToString:@"like"]) {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"liked_image_owner"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.img_count =[dict valueForKey:@"image_likes"];
                    
                    
                    shareobj.array_img =[dict valueForKey:@"image"];
                    [self.array_follow addObject:shareobj];
                    
                }
            }
        }
        
        [self.tbl_feed reloadData];
        
        self.lbl_noResult.text=@"Follow people to see there activities";
        if (is_following == 0)
        {
            [self.tbl_feed setHidden:YES];
            
            [self.lbl_noResult setHidden:NO];
            [self.btn_find_following setHidden:NO];
        }
        else
        {
            [self.tbl_feed setHidden:NO];
            
            [self.lbl_noResult setHidden:YES];
            [self.btn_find_following setHidden:YES];
        }

        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
        [SVProgressHUD dismiss];
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_follow.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Favourite_news_following_share *shareobj =[self.array_follow objectAtIndex:indexPath.row];
    if ([shareobj.user_action isEqualToString:@"follow"] || [shareobj.user_action isEqualToString:@"follow_category"]) {
        
        if ([shareobj.user_action isEqualToString:@"follow"])
            [self.lbl_temp2 setText:[NSString stringWithFormat:@"%@ %@ %@",shareobj.username,shareobj.feed,shareobj.following_name ]];
        else
            [self.lbl_temp2 setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
        
        if (shareobj.following_name.length !=0) {
            [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.following_name];
        }
        [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.username];
        
        if (self.lbl_temp2.height <21) {
            //  NSLog(@"row:%d, height:%f",indexPath.row,55.0f+20.0f);
            return 55.0f+20.0f;
            
        }
        else{
            // NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp2.height +34.0f+10.0f+5);
            return self.lbl_temp2.height +34.0f+10.0f+5;
        }
    }
    else if([shareobj.user_action isEqualToString:@"comment"]){
        
        [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
        
        [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];
        
        
        if (self.lbl_temp.height <29) {
            //NSLog(@"row:%d, height:%d",indexPath.row,55);
            return 55;//  55.0f+15.0f+5;
        }
        else {
            // NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp.height +28.0f+10.0f+5);
            return self.lbl_temp.height +28.0f+10.0f+5;
        }
    }
    else if([shareobj.user_action isEqualToString:@"like"]){
        if ([shareobj.img_count isEqualToString:@"1"]) {
            [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
            [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];
            
            if (self.lbl_temp.height <29) {
                //NSLog(@"row:%d, height:%d",indexPath.row,55);
                return 55.0f;
            }
            else {
                //NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp.height +28.0f+5.0f+10.0f+5);
                return self.lbl_temp.height +28.0f+5.0f+10.0f+5;
            }
        } else {
            int count =(([shareobj.img_count intValue])/3);
            int height =count*105;
            if((([shareobj.img_count intValue])%3) >0) {
                height+=105;
            }
            
            // NSLog(@"row:%d, height:%f",indexPath.row,55.0f + height+10.0f+5);
            return 55.0f + height+10.0f+5;
        }
    }
    // NSLog(@"row:%d, height:%d",indexPath.row,55);
    return 55.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Favourite_news_following_share *shareObj =[self.array_follow objectAtIndex:indexPath.row];
    if ([shareObj.user_action isEqualToString:@"follow"] || [shareObj.user_action isEqualToString:@"follow_category"]) {
        static NSString *identifier =@"News_feed_following_cell";
        News_feed_following_cell *cell=(News_feed_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell draw_in_cell];
        }
        cell.imgRateImage.hidden=YES;
        
        cell.btn_user_img.layer.cornerRadius = 22.0;
        cell.btn_user_img.layer.masksToBounds=YES;
        
        // following table username
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
        LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
        
        [userStyle addTarget:self action:@selector(userSelected:)];
        
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
        if ([shareObj.user_action isEqualToString:@"follow_category"])
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
        else
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@ %@",shareObj.username,shareObj.feed,shareObj.following_name]];
        
        cell.lbl_time.font = [UIFont fontWithName:@"Helvetica" size:10.0];
        
        [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_user_img.image = image;
            }
        }];
        cell.lbl_time.text =[self get_time_different:shareObj.status_date];
        
        if (cell.lbl_dec.height <21) {
            cell.lbl_dec.frame = CGRectMake(58,13,265,20);
            cell.lbl_time.frame =CGRectMake(58,33,105,21);
        }
        else {
            cell.lbl_dec.frame = CGRectMake(58,13,265,cell.lbl_dec.height);
            cell.lbl_time.frame =CGRectMake(58,cell.lbl_dec.height+13,105,21);
        }
        return cell;
    }
    else if([shareObj.user_action isEqualToString:@"comment"]){
        static NSString *identifier =@"News_feed_comment_cell";
        News_feed_comment_cell *cell=(News_feed_comment_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell==nil) {
            NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_comment_cell" owner:self options:nil];
            cell =[nib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
            [cell draw_dec_lbl];
            
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            
            LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [hashStyle addTarget:self action:@selector(hashSelected:)];
            
            LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [atStyle addTarget:self action:@selector(atSelected:)];
            
            LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [urlStyle addTarget:self action:@selector(urlSelected:)];
            
            [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
            [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
            [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
            
            
            //                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
            //                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
        }
        
        cell.btn_img1.tag=indexPath.row;
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
        LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
        
        [userStyle addTarget:self action:@selector(userSelected:)];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
        [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.feed];
        
        [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
        
        cell.btn_user_img.layer.cornerRadius = 22.0;
        cell.btn_user_img.layer.masksToBounds=YES;
        
        [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_user_img.image = image;
            }
        }];
        [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:shareObj.img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil)
            {
                cell.btn_img.image = image;
            }
        }];
        
        cell.lbl_time.text =[self get_time_different:shareObj.status_date];
        
        if (cell.lbl_dec.height <29) {
            cell.lbl_dec.frame = CGRectMake(63,5,205,29);
            cell.lbl_time.frame = CGRectMake(63,34,105,19);
            cell.imgBottomLine.frame=CGRectMake(0,53,kViewWidth,1);
        } else {
            cell.lbl_dec.frame = CGRectMake(63,5,205,cell.lbl_dec.height);
            cell.lbl_time.frame = CGRectMake(63,cell.lbl_dec.height+5, 105, 19);
            cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+5+19,kViewWidth,1);
        }
        return cell;
    }
    else if([shareObj.user_action isEqualToString:@"like"]){
        if ([shareObj.img_count isEqualToString:@"1"]) {
            static NSString *identifier =@"News_feed_like_following_cell";
            News_feed_like_following_cell *cell=(News_feed_like_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_following_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
                [cell draw_dec_lbl];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                
                //                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                //                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            }
            
            cell.btn_img1.tag = indexPath.row;
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            
            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            for (NSDictionary *dict in shareObj.array_img) {
                [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil)
                    {
                        cell.btn_img.image = image;
                    }
                }];
            }
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            if (cell.lbl_dec.height <29) {
                cell.lbl_dec.frame = CGRectMake(63,5,205,29);
                cell.lbl_time.frame = CGRectMake(63,34,105,19);
                cell.imgBottomLine.frame=CGRectMake(0,53,kViewWidth,1);
                
            } else {
                cell.lbl_dec.frame = CGRectMake(63,5,205,cell.lbl_dec.height);
                cell.lbl_time.frame = CGRectMake(63,cell.lbl_dec.height+5, 105,19);
                cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+5+19,kViewWidth,1);
            }
            return cell;
            
        }
        else {
            static NSString *identifier =@"Favourite_feed_following_collection_cell";
            Favourite_feed_following_collection_cell *cell=(Favourite_feed_following_collection_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Favourite_feed_following_collection_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell draw_collectionview_in_cell];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                
                //                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                //                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                
            }
            
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            
            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            int count =(([shareObj.img_count intValue])/3);
            int height =count*105;
            if((([shareObj.img_count intValue])%3) >0){
                height+=105;
            }
            
            cell.collectionView.frame = CGRectMake(0,55,kViewWidth,height);
            
            cell.imgBottomImage.frame=CGRectMake(0,55+height+5,kViewWidth, 1);
            [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
            cell.collectionView.delegate = self;
            cell.collectionView.dataSource = self;
            return cell;
        }
    }
    return nil;
}
#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring {
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"just now"];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.f minutes ago",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.f hours ago",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.f days ago",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.f months ago",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.f year ago",diff/(3600*24*30*12)];
    }
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
    return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (IBAction)find_following:(id)sender {
    [self.navigationController pushViewController:self.find_inviteViewObj animated:YES];
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(Collectionview_delegate *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(Collectionview_delegate *)collectionView numberOfItemsInSection:(NSInteger)section {
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    
    return [obj.img_count integerValue];
}

- (CGSize)collectionView:(Collectionview_delegate *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width-15)/4, (collectionView.frame.size.width-5)/4);
}

-(UICollectionViewCell *)collectionView:(Collectionview_delegate *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"image_collection_cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:10000];
    
    if (imageView == nil)
    {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        imageView.userInteractionEnabled = NO;
        imageView.tag = 10000;
    }
    
    //    image_collection_cell *cell = (image_collection_cell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    //
    //    cell.img_photo.layer.borderWidth =1.0f;
    //    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    //    cell.img_photo.layer.cornerRadius =1.5f;
    //    cell.img_photo.layer.masksToBounds =YES;
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];
    //NSLog(@"***********>owner_id->%@", [StaticClass urlDecode:[dict valueForKey:@"owner_id"]]);
    
    // cell.img_photo.imageURL =[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error == nil)
        {
            @autoreleasepool {
                imageView.image = image;
            }
        }
    }];
    
    // cell.img_photo.image =[UIImage imageNamed:@"test1.png"];
    cell.tag = indexPath.row;
    [cell addSubview:imageView];
    
    return cell;
}

- (void)collectionView:(Collectionview_delegate *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];
    
    self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
}


-(IBAction)btn_image_detail_like_following_click:(id)sender {
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:((UIButton *)sender).tag];
    
    if ([obj.user_action isEqualToString:@"comment"] || [obj.user_action isEqualToString:@"mention"]){
        self.image_detailViewObj.image_id =obj.image_id;
    }
    else{
        NSDictionary *dict =[obj.array_img objectAtIndex:0];
        self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    }
    
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
    
}
@end
