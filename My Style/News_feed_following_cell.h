//
//  News_feed_following_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/10/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"

@interface News_feed_following_cell : UITableViewCell{

   UIImageView *btn_user_img;
    UILabel *lbl_time;
    LORichTextLabel *lbl_dec;
    UIImageView *imgRateImage;
    UIImageView *imgBottomLine;
}

-(void)draw_in_cell;

@property(nonatomic,strong) LORichTextLabel *lbl_dec;

@property(nonatomic,strong)IBOutlet UIImageView *btn_user_img;
@property(nonatomic,strong)IBOutlet UILabel *lbl_time;
@property(nonatomic,strong)IBOutlet UIImageView *imgRateImage;
@property(nonatomic,strong)IBOutlet UIImageView *imgBottomLine;

@end
