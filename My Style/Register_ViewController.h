//
//  Register_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/9/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BVUnderlineButton.h"
#import "EGOImageButton.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "MKInfoPanel.h"
#import <Twitter/Twitter.h>
#import "Twitter_login_ViewController.h"
#import "Facebook_Find_Friends_ViewController.h"
#import "webview_viewcontroller.h"

#import "DCRoundSwitch.h"

@interface Register_ViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AmazonServiceRequestDelegate,UITextFieldDelegate>{

    webview_viewcontroller *webviewObj;
    BVUnderlineButton *btn_privacy_policy;
    BVUnderlineButton *btn_terms_of_service;
    EGOImageButton *btn_add_photo;
    
    UIImagePickerController *photoPickerController;
    
    NSString *image_url;
    
    UITextField *txt_username;
    UITextField *txt_password;
    UITextField *txt_email;
    UITextField *txt_name;
    UITextField *txt_phone_no;
    UIView *view_error;
    
    UIButton *btn_facebook_info;
    
    UIScrollView *scrollview;
    Twitter_login_ViewController *twitter_login;
    
    UIImageView *img_first_cell_bg;
    UIImageView *img_second_cell_bg;
    UIActivityIndicatorView *activity;
    UILabel *lblSpinnerBg;
    UITextField *txt_CountryCode;
    
    UITextField *txt_weburl;
    UITextView *txt_bio;
    UIButton *btn_gender;
    UILabel *lblgender, *placeholderLabel;
    UIImageView *imgswitchgendermale,*imgswitchgenderfemale;
}
@property(nonatomic,strong)IBOutlet UITextField *txt_CountryCode;
@property(nonatomic,strong)IBOutlet UIImageView *img_first_cell_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_second_cell_bg;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong)IBOutlet  UIButton *btn_facebook_info;
@property(nonatomic,strong)IBOutlet UIView *view_error;
@property(nonatomic,strong) NSString *image_url;
@property(nonatomic,strong)UIImagePickerController *photoPickerController;

@property(nonatomic,strong)IBOutlet BVUnderlineButton *btn_privacy_policy;
@property(nonatomic,strong)IBOutlet BVUnderlineButton *btn_terms_of_service;
@property(nonatomic,strong)IBOutlet EGOImageButton *btn_add_photo;

@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)IBOutlet UITextField *txt_password;
@property(nonatomic,strong)IBOutlet UITextField *txt_email;
@property(nonatomic,strong)IBOutlet UITextField *txt_name;
@property(nonatomic,strong)IBOutlet UITextField *txt_phone_no;
@property(nonatomic,strong)webview_viewcontroller *webviewObj;
@property (nonatomic, strong) AmazonS3Client *s3;
@property (nonatomic, strong) UIActivityIndicatorView *activity;
@property (nonatomic, strong) UILabel *lblSpinnerBg;

@property (weak, nonatomic) IBOutlet UITextField *txt_facebook_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_twitter_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_pinterest_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_google_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_instagram_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_youtube_url;
@property (strong, nonatomic) IBOutlet UITextField *txt_delivery_time;
@property (strong, nonatomic) IBOutlet UITextField *txt_working_hours;
@property (strong, nonatomic) IBOutlet UITextField *txt_shipping_fee;
@property(nonatomic,strong)IBOutlet UITextField *txt_weburl;
@property (strong, nonatomic) IBOutlet UITextView *txt_bio;
@property(nonatomic,strong)IBOutlet UILabel *lblgender;
@property(nonatomic,strong)IBOutlet UIButton *btn_gender;
@property (nonatomic, strong) IBOutlet DCRoundSwitch *switch_gender;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchgendermale,*imgswitchgenderfemale;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@end
