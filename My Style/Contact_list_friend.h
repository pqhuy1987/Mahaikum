//
//  Contact_list_friend.h
//  My Style
//
//  Created by Tis Macmini on 5/9/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "DejalActivityView.h"
#import <AddressBook/AddressBook.h>
#import "AJNotificationView.h"
#import "Suggested_friend_list_reg.h"

@interface Contact_list_friend : UIViewController{
    
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
    Suggested_friend_list_reg *suggested_viewObj;
    NSMutableArray *array_email;

}

@property(nonatomic,strong) Suggested_friend_list_reg *suggested_viewObj;
@property(nonatomic,strong)NSMutableArray *array_friend;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_friend;
@property(nonatomic,strong)NSMutableArray *array_email;
@end
