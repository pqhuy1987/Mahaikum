//
//  AppDelegate.h
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import "ViewController.h"
#import "GlobalDefine.h"
#import "ElasticTransition.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,ChartboostDelegate>{//RevMobAdsDelegate//ChartboostDelegate
    CLLocationManager *locationManager;
    Chartboost *cb;
    
    ViewController *viewController;
    //ViewController_iPad *viewController_iPad;
    UINavigationController *navObj;

    ElasticTransition *transition;
}

@property (nonatomic, strong) GlobalDefine *myDefine;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) UINavigationController *navObj;
@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) NSString *device_token;
@property(nonatomic,strong) NSString *badge;
@property(nonatomic,strong) NSString *fromUser;
@property(nonatomic,strong) NSString *fromUserID;
@property(nonatomic,strong) NSString *fromUserImage;
@property(nonatomic,strong) ElasticTransition *transition;

@property BOOL isChattingRoom;
@property UIAlertView *alertView;
@property NSString *push_type; // type1: 'all push', type2: 'chat'
@property ALTabBarController *tabbar;

//
//@property (strong, nonatomic) LocationTracker * locationTracker;
//@property (nonatomic) CLLocationCoordinate2D myCurrentLocation;
//@property (nonatomic) NSTimer* locationUpdateTimer;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
- (void)RepeateChatBoost;
+ (AppDelegate *)sharedDelegate;

@property (strong, nonatomic) FBSession *session;

@end
