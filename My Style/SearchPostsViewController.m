
#import "SearchPostsViewController.h"
#import "Search_ViewController.h"
#import "LMPullToBounceWrapper.h"
#import "NSTimer+PullToBounce.h"
#import "UIView+PullToBounce.h"
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

@interface SearchPostsViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UIGestureRecognizerDelegate, STPopupPreviewRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UIButton *_btn_reload;
    BOOL isRefreshing;
}
@property (nonatomic, strong) LMPullToBounceWrapper *pushToBounceWrapper;
@end

@implementation SearchPostsViewController
@synthesize getFeedKind;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"image_collection_cell" bundle:nil] forCellWithReuseIdentifier:@"image_collection_cell"];
    self.viewTitle.text = self.viewTitleString;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"colarge_iPad.png"]];
    //    CGRect bodyViewFrame = self.collectionView.frame;
    //    UIView *bodyView = [[UIView alloc] initWithFrame:bodyViewFrame];
    //    [self.view addSubview:bodyView];
    self.collectionView.frame = CGRectMake(0, 0, self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    NSLog(@"CollectionViewSize=%f %f", self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    self.pushToBounceWrapper = [[LMPullToBounceWrapper alloc] initWithScrollView:self.collectionView];
    self.pushToBounceWrapper.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    [self.bodyView addSubview:self.pushToBounceWrapper];
    WS(weakSelf);
    
    [self.pushToBounceWrapper setDidPullTorefresh:^(){
        isRefreshing = YES;
        [weakSelf btn_refresh_click:nil];
        //        [NSTimer schedule:2.0 handler:^(CFRunLoopTimerRef timer) {
        //            [weakSelf.pushToBounceWrapper stopLoadingAnimation];
        //        }];
    }];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];

//    _btn_reload = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_btn_reload setImage:[UIImage imageNamed:@"refreshbtn.png"] forState:UIControlStateNormal];
//    [_btn_reload addTarget:self action:@selector(btn_refresh_click:) forControlEvents:UIControlEventTouchUpInside];
//    [_btn_reload setFrame:CGRectMake(kViewWidth - 46.0, 30, 26, 26)];
//    
//    [self.view addSubview:_btn_reload];
    
}

#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];

    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

- (void)performWithEmptyResponse
{
    self.posts = [NSMutableArray array];
}

- (IBAction)btn_refresh_click:(id)sender
{
    if ( !isRefreshing )
        [SVProgressHUD show];

    _btn_reload.enabled = NO;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = @"";
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    
    if (getFeedKind == 1) {
        requestStr =[NSString stringWithFormat:@"%@get_random_images.php?%@",[[Singleton sharedSingleton] getBaseURL], postString];
    }
    else {
        requestStr =[NSString stringWithFormat:@"%@get_top_images.php?%@",[[Singleton sharedSingleton] getBaseURL], postString];
    }
    
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:sig, @"sign",salt, @"salt",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",nil];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
//        _btn_reload.enabled = YES;
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            [self performWithEmptyResponse];
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            self.posts = [[NSMutableArray alloc] init];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.price = dict[@"price"];
                shareObj.currency = dict[@"currency"];
                shareObj.sold = [dict[@"sold"] integerValue];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        [shareObj.array_comments addObject:obj];
                    }
                    
                }else{
                    // NSLog(@"NSString");
                    
                }
                
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                [self.posts addObject:shareObj];
            }
        }
        
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];
        [SVProgressHUD dismiss];
        isRefreshing = NO;
        [self.collectionView reloadData];
    } failure:^(NSString *errorString) {
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];
        [SVProgressHUD dismiss];
        isRefreshing = NO;
        _btn_reload.enabled = YES;
        [self performWithEmptyResponse];
        [self.collectionView reloadData];

        //        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(backedFromView:)])
    {
        [self.delegate backedFromView:self];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.posts.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kViewWidth-2)/3, (kViewWidth-2)/3);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = [cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)cell.img_photo;
    
//    if (imageView == nil)
//    {
//        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 105, 105)];
//        imageView.userInteractionEnabled = NO;
//        imageView.tag = 10000;
//    }
    if (!cell.popupPreviewRecognizer) {
        cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
    }
    Home_tableview_data_share *shareObj = [self.posts objectAtIndex:indexPath.row];
    NSLog(@"%@", shareObj.image_path);
    [imageView sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:[UIImage imageNamed:@"default_user_image"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error == nil)
        {
            imageView.image = image;
        }
    }];
    cell.data = shareObj;
    NSString *strPrice = @"";
    
    if ( shareObj.price.length != 0 )
        strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
    
    if ([strPrice isEqualToString:@"(null)(null)"])
        cell.lbl_price.text = @"";
    else
        cell.lbl_price.text = strPrice;
    
    if ( [cell.lbl_price.text isEqualToString:@""] )
        cell.lbl_price.hidden = YES;
    else
        cell.lbl_price.hidden = NO;

//    [cell addSubview:imageView];
    cell.tag = indexPath.row;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.image_detail_viewObj.shareObj =[self.posts objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj = [[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    
    Home_tableview_data_share *sh = [self.posts objectAtIndex:indexPath.row];
    NSLog(@"PassedImage:%@", sh.image_id);
    
    self.image_detail_viewObj.arrayFeedArray=[self.posts mutableCopy];
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    self.image_detail_viewObj.promotion = NO;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

@end
