//
//  Photos_you_liked.m
//  My Style
//
//  Created by Tis Macmini on 5/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Photos_you_liked.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

@interface Photos_you_liked ()  <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate, UIGestureRecognizerDelegate,STPopupPreviewRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
}

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation Photos_you_liked

@synthesize tbl_news,liker_list_viewObj;
@synthesize array_feeds;
@synthesize img_like_heart,comments_list_viewObj;
@synthesize share_photo_view;
@synthesize view_header,view_header_tbl;
@synthesize collectionViewObj;
@synthesize img_header_bg,img_header_bg_tbl;
@synthesize image_detail_viewObj,img_down_arrow,img_down_arrow_tbl;
@synthesize activity;

static NSInteger cellClickIndex2=0;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
    self.tbl_news.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tbl_news.delegate = delegate;
    self.tableViewDelegate = delegate;
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    self.img_down_arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    self.img_down_arrow_tbl.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    
    self.view.backgroundColor =[UIColor whiteColor];
    
     self.view_header_tbl.backgroundColor =[UIColor whiteColor];
    
//    self.img_header_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
//    self.img_header_bg.layer.cornerRadius =4.0f;
//    
//    self.img_header_bg_tbl.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_header_bg_tbl.layer.cornerRadius =4.0f;
    
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.array_feeds=[[NSMutableArray alloc]init];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];

    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"500" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"500" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-500" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-500" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"501" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"501" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-501" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-501" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"502" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"502" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-502" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-502" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"503" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"503" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-503" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-503" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"504" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"504" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-504" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-504" object:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    self.image_detail_viewObj =[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    //self.tbl_news.tableHeaderView = self.view_header_tbl;
 
    [self.img_like_heart setHidden:YES];
    
}

#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];

    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    view_header.hidden=NO;
    view_header_tbl.hidden=YES;
    
    self.tbl_news.contentOffset=CGPointMake(0,0);
    self.collectionViewObj.contentOffset=CGPointMake(0,0);
    
    activity.hidden=YES;
    [activity stopAnimating];
    
//    if (is_iPhone_5) {
//        self.tbl_news.frame = CGRectMake(0, 120, 320, 404);
//        self.collectionViewObj.frame=CGRectMake(0,120,320,404);
//    }
//    else{
//        self.tbl_news.frame = CGRectMake(0, 120, 320, 316);
//        self.collectionViewObj.frame=CGRectMake(0,120,320,316);
//    }
    
    [self.tbl_news reloadData];
    [self.collectionViewObj reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self get_news_feed];
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObj=[array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked= (int)userActionCell.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

    if (![shareObj.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}
-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}
-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:userActionCell.tag];
  //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}
-(void)userWillShare:(STXUserActionCell *)userActionCell {
    NSInteger tag =userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];

        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        //report_id : 1= i dont like this photo,  2=this photo is spam or a scam,  3=this photo puts people at risk,  4=this photo shouldn't be on mystyle
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
            //  [controller release];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:commentCell.tag];
   // self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag =likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked = (int)tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
   // pushPopFlg=1;
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
   // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}


- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
   // pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(IBAction)btnProfileClicked:(id)sender {
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld",(long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:posterLike.tag];
    which_image_liked = (int)posterLike.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Collectionview & Tableview  choose for show
-(IBAction)btn_collectionview_click:(id)sender {
    [self.collectionViewObj setContentOffset:self.tbl_news.contentOffset];
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden =NO;
    
    view_header.hidden=NO;
    view_header_tbl.hidden=YES;
    
    self.tbl_news.contentOffset=CGPointMake(0,0);
    self.collectionViewObj.contentOffset=CGPointMake(0,0);
}

-(IBAction)btn_tableview_click:(id)sender{
    [self.tbl_news setContentOffset:self.collectionViewObj.contentOffset];
    self.tbl_news.hidden = NO;
    self.collectionViewObj.hidden =YES;
    
    view_header.hidden=YES;
    view_header_tbl.hidden=NO;
    
    self.tbl_news.contentOffset=CGPointMake(0,0);
    self.collectionViewObj.contentOffset=CGPointMake(0,0);
}

#pragma mark Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
}

#pragma mark Get News Feed
-(void)get_news_feed {    
    //   http://www.techintegrity.in/mystyle/get_image_detail.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=1
    
    activity.hidden=NO;
    [activity startAnimating];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_likes_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"500":nil];
}

-(void)getnewsResponce:(NSNotification *)notification {
    activity.hidden=YES;
    [activity stopAnimating];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
       // NSLog(@"%@",[result valueForKey:@"curr_utc"]);
        
//        for (Home_tableview_data_share *obj in self.array_feeds) {
//            [obj release];
//            obj=nil;
//        }
        [self.array_feeds removeAllObjects];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict in array) {
            
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            
            
            
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.price = dict[@"price"];
            shareObj.currency = dict[@"currency"];
            shareObj.sold = [dict[@"sold"] integerValue];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            shareObj.category_id = [dict valueForKey:@"category_id"];
            
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            
            if (![[dict valueForKey:@"description"] isEqualToString:@""])
            {
                Comment_share *obj =[[Comment_share alloc]init];
                obj.uid =shareObj.uid;
                obj.username =[StaticClass urlDecode:shareObj.username];
                obj.name =shareObj.name;
                obj.image_url=shareObj.user_image;
                obj.comment_desc=shareObj.description;
                obj.datecreated=shareObj.datecreated;
                [shareObj.array_comments addObject:obj];
            }
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    
                    [shareObj.array_comments addObject:obj];

                }
                
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            [self.array_feeds addObject:shareObj];
        }
        
    }
    
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    
    self.tableViewDataSource.posts =self.array_feeds;// copy];
    self.tableViewDelegate.posts =self.array_feeds;// copy];
    [self.tbl_news reloadData];
    [self.collectionViewObj reloadData];
}

-(void)FailNewsReson:(NSNotification *)notification {
    activity.hidden=YES;
    [activity stopAnimating];

    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - UICollectionView
/*
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.view_header.frame;

        
        
        [headerView addSubview:self.view_header];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(0, self.view_header.frame.size.height);
        
        reusableview = headerView;

        return reusableview;
    }
    return reusableview;
}
*/

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.array_feeds.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {            return CGSizeMake(100, 100);
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =0.0f;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    if (!cell.popupPreviewRecognizer) {
        cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
    }
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.row];
  //  cell.img_photo.imageURL =[NSURL URLWithString:shareObj.image_path];
    [cell.img_photo sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    cell.data = shareObj;
    NSString *strPrice = @"";
    
    if ( shareObj.price.length != 0 )
        strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
    
    if ([strPrice isEqualToString:@"(null)(null)"])
        cell.lbl_price.text = @"";
    else
        cell.lbl_price.text = strPrice;
    
    if ( [cell.lbl_price.text isEqualToString:@""] )
        cell.lbl_price.hidden = YES;
    else
        cell.lbl_price.hidden = NO;

    cell.tag = indexPath.row;
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.image_detail_viewObj.shareObj =[self.array_feeds objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_feeds;
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
   
}


#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.array_feeds.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 41.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:section];
    
    UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"strip.png"]];
    strip.alpha = 1.0;
    strip.frame =CGRectMake(0,0,320,41);
    
    UIImageView *userimg_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_photobg.png"]];
    userimg_bg.alpha = 1.0;
    userimg_bg.frame =CGRectMake(5,1,40,40);
    
    NPRImageView *img_user =[[[NPRImageView alloc]initWithFrame:CGRectMake(9, 5, 32, 32)]autorelease];
    [img_user setImageWithContentsOfURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    img_user.layer.cornerRadius = 16.0;
    img_user.layer.masksToBounds=YES;
    
    EGOImageButton *img_userimage=[[[EGOImageButton alloc]initWithFrame:CGRectMake(5,1, 40, 40)] autorelease];
  //  img_userimage.imageURL =[NSURL URLWithString:shareObj.user_image];
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    img_userimage.layer.cornerRadius = 20.0f;
    img_userimage.layer.masksToBounds=YES;
    
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:shareObj.username forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(50,0,150,21)];
    [btn_username setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:13.0];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"category.png"]];
    img_locationicon.frame = CGRectMake(50,27,8,11);
    
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:shareObj.location forState:UIControlStateNormal];
    [btn_location setTitleColor:[UIColor colorWithRed:30.0f/255.0f green:48.0f/255.0f blue:62.0f/255.0f alpha:1] forState:UIControlStateNormal];
    btn_location.titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:13.0];
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //   [btn_location addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon.png"]];
    [img_clock setFrame:CGRectMake(240,10,62,21)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:shareObj.datecreated];
    lbl_time.font = [UIFont boldSystemFontOfSize:15];
    lbl_time.textColor =[UIColor whiteColor];
    lbl_time.frame =CGRectMake(265,14,40,16);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,41)];
    [view autorelease];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =1.0;
    [view addSubview:strip];
    [view addSubview:userimg_bg];
    [view addSubview:img_user];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    [view addSubview:lbl_time];
    
    if (shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(50,2,150,20)];
        [btn_location setFrame:CGRectMake(63,23,175,20)];
        img_locationicon.hidden=NO;
        strip.frame =CGRectMake(0,0,320,46);//320,70
        
    }else{
        img_locationicon.hidden=YES;
        strip.frame =CGRectMake(0, 0,320,46);
        //[btn_username setFrame:CGRectMake(55, 0, 150, 40)];
        [btn_username setFrame:CGRectMake(50,0,150,41)];
    }
    
    return view;
}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    return [Home_tableview_cell get_tableview_hight:shareObj];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Home_tableview_cell";
	Home_tableview_cell *cell = (Home_tableview_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor clearColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
         
        [cell.img_big addGestureRecognizer:tapImage];
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"www."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"WWW."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Www."];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"https://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Https://"];
        
//        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
//        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"www."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"WWW."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Www."];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"https://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Https://"];
//        
//        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
//        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
        
	}
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    [cell redraw_cell:shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    
    cell.dlstarObj.delegate = self;
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Method for Stop Header scrolling in UITableview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            NSLog(@"Delete");
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            
        }];
        
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            NSLog(@"");
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            NSLog(@"");
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            NSLog(@"");
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                NSLog(@"%@",htmlStr);
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        NSLog(@"");
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        NSLog(@"");
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        NSLog(@"");
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            NSLog(@"%@",htmlStr);
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    /*  [sheet addButtonWithTitle:@"Show Action Sheet on top" block:^{
     NSLog(@"");
     }];
     [sheet addButtonWithTitle:@"Show another alert" block:^{
     NSLog(@"");
     }];
     */
    [sheet showInView:self.view];
}

-(void)report_inappropriate:(int )index{
    activity.hidden=NO;
    [activity startAnimating];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    //post_report_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&report_id=2&item_id=12&uid=1
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [activity stopAnimating];
    activity.hidden=YES;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [activity stopAnimating];
    activity.hidden=YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_liked = (int)tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark Go to Profile
-(IBAction)btn_profile_click:(id)sender {
    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld", (long)tag);
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:(int)buttonIndex];
    }
}

#pragma mark Delete Photo
-(void)delete_photo{
    //     http://www.techintegrity.in/mystyle/post_delete_image.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    activity.hidden=NO;
    [activity startAnimating];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    self.tableViewDataSource.posts = self.array_feeds;// copy];
    self.tableViewDelegate.posts = self.array_feeds;// copy];
    [self.tbl_news reloadData];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"502":nil];
    
}

-(void)getdeleteResponce:(NSNotification *)notification {
    activity.hidden=YES;
    [activity stopAnimating];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
       // [self get_news_feed];
        
    }
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    //  http://www.techintegrity.in/mystyle/post_rating.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&rate=2&item_id=12&uid=1
    
    activity.hidden=NO;
    [activity startAnimating];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    
    cellClickIndex2=control.tag;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"503":nil];
    obj.my_rating = [NSString stringWithFormat:@"%lu", (unsigned long)rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
}

-(void)getRatingResponce:(NSNotification *)notification {
    activity.hidden=YES;
    [activity stopAnimating];
    
    NSDictionary* dict = [notification userInfo];

    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex2];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld", (long)[[dataDict objectForKey:@"rate"] integerValue]];
        
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex2] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    which_image_liked = (int)tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    //[SVProgressHUD show];
    
    activity.hidden=NO;
    [activity startAnimating];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"501":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];

    activity.hidden=YES;
    [activity stopAnimating];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
//        if ([shareObj.liked isEqualToString:@"no"]) {
//            shareObj.liked=@"yes";
//            int count =[shareObj.likes intValue];
//            count++;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//        }else{
//            shareObj.liked=@"no";
//            int count =[shareObj.likes intValue];
//            count--;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            
//            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//        }
//        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
//        // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark
#pragma mark Double Tab for like

-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture {
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked = (int)tappedRow.section;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
    
    if ([shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like {
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=like&item_id=12&uid=1
    //http://www.techintegrity.in/mystyle/post_like_unlike.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&action=unlike&id=2&uid=1
    
    activity.hidden=NO;
    [activity startAnimating];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"504":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    activity.hidden=YES;
    [activity stopAnimating];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        // NSLog(@"%@",shareObj.image_id);
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            // NSLog(@"%@",shareObj.array_liked_by);
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
            
        }
        
    }
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
  //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    //  [self.navigationController pushViewController:self.comments_list_viewObj animated:YES];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    //    if (result == MFMailComposeResultSent) {
    //
    //    }
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end