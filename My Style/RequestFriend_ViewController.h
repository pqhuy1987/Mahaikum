//
//  Notification_setting_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notification_setting_cell.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "DCRoundSwitch.h"

@interface RequestFriend_ViewController : UIViewController{

    UITableView *tbl_notification;
    NSMutableArray *array_reqeust;
    UILabel *lblheader;
    UIView *tbl_footer;
    
    NSMutableDictionary *dict_notification;
    
    UIAlertView *denayAlertViewObj;
}
@property (nonatomic,strong) IBOutlet  UITableView *tbl_notification;
@property (nonatomic,strong) NSMutableArray *array_reqeust;
@property (nonatomic,strong) IBOutlet UIView *tbl_footer;
@property (nonatomic,strong) IBOutlet UILabel *lblheader;
@property (nonatomic,strong) NSMutableDictionary *dict_notification;

@property (nonatomic,strong) UIAlertView *denayAlertViewObj;
@end
