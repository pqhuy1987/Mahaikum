//
//  Suggested_friend_list.h
//  My Style
//
//  Created by Tis Macmini on 6/15/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Suggested_friend_list_cell.h"
#import "image_collection_cell.h"
#import "Suggested_friend_list_share.h"

@interface Suggested_friend_list : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{

    NSMutableArray *array_friend;
    UITableView *tbl_friend;
    UILabel *lblheader;
    NSMutableArray *array_feeds;
    UIActivityIndicatorView *activity;
    UILabel *lblSpinnerBg;
}
@property(nonatomic,strong) NSMutableArray *array_feeds;
@property (nonatomic, strong) NSMutableDictionary *feeds_temp_dic;
@property(nonatomic,strong)NSMutableArray *array_friend;
@property(nonatomic,strong)IBOutlet UITableView *tbl_friend;
@property(nonatomic,strong)IBOutlet UILabel *lblheader;
@property(nonatomic,strong)UIActivityIndicatorView *activity;
@property(nonatomic,strong)UILabel *lblSpinnerBg;

@end
