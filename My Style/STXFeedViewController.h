//
//  STXFeedViewController.h
//
//  Created by Jesse Armand on 20/1/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

#import "Comments_list_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "Share_photo.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Likers_list_ViewController.h"

@interface STXFeedViewController : UIViewController<MFMailComposeViewControllerDelegate> {
    
    Comments_list_ViewController *comments_list_viewObj;
    NSMutableArray *array_feeds;
    
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    Share_photo *share_photo_view;
    Likers_list_ViewController *liker_list_viewObj;
    
    UIButton *btn_reload;
}

@property(nonatomic,retain) Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,retain) NSMutableArray *array_feeds;
@property(nonatomic,retain) Share_photo *share_photo_view;
@property(nonatomic,retain) Likers_list_ViewController *liker_list_viewObj;
@property(nonatomic,retain)UIButton *btn_reload;

@end
