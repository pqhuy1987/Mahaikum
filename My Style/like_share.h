//
//  like_share.h
//  My Style
//
//  Created by Tis Macmini on 4/20/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface like_share : NSObject{

    NSString *name;
    NSString *uid;
    NSString *username;
    NSString *img_path;
}
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *img_path;

@end
