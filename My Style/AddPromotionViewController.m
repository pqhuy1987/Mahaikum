//
//  AddPromotionViewController.m
//  
//
//  Created by Charles Moon on 12/22/15.
//
//

#import "AddPromotionViewController.h"
#import "TWPhotoPickerController.h"
#import "TGCameraViewController.h"

@interface AddPromotionViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, CLImageEditorDelegate>

@end

@implementation AddPromotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.posterDescription.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.1f];
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    if (self.editing)
    {
        [self.posterImage sd_setImageWithURL:[NSURL URLWithString:self.homeData.image_path] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.posterImage.image = image;
        }];
        self.posterDescription.text = self.homeData.description;
        self.posterImage.userInteractionEnabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([self.posterDescription.text isEqualToString:@"Write a description..."]) {
        self.posterDescription.text=@"";
    }
    
    return TRUE;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.posterDescription.text.length == 0) {
        self.posterDescription.text = @"Write a description...";
    }
}

- (void)postImage
{
    NSLog(@"s3 Start Time:%@",[NSDate date]);
    CGSize size = CGSizeMake(PostImageWidth, PostImageWidth * self.posterImage.image.size.height / self.posterImage.image.size.width);
    UIImage *newImage = [GlobalDefine imageWithImage:self.posterImage.image scaledToSize:size];
    NSData *img_data = UIImageJPEGRepresentation(newImage, 1.0);
    
    [SVProgressHUD show];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSString *dateString1 = [GlobalDefine toDateTimeStringFromDateWithFormat:[NSDate date] formatString:@"yyyy-MM-dd hh:mm:ss"];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                [SVProgressHUD dismiss];
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else {
                NSLog(@"s3 End Time:%@",[NSDate date]);
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postImagesAPIResponce:) name:@"14234" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostImagesAPIResponce:) name:@"-14234" object:nil];
                
                NSString *requestStr =[NSString stringWithFormat:@"%@promotion_post_images.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSString *strDesc=@"";
                if ([self.posterDescription.text isEqualToString:@"Write a caption..."]) {
                    //[request setPostValue:@"" forKey:@"description"];
                    strDesc=@"";
                    
                }else{
                    //[request setPostValue:self.txt_desc.text forKey:@"description"];
                    strDesc = self.posterDescription.text;
                }
                
                NSString *strLocation=@"";
                NSString *strLatitude=@"";
                NSString *strLongitude=@"";
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        strDesc,@"description",
                                        imageKeys,@"image",
                                        strLocation,@"location",
                                        strLatitude,@"lat",
                                        strLongitude,@"lng",
                                        nil];
                
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14234" :params];
            }
            
        });
    });
}

- (void)postImagesAPIResponce :(NSNotification *)notification {
    NSLog(@"API EndTime:%@",[NSDate date]);
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"Success");
        // [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeBlue title:@"Photo upload successfully." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Photo upload successfully." hideAfter:2];
        
        [self.navigationController popViewControllerAnimated:YES];
        
        [StaticClass saveToUserDefaults:@"1" :@"IMAGEUPLOADEDSUCCESSFULLY"];
        [self dismissViewControllerAnimated:NO completion:^{
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view_from_share" object:self];
            
        }];
    }
}

- (void)FailpostImagesAPIResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14234" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14234" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)postEdit
{
    NSString *requestStr =[NSString stringWithFormat:@"%@promotion_post_edit.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSString *strDesc;
    if ([self.posterDescription.text isEqualToString:@"Write a caption..."]) {
        //[request setPostValue:@"" forKey:@"description"];
        strDesc=@"";
        
    }else{
        //[request setPostValue:self.txt_desc.text forKey:@"description"];
        strDesc = self.posterDescription.text;
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            strDesc,@"description",
                            self.homeData.image_id, @"imageid",
                            nil];
    
    self.homeData.description = self.posterDescription.text;
    [SVProgressHUD show];
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
        if ([responseObject[@"success"] isEqualToString:@"1"])
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(shareObjsChangedInPromotion:shareObjs:)])
            {
                [self.delegate shareObjsChangedInPromotion:self shareObjs:self.homeData];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
        NSLog(@"%@", errorString);
        [SVProgressHUD dismiss];
    }];
}

- (IBAction)doneClicked:(id)sender
{
    if ([self.posterDescription.text isEqualToString:@"Write a description..."])
    {
        [ProgressHUD showError:@"Please write a description!" Interaction:NO];
        return;
    }
    
    if (!self.editing)
    {
        [self postImage];
    }
    else
    {
        [self postEdit];
    }
}

- (IBAction)backClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)imagePickerController:(UIImagePickerController*)reader didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.posterImage.image = chosenImage;
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:chosenImage];
    }];
}

- (void)presentImageEditorWithImage:(UIImage*)image
{
    //begin
    //[[CLImageEditorTheme theme] setBackgroundColor:[UIColor blackColor]];
    //[[CLImageEditorTheme theme] setToolbarColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    //[[CLImageEditorTheme theme] setToolbarTextColor:[UIColor whiteColor]];
    //[[CLImageEditorTheme theme] setToolIconColor:@"white"];
    
    //[[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    //[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //end
    
    CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
    editor.delegate = self;
    
    [self presentViewController:editor animated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image
{
    self.posterImage.image = image;
    [editor dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)imageViewTapped:(id)sender
{
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    
    photoPicker.cropBlock = ^(UIImage *image) {
        //do something
        self.posterImage.image = image;
        
        
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"0"]) {
            
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }
    };
    
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    [navCon setNavigationBarHidden:YES];
    
    [self presentViewController:navCon animated:YES completion:NULL];
    
}

//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//        UIImagePickerController *imgPickerController = [[UIImagePickerController alloc] init];
////        imgPickerController.allowsEditing = NO;
//        imgPickerController.delegate = self;
//        imgPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
//        imgPickerController.allowsEditing = YES;
//        imgPickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
//        [self presentViewController:imgPickerController animated:YES completion:nil];
//    }];
//    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"Select photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
////        UIImagePickerController *imgPickerController = [[UIImagePickerController alloc] init];
////        imgPickerController.allowsEditing = NO;
////        imgPickerController.delegate = self;
////        imgPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
////        [self presentViewController:imgPickerController animated:YES completion:nil];
//        
//        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
//        
//        photoPicker.cropBlock = ^(UIImage *image) {
//            //do something
//            UIImage *chosenImage = image;
//            self.posterImage.image = chosenImage;
//            // ADD: dismiss the controller (NB dismiss from the *reader*!)
//            [self presentImageEditorWithImage:chosenImage];
//        };
//        
//        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
//        [navCon setNavigationBarHidden:YES];
//        
//        [self presentViewController:navCon animated:YES completion:NULL];
//
//    }];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//    
//    [alert addAction:cameraAction];
//    [alert addAction:libraryAction];
//    [alert addAction:cancelAction];
//    [alert setModalPresentationStyle:UIModalPresentationPopover];
//    
//    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
//    popPresenter.sourceView = self.posterImage;
//    popPresenter.sourceRect = self.posterImage.bounds;
//    [self presentViewController:alert animated:YES completion:nil];



#pragma mark -
#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    UIImage *chosenImage = image;
    self.posterImage.image = chosenImage;
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:chosenImage];
    }];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    UIImage *chosenImage = image;
    self.posterImage.image = chosenImage;
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:chosenImage];
    }];
}

#pragma mark -
#pragma mark - TGCameraDelegate optional

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidSavePhotoAtPath:(NSURL *)assetURL
{
    NSLog(@"%s album path: %@", __PRETTY_FUNCTION__, assetURL);
}

- (void)cameraDidSavePhotoWithError:(NSError *)error
{
    NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
}


@end
