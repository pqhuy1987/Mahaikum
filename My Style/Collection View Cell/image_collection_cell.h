//
//  image_collection_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/2/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_data_share.h"

@interface image_collection_cell :UICollectionViewCell{

    UIImageView *img_photo;
    UILabel *lbl_price;
}
@property(nonatomic,strong)IBOutlet UIImageView *img_photo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_price;
@property (strong, nonatomic) UIImageView *avatarImage;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) Home_tableview_data_share *data;
@end
