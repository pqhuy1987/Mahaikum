//
//  Detail_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/30/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Detail_view_cell.h"
#import "Detail_view_complete_cell.h"
@interface Detail_ViewController : UIViewController{

    NSString *title1;
    NSString *desc;
    NSString *date;
    NSString *complete_flag;
    
    UITableView *tbl_detail;
    
}
@property(nonatomic,strong)IBOutlet UITableView *tbl_detail;
@property(nonatomic,strong)NSString *title1;
@property(nonatomic,strong)NSString *desc;
@property(nonatomic,strong)NSString *date;
@property(nonatomic,strong)NSString *complete_flag;
@end
