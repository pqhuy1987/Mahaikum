//
//  Sign_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"
#import "Register_ViewController.h"
#import "Forgot_password_ViewController.h"
#import "Facebook_Find_Friends_ViewController.h"
#import "ALTabBarController.h"
#import "Detail_ViewController.h"
#import "HyLoginButton.h"

@interface Sign_ViewController : UIViewController <UITabBarControllerDelegate>{
    Register_ViewController *register_viewObj;
    Forgot_password_ViewController *forgot_password_viewObj;
}

@property(nonatomic,strong)IBOutlet UIView *bottomView;
@property(nonatomic,strong)IBOutlet ALTabBarController *tabbar;
@property (strong, nonatomic) IBOutlet ALTabBarController *tabBarController;
@property(nonatomic,strong)IBOutlet HyLoginButton *btn_sign;
@property(nonatomic,strong)IBOutlet UIButton *btn_register;
@property (strong, nonatomic) IBOutlet UIButton *btn_forgotpassword;
@property (strong, nonatomic) IBOutlet UIButton *btn_facebook;
@property (strong, nonatomic) IBOutlet UILabel *lblRegister;

@property(nonatomic,strong)IBOutlet UILabel *lblLogin;
@property(nonatomic,strong)IBOutlet UIActivityIndicatorView *activityViewObj;
@property(nonatomic,strong) UIActivityIndicatorView *activity;
@property(nonatomic,strong) UILabel *lblSpinnerBg;

@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *logoBG;

@end
