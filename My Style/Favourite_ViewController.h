//
//  Favourite_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "News_feed_like_cell.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"
#import "News_feed_like_Share.h"
#import "News_feed_following_cell.h"
#import "Favourite_news_following_share.h"
#import "Favourite_feed_following_collection_cell.h"
#import "Collectionview_delegate.h"
#import "Image_detail_url.h"
#import "Image_detail.h"
#import "News_feed_like_following_cell.h"
#import "News_feed_comment_cell.h"
#import "MCSegmentedControl.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Find_invite_friend.h"
#import "Category_info_ViewController.h"

@interface Favourite_ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{

    Image_detail_url *image_detailViewObj;
    UIButton *btnreload;
    
    NSMutableArray *array_news_feed;
    NSMutableArray *array_follow;
    UITableView *tbl_feed;
    UITableView *tbl_news;
    //MCSegmentedControl *segment;
    UISegmentedControl *segment;
    Image_detail  *image_detail_viewObj;
    UIImageView *imgsegment1,*imgsegment2;
    
    LORichTextLabel *lbl_temp;
    LORichTextLabel *lbl_temp2;
    LORichTextLabelStyle *userStyle1;
    
    UIActivityIndicatorView *activity;
    UILabel *lblSpinnerBg;
    UIImageView *imgArrow;
    
    int is_following;
    int is_news;

    Find_invite_friend *find_inviteViewObj;
    Category_info_ViewController *categoryInfoView;
}

@property(nonatomic,strong) IBOutlet UIImageView *imgArrow;
@property(nonatomic,strong) UIButton *btnreload;
@property(nonatomic,strong) NSMutableArray *array_news_feed;
@property(nonatomic,strong) NSMutableArray *array_follow;
@property(nonatomic,strong) IBOutlet UITableView *tbl_feed;
@property(nonatomic,strong) IBOutlet  UITableView *tbl_news;
@property(nonatomic,strong) IBOutlet  UIImageView *imgsegment1,*imgsegment2;
//@property(nonatomic,retain) IBOutlet MCSegmentedControl *segment;
@property(nonatomic,strong) IBOutlet UISegmentedControl *segment;
@property(nonatomic,strong) Image_detail_url *image_detailViewObj;
@property(nonatomic,strong) Image_detail  *image_detail_viewObj;
@property(nonatomic,strong) LORichTextLabel *lbl_temp;
@property(nonatomic,strong) LORichTextLabel *lbl_temp2;
@property(nonatomic,strong) LORichTextLabelStyle *userStyle1;
@property(nonatomic,strong) UIActivityIndicatorView *activity;
@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property (strong, nonatomic) IBOutlet UILabel *lbl_noResult;
- (IBAction)find_following:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btn_find_following;
@property(nonatomic,strong) Find_invite_friend *find_inviteViewObj;
@property (nonatomic, strong) NSMutableArray *array_categories;

@end
