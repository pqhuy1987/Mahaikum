//
//  Edit_your_profile_share.h
//  My Style
//
//  Created by Tis Macmini on 5/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Edit_your_profile_share : NSObject{


    NSString *bio;
    NSString *email;
    NSString *facebook_id;
    NSString *gender;
    NSString *image;
    NSString *name;
    NSString *photos_status;
    NSString *user_id;
    NSString *username;
    NSString *weburl;
    NSString *phone;
    NSString *countryCode;
}
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *bio;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *facebook_id;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *photos_status;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *weburl;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *facebook_url;
@property (nonatomic, strong) NSString *twitter_url;
@property (nonatomic, strong) NSString *pinterest_url;
@property (nonatomic, strong) NSString *google_url;
@property (nonatomic, strong) NSString *instagram_url;
@property (nonatomic, strong) NSString *youtube_url;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *delivery_time;
@property (nonatomic, strong) NSString *working_hours;
@property (nonatomic, strong) NSString *shipping_fee;

@end
