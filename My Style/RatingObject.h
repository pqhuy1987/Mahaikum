//
//  RatingObject.h
//  
//
//  Created by Charles Moon on 1/27/16.
//
//

#import <Foundation/Foundation.h>

@interface RatingObject : NSObject

@property (nonatomic, strong) NSString *user_bio;
@property (nonatomic, strong) NSString *review_datecreated;
@property (nonatomic, strong) NSString *review_descs;
@property (nonatomic, strong) NSString *review_id;
@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) NSString *user_image;
@property (nonatomic, strong) NSString *user_name;
@property (nonatomic, strong) NSString *review_rating;
@property (nonatomic, strong) NSString *user_username;

@end
