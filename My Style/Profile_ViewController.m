//
//  Profile_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Profile_ViewController.h"
#import "AppDelegate.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "Comments_list_cell.h"
#import "ORGContainerCell.h"
#import "AddPromotionViewController.h"
#import "WriteReviewViewController.h"
#import "Category_info_ViewController.h"
#import "Chameleon.h"
#import "TWPhotoPickerController.h"
#import "MXScrollView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MDDirectionService.h"
#import "OpenInGoogleMapsController.h"
#import "Enums.h"
#import "MJCollectionViewCell.h"
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

@implementation UILabel (UILabel_Auto)

- (void)adjustHeight {
    
    if (self.text == nil) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, 0);
        return;
    }
    
    CGSize aSize = self.bounds.size;
    CGSize tmpSize = CGRectInfinite.size;
    tmpSize.width = aSize.width;
    
    tmpSize = [self.text sizeWithFont:self.font constrainedToSize:tmpSize];
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, aSize.width, tmpSize.height);
}
@end

#define screen_width     [[UIScreen mainScreen] bounds].size.width

static NSInteger cellClickIndex = 0;
NSInteger tblRefresOffset1 = 0;
static NSInteger selectedItem = 0;

@interface Profile_ViewController () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate,Comment_list_CellDelegate,Comment_list_viewcontrollerDelegate, UITableViewDelegate, UITableViewDataSource, ReviewTableViewDelegate, STXSectionHeaderViewDelegate,GMSMapViewDelegate, UIGestureRecognizerDelegate, STPopupPreviewRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {

    NSString *image_url;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    AddPromotionViewController *addPromotionView;
    HCSStarRatingView *starRatingView;
    UIScrollView *infoScrollView;
    CGSize reviewTableViewContentSize;
    Category_info_ViewController *categoryInfoView;
    CLLocationCoordinate2D  startlocation;
    CLLocationCoordinate2D destinationlocation;
    GMSMapView  *googlemapview;
    
    int currentSelectedIndex;
}

@property(nonatomic,strong)NSString *image_url;

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation Profile_ViewController
@synthesize collectionViewObj,imgfullscreenviewobj,lblSpinnerBg;
@synthesize view_header,img_photo,img_bg_firstcell,img_bg_scondcell,view_img_photo;
@synthesize view_header_tbl,img_photo_tbl,img_photo_tbl_bg,img_photo_tbl_bg_list,img_bg_firstcell_tbl,img_bg_scondcell_tbl,view_img_photo_tbl;

@synthesize tbl_news;
@synthesize mapviewObj;

@synthesize view_footer,img_spinner,view_footer_tbl,img_spinner_tbl;
@synthesize array_feeds,img_like_heart, array_categories, composed_feeds;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view,image_detail_viewObj,followers_list_viewObj,edit_your_profile_viewObj,setting_profileViewObj, category_list_view;

@synthesize profile_share_obj;
@synthesize lbl_total_followers,lbl_total_followings,lbl_total_photos,lbl_total_followers_tbl,lbl_total_followings_tbl,lbl_total_photos_tbl;
@synthesize lbl_name,lbl_name_tbl,image_url,lbl_url,lbl_description;
@synthesize photoPickerController;
@synthesize view_secondcell,view_secondcell_tbl;
@synthesize img_down_arrow,img_down_arrow_tbl,activity;

@synthesize username_style,lblheader,lblfollowers,lblfollowing,lblphotos,lblfollowers_tbl,lblfollowing_tbl,lblphotos_tbl;

@synthesize img_PostBG,img_tbl_PostBG;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refresh_profile_view_data:(NSNotification *)notification {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    currentSelectedIndex = 0;
    
#if 0
    // Initialize NKNumberBadgeView...
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(self.btn_request_friend.xPosition + 20, self.btn_request_friend.yPosition -20, 10,10)];
    number.value = 10;
    
   // self.btn_request_friend.frame = CGRectMake(0, 0, 70, 30);
    self.btn_request_friend.layer.cornerRadius = 8;
    
    [self.btn_request_friend addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:self.btn_request_friend];
    //self.navigationItem.leftBarButtonItem = proe;
#else
     _lbl_request_number.text = @"";
#endif
    
    self.composed_feeds = [[NSMutableArray alloc] init];
//    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.tbl_news registerClass:[ORGContainerCell class] forCellReuseIdentifier:@"ORGContainerCell"];
    
//    [self.tbl_news registerClass:[MJCollectionViewCell class] forCellWithReuseIdentifier:@"MJCollectionViewCell"];
//    [self.tbl_news registerClass:[ORGContainerCell class] forCellReuseIdentifier:@"ORGContainerCell"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refresh_profile_view_data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh_profile_view_data:) name:@"refresh_profile_view_data" object:nil];

    infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 66, self.view.frame.size.width, self.view.frame.size.height - 44 - 50 - 20)];
    
    collectionViewObj.parallaxHeader.view = self.headerView2;
    collectionViewObj.parallaxHeader.height = 177;
    collectionViewObj.parallaxHeader.mode = MXParallaxHeaderModeFill;
    collectionViewObj.parallaxHeader.minimumHeight = 0;

    tbl_news.parallaxHeader.view = self.headerView1;
    tbl_news.parallaxHeader.height = 177;
    tbl_news.parallaxHeader.mode = MXParallaxHeaderModeFill;
    tbl_news.parallaxHeader.minimumHeight = 0;

//    self.scrollview_collection.parallaxHeader.view = [NSBundle.mainBundle loadNibNamed:@"StarshipHeader" owner:self options:nil].firstObject;
//    self.scrollview_collection.parallaxHeader.height = 200;
//    self.scrollview_collection.parallaxHeader.mode = MXParallaxHeaderModeFill;
//    self.scrollview_collection.parallaxHeader.minimumHeight = 20;

    infoScrollView.showsVerticalScrollIndicator = YES;
    infoScrollView.scrollEnabled = YES;
    infoScrollView.userInteractionEnabled = YES;
    infoScrollView.hidden = YES;
    [self.view addSubview:infoScrollView];
    infoScrollView.contentSize = CGSizeMake(kViewWidth, self.view.frame.size.height * 1.12f);
    
    [infoScrollView addSubview:self.sellerInfoView];
    self.sellerInfoView.hidden = YES;
    self.sellerInfoView.frame = CGRectMake(0.0f, 0.0, kViewWidth, self.view.frame.size.height - 64.0f - 50.0f);
    
    [infoScrollView addSubview:self.reviewInfoView];
    self.reviewInfoView.hidden = YES;
    self.reviewInfoView.frame = CGRectMake(0.0f, 40.0, kViewWidth, 200.0);
    
    infoScrollView.parallaxHeader.view = self.headerView3;
    infoScrollView.parallaxHeader.height = 177;
    infoScrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    infoScrollView.parallaxHeader.minimumHeight = 0;
    
    starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.totalReview_riv.frame.size.width, self.totalReview_riv.frame.size.height)];
    starRatingView.opaque = NO;
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 0;
    starRatingView.value = 0;
    starRatingView.tintColor = [GlobalDefine colorWithHexString:@"1fae38"];
    starRatingView.userInteractionEnabled = NO;
    [self.totalReview_riv addSubview:starRatingView];
    self.totalReview_riv.backgroundColor = [UIColor clearColor];
    
    self.tbl_review_riv = [[ReviewTableView alloc] initWithFrame:CGRectMake(8.0f, 41.0f, kViewWidth-10, 100) style:UITableViewStylePlain];
    self.tbl_review_riv.customeDelegate = self;
    [self.reviewInfoView addSubview:self.tbl_review_riv];
    
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];
//    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
//    self.tbl_news.dataSource = dataSource;
//    self.tableViewDataSource = dataSource;
//    
//    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
//    self.tbl_news.delegate = delegate;
//    self.tableViewDelegate = delegate;
    
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:16.0];
    lblphotos.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowing.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowers.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    
    lblphotos_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowing_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowers_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    self.category_list_view = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil];
    self.chat_viewObj=[[Chat_ViewController alloc]initWithNibName:@"Chat_ViewController" bundle:nil];
    self.inbox_viewObj=[[Inbox_list_ViewController  alloc]initWithNibName:@"Inbox_list_ViewController" bundle:nil];
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.profile_share_obj =[[Profile_share alloc]init];
    
    self.array_feeds = [[NSMutableArray alloc]init];
    self.array_promotions = [[NSMutableArray alloc] init];
    [self.img_like_heart setHidden:YES];
    
    self.img_down_arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    self.img_down_arrow_tbl.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    
    self.followers_list_viewObj =[[Followers_list alloc]initWithNibName:@"Followers_list" bundle:nil];
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    allFriendViewObj = [[AllFriend_ViewController alloc] initWithNibName:@"AllFriend_ViewController" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    self.image_detail_viewObj=[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    
    self.edit_your_profile_viewObj =[[Edit_your_profile alloc]initWithNibName:@"Edit_your_profile" bundle:nil];
    self.setting_profileViewObj=[[Setting_profile alloc]initWithNibName:@"Setting_profile" bundle:nil];
    requestFrndViewObj = [[RequestFriend_ViewController alloc] initWithNibName:@"RequestFriend_ViewController" bundle:nil];
    
    array_spinner =@[[UIImage imageNamed:@"feedstate-spinner-frame01.png"],[UIImage imageNamed:@"feedstate-spinner-frame02.png"],[UIImage imageNamed:@"feedstate-spinner-frame03.png"],[UIImage imageNamed:@"feedstate-spinner-frame04.png"],[UIImage imageNamed:@"feedstate-spinner-frame05.png"],[UIImage imageNamed:@"feedstate-spinner-frame06.png"],[UIImage imageNamed:@"feedstate-spinner-frame07.png"],[UIImage imageNamed:@"feedstate-spinner-frame08.png"]];
    
    self.img_spinner.animationImages =array_spinner;
    self.img_spinner.animationRepeatCount = HUGE_VAL;
    self.img_spinner.animationDuration=1.0f;
    
    self.img_spinner_tbl.animationImages =array_spinner;
    self.img_spinner_tbl.animationRepeatCount = HUGE_VAL;
    self.img_spinner_tbl.animationDuration=1.0f;
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    self.mapviewObj =[[Show_map_ViewController alloc]initWithNibName:@"Show_map_ViewController" bundle:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];

    UINib *cellNib_tbl = [UINib nibWithNibName:@"MJCollectionViewCell" bundle:nil];
    [self.tbl_news registerNib:cellNib_tbl forCellWithReuseIdentifier:@"MJCollectionViewCell"];
    
     UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    [self.tbl_news registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];

    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    [self.tbl_news registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
//    lbl_url = [[UILabel alloc]initWithFrame:CGRectMake(10, 207, screen_width-20, 30)];
//    [lbl_url setNumberOfLines:2];
//    [lbl_url setBackgroundColor:[UIColor clearColor]];
//    lbl_url.font = [UIFont systemFontOfSize:14.0f];
//    [lbl_url setTextAlignment:NSTextAlignmentLeft];
//    [lbl_url setTextColor:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
//
//    buttonUrl = [UIButton buttonWithType:UIButtonTypeCustom];
//    [buttonUrl addTarget:self action:@selector(urlSelected:) forControlEvents:UIControlEventTouchUpInside];
//    [buttonUrl setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//    [buttonUrl setFrame:lbl_url.frame];
//    [buttonUrl setBackgroundColor:[UIColor clearColor]];
//    
//    [self.view_header addSubview:lbl_url];
//    [self.view_header addSubview:buttonUrl];
    
//    lbl_description = [[UILabel alloc]initWithFrame:CGRectMake(10, 165, screen_width-20, 50)];
//    [lbl_description setNumberOfLines:0];
//    [lbl_description setBackgroundColor:[UIColor clearColor]];
//    lbl_description.font = [UIFont systemFontOfSize:12.0f];
//    [lbl_description setTextAlignment:NSTextAlignmentLeft];
//    [self.view_header addSubview:lbl_description];
    
//    self.tbl_news.tableHeaderView = self.view_header_tbl;
//    self.tbl_news.tableFooterView = self.view_footer_tbl;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"301" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"301" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-301" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-301" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"303" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"303" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-303" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-303" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"304" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"304" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-304" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-304" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"305" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_Responce:) name:@"305" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-305" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-305" object:nil];
    
    image_upload_flag=0;
    
    self.username_style = [LORichTextLabelStyle styleWithFont:[UIFont boldSystemFontOfSize:17.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    self.username_style.font = [UIFont fontWithName:@"Roboto-Medium" size:13.0];
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    
    LORichTextLabelStyle  *url_style = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    
    [url_style addTarget:self action:@selector(urlSelected:)];

    [self.lbl_name addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"www."];
    [self.lbl_name addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name addStyle:url_style forPrefix:@"Https://"];
    
    
    [self.lbl_name addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Https://"];
    
    
    [self.lbl_name_tbl addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name_tbl addStyle:atStyle forPrefix:@"@"];
    
    img_photo.layer.cornerRadius = 55.0f;
    img_photo.layer.masksToBounds = YES;
    
    img_photo_tbl.layer.cornerRadius = 55.0f;
    img_photo_tbl.layer.masksToBounds = YES;
    
    self.img_photo_siv.layer.cornerRadius = 55.0f;
    self.img_photo_siv.layer.masksToBounds =YES;
    
    self.map_viewObj=[[GoogleMap_ViewController alloc]initWithNibName:@"GoogleMap_ViewController" bundle:nil];
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRight:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];

    UISwipeGestureRecognizer *gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureLeft:)];
    [gesture1 setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:gesture1];
   
    
}

#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];

    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}

- (void)gestureLeft:(id)sender
{
    currentSelectedIndex = ( currentSelectedIndex + 1 ) % 4;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
    else if ( currentSelectedIndex == 2 )
    {
        [self btn_reviewinfo_click:self];
    }
    else if ( currentSelectedIndex == 3 )
    {
        [self btn_sellerinfo_click:self];
    }
}

- (void)gestureRight:(id)sender
{
    if ( currentSelectedIndex == 0 )
        currentSelectedIndex = 4;
    
    currentSelectedIndex = ( currentSelectedIndex - 1 ) % 4;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
    else if ( currentSelectedIndex == 2 )
    {
        [self btn_reviewinfo_click:self];
    }
    else if ( currentSelectedIndex == 3 )
    {
        [self btn_sellerinfo_click:self];
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;    
    self.tbl_news.tag = 11001;
    self.lbl_request_number.text = @"";

    //News feed
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItemFromCollectionView:) name:@"didSelectItemFromCollectionView" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (image_upload_flag ==0) {
        [self get_user_info];
    }
    [self get_notification_info];
    
}

-(void)get_notification_info {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMutualFriendResponce:) name:@"14224" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetMutualFriendResponce:) name:@"-14224" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_mutual_friend.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14224" :params];
}

-(void)getMutualFriendResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSString *str=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"str:%@",str);
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        if([[result valueForKey:@"success"] isEqualToString:@"1"]) {
            if (dict.count > 0)
                self.lbl_request_number.text = [NSString stringWithFormat:@"%ld", dict.count];
        }
        else
            self.lbl_request_number.text = @"";
    }
    else
        self.lbl_request_number.text = @"";
}

-(void)FailgetMutualFriendResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didSelectItemFromCollectionView" object:nil];
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObj=[array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked = (int)userActionCell.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}
-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}
-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:userActionCell.tag];
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}
-(IBAction)didClickShare_btn:(id)sender
{
    NSInteger tag =((UIButton *)sender).tag;
    which_image_delete = (int)tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    // if ([shareObj.image_owner isEqualToString:@"yes"]) {
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:nil];
    
    [sheet addButtonWithTitle:@"Copy Profile URL" block:^{
        NSString*objCopyProfileUrl=[NSString stringWithFormat:@"http://www.mahalkum.com/user_profile.html?uname=%@",shareObj.username];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =objCopyProfileUrl;
        NSLog(@"%@",objCopyProfileUrl);
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This profile's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
    return;
    
}
-(void)userWillShare:(STXUserActionCell *)userActionCell {
    NSInteger tag = (int)userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:commentCell.tag];
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag = (int)likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
   // pushPopFlg=1;
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
   // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}
-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}


- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
   // pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (IBAction)btnProfileClicked:(id)sender {
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld",(long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
    }];
    
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:posterLike.tag];
    which_image_liked = (int)posterLike.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];
}

#pragma mark - Social buttons
- (IBAction)socialFacebookClicked:(id)sender {
    NSString *url = self.profile_share_obj.facebook_url;
    [self openUrl:url];
}

- (IBAction)socialTwitterClicked:(id)sender {
    NSString *url = self.profile_share_obj.twitter_url;
    [self openUrl:url];
}

- (IBAction)socialPinterestClicked:(id)sender {
    NSString *url = self.profile_share_obj.pinterest_url;
    [self openUrl:url];
}

- (IBAction)socialGoogleClicked:(id)sender {
    NSString *url = self.profile_share_obj.google_url;
    [self openUrl:url];
}
- (IBAction)socialInstagramClicked:(id)sender {
    NSString *url = self.profile_share_obj.instagram_url;
    [self openUrl:url];
}
- (IBAction)socialYoutubeClicked:(id)sender {
    NSString *url = self.profile_share_obj.youtube_url;
    [self openUrl:url];
}

- (void)openUrl:(NSString *)urlString
{
    NSURL *webpageUrl;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        webpageUrl = [NSURL URLWithString:urlString];
    } else {
        webpageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
     UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
     KINWebBrowserViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
//     [webBrowser setDelegate:self];
     webBrowser.showsURLInNavigationBar = YES;
     webBrowser.tintColor = [UIColor whiteColor];
     webBrowser.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"colarge_ipad"]];
     webBrowser.showsPageTitleInNavigationBar = NO;
     webBrowser.showsURLInNavigationBar = NO;
     [self presentViewController:webBrowserNavigationController animated:YES completion:nil];
     
     [webBrowser loadURL:webpageUrl];
}

#pragma mark - Photos count click
-(IBAction)btn_photos_count_click:(id)sender{

    [self.collectionViewObj setContentOffset:CGPointMake(100, self.view_header.frame.size.height-68)];
   
    [self.tbl_news setContentOffset:CGPointMake(0, self.view_header.frame.size.height-68)];
}

#pragma mark - Followes count click

- (IBAction)btn_categories_count_click:(id)sender {
    self.category_list_view.user_id =self.profile_share_obj.user_id;
    self.category_list_view.nav_title = @"Categories";
    self.category_list_view.editable = YES;
    
    [self.navigationController pushViewController:self.category_list_view animated:YES];
}

-(IBAction)btn_followers_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"FOLLOWERS";
    
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

#pragma mark - Following count click
-(IBAction)btn_following_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"FOLLOWING";
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

#pragma mark - Edit your profile click
-(IBAction)btn_edit_your_profile_click:(id)sender{
    self.edit_your_profile_viewObj.is_reload_data=1;
    
    [self.navigationController pushViewController:self.edit_your_profile_viewObj animated:YES];
}

#pragma mark - Setting Button click
-(IBAction)btn_setting_click:(id)sender{
    [self.navigationController pushViewController:self.setting_profileViewObj animated:YES];
}

#pragma mark - Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
}

#pragma mark - Get user Profile Info
- (void)get_user_info{
    [SVProgressHUD dismiss];
    
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login=%@&flag=1",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_profile.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"305":nil];
}

- (void)get_user_info_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if([[result valueForKey:@"success"] isEqualToString:@"1"])
    {
        NSDictionary *dict = [result valueForKey:@"data"];
        
        self.profile_share_obj.user_id = [dict valueForKey:@"user_id"];
        self.profile_share_obj.username = [StaticClass urlDecode:[dict valueForKey:@"username"]];
        self.profile_share_obj.name = [StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_share_obj.email = [StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_share_obj.facebook_id = [dict valueForKey:@"facebook_id"];
        self.profile_share_obj.total_followers = [dict valueForKey:@"total_followers"];
        self.profile_share_obj.total_following = [dict valueForKey:@"total_following"];
        self.profile_share_obj.total_categories = [dict valueForKey:@"total_categories"];
        self.profile_share_obj.total_photos =[dict valueForKey:@"total_images"];
        self.profile_share_obj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_share_obj.phone = [dict valueForKey:@"phone"];
        
        self.profile_share_obj.delivery_time = [StaticClass urlDecode:[dict valueForKey:@"delivery_time"]];

        self.profile_share_obj.working_hours = [StaticClass urlDecode:[dict valueForKey:@"working_hours"]];
        
        self.profile_share_obj.shipping_fee = [StaticClass urlDecode:[dict valueForKey:@"shipping_fee"]];

        self.profile_share_obj.facebook_url =[StaticClass urlDecode:[dict valueForKey:@"facebook_url"]];
        self.profile_share_obj.twitter_url =[StaticClass urlDecode:[dict valueForKey:@"twitter_url"]];
        self.profile_share_obj.pinterest_url =[StaticClass urlDecode:[dict valueForKey:@"pinterest_url"]];
        self.profile_share_obj.google_url =[StaticClass urlDecoding:[dict valueForKey:@"google_url"]];
        self.profile_share_obj.instagram_url =[StaticClass urlDecode:[dict valueForKey:@"instagram_url"]];
        self.profile_share_obj.youtube_url =[StaticClass urlDecode:[dict valueForKey:@"youtube_url"]];
        self.profile_share_obj.latitude =[StaticClass urlDecode:[dict valueForKey:@"latitude"]];
        self.profile_share_obj.longitude =[StaticClass urlDecode:[dict valueForKey:@"longitude"]];
        
        self.profile_share_obj.bio=[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        
        self.profile_share_obj.web_url=[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_tbl_bg sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl_bg_list sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_siv sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_tbl_bg_siv sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        if (self.profile_share_obj.total_followers.length==0) {
            self.lbl_total_followers.text=@"0";
            self.lbl_total_followers_tbl.text=@"0";
            self.lbl_total_followers_siv.text = @"0";
        }else{
            self.lbl_total_followers.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_tbl.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_siv.text=self.profile_share_obj.total_followers;
        }
        
        if (self.profile_share_obj.total_following.length==0) {
            self.lbl_total_followings.text=@"0";
            self.lbl_total_followings_tbl.text=@"0";
            self.lbl_total_followings_siv.text = @"0";
        }else{
            self.lbl_total_followings.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_tbl.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_siv.text=self.profile_share_obj.total_following;
        }
        
        if (self.profile_share_obj.total_photos.length==0) {
            self.lbl_total_photos.text=@"0";
            self.lbl_total_photos_tbl.text=@"0";
            self.lbl_total_photos_siv.text = @"0";
        }else{
            self.lbl_total_photos.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_tbl.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_siv.text=self.profile_share_obj.total_photos;
        }
        
        if (self.profile_share_obj.total_categories.length == 0) {
            self.lbl_total_categories.text = @"0";
            self.lbl_total_categories_tbl.text = @"0";
            self.lbl_total_categories_siv.text = @"0";
        }
        else {
            self.lbl_total_categories.text=self.profile_share_obj.total_categories;
            self.lbl_total_categories_tbl.text=self.profile_share_obj.total_categories;
            self.lbl_total_categories_siv.text=self.profile_share_obj.total_categories;
        }
        
        NSMutableString *str =[[NSMutableString alloc]init];
        self.objUserNameLbl.text=self.profile_share_obj.name;
        if (self.profile_share_obj.name.length==0) {
            [self.lbl_name setText:@""];
            [self.lbl_name_tbl setText:@""];
        }
        else {
           // [str appendFormat:@"%@ \n\n",self.profile_share_obj.name];
            for (NSString *name in [self.profile_share_obj.name componentsSeparatedByString:@" "]) {
                if (name.length>0) {
                    [self.lbl_name addStyle:self.username_style forPrefix:name];
                    [self.lbl_name_tbl addStyle:self.username_style forPrefix:name];
                }
            }
        }
        
        int fl=0;
        if (!(self.profile_share_obj.bio.length == 0)) {
            [str appendFormat:@"%@",self.profile_share_obj.bio];
        }
        else {
            fl=1;
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        }
        
        self.lbl_bio_siv.text = str;
        [self.lbl_bio_siv adjustHeight];
        
        if (fl==1) {
            [str appendFormat:@"\n"];
        }
        
        if (!(self.profile_share_obj.email.length == 0))
        {
            self.lbl_email_siv.text = self.profile_share_obj.email;
        }
        if (!(self.profile_share_obj.phone.length == 0))
        {
            self.lbl_mobile_siv.text = self.profile_share_obj.phone;
        }

        if (!(self.profile_share_obj.delivery_time.length == 0))
        {
            self.lbl_delivery_siv.text = self.profile_share_obj.delivery_time;
        }
        else
            self.lbl_delivery_siv.text =@"";

        if (!(self.profile_share_obj.working_hours.length == 0))
        {
            self.lbl_hours_siv.text = self.profile_share_obj.working_hours;
        }
        else
            self.lbl_hours_siv.text =@"";
        if (!(self.profile_share_obj.shipping_fee.length == 0))
        {
            self.lbl_fee_siv.text = self.profile_share_obj.shipping_fee;
        }
        else
            self.lbl_fee_siv.text = @"";
        
        [self.lbl_delivery_siv adjustHeight];
        [self.lbl_hours_siv adjustHeight];
        [self.lbl_fee_siv adjustHeight];
        
        self.lbl_hours_title_siv.frame = CGRectMake(self.lbl_hours_title_siv.frame.origin.x, self.lbl_delivery_siv.origin.y+self.lbl_delivery_siv.size.height+11, self.lbl_hours_title_siv.size.width, self.lbl_hours_title_siv.size.height);

        self.lbl_hours_siv.frame = CGRectMake(self.lbl_hours_siv.frame.origin.x, self.lbl_hours_title_siv.origin.y+self.lbl_hours_title_siv.size.height+3, self.lbl_hours_siv.size.width, self.lbl_hours_siv.size.height);

        self.lbl_fee_title_siv.frame = CGRectMake(self.lbl_fee_title_siv.frame.origin.x, self.lbl_hours_siv.origin.y+self.lbl_hours_siv.size.height+11, self.lbl_fee_title_siv.size.width, self.lbl_fee_title_siv.size.height);

        self.lbl_fee_siv.frame = CGRectMake(self.lbl_fee_siv.frame.origin.x, self.lbl_fee_title_siv.origin.y+self.lbl_fee_title_siv.size.height+3, self.lbl_fee_siv.size.width, self.lbl_fee_siv.size.height);
        
        self.img_delivery_siv.frame = CGRectMake(self.img_delivery_siv.frame.origin.x, self.lbl_delivery_title_siv.frame.origin.y+1, self.img_delivery_siv.frame.size.width, self.img_delivery_siv.frame.size.height);
        
        self.img_hours_siv.frame = CGRectMake(self.img_hours_siv.frame.origin.x, self.lbl_hours_title_siv.frame.origin.y-2, self.img_hours_siv.frame.size.width, self.img_hours_siv.frame.size.height);
        
        self.img_fee_siv.frame = CGRectMake(self.img_fee_siv.frame.origin.x, self.lbl_fee_title_siv.frame.origin.y-2, self.img_fee_siv.frame.size.width, self.img_fee_siv.frame.size.height);
        
        self.view_content.frame = CGRectMake(self.view_content.frame.origin.x, self.lbl_bio_siv.frame.origin.y+_lbl_bio_siv.frame.size.height + 10, self.view_content.frame.size.width, self.lbl_fee_siv.origin.y+self.lbl_fee_siv.size.height + 50);
        
        [self autoChangeScrollViewContentSize];
        
        if (!(self.profile_share_obj.web_url.length == 0)) {
            self.lbl_website_siv.text = self.profile_share_obj.web_url;
            [str appendFormat:@"%@",self.profile_share_obj.web_url];
        }
        else {
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        [self setPlaceMark:self.profile_share_obj.latitude loStr:self.profile_share_obj.longitude];
        
        NSLog(@"str:%@",str);
        NSString *strWebUrl = [NSString stringWithFormat:@"%@",self.profile_share_obj.web_url];
        
        if ([strWebUrl isEqualToString:@""] || [strWebUrl isEqualToString:@"null"] || strWebUrl ==nil) {
            [buttonUrl setUserInteractionEnabled:NO];
        }
        else{
            [buttonUrl setTitle:strWebUrl forState:UIControlStateNormal];
            lbl_url.text = strWebUrl;
        }
        
        NSString *strDescription = [NSString stringWithFormat:@"%@",self.profile_share_obj.bio];
        
        if ([strDescription isEqualToString:@""] || [strDescription isEqualToString:@"null"] || strDescription ==nil) {
            
        }
        else{
            lbl_description.text = strDescription;
        }
        
        NSLog(@"%f , %f,",self.lbl_name.height,self.lbl_name.frame.size.height);
        
        if (self.lbl_name.height>22) {
            
//            [self.tbl_news setTableHeaderView:self.view_header_tbl];
        }
        [self get_categories_list];
        
        if ( self.lbl_bio_siv.hidden == NO )
            [self showsSellerInfoSubViews];
    }
    else
    {
        [SVProgressHUD dismiss];
    }
}

- (void)setPlaceMark:(NSString *)laStr loStr:(NSString *)loStr
{
    CGFloat latitude = [laStr floatValue];
    CGFloat longitude = [loStr floatValue];
    
//    self.mapView.mapType = MKMapTypeStandard;
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latitude, longitude);
//    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
//    MKCoordinateRegion regionToDisplay = MKCoordinateRegionMake(center, span);
//    [self.mapView setRegion:regionToDisplay];
//    
//    MyAnnotation *ann = [[MyAnnotation alloc] init];
//    ann.title = self.profile_share_obj.username;
//    ann.coordinate = center;
//    [self.mapView addAnnotation:ann];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:10];
    GMSMapView *view = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
    view.settings.scrollGestures = NO;
    view.settings.zoomGestures = NO;
//    view.myLocationEnabled = YES;
    view.delegate = self;
    [self.mapView removeSubviews];
    [self.mapView addSubview:view];
    CLLocationCoordinate2D  position = CLLocationCoordinate2DMake(latitude, longitude);
    destinationlocation = position;
    GMSMarker   *marker = [GMSMarker markerWithPosition:position];
    marker.title = [NSString stringWithFormat:@"%@ >", profile_share_obj.name];
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.map = view;
    marker.userData = marker;
    googlemapview = view;
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    self.map_viewObj.str_latitude=self.profile_share_obj.latitude;
    self.map_viewObj.str_longitude=self.profile_share_obj.longitude;
    self.map_viewObj.str_name = self.profile_share_obj.name;
    
    [self.navigationController pushViewController:self.map_viewObj animated:YES];
}

- (IBAction)lbl_email_sivTapped:(id)sender {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setToRecipients:[NSArray arrayWithObject:self.lbl_email_siv.text]];
    [picker setSubject:@"Hello"];
    [self presentViewController:picker animated:NO completion:nil];
}

- (IBAction)lbl_website_sivTapped:(id)sender {
    NSString *urlString = self.lbl_website_siv.text;
    NSURL *webpageUrl;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        webpageUrl = [NSURL URLWithString:urlString];
    } else {
        webpageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:webpageUrl]) {
        [[UIApplication sharedApplication] openURL:webpageUrl];
    }
    else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invaild Url!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)lbl_mobile_sivTapped:(id)sender {
    NSString *phNo = self.lbl_mobile_siv.text;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (void)get_categories_list
{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&login_id=%@", salt, sig, self.profile_share_obj.user_id, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];

    NSString *requestStr = [NSString stringWithFormat:@"%@get_category_followers.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"])
        {
            self.array_categories = [[NSMutableArray alloc] init];
            
            NSArray *dataArray = responseObject[@"data"];
            for(NSDictionary *data in dataArray)
            {
                CategoryObject *object = [[CategoryObject alloc] init];
                object.categoryId = data[@"id"];
                object.categoryName = data[@"name"];
                object.categoryDescs = data[@"description"];
                object.categoryFollowers = data[@"followers"];
                object.categoryUserFollower = data[@"user_follower"];
                [self.array_categories addObject:object];
            }
            [self get_news_feed];
        }
        else {
            [self.array_categories removeAllObjects];
            [self get_news_feed];
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get categories" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark - Get News Feed
- (void)get_news_feed
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&off=%ld",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],(long)tblRefresOffset1];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        tblRefresOffset1 =[[responseObject valueForKey:@"offset"] integerValue];
        [self.array_feeds removeAllObjects];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            self.feeds_temp_dic = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.price = dict[@"price"];
                shareObj.currency = dict[@"currency"];
                shareObj.sold = [dict[@"sold"] integerValue];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                }
                else{
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if (![[dict valueForKey:@"description"] isEqualToString:@""])
                {
                    Comment_share *obj =[[Comment_share alloc]init];
                    obj.uid =shareObj.uid;
                    obj.username =shareObj.username;
                    obj.name =shareObj.name;
                    obj.image_url=shareObj.user_image;
                    obj.comment_desc=shareObj.description;
                    obj.datecreated=shareObj.datecreated;
                    [shareObj.array_comments addObject:obj];
                }
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        [shareObj.array_comments addObject:obj];
                    }
                }
                else
                {
                    
                }
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (shareObj.image_path.length !=0) {
                    [self.array_feeds addObject:shareObj];
                }
                
                NSMutableArray *tempArray = [self.feeds_temp_dic[shareObj.category_id] mutableCopy];
                if (!tempArray)
                    tempArray = [[NSMutableArray alloc] init];
                [tempArray addObject:shareObj];
                self.feeds_temp_dic[shareObj.category_id] = [tempArray copy];
                
            }
        }
        
        [self composeCategoryPosts];
        [self get_promotion_list];
    } failure:^(NSString *errorString) {
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        [SVProgressHUD dismiss];
    }];
}

- (void)composeCategoryPosts
{
    self.composed_feeds = [[NSMutableArray alloc] initWithCapacity:self.array_categories.count];
    for (NSInteger index = 0; index < self.array_categories.count; index ++)
    {
        CategoryObject *obj = self.array_categories[index];
        NSMutableArray *tempArray = [self.feeds_temp_dic[obj.categoryId] mutableCopy];
        if (!tempArray)
            tempArray = [[NSMutableArray alloc] init];
        self.composed_feeds[index] = [tempArray copy];
    }
    
    [self.tbl_news reloadData];
}

- (void)get_promotion_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&flag=1&off=0&login_id=%@", salt, sig, self.profile_share_obj.user_id, LOGINEDUSERID];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_promotion_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@", requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        [self.array_promotions removeAllObjects];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO promotion images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            self.feeds_temp_dic = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                }
                else{
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[tempdict valueForKey:@"username"];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        if ([shareObj.username isEqualToString:obj.username] && [shareObj.description isEqualToString:obj.comment_desc])
                            continue;
                        
                        [shareObj.array_comments addObject:obj];
                    }
                }
                else
                {
                    
                }
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (shareObj.image_path.length !=0) {
                    [self.array_promotions addObject:shareObj];
                }
            }
        }
        [self get_reviews];
        
        [self.collectionViewObj reloadData];
        [self.tbl_news reloadData];
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get promotions" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (void)get_reviews
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@", salt, sig, self.profile_share_obj.user_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_reviews.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@", requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"])
        {
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO reviews found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        
        self.array_reviews = [[NSMutableArray alloc] init];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            starRatingView.value = [responseObject[@"total_rating"] floatValue];
            self.lbl_total_reviews_riv.text = [NSString stringWithFormat:@"%@ reviews", responseObject[@"total_amount"]];
            self.lbl_total_reviews_value_riv.text = [NSString stringWithFormat:@"(%.1f)", [responseObject[@"total_rating"] floatValue]];
            
            for (NSDictionary *dic in (NSArray *)responseObject[@"data"])
            {
                RatingObject *object = [[RatingObject alloc] init];
                object.user_bio = [StaticClass urlDecode:dic[@"bio"]];
                object.review_datecreated = [StaticClass urlDecode:dic[@"datecreated"]];
                object.review_descs = [StaticClass urlDecode:dic[@"descs"]];
                object.review_id = dic[@"review_id"];
                object.user_id = dic[@"id"];
                object.user_image = [StaticClass urlDecode:dic[@"image"]];
                object.user_name = dic[@"name"];
                object.user_username = dic[@"username"];
                object.review_rating = dic[@"rating"];
                [self.array_reviews addObject:object];
            }
            
        }
        else
        {
            starRatingView.value = 0.0;
            self.lbl_total_reviews_riv.text = @"0 reviews";
            self.lbl_total_reviews_value_riv.text = @"(0)";
        }
       
        if (selectedItem == 2)
        {
        if ([self.array_reviews count] == 0)
        {
            [self.lbl_no_reviews_riv setHidden:NO];
            
            [self.lbl_total_reviews_riv setHidden:YES];
            [self.lbl_total_reviews_value_riv setHidden:YES];
            [self.totalReview_riv setHidden:YES];
        }
        else
        {
            [self.lbl_no_reviews_riv setHidden:YES];
            
            [self.lbl_total_reviews_riv setHidden:NO];
            [self.lbl_total_reviews_value_riv setHidden:NO];
            [self.totalReview_riv setHidden:NO];
        }
        }
        else
        {
            [self.lbl_no_reviews_riv setHidden:YES];
            [self.lbl_total_reviews_riv setHidden:YES];
            [self.lbl_total_reviews_value_riv setHidden:YES];
            [self.totalReview_riv setHidden:YES];
        }
        
        [self.tbl_review_riv setTblDataSource:self.array_reviews];
        [self.tbl_review_riv reloadData];
        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get reviews" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark UITableview Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.tag == 11001) {
        float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
        if (bottomEdge >= scrollView.contentSize.height) {
            // we are at the end
//            [self get_news_feed];
            NSLog(@"Scroll In Table last cell scrolled");
        }
    }
}

#pragma mark - Collectionview & Tableview  choose for show 
-(IBAction)btn_collectionview_click:(id)sender{
    currentSelectedIndex = 0;
    
    [self.collectionViewObj setContentOffset:self.tbl_news.contentOffset];

    self.tbl_news.hidden = NO;
    self.collectionViewObj.hidden = YES;
    infoScrollView.hidden = YES;
    selectedItem = 0;
}

-(IBAction)btn_tableview_click:(id)sender{
    currentSelectedIndex = 1;
    
    [self.tbl_news setContentOffset:self.collectionViewObj.contentOffset];
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden = NO;
    infoScrollView.hidden = YES;
    selectedItem = 1;
}

- (IBAction)btn_sellerinfo_click:(id)sender
{
    currentSelectedIndex = 3;
    
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden = YES;
    self.selectionTapImageView_siv.image = [UIImage imageNamed:@"Seller Info View Selected.png"];
    [self showsSellerInfoSubViews];
    infoScrollView.hidden = NO;
    self.sellerInfoView.hidden = NO;
    self.reviewInfoView.hidden = YES;
    [self autoChangeScrollViewContentSize];
    selectedItem = 3;
}

- (IBAction)btn_reviewinfo_click:(id)sender
{
    currentSelectedIndex = 2;
    
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden = YES;
    self.selectionTapImageView_siv.image = [UIImage imageNamed:@"Review Selected.png"];
    [self hiddenSellerInfoSubViews];
    infoScrollView.hidden = NO;
    self.sellerInfoView.hidden = NO;
    self.reviewInfoView.hidden = NO;
    [self autoChangeScrollViewContentSize];
    selectedItem = 2;
    
    if ([self.array_reviews count] == 0)
    {
        [self.lbl_no_reviews_riv setHidden:NO];
        
        [self.lbl_total_reviews_riv setHidden:YES];
        [self.lbl_total_reviews_value_riv setHidden:YES];
        [self.totalReview_riv setHidden:YES];
    }
    else
    {
        [self.lbl_no_reviews_riv setHidden:YES];
        
        [self.lbl_total_reviews_riv setHidden:NO];
        [self.lbl_total_reviews_value_riv setHidden:NO];
        [self.totalReview_riv setHidden:NO];
    }
}

#pragma mark - Review Table View Delegate
- (void)reviewTableViewContentSizeChanged:(ReviewTableView *)tableView contentSize:(CGSize)contentSize
{
    reviewTableViewContentSize = contentSize;
    [self autoChangeScrollViewContentSize];
}

- (void)autoChangeScrollViewContentSize
{
    if (self.reviewInfoView.hidden)
    {
//        infoScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height * 1.12f);
        infoScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height +self.lbl_fee_siv.frame.size.height +self.lbl_hours_siv.frame.size.height +self.lbl_delivery_siv.frame.size.height +self.lbl_bio_siv.frame.size.height);
    }
    else
    {
        CGFloat newContentHeight = self.reviewInfoView.frame.origin.y + self.tbl_review_riv.frame.origin.y + self.tbl_review_riv.frame.size.height * 1.5f;
        infoScrollView.contentSize = CGSizeMake(self.view.frame.size.width, newContentHeight > self.view.frame.size.height * 1.12f ? newContentHeight : self.view.frame.size.height * 1.12f);
    }
    
    CGRect frame = self.reviewInfoView.frame;
    frame.size.height = infoScrollView.contentSize.height - 230.0;
    self.reviewInfoView.frame = frame;
    
    frame = self.sellerInfoView.frame;
    frame.size.height = infoScrollView.contentSize.height;
    self.sellerInfoView.frame = frame;
}

- (void)hiddenSellerInfoSubViews
{
    self.lbl_bio_siv.hidden = YES;
    self.lbl_mobile_siv.hidden = YES;
    self.lbl_email_siv.hidden = YES;
    self.lbl_website_siv.hidden = YES;
    self.lbl_mobile_title_siv.hidden = YES;
    self.lbl_email_title_siv.hidden = YES;
    self.lbl_website_title_siv.hidden = YES;
    self.img_phone_siv.hidden = YES;
    self.img_mail_siv.hidden = YES;
    self.img_web_siv.hidden = YES;
    self.btn_phone_siv.hidden = YES;
    self.btn_mail_siv.hidden = YES;
    self.btn_web_siv.hidden = YES;
    
    self.img_delivery_siv.hidden = YES;
    self.img_hours_siv.hidden = YES;
    self.img_fee_siv.hidden = YES;

    self.lbl_delivery_siv.hidden=YES;
    self.lbl_hours_siv.hidden=YES;
    self.lbl_fee_siv.hidden=YES;
    self.lbl_delivery_title_siv.hidden=YES;
    self.lbl_hours_title_siv.hidden=YES;
    self.lbl_fee_title_siv.hidden=YES;
    self.btn_delivery_siv.hidden=YES;
    self.about_bg_siv.hidden=YES;
    self.contact_bg_siv.hidden = YES;
    self.store_bg_siv.hidden = YES;
    self.btn_hours_siv.hidden=YES;
    self.btn_fee_siv.hidden=YES;

    self.socialFacebookButton.hidden = YES;
    self.socialGoogleButton.hidden = YES;
    self.socialPinterestButton.hidden = YES;
    self.socialTwitterButton.hidden = YES;
    self.socialInstagramButton.hidden = YES;
    self.socialYoutubeButton.hidden = YES;
    self.mapView.hidden = YES;

    
     self.lbl_mobile_siv.enabled = NO;
     self.lbl_email_siv.enabled = NO;
     self.lbl_website_siv.enabled = NO;
     self.lbl_mobile_title_siv.enabled = NO;
     self.lbl_email_title_siv.enabled = NO;
     self.lbl_website_title_siv.enabled = NO;
     self.btn_phone_siv.enabled = NO;
     self.btn_mail_siv.enabled = NO;
     self.btn_web_siv.enabled = NO;
     
     self.lbl_delivery_siv.enabled = NO;
     self.lbl_hours_siv.enabled = NO;
     self.lbl_fee_siv.enabled = NO;
     self.lbl_delivery_title_siv.enabled = NO;
     self.lbl_hours_title_siv.enabled = NO;
     self.lbl_fee_title_siv.enabled = NO;
     self.btn_delivery_siv.enabled = NO;
     self.btn_hours_siv.enabled = NO;
     self.btn_fee_siv.enabled = NO;
     
     self.socialFacebookButton.enabled = NO;
     self.socialGoogleButton.enabled = NO;
     self.socialPinterestButton.enabled = NO;
     self.socialTwitterButton.enabled = NO;
     self.socialInstagramButton.enabled = NO;
     self.socialYoutubeButton.enabled = NO;
     self.mapView.hidden = YES;

    
}

- (void)showsSellerInfoSubViews
{
    self.lbl_bio_siv.hidden = NO;

//    self.lbl_mobile_siv.hidden = self.profile_share_obj.phone.length==0;
//    self.lbl_email_siv.hidden = self.profile_share_obj.email.length==0;
//    self.lbl_website_siv.hidden = self.profile_share_obj.web_url.length==0;
/*    self.lbl_mobile_title_siv.hidden = self.profile_share_obj.phone.length==0;
    self.lbl_email_title_siv.hidden = self.profile_share_obj.email.length==0;
    self.lbl_website_title_siv.hidden = self.profile_share_obj.web_url.length==0;
    self.img_phone_siv.hidden = self.profile_share_obj.phone.length==0;
    self.img_mail_siv.hidden = self.profile_share_obj.email.length==0;
    self.img_web_siv.hidden = self.profile_share_obj.web_url.length==0;
    self.btn_phone_siv.hidden = self.profile_share_obj.phone.length==0;
    self.btn_mail_siv.hidden = self.profile_share_obj.email.length==0;
    self.btn_web_siv.hidden = self.profile_share_obj.web_url.length==0;

    self.img_delivery_siv.hidden = self.profile_share_obj.delivery_time.length==0;
    self.lbl_delivery_title_siv.hidden = self.profile_share_obj.delivery_time.length==0;
    self.lbl_delivery_siv.hidden = self.profile_share_obj.delivery_time.length==0;

    self.img_hours_siv.hidden = self.profile_share_obj.working_hours.length==0;
    self.lbl_hours_title_siv.hidden = self.profile_share_obj.working_hours.length==0;
    self.lbl_hours_siv.hidden = self.profile_share_obj.working_hours.length==0;

    self.img_fee_siv.hidden = self.profile_share_obj.shipping_fee.length==0;
    self.lbl_fee_title_siv.hidden = self.profile_share_obj.shipping_fee.length==0;
    self.lbl_fee_siv.hidden = self.profile_share_obj.shipping_fee.length==0;*/
    
    self.lbl_mobile_title_siv.hidden = NO;
    self.lbl_email_title_siv.hidden = NO;
    self.lbl_website_title_siv.hidden = NO;
    self.img_phone_siv.hidden = NO;
    self.img_mail_siv.hidden = NO;
    self.img_web_siv.hidden = NO;
    self.btn_phone_siv.hidden = NO;
    self.btn_mail_siv.hidden = NO;
    self.btn_web_siv.hidden = NO;
    self.about_bg_siv.hidden = NO;
    self.contact_bg_siv.hidden = NO;
    self.store_bg_siv.hidden = NO;
    self.img_delivery_siv.hidden=NO;
    self.img_hours_siv.hidden=NO;
    self.img_fee_siv.hidden=NO;
    self.lbl_delivery_title_siv.hidden=NO;
    self.lbl_hours_title_siv.hidden=NO;
    self.lbl_fee_title_siv.hidden=NO;
    self.lbl_delivery_siv.hidden=NO;
    self.lbl_hours_siv.hidden=NO;
    self.lbl_fee_siv.hidden=NO;
    
    self.lbl_mobile_title_siv.enabled = self.profile_share_obj.phone.length!=0;
    self.lbl_email_title_siv.enabled = self.profile_share_obj.email.length!=0;
    self.lbl_website_title_siv.enabled = self.profile_share_obj.web_url.length!=0;
    self.btn_phone_siv.enabled = self.profile_share_obj.phone.length!=0;
    self.btn_mail_siv.enabled = self.profile_share_obj.email.length!=0;
    self.btn_web_siv.enabled = self.profile_share_obj.web_url.length!=0;
    
    self.lbl_delivery_title_siv.enabled=self.profile_share_obj.delivery_time.length!=0;
    self.lbl_hours_title_siv.enabled=self.profile_share_obj.working_hours.length!=0;
    self.lbl_fee_title_siv.enabled=self.profile_share_obj.shipping_fee.length!=0;
    self.lbl_delivery_siv.enabled=self.profile_share_obj.delivery_time.length!=0;
    self.lbl_hours_siv.enabled=self.profile_share_obj.working_hours.length!=0;
    self.lbl_fee_siv.enabled=self.profile_share_obj.shipping_fee.length!=0;

    
/*    self.socialFacebookButton.hidden = self.profile_share_obj.facebook_url.length==0;
    self.socialGoogleButton.hidden = self.profile_share_obj.google_url.length==0;
    self.socialPinterestButton.hidden = self.profile_share_obj.pinterest_url.length==0;
    self.socialTwitterButton.hidden = self.profile_share_obj.twitter_url.length==0;
    self.socialInstagramButton.hidden = self.profile_share_obj.instagram_url.length==0;
    self.socialYoutubeButton.hidden = self.profile_share_obj.youtube_url.length==0;
 */
    
    self.socialFacebookButton.hidden = NO;
    self.socialGoogleButton.hidden = NO;
    self.socialPinterestButton.hidden = NO;
    self.socialTwitterButton.hidden = NO;
    self.socialInstagramButton.hidden = NO;
    self.socialYoutubeButton.hidden = NO;
    
    self.socialFacebookButton.enabled = self.profile_share_obj.facebook_url.length!=0;
    self.socialGoogleButton.enabled = self.profile_share_obj.google_url.length!=0;
    self.socialPinterestButton.enabled = self.profile_share_obj.pinterest_url.length!=0;
    self.socialTwitterButton.enabled = self.profile_share_obj.twitter_url.length!=0;
    self.socialInstagramButton.enabled = self.profile_share_obj.instagram_url.length!=0;
    self.socialYoutubeButton.enabled = self.profile_share_obj.youtube_url.length!=0;

    self.mapView.hidden = NO;
}

#pragma mark - UICollectionView

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ( collectionView == collectionViewObj )
    {
        UICollectionReusableView *reusableview = nil;
        
        if (kind == UICollectionElementKindSectionHeader) {
            
            image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
            headerView.frame =self.view_header.frame;
            [headerView addSubview:self.view_header];
            
            UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
            
            collectionViewLayout.headerReferenceSize = CGSizeMake(-10, self.view_header.frame.size.height );
            
            reusableview = headerView;
            NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
            return reusableview;
        }
        if (kind == UICollectionElementKindSectionFooter) {
            
            image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
            footerview.frame =self.view_footer.frame;
            [footerview addSubview:self.view_footer];
            reusableview = footerview;
            
            return reusableview;
        }
        
        return reusableview;
    }

    UICollectionReusableView *reusableview = nil;
        
    if (kind == UICollectionElementKindSectionHeader) {
        
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.view_header_tbl.frame;
        [headerView addSubview:self.view_header_tbl];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(-10, self.view_header_tbl.frame.size.height );
        
        reusableview = headerView;
        NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
        return reusableview;
    }
    if (kind == UICollectionElementKindSectionFooter) {
        
        image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
        footerview.frame =self.view_footer_tbl.frame;
        [footerview addSubview:self.view_footer_tbl];
        reusableview = footerview;
        
        return reusableview;
    }
    
    return reusableview;
}
 
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    if ( view == collectionViewObj )
        return self.array_promotions.count;
    
    return self.composed_feeds.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( collectionView == collectionViewObj )
        return CGSizeMake((collectionView.frame.size.width-5)/4, (collectionView.frame.size.width-5)/4);

    return CGSizeMake(collectionView.frame.size.width, 150);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( cv == collectionViewObj )
    {
        static NSString *CellIdentifier = @"image_collection_cell";
        image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.img_photo.layer.borderWidth =0.0f;
        cell.img_photo.layer.cornerRadius =0.5f;
        cell.img_photo.layer.masksToBounds =YES;
        
        if (!cell.popupPreviewRecognizer) {
            cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
        }
        
        Home_tableview_data_share *shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        [cell.img_photo sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        cell.data = shareObj;
        NSString *strPrice = @"";
        
        if ( shareObj.price.length != 0 )
            strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
        
        if ([strPrice isEqualToString:@"(null)(null)"])
            cell.lbl_price.text = @"";
        else
            cell.lbl_price.text = strPrice;
        
        if ( [cell.lbl_price.text isEqualToString:@""] )
            cell.lbl_price.hidden = YES;
        else
            cell.lbl_price.hidden = NO;

        
        cell.tag = indexPath.row;
        
        for (id gesture in cell.gestureRecognizers)
        {
            if ([gesture isKindOfClass:[MyLongPressGestureRecognizer class]])
            {
                [cell removeGestureRecognizer:gesture];
            }
        }
        
        MyLongPressGestureRecognizer *lpgr = [[MyLongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editPromotion:)];
        lpgr.indexPath = indexPath;
        lpgr.delaysTouchesBegan = YES;
        [cell addGestureRecognizer:lpgr];
        
        return cell;
    }

    static NSString *CellIdentifier = @"MJCollectionViewCell";
    MJCollectionViewCell *cell = (MJCollectionViewCell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    
    //set offset accordingly
    CGFloat yOffset = ((self.tbl_news.contentOffset.y - cell.frame.origin.y) / IMAGE_HEIGHT) * IMAGE_OFFSET_SPEED;
    cell.imageOffset = CGPointMake(0.0f, yOffset);
    
    NSArray *cellData = self.composed_feeds[indexPath.row];

    if ( cellData.count > 0 )
    {
        [cell.MJImageView sd_setImageWithURL:[NSURL URLWithString:((Home_tableview_data_share *)cellData[0]).image_path] placeholderImage:[UIImage imageNamed:@"background"]];
    }
    else
    {
        cell.MJImageView.image = [UIImage imageNamed:@"background"];
    }

    CategoryObject *obj = ((CategoryObject *)self.array_categories[indexPath.row]);
    
    cell.lblCategoryName.text = obj.categoryName;
    cell.lblFollowers.text = obj.categoryFollowers;
    cell.lblPosts.text = [NSString stringWithFormat:@"%ld", (long)cellData.count];
    cell.btnFollow.hidden = YES;

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ( collectionView == collectionViewObj )
    {
        self.image_detail_viewObj.shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        self.image_detail_viewObj.arrayFeedArray = self.array_promotions;
        self.image_detail_viewObj.currentSelectedIndex = indexPath.row;
        self.image_detail_viewObj.promotion = YES;
        
        [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
        
        return;
    }
    
    categoryInfoView.user_id = self.profile_share_obj.user_id;
    categoryInfoView.category = ((CategoryObject *)self.array_categories[indexPath.row]);
    [self.navigationController pushViewController:categoryInfoView animated:YES];
}

- (void)editPromotion:(MyLongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    NSIndexPath *indexPath = gestureRecognizer.indexPath;
    
    if (indexPath.row != -1 && indexPath.row < self.array_promotions.count)
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Promotion Options"];
        
        NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
        NSString *uid2=[NSString stringWithFormat:@"%ld",(long)[self.profile_share_obj.user_id integerValue]];
        
        Home_tableview_data_share *shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        if ([uid1 isEqualToString:uid2])
        {
            [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@", salt, sig, shareObj.image_id];
                NSString *requestStr = [NSString stringWithFormat:@"%@promotion_post_delete.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
                NSLog(@"%@", requestStr);
                
                [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
                    NSLog(@"%@", responseObject);
                    if ([responseObject[@"success"] isEqualToString:@"1"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.array_promotions removeObjectAtIndex:indexPath.row];
                            [self.collectionViewObj deleteItemsAtIndexPaths:@[indexPath]];
                        });
                    }
                    [SVProgressHUD dismiss];
                } failure:^(NSString *errorString) {
                    [SVProgressHUD dismiss];
                    NSLog(@"%@", errorString);
                }];
            }];
            
            [sheet addButtonWithTitle:@"Edit" block:^{
                addPromotionView = [[AddPromotionViewController alloc] initWithNibName:@"AddPromotionViewController" bundle:nil];
                addPromotionView.user_id = self.profile_share_obj.user_id;
                addPromotionView.homeData = shareObj;
                addPromotionView.editing = YES;
                [self.navigationController pushViewController:addPromotionView animated:YES];
            }];
        }
        else
        {
            [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
                
            }];
        }
        
        [sheet addButtonWithTitle:@"Share" block:^{
            NSString *textToShare = shareObj.description;
            NSString *myWebsite = shareObj.image_path;
            
            NSArray *objectsToShare = @[textToShare, myWebsite];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo,
                                           UIActivityTypePostToFacebook,
                                           UIActivityTypePostToTwitter,
                                           UIActivityTypePostToWeibo,
                                           UIActivityTypeMessage,
                                           UIActivityTypeMail];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        
        [sheet showInView:self.view];
    }
}

#pragma mark UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.composed_feeds.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 40.0)];
    
    MyControl *control = [[MyControl alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 40.0)];
    control.backgroundColor = [UIColor whiteColor];
    control.section = section;
    [control addTarget:self action:@selector(viewHeaderTouched:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, tableView.frame.size.width, 40.0)];
    label.text = ((CategoryObject *)self.array_categories[section]).categoryName;
    label.textColor = [UIColor flatSkyBlueColorDark];
    label.font = [UIFont boldSystemFontOfSize:15.0];
    UILabel *detaillabel = [[UILabel alloc] initWithFrame:CGRectMake(-10.0, 0.0, tableView.frame.size.width, 40.0)];
    detaillabel.text = @"Details";
    [detaillabel setTextAlignment:NSTextAlignmentRight];
    detaillabel.textColor = [UIColor flatRedColor];
    detaillabel.font = [UIFont boldSystemFontOfSize:15.0];
    
    [view addSubview:control];
    [view addSubview:label];
    [view addSubview:detaillabel];
    view.userInteractionEnabled = YES;
    
    return view;
}

- (void)viewHeaderTouched:(MyControl *)control
{
    categoryInfoView.user_id = self.profile_share_obj.user_id;
    categoryInfoView.category = ((CategoryObject *)self.array_categories[control.section]);
    [self.navigationController pushViewController:categoryInfoView animated:YES];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *header = ((CategoryObject *)self.array_categories[section]).categoryName;
//    return header;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
    NSArray *cellData = self.composed_feeds[indexPath.section];
    [cell setCollectionData:cellData section:indexPath.section];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)didSelectItemFromCollectionView:(NSNotification *)notification
{
    NSIndexPath *indexPath = notification.userInfo[@"IndexPath"];
    
    self.image_detail_viewObj.shareObj = self.composed_feeds[indexPath.section][indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray = self.composed_feeds[indexPath.section];
    self.image_detail_viewObj.currentSelectedIndex = indexPath.row;
    self.image_detail_viewObj.promotion = NO;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

- (IBAction)addPosterClicked:(id)sender
{
    addPromotionView = [[AddPromotionViewController alloc] initWithNibName:@"AddPromotionViewController" bundle:nil];
    addPromotionView.user_id = self.profile_share_obj.user_id;
    addPromotionView.editing = NO;
    [self.navigationController pushViewController:addPromotionView animated:YES];
}

#pragma mark Method for Stop Header scrolling in UITableview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ( scrollView == tbl_news )
    {
        for(MJCollectionViewCell *view in self.tbl_news.visibleCells) {
            CGFloat yOffset = ((self.tbl_news.contentOffset.y - view.frame.origin.y) / IMAGE_HEIGHT) * IMAGE_OFFSET_SPEED;
            view.imageOffset = CGPointMake(0.0f, yOffset);
        }
    }
//    CGFloat sectionHeaderHeight = 100;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
}

- (IBAction)handleSingleTap:(UITapGestureRecognizer *)gesture
{
//    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
//    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
//    which_image_liked = (int)tappedRow.section;
//    
//    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
//    UIImageView *nprfullscreenimg = [[UIImageView alloc] init];
//    
//    [nprfullscreenimg sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:Nil];
//    
//    nprfullscreenimg.contentMode = UIViewContentModeScaleToFill;
//    
//    nprfullscreenimg.frame = CGRectMake(0, 20, 320, 418);
//    
//    UIButton *btnclose = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnclose setFrame:CGRectMake(290, 30, 26, 26)];
//    [btnclose setImage:[UIImage imageNamed:[NSString stringWithFormat:@"closebtn_pop.png"]] forState:UIControlStateNormal];//with image
//    [btnclose addTarget:self action:@selector(btnclosefullscreenClick) forControlEvents:UIControlEventTouchUpInside];
//    
//    imgfullscreenviewobj = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
//    [imgfullscreenviewobj setBackgroundColor:[UIColor blackColor]];
//    imgfullscreenviewobj.alpha =1.0;
//    
//    [self.view addSubview:imgfullscreenviewobj];
//    [imgfullscreenviewobj addSubview:nprfullscreenimg];
//    [imgfullscreenviewobj addSubview:btnclose];
}

-(IBAction)btnclosefullscreenClick{
    [imgfullscreenviewobj removeFromSuperview];
    [self get_news_feed];
}

#pragma mark Action sheet

-(IBAction)btn_photo_option_click:(id)sender{
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];

                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}
-(void)report_inappropriate:(int )index{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this post if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender{
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark Go to Profile

-(IBAction)btn_profile_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld", (long)tag);
}

#pragma mark UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            NSLog(@"DELETE");
            [self delete_photo];
        }
    }
    if (alertView.tag==2) {
        if (buttonIndex==0) {
            return;
        }
        [self report_inappropriate:(int)buttonIndex];
        
    }
    
}

#pragma mark Delete Photo
-(void)delete_photo{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    self.tableViewDataSource.posts = self.array_feeds;// copy];
    self.tableViewDelegate.posts = self.array_feeds;// copy];
    [self.tbl_news reloadData];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    NSLog(@"TAG:%ld",(long)control.tag);
    cellClickIndex=control.tag;
    NSLog(@"cellClickIndex:%ld",(long)cellClickIndex);
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"303":nil];
    
    obj.my_rating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
    // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
    // [dlstarObj  setRating:3];
}

-(void)getRatingResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld",(long)[[dataDict objectForKey:@"rate"] integerValue]];
        
        [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:cellClickIndex inSection:0]]];
//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    //[SVProgressHUD show];

    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];

    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"301":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];

    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
//        
//        if ([shareObj.liked isEqualToString:@"no"]) {
//            shareObj.liked=@"yes";
//            int count =[shareObj.likes intValue];
//            count++;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            
//        }else{
//            
//            shareObj.liked=@"no";
//            int count =[shareObj.likes intValue];
//            count--;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//        }
//        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
//        [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];

//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    
}

#pragma mark
#pragma mark Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture
{
//    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
//    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
//    
//    which_image_liked = (int)tappedRow.section;
//    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
//    
//    if ([shareObj.liked isEqualToString:@"no"]) {
//        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
//    }
//    
//    [self.img_like_heart setAlpha:0.0];
//    [self.img_like_heart setHidden:NO];
//    
//    [UIView animateWithDuration:0.7 animations:^{
//        [self.img_like_heart setAlpha:1.0];
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.7 animations:^{
//            [self.img_like_heart setAlpha:0.0];
//        } completion:^(BOOL finished) {
//            [self.img_like_heart setHidden:YES];
//        }];
//    }];
    
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"304":nil];
    
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            
            [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];

//            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }
    
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender {
     NSLog(@"@ click");
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender {
    NSLog(@"url click");
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender {
     NSLog(@"user name click");
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

#pragma mark - Show Mapview
-(IBAction)btn_mapview_click:(id)sender{

    self.mapviewObj.array_data =self.array_feeds;
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.mapviewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)show_chatting:(id)sender {
    
    NSLog(@"My chatting inbox");
    
//    self.chat_viewObj.toUserID = self.profile_share_obj.user_id;
//    self.chat_viewObj.toUserImage = self.profile_share_obj.image;
//
//    [self presentViewController:self.chat_viewObj animated:YES completion:nil];
    
    self.inbox_viewObj.userID = self.profile_share_obj.user_id;
    
    [self presentViewController:self.inbox_viewObj animated:YES completion:nil];

}

-(IBAction)requestFrndBtnClick :(id)sender {
    [self.navigationController pushViewController:requestFrndViewObj animated:YES];
}

-(IBAction)allFrndBtnClick :(id)sender {
    [self.navigationController pushViewController:allFriendViewObj animated:YES];
}

- (IBAction)gotoBigMap:(id)sender {
    self.map_viewObj.str_latitude=self.profile_share_obj.latitude;
    self.map_viewObj.str_longitude=self.profile_share_obj.longitude;
    self.map_viewObj.str_name = self.profile_share_obj.name;
    
    [self.navigationController pushViewController:self.map_viewObj animated:YES];
}

#pragma mark - Change Profile pic
-(IBAction)btn_profile_btn_click:(id)sender{

    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Change Profile Picture"];
    
    [sheet setDestructiveButtonWithTitle:@"Remove Current Photo" block:^{
    }];
    
    [sheet addButtonWithTitle:@"Import from Facebook" block:^{
        [self get_user_photo_facebok];
    }];
    
    [sheet addButtonWithTitle:@"Import from Twitter" block:^{
        
    }];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [sheet addButtonWithTitle:@"Take Photo" block:^{
            photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            photoPickerController.allowsEditing = YES;
            [self presentViewController:photoPickerController animated:YES completion:nil];
        }];
    }
    
    [sheet addButtonWithTitle:@"Choose from Library" block:^{
//        photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        [self presentViewController:photoPickerController animated:YES completion:nil];
        
        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
        
        photoPicker.cropBlock = ^(UIImage *image) {
            //do something
            image_upload_flag=1;
            
            if (image == nil) return;
            self.img_photo.image=image;
            self.img_photo_tbl.image=image;
            [self upload_profile_pict];
        };
        
        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
        [navCon setNavigationBarHidden:YES];
        
        [self presentViewController:navCon animated:YES completion:NULL];

    }];
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    image_upload_flag=1;
    
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
	if (baseImage == nil) return;
    self.img_photo.image=baseImage;
    self.img_photo_tbl.image=baseImage;
    [self upload_profile_pict];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}

-(void)apiFQLI_pic_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(set_btn_add_photo_image) withObject:self afterDelay:0.1];
        
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
}

- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection result:(id)result error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    } else {
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
        
        self.image_url=imgU;
    }
}

-(void)set_btn_add_photo_image {
    self.img_photo.image =[UIImage imageNamed:@"default_user_image"];
    self.img_photo_tbl.image =[UIImage imageNamed:@"default_user_image"];
    
    [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.image_url] placeholderImage:nil];

    [self.img_photo_tbl sd_setImageWithURL:[NSURL URLWithString:self.image_url] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
}

-(void)upload_profile_pict {
    
//    NSData *img_data  = UIImageJPEGRepresentation(self.img_photo.image,1);
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    CGSize size = CGSizeMake(PostImageWidth, PostImageWidth * self.img_photo.image.size.height / self.img_photo.image.size.width);
    
    UIImage *newImage = [GlobalDefine imageWithImage:self.img_photo.image scaledToSize:size];

    NSData *img_data = UIImageJPEGRepresentation(newImage, 1.0);
    
    dispatch_async(queue, ^{
        NSString *dateString1 = [GlobalDefine toDateTimeStringFromDateWithFormat:[NSDate date] formatString:@"yyyy-MM-dd hh:mm:ss"];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else
            {
                
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14222" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postProfileChangeResponce:) name:@"14222" object:nil];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14222" object:nil];
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostProfileChangeResponce:) name:@"-14222" object:nil];
                
                NSString *requestStr = [NSString stringWithFormat:@"%@post_profilechange.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        imageKeys,@"image",
                                        nil];
                NSLog(@"params:%@",params);
                AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
                [networkQueue queueItems:requestStr :@"14222" :params];
            }
        });
    });
}

-(void)postProfileChangeResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14222" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14222" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
    }
}

-(void)FailpostProfileChangeResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14222" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14222" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Google Map Delegate
- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidStartTileRendering");
}

- (void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidFinishTileRendering");
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    NSLog(@"didTapMarker");
//    startlocation = googlemapview.myLocation.coordinate;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    startlocation = delegate.locationManager.location.coordinate;

    GMSMarker   *marker1 = [GMSMarker markerWithPosition:startlocation];
    marker1.title = @"Current Location";
    marker1.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker1.map = googlemapview;
    marker1.userData = marker1;
    marker1.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    bounds = [bounds includingCoordinate:destinationlocation];
    bounds = [bounds includingCoordinate:startlocation];
    
    [googlemapview animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    
    //    GMSMutablePath *path = [GMSMutablePath path];
    //    [path addCoordinate:googlemapview.myLocation.coordinate];
    //    [path addCoordinate:destinationlocation];
    //
    //    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    //    rectangle.strokeWidth = 2.f;
    //    rectangle.map = googlemapview;
    
    NSMutableArray *waypointStrings = [NSMutableArray array];
    NSString *startPositionString = [NSString stringWithFormat:@"%f,%f",startlocation.latitude,startlocation.longitude];
    [waypointStrings addObject:startPositionString];
    NSString *endPositionString = [NSString stringWithFormat:@"%f,%f",destinationlocation.latitude,destinationlocation.longitude];
    [waypointStrings addObject:endPositionString];
    
    MDDirectionService *mds = [[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    NSDictionary *query = @{ @"sensor" : @"false",
                             @"waypoints" : waypointStrings };
    
    [mds setDirectionsQuery:query
               withSelector:selector
               withDelegate:self];
    
    
    return NO;
}

-(void)addDirections:(NSDictionary *)json{
    if ( [json[@"routes"] count] > 0 )
    {
        NSDictionary *routes = json[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 2.f;
        polyline.map = googlemapview;
    }
}


- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSLog(@"didTapInfoWindowOfMarker");
    [self openDirectionsInGoogleMaps];
}

- (void)openDirectionsInGoogleMaps
{
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    
    //    GoogleDirectionsWaypoint *start = [[GoogleDirectionsWaypoint alloc] init];
    //    start.queryString = @"";
    //    start.location = startlocation;
    //    directionsDefinition.startingPoint = start;
    
    directionsDefinition.startingPoint = nil;
    
    GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
    destination.queryString = @"";
    destination.location = destinationlocation;
    directionsDefinition.destinationPoint = destination;
    
    directionsDefinition.travelMode = kTravelModeDriving;
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}

@end
