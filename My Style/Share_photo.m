//
//  Share_photo.m
//  My Style
//
//  Created by Tis Macmini on 5/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Share_photo.h"

@interface Share_photo ()

@end

@implementation Share_photo

@synthesize img_bg_firstcell;
@synthesize img_show_photo,view_bg_show_photo,txt_desc;

@synthesize view_third_cell,img_bg_third_cell;
@synthesize str_img_url;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.img_bg_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell.layer.cornerRadius =35.0f;
    
    self.img_bg_third_cell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_third_cell.layer.cornerRadius =35.0f;
    
    self.view_bg_show_photo.backgroundColor =[UIColor colorWithRed:255.0f/255.0f green:72.0f/255.0f blue:74.0f/255.0f alpha:1.0f];
    view_bg_show_photo.layer.cornerRadius =40.0f;
    view_bg_show_photo.layer.masksToBounds =YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [self.img_show_photo sd_setImageWithURL:[NSURL URLWithString:self.str_img_url] placeholderImage:nil];
 //   self.img_show_photo.imageURL=[NSURL URLWithString:self.str_img_url];
    
    img_show_photo.layer.cornerRadius =35.0f;
    img_show_photo.layer.masksToBounds =YES;
}


-(IBAction)btn_done_click:(id)sender{

    [self.view endEditing:YES];
}

#pragma mark UIScrollview Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [self.txt_desc resignFirstResponder];
}

#pragma mark UITextview Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    return TRUE;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
        self.txt_desc.text=@"";
    }

    
    return TRUE;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.txt_desc.text.length==0) {
        self.txt_desc.text=@"Write a caption...";
    }

}

#pragma mark - Messaage Sharing
-(IBAction)btn_message_share_click:(id)sender{

    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = self.img_show_photo.image;
    
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    
    if([MFMessageComposeViewController canSendText]) {
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Your Message Body"];
        picker.messageComposeDelegate = self;
      //  picker.recipients = [NSArray arrayWithObject:@"123456789"];
        [picker setBody:emailBody];// your recipient number or self for testing
        picker.body = emailBody;
        NSLog(@"Picker -- %@",picker.body);
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{

    /*    MessageComposeResultCancelled,
     MessageComposeResultSent,
     MessageComposeResultFailed*/
    switch (result) {
        case MessageComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MessageComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent failed!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MessageComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
    }
}
#pragma mark - Email Sharing

-(IBAction)btn_email_share_click:(id)sender{

    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        NSData *photoData=UIImagePNGRepresentation(self.img_show_photo.image);
        [controller addAttachmentData:photoData mimeType:@"image/png" fileName:[NSString stringWithFormat:@"photo.png"]];
        controller.mailComposeDelegate = self;
        if (controller)
            [self presentViewController:controller animated:YES completion:nil];
    }else {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{

    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Facebook Sharing
-(IBAction)btn_facebook_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark - Twitter Sharing
-(IBAction)btn_twitter_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
