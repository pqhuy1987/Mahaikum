//
//  HashtagSearchRecord.h
//  Mahalkum
//
//  Created by OCS Developer 1 on 02/06/15.
//  Copyright (c) 2015 Kuwait E-Gate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HashtagSearchRecord : NSObject
@property(nonatomic,strong)NSString*objImageCaption;
@property(nonatomic,strong)NSString*objComment;
@property(nonatomic,strong)NSString*objImagePath;
@property(nonatomic)NSUInteger objIndex;
@end
