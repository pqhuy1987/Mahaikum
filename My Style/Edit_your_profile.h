//
//  Edit_your_profile.h
//  My Style
//
//  Created by Tis Macmini on 5/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "BlockActionSheet.h"
#import "Singleton.h"
#import "EGOImageButton.h"
#import "ActionSheetStringPicker.h"
#import "Edit_your_profile_share.h"
#import "Change_password.h"

#import "DCRoundSwitch.h"

@interface Edit_your_profile : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,AmazonServiceRequestDelegate>{

    UIScrollView *scrollview;
    
    UIImageView *img_first_cell;
    UIImageView *img_second_cell;
    UIImageView *img_third_cell;
    UIImageView *img_fourth_cell;
    EGOImageButton *img_photo;
    
    UITextField *txt_name;
    UITextField *txt_username;
    UITextField *txt_weburl;
    //UITextField *txt_bio;
    UITextView *txt_bio;
    UITextField *txt_email;
    UITextField *txt_phone;
    UIButton *btn_gender;
    UIButton *btn_switch_private;
    UILabel *lblgender, *placeholderLabel;
    UIImageView *imgswitchgendermale,*imgswitchgenderfemale;
    UILabel *lblheader,*lblsubheaders;
    UIButton *btnchangepassword;
    UIImagePickerController *photoPickerController;
    NSString *image_url;
    
    Edit_your_profile_share *profile_shareObj;
    
    int is_reload_data;
    DCRoundSwitch *switch_private;
    int is_show_alert;
    UIActivityIndicatorView *activity;
    
    UILabel *lblSpinnerBg;
    UITextField *txt_CountryCode;
}
@property(nonatomic,strong)IBOutlet UITextField *txt_CountryCode;

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,assign)int is_reload_data;
@property(nonatomic,assign)int is_show_alert;

@property(nonatomic,strong)UIImagePickerController *photoPickerController;
@property(nonatomic,strong) NSString *image_url;
@property(nonatomic,strong)IBOutlet UIButton *btn_switch_private;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong)IBOutlet UIImageView *img_first_cell;
@property(nonatomic,strong)IBOutlet UIImageView *img_second_cell;
@property(nonatomic,strong)IBOutlet UIImageView *img_third_cell;
@property(nonatomic,strong)IBOutlet UIImageView *img_fourth_cell;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchgendermale,*imgswitchgenderfemale;
@property(nonatomic,strong)IBOutlet EGOImageButton *img_photo;
@property(nonatomic,strong)IBOutlet UIButton *btnchangepassword;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblsubheaders;
@property(nonatomic,strong)IBOutlet UILabel *lblgender;
@property(nonatomic,strong)IBOutlet UITextField *txt_name;
@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)IBOutlet UITextField *txt_weburl;
@property (strong, nonatomic) IBOutlet UITextView *txt_bio;
//@property(nonatomic,strong)IBOutlet UITextField *txt_bio;
@property (weak, nonatomic) IBOutlet UITextField *txt_facebook_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_twitter_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_pinterest_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_google_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_instagram_url;
@property (weak, nonatomic) IBOutlet UITextField *txt_youtube_url;
@property(nonatomic,strong)IBOutlet UITextField *txt_email;
@property(nonatomic,strong)IBOutlet UITextField *txt_phone;
@property (strong, nonatomic) IBOutlet UITextField *txt_delivery_time;
@property (strong, nonatomic) IBOutlet UITextField *txt_working_hours;
@property (strong, nonatomic) IBOutlet UITextField *txt_shipping_fee;
@property(nonatomic,strong)IBOutlet UIButton *btn_gender;
@property(nonatomic,strong)Edit_your_profile_share *profile_shareObj;

@property (nonatomic, strong) IBOutlet DCRoundSwitch *switch_gender;
@property (nonatomic, strong) IBOutlet DCRoundSwitch *switch_private;

//@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *mapView;

@property (nonatomic, strong) AmazonS3Client *s3;
@property (nonatomic, strong) UIActivityIndicatorView *activity;

-(IBAction)btnswitchgenderClick:(id)sender;

@end
