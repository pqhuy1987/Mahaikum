//
//  Notification_setting_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Notification_setting_ViewController.h"

NSInteger btnswitchflg = 0;
@interface Notification_setting_ViewController ()

@end

@implementation Notification_setting_ViewController
@synthesize tbl_notification,array_notification,dict_notification;
@synthesize tbl_footer,lblheader;
@synthesize btnSave;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.array_notification =[[NSMutableArray alloc]init];
    self.dict_notification =[[NSMutableDictionary alloc]init];
    NSMutableArray *array =[[NSMutableArray alloc]init];
    
    notification_type first;
    first.is_checked=0;
    first.title=@"Off";
    first.notification_type=@"Like Notification";
    
    notification_type second;
    second.is_checked=0;
    second.title=@"From People I Follow";
    second.notification_type=@"Comment Notifications";
    
    notification_type third;
    third.is_checked=1;
    third.title=@"From Everyone";
    third.notification_type=@"Contact Notifications";
    
    [array addObject: [NSData dataWithBytes: &first length: sizeof(first)]];
    [array addObject: [NSData dataWithBytes: &second length: sizeof(second)]];
    [array addObject: [NSData dataWithBytes: &third length: sizeof(third)]];
    self.tbl_notification.tableFooterView=self.tbl_footer;
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    if (is_iPhone_5) {
        self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
        self.tbl_notification.frame = CGRectMake(0,71, 320, 445);
    }else{
        self.tbl_notification.frame = CGRectMake(0,63, kViewWidth, 460);
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self get_notification_info];
}
-(void)get_notification_info {
    //ttp://techintegrity.in/mystyle/get_notification.php?uid=6
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNotificationAPIResponce:) name:@"14220" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetNotificationAPIResponce:) name:@"-14220" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_notification.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14220" :params];
}

-(void)getNotificationAPIResponce:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_notification removeAllObjects];
        
        NSDictionary *dict =[result valueForKey:@"data"];
        NSMutableArray *array =[[NSMutableArray alloc]initWithCapacity:3];
        for (int i =0; i<3; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            
            if (i==[[dict valueForKey:@"like_notification"] integerValue]) {
                obj.is_checked=@"1";
            } else {
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            } else if(i==1) {
                obj.title=@"From People I Follow";
            } else {
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"Like Notifications";
            [array addObject:obj];
        }
        [self.array_notification addObject:array];
        
        NSMutableArray *array1 =[[NSMutableArray alloc]initWithCapacity:3];
        for (int i =0; i<3; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"comment_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else {
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else if(i==1){
                obj.title=@"From People I Follow";
            }else{
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"Comment Notifications";
            [array1 addObject:obj];
        }
        [self.array_notification addObject:array1];
        
        NSMutableArray *array2=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =0; i<2; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"new_followers_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else {
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"New Followers";
            [array2 addObject:obj];
        }
        [self.array_notification addObject:array2];
        
        NSMutableArray *array3=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =0; i<2; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"accept_follow_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else {
                obj.title=@"From Everyone";
            }
            obj.notification_type=@"Accepted Follow Requests";
            [array3 addObject:obj];
        }
        [self.array_notification addObject:array3];
        
        NSMutableArray *array4=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =0; i<2; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"contact_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else {
                obj.title=@"All New Contacts";
            }
            obj.notification_type=@"Contact Notifications";
            [array4 addObject:obj];
        }
        [self.array_notification addObject:array4];

        NSMutableArray *array5=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =0; i<2; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"image_upload_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else {
                obj.title=@"From People I Follow";
            }
            obj.notification_type=@"Image Upload Notification";
            [array5 addObject:obj];
        }
        [self.array_notification addObject:array5];
        
        NSMutableArray *array6=[[NSMutableArray alloc]initWithCapacity:2];
        for (int i =0; i<2; i++) {
            Notification_setting_share *obj =[[Notification_setting_share alloc]init];
            if (i==[[dict valueForKey:@"rating_notification"] integerValue]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            
            if (i==0) {
                obj.title=@"Off";
            }else {
                obj.title=@"From People I Follow";
            }
            obj.notification_type=@"Rating Notification";
            [array6 addObject:obj];
        }
        [self.array_notification addObject:array6];
        
        [tbl_notification reloadData];
    }
}

-(void)FailgetNotificationAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14220" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14220" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.array_notification.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section>=2) {
        return 2;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSMutableArray *array =[self.array_notification objectAtIndex:section];
    Notification_setting_share *obj =[array objectAtIndex:0];
    
    UIImageView *img1=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,kViewWidth, 2)];
    img1.backgroundColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    
    UILabel *lbl_title =[[UILabel alloc]initWithFrame:CGRectMake(35,2,250, 46)];
    lbl_title.text=obj.notification_type;
    lbl_title.backgroundColor =[UIColor clearColor];
    lbl_title.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    
    lbl_title.textColor =[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    
    UIImageView *img=[[UIImageView alloc] initWithFrame:CGRectMake(0,48,kViewWidth, 2)];
    img.backgroundColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,kViewWidth, 30)];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =1.0;
    [view addSubview:img1];
    [view addSubview:lbl_title];
    [view addSubview:img];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   // if (indexPath.row!=0) {
        
    static NSString *CellIdentifier = @"Notification_setting_cell";
    Notification_setting_cell *cell = (Notification_setting_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)	{
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Notification_setting_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
	}

    NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
    Notification_setting_share *obj =[array objectAtIndex:indexPath.row];

    cell.lbl_title.text =obj.title;
    
    if ([obj.is_checked isEqualToString:@"1"]) {
        [cell.img_checkmark setHidden:NO];
    }
    else {
        [cell.img_checkmark setHidden:YES];
    }
    return cell;
   
    // }
    /*
    else {
        static NSString *CellIdentifier = @"Notification_setting_on_off_cell";
        Notification_setting_on_off_cell *cell = (Notification_setting_on_off_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil)	{
            NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Notification_setting_on_off_cell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.showsReorderControl = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell.btnswitch addTarget:self action:@selector(btnswtichClick) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.backgroundColor = [UIColor clearColor];
            [cell.switch_on_off addTarget:self action:@selector(btn_switch_on_off_click:) forControlEvents:UIControlEventValueChanged];
            cell.switch_on_off.onText = NSLocalizedString(@"ON", @"");
            cell.switch_on_off.offText = NSLocalizedString(@"OFF", @"");
            cell.switch_on_off.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
        }
        cell.switch_on_off.tag =indexPath.section;
        NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
        Notification_setting_share *obj =[array objectAtIndex:indexPath.row];
        
        if ([obj.is_checked isEqualToString:@"1"]) {

            [cell.switch_on_off setOn:NO animated:NO];
        }else{

            [cell.switch_on_off setOn:YES animated:NO];
        }
        return cell;
    }
    */
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ///if (indexPath.row !=0) {
        NSMutableArray *array =[self.array_notification objectAtIndex:indexPath.section];
        Notification_setting_share *temObj =[array objectAtIndex:indexPath.row];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
    
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:indexPath.section withObject:temparray];
        [self.tbl_notification reloadData];
       // [self update_notification_seting:indexPath];
    //}
}

-(IBAction)btnSaveClick:(id)sender {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNotificationAPIResponce:) name:@"14221" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsetNotificationAPIResponce:) name:@"-14221" object:nil];
    
    NSString *like=@"";
    NSString *comment=@"";
    NSString *newFollowers=@"";
    NSString *acceptFollowRequest=@"";
    NSString *contact=@"";
    NSString *imageUpload=@"";
    NSString *rating=@"";
    
    for (int i=0;i<self.array_notification.count; i++) {
        NSMutableArray *array =[self.array_notification objectAtIndex:i];
        
        for (int j=0;j<[array count]; j++) {
            Notification_setting_share *obj =[array objectAtIndex:j];
            if ([obj.is_checked isEqualToString:@"1"]) {
                if ([obj.notification_type isEqualToString:@"Like Notifications"]) {
                    like=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"Comment Notifications"]) {
                    comment=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"New Followers"]) {
                    newFollowers=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"Accepted Follow Requests"]) {
                    acceptFollowRequest=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"Contact Notifications"]) {
                    contact=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"Image Upload Notification"]) {
                    imageUpload=[NSString stringWithFormat:@"%d",j];
                }
                else if ([obj.notification_type isEqualToString:@"Rating Notification"]) {
                    rating=[NSString stringWithFormat:@"%d",j];
                }
                break;
            }
        }
    }
    
    NSString *requestStr = [NSString stringWithFormat:@"%@set_notification.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            like,@"like_notification",
                            comment,@"comment_notification",
                            newFollowers,@"new_followers_notification",acceptFollowRequest,@"accept_follow_notification",
                            contact,@"contact_notification",
                            imageUpload,@"image_upload_notification",
                            rating,@"rating_notification",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14221" :params];
}

#pragma mark - Update notification setting
-(void)update_notification_seting:(NSIndexPath *)indexpath {
    NSString *type;
    
    switch (indexpath.section) {
        case 0:{
        type=@"like";
            break;
        }
        case 1:{
            type=@"comment";
            break;
        }            
        default:{
            type=@"contact";
            break;
        }
    }
    //ttp://techintegrity.in/mystyle/set_notification.php?uid=4&value=3&ntype=like
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setNotificationAPIResponce:) name:@"14221" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsetNotificationAPIResponce:) name:@"-14221" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@set_notification.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                           [NSString stringWithFormat:@"%ld",indexpath.row+1], @"value",
                            type,@"ntype",
                            nil];
  //  NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14221" :params];
}

-(void)setNotificationAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"success");
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Success" message:@"Setting saved successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    }
   // [self.tbl_notification reloadData];
}

-(void)FailsetNotificationAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14221" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14221" object:nil];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)btnswtichClick:(id)sender{
    UIButton *btn_switch =(UIButton *)sender;
    NSInteger tag =btn_switch.tag;
    
    if (btnswitchflg == 0) {
        btnswitchflg =1;
        not_on_off_Obj.imgswitchoff.hidden = YES;
        not_on_off_Obj.imgswitchon.hidden = NO;
        NSMutableArray *array =[self.array_notification objectAtIndex:tag];
        Notification_setting_share *temObj =[array objectAtIndex:0];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"0";
            }else{
                if ([obj isEqual:[array lastObject]]) {
                    obj.is_checked=@"1";
                }else{
                    obj.is_checked=@"0";
                }
                
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
        [self.tbl_notification reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(array.count-1) inSection:tag];
        [self update_notification_seting:indexPath];
    }
    else{
        not_on_off_Obj.imgswitchoff.hidden = NO;
        not_on_off_Obj.imgswitchon.hidden = YES;
        btnswitchflg = 0;
        NSMutableArray *array =[self.array_notification objectAtIndex:tag];
        Notification_setting_share *temObj =[array objectAtIndex:0];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"1";
            }else{
                obj.is_checked=@"0";
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
        [self.tbl_notification reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:tag];
        [self update_notification_seting:indexPath];
    }
}

-(IBAction)btn_switch_on_off_click:(id)sender{
    UISwitch *btn_switch =(UISwitch *)sender;
    NSInteger tag =btn_switch.tag;
    if (!btn_switch.on) {
    NSMutableArray *array =[self.array_notification objectAtIndex:tag];
    Notification_setting_share *temObj =[array objectAtIndex:0];
    NSMutableArray *temparray =[[NSMutableArray alloc]init];
    
    for (Notification_setting_share *obj in array) {
        if ([obj.title isEqualToString:temObj.title]) {
            obj.is_checked=@"1";
        }else{
            obj.is_checked=@"0";
        }
        [temparray addObject:obj];
    }
    [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
    [self.tbl_notification reloadData];

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:tag];
        [self update_notification_seting:indexPath];
    }else{
    
        NSMutableArray *array =[self.array_notification objectAtIndex:tag];
        Notification_setting_share *temObj =[array objectAtIndex:0];
        NSMutableArray *temparray =[[NSMutableArray alloc]init];
        
        for (Notification_setting_share *obj in array) {
            if ([obj.title isEqualToString:temObj.title]) {
                obj.is_checked=@"0";
            }else{
                if ([obj isEqual:[array lastObject]]) {
                    obj.is_checked=@"1";
                }else{
                    obj.is_checked=@"0";
                }
                
            }
            [temparray addObject:obj];
        }
        [self.array_notification replaceObjectAtIndex:tag withObject:temparray];
        [self.tbl_notification reloadData];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(array.count-1) inSection:tag];
        [self update_notification_seting:indexPath];
    }
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
