//
//  Change_password.m
//  My Style
//
//  Created by Tis Macmini on 5/24/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Change_password.h"

@interface Change_password ()

@end

@implementation Change_password
@synthesize txt_old_password,txt_new_password,txt_new_password_again,img_bd_firstcell,img_bd_secondcell,lblsubheader,lblheader,activity,lblSpinnerBg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:16.0];
    txt_new_password.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    txt_new_password_again.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    txt_old_password.font = [UIFont fontWithName:@"Robot-Light" size:16.0];
    lblsubheader.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    
    self.img_bd_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bd_firstcell.layer.cornerRadius =5.0f;

    self.img_bd_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bd_secondcell.layer.cornerRadius =5.0f;
  
//    UIToolbar *toolbar =[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    toolbar.barStyle= UIBarStyleBlackTranslucent;
//    toolbar.items =@[[[UIBarButtonItem alloc]initWithTitle:@"Cancel"style:UIBarButtonItemStyleBordered target:self action:@selector(btn_cancel_click:)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btn_done_click:)]];
//    [toolbar sizeToFit];
//    
//    self.txt_new_password.inputAccessoryView=toolbar;
//    self.txt_new_password_again.inputAccessoryView=toolbar;
//    self.txt_old_password.inputAccessoryView=toolbar;
    
    // Do any additional setup after loading the view from its nib.
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    
    if (isiPhone) {
        txt_new_password.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_new_password_again.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_old_password.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
    }else{
        txt_new_password.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_new_password_again.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_old_password.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
    }
}

-(IBAction)btn_cancel_click:(id)sender{
    [self.view endEditing:YES];
}

-(IBAction)btn_done_click:(id)sender{
    [self.view endEditing:YES];
    
    if(![self.txt_new_password_again.text isEqualToString:self.txt_new_password.text]){
        
        
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:@"Passwords do not match." hideAfter:2];
        return;
    }
    
    if (self.txt_new_password.text.length<6) {
        
        
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:@"Password must be at least 6 characters." hideAfter:2];
        return;
    }
    
    [self change_password];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField==self.txt_old_password) {
        
        [self.txt_new_password becomeFirstResponder];
        
    }else if(textField==self.txt_new_password){
        
        [self.txt_new_password_again becomeFirstResponder];
        
    }
    return YES;
}

-(void)change_password{
    [SVProgressHUD show];
        //techintegrity.in/mystyle/post_change_password.php?uid=3&oldpassword=piyush&newpassword=abc123
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postPasswordChangeAPIResponce:) name:@"14203" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostPasswordChangeAPIResponce:) name:@"-14203" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_change_password.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);

    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"uid",                            self.txt_old_password.text,@"oldpassword",self.txt_new_password.text,@"newpassword",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14203" :params];
}

-(void)postPasswordChangeAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    
    NSString *strResult=[result valueForKey:@"Status"];
    if ([strResult isEqualToString:@"false"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:@"Please check your old password" hideAfter:2];
    }
    else {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Password change successfully" hideAfter:2];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)FailpostPasswordChangeAPIResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14203" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14203" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
