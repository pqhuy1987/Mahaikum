//
//  Faceabook_find_friend_cell.h
//  My Style
//
//  Created by Tis Macmini on 4/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface Faceabook_find_friend_cell : UITableViewCell{

    UILabel *lbl_name;
    UILabel *lbl_username;
    UIImageView *img_user;
    UIButton *btn_follow;
}
@property(nonatomic,strong)IBOutlet UILabel *lbl_name;
@property(nonatomic,strong)IBOutlet UILabel *lbl_username;
@property(nonatomic,strong)IBOutlet UIImageView *img_user;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow;
@end
