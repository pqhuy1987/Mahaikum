//
//  Sign_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Sign_ViewController.h"
#import "AppDelegate.h"
#import "HyTransitions.h"

@interface Sign_ViewController ()<UIViewControllerTransitioningDelegate>
{
    CustomAlertView *ddAlert;
}
@end

@implementation Sign_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] ) {
        self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y - 100, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        self.lblLogin.frame = CGRectMake(self.lblLogin.frame.origin.x, self.lblLogin.frame.origin.y - 50, self.lblLogin.frame.size.width, self.lblLogin.frame.size.height);
        self.activityViewObj.frame = CGRectMake(self.activityViewObj.frame.origin.x, self.activityViewObj.frame.origin.y - 50, self.activityViewObj.frame.size.width, self.activityViewObj.frame.size.height);
        self.logoBG.frame = CGRectMake(self.logoBG.frame.origin.x, self.logoBG.frame.origin.y - 50, self.logoBG.frame.size.width, self.logoBG.frame.size.height);
        
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"email",@"public_profile",@"user_friends", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,FBSessionState status, NSError *error) {
            }];
        }
    }
    
    appDelegate.tabbar = self.tabbar;
    
    self.tabbar.transitioningDelegate = self;
    
    register_viewObj = [[Register_ViewController alloc]initWithNibName:@"Register_ViewController" bundle:nil];
    forgot_password_viewObj = [[Forgot_password_ViewController alloc]initWithNibName:@"Forgot_password_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stop_spinner" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stop_spinner:) name:@"stop_spinner" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"close_popup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(close_popup:) name:@"close_popup" object:nil];

    //forgot_password_click
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"forgot_password_click" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forgot_password_click:) name:@"forgot_password_click" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"go_to_home_view" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_home_view:) name:@"go_to_home_view" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"login_with_facebook_click" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login_with_facebook_click:) name:@"login_with_facebook_click" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"login_with_twitter_click" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login_with_twitter_click:) name:@"login_with_twitter_click" object:nil];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"start_user_login" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(start_user_login:) name:@"start_user_login" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.view.userInteractionEnabled = YES;

    if ([[StaticClass retrieveFromUserDefaults:USER_IS_LOGIN] isEqualToString:@"0"])
    {
        [self stopAnimating];
    }
    else
    {
        self.navigationController.navigationBarHidden = YES;
        NSString *strName=[StaticClass retrieveFromUserDefaults:@"STOREUSERNAME"];
        NSString *strPassword=[StaticClass retrieveFromUserDefaults:@"STOREPASSWORD"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *paths1 = paths[0];
        NSLog(@"---->%@", paths1);
        NSString *strUserIdss=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
        
        if (![strName isEqualToString:@""]&&strName!=nil&&![strPassword isEqualToString:@""]&&strPassword != nil)
        {
            [self startAnimating];
            
            self.view.userInteractionEnabled = NO;
            
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = SIGNSALTAPIKEY;
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            NSString *myRequestString = [NSString stringWithFormat:@"salt=%@&sign=%@&username=%@&password=%@&device_token=%@",salt,sig,strName,strPassword,[StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"]];
            
            NSString *requestStr = [NSString stringWithFormat:@"%@get_login.php?%@",[[Singleton sharedSingleton] getBaseURL],[myRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            NSLog(@"%@",requestStr);
            
            [self startLogin:requestStr];
        }
        else if (![strName isEqualToString:@""]&&strName!=nil&&(![strUserIdss isEqualToString:@""]||![strUserIdss isEqualToString:@"0"]))
        {
            [self startAnimating];
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = SIGNSALTAPIKEY;
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            NSString *requestStr =[NSString stringWithFormat:@"%@get_facebook_login.php",[[Singleton sharedSingleton] getBaseURL]];
            NSLog(@"requestStr:%@",requestStr);
            
            NSString *emailIds=[StaticClass retrieveFromUserDefaults:@"STOREEMAILID"];
            NSString *strUid=[StaticClass retrieveFromUserDefaults:@"STOREFACEBOOKID"];
            
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sig, @"sign",
                                    salt, @"salt",
                                    emailIds,@"email",
                                    strUid,@"facebook_id",
                                    strName,@"username",
                                    [StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"],@"device_token=%@",
                                    nil];
            
            [self fbLogin:params requestStr:requestStr];
        }
        else
        {
            [self stopAnimating];
        }
    }
}

- (void)start_user_login:(NSNotification *)notification
{
    NSString *requestStr = notification.userInfo[@"requestStr"];
    [self startLogin:requestStr];
}

- (void)startLogin:(NSString *)requestStr
{
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        NSString *strResult=[responseObject valueForKey:@"success"];
        if ([strResult isEqualToString:@"false"]) {
            [self.btn_sign failedAnimationWithCompletion:nil];
            NSString *msg=[responseObject valueForKey:@"message"];
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            self.view.userInteractionEnabled = YES;
            [self stopAnimating];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"stop_spinner" object:nil];
        }
        else
        {
            [self.btn_sign succeedAnimationWithCompletion:^{
                NSDictionary *data =[responseObject valueForKey:@"data"];
                // if ([[result valueForKey:@"success"]isEqualToString:@"1" ]) {
                NSMutableArray *tempArray=[[NSMutableArray alloc] init];
                id tempDictFollow=[data objectForKey:@"following_username"];
                if ([tempDictFollow isKindOfClass:[NSString class]])
                {
                    if ([tempDictFollow isEqualToString:@""]||tempDictFollow==nil)
                    {
                        NSLog(@"nil found");
                    }
                    else
                    {
                        NSDictionary *tempFollwingDict=[data objectForKey:@"following_username"];
                        
                        for (NSDictionary *key in tempFollwingDict)
                        {
                            NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                            [tempArray addObject:str];
                        }
                    }
                }
                else
                {
                    NSDictionary *tempFollwingDict=[data objectForKey:@"following_username"];
                    
                    for (NSDictionary *key in tempFollwingDict)
                    {
                        NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                        [tempArray addObject:str];
                    }
                }
                
                [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
                
                NSLog(@"======>%@", [StaticClass urlDecode:[data valueForKey:@"username"]]);
                
                //close_popup
                [StaticClass saveToUserDefaults:[data valueForKey:@"user_id"] :USER_ID];
                [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"username"]] :USERNAME];
                [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"name"]] :USER_NAME];
                [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"image"]] :USER_IMAGE];
                
                [StaticClass saveToUserDefaults:[data valueForKey:@"is_private"] :@"ISPRIVATEPHOTOORNOT"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"close_popup" object:nil];
                [StaticClass saveToUserDefaults:@"1" :USER_IS_LOGIN];
                [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(goTOHOMEPUSH) userInfo:self repeats:NO];
                // }
            }];
        }
    } failure:^(NSString *errorString) {
        [self.btn_sign failedAnimationWithCompletion:nil];
        [self.activityViewObj stopAnimating];
        self.activityViewObj.hidden = YES;
        self.lblLogin.hidden = YES;
        self.btn_register.hidden=NO;
        self.btn_sign.hidden=NO;
        self.btn_facebook.hidden = NO;
        self.btn_forgotpassword.hidden = NO;
        self.lblRegister.hidden = NO;
        self.bottomView.hidden = NO;
        
        
        self.view.userInteractionEnabled = YES;
        
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:@"There was a problem with the Internet connection. Please try again later." hideAfter:2];
    }];
}

- (void)login_with_facebook_click:(NSNotification *)notification
{
    [self get_user_photo_facebok];
}

- (void)login_with_twitter_click:(NSNotification *)notification
{
    NSLog(@"Login in with twitter");
}

- (void)goTOHOMEPUSH
{
    [self go_to_home_view];
    
    [self.tabbar.badgeView updateAlertStatus];
}

- (void)startAnimating
{
    [self.activityViewObj startAnimating];
    self.activityViewObj.hidden = NO;
    self.lblLogin.hidden = NO;
    self.btn_register.hidden = YES;
    self.btn_sign.hidden = YES;
    self.btn_facebook.hidden = YES;
    self.btn_forgotpassword.hidden = YES;
    self.lblRegister.hidden = YES;
    self.bottomView.hidden = YES;

}

- (void)stopAnimating
{
    [self.activityViewObj stopAnimating];
    self.activityViewObj.hidden = YES;
    self.lblLogin.hidden = YES;
    self.btn_register.hidden = NO;
    self.btn_sign.hidden = NO;
    self.btn_facebook.hidden = NO;
    self.btn_forgotpassword.hidden = NO;
    self.lblRegister.hidden = NO;
    self.bottomView.hidden = NO;

}

- (void)awakeFromNib
{
    NSLog(@"awakeFromNib");
    ((AppDelegate*)([UIApplication sharedApplication].delegate)).tabbar = self.tabbar;
}

- (void)go_to_home_view
{
    int sure;
    @try
    {
        //[self.tabBarController setSelectedIndex:0];
        
        [self.tabbar setSelectedIndex:0];
        [self.activityViewObj stopAnimating];
        self.activityViewObj.hidden=YES;
        [self.navigationController pushViewController:self.tabbar animated:YES];
        sure = 0;
    }
    @catch (NSException * ex)
    {
        NSLog(@"Exception: [%@]:%@",[ex  class], ex );
        NSLog(@"ex.name:'%@'", ex.name);
        NSLog(@"ex.reason:'%@'", ex.reason);
        //Full error includes class pointer address so only care if it starts with this error
        NSRange range = [ex.reason rangeOfString:@"Pushing the same view controller instance more than once is not supported"];
        
        if([ex.name isEqualToString:@"NSInvalidArgumentException"] &&
           range.location != NSNotFound)
        {
            [StaticClass saveToUserDefaults:@"1" :IS_HOME_PAGE_REFRESH];
            [self.tabbar setSelectedIndex:0];
            [self.navigationController popToViewController:self.tabbar animated:NO];
            sure =1;
        }else{
            NSLog(@"ERROR:UNHANDLED EXCEPTION TYPE:%@", ex);
        }
    }
    @finally
    {
        NSLog(@"finally");
        if (sure==1) {
            sure=0;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (IBAction)btn_sign_click:(id)sender
{
    ddAlert =[[CustomAlertView alloc]initWithDelegate:self theme:DDSocialDialogThemePlurk alertImage:nil];
    [ddAlert show];
}

- (IBAction)btn_register_click:(id)sender
{
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:register_viewObj ];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)forgot_password_click:(NSNotification *)notification
{
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:forgot_password_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
    
}

- (void)go_to_home_view:(NSNotification *)notification
{
    [self go_to_home_view];
}

#pragma mark Facebook Login

#pragma mark Facebook Add Photo
- (void)get_user_photo_facebok
{
    [self startAnimating];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
    {
        if (error) {
            NSLog(@"Process error");
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
        } else {
            if ([result.grantedPermissions containsObject:@"email"])
            {
                NSLog(@"Logged in");
                
                NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@"id,first_name,last_name,name,email,picture.height(180).width(180)" forKey:@"fields"];
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                     
                     if (!error)
                     {
                         NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                         NSString *key = SIGNSALTAPIKEY;
                         NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                         NSString *sig = [StaticClass returnMD5Hash :tempStr];
                         
                         NSString *requestStr =[NSString stringWithFormat:@"%@get_facebook_login.php",[[Singleton sharedSingleton] getBaseURL]];
                         NSLog(@"requestStr:%@",requestStr);
                         
                         NSString *strName = result[@"name"];
                         NSString *strEmail = result[@"email"];
                         NSString *strUid = result[@"id"];
                         NSString *strUserName = result[@"first_name"];
                         NSString *strGender = @"";
                         
                         NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 sig, @"sign",
                                                 salt, @"salt",
                                                 strName,@"name",
                                                 strEmail,@"email",
                                                 strUid,@"facebook_id",
                                                 strUserName,@"username",
                                                 strGender,@"gender",
                                                 [StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"],@"device_token=%@",
                                                 nil];
                         NSLog(@"params:%@",params);
                         
                         [self fbLogin:params requestStr:requestStr];
                     }
                     else
                     {
                         
                     }
                 }];
            }
        }
    }];
}

- (void)fbLogin:(NSDictionary *)params requestStr:(NSString *)requestStr
{
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"1"]){
            
            NSDictionary *data =[responseObject valueForKey:@"data"];
            
            [StaticClass saveToUserDefaults:[data valueForKey:@"uid"] :USER_ID];
            [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"username"]] :USERNAME];
            [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"name"]] :USER_NAME];
            [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"pic"]] :USER_IMAGE];
            
            [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"username"]] :@"STOREUSERNAME"];
            [StaticClass saveToUserDefaults:[StaticClass urlDecode:[data valueForKey:@"email"]] :@"STOREEMAILID"];
            [StaticClass saveToUserDefaults:[data valueForKey:@"facebook_id"] :@"STOREFACEBOOKID"];
            
            [StaticClass saveToUserDefaults:@"1" :USER_IS_LOGIN];
            
            [GlobalDefine performBlock:^{
                [self go_to_home_view];
            } afterDelay:0.5f];
        }
    } failure:^(NSString *errorString) {
        [self stopAnimating];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't login" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)loginwithfacebook:(id)sender {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"login_with_facebook_click" object:nil];
}
- (IBAction)forgotpassword:(id)sender {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"forgot_password_click" object:nil];
}

-(IBAction)btn_login_click:(id)sender {
    if(self.txtUsername.text.length==0){
        [self.btn_sign failedAnimationWithCompletion:nil];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(self.txtPassword.text.length==0){
        [self.btn_sign failedAnimationWithCompletion:nil];
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
//    [SVProgressHUD show];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *myRequestString = [NSString stringWithFormat:@"salt=%@&sign=%@&username=%@&password=%@&device_token=%@",salt,sig,self.txtUsername.text,self.txtPassword.text,[StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_login.php?%@",[[Singleton sharedSingleton] getBaseURL],[myRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSLog(@"%@",requestStr);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"start_user_login" object:nil userInfo:@{@"requestStr": requestStr}];
}

-(void)stop_spinner:(NSNotification *)notification {
    [SVProgressHUD dismiss];
}

-(void)close_popup:(NSNotification *)notification{
    [SVProgressHUD dismiss];
    
    if ( ![self.txtUsername.text isEqualToString:@""] )
    {
        NSString *strUserName=self.txtUsername.text;
        NSString *strPassword=self.txtPassword.text;
        
        [StaticClass saveToUserDefaults:strUserName :@"STOREUSERNAME"];
        [StaticClass saveToUserDefaults:strPassword :@"STOREPASSWORD"];
    }
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    return [[HyTransitions alloc]initWithTransitionDuration:0.4f StartingAlpha:0.5f isPush:true];
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return [[HyTransitions alloc]initWithTransitionDuration:0.4f StartingAlpha:0.8f isPush:false];
}

@end
