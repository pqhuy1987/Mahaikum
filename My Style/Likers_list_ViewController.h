//
//  Likers_list_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Likers_list_cell.h"
#import "UIImageView+PMRoundEffect_imageview.h"
#import "Singleton.h"
#import "StaticClass.h"
#import "like_share.h"

@interface Likers_list_ViewController : UIViewController {
    UITableView *tbl_likers;
    NSMutableArray *array_likers;
    NSString *image_id;
}

@property(nonatomic,strong)IBOutlet UITableView *tbl_likers;
@property(nonatomic,strong)NSMutableArray *array_likers;
@property(nonatomic,strong)NSString *image_id;

@end
