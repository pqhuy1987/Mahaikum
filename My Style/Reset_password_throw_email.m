//
//  Reset_password_throw_email.m
//  My Style
//
//  Created by Tis Macmini on 6/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Reset_password_throw_email.h"
#import "AppDelegate.h"
@interface Reset_password_throw_email ()

@end

@implementation Reset_password_throw_email

@synthesize str_username,str_email,str_image_url;
@synthesize img_cell_bg,img_image,btnEmail,btnFacebook;
@synthesize lbl_username,lbl_dec,reset_fb_viewObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =4.0f;
    
    self.img_image.layer.cornerRadius = 4.0f;
    
    self.reset_fb_viewObj =[[Rest_password_throw_fb alloc]initWithNibName:@"Rest_password_throw_fb" bundle:nil];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        // appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self.img_image sd_setImageWithURL:[NSURL URLWithString:self.str_image_url] placeholderImage:nil];
    //self.img_image.imageURL =[NSURL URLWithString:self.str_image_url];
    self.lbl_username.text = self.str_username;
    self.lbl_dec.text =[NSString stringWithFormat:@"Hi %@. How would you like to reset your password?",self.str_username];
    
    if (isiPhone) {
        btnEmail.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    }else{
        btnEmail.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:32.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:32.0];
    }
}

-(IBAction)btn_reset_useing_email:(id)sender{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14228" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendPasswordAPIResponce:) name:@"14228" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14228" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailsendPasswordAPIResponce:) name:@"-14228" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@send_password.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            self.str_email,@"email",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14228" :params];
}

-(void)sendPasswordAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14228" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14228" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
         //[MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Password send to your email ID. Please cheack your mail." hideAfter:2];
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Password send to your email ID. Please check your mail." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)FailsendPasswordAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14228" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14228" object:nil];
 //   [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning!" subtitle:@"Please check your internet connection." hideAfter:2];
    
    [self stop_activity];
}

-(IBAction)btn_reset_useing_facebook:(id)sender{

    [self start_activity];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self get_user_data_from_fb];
    }];
}

-(void)get_user_data_from_fb{
    
    NSLog(@"login facebook click");
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"me/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
}

- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    
     [self stop_activity];
    
    if (error) {
        
        
        NSLog(@"%@", error.debugDescription);
       
    } else {
        [StaticClass saveToUserDefaults:@"1" :BY_FACEBOOK_LOGIN];
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSLog(@"%@",[dictionary valueForKey:@"email"]);
        

        if ([[dictionary valueForKey:@"email"] isEqualToString:self.str_email]) {
            
            self.reset_fb_viewObj.str_username =self.str_username;
            self.reset_fb_viewObj.str_image_url =self.str_image_url;
            self.reset_fb_viewObj.str_email =self.str_email;
            [self.navigationController pushViewController:self.reset_fb_viewObj animated:YES];
            
        }else{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Facebook" message:@"Sorry, this Facebook account is not linked to your Mahalkum account. Please request a password reset email instead." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }

        
    }
}


#pragma mark - UIA;ertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Start Stop Activity
-(void)start_activity{
    [DejalBezelActivityView activityViewForView:self.view];
}
-(void)stop_activity{
    [DejalBezelActivityView removeView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
