//
//  Share_photo.h
//  My Style
//
//  Created by Tis Macmini on 5/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface Share_photo : UIViewController<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>{

    
    UIScrollView *scrollview;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_secondcell;
    
    UIImageView *img_show_photo;
    UIView *view_bg_show_photo;
    UITextView *txt_desc;
    
    
    UIView *view_third_cell;
    UIImageView *img_bg_third_cell;
    
    NSString *str_img_url;

}


@property(nonatomic,strong)NSString *str_img_url;

@property(nonatomic,strong)IBOutlet UITextView *txt_desc;
@property(nonatomic,strong)IBOutlet UIImageView *img_show_photo;
@property(nonatomic,strong)IBOutlet UIView *view_bg_show_photo;

@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell;


@property(nonatomic,strong)IBOutlet UIView *view_third_cell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_third_cell;
@end
