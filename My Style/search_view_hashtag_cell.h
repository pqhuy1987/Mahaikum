//
//  search_view_hashtag_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/1/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface search_view_hashtag_cell : UITableViewCell{

    UILabel *lbl_title;
    UILabel*lbl_ImageCaption;
    UIImageView *image_photo;

}
@property(nonatomic,strong)IBOutlet UILabel *lbl_title;
@property(nonatomic,strong)IBOutlet UILabel*lbl_ImageCaption;
@property (strong, nonatomic) IBOutlet UIImageView *image_photo;
@end
