//
//  Comments_list_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/27/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "Comments_list_cell.h"
#import "UIImageView+PMRoundEffect_imageview.h"
#import "AJNotificationView.h"
#import "Singleton.h"
#import "StaticClass.h"

#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "Comment_share.h"
#import "BlockActionSheet.h"
#import "Home_tableview_data_share.h"
#import "UITextView+AVTagTextView.h"
#import "STXCommentCell.h"
@class Comments_list_ViewController;

@protocol Comment_list_viewcontrollerDelegate <NSObject>

@optional


@end

@interface Comments_list_ViewController : UIViewController<AVTagTextViewDelegate,UITextViewDelegate,HPGrowingTextViewDelegate>
{
    UIView *containerView;
    UITextView *textView;
}

@property(nonatomic,strong) IBOutlet UITableView *tblSearch;
@property(nonatomic,strong) IBOutlet UITableView *tbl_comments;
@property(nonatomic,strong)IBOutlet UILabel *imageCaption_lbl;

@property (strong, nonatomic) IBOutlet UIView *containerView1;
@property(nonatomic,strong)id<Comment_list_viewcontrollerDelegate>delegate;
@property(nonatomic,strong) NSMutableArray *searchArray;
@property(nonatomic,strong) NSMutableArray *tagsArray;
@property(nonatomic,strong) NSString *comment_id;
@property(nonatomic,strong) NSMutableArray *array_comments;
@property(nonatomic,strong) NSString *image_id;
@property(nonatomic,strong) NSString*objOwnPost;
@property(nonatomic,strong) NSString*post_user_id;
@property(nonatomic,strong) NSString *str_comments;
@property(nonatomic,strong) UIButton *btn_reply;
@property(nonatomic,strong) UIButton *btn_delete;
@property(nonatomic,strong) UIButton *btn_Warning;
@property(nonatomic,strong) NSString*imageCaption_Tittle;
@property(nonatomic,strong) UIButton *btn_Spam;
@property(nonatomic,strong) UIButton *btn_Abusive;
@property(nonatomic,strong) UIButton *doneBtn;
@property (nonatomic) UISwipeGestureRecognizerDirection sideSwipeDirection;
@property (nonatomic) BOOL animatingSideSwipe;

- (void)resignTextView;
- (Comments_list_cell *)comment_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
//-(void)setCurrentcommentArray:(NSMutableArray **)arrs;

@end
