//
//  GoogleMap_ViewController.h
//  Mahalkum
//
//  Created by user on 6/26/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#ifndef GoogleMap_ViewController_h
#define GoogleMap_ViewController_h


#import <UIKit/UIKit.h>

@interface GoogleMap_ViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *mapView;
@property NSString *str_latitude;
@property NSString *str_longitude;
@property NSString *str_name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_username;
- (IBAction)btn_dirction_clicked:(id)sender;

@end

#endif /* GoogleMap_ViewController_h */
