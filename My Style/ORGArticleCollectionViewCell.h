//
//  ORGArticleCollectionViewCell.h
//  HorizontalCollectionViews
//
//  Created by James Clark on 4/23/13.
//  Copyright (c) 2013 OrgSync, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ORGArticleCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *articleImage;
@property (nonatomic, strong) IBOutlet UILabel *articleTitle;

@end
