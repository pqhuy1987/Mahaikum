//
//  Search_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "image_collection_header.h"
#import "image_collection_footer.h"
#import "image_collection_cell.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Home_tableview_data_share.h"
#import "AJNotificationView.h"

#import "Comment_share.h"
#import "Image_detail.h"
#import "search_view_cell.h"

#import "User_info_ViewController.h"
#import "VerifiedUsersViewController.h"
//#import "FMDatabase.h"
#import "search_view_hashtag_cell.h"
#import "JOLImageSlider.h"
#import "Hash_tag_ViewController.h"
#import "MCSegmentedControl.h"
#import "Comments_list_ViewController.h"

@interface Search_ViewController : UIViewController <JOLImageSliderDelegate> {

    User_info_ViewController *user_info_view;
    
    UICollectionView *collectionViewObj;
    NSMutableArray *array_search;
    UIView *view_header;
    UIView *view_footer;
    UIImageView *img_spinner,*imgsegmentusers,*imgsegmenthashtags;
    Image_detail *image_detail_viewObj;
    
    UIView *view_search;
    UITextField *txt_search;
    
 //   MCSegmentedControl *segment;
    UISegmentedControl *segment;
    UITableView *tbl_users;
    UITableView *tbl_hashtag;
    NSMutableArray *array_all_users;
    NSMutableArray *array_search_result;
    NSMutableArray *array_hashtag_result;
    
    NSMutableDictionary *dict_hashposts;
    
    Hash_tag_ViewController *hash_tag_viewObj;
    
    UIView *view_fillter;
    
    UILabel *lbl_header;
    int which_filter;
    UIActivityIndicatorView *activity;
    
    UILabel *lblSpinnerBg;
    UIImageView *imgArrow;
    
    //Comments_list_ViewController *comments_list_viewObj;
}

@property(nonatomic,strong) Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,strong)IBOutlet UIImageView *imgArrow;
@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong)IBOutlet UILabel *lbl_header;
@property(nonatomic,strong)IBOutlet UIView *view_fillter;
@property(nonatomic,strong) User_info_ViewController *user_info_view;
@property(nonatomic,strong) VerifiedUsersViewController *verifieduser_info_view;
@property(nonatomic,strong)IBOutlet UIView *view_search;
@property(nonatomic,strong)IBOutlet UITextField *txt_search;
//@property(nonatomic,retain)IBOutlet  MCSegmentedControl *segment;
@property(nonatomic,strong)IBOutlet  UICollectionView *collectionViewObj;
@property(nonatomic,strong)IBOutlet  NSMutableArray *array_search;
@property(nonatomic,strong)IBOutlet UIView *view_header;
@property(nonatomic,strong)IBOutlet UIView *view_footer;
@property(nonatomic,strong)IBOutlet UIImageView *img_spinner,*imgsegmentusers,*imgsegmenthashtags;
@property(nonatomic,strong) Image_detail *image_detail_viewObj;
@property (strong, nonatomic) IBOutlet UISearchBar *search_bar;

@property(nonatomic,strong)IBOutlet  UITableView *tbl_users;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_hashtag;
@property(nonatomic,strong) NSMutableArray *array_all_users;
@property(nonatomic,strong) NSMutableArray *array_search_result;
@property(nonatomic,strong)  NSMutableArray *array_hashtag_result;
@property(nonatomic,strong) Hash_tag_ViewController *hash_tag_viewObj;
@property(nonatomic,strong)IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *viewSegment;

@property(nonatomic,strong)IBOutlet UIButton *btn_reload;
@property(nonatomic,strong) UIActivityIndicatorView *activity;

@property NSMutableArray *topLikes;
@property NSMutableArray *freshs;
@property (strong, nonatomic) IBOutlet UITableView *verifyUsersTableView;
@property (strong, nonatomic) IBOutlet UICollectionView *postCollectionView;


@property search_view_cell *objVerifiedUsers_list_cell;

@property (nonatomic, strong) NSMutableArray *posts;

-(IBAction)btn_fillter_clikc:(id)sender;
- (void)refreshFeeds;
@end
