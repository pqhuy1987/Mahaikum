//
//  Facebook_follow_frnd_list.h
//  My Style
//
//  Created by Tis Macmini on 5/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "DejalActivityView.h"
#import "Suggested_friend_list_reg.h"

@interface Facebook_follow_frnd_list : UIViewController{

    NSMutableArray *array_friend;
    UITableView *tbl_friend;
    Suggested_friend_list_reg *suggested_viewObj;
}

@property(nonatomic,strong) Suggested_friend_list_reg *suggested_viewObj;

@property(nonatomic,strong)NSMutableArray *array_friend;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_friend;
@property BOOL loadFriends;
@end
