//
//  Home_tableview_cell.m
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Home_tableview_cell.h"

#define CGRectSetPos( r, x, y ) CGRectMake( x, y, r.size.width, r.size.height )

@implementation Home_tableview_cell
@synthesize img_big,btn_likes_count,btn_like,btn_comment,btn_photo_option;
@synthesize lbl_desc,img_likes,view_like_btn,img_desc,dlstarObj,btn_view_all_comments;
@synthesize lbl_comments,imgDetailBG,lblavgrating,lbltotrating,ratingview;
@synthesize dlstarAvgObj,ratingimg,imgWhiteImage;

-(void)draw_desc_in_cell {
    self.lbl_desc = [[LORichTextLabel alloc] initWithWidth:289];
	[self.lbl_desc setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
	[self.lbl_desc setTextColor:[UIColor whiteColor]];
	[self.lbl_desc setBackgroundColor:[UIColor clearColor]];
	[self.lbl_desc positionAtX:26.0 andY:338.0];
    [self.contentView addSubview:self.lbl_desc];
    
    // Comments view
     self.lbl_comments = [[LORichTextLabel alloc] initWithWidth:289];
    [self.lbl_comments setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
	[self.lbl_comments setTextColor:[UIColor whiteColor]];
	[self.lbl_comments setBackgroundColor:[UIColor clearColor]];
	[self.lbl_comments positionAtX:26.0 andY:338.0];
    [self.contentView addSubview:self.lbl_comments];
    
    [self.img_big setBackgroundColor:[UIColor whiteColor]];
    //[self.img_big setPlaceholderImage:[UIImage imageNamed:@"placeholder.png"]];
    [self.img_big setContentMode:UIViewContentModeScaleAspectFill];
    
    imgDetailBG.frame=CGRectMake(0,0,320,65);//342-2-15-6-3
}

-(void)setImageURL:(NSURL *)imageURL placeholderImage:(UIImage *)placeholderImage{
    [self.img_big sd_setImageWithURL:imageURL placeholderImage:placeholderImage];
}

-(void)draw_like_button_in_cell {
    self.view_like_btn = [[LORichTextLabel alloc] initWithWidth:289];
    [self.view_like_btn setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    //[self.view_like_btn setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10.0]];
	[self.view_like_btn setTextColor:[UIColor whiteColor]];
	[self.view_like_btn setBackgroundColor:[UIColor clearColor]];
    [self.view_like_btn positionAtX:26.0 andY:320.0f];
    [self.contentView addSubview:self.view_like_btn];
    
    
}

+(float)get_tableview_hight:(Home_tableview_data_share *)shareObj {
    int flgs=0;
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:13.0];
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor whiteColor]];
    
    LORichTextLabel *lbl_temp = [[LORichTextLabel alloc] initWithWidth:289];
	[lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:13.0]];
    [lbl_temp setTextColor:[UIColor whiteColor]];
    [lbl_temp addStyle:urlStyle forPrefix:@"#"];
    [lbl_temp addStyle:urlStyle forPrefix:@"@"];
    

    [lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [lbl_temp addStyle:urlStyle forPrefix:@"www."];
    [lbl_temp addStyle:urlStyle forPrefix:@"WWW."];
    [lbl_temp addStyle:urlStyle forPrefix:@"Www."];
    [lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    [lbl_temp addStyle:urlStyle forPrefix:@"https://"];
    [lbl_temp addStyle:urlStyle forPrefix:@"Https://"];
    
//    [lbl_temp addStyle:urlStyle forPrefix:@"http://"];
//    [lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    
    
    [lbl_temp addStyle:urlStyle forPrefix:shareObj.username];
    
    float height = 316.0f;
    if ([shareObj.likes intValue]==0){
       // NSLog(@"Like Count 0");
    }
    else {
        flgs=1;
        if ([shareObj.likes intValue]<6) {
            
            lbl_temp.text =[shareObj.array_liked_by componentsJoinedByString:@" "];
            
            for (NSString *temusername in shareObj.array_liked_by) {
                [lbl_temp addStyle:urlStyle forPrefix:temusername];
            }
            height = height+8+17+2;//height+8+17
        }
        else {
            height = height+8+17+2;//height+8+17
        }
    }
    
    [lbl_temp setText:[NSString stringWithFormat:@"%@ %@",[StaticClass urlDecode: shareObj.username],shareObj.description]];
    
    if (shareObj.description.length==0 &&[shareObj.likes intValue]!=0) {
//        if ([shareObj.array_comments count]>0) {
//            height =height+8+17;
//        }
//        else {
//            height =height+8;
//        }
        height =height+8;
    }
    else if (shareObj.description.length>0 ) {
        flgs=1;
        height =height + lbl_temp.height+8;
    }
    else {
        
    }
    
    if (shareObj.array_comments.count>3) {
        height =height +25.0f;
    }
    
    if (shareObj.array_comments.count >0) {
        flgs=1;
        NSMutableString *coments =[[NSMutableString alloc]init];
        
        int comment_count_flag =0;
        
        for (Comment_share *obj in shareObj.array_comments) {
            
            NSString *tem =[NSString stringWithFormat:@"%@ %@",[StaticClass urlDecode:obj.username],obj.comment_desc];
            NSArray *temarray =[tem componentsSeparatedByString:@" "];
            NSMutableArray *array =[NSMutableArray arrayWithArray:temarray];
            
            if ([[array lastObject] isEqualToString:@""]) {
                [array removeLastObject];
            }
            
            [coments appendFormat:@"\n\n %@",[array componentsJoinedByString:@" "]];
            [lbl_temp addStyle:urlStyle forPrefix:obj.username];
            comment_count_flag++;
            if (comment_count_flag>2) {
                break;
            }
        }
        
        NSMutableString * newString = [NSMutableString stringWithString:coments];
        NSRange foundRange = [newString rangeOfString:@"\n\n"];
        if (foundRange.location != NSNotFound)
        {
            [newString replaceCharactersInRange:foundRange withString:@""];
        }
        
        lbl_temp.text =[NSString stringWithFormat:@"%@",[StaticClass urlDecode:newString]];
        
        if ([[NSString stringWithFormat:@"%@",coments]length]==0) {

        }
        else{
            height =height + lbl_temp.height+8-5;
        }
    }
    
    if (flgs==1) {
        height = height+35.0+8.0f+2;
    }
    else {
        height = height+35.0+2;
    }
    
    //NSLog(@"Image IDs:%@ AND height:%f",shareObj.image_id,height);
    
    return height;
}

-(void)redraw_cell:(Home_tableview_data_share *)shareObj andUserStyle:(LORichTextLabelStyle *)userStyle AtIndexPath:(NSIndexPath *)indexPath {
    int flgs=0;
    [self.lbl_desc addStyle:userStyle forPrefix:[StaticClass urlDecode:shareObj.username]];
    
    [self setImageURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    
    if ([shareObj.liked isEqualToString:@"no"]) {
        [self.btn_like setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btn_like setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }

    self.btn_photo_option.tag=indexPath.section;
    self.btn_like.tag=indexPath.section;
    self.btn_likes_count.tag =indexPath.section;
    self.btn_comment.tag = indexPath.section;
    self.btn_view_all_comments.tag = indexPath.section;
    [self.dlstarObj setTag:indexPath.section];
    [self.dlstarObj setRating:[shareObj.my_rating intValue]];
    [self.dlstarAvgObj setRating:[shareObj.avgrating integerValue]];
    
    
    [ratingview bringSubviewToFront:img_big];
    [img_big bringSubviewToFront:imgDetailBG];
    if ([shareObj.avgrating integerValue] == 1) {
        self.ratingview.hidden = NO;
        self.ratingimg.image = [UIImage imageNamed:@"1star2.png"];
    }
    else if ([shareObj.avgrating integerValue] == 2){
        self.ratingview.hidden = NO;
        self.ratingimg.image = [UIImage imageNamed:@"2star2.png"];
    }
    else if ([shareObj.avgrating integerValue] == 3){
        self.ratingview.hidden = NO;
        self.ratingimg.image = [UIImage imageNamed:@"3star2.png"];
    }
    else if ([shareObj.avgrating integerValue] == 4){
        self.ratingview.hidden = NO;
        self.ratingimg.image = [UIImage imageNamed:@"4star2.png"];
    }
    else if ([shareObj.avgrating integerValue] == 5){
        self.ratingview.hidden = NO;
        self.ratingimg.image = [UIImage imageNamed:@"5star2.png"];
    }
    else{
        self.ratingview.hidden = YES;
    }
    
    self.dlstarAvgObj.userInteractionEnabled=NO;
    
    float height = 316.0f;
    if ([shareObj.likes intValue]==0){
        self.btn_likes_count.hidden=YES;
        self.img_likes.hidden = YES;
        self.view_like_btn.hidden =YES;
    }
    else{
        flgs=1;
        self.img_likes.hidden = NO;
        
        if ([shareObj.likes intValue]<6) {
            self.view_like_btn.text =[shareObj.array_liked_by componentsJoinedByString:@" "];
            self.view_like_btn.hidden=NO;
            self.btn_likes_count.hidden=YES;
            
            for (NSString *temusername in shareObj.array_liked_by) {
                [self.view_like_btn addStyle:userStyle forPrefix:temusername];
            }
            self.view_like_btn.frame =CGRectMake(26,height+10,289,17);//HEIGHT 314//+8
        }
        else {
            self.view_like_btn.hidden=YES;
            self.btn_likes_count.hidden=NO;
            
            [self.btn_likes_count setTitle:[NSString stringWithFormat:@"%@ likes",shareObj.likes] forState:UIControlStateNormal];
            self.view_like_btn.frame =CGRectMake(26,height+10,289,17);//HEIGHT 358//+8
        }
        
        height=height+8+17+2;
    }

    [self.lbl_desc setText:[NSString stringWithFormat:@"%@ %@",[StaticClass urlDecode:shareObj.username],[StaticClass urlDecode:shareObj.description]]];
    
    if (shareObj.description.length==0) {
        self.lbl_desc.hidden=YES;
        self.img_desc.hidden=YES;
    }
    else {
        self.lbl_desc.hidden=NO;
        self.img_desc.hidden=NO;
    }
    
    if (shareObj.description.length==0  &&[shareObj.likes intValue]!=0) {
        self.lbl_desc.hidden=YES;
        self.img_desc.hidden=YES;
        height =height+8;
    }
    else if (shareObj.description.length>0) {
        flgs=1;
        self.lbl_desc.hidden=NO;
        self.img_desc.hidden=NO;
        
        self.img_desc.frame = CGRectMake(self.img_desc.frame.origin.x, height+8, self.img_desc.frame.size.width, self.img_desc.frame.size.height);
        
        self.lbl_desc.frame = CGRectMake(self.lbl_desc.frame.origin.x, height+8, self.lbl_desc.frame.size.width, self.lbl_desc.frame.size.height);
        //CGRectSetPos(self.lbl_desc.frame,  height+8, height+8);//height+50
       
        height = height + self.lbl_desc.size.height +8;
    }
    else {
        
    }
  
    if ([shareObj.array_comments count]>3) {
        self.btn_view_all_comments.hidden =NO;
        if (shareObj.description.length>0) {
            self.btn_view_all_comments.frame = CGRectMake(26,height+8,270,17);
        }
        else {
            self.btn_view_all_comments.frame = CGRectMake(26,height+8-5,270,17);
        }
        
        [self.btn_view_all_comments setTitle:[NSString stringWithFormat:@"view all %@ comments",shareObj.comment_count] forState:UIControlStateNormal];
        //[self.btn_view_all_comments addTarget:self action:@selector(buttonClickedtoview_All_Command:) forControlEvents:UIControlEventTouchUpInside];

        height =height +25.0f;
    }
    else {
        self.btn_view_all_comments.hidden=YES;
    }
    
    if (shareObj.array_comments.count >0) {
        flgs=1;
        self.lbl_comments.hidden =NO;
        NSMutableString *coments =[[NSMutableString alloc]init];
        
        int comment_count_flag =0;
        
        for (Comment_share *obj in shareObj.array_comments) {

            NSString *tem =[NSString stringWithFormat:@"%@ %@",[StaticClass urlDecode:obj.username],[StaticClass urlDecode:obj.comment_desc]];
            
            NSArray *temarray =[tem componentsSeparatedByString:@" "];
            NSMutableArray *array =[NSMutableArray arrayWithArray:temarray];
            
            if ([[array lastObject] isEqualToString:@""]) {
                [array removeLastObject];
            }
            
            [coments appendFormat:@"\n\n %@",[array componentsJoinedByString:@" "]];
            [self.lbl_comments addStyle:userStyle forPrefix:[StaticClass urlDecode:obj.username]];
            comment_count_flag++;
            if (comment_count_flag>2) {
                break;
            }
        }
        NSMutableString * newString = [NSMutableString stringWithString:coments];
        NSRange foundRange = [newString rangeOfString:@"\n\n"];
        if (foundRange.location != NSNotFound) {
            [newString replaceCharactersInRange:foundRange withString:@""];
        }

        self.lbl_comments.text =[NSString stringWithFormat:@"%@",[StaticClass urlDecode:newString]];
        if ([[NSString stringWithFormat:@"%@",coments]length]==0) {
            
        }
        else {            
            self.lbl_comments.frame =CGRectMake(self.lbl_comments.frame.origin.x, height+8-5, self.lbl_comments.frame.size.width, self.lbl_comments.frame.size.height);// CGRectSetPos(self.lbl_comments.frame, 25, height+8);
            
            if (shareObj.description.length==0) {
                self.img_desc.hidden =NO;
                
                if ([shareObj.array_comments count]>3) {
                    self.img_desc.frame = CGRectMake(self.img_desc.frame.origin.x, height-25+8-5, self.img_desc.frame.size.width, self.img_desc.frame.size.height);
                }
                else {
                    self.img_desc.frame = CGRectMake(self.img_desc.frame.origin.x, height+8-5, self.img_desc.frame.size.width, self.img_desc.frame.size.height);
                }
            }
            height =height +  self.lbl_comments.height+8;
        }
    }
    else {
        self.lbl_comments.hidden =YES;
    }
    
    if (flgs==1) {
        height=height+8;
    }
    else {
        height=height;
    }
    
    imgDetailBG.frame=CGRectMake(0,342-2-15-6-3,320,height+2);
    
    self.btn_like.frame = CGRectMake(self.btn_like.frame.origin.x,height+5, self.btn_like.frame.size.width, self.btn_like.frame.size.height);
    
    self.btn_comment.frame = CGRectMake(self.btn_comment.frame.origin.x,height+5, self.btn_comment.frame.size.width, self.btn_comment.frame.size.height);
    
    self.dlstarObj.frame =CGRectMake(self.dlstarObj.self.origin.x,height+9, self.dlstarObj.frame.size.width, self.dlstarObj.frame.size.height);
    
    self.btn_photo_option.frame = CGRectMake(self.btn_photo_option.frame.origin.x,height+5, self.btn_photo_option.frame.size.width, self.btn_photo_option.frame.size.height);
    
    self.imgWhiteImage.frame = CGRectMake(self.imgWhiteImage.frame.origin.x,height, self.imgWhiteImage.frame.size.width, self.imgWhiteImage.frame.size.height);
}
-(void)buttonClickedtoview_All_Command:(id)sender
{
    
}
@end
