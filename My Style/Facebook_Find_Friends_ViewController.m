//
//  Facebook_Find_Friends_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Facebook_Find_Friends_ViewController.h"
#import "AppDelegate.h"

@interface Facebook_Find_Friends_ViewController ()

@end

@implementation Facebook_Find_Friends_ViewController

@synthesize view_find_friend,tbl_friend,array_friend;
@synthesize array_frnd,img_cell_bg,suggested_viewObj;
@synthesize btnContact,btnFacebook;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_cell_bg.layer.cornerRadius =4.0f;
    
    self.suggested_viewObj =[[Suggested_friend_list_reg alloc]initWithNibName:@"Suggested_friend_list_reg" bundle:nil];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    if (!appDelegate.session.isOpen) {
     //   appDelegate.session = [[FBSession alloc] init];
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            
            
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    
    // Get Friends List of Facebook Friends
    self.array_friend=[[NSMutableArray alloc]init];
    self.array_frnd =[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"register_get_friend_list" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_facebok_list:) name:@"register_get_friend_list" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFollowResponce:) name:@"10" object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailReson:) name:@"-10" object:nil];
    

}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self.tbl_friend setHidden:YES];
    
    if (isiPhone) {
        btnContact.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    }else{
        btnContact.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:20.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    }
    
}
-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)btn_skip_click:(id)sender{
    [self.navigationController pushViewController:self.suggested_viewObj animated:YES];
}
#pragma mark Find Facebook Friend btn Click
-(IBAction)btn_facebook_click:(id)sender{

    Facebook_follow_frnd_list *obj =[[Facebook_follow_frnd_list alloc]initWithNibName:@"Facebook_follow_frnd_list" bundle:nil];
    obj.loadFriends = YES;
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark Find Contact Friend btn Click
-(IBAction)btn_contact_click:(id)sender{
    
    Contact_list_friend *obj =[[Contact_list_friend alloc]initWithNibName:@"Contact_list_friend" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
  
}

#pragma mark Find Facebook 

-(IBAction)btn_find_facebook_click:(id)sender{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        appDelegate.session = [[FBSession alloc] init];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self get_friend_list];
    }];
    
    
}

-(void)get_friend_list{

    
    NSString *query=@"SELECT uid,name,first_name,last_name,pic_square FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        [self requestFacebookUserInfoCompleted:connection result:result error:error];
        [self performSelector:@selector(reloadTable) withObject:self afterDelay:0.3];
    };
    [requester addRequest:request completionHandler:handler];
    
    [requester start];
    
   
}
- (void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection
                                  result:(id)result
                                   error:(NSError *)error {
    
    
    if (error) {
        
        
        NSLog(@"%@", error.localizedDescription);
    } else {
        FBGraphObject *dictionary = (FBGraphObject *)result;
        // NSString* userId = (NSString *)[dictionary objectForKey:@"id"];
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            

            [self.array_friend removeAllObjects];
            
            for (id result in tempResult) {
                [self.array_friend addObject:[result objectForKey:@"uid"]];
            }
            
        });
        
    }
    
}

-(void)reloadTable{

    NSLog(@"%@",self.array_friend);
  //  http://www.techintegrity.in/mystyle/get_facebook_users.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&fbId=array
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14211" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getFacebookUsersResponce:) name:@"14211" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14211" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetFacebookUsersResponce:) name:@"-14211" object:nil];
    
    NSMutableString *fb_list=[[NSMutableString alloc] init];
    [fb_list appendString:[self.array_friend componentsJoinedByString:@","]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_facebook_users.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [NSString stringWithFormat:@"%@",fb_list],@"fbId",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14211" :params];
}

-(void)getFacebookUsersResponce:(NSNotification *)notification {
    //  [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14211" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14211" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    [self.array_frnd removeAllObjects];
    
    for(NSString *dict in result){
        Faceabook_find_friend_Share *shareObj =[[Faceabook_find_friend_Share alloc]init];
        shareObj.email =[dict valueForKey:@"email"];
        shareObj.facebook_id=[dict valueForKey:@"facebook_id"];
        shareObj.user_id=[dict valueForKey:@"id"];
        shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
        shareObj.name=[dict valueForKey:@"name"];
        shareObj.url_image=[dict valueForKey:@"image"];
        [self.array_frnd addObject:shareObj];
        
    }
    self.tbl_friend.hidden =NO;
    self.view_find_friend.hidden=YES;
    [self.tbl_friend reloadData];
    
}

-(void)FailgetFacebookUsersResponce:(NSNotification *)notification {
    //  [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14211" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14211" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	
    return 60.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_frnd.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Faceabook_find_friend_cell";
	Faceabook_find_friend_cell *cell = (Faceabook_find_friend_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Faceabook_find_friend_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.btn_follow addTarget:self action:@selector(btn_follow_click:) forControlEvents:UIControlEventTouchUpInside];
        cell.btn_follow.layer.cornerRadius = 5;
        [self addGradient:cell.btn_follow];
		
	}
   
    Faceabook_find_friend_Share *shareObj =[self.array_frnd objectAtIndex:indexPath.row];
    cell.lbl_name.text = shareObj.name;
    cell.lbl_username.text =shareObj.username;
    
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.url_image] placeholderImage:nil];
   // cell.img_user.imageURL =[NSURL URLWithString:shareObj.url_image];
    cell.btn_follow.tag =indexPath.row;
	return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark Follow Click
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    NSLog(@"%@",[btn currentTitle]);
    if ([[btn currentTitle] isEqualToString:@"Follow"]) {
        [btn setTitle:@"Following" forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor greenColor]];
        [btn.layer setBackgroundColor:[UIColor greenColor].CGColor];
    }else{
        [btn setTitle:@"Follow" forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor colorWithRed:74.0f/255.0f green:126.0f/255.0f blue:190.0f/255.0f alpha:1]];
        [btn.layer setBackgroundColor:[UIColor colorWithRed:74.0f/255.0f green:126.0f/255.0f blue:190.0f/255.0f alpha:1].CGColor];
    }
    
    [self addGradient:btn];
    
    NSInteger tag =btn.tag;
    
    Faceabook_find_friend_Share *shareObj =[self.array_frnd objectAtIndex:tag];
    //post user follow
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&following_id=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],shareObj.user_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_user_following.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"10":nil];
    
}

-(void)getFollowResponce:(NSNotification *)notification {
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    //akshay
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }

}
-(void)FailReson:(NSNotification *)notification {
	//[SVProgressHUD dismiss];
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Warning!" message:@"There was a problem with the Internet connection. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

-(void) addGradient:(UIButton *) _button {
    
    // Add Border
    
    CALayer *layer = _button.layer;
   // [layer removeFromSuperlayer];
   // layer.backgroundColor =[UIColor redColor].CGColor;
    layer.cornerRadius = 5.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = [UIColor colorWithWhite:0.5f alpha:0.2f].CGColor;
    
    // Add Shine
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.8f],
                            [NSNumber numberWithFloat:1.0f],
                            nil];
    [layer addSublayer:shineLayer];
 //   [_button.layer addSublayer:layer];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
