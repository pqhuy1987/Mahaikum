//
//  Reset_password_from_username_email.h
//  My Style
//
//  Created by Tis Macmini on 6/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "Reset_password_throw_email.h"

@interface Reset_password_from_username_email : UIViewController{
    UIImageView *img_cell_bg;
    UITextField *txt_username;
    Reset_password_throw_email *thro_email_viewObj;
}

@property(nonatomic,strong)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)Reset_password_throw_email *thro_email_viewObj;

@end
