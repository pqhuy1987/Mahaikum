//
//  Followers_list_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Followers_list_cell : UITableViewCell{

    UIImageView *img_user;
    UILabel *lbl_username;
    UILabel *lbl_name;
    UIButton *btn_follow;
}
@property(nonatomic,strong)IBOutlet UIImageView *img_user;
@property(nonatomic,strong)IBOutlet UILabel *lbl_username;
@property(nonatomic,strong)IBOutlet UILabel *lbl_name;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow;
@end
