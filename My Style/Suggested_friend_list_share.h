//
//  Suggested_friend_list_share.h
//  My Style
//
//  Created by Tis Macmini on 6/21/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Suggested_friend_list_share : NSObject{

    NSString *bio;
    NSArray *image_data;
    NSString *total_img;
    NSString *user_img;
    NSString *username;
    NSString *name;
    NSString *is_follow;
    NSString *user_id;
    NSString *is_private;
    NSString *is_user_friend;
}
@property(nonatomic,strong)  NSString *user_id;
@property(nonatomic,strong) NSString *bio;
@property(nonatomic,strong) NSArray *image_data;
@property(nonatomic,strong) NSString *total_img;
@property(nonatomic,strong) NSString *user_img;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong)  NSString *is_follow;
@property(nonatomic,strong)  NSString *is_private;
@property(nonatomic,strong)  NSString *is_user_friend;
@end
