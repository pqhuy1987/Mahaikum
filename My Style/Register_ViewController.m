//
//  Register_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/9/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Register_ViewController.h"
#import "AppDelegate.h"
#import "AJNotificationView.h"
#import "TWPhotoPickerController.h"
#import "TGCameraViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UIView+Layout.h"

@interface Register_ViewController () <FindLocationViewControllerDelegate, GMSMapViewDelegate>
{
    FindLocationViewController *findLocationView;
    CLLocationCoordinate2D backedLocation;
    GMSMapView  *googlemapview;
    NSInteger btnswitchgenderflg;
}
@end

@implementation Register_ViewController
@synthesize btn_privacy_policy,btn_terms_of_service,btn_add_photo;
@synthesize photoPickerController;
@synthesize image_url;
@synthesize txt_username,txt_password,txt_email,txt_name,txt_phone_no;
@synthesize view_error;
@synthesize btn_facebook_info,scrollview;
@synthesize img_first_cell_bg,img_second_cell_bg,webviewObj;
@synthesize lblSpinnerBg,activity;
@synthesize txt_CountryCode;
@synthesize txt_weburl,txt_bio;
@synthesize imgswitchgenderfemale,imgswitchgendermale,lblgender;
@synthesize btn_gender;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    self.img_first_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_first_cell_bg.layer.cornerRadius =4.0f;
    
    self.img_second_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_second_cell_bg.layer.cornerRadius =4.0f;
    
    lblgender.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    txt_weburl.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    txt_bio.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    
    btnswitchgenderflg = 0;
    [self btnswitchgenderClick:self];

    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(4.5, 11.0, txt_bio.frame.size.width - 20.0, 14.0)];
    [placeholderLabel setText:@"Bio"];
    // placeholderLabel is instance variable retained by view controller
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    [placeholderLabel setTextColor:[UIColor lightGrayColor]];
    placeholderLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
  
    [txt_bio addSubview:placeholderLabel];
    
    self.switch_gender.onText = NSLocalizedString(@"Male", @"");
    self.switch_gender.offText = NSLocalizedString(@"Female", @"");
    self.switch_gender.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    

    self.webviewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    if (!appDelegate.session.isOpen) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
        
        if (appDelegate.session.state == FBSessionStateCreatedTokenLoaded) {
            [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
            }];
        }
    }
    
    twitter_login=[[Twitter_login_ViewController alloc]initWithNibName:@"Twitter_login_ViewController" bundle:nil];
    
    if (is_iPhone_5) {
        self.scrollview.contentSize=CGSizeMake(kViewWidth, kViewHeight+100);
        self.scrollview.frame = CGRectMake(0, 0, kViewWidth, kViewHeight);
    }
    else{
        self.scrollview.contentSize=CGSizeMake(kViewWidth, kViewHeight+100);
        self.scrollview.frame = CGRectMake(0, 0, kViewWidth, kViewHeight);
    }
    
    //Notification for get Image from Facebook
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"register_add_photo" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_photo_facebok:) name:@"register_add_photo" object:nil];

    //Notification for get info from Facebook
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"register_use_fb_info" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_facebok:) name:@"register_use_fb_info" object:nil];
    
    self.btn_privacy_policy.underlinePosition=2.0;
    self.btn_terms_of_service.underlinePosition=2.0;
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.btn_add_photo.imageView.contentMode =UIViewContentModeScaleAspectFit;

    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    backedLocation = delegate.locationManager.location.coordinate;
    [self selectGooglemapLocation:backedLocation name:@"Current Location"];
}

-(void)enableKeyboardManger:(UIBarButtonItem*)barButton
{
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

-(void)disableKeyboardManager:(UIBarButtonItem*)barButton
{
    [[IQKeyboardManager sharedManager] setEnable:NO];
}

-(IBAction)btn_back_click:(id)sender {
    [self.view endEditing:YES];
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    self.txt_username.text = @"";
    self.txt_password.text = @"";
    self.txt_email.text = @"";
    self.txt_name.text = @"";
    self.txt_phone_no.text = @"";
    [self.btn_facebook_info setImage:[UIImage imageNamed:@"facebookbtn.png"] forState:UIControlStateNormal];
    [self.btn_add_photo setImage:[UIImage imageNamed:@"btn_add_photo.png"] forState:UIControlStateNormal];
    [StaticClass saveToUserDefaults:@"" :FACEBOOK_ID];
    
    AppDelegate *appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate.session closeAndClearTokenInformation];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma viewWillAppear
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    if (is_iPhone_5) {
        self.scrollview.contentSize=CGSizeMake(kViewWidth, kViewHeight+100);
        self.scrollview.frame = CGRectMake(0, 0, kViewWidth, kViewHeight);
    }
    else{
        self.scrollview.contentSize=CGSizeMake(kViewWidth, kViewHeight+400);
        self.view.frame = CGRectMake(0, 0, kViewWidth, kViewHeight);
        self.scrollview.frame = CGRectMake(0, 0, kViewWidth, kViewHeight);
    }
    
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    NSLog(@"%@",NSStringFromCGRect(self.scrollview.frame));
    
    btn_add_photo.layer.cornerRadius =40.0f;
    btn_add_photo.layer.masksToBounds =YES;
    btn_add_photo.clipsToBounds=YES;

    if (isiPhone) {
        txt_username.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_password.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_email.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_name.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
        txt_phone_no.font = [UIFont fontWithName:@"Century Gothic" size:12.0];
    } else {
        txt_username.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_password.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_email.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_name.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
        txt_phone_no.font = [UIFont fontWithName:@"Century Gothic" size:24.0];
    }
}

#pragma mark Add Photo Click
-(IBAction)btn_add_photo_click:(id)sender{
    UIActionSheet *sheet =[[UIActionSheet alloc]init];
    [sheet setTitle:@"Change Profile Picture"];
    [sheet addButtonWithTitle:@"Import from Facebook"];
    [sheet addButtonWithTitle:@"Import from Twitter"];
    
//    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
	{
        [sheet addButtonWithTitle:@"Take Photo"];
    }
//    [sheet addButtonWithTitle:@"Choose from Library"];
    sheet.cancelButtonIndex = [sheet addButtonWithTitle: @"Cancel"];
    sheet.delegate=self;
    [sheet showInView:self.view];
    
}

#pragma mark ActionSheet Delegate 

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
        {
            NSLog(@"Import from Facebook");
            [self get_user_photo_facebok];
            break;
        }
        case 1:
        {
            NSLog(@"Import from Twitter");
            [self getImage_from_twitter];
            break;
        }
        case 2:
        {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                NSLog(@"Take Photo");
                TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
                [self presentViewController:navigationController animated:YES completion:nil];

//                photoPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
//                photoPickerController.allowsEditing = YES;
//                [self presentViewController:photoPickerController animated:YES completion:nil];
                
            }else{
                NSLog(@"Choose from Library");
//                photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//                [self presentViewController:photoPickerController animated:YES completion:nil];
                
                TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
                
                photoPicker.cropBlock = ^(UIImage *image) {
                    //do something
                    if (image == nil) return;
                    self.btn_add_photo.imageView.image=image;
                };
                
                UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
                [navCon setNavigationBarHidden:YES];
                
                [self presentViewController:navCon animated:YES completion:NULL];

            }
            break;
        }
//        case 3:
//        {
//            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//            {
//                NSLog(@"Choose from Library");
////                photoPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
////                [self presentViewController:photoPickerController animated:YES completion:nil];
//
//                TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
//                
//                photoPicker.cropBlock = ^(UIImage *image) {
//                    //do something
//                    if (image == nil) return;
//                    self.btn_add_photo.imageView.image=image;
//                };
//                
//                UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
//                [navCon setNavigationBarHidden:YES];
//                
//                [self presentViewController:navCon animated:YES completion:NULL];
//            }else{
//                NSLog(@"Cancel");
//            }
//            
//            break;
//        }
        case 3:
        {
            NSLog(@"Cancel");

            break;
        }
        default:
            break;
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:nil];
	UIImage *baseImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
	if (baseImage == nil) return;
    self.btn_add_photo.imageView.image=baseImage;
}

#pragma mark Use Your Facebook Info
-(IBAction)btn_use_fb_info_click:(id)sender{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_info_Me];
    }];
}

#pragma Mark Facebook Add Photo
-(void)get_user_info_facebok:(NSNotification *)notification {
    [self apiFQLI_info_Me];
}

-(void)apiFQLI_info_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
   // NSString *query = @"SELECT  pic FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {
        [self info_requestFacebookUserInfoCompleted:connection result:result error:error];
    };
    [requester addRequest:request completionHandler:handler];
    [requester start];
}

- (void)info_requestFacebookUserInfoCompleted:(FBRequestConnection *)connection result:(id)result error:(NSError *)error {
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    } else {
        
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];
        
        NSLog(@"%@",dictionary);
        NSLog(@"%@",NSStringFromClass([[dictionary objectForKey:@"data"] class]));
        for (NSString *str in [tempResult valueForKey:@"name"]) {
            self.txt_name.text =str;
        }
        
        for (NSString *str in [tempResult valueForKey:@"email"]) {
            self.txt_email.text =str;
        }
        
        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        for (NSString *str in [tempResult valueForKey:@"pic"]) {
            NSLog(@"str:%@",str);
            NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
            self.btn_add_photo.imageURL =[NSURL URLWithString:imgU];
            
        }
        
        [self.btn_facebook_info setImage:[UIImage imageNamed:@"facebookbtn_selected.png"] forState:UIControlStateNormal];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        });
    }
}


#pragma mark Done Button Click

-(IBAction)btn_done_click:(id)sender{
    if (self.txt_username.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a username." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    if (self.txt_name.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a name." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    if (self.txt_password.text.length==0) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please create a password." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    
    if (self.txt_password.text.length<6) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Password must be at least 6 characters." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    if (![StaticClass validateEmail:self.txt_email.text]) {
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please enter a valid email address." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        return;
    }
    
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Is this correct?" message:[NSString stringWithFormat:@"You entered your email as :\n%@",self.txt_email.text] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    alert.tag=1;
    [alert show];

}

-(void)register_call {
    [SVProgressHUD show];
    
    UIImage *newImage = [self.btn_add_photo.imageView.image resizedImage:CGSizeMake(200,200) interpolationQuality:3];
    NSData *img_data = UIImageJPEGRepresentation(newImage,1);
    
    
    //NSData *img_data  = UIImageJPEGRepresentation(self.btn_add_photo.imageView.image,1);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSString *dateString1 = [GlobalDefine toDateTimeStringFromDateWithFormat:[NSDate date] formatString:@"yyyy-MM-dd hh:mm:ss"];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];

        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        NSLog(@"KEY:%@",imageKeys);
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(putObjectResponse.error != nil) {
                NSLog(@"Error: %@", putObjectResponse.error);
                [SVProgressHUD dismiss];
                [self registerAPICall:imageKeys];
            }
            else {
                [self registerAPICall:imageKeys];
            }
        });
    });
}

-(void)registerAPICall:(NSString *)ky {
    NSString *imageKeys=ky;
    NSLog(@"KEY:%@",imageKeys);
    NSString *salt =@"123";// [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRegisterAPIResponce:) name:@"14223" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetRegisterAPIResponce:) name:@"-14223" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_register.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSString *fbIds=[[NSUserDefaults standardUserDefaults] objectForKey:FACEBOOK_ID];
    if ([fbIds isEqualToString:@""]||fbIds==nil) {
        fbIds=@"";
    }
    NSString *deviceIds=[StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"];
    if ([deviceIds isEqualToString:@""]||deviceIds==nil) {
        deviceIds=@"";
    }
    
    NSString *latitude = [NSString stringWithFormat:@"%f", backedLocation.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", backedLocation.longitude];

    NSString *strGenderString=@"";
    if ([lblgender.text isEqualToString:@"Male"]) {
        strGenderString=@"m";
    }
    else if([lblgender.text isEqualToString:@"Female"]) {
        strGenderString=@"f";
    }
    else {
        strGenderString=@"-";
    }
    

    NSDictionary *params = @{
                            @"sign":sig,
                            @"salt":salt,
                            @"username":self.txt_username.text,
                            @"password":self.txt_password.text,
                            @"facebook_id":fbIds,
                            @"name":self.txt_name.text,
                            @"email":self.txt_email.text,
                            @"phone":self.txt_phone_no.text,
                            @"image":imageKeys,
                            @"device_token":deviceIds,
                            @"country_code":self.txt_CountryCode.text,
                            @"gender": strGenderString,
                            @"weburl": self.txt_weburl.text,
                            @"bio": self.txt_bio.text,
                            @"facebook_url": self.txt_facebook_url.text,
                            @"twitter_url": self.txt_twitter_url.text,
                            @"pinterest_url": self.txt_pinterest_url.text,
                            @"google_url": [StaticClass urlEncoding:self.txt_google_url.text],
                            @"instagram_url": self.txt_instagram_url.text,
                            @"youtube_url": self.txt_youtube_url.text,
                            @"latitude": latitude,
                            @"longitude": longitude,
                            @"delivery_time":self.txt_delivery_time.text,
                            @"working_hours":self.txt_working_hours.text,
                            @"shipping_fee":self.txt_shipping_fee.text};
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14223" :params];
}

-(void)getRegisterAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    NSString *strMessage=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMessage:%@",strMessage);
    
    
    NSString *strResult=[result valueForKey:@"success"];
    
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"Register result value." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
    
    NSLog(@"Register result value: %@, %@, %@", result, strResult, [result valueForKey:@"success"]);
    
    if ([strResult isEqualToString:@"false"]) {
        NSString *msg=[result valueForKey:@"message"];
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Warning !" subtitle:msg hideAfter:2];
    }
    else {
        NSDictionary *dict =[result valueForKey:@"data"];
        
        [StaticClass saveToUserDefaults:[dict valueForKey:@"uid"] :USER_ID];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"username"]] :USERNAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"name"]] :USER_NAME];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"image"]] :USER_IMAGE];
        
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"username"]] :@"STOREUSERNAME"];
        [StaticClass saveToUserDefaults:self.txt_password.text :@"STOREPASSWORD"];
        [StaticClass saveToUserDefaults:[StaticClass urlDecode:[dict valueForKey:@"email"]] :@"STOREEMAILID"];
        [StaticClass saveToUserDefaults:[dict valueForKey:@"facebook_id"] :@"STOREFACEBOOKID"];
        [StaticClass saveToUserDefaults:@"1" :USER_IS_LOGIN];
        
        Facebook_Find_Friends_ViewController *tempObj =[[Facebook_Find_Friends_ViewController alloc]initWithNibName:@"Facebook_Find_Friends_ViewController" bundle:nil];
        [self.navigationController pushViewController:tempObj animated:YES];
    }
}

-(void)FailgetRegisterAPIResponce :(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14223" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14223" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@"Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
}

#pragma mark Facebook Add Photo
-(void)get_user_photo_facebok{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.session.state != FBSessionStateCreated) {
        
        appDelegate.session = [[FBSession alloc] initWithPermissions:[NSArray arrayWithObjects:@"read_stream",@"publish_stream",@"email", nil]];
    }
    
    [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                     FBSessionState status,
                                                     NSError *error) {
        // and here we make sure to update our UX according to the new session state
        //  [self updateView];
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@",
                      appDelegate.session.accessTokenData.accessToken]);
        NSLog(@"%@",appDelegate.session.appID);
        [StaticClass saveToUserDefaults:appDelegate.session.accessTokenData.accessToken :FACEBOOK_ACCESS_TOKEN];
        [self apiFQLI_pic_Me];
    }];
}
-(void)apiFQLI_pic_Me {
    NSString *query = @"SELECT uid, name, pic, email,sex FROM user WHERE uid=me()";
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    FBRequestConnection *requester = [[FBRequestConnection alloc] init];
    
    FBRequest *request =  [FBRequest requestWithGraphPath:[NSString stringWithFormat:@"/fql/?access_token=%@",[[NSUserDefaults standardUserDefaults]objectForKey:FACEBOOK_ACCESS_TOKEN ]] parameters:queryParam HTTPMethod:@"GET"];
    
    FBRequestHandler handler = ^(FBRequestConnection *connection, id result, NSError *error) {

        [self requestFacebookUserInfoCompleted:connection result:result error:error];
    };
    [requester addRequest:request completionHandler:handler];
    [requester start];
       
}

-(void)requestFacebookUserInfoCompleted:(FBRequestConnection *)connection result:(id)result error:(NSError *)error {
    
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    else {
        FBGraphObject *dictionary = (FBGraphObject *)result;
        NSArray *tempResult=[dictionary objectForKey:@"data"];

        NSString *facebookUID=@"";
        for (NSString *str in [tempResult valueForKey:@"uid"]) {
            facebookUID=str;
            [StaticClass saveToUserDefaults:str :FACEBOOK_ID];
        }
        
        NSString *imgU=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",facebookUID];
        
        self.image_url=imgU;
        self.btn_add_photo.imageURL =[NSURL URLWithString:self.image_url];
    }
}

-(void)set_btn_add_photo_image {
    self.btn_add_photo.imageURL =[NSURL URLWithString:self.image_url];
}

#pragma mark Photo From Twitter
-(void)getImage_from_twitter {
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
        if (granted) {
        
        } else {

        }
        if (error) {
             NSLog(@"%@",[error debugDescription]);
            [self performSelector:@selector(go_to_login_twitter) withObject:self afterDelay:0.1];
        }
    }];
}

-(void)go_to_login_twitter {
    [self.navigationController pushViewController:twitter_login animated:YES];
}

#pragma mark KeyBoard Done & Calcel Method
-(void)doneWithKeyboard {
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    [self.view endEditing:YES];
}

-(void)cancelKeyboard {
    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
    NSLog(@"%@",NSStringFromCGRect(self.scrollview.frame));
    [self.view endEditing:YES];
}

#pragma mark UITextView Delegate
-(void) textViewDidChange:(UITextView *)textView
{
    if(![txt_bio hasText]) {
        [txt_bio addSubview:placeholderLabel];
    } else if ([[txt_bio subviews] containsObject:placeholderLabel]) {
        [placeholderLabel removeFromSuperview];
    }
}
- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![txt_bio hasText]) {
        [txt_bio addSubview:placeholderLabel];
    }
}

#pragma mark UITextfield Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField==txt_username) {
         self.scrollview.contentOffset=CGPointMake(0,0);
    }else if(textField==txt_password){
        self.scrollview.contentOffset=CGPointMake(0,10);
        
    }else if(textField==txt_email){
        if (is_iPhone_5) {
            self.scrollview.contentOffset=CGPointMake(0,50);
        }else{
            self.scrollview.contentOffset=CGPointMake(0,50+88);
        }
    }else if(textField==txt_name){
        if (is_iPhone_5) {
            self.scrollview.contentOffset=CGPointMake(0,90);
        }else{
            self.scrollview.contentOffset=CGPointMake(0,90+88);

        }
    }else if(textField==txt_phone_no){
        if (is_iPhone_5) {
            self.scrollview.contentOffset=CGPointMake(0, 100);
        }else{
            self.scrollview.contentOffset=CGPointMake(0, 100+88);
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField==txt_username) {
        
        [txt_password becomeFirstResponder];
        
    }else if(textField==txt_password){
        
        [txt_email becomeFirstResponder];
        
    }else if(textField==txt_email){
        
        [txt_name becomeFirstResponder];
        
    }else if(textField==txt_name){
        
        [txt_phone_no becomeFirstResponder];
        
    }else if(textField==txt_phone_no){
        
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField==self.txt_username || textField==self.txt_email) {
        if ([string isEqualToString:@" "]) {
            return NO;
        }
    }
    NSString *fullString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([textField isEqual:self.txt_facebook_url] && ![fullString hasPrefix:@"www.facebook.com/"])
    {
        if (fullString.length < @"www.facebook.com/".length)
            textField.text = @"www.facebook.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_twitter_url] && ![fullString hasPrefix:@"www.twitter.com/"])
    {
        if (fullString.length < @"www.twitter.com/".length)
            textField.text = @"www.twitter.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_pinterest_url] && ![fullString hasPrefix:@"www.pinterest.com/"])
    {
        if (fullString.length < @"www.pinterest.com/".length)
            textField.text = @"www.pinterest.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_google_url] && ![fullString hasPrefix:@"plus.google.com/"])
    {
        if (fullString.length < @"plus.google.com/".length)
            textField.text = @"plus.google.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_instagram_url] && ![fullString hasPrefix:@"www.instagram.com/"])
    {
        if (fullString.length < @"www.instagram.com/".length)
            textField.text = @"www.instagram.com/";
        return NO;
    }
    
    if ([textField isEqual:self.txt_youtube_url] && ![fullString hasPrefix:@"www.youtube.com/"])
    {
        if (fullString.length < @"www.youtube.com/".length)
            textField.text = @"www.youtube.com/";
        return NO;
    }
    return YES;
}

#pragma mark UIAlert Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==1) {
        if (buttonIndex==1) {
            [self register_call];
        }
    }
}

-(IBAction)btn_privacy_policy_click:(id)sender{
    self.webviewObj.web_url =[[Singleton sharedSingleton]get_privacy_police_url];
    [self.navigationController pushViewController:self.webviewObj animated:YES];
}

-(IBAction)btn_terms_of_service_click:(id)sender{
    self.webviewObj.web_url =[[Singleton sharedSingleton]get_terms_of_use_url];
    [self.navigationController pushViewController:self.webviewObj animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *baseImage = image;
    
    if (baseImage == nil) return;
    self.btn_add_photo.imageView.image=baseImage;
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *baseImage = image;
    
    if (baseImage == nil) return;
    self.btn_add_photo.imageView.image=baseImage;
}

#pragma mark -
#pragma mark - TGCameraDelegate optional

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidSavePhotoAtPath:(NSURL *)assetURL
{
    NSLog(@"%s album path: %@", __PRETTY_FUNCTION__, assetURL);
}

- (void)cameraDidSavePhotoWithError:(NSError *)error
{
    NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
}

- (IBAction)chooseLocationClicked:(id)sender
{
    findLocationView =[[FindLocationViewController alloc]initWithNibName:@"FindLocationViewController" bundle:nil];
    findLocationView.delegate = self;
    [self.navigationController pushViewController:findLocationView animated:YES];
}

- (void)selectGooglemapLocation:(CLLocationCoordinate2D)location name:(NSString *)locationname
{
    if ( googlemapview == nil )
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:location zoom:13];
        GMSMapView *view = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
        //    view.myLocationEnabled = YES;
        view.delegate = self;
        [self.mapView removeSubviews];
        [self.mapView addSubview:view];
        GMSMarker   *marker = [GMSMarker markerWithPosition:location];
        marker.title = locationname;
        marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
        marker.map = view;
        marker.userData = marker;
        googlemapview = view;
    }
    else
    {
        [googlemapview clear];
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:location zoom:13];
        
        googlemapview.camera = camera;
        
        GMSMarker   *marker = [GMSMarker markerWithPosition:location];
        marker.title = locationname;
        marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
        marker.map = googlemapview;
        marker.userData = marker;
    }
}

- (void)mapItemSelected:(FindLocationViewController *)locationViewController mapItem:(MKMapItem *)mapItem selectedLocationKind:(NSString *)selectedLocationKind
{
    locationViewController.delegate = nil;
    backedLocation = CLLocationCoordinate2DMake([(CLCircularRegion *)mapItem.placemark.region center].latitude, [(CLCircularRegion *)mapItem.placemark.region center].longitude);
    NSString *locationName = mapItem.placemark.name;
    //    formattedAddress = [mapItem.placemark.addressDictionary[@"FormattedAddressLines"] componentsJoinedByString:@", "];
    [self.navigationController popViewControllerAnimated:YES];
    
    //    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
    //    MKCoordinateRegion regionToDisplay = MKCoordinateRegionMake(backedLocation, span);
    //    [self.mapView setRegion:regionToDisplay];
    //
    //    MyAnnotation *ann = [[MyAnnotation alloc] init];
    //    ann.title = locationName;
    //    ann.coordinate = backedLocation;
    //    [self.mapView addAnnotation:ann];
    
    [self selectGooglemapLocation:backedLocation name:locationName];
}

- (void)mapItemSelected:(FindLocationViewController *)locationViewController place:(GMSPlace *)place selectedLocationKind:(NSString *)selectedLocationKind
{
    locationViewController.delegate = nil;
    backedLocation = place.coordinate;
    NSString *locationName = place.name;
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [self selectGooglemapLocation:backedLocation name:locationName];
}

#pragma mark Google Map Delegate
- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidStartTileRendering");
}

- (void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidFinishTileRendering");
}

#pragma mark - Gender selected
-(IBAction)btnswitchgenderClick:(id)sender{
//    if (btnswitchgenderflg == 0) {
//        btnswitchgenderflg = 1;
//        lblgender.text = @"Male";
//        imgswitchgendermale.hidden = NO;
//        imgswitchgenderfemale.hidden = YES;
//    }
//    else{
//        btnswitchgenderflg = 0;
//        lblgender.text = @"Female";
//        imgswitchgendermale.hidden = YES;
//        imgswitchgenderfemale.hidden = NO;
//    }
}

@end
