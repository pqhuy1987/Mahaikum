//
//  ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize img_bg,img_logo;

-(void)viewDidLoad {
    [super viewDidLoad];
    
    sign_in_ViewObj=[[Sign_ViewController alloc]initWithNibName:@"Sign_ViewController" bundle:nil];
    if (is_iPhone_5) {
        self.img_logo.frame=CGRectMake(22,568,img_logo.frame.size.width, img_logo.frame.size.height);
    }
    else {
        self.img_logo.frame=CGRectMake(22,980,img_logo.frame.size.width, img_logo.frame.size.height);
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [UIView animateWithDuration:1.5 animations:^{
        if (is_iPhone_5) {
            self.img_logo.frame=CGRectMake(22,171+44, img_logo.frame.size.width, img_logo.frame.size.height);
        }else{
            self.img_logo.frame=CGRectMake(22,171, img_logo.frame.size.width, img_logo.frame.size.height);
        }
    } completion:^ (BOOL finished) {
         [self.navigationController pushViewController:sign_in_ViewObj animated:YES];
     }];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
