//
//  PreviewViewController.h
//  STPopupPreviewExample
//
//  Created by Kevin Lin on 22/5/16.
//  Copyright © 2016 Sth4Me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_data_share.h"

@interface PreviewViewController : UIViewController

@property (nonatomic, strong) UIImage *avatarImage;
@property (nonatomic, strong) UIImage *showImage;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) Home_tableview_data_share *data;

@end
