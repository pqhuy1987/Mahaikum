//
//  webview_viewcontroller.h
//  My Style
//
//  Created by Tis Macmini on 5/22/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DejalActivityView.h"
#import "BlockActionSheet.h"

@interface webview_viewcontroller : UIViewController{

    NSString *web_url;
    UIWebView *webview;
}
@property(nonatomic,strong) NSString *web_url;
@property(nonatomic,strong)IBOutlet  UIWebView *webview;

@end
