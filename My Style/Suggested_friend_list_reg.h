//
//  Suggested_friend_list_reg.h
//  My Style
//
//  Created by Tis Macmini on 6/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Suggested_friend_list_cell.h"
#import "image_collection_cell.h"
#import "Suggested_friend_list_share.h"

@interface Suggested_friend_list_reg : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>{
    
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
}
@property(nonatomic,strong)NSMutableArray *array_friend;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_friend;

@end
