//
//  Setting_profile.h
//  My Style
//
//  Created by Tis Macmini on 5/22/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "webview_viewcontroller.h"
#import "Notification_setting_ViewController.h"
#import "Photos_you_liked.h"
#import "DCRoundSwitch.h"
#import "Find_invite_friend.h"
@interface Setting_profile : UIViewController{

    UIImageView *img_firstcell;
    UIImageView *img_secondcell;
    UIImageView *img_thirdcell;
    UIImageView *img_fourthcell;
    UILabel *lblheader,*lblprivatephoto,*lbloriginalphoto,*lblinformation1,*lblinformation2,*lblinformation3,*lblsubheader1,*lblsubheader2,*lblsubheader3;
    UIImageView *imgswitchprivateon,*imgswitchprivateoff;
    UIImageView *imgswitchphotoson,*imgswitchphotosoff;
    UIScrollView *scrollview;
    UIButton *btnfindfriends,*btnphotoliked,*btnnotification,*btntos;

    Notification_setting_ViewController *notification_settingViewObj;
    int is_private_flag;
    
    Find_invite_friend *find_inviteViewObj;
}
@property(nonatomic,strong) Find_invite_friend *find_inviteViewObj;
@property(nonatomic,strong) Notification_setting_ViewController *notification_settingViewObj;
@property(nonatomic,strong)IBOutlet UIButton *btnfindfriends,*btnphotoliked,*btnnotification,*btntos;
@property(nonatomic,strong)IBOutlet UIImageView *img_firstcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_secondcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_thirdcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_fourthcell;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchprivateon,*imgswitchprivateoff;
@property(nonatomic,strong)IBOutlet UIImageView *imgswitchphotoson,*imgswitchphotosoff;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblprivatephoto,*lbloriginalphoto,*lblinformation,*lblinformation2,*lblinformation3,*lblsubheader1,*lblsubheader2,*lblsubheader3;
@property (nonatomic, strong) IBOutlet DCRoundSwitch *switch_private;
@property (nonatomic,strong)  IBOutlet DCRoundSwitch *swith_photos_save;

-(void)setPrivatePhotoFlg ;

-(IBAction)switchprivatebtnClick:(id)sender;
-(IBAction)switchphotosbtnClick:(id)sender;
@end
