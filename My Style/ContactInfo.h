

#import <Foundation/Foundation.h>

@interface ContactInfo : NSObject {
    NSString *firstName;
    NSString *lastName;
    NSString *emailIds;
    NSString *phoneNo;
    NSData *imageData;
    BOOL selected;
}

@property(nonatomic,strong) NSString *firstName;
@property(nonatomic,strong) NSString *lastName;
@property(nonatomic,strong) NSString *emailIds;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, strong) NSData *imageData;

@end
