//
//  Forgot_password_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reset_password_from_username_email.h"
#import "Rest_password_throw_fb.h"
#import "DejalActivityView.h"

@interface Forgot_password_ViewController : UIViewController{

    UIImageView *img_cell_bg;
    
    UIButton *btnUserName, *btnFacebook;
    
   // Reset_password_from_username_email *reset_email_viewObj;
   // Rest_password_throw_fb *reset_fb_viewObj;
}
@property(nonatomic,strong)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,strong)Reset_password_from_username_email *reset_email_viewObj;
@property(nonatomic,strong)Rest_password_throw_fb *reset_fb_viewObj;
@property(nonatomic,strong) IBOutlet UIButton *btnUserName, *btnFacebook;
@end
