//
//  STXFeedViewController.m
//
//  Created by Jesse Armand on 20/1/14.
//  Copyright (c) 2014 2359 Media. All rights reserved.
//

#import "STXFeedViewController.h"
#import "AFNetworkingQueue.h"
//#import "STXPost.h"

#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "Home_tableview_data_share.h"
#import "Comment_share.h"

#define PHOTO_CELL_ROW 0

static NSInteger which_image_delete=0;
static NSInteger which_image_liked=0;
static NSInteger tblRefresOffset=0;
static NSInteger cellClickIndex=0;

@interface STXFeedViewController () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation STXFeedViewController
@synthesize comments_list_viewObj,array_feeds;
@synthesize share_photo_view,liker_list_viewObj;
@synthesize btn_reload;

-(void)viewDidLoad {
    [super viewDidLoad];
    
   // self.title = NSLocalizedString(@"Feed", nil);
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tableView];
    self.tableView.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tableView.delegate = delegate;
    self.tableViewDelegate = delegate;
   
    //self.activityIndicatorView = [self activityIndicatorViewOnView:self.view];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    array_feeds =[[NSMutableArray alloc] init];
    
    UIView *tableview_header =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIImageView *img_header_bg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [img_header_bg setImage:[UIImage imageNamed:@"header.png"]];
    
    btn_reload =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_reload setImage:[UIImage imageNamed:@"refreshbtn.png"] forState:UIControlStateNormal];
    [btn_reload addTarget:self action:@selector(btn_refresh_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_reload setFrame:CGRectMake(274, 10, 26, 26)];
    
    [tableview_header addSubview:img_header_bg];
    [tableview_header addSubview:btn_reload];
    self.tableView.tableHeaderView =tableview_header;
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    if (is_iPhone_5) {
        self.tableView.frame = CGRectMake(0, 20, 320,517-20);
    }
    else{
        self.tableView.frame = CGRectMake(0, 20, 320, 380+51-20);
    }
    [self get_news_feed];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.tableViewDataSource.posts count] == 0) {
        [self.activityIndicatorView startAnimating];
    }
}

-(IBAction)btn_refresh_click:(id)sender{
    tblRefresOffset = 0;
    [self get_news_feed];
}

-(void)get_news_feed {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1001001" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"1001001" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1001001" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-1001001" object:nil];

//    [self stopSpinner];
//    [self startSpinner];
//    [self start_activity];
//    
//    _btn_reload.enabled = NO;
    //   http://www.techintegrity.in/mystyle/get_image_detail.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=1
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&off=%d",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],tblRefresOffset];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URL:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1001001":nil];
}

-(void)getnewsResponce:(NSNotification *)notification {
//    [self stopSpinner];
//    [self stop_activity];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1001001" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1001001" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        NSDictionary *array =[result valueForKey:@"data"];
        
        if(tblRefresOffset == 0) {
            [array_feeds removeAllObjects];
        }
        for (id dict in array) {
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[dict valueForKey:@"username"];
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"location"]];
            shareObj.lat=[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[tempdict valueForKey:@"username"]];
                }
            }else{
                // NSLog(@"NSString");
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[tempdict valueForKey:@"username"];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    [shareObj.array_comments addObject:obj];
                    //[obj release];
                }
            }
            else{
                // NSLog(@"NSString");
            }
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            
            if (shareObj.image_path.length !=0) {
                [array_feeds addObject:shareObj];
            }
            //[shareObj release];
        }
    }
    
    tblRefresOffset =[[result valueForKey:@"offset"] integerValue];
    
    self.tableViewDataSource.posts =[array_feeds copy];
    self.tableViewDelegate.posts =[array_feeds copy];
    
    [self.tableView reloadData];
}

-(void)FailNewsReson:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1001001" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1001001" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100103" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100102" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100102" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010103" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010101" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010101" object:nil];
    
//    [self stopSpinner];
//    [self stop_activity];
//    _btn_reload.enabled = YES;
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - User Action Cell
-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObjs=[array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked=userActionCell.tag;
    if (![shareObjs.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObjs.uid imageid:shareObjs.image_id action:@"like"];
    }
}
-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}
-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:userActionCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}
-(void)userWillShare:(STXUserActionCell *)userActionCell {
    //   id<STXPostItem> postItem = userActionCell.postItem;
    
    // NSIndexPath *photoCellIndexPath = [NSIndexPath indexPathForRow:PHOTO_CELL_ROW inSection:userActionCell.indexPath.section];
    // STXFeedPhotoCell *photoCell = (STXFeedPhotoCell *)[self.tableView cellForRowAtIndexPath:photoCellIndexPath];
    //  UIImage *photoImage = photoCell.photoImage;
    
    // [self shareImage:photoImage text:postItem.captionText url:postItem.sharedURL];
    
    int tag =userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"My Style\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        //report_id : 1= i dont like this photo,  2=this photo is spam or a scam,  3=this photo puts people at risk,  4=this photo shouldn't be on mystyle
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"I don't like this photo",@"This photo is spam or a scam",@"This photo puts people at risk",@"This photo shouldn't be on mystyle", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}
-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:commentCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    int tag =likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}


- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
    //pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010101" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"10010101" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010101" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-10010101" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"10010101":nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010101" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010101" object:nil];
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[array_feeds objectAtIndex:which_image_liked];
        
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
        else{
            shareObj.liked=@"no";
            int count =[shareObj.likes intValue];
            count--;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            
            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            }
        }
        [array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
        
        self.tableViewDataSource.posts =[array_feeds copy];
        self.tableViewDelegate.posts =[array_feeds copy];
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height-150) {
        // we are at the end
        [self get_news_feed];
        NSLog(@"Scroll In Table last cell scrolled");
    }
    else {
        NSLog(@"Else part123");
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    // To prevent crash when popping this from navigation controller
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
}

-(void)newRating : (DLStarRatingControl *) control : (NSUInteger)rating {
    NSLog(@"Rating:%d",rating);
    NSLog(@"Control:%d",control.tag);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010103" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"10010103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010103" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-10010103" object:nil];
    
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    cellClickIndex=control.tag;
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%d&item_id=%@&uid=%@",salt,sig,rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URLString:%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"10010103":nil];
    obj.my_rating = [NSString stringWithFormat:@"%d",rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
}

-(void)getRatingResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10010103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10010103" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%d",[[dataDict objectForKey:@"rate"] integerValue]];
        [self.array_feeds replaceObjectAtIndex:cellClickIndex withObject:shareObj];
        
        self.tableViewDataSource.posts =(NSArray *)[array_feeds copy];
        self.tableViewDelegate.posts =(NSArray *)[array_feeds copy];
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag==1) {
        if (buttonIndex!=0) {
            [self delete_photo];
        }
    }
    
    if (alertView.tag==2) {
        [self report_inappropriate:buttonIndex];
    }
}

-(void)report_inappropriate:(int )index {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100103" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInAppropriateResponce:) name:@"100103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100103" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-100103" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"100103":nil];
}

-(void)getReportInAppropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100103" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100103" object:nil];
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"TESTINGSS");
    }
}

-(void)delete_photo {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100102" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"100102" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100102" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-100102" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    
    self.tableViewDataSource.posts = [array_feeds copy];
    self.tableViewDelegate.posts = [array_feeds copy];
    [self.tableView reloadData];
    
    //    [self.tableView beginUpdates];
    //    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:which_image_delete] withRowAnimation:UITableViewRowAnimationFade];
    //    [self.tableView endUpdates];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = @"coco";
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URL:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"100102":nil];
}

-(void)getdeleteResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"100102" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-100102" object:nil];
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"Deleted successfully");
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller           didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            break;
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            break;
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Feed
 -(void)loadFeed {
 NSString *feedPath = [[NSBundle mainBundle] pathForResource:@"instagram_media_popular" ofType:@"json"];
 
 NSError *error;
 NSData *jsonData = [NSData dataWithContentsOfFile:feedPath options:NSDataReadingMappedIfSafe error:&error];
 if (jsonData) {
 NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
 if (error) {
 NSLog(@"%@", error);
 }
 
 NSDictionary *instagramPopularMediaDictionary = [jsonObject objectWithJSONSafeObjects];
 if (instagramPopularMediaDictionary) {
 id data = [instagramPopularMediaDictionary valueForComplexKeyPath:@"data"];
 NSArray *mediaDataArray = [data objectWithJSONSafeObjects];
 
 NSMutableArray *posts = [NSMutableArray array];
 for (NSDictionary *mediaDictionary in mediaDataArray) {
 STXPost *post = [[STXPost alloc] initWithDictionary:mediaDictionary];
 [posts addObject:post];
 }
 
 self.tableViewDataSource.posts = [posts copy];
 
 [self.tableView reloadData];
 
 } else {
 if (error) {
 NSLog(@"%@", error);
 }
 }
 } else {
 if (error) {
 NSLog(@"%@", error);
 }
 }
 
 }
 */

@end