//
//  AddPromotionViewController.h
//  
//
//  Created by Charles Moon on 12/22/15.
//
//

#import <UIKit/UIKit.h>

@protocol AddPromotionViewControllerDelegate;

@interface AddPromotionViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *posterImage;
@property (strong, nonatomic) IBOutlet UITextView *posterDescription;

@property (nonatomic, strong) NSString *user_id;
@property (nonatomic, strong) AmazonS3Client *s3;
@property (nonatomic) BOOL editing;
@property (nonatomic, strong) Home_tableview_data_share *homeData;

@property (nonatomic, strong) id<AddPromotionViewControllerDelegate> delegate;

@end

@protocol AddPromotionViewControllerDelegate <NSObject>

@optional
- (void)shareObjsChangedInPromotion:(AddPromotionViewController *)viewController shareObjs:(Home_tableview_data_share *)shareObjs;

@end