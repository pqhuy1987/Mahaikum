//
//  Contact_list_friend.m
//  My Style
//
//  Created by Tis Macmini on 5/9/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Contact_list_friend.h"

@interface Contact_list_friend ()

@end

@implementation Contact_list_friend

@synthesize array_friend,tbl_friend,array_email;
@synthesize suggested_viewObj;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btn_skip_click:(id)sender{
    [self.navigationController pushViewController:self.suggested_viewObj animated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.array_email =[[NSMutableArray alloc]init];
    
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.suggested_viewObj =[[Suggested_friend_list_reg alloc]initWithNibName:@"Suggested_friend_list_reg" bundle:nil];
    self.array_friend =[[NSMutableArray alloc]init];
}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self getContact];
}

-(void)getContact{
    
    NSArray *array=[self loadContacts];
    
    
    //ttp://www.techintegrity.in/mystyle/post_search_by_email_phone.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&search_type=phone&search_data=19,15,14
    //ttp://www.techintegrity.in/mystyle/post_search_by_email_phone.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=3&search_phone=9427565226,9427565227,9427565228&search_email=hitesh@skyzoan.com,hiren@techintegrity.in,sarju1@techintegrity.in
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14207" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postSearchByEmailPhoneAPIResponce:) name:@"14207" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14207" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostSearchByEmailPhoneAPIResponce:) name:@"-14207" object:nil];
    
    NSMutableString *contact_list=[[NSMutableString alloc] init];
    [contact_list appendString:[array componentsJoinedByString:@","]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_search_by_email_phone.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            @"phone", @"search_type",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            [self.array_email componentsJoinedByString:@","],@"search_email",
                            [NSString stringWithFormat:@"%@",contact_list],@"search_phone"
                            ,nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14207" :params];
}

- (NSArray *)loadContacts
{
    
    [self.array_email removeAllObjects];
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
//    NSMutableArray *array = nil;
    __block BOOL accessGranted = NO;
    
    if (&ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    
    if (accessGranted) {
        
        // NSArray *thePeople = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBook);
        // Do whatever you need with thePeople...
        
    }
    // ABAddressBookRef addressBook = ABAddressBookCreate();
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    NSMutableArray *contacts = [NSMutableArray array];
    
    for (int i = 0 ; i < nPeople ; i++ ) {
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        NSString *fullName;
        
        if (firstName && lastName) {
            fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        } else {
            if (firstName) {
                fullName = [NSString stringWithString:firstName];
            } else if (lastName) {
                fullName = [NSString stringWithString:lastName];
            } else {
                continue;
            }
        }
        
        NSString *phone = nil;
        ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        if (phones) {
            NSArray *phoneAddresses = (NSArray *)CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phones));
            if (phoneAddresses && [phoneAddresses count] > 0)
                phone = [phoneAddresses objectAtIndex:0];
            CFRelease(phones);
        }
        
        if (phone) {
            /*   NSDictionary *contact = [NSDictionary
             dictionaryWithObjectsAndKeys:fullName, @"name",
             phone, @"phone", nil];
             */

            [contacts addObject:[NSString stringWithFormat:@"%@",phone]];
        }
        
        NSString *email = nil;
        ABMultiValueRef emails = ABRecordCopyValue(person, kABPersonEmailProperty);
        if (emails) {
            NSArray *emailsAddresses = (NSArray *)CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(emails));
            if (emailsAddresses && [emailsAddresses count] > 0)
                email = [emailsAddresses objectAtIndex:0];
            CFRelease(emails);
        }
        
        if (email) {

            [self.array_email addObject:[NSString stringWithFormat:@"%@",email]];
        }
        
        if (firstName)
            CFRelease((__bridge CFTypeRef)(firstName));
        if (lastName)
            CFRelease((__bridge CFTypeRef)(lastName));
    }
    
    CFRelease(allPeople);
    CFRelease(addressBook);
    return contacts;
}


-(void)reloadTable{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14207" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postSearchByEmailPhoneAPIResponce:) name:@"14207" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14207" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostSearchByEmailPhoneAPIResponce:) name:@"-14207" object:nil];
    NSMutableString *fb_list=[[NSMutableString alloc] init];
    
    [fb_list appendString:[self.array_friend componentsJoinedByString:@","]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_search_by_email_phone.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSString *strSearchDAta=[NSString stringWithFormat:@"%@",fb_list];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            @"phone", @"search_type",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            strSearchDAta,@"search_data",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14207" :params];
}

-(void)postSearchByEmailPhoneAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14207" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14207" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    [self.array_friend removeAllObjects];
    if([[result valueForKey:@"success"] isEqualToString:@"1"]){
        for(NSString *dict in [result valueForKey:@"data"]){
            Faceabook_find_friend_Share *shareObj =[[Faceabook_find_friend_Share alloc]init];
            shareObj.email =[StaticClass urlDecode:[dict valueForKey:@"email"]];
            shareObj.facebook_id=[dict valueForKey:@"facebook_id"];
            shareObj.user_id=[dict valueForKey:@"id"];
            shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            shareObj.name=[StaticClass urlDecode:[dict valueForKey:@"name"]];
            shareObj.url_image=[StaticClass urlDecode:[dict valueForKey:@"image"]];
            shareObj.is_following=@"no";
            [self.array_friend addObject:shareObj];
            
        }}
    else{
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Data don't found" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    self.tbl_friend.hidden =NO;
    
    [self.tbl_friend reloadData];
    [self stop_activity];
}

-(void)FailpostSearchByEmailPhoneAPIResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14207" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14207" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_friend.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Faceabook_find_friend_cell";
	Faceabook_find_friend_cell *cell = (Faceabook_find_friend_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Faceabook_find_friend_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.btn_follow addTarget:self action:@selector(btn_follow_click:) forControlEvents:UIControlEventTouchUpInside];
        // cell.btn_follow.layer.cornerRadius = 5;
		
	}
    
    Faceabook_find_friend_Share *shareObj =[self.array_friend objectAtIndex:indexPath.row];
    cell.lbl_name.text = shareObj.name;
    cell.lbl_username.text =shareObj.username;
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.url_image] placeholderImage:nil];
    //cell.img_user.imageURL =[NSURL URLWithString:shareObj.url_image];
    cell.btn_follow.tag =indexPath.row;
    if ([shareObj.is_following isEqualToString:@"no"]) {
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
    }else{
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
    }
	return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Follow Click
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    
    
    NSInteger tag =btn.tag;
    
    Faceabook_find_friend_Share *shareObj =[self.array_friend objectAtIndex:tag];
    //post user follow
    //http://www.techintegrity.in/mystyle/post_user_following.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=1&following_id=2
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    if ([shareObj.is_following isEqualToString:@"no"]) {
        shareObj.is_following =@"yes";
        url=@"post_user_following.php";
    }else{
        shareObj.is_following =@"no";
        url=@"post_user_unfollow.php";
    }
    
    [self.array_friend replaceObjectAtIndex:tag withObject:shareObj];
    [self.tbl_friend reloadData];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14208" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingUnfollowingAPIResponce:) name:@"14208" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14208" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingUnfollowingAPIResponce:) name:@"-14208" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            shareObj.user_id,@"following_id"
                            ,nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14208" :params];
    
}

-(void)postUserFollowingUnfollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14208" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14208" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSLog(@"result:%@",result);
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }
}

-(void)FailpostUserFollowingUnfollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14208" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14208" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Start Stop Activity
-(void)start_activity {
    [DejalBezelActivityView activityViewForView:self.view];
}

-(void)stop_activity {
    [DejalBezelActivityView removeView];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
