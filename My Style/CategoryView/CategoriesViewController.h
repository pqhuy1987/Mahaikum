//
//  CategoriesViewController.h
//  
//
//  Created by Charles Moon on 11/12/15.
//
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "AddCategoryViewController.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "Category_info_ViewController.h"

@protocol CategoriesViewControllerDelegate;

@interface CategoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MGSwipeTableCellDelegate>

@property(nonatomic) BOOL editable;
@property(nonatomic) BOOL postable;
@property(nonatomic,strong)NSString *nav_title;
@property(nonatomic,strong)NSString *user_id;
@property(nonatomic,strong)IBOutlet  UILabel *lbl_title;
@property(nonatomic,strong)IBOutlet UITableView *tbl_categories;
@property (strong, nonatomic) IBOutlet UIButton *btn_add;
@property(nonatomic,strong) NSMutableArray *array_categories;

@property (nonatomic, strong) AddCategoryViewController *add_category_view;
@property (nonatomic, strong) id<CategoriesViewControllerDelegate> delegate;

@end

@protocol CategoriesViewControllerDelegate <NSObject>

@optional
- (void)dismissButtonClicked:(CategoriesViewController *)viewController;
- (void)categorySelected:(CategoriesViewController *)viewController category:(CategoryObject *)category;

@end
