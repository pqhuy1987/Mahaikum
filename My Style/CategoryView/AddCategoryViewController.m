//
//  AddCategoryViewController.m
//  
//
//  Created by Charles Moon on 11/13/15.
//
//

#import "AddCategoryViewController.h"

@interface AddCategoryViewController () <UITextViewDelegate>
{
    UIColor *placeHolderColor;
}

@end

@implementation AddCategoryViewController

@synthesize lblSpinnerBg, activity, tdt_categoryname, tev_categorydesc, category;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    placeHolderColor = [UIColor lightGrayColor];
    
    tdt_categoryname.text = self.category.categoryName;
    tev_categorydesc.text = self.category.categoryDescs;
    
    tdt_categoryname.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    tdt_categoryname.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.05];
    tev_categorydesc.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.05];
    
    if ([tdt_categoryname respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        tdt_categoryname.attributedPlaceholder = [[NSAttributedString alloc] initWithString:CategoryName attributes:@{NSForegroundColorAttributeName: placeHolderColor}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
    
    if ([self.category.categoryDescs isEqualToString:@""])
    {
        tev_categorydesc.text = CategoryDescription;
        tev_categorydesc.textColor = placeHolderColor;
    }
    else
        tev_categorydesc.textColor = [UIColor blackColor];
    
    if (self.edit)
        self.lbl_title.text = @"Edit Category";
    else
        self.lbl_title.text = @"Add Category";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// textview delegate methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView.textColor == placeHolderColor)
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0)
    {
        textView.text = CategoryDescription;
        textView.textColor = placeHolderColor;
        [textView resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0)
    {
        textView.text = CategoryDescription;
        textView.textColor = placeHolderColor;
        [textView resignFirstResponder];
    }
}

- (void)addCategory
{
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(add_category:) name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-62" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@", salt, sig, self.user_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_category.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.tev_categorydesc.text, @"category_description",
                            self.tdt_categoryname.text, @"category",
                            nil];
    [networkQueue queueItems :requestStr:@"62":params];
    
    NSLog(@"params:%@",params);
}

- (void)add_category:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
        [self back];
    else
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not add category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not add category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

- (void)updateCategory
{
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update_category:) name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-62" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&id=%@", salt, sig, self.user_id, self.category.categoryId];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_category.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.tev_categorydesc.text, @"category_description",
                            self.tdt_categoryname.text, @"category",
                            nil];
    [networkQueue queueItems :requestStr:@"62":params];
    
    NSLog(@"params:%@",params);
}

- (void)update_category:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
        [self back];
    else
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not add category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

- (void)back
{
    [GlobalDefine performBlock:^{
        if (!self.postable)
            [self.navigationController popViewControllerAnimated:YES];
        else
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(backedFromAddCategoryView:)])
                [self.delegate backedFromAddCategoryView:self];
        }
    } afterDelay:0.5];
}

- (IBAction)btn_back_click:(id)sender {
    [self back];
}

- (IBAction)doneButtonClicked:(id)sender
{
    if (tdt_categoryname.text.length == 0 || tev_categorydesc.text.length == 0 || [tev_categorydesc.text isEqualToString:CategoryDescription])
    {
        //[AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please fill all fields" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"You must fill all fields." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (self.edit)
            [self updateCategory];
        else
            [self addCategory];
    }
}

- (IBAction)bgTapped:(id)sender
{
    [tdt_categoryname resignFirstResponder];
    [tev_categorydesc resignFirstResponder];
}

@end
