//
//  CategoriesViewController.m
//  
//
//  Created by Charles Moon on 11/12/15.
//
//

#import "CategoriesViewController.h"

@interface CategoriesViewController () <UIAlertViewDelegate, AddCategoryViewControllerDelegate>
{
    Category_info_ViewController *categoryInfoView;
    NSIndexPath *selectedCategory;
}
@end

@implementation CategoriesViewController

@synthesize user_id,nav_title,lbl_title, btn_add;
@synthesize tbl_categories,array_categories;
@synthesize add_category_view;
@synthesize delegate;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    if (!self.postable)
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissButtonClicked:)])
            [self.delegate dismissButtonClicked:self];
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.array_categories = [[NSMutableArray alloc] init];
    self.tbl_categories.separatorColor = [UIColor blackColor];
    self.btn_add.hidden = !self.editable;
    
    self.add_category_view = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    if (is_iPhone_5) {
        tbl_categories.frame=CGRectMake(0,64,kViewWidth,455);
    }
    else {
        tbl_categories.frame=CGRectMake(0,64,kViewWidth,455-88);
    }
    
    //    self.lbl_title.text = self.nav_title;
    self.lbl_title.font = [UIFont fontWithName:@"Roboto" size:16.0];
    
    if ([self.user_id isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]])
    {
        self.editable = YES;
    }
    else
    {
        self.editable = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_categories_list];
}

- (void)get_categories_list
{
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"61" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(get_categories_list_responce:) name:@"61" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-61" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-61" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@", salt, sig, self.user_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_category.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"61":nil];
    
    if ([self.nav_title isEqualToString:@"FOLLOWERS"] || [self.nav_title isEqualToString:@"Followers"]||[self.nav_title isEqualToString:@"followers"])
        self.lbl_title.text = @"Followers";
    else if ([self.nav_title isEqualToString:@"Categories"])
        self.lbl_title.text = @"Categories";
    else
        self.lbl_title.text = @"Following";
}

- (void)get_categories_list_responce:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"61" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-61" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"])
    {
        [self.array_categories removeAllObjects];
        
        NSArray *dataArray = result[@"data"];
        for(NSDictionary *data in dataArray)
        {
            CategoryObject *object = [[CategoryObject alloc] init];
            object.categoryId = data[@"id"];
            object.categoryName = data[@"name"];
            object.categoryDescs = data[@"description"];
            [self.array_categories addObject:object];
        }
        [self.tbl_categories reloadData];
    }
    else {
        [self.array_categories removeAllObjects];
        [self.tbl_categories reloadData];
    }
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"61" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-61" object:nil];
    [SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get categories" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

- (void)deleteCategory:(NSIndexPath *)indexPath
{
    CategoryObject *data = (CategoryObject *)self.array_categories[indexPath.row];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&id=%@", salt, sig, self.user_id, data.categoryId];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_category.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([responseObject[@"success"] isEqualToString:@"1"])
            [self get_categories_list];
        else
            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not delete category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not delete category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark UITableview Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_categories.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Categories_list_cell";
    
    CategoryObject *data = (CategoryObject *)self.array_categories[indexPath.row];
    MGSwipeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)	{
        cell = [[MGSwipeTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    
    cell.textLabel.text = data.categoryName;
    cell.textLabel.font = [UIFont fontWithName:@"Myriad Pro" size:17.0f];
    
    if (![data.categoryName isEqualToString:DefaultCategoryName] && self.editable)
    {
        MGSwipeButton *deleteButton = [MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]];
        MGSwipeButton *editButton = [MGSwipeButton buttonWithTitle:@"Edit" backgroundColor:[UIColor lightGrayColor]];
        
        deleteButton.indexPath = indexPath;
        editButton.indexPath = indexPath;
        
        [deleteButton addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.rightButtons = @[deleteButton, editButton];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryObject *data = (CategoryObject *)self.array_categories[indexPath.row];
    if (!self.postable)
    {
        categoryInfoView.user_id = self.user_id;
        categoryInfoView.category = data;
        [self.navigationController pushViewController:categoryInfoView animated:YES];
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(categorySelected:category:)])
            [self.delegate categorySelected:self category:data];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)deleteButtonClicked:(MGSwipeButton *)sender
{
    selectedCategory = sender.indexPath;
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Warning"
                              message:@"If you delete this category all the posts inside the category will be moved to the Miscellaneous Category."
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Delete", nil];
    [alertView show];
}

- (void)editButtonClicked:(MGSwipeButton *)sender
{
    CategoryObject *data = (CategoryObject *)self.array_categories[sender.indexPath.row];
    self.add_category_view.user_id = self.user_id;
    self.add_category_view.category = data;
    self.add_category_view.edit = YES;
    if (!self.postable)
        [self.navigationController pushViewController:self.add_category_view animated:YES];
    else
    {
        self.add_category_view.postable = self.postable;
        self.add_category_view.delegate = self;
        [self presentViewController:self.add_category_view animated:NO completion:nil];
    }
}

// add category view controller delegate
- (void)backedFromAddCategoryView:(AddCategoryViewController *)viewController
{
    viewController.delegate = nil;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

// alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Delete"])
    {
        [self deleteCategory:selectedCategory];
    }
}


- (IBAction)addButtonClicked:(id)sender
{
    CategoryObject *data = [[CategoryObject alloc] init];
    self.add_category_view.user_id = self.user_id;
    self.add_category_view.category = data;
    self.add_category_view.edit = NO;
    
    if (!self.postable)
        [self.navigationController pushViewController:self.add_category_view animated:YES];
    else
    {
        self.add_category_view.postable = self.postable;
        self.add_category_view.delegate = self;
        [self presentViewController:self.add_category_view animated:NO completion:nil];
    }
}

@end
