//
//  User_info_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/28/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "image_collection_cell.h"
#import "image_collection_header.h"
#import "image_collection_footer.h"
#import "Likers_list_cell.h"
#import "Show_map_ViewController.h"
#import "StaticClass.h"
#import "Singleton.h"

#import "AJNotificationView.h"

#import "Home_tableview_data_share.h"
#import "Comment_share.h"

#import "EGOImageButton.h"
#import "Home_tableview_cell.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "BlockActionSheet.h"
#import "Likers_list_ViewController.h"
#import "Comments_list_ViewController.h"
#import "Share_photo.h"
#import "Image_detail.h"

#import "Followers_list.h"
#import "Profile_share.h"
#import "Edit_your_profile.h"
#import "Setting_profile.h"
#import "LORichTextLabel.h"
#import "CategoriesViewController.h"
#import "AddCategoryViewController.h"

@interface Category_info_ViewController : UIViewController<MFMailComposeViewControllerDelegate,DLStarRatingDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    //News feed
    NSInteger which_image_liked;
    NSInteger which_image_delete;
    NSInteger image_upload_flag;
    
    NSInteger fastCategoryFollow;
    
    NSString *totalFollowers;
}

@property(nonatomic,strong)IBOutlet UIImageView *imgRequestSent;
@property(nonatomic,strong)IBOutlet UIButton *btn_edit;
@property(nonatomic,strong)IBOutlet UIButton *btn_edit_tbl;
@property(nonatomic,strong) LORichTextLabelStyle *username_style;

@property(nonatomic,strong) UIView *imgfullscreenviewobj;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) CategoryObject *category;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblphotos,*lblfollowing,*lblfollowers;
@property(nonatomic,strong)IBOutlet UILabel *lblphotos_tbl,*lblfollowing_tbl,*lblfollowers_tbl;
@property(nonatomic,strong)IBOutlet  UIView *view_secondcell;
@property(nonatomic,strong)IBOutlet  UIView *view_secondcell_tbl;

@property(nonatomic,strong)UIImagePickerController *photoPickerController;

@property(nonatomic,strong) Profile_share *profile_share_obj;
@property(nonatomic,strong) Followers_list *followers_list_viewObj;
@property(nonatomic,strong) Image_detail *image_detail_viewObj;
@property(nonatomic,strong) Likers_list_ViewController *liker_list_viewObj;
@property(nonatomic,strong) Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,strong) Edit_your_profile *edit_your_profile_viewObj;
@property(nonatomic,strong) Setting_profile *setting_profileViewObj;
@property (nonatomic, strong) AddCategoryViewController *add_category_view;

@property(nonatomic,strong) Share_photo *share_photo_view;
@property(nonatomic,strong)IBOutlet UIImageView *img_like_heart;
@property(nonatomic,strong) NSMutableArray *array_feeds;
//Footer view
@property(nonatomic,strong)IBOutlet UIView *view_footer;
@property(nonatomic,strong)IBOutlet UIView *view_footer_tbl;

@property(nonatomic,strong)Show_map_ViewController *mapviewObj;

@property(nonatomic,strong)IBOutlet  UICollectionView *collectionViewObj;

// for category
@property(nonatomic,strong)IBOutlet UIView *category_header;
@property(nonatomic,strong)IBOutlet UIView *category_header_tbl;
@property (strong, nonatomic) IBOutlet UILabel *category_followers;
@property (strong, nonatomic) IBOutlet UILabel *category_posts;
@property (strong, nonatomic) IBOutlet UILabel *category_followers_tbl;
@property (strong, nonatomic) IBOutlet UILabel *category_posts_tbl;
@property (strong, nonatomic) IBOutlet UITextView *category_descs;
@property (strong, nonatomic) IBOutlet UITextView *category_descs_tbl;
@property (strong, nonatomic) IBOutlet UIButton *btn_category_follow;
@property (strong, nonatomic) IBOutlet UIButton *btn_category_follow_tbl;

//Header view
@property(nonatomic,strong)IBOutlet UIView *view_header;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo;
@property(nonatomic,strong)IBOutlet UIView *view_img_photo;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_scondcell;

@property(nonatomic,strong)IBOutlet UIView *view_header_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl_bg_list;
@property(nonatomic,strong)IBOutlet UIView *view_img_photo_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_scondcell_tbl;

@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followers;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followings;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_photos;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_categories;

@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followers_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followings_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_photos_tbl;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_categories_tbl;

@property(nonatomic,strong)IBOutlet LORichTextLabel *lbl_name;
@property(nonatomic,strong)IBOutlet LORichTextLabel *lbl_name_tbl;

//UITableview
@property(nonatomic,strong)IBOutlet UITableView *tbl_news;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow_tbl;
@property(nonatomic,strong)IBOutlet UIButton *btn_friend_tbl;
@property(nonatomic,strong)IBOutlet UIButton *btn_friend;

@property(nonatomic,strong) IBOutlet UIButton *btnAccept;
@property(nonatomic,strong) IBOutlet UIButton *btnNotNow;
@property(nonatomic,strong) IBOutlet UIButton *btnAcceptTbl;
@property(nonatomic,strong) IBOutlet UIButton *btnNotNowTbl;

@property(nonatomic,strong) UIAlertView *denayAlertViewObj;
@property(nonatomic,strong) IBOutlet UIImageView *imgRequestSentTbl;
@property(nonatomic,strong) IBOutlet UILabel *lblPhotoPrivate;
@property(nonatomic,strong) IBOutlet UILabel *lblFriendRequest;
@property(nonatomic,strong) IBOutlet UILabel *lblFriendRequestTbl;
@property(strong,nonatomic) IBOutlet UILabel *lbl_no_posts;

@property(nonatomic,strong) IBOutlet UIImageView *img_PostBG;
@property(nonatomic,strong) IBOutlet UIImageView *img_tbl_PostBG;
@property(nonatomic,strong) IBOutlet UIButton *btn_postButton;
@property(nonatomic,strong) IBOutlet UIButton *btn_tbl_postButton;

@property(nonatomic,strong)IBOutlet UIButton*BlockUser_Btn;

@property(nonatomic,strong)IBOutlet UILabel*objUserName_lbl;
@property (nonatomic, strong) Share_photo_ViewController *share_photo_viewObj;

-(IBAction)btn_friend_click:(id)sender;

@end
