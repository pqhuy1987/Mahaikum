//
//  AddCategoryViewController.h
//  
//
//  Created by Charles Moon on 11/13/15.
//
//

#import <UIKit/UIKit.h>

@protocol AddCategoryViewControllerDelegate;

@interface AddCategoryViewController : UIViewController

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong) UIActivityIndicatorView *activity;
@property (strong, nonatomic) IBOutlet UITextField *tdt_categoryname;
@property (strong, nonatomic) IBOutlet UITextView *tev_categorydesc;
@property(nonatomic,strong)IBOutlet  UILabel *lbl_title;
@property (nonatomic, strong) CategoryObject *category;
@property(nonatomic,strong) NSString *user_id;
@property (nonatomic) BOOL edit;
@property (nonatomic) BOOL postable;

@property (nonatomic, strong) id<AddCategoryViewControllerDelegate> delegate;

@end

@protocol  AddCategoryViewControllerDelegate <NSObject>

@optional
- (void)backedFromAddCategoryView:(AddCategoryViewController *)viewController;

@end