//
//  CategoryObject.h
//  
//
//  Created by Charles Moon on 11/13/15.
//
//

#import <Foundation/Foundation.h>

@interface CategoryObject : NSObject

@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *categoryDescs;
@property (nonatomic, strong) NSString *categoryFollowers;
@property (nonatomic, strong) NSString *categoryUserFollower;

@end
