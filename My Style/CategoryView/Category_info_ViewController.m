//
//  User_info_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/28/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Category_info_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "BlockUserRecord.h"
#import <STPopup/STPopup.h>
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

#define screen_width     [[UIScreen mainScreen] bounds].size.width
static NSInteger cellClickIndex=0;

@interface Category_info_ViewController () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, STXSectionHeaderViewDelegate, UIGestureRecognizerDelegate,UICollectionViewDelegateFlowLayout,STPopupPreviewRecognizerDelegate>{
    NSString *image_url;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    BlockUserRecord*objBlockUserRecord;
    NSString*Block_User_Tittle_Str;
    CategoriesViewController *category_list_view;
    BOOL categoryFollowed;
    
    int currentSelectedIndex;
}

@property(nonatomic,strong)NSString *image_url;

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@end

@implementation Category_info_ViewController

@synthesize category_header,category_header_tbl,category, category_followers, category_followers_tbl, category_posts, category_posts_tbl, category_descs, category_descs_tbl, objUserName_lbl, btn_category_follow, btn_category_follow_tbl;
@synthesize collectionViewObj,btn_friend_tbl,btn_friend;
@synthesize view_header,img_photo,img_bg_firstcell,img_bg_scondcell,view_img_photo;
@synthesize view_header_tbl,img_photo_tbl,img_photo_tbl_bg,img_photo_tbl_bg_list,img_bg_firstcell_tbl,img_bg_scondcell_tbl,view_img_photo_tbl,btnAccept,btnNotNow,btnAcceptTbl,btnNotNowTbl;

@synthesize tbl_news,imgRequestSent,denayAlertViewObj;
@synthesize mapviewObj,lblFriendRequest,lblFriendRequestTbl;

@synthesize view_footer,view_footer_tbl;
@synthesize array_feeds,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view,image_detail_viewObj,followers_list_viewObj,edit_your_profile_viewObj,setting_profileViewObj;

@synthesize profile_share_obj;
@synthesize lbl_total_followers,lbl_total_followings,lbl_total_photos,lbl_total_categories,lbl_total_followers_tbl,lbl_total_followings_tbl,lbl_total_photos_tbl,lbl_total_categories_tbl;
@synthesize lbl_name,lbl_name_tbl,image_url;
@synthesize photoPickerController;
@synthesize view_secondcell,view_secondcell_tbl;
@synthesize img_down_arrow,img_down_arrow_tbl,user_id;
@synthesize btn_follow,btn_follow_tbl,imgRequestSentTbl,lblPhotoPrivate;

@synthesize username_style,imgfullscreenviewobj;
@synthesize btn_edit,btn_edit_tbl,lblfollowers,lblfollowers_tbl,lblfollowing,lblfollowing_tbl,lblheader,lblphotos_tbl,lblphotos;

@synthesize img_PostBG,img_tbl_PostBG,btn_postButton,btn_tbl_postButton;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentSelectedIndex = 0;
    
    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
    self.tbl_news.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tbl_news.delegate = delegate;
    self.tableViewDelegate = delegate;
    
    
    self.btn_edit.hidden = YES;
    self.btn_edit_tbl.hidden = YES;
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:16.0];
    lblphotos.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowing.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowers.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    
    lblphotos_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowing_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    lblfollowers_tbl.font = [UIFont fontWithName:@"Roboto-Light" size:14.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    self.add_category_view = [[AddCategoryViewController alloc] initWithNibName:@"AddCategoryViewController" bundle:nil];
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.profile_share_obj =[[Profile_share alloc]init];
    
    self.array_feeds =[[NSMutableArray alloc]init];
    [self.img_like_heart setHidden:YES];
    
    self.img_down_arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    self.img_down_arrow_tbl.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    
    self.followers_list_viewObj =[[Followers_list alloc]initWithNibName:@"Followers_list" bundle:nil];
    category_list_view = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil];
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    self.image_detail_viewObj=[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    
    self.edit_your_profile_viewObj =[[Edit_your_profile alloc]initWithNibName:@"Edit_your_profile" bundle:nil];
    self.setting_profileViewObj=[[Setting_profile alloc]initWithNibName:@"Setting_profile" bundle:nil];
    
    //   self.tbl_news.hidden =YES;
    self.mapviewObj =[[Show_map_ViewController alloc]initWithNibName:@"Show_map_ViewController" bundle:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    
    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    
    // self.img_photo.placeholderImage =[UIImage imageNamed:@"default_user_image"];
    // self.img_photo_tbl.placeholderImage =[UIImage imageNamed:@"default_user_image"];
    
    //Header Name and bio
    self.lbl_name = [[LORichTextLabel alloc] initWithWidth:screen_width -20];
    [self.lbl_name setBackgroundColor:[UIColor clearColor]];
    [self.lbl_name setTextColor:[UIColor blackColor]];
    [self.lbl_name positionAtX:10.0 andY:170.0];//15,90
    [self.view_header addSubview:self.lbl_name];
    
    self.lbl_name_tbl = [[LORichTextLabel alloc] initWithWidth:screen_width -20];
    [self.lbl_name_tbl setBackgroundColor:[UIColor clearColor]];
    [self.lbl_name_tbl setTextColor:[UIColor blackColor]];
    [self.lbl_name_tbl positionAtX:10.0 andY:170.0];//15,90
    
    [self.category_header_tbl addSubview:self.lbl_name_tbl];
    self.tbl_news.tableHeaderView =self.category_header_tbl;
    self.tbl_news.tableFooterView = self.view_footer_tbl;
    
    image_upload_flag=0;
    
    self.username_style = [LORichTextLabelStyle styleWithFont:[UIFont boldSystemFontOfSize:17.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    self.username_style.font = [UIFont fontWithName:@"Roboto-Medium" size:13.0];
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    
    LORichTextLabelStyle  *url_style = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    
    [url_style addTarget:self action:@selector(urlSelected:)];
    
    [self.lbl_name addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"www."];
    [self.lbl_name addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name addStyle:url_style forPrefix:@"Https://"];
    
    [self.lbl_name addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Https://"];
    
    [self.lbl_name_tbl addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name_tbl addStyle:atStyle forPrefix:@"@"];
    
    [self.collectionViewObj addSubview:lblPhotoPrivate];
//    [self block_user_list];
    
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRight:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];
    
    UISwipeGestureRecognizer *gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureLeft:)];
    [gesture1 setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:gesture1];
    
}


#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];

    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}


- (void)gestureLeft:(id)sender
{
    currentSelectedIndex = ( currentSelectedIndex + 1 ) % 2;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
}

- (void)gestureRight:(id)sender
{
    if ( currentSelectedIndex == 0 )
    {
        [self btn_back_click:self];
        return;
    }
    
    currentSelectedIndex = ( currentSelectedIndex - 1 ) % 2;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;
    
    imgRequestSent.hidden=YES;
    imgRequestSentTbl.hidden=YES;
    
    btn_follow.hidden = YES;
    btn_follow_tbl.hidden = YES;
    btn_category_follow.hidden = YES;
    btn_category_follow_tbl.hidden = YES;
    
    btn_friend.hidden=YES;
    btn_friend_tbl.hidden=YES;
    
    lblFriendRequest.hidden=YES;
    lblFriendRequestTbl.hidden=YES;
    
    [self.collectionViewObj setContentOffset:CGPointZero];
    [self.tbl_news setContentOffset:CGPointZero];
    
    self.category_descs.text = self.category.categoryDescs;
    self.category_descs_tbl.text = self.category.categoryDescs;
    self.objUserName_lbl.text = self.category.categoryName;
    
    img_photo.layer.cornerRadius =55.0f;
    img_photo.layer.masksToBounds =YES;
    img_photo_tbl.layer.cornerRadius =55.0f;
    img_photo_tbl.layer.masksToBounds =YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    //    [self get_user_info];
    [self get_news_feed];
}


-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObj=[array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked = (int)userActionCell.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    //////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

    if (![shareObj.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}

-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}

-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:userActionCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)userWillShare:(STXUserActionCell *)userActionCell {
    
    NSInteger tag = (int)userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];

        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            } else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        //report_id : 1= i dont like this photo,  2=this photo is spam or a scam,  3=this photo puts people at risk,  4=this photo shouldn't be on mystyle
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
            //  [controller release];
        } else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:commentCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag = (int)likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
    //hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
    //pushPopFlg=1;
    //hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
    // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}

- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
    //  pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (IBAction)btnProfileClicked:(id)sender {
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld", (long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:posterLike.tag];
    which_image_liked = (int)posterLike.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    //////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    [self.lbl_name setText:@""];
    self.lbl_total_followers.text =@"0";
    self.lbl_total_followings.text = @"0";
    self.lbl_total_photos.text = @"0";
    self.lbl_total_categories.text = @"0";
    self.img_photo.image = [UIImage imageNamed:@"default_user_image"];
    
    [self.lbl_name_tbl setText:@""];
    self.lbl_total_followers_tbl.text =@"0";
    self.lbl_total_followings_tbl.text = @"0";
    self.lbl_total_photos_tbl.text = @"0";
    self.lbl_total_categories_tbl.text = @"0";
    self.img_photo_tbl.image = [UIImage imageNamed:@"default_user_image"];
    
    self.category_followers.text = @"0";
    self.category_followers_tbl.text = @"0";
    self.category_posts.text = @"0";
    self.category_posts_tbl.text = @"0";
    
    self.btn_follow.hidden =YES;
    self.btn_follow_tbl.hidden =YES;
    self.btn_category_follow.hidden = YES;
    self.btn_category_follow_tbl.hidden = YES;
}

#pragma mark - Photos count click

- (IBAction)btn_categories_count_click:(id)sender {
    category_list_view.user_id =self.profile_share_obj.user_id;
    category_list_view.editable = NO;
    category_list_view.nav_title = @"Categories";
    
    [self.navigationController pushViewController:category_list_view animated:YES];
}

-(IBAction)btn_photos_count_click:(id)sender{
    [self.collectionViewObj setContentOffset:CGPointMake(100, self.category_header.frame.size.height-68)];
    [self.tbl_news setContentOffset:CGPointMake(0, self.category_header.frame.size.height-68)];
}

#pragma mark - Followes count click
-(IBAction)btn_followers_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"Followers";
    
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

-(IBAction)btn_category_followers_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.user_id;
    self.followers_list_viewObj.category = self.category;
    self.followers_list_viewObj.categoryViewed = YES;
    self.followers_list_viewObj.nav_title = @"Followers";
    
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}


#pragma mark - Following count click
-(IBAction)btn_following_count_click:(id)sender{
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"Following";
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

#pragma mark - Edit your profile click
-(IBAction)btn_edit_your_profile_click:(id)sender{
    self.edit_your_profile_viewObj.hidesBottomBarWhenPushed = YES;
    self.edit_your_profile_viewObj.is_reload_data=1;
    
    [self.navigationController pushViewController:self.edit_your_profile_viewObj animated:YES];
    
}

#pragma mark - Setting Button click
-(IBAction)btn_setting_click:(id)sender{
    [self.navigationController pushViewController:self.setting_profileViewObj animated:YES];
}

#pragma mark - Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
    [self block_user_list];
}

#pragma mark - Get user Profile Info
-(void)get_user_info {
    if (fastCategoryFollow != 1)
        [SVProgressHUD show];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_Responce:) name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-605" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-605" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login=%@&to_id=%@&flag=1",salt,sig,[self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],[self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_profile.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"605":nil];
    
    fastCategoryFollow = 0;
}

-(void)get_user_info_Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-605" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dict = [result valueForKey:@"data"];
        self.profile_share_obj.user_id = [dict valueForKey:@"user_id"];
        self.profile_share_obj.username = [StaticClass urlDecode:[dict valueForKey:@"username"]];
        self.profile_share_obj.name = [StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_share_obj.email = [StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_share_obj.facebook_id = [dict valueForKey:@"facebook_id"];
        self.profile_share_obj.total_followers = [dict valueForKey:@"total_followers"];
        self.profile_share_obj.total_following = [dict valueForKey:@"total_following"];
        self.profile_share_obj.total_categories = [dict valueForKey:@"total_categories"];
        
        self.profile_share_obj.total_photos =[dict valueForKey:@"total_images"];
        self.profile_share_obj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_share_obj.bio=[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        self.profile_share_obj.web_url=[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        
        self.profile_share_obj.is_user_frined = [StaticClass urlDecode:[dict valueForKey:@"user_friend"]];
        self.profile_share_obj.is_user_follow=[StaticClass urlDecode:[dict valueForKey:@"user_following"]];
        
        self.profile_share_obj.is_private_flg=[dict valueForKey:@"is_private"];

        self.objUserName_lbl.text=profile_share_obj.name;
        [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_tbl_bg sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl_bg_list sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        //        self.img_photo.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
        //        self.img_photo_tbl.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
        
        if (self.profile_share_obj.total_followers.length==0) {
            self.lbl_total_followers.text=@"0";
            self.lbl_total_followers_tbl.text=@"0";
        }
        else {
            self.lbl_total_followers.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_tbl.text=self.profile_share_obj.total_followers;
        }
        
        if (self.profile_share_obj.total_following.length==0) {
            self.lbl_total_followings.text=@"0";
            self.lbl_total_followings_tbl.text=@"0";
        }
        else {
            self.lbl_total_followings.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_tbl.text=self.profile_share_obj.total_following;
        }
        
        if (self.profile_share_obj.total_photos.length==0) {
            self.lbl_total_photos.text=@"0";
            self.lbl_total_photos_tbl.text=@"0";
        } else {
            self.lbl_total_photos.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_tbl.text=self.profile_share_obj.total_photos;
        }
        
        if (self.profile_share_obj.total_categories.length == 0) {
            self.lbl_total_categories.text = @"0";
            self.lbl_total_categories_tbl.text = @"0";
        } else {
            self.lbl_total_categories.text = self.profile_share_obj.total_categories;
            self.lbl_total_categories_tbl.text = self.profile_share_obj.total_categories;
        }
        
        NSMutableString *str =[[NSMutableString alloc]init];
        
        if (self.profile_share_obj.name.length==0) {
            [self.lbl_name setText:@""];
            [self.lbl_name setText:@""];
        }
        else {
            // [str appendFormat:@"%@ \n\n",self.profile_share_obj.name];
            
            for (NSString *name in [self.profile_share_obj.name componentsSeparatedByString:@" "]) {
                if (name.length>0) {
                    [self.lbl_name addStyle:self.username_style forPrefix:name];
                    [self.lbl_name_tbl addStyle:self.username_style forPrefix:name];
                }
            }
        }
        
        NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
        NSString *uid2=[NSString stringWithFormat:@"%ld",(long)[self.profile_share_obj.user_id integerValue]];
        
        int isOweNerorNot=0;
        if (![uid1 isEqualToString:uid2]) {
            isOweNerorNot=0;
//            if ([self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
//                
//                [self.btn_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
//                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
//                
//            } else {
//                [self.btn_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
//                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
//            }
            
            if ([self.profile_share_obj.is_user_frined isEqualToString:@"no"]||[self.profile_share_obj.is_user_frined isEqualToString:@"request not sent"] ) {
                
                [self.btn_friend_tbl setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
                
                [self.btn_friend setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
            } else {
                [self.btn_friend_tbl setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
                
                [self.btn_friend setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
            }
            
            
            if([profile_share_obj.is_user_frined isEqualToString:@"request not sent"]) {
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=NO;
                btn_friend_tbl.hidden=NO;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                btn_follow_tbl.hidden = NO;
                btn_follow.hidden = NO;
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
                
                
            }
            else if([profile_share_obj.is_user_frined isEqualToString:@"request sent"]) {
                imgRequestSent.hidden = NO;
                imgRequestSentTbl.hidden =NO;
                
                btn_friend.hidden=YES;
                btn_friend_tbl.hidden=YES;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                btn_follow_tbl.hidden = NO;
                btn_follow.hidden = NO;
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
            }
            else if([profile_share_obj.is_user_frined isEqualToString:@"request pending"]) {
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=YES;
                btn_friend_tbl.hidden=YES;
                
                btnNotNow.hidden=NO;
                btnNotNowTbl.hidden=NO;
                
                btnAccept.hidden=NO;
                btnAcceptTbl.hidden=NO;
                
                btn_follow_tbl.hidden = YES;
                btn_follow.hidden = YES;
                
                lblFriendRequest.hidden=NO;
                lblFriendRequestTbl.hidden=NO;
            }
            else {
                
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=NO;
                btn_friend_tbl.hidden=NO;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                btn_follow_tbl.hidden = NO;
                btn_follow.hidden = NO;
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
            }
            
            self.btn_edit_tbl.hidden = YES;
            self.btn_edit.hidden = YES;
            
            [lbl_name setWidth:screen_width -20];
            [lbl_name_tbl setWidth:screen_width -20];
        }
        else {
            isOweNerorNot=1;
            self.btn_follow_tbl.hidden = YES;
            self.btn_follow.hidden = YES;
            
            self.btn_friend_tbl.hidden = YES;
            self.btn_friend.hidden = YES;
            
            self.imgRequestSentTbl.hidden=YES;
            self.imgRequestSent.hidden=YES;
            
            self.btnAcceptTbl.hidden=YES;
            self.btnAccept.hidden=YES;
            
            self.btnNotNowTbl.hidden=YES;
            self.btnNotNow.hidden=YES;
            
            self.btn_edit_tbl.hidden = NO;
            self.btn_edit.hidden = NO;
            
            lblFriendRequest.hidden=YES;
            lblFriendRequestTbl.hidden=YES;
            
            [lbl_name setWidth:screen_width-20];
            [lbl_name_tbl setWidth:screen_width-20];
        }
        
        //        if (!self.profile_share_obj.web_url.length==0) {
        //            [str appendFormat:@"%@",self.profile_share_obj.web_url];
        //        }
        
        int fl=0;
        if (!(self.profile_share_obj.bio.length == 0)) {
            [str appendFormat:@"%@ \n\n",self.profile_share_obj.bio];
        }
        else {
            fl=1;
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
        }
        if (fl==1) {
            [str appendFormat:@"\n"];
        }
        
        if (!(self.profile_share_obj.web_url.length == 0)) {
            [str appendFormat:@"%@",self.profile_share_obj.web_url];
        }
        else {
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        NSString *tem=[NSString stringWithFormat:@"%@",str];
        [self.lbl_name setText:tem];
        [self.lbl_name_tbl setText:tem];
        
        if (self.lbl_name.height>22) {
            //  CGSize size = [[NSString stringWithFormat:@"%@",tem]  sizeWithFont:[UIFont boldSystemFontOfSize:17.0f]];
            
            //            CGRect frame_header =  self.view_header.frame;
            //            frame_header.size.height = 160.0f + self.lbl_name.height -22;
            //            CGRect frame_header_tbl =  self.view_header_tbl.frame;
            //            frame_header_tbl.size.height = 160.0f + self.lbl_name_tbl.height -22;
            [self.tbl_news setTableHeaderView:self.category_header_tbl];
        }
        /* else {
         self.img_bg_firstcell.frame = CGRectMake(5,10,310, 135);
         self.view_secondcell.frame = CGRectMake(0,160,320,60);
         self.view_header.frame = CGRectMake(0,0,320,225);
         self.img_bg_firstcell_tbl.frame = CGRectMake(5,10,310, 135);
         self.view_secondcell_tbl.frame = CGRectMake(0,160,320,60);
         self.view_header_tbl.frame = CGRectMake(0,0,320,225);
         [self.tbl_news setTableHeaderView:self.view_header_tbl];
         }
         */
        
        [self.array_feeds removeAllObjects];
        [self.collectionViewObj reloadData];
        
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        
        [self.tbl_news reloadData];
        
        if (isOweNerorNot==1) {
            [self get_news_feed];
            lblPhotoPrivate.hidden=YES;
        }
        else {
            if ([[dict valueForKey:@"is_private"] isEqualToString:@"1"] && [[dict valueForKey:@"user_following"] isEqualToString:@"no"]) {
                lblPhotoPrivate.hidden=NO;
                lblPhotoPrivate.frame=CGRectMake(80,tbl_news.tableHeaderView.frame.origin.y+tbl_news.tableHeaderView.frame.size.height+15 ,160 ,42 );
            }
            else {
                [self get_news_feed];
                lblPhotoPrivate.hidden=YES;
            }
        }
    }
    else {
        lblPhotoPrivate.hidden=YES;
        [self.array_feeds removeAllObjects];
        [self.collectionViewObj reloadData];
        
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        [self.tbl_news reloadData];
    }
    
}


-(IBAction)btnAcceptClick :(id)sender {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIResponceFlag:) name:@"14245" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIResponceFlag:) name:@"-14245" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            @"1",@"flag",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"to_id",
                            self.profile_share_obj.user_id,@"from_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14245" :params];
}

-(void)postAcceptRequestAPIResponceFlag:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSLog(@"result:%@",result);
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;
    [self get_user_info];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Accept Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailpostAcceptRequestAPIResponceFlag:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(IBAction)btnNotNowClick :(id)sender {
    UIButton *temp = (UIButton *)sender;
    
    denayAlertViewObj = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"are you sure !! you want to reject this request ??" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok",@"cancel", nil];
    
    denayAlertViewObj.tag = temp.tag;
    [denayAlertViewObj show];
}

#pragma mark - Get News Feed
-(void)get_news_feed
{
    if (fastCategoryFollow != 1)
        [SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"600" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getnewsResponce:) name:@"600" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-600" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-600" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login_id=%@&category_id=%@",salt,sig,[self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID], self.category.categoryId];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URL:%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"600":nil];
}

-(void)getnewsResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"600" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-600" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        [self.array_feeds removeAllObjects];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"])
    {
        [[Singleton sharedSingleton]setCurrent_time:[result valueForKey:@"curr_utc"]];
        [self.array_feeds removeAllObjects];
        
        self.category_descs.text = result[@"category_desc"];
        self.category_descs_tbl.text = result[@"category_desc"];
        self.objUserName_lbl.text = result[@"category_name"];
        
        NSArray *array =[result valueForKey:@"data"];
        
        for (NSDictionary *dict in array) {
            
            Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
            shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
            
            shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
            shareObj.image_id =[dict valueForKey:@"id"];
            shareObj.image_owner=[dict valueForKey:@"image_owner"];
            shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
            shareObj.lat =[dict valueForKey:@"lat"];
            shareObj.lng=[dict valueForKey:@"lng"];
            shareObj.price = dict[@"price"];
            shareObj.currency = dict[@"currency"];
            shareObj.sold = [dict[@"sold"] integerValue];
            shareObj.likes=[dict valueForKey:@"likes"];
            shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
            shareObj.uid=[dict valueForKey:@"uid"];
            shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
            shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
            shareObj.category_id = [dict valueForKey:@"category_id"];
            
            shareObj.liked=[dict valueForKey:@"user_liked"];
            shareObj.comment_count =[dict valueForKey:@"total_comment"];
            
            shareObj.array_liked_by=[[NSMutableArray alloc]init];
            //shareObj.blockStatus=NO;
            
            if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"liked_by"];
                
                for (NSDictionary *tempdict in tempArray) {
                    [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                }
                
            }else{
                // NSLog(@"NSString");
            }
            
            shareObj.array_comments=[[NSMutableArray alloc]init];
            shareObj.array_comments_tmp=[[NSMutableArray alloc]init];
            
            if (![[dict valueForKey:@"description"] isEqualToString:@""])
            {
                Comment_share *obj =[[Comment_share alloc]init];
                obj.uid =shareObj.uid;
                obj.username =shareObj.username;
                obj.name =shareObj.name;
                obj.image_url=shareObj.user_image;
                obj.comment_desc=shareObj.description;
                obj.datecreated=shareObj.datecreated;
                [shareObj.array_comments_tmp addObject:obj];
            }
            
            if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                NSArray *tempArray =[dict valueForKey:@"comments"];
                
                for (NSDictionary *tempdict in tempArray) {
                    Comment_share *obj =[[Comment_share alloc]init];
                    obj.comment_id =[tempdict valueForKey:@"id"];
                    obj.uid =[tempdict valueForKey:@"uid"];
                    obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                    obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                    obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                    obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                    obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                    
                    [shareObj.array_comments addObject:obj];
                    [shareObj.array_comments_tmp addObject:obj];
                }
            }else{
                // NSLog(@"NSString");
                
            }
            
            shareObj.rating=[dict valueForKey:@"rating"];
            shareObj.my_rating=[dict valueForKey:@"my_rating"];
            shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
            shareObj.totalUser=[dict valueForKey:@"tot_user"];
            if (shareObj.image_path.length !=0) {
                [self.array_feeds addObject:shareObj];
            }
        }
    }
    
    NSString *followers = [result valueForKey:@"category_followers"];
    
    self.category_followers.text = followers;
    self.category_followers_tbl.text = followers;
    totalFollowers = followers;
    
    self.category_posts.text = [NSString stringWithFormat:@"%ld", (long)self.array_feeds.count];
    self.category_posts_tbl.text = [NSString stringWithFormat:@"%ld", (long)self.array_feeds.count];
    
    if ([array_feeds count] == 0)
    {
        [self.lbl_no_posts setHidden:NO];
    }
    else
    {
        [self.lbl_no_posts setHidden:YES];
    }
    
    categoryFollowed = [result[@"user_follower"] boolValue];
    
    if ([self.user_id isEqualToString:LOGINEDUSERID])
    {
        self.btn_category_follow.hidden = YES;
        self.btn_category_follow_tbl.hidden = YES;
    }
    else
    {
        self.btn_category_follow.hidden = NO;
        self.btn_category_follow_tbl.hidden = NO;
    }
    
    if (!categoryFollowed) {

        [self.btn_category_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
        [self.btn_category_follow_tbl setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
        
    } else {
        [self.btn_category_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
        [self.btn_category_follow_tbl setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
    }
    
    NSLog(@"Count:%ld", (long)[self.array_feeds count]);
    [self block_user_list];
    
    self.tableViewDataSource.posts =self.array_feeds;// copy];
    self.tableViewDelegate.posts =self.array_feeds;// copy];
    
    [self.tbl_news reloadData];
    [self.collectionViewObj reloadData];
    [SVProgressHUD dismiss];
}

-(void)FailNewsReson:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"600" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-600" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
    
    //[SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    [SVProgressHUD dismiss];
}

#pragma mark - Collectionview & Tableview  choose for show
-(IBAction)btn_collectionview_click:(id)sender{
    [self.collectionViewObj setContentOffset:self.tbl_news.contentOffset];
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden =NO;
    [self.collectionViewObj addSubview:lblPhotoPrivate];
    
}

-(IBAction)btn_tableview_click:(id)sender{
    [self.tbl_news setContentOffset:self.collectionViewObj.contentOffset];
    self.tbl_news.hidden = NO;
    self.collectionViewObj.hidden =YES;
    [self.tbl_news addSubview:lblPhotoPrivate];
    
}

#pragma mark - UICollectionView
-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.category_header.frame;
        [headerView addSubview:self.category_header];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(0,self.category_header.frame.size.height);
        
        reusableview = headerView;
        NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
        return reusableview;
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        
        image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
        
        footerview.frame =self.view_footer.frame;
        [footerview addSubview:self.view_footer];
        
        reusableview = footerview;
        return reusableview;
    }
    
    return reusableview;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section; {
    return self.array_feeds.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width-5)/4, (collectionView.frame.size.width-5)/4);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath; {
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.img_photo.layer.borderWidth =0.0f;
    cell.img_photo.layer.cornerRadius =1.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    if (!cell.popupPreviewRecognizer) {
        cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
    }
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.row];
    cell.data = shareObj;
    [cell.img_photo sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:nil];
    
    NSString *strPrice = @"";
    
    if ( shareObj.price.length != 0 )
        strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
    
    if ([strPrice isEqualToString:@"(null)(null)"])
        cell.lbl_price.text = @"";
    else
        cell.lbl_price.text = strPrice;
    
    if ( [cell.lbl_price.text isEqualToString:@""] )
        cell.lbl_price.hidden = YES;
    else
        cell.lbl_price.hidden = NO;
    

    cell.tag = indexPath.row;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.image_detail_viewObj.shareObj =[self.array_feeds objectAtIndex:indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray=self.array_feeds;
    self.image_detail_viewObj.currentSelectedIndex=indexPath.row;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.array_feeds.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:section];
    if (shareObj.location.length != 0) {
        return 41.0f;
    }
    else {
        return 41.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:section];
    UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"strip.png"]];
    strip.alpha = 1.0;
    strip.frame =CGRectMake(0,0,kViewWidth,41);
    
    UIImageView *userimg_bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_photobg.png"]];
    userimg_bg.alpha = 1.0;
    userimg_bg.frame =CGRectMake(5,1,40,40);
    
    UIImageView *img_user =[[UIImageView alloc]initWithFrame:CGRectMake(9, 5, 32, 32)];
    [img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.user_image] placeholderImage:nil];
    img_user.layer.cornerRadius = 16.0;
    img_user.layer.masksToBounds=YES;
    
    EGOImageButton *img_userimage=[[EGOImageButton alloc]initWithFrame:CGRectMake(5,1, 40, 40)];
    img_userimage.layer.cornerRadius = 20.0f;
    img_userimage.layer.masksToBounds=YES;
    
    [img_userimage addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btn_username=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_username setTitle:shareObj.username forState:UIControlStateNormal];
    [btn_username setFrame:CGRectMake(50,0,150,21)];
    [btn_username setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn_username.titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:13.0];
    btn_username.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn_username addTarget:self action:@selector(btn_profile_click:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *img_locationicon =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"category.png"]];
    img_locationicon.frame = CGRectMake(50,27,8,11);
    
    UIButton *btn_location=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_location setTitle:shareObj.location forState:UIControlStateNormal];
    
    btn_location.titleLabel.font = [UIFont fontWithName:@"Myriad Pro" size:13.0];
    [btn_location setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btn_location.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    btn_username.tag = section;
    img_userimage.tag =section;
    btn_location.tag=section;
    
    UIImageView *img_clock =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"timeicon.png"]];
    [img_clock setFrame:CGRectMake(240,10,62,21)];
    
    UILabel *lbl_time =[[UILabel alloc]init];
    lbl_time.text =[self get_time_different:shareObj.datecreated];
    lbl_time.font = [UIFont fontWithName:@"Quicksand" size:10.0];
    lbl_time.textColor =[UIColor grayColor];
    lbl_time.frame =CGRectMake(265, 14, 40, 16);
    lbl_time.backgroundColor =[UIColor clearColor];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
    [view setBackgroundColor:[UIColor clearColor]];
    view.alpha =1.0;
    [view addSubview:strip];
    [view addSubview:userimg_bg];
    [view addSubview:img_user];
    [view addSubview:img_userimage];
    [view addSubview:btn_username];
    [view addSubview:img_clock];
    [view addSubview:btn_location];
    [view addSubview:img_locationicon];
    [view addSubview:lbl_time];
    
    if (shareObj.location.length != 0) {
        [btn_username setFrame:CGRectMake(50,2,150,20)];
        [btn_location setFrame:CGRectMake(63,23,175,20)];
        img_locationicon.hidden=NO;
        strip.frame =CGRectMake(0,0,kViewWidth,46);
    }
    else {
        img_locationicon.hidden=YES;
        strip.frame =CGRectMake(0, 0,kViewWidth, 46);
        [btn_username setFrame:CGRectMake(50,0,150,41)];
    }
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    return [Home_tableview_cell get_tableview_hight:shareObj];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Home_tableview_cell";
    Home_tableview_cell *cell = (Home_tableview_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)	{
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Home_tableview_cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        [cell draw_desc_in_cell];
        [cell draw_like_button_in_cell];
        
        cell.showsReorderControl = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor=[UIColor whiteColor];
        [cell.btn_photo_option addTarget:self action:@selector(btn_photo_option_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_likes_count addTarget:self action:@selector(btn_show_likers_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_like addTarget:self action:@selector(btn_like_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_comment addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_view_all_comments addTarget:self action:@selector(btn_comment_click:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        //Double tap image to LIKE it.
        UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLikeTap:) ];
        [tapImage setNumberOfTapsRequired:2];
        
        [cell.img_big addGestureRecognizer:tapImage];
        
        //
        
        UIFont *highlightFont = [UIFont fontWithName:@"Helvetica" size:12.0];
        
        LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
        [hashStyle addTarget:self action:@selector(hashSelected:)];
        
        LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
        [atStyle addTarget:self action:@selector(atSelected:)];
        
        LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0 green:112.0 blue:112.0 alpha:1.0]];
        [urlStyle addTarget:self action:@selector(urlSelected:)];
        
        
        
        [cell.lbl_desc addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_desc addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_desc addStyle:urlStyle forPrefix:@"Http://"];
        
        [cell.lbl_comments addStyle:hashStyle forPrefix:@"#"];
        [cell.lbl_comments addStyle:atStyle forPrefix:@"@"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"http://"];
        [cell.lbl_comments addStyle:urlStyle forPrefix:@"Http://"];
    }
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:12.0];
    LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    [userStyle addTarget:self action:@selector(userSelected:)];
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:indexPath.section];
    [cell redraw_cell:shareObj andUserStyle:userStyle AtIndexPath:indexPath];
    
    cell.lblavgrating.text=[shareObj avgrating];
    //    cell.lblavgrating.text=  [shareObj avgrating];
    cell.lbltotrating.text=shareObj.totalUser;
    cell.dlstarObj.delegate = self;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Method for Stop Header scrolling in UITableview
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 50;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark Action sheet
-(IBAction)btn_photo_option_click:(id)sender {
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
}

-(void)report_inappropriate:(int )index {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender {
    
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark Go to Profile
-(IBAction)btn_profile_click:(id)sender{
    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld", (long)tag);
    /*
     Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
     user_info_view.user_id=obj.uid;
     [self.navigationController pushViewController:user_info_view animated:YES];
     */
}

#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView == denayAlertViewObj) {
        if(buttonIndex == 0) {
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = SIGNSALTAPIKEY;
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIResponce:) name:@"14243" object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIResponce:) name:@"-14243" object:nil];
            
            NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
            NSLog(@"requestStr:%@",requestStr);
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sig, @"sign",
                                    salt, @"salt",
                                    @"2",@"flag",
                                    [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                    self.profile_share_obj.user_id,@"to_id",nil];
            NSLog(@"params:%@",params);
            AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
            [networkQueue queueItems:requestStr :@"14243" :params];
        }
        
    }
    else {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if ([title isEqualToString:@"Delete"] && alertView.tag == 100)
        {
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = SIGNSALTAPIKEY;
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&id=%@", salt, sig, [self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], self.category.categoryId];
            NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_category.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
            
            [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
                [SVProgressHUD dismiss];
                if ([responseObject[@"success"] isEqualToString:@"1"])
                    [self.navigationController popViewControllerAnimated:YES];
                else
                    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not delete category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
            } failure:^(NSString *errorString) {
                [SVProgressHUD dismiss];
                [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not delete category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
            }];
        }
        
        if (alertView.tag==1) {
            if (buttonIndex!=0) {
                NSLog(@"DELETE");
                [self delete_photo];
            }
        }
        
        if (alertView.tag==2) {
            if (buttonIndex==0) {
                return;
            }
            [self report_inappropriate:(int)buttonIndex];
            
        }
    }
}

#pragma mark Delete Photo
-(void)delete_photo {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-602" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    self.tableViewDataSource.posts = self.array_feeds;// copy];
    self.tableViewDelegate.posts = self.array_feeds;// copy];
    [self.collectionViewObj reloadData];
    [self.tbl_news reloadData];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"602":nil];
    
}

-(void)getdeleteResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
    }
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-603" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    NSLog(@"TAG:%ld",(long)control.tag);
    cellClickIndex=control.tag;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"603":nil];
    
    obj.my_rating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
}

-(void)getRatingResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld",(long)[[dataDict objectForKey:@"rate"] integerValue]];
        
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    //////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

    
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    //[SVProgressHUD show];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-601" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"601":nil];
    
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
//        
//        if ([shareObj.liked isEqualToString:@"no"]) {
//            shareObj.liked=@"yes";
//            int count =[shareObj.likes intValue];
//            count++;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            
//        }else{
//            
//            shareObj.liked=@"no";
//            int count =[shareObj.likes intValue];
//            count--;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//        }
//        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

#pragma mark

// Single tap for fullscreen image
-(IBAction)handleSingleTap:(UITapGestureRecognizer *)gesture {
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    which_image_liked=tappedRow.section;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
    UIImageView *nprfullscreenimg = [[UIImageView alloc] init];
    
    [nprfullscreenimg sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:Nil];
    
    nprfullscreenimg.contentMode = UIViewContentModeScaleToFill;
    
    nprfullscreenimg.frame = CGRectMake(0, 20, kViewWidth, 418);
    
    UIButton *btnclose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnclose setFrame:CGRectMake(290, 30, 26, 26)];
    [btnclose setImage:[UIImage imageNamed:[NSString stringWithFormat:@"closebtn_pop.png"]] forState:UIControlStateNormal];//with image
    [btnclose addTarget:self action:@selector(btnclosefullscreenClick) forControlEvents:UIControlEventTouchUpInside];
    
    imgfullscreenviewobj = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 480)];
    [imgfullscreenviewobj setBackgroundColor:[UIColor blackColor]];
    imgfullscreenviewobj.alpha =1.0;
    
    [self.view addSubview:imgfullscreenviewobj];
    [imgfullscreenviewobj addSubview:nprfullscreenimg];
    [imgfullscreenviewobj addSubview:btnclose];
}

-(IBAction)btnclosefullscreenClick{
    [imgfullscreenviewobj removeFromSuperview];
    [self get_news_feed];
    [self block_user_list];
}

#pragma mark Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture {
    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
    
    which_image_liked=tappedRow.section;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];

    if ([shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    [self.img_like_heart setAlpha:0.0];
    [self.img_like_heart setHidden:NO];
    
    [UIView animateWithDuration:0.7 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7 animations:^{
            [self.img_like_heart setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.img_like_heart setHidden:YES];
        }];
    }];
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-604" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"604":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            // NSLog(@"%@",shareObj.array_liked_by);
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        }
    }
}

#pragma mark
#pragma mark Handler Methods

-(NSString *)tagFromSender:(id)sender {
    return ((UIButton *)sender).titleLabel.text;
}

-(void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

#pragma mark - Show Mapview
-(IBAction)btn_mapview_click:(id)sender{
    self.mapviewObj.array_data =self.array_feeds;
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.mapviewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

-(IBAction)btn_friend_click:(id)sender{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    if ([self.profile_share_obj.is_user_frined isEqualToString:@"no"]||[self.profile_share_obj.is_user_frined isEqualToString:@"request not sent"]) {
        
        [btn_friend_tbl setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
        
        [btn_friend setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
        
        url=@"post_mutual_friend.php";
        
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postMutualFriendAPIResponce:) name:@"14244" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostMutualFriendAPIResponce:) name:@"-14244" object:nil];
        
        NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
        NSLog(@"requestStr:%@",requestStr);
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                self.profile_share_obj.user_id,@"to_id",nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14244" :params];
    }
    else{
        [btn_friend_tbl setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
        
        [self.btn_friend setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
        
        url=@"post_mutual_friend.php";
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIFlag2Responce:) name:@"14246" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIFlag2Responce:) name:@"-14246" object:nil];
        
        NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
        NSLog(@"requestStr:%@",requestStr);
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                @"2",@"flag",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                self.profile_share_obj.user_id,@"to_id",nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14246" :params];
    }
}

-(void)postAcceptRequestAPIFlag2Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    NSLog(@"result:%@",result);
    
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    
    [self get_user_info];
    
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Not Now Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    
}

-(void)FailpostAcceptRequestAPIFlag2Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)postMutualFriendAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        imgRequestSent.hidden = NO;
        imgRequestSentTbl.hidden=NO;
        [self get_user_info];
    }
}

-(void)FailpostMutualFriendAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)postAcceptRequestAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
    }
}

-(void)FailpostAcceptRequestAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - Follow and Following
-(IBAction)btn_follow_click:(id)sender{
    
    NSLog(@"btn_follow_click");
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    if (![self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
        [self.btn_follow setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        url=@"post_user_unfollow.php";
    }else{
        [self.btn_follow setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        url=@"post_user_following.php";
    }
    NSLog(@"%@%@",[[Singleton sharedSingleton] getBaseURL],url);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingAPIResponce:) name:@"14242" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingAPIResponce:) name:@"-14242" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            self.profile_share_obj.user_id,@"following_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14242" :params];
}

-(void)postUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
        
        
        //akshay
        // if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
        // }
    }
}

-(void)FailpostUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please check your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(IBAction)btn_category_follow_click:(id)sender{
    
    NSLog(@"btn_category_follow_click");
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    int currentFollowers = [[totalFollowers stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    
    if (categoryFollowed) {
        [self.btn_category_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
        [self.btn_category_follow_tbl setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
        url=@"post_user_unfollow.php";
        categoryFollowed = NO;
        currentFollowers--;
    }else{
        [self.btn_category_follow setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_category_follow_tbl setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        url=@"post_user_following.php";
        categoryFollowed = YES;
        currentFollowers++;
    }
    NSLog(@"%@%@",[[Singleton sharedSingleton] getBaseURL],url);
    
    fastCategoryFollow = 1;
    
    totalFollowers =[NSString stringWithFormat:@"%d", currentFollowers];
    
    self.category_followers.text = totalFollowers;
    self.category_followers_tbl.text = totalFollowers;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserCategoryFollowingAPIResponce:) name:@"14242" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserCategoryFollowingAPIResponce:) name:@"-14242" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            LOGINEDUSERID, @"uid",
                            [self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], @"following_id",
                            self.category.categoryId, @"category_ids",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14242" :params];
}

-(void)postUserCategoryFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_news_feed];
        
        
        //akshay
        // if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
        // }
    }
}

-(void)FailpostUserCategoryFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}


#pragma Block_User_list
-(void) block_user_list
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBlockUserResponce:) name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailBlockUserResponce:) name:@"-1424" object:nil];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_list.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1424":nil];
}
-(void)DidClicktoBlockUser
{
    if([self.BlockUser_Btn.currentTitle isEqualToString:@"Block User"])
    {
        NSString *buttonTitle = self.BlockUser_Btn.currentTitle;
        if ([buttonTitle length] > 0) {
            Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
        }
        [self get_block_user];
    }
    else if ([self.BlockUser_Btn.currentTitle isEqualToString:@"Unblock User"])
    {
        NSString *buttonTitle = self.BlockUser_Btn.currentTitle;
        
        if ([buttonTitle length] > 0) {
            Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
        }
        [self get_unblock_user];
    }
    
}
-(void)getBlockUserResponce:(NSNotification*)notification
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *tempArray =[result valueForKey:@"data"];
        
        for (BlockUserRecord * obj in tempArray)
        {
            if([self.profile_share_obj.user_id isEqualToString:(NSString *)obj])
            {
                
                NSString *buttonTitle = self.BlockUser_Btn.currentTitle;
                NSLog(@"comparision successful");
                Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
                [self get_unblock_user];
                [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];
                
            }
            
        }
    }
}
-(void)FailBlockUserResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)get_block_user {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1454" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_blockuser_get:) name:@"1454" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1454" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailblockuserReson:) name:@"-1454" object:nil];
    
    //http://www.techintegrity.in/mystyle/get_comments.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&block_user_id=%@&action=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],profile_share_obj.user_id,Block_User_Tittle_Str];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_for_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1454":nil];
}
-(void)response_blockuser_get:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1454" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1454" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
        
        [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"2"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
        
        [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
        
        
    }
    
}
-(void)FailblockuserReson :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)get_unblock_user
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1455" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_unblockuser_get:) name:@"1455" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1455" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailUnblockuserReson:) name:@"-1455" object:nil];
    
    //http://www.techintegrity.in/mystyle/get_comments.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&block_user_id=%@&action=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],profile_share_obj.user_id,Block_User_Tittle_Str];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_for_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1455":nil];
    
}
-(void)response_unblockuser_get:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1455" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1455" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
        [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"2"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
        
        [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];
    }
}

-(void)FailUnblockuserReson :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)DidClickToShare:(id)sender
{
    if([self.user_id isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]] && ![self.category.categoryName isEqualToString:DefaultCategoryName])
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:nil];
        
        [sheet setDestructiveButtonWithTitle:@"Delete Category" block:^{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Warning"
                                      message:@"If you delete this category all the posts inside the category will be moved to the Miscellaneous Category."
                                      delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      otherButtonTitles:@"Delete", nil];
            alertView.tag = 100;
            [alertView show];
        }];
        
        [sheet addButtonWithTitle:@"Edit Category" block:^{
            self.add_category_view.user_id = self.user_id;
            self.add_category_view.category = self.category;
            self.add_category_view.edit = YES;
            [self.navigationController pushViewController:self.add_category_view animated:YES];
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    else
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:nil];
        
        [sheet setDestructiveButtonWithTitle:@"Report Category" block:^{
            
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
    }
    
}
-(void)report_Inappropriate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getreport_InappropriateResponce:) name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Failgetreport_InappropriateResponce:) name:@"-7773" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%@&uid=%@",salt,sig,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_user.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"7773":nil];
}

-(void)getreport_InappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_delete];
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        //        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)Failgetreport_InappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
