//
//  Suggested_friend_list.m
//  My Style
//
//  Created by Tis Macmini on 6/15/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Suggested_friend_list.h"
#import "User_info_ViewController.h"

@interface Suggested_friend_list (){
    User_info_ViewController *user_info_view;
}

@end

@implementation Suggested_friend_list
@synthesize array_friend,tbl_friend,lblheader;
@synthesize activity,lblSpinnerBg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {

    [super viewDidLoad];
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
      self.view.backgroundColor =[UIColor whiteColor];
    self.array_friend =[[NSMutableArray alloc]init];
    user_info_view =[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    // Do any additional setup after loading the view from its nib.
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    [self get_suggested];
    
    
//    if (is_iPhone_5) {
//        tbl_friend.frame=CGRectMake(0,70,320,self.view.frame.size.height - 115);
//    }
//    else {
//        tbl_friend.frame=CGRectMake(0,70,320,self.view.frame.size.height - 25);
//    }
    
}
-(void)get_suggested{
    [SVProgressHUD show];
    //http://www.techintegrity.in/mystyle/get_suggest_user_follow.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&uid=3
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14236" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSuggestUserFollowResponce:) name:@"14236" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14236" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetSuggestUserFollowResponce:) name:@"-14236" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@get_suggest_user_follow.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14236" :params];
}

-(void)getSuggestUserFollowResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14236" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14236" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self.array_friend removeAllObjects];
        for (NSDictionary *dict in [result valueForKey:@"data"]) {
            Suggested_friend_list_share *shareobj =[[Suggested_friend_list_share alloc]init];
            shareobj.bio =[StaticClass urlDecode:[dict valueForKey:@"bio"]];
            shareobj.is_private = [StaticClass urlDecode:[dict valueForKey:@"is_private"]];
            //if ([shareobj.is_private isEqualToString:@"1"])
            //    continue;
            
            NSLog(@"%@",NSStringFromClass([[dict valueForKey:@"images"] class]));
            if ([[dict valueForKey:@"images"] isKindOfClass:[NSArray class]] ) {
                shareobj.image_data =[dict valueForKey:@"images"];
            } else{
                NSLog(@"Stri");
            }
            shareobj.total_img=[dict valueForKey:@"total_images"];
            shareobj.user_img =[StaticClass urlDecode:[dict valueForKey:@"user_image"]];
            shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
            shareobj.is_follow=@"no";
            shareobj.user_id=[dict valueForKey:@"uid"];
            shareobj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
            shareobj.is_user_friend=[StaticClass urlDecode:[dict valueForKey:@"user_friend"]];
            NSLog(@"--------->%@", shareobj.username);
            [self.array_friend addObject:shareobj];
        }
        
    }else{
        [self.array_friend removeAllObjects];
    }
    [self.tbl_friend reloadData];
}

-(void)FailgetSuggestUserFollowResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14236" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14236" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array_friend.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    float height = 55.0f;
    Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:indexPath.row];
    
    if (!([obj.total_img intValue] == 0) && [obj.is_private isEqualToString:@"0"]) {
        height +=105.0f;
    }
    
     CGSize size = [obj.bio sizeWithFont:[UIFont systemFontOfSize:15.0f]
     constrainedToSize:CGSizeMake(250, HUGE_VALL)
     lineBreakMode:NSLineBreakByWordWrapping];
    
    height +=size.height;
    return height +5.0f;
     
    //return 170;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier =@"Suggested_friend_list_cell";
    Suggested_friend_list_cell *cell=(Suggested_friend_list_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Suggested_friend_list_cell" owner:self options:nil];
        cell =[nib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.showsReorderControl = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell draw_collectionview_in_cell];
        [cell.btn_follow addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn_user_img addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.btn_follow.tag = indexPath.row;
    cell.btn_user_img.tag = indexPath.row;
    
    cell.btn_user_img.layer.cornerRadius = 22.0;
    cell.btn_user_img.layer.masksToBounds=YES;
    
    Suggested_friend_list_share *shareobj =[self.array_friend objectAtIndex:indexPath.row];
    cell.lbl_username.text = shareobj.username;
    cell.lbl_name.text = shareobj.name;
    cell.btn_user_img.imageURL =[NSURL URLWithString:shareobj.user_img];
    if ([shareobj.is_follow isEqualToString:@"no"]) {
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
    }else{
        [cell.btn_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
    }
    
    cell.lbl_dec.text = shareobj.bio;
    
    NSLog(@"is_private=%@, lbl_desc:%@, lbl_username:%@, lbl_name:%@", shareobj.is_private, shareobj.bio, shareobj.username, shareobj.name);
    
    CGSize size = [shareobj.bio  sizeWithFont:[UIFont systemFontOfSize:15.0f]
                            constrainedToSize:CGSizeMake(250, HUGE_VALL)
                                lineBreakMode:NSLineBreakByWordWrapping];
    
    if (!([shareobj.total_img intValue] ==0)) {
        if ([shareobj.is_private isEqualToString:@"1"])
        {
            cell.collectionView.frame = CGRectMake(0, 55, kViewWidth, 0);
            cell.lbl_dec.frame = CGRectMake(0,55,kViewWidth,size.height);
            cell.lbl_dec.text = @"this user is private, follow him to see his feed";
        }
        else
        {
            cell.collectionView.frame = CGRectMake(0, 55, kViewWidth, 100);
            [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
        
            cell.lbl_dec.frame = CGRectMake(0,160,kViewWidth,size.height);
        }
        
    }else{
        cell.collectionView.frame = CGRectMake(0, 55, kViewWidth, 0);
        cell.lbl_dec.frame = CGRectMake(0,55,kViewWidth,size.height);
    }
    
    cell.lbl_username.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0];
    cell.lbl_username.textColor =[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    
    cell.lbl_name.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    cell.lbl_name.textColor = [UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];

    cell.lbl_dec.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    cell.lbl_dec.textColor = [UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
       return cell;
}

#pragma mark - UICollectionViewDataSource Methods
-(NSInteger)numberOfSectionsInCollectionView:(Collectionview_delegate *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(Collectionview_delegate *)collectionView numberOfItemsInSection:(NSInteger)section {
    Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:collectionView.index];
    return [obj.total_img integerValue];
}

-(CGSize)collectionView:(Collectionview_delegate *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

-(UICollectionViewCell *)collectionView:(Collectionview_delegate *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"image_collection_cell";
    image_collection_cell *cell = (image_collection_cell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.img_photo.layer.borderWidth =0.0f;
    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
    cell.img_photo.layer.cornerRadius =0.5f;
    cell.img_photo.layer.masksToBounds =YES;
    
    
     Suggested_friend_list_share *obj =[self.array_friend objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.image_data objectAtIndex:indexPath.row];

    [cell.img_photo sd_setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image_path"]]] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
 
    cell.lbl_price.hidden = YES;

    cell.tag = indexPath.row;
    
    return cell;
}

#pragma mark Follow - unfollow
-(IBAction)btn_follow_click:(id)sender{
    UIButton *btn =(UIButton *)sender;
    NSInteger tag =btn.tag;
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    Suggested_friend_list_share *shareobj =[self.array_friend objectAtIndex:tag];

    if ([shareobj.is_follow isEqualToString:@"no"]) {
        shareobj.is_follow =@"yes";
        url=@"post_user_following.php";
    }
    else {
        shareobj.is_follow =@"no";
        url=@"post_user_unfollow.php";
    }
    
    [self.array_friend replaceObjectAtIndex:tag withObject:shareobj];
    [self.tbl_friend reloadData];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14237" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingUnfollowAPIResponce:) name:@"14237" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14237" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingUnfollowAPIResponce:) name:@"-14237" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            shareobj.user_id,@"following_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14237" :params];
}

-(void)postUserFollowingUnfollowAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14237" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14237" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    //akshay
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
            NSMutableArray *tempArray=[[NSMutableArray alloc] init];
            for (NSDictionary *key in tempFollwingDict) {
                NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                [tempArray addObject:str];
            }
            [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
        }
    }
    
    
}

-(void)FailpostUserFollowingUnfollowAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14237" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14237" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)btn_user_info_click:(id)sender{
    Suggested_friend_list_share *shareobj =[self.array_friend objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=shareobj.username;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)stopSpinner {
    activity.hidden=YES;
    [activity stopAnimating];
    [activity removeFromSuperview];
    [lblSpinnerBg removeFromSuperview];
    [self.view setUserInteractionEnabled:YES];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
