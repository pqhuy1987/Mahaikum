//
//  Reset_password_throw_email.h
//  My Style
//
//  Created by Tis Macmini on 6/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#import "StaticClass.h"
#import "AJNotificationView.h"
#import "DejalActivityView.h"
#import "Rest_password_throw_fb.h"

@interface Reset_password_throw_email : UIViewController{

    NSString *str_username;
    NSString *str_email;
    NSString *str_image_url;
    
    UIImageView *img_cell_bg;
    UIImageView *img_image;
    
    UILabel *lbl_username;
    UILabel *lbl_dec;
    Rest_password_throw_fb *reset_fb_viewObj;

    UIButton *btnEmail;
    UIButton *btnFacebook;
}
@property(nonatomic,strong)  Rest_password_throw_fb *reset_fb_viewObj;
@property(nonatomic,strong) NSString *str_username;
@property(nonatomic,strong) NSString *str_email;
@property(nonatomic,strong) NSString *str_image_url;

@property(nonatomic,strong)IBOutlet  UIImageView *img_cell_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_image;
@property(nonatomic,strong)IBOutlet UILabel *lbl_username;
@property(nonatomic,strong)IBOutlet UILabel *lbl_dec;

@property(nonatomic,strong)IBOutlet UIButton *btnEmail;
@property(nonatomic,strong)IBOutlet UIButton *btnFacebook;

@end
