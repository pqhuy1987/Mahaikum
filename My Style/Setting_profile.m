//
//  Setting_profile.m
//  My Style
//
//  Created by Tis Macmini on 5/22/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Setting_profile.h"

NSInteger btnswitchprivateflg = 0;
NSInteger btnswitchphotosflg = 0;

@interface Setting_profile ()

@end

@implementation Setting_profile
@synthesize scrollview,switch_private,swith_photos_save;
@synthesize img_firstcell,img_secondcell,img_thirdcell,img_fourthcell;
@synthesize notification_settingViewObj,find_inviteViewObj,imgswitchprivateoff,imgswitchprivateon,imgswitchphotosoff,imgswitchphotoson,lblheader,lblinformation,lbloriginalphoto,lblprivatephoto,btnnotification,btnphotoliked,btntos,btnfindfriends,lblinformation2,lblinformation3,lblsubheader1,lblsubheader2,lblsubheader3;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:16.0];
    btnfindfriends.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    btnphotoliked.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    lblsubheader1.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    lblprivatephoto.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    btnnotification.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    lblinformation.font = [UIFont fontWithName:@"Myriad Pro" size:12.0];
    lblinformation2.font = [UIFont fontWithName:@"Myriad Pro" size:12.0];
    lblsubheader2.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    lbloriginalphoto.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    lblinformation3.font = [UIFont fontWithName:@"Myriad Pro" size:12.0];
    lblsubheader3.font = [UIFont fontWithName:@"Roboto-Medium" size:12.0];
    btntos.titleLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.scrollview.backgroundColor =[UIColor whiteColor];
    self.notification_settingViewObj= [[Notification_setting_ViewController alloc]initWithNibName:@"Notification_setting_ViewController" bundle:nil];
    self.find_inviteViewObj =[[Find_invite_friend alloc]initWithNibName:@"Find_invite_friend" bundle:nil];
  
    self.img_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_firstcell.layer.cornerRadius =4.0f;

    self.img_secondcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_secondcell.layer.cornerRadius =4.0f;
    
    self.img_thirdcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_thirdcell.layer.cornerRadius =4.0f;
    
    self.img_fourthcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_fourthcell.layer.cornerRadius =4.0f;

    imgswitchprivateon.hidden = YES;
    imgswitchprivateon.hidden = NO;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"0"]) {
        [self.swith_photos_save setOn:NO animated:YES];

    }else{
        [self.swith_photos_save setOn:YES animated:YES];

    }
    
//    self.swith_photos_save.onText = NSLocalizedString(@"ON", @"");
//	self.swith_photos_save.offText = NSLocalizedString(@"OFF", @"");
//    self.swith_photos_save.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    [self.swith_photos_save addTarget:self action:@selector(btn_photo_save_valuechange:) forControlEvents:UIControlEventValueChanged];
    
//    self.switch_private.onText = NSLocalizedString(@"ON", @"");
//	self.switch_private.offText = NSLocalizedString(@"OFF", @"");
//    self.switch_private.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    
    [UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    [self.switch_private addTarget:self action:@selector(btn_private_valuechange:) forControlEvents:UIControlEventValueChanged];
    
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    NSString *strFlg=[StaticClass retrieveFromUserDefaults:@"ISPRIVATEPHOTOORNOT"];
    
    if ([strFlg isEqualToString:@"1"]) {
        imgswitchprivateoff.hidden = YES;
        imgswitchprivateon.hidden = NO;
        btnswitchprivateflg=1;
    }
    else {
        imgswitchprivateoff.hidden = NO;
        imgswitchprivateon.hidden = YES;
        btnswitchprivateflg=0;
    }
    
//    if (is_iPhone_5) {
//        self.scrollview.contentSize=CGSizeMake(kViewWidth, 650);
//        self.scrollview.frame = CGRectMake(0, 62, kViewWidth, 448);
//    } else {
//        self.scrollview.contentSize=CGSizeMake(kViewWidth, 650);
//        self.scrollview.frame = CGRectMake(0, 62, kViewWidth, 360);
//    }
//    
//    [self.scrollview setContentOffset:CGPointMake(0,0) animated:YES];
}

-(IBAction)btn_find_invite_click:(id)sender {
    [self.navigationController pushViewController:self.find_inviteViewObj animated:YES];
}

#pragma mark Logout
-(IBAction)btn_logout_click:(id)sender {
    [StaticClass saveToUserDefaults:@"": @"STOREUSERNAME"];
    [StaticClass saveToUserDefaults:@"": @"STOREPASSWORD"];
    
    [StaticClass saveToUserDefaults:@"0" :USER_IS_LOGIN];
    
    [StaticClass saveToUserDefaults:@"1" :@"USERNOWLOGOUT"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:nil];
}

#pragma mark Push notification setting
-(IBAction)btn_push_notification_btn_click:(id)sender{
    [self.navigationController pushViewController:self.notification_settingViewObj animated:YES];
}

#pragma mark photos you have liked
-(IBAction)btn_photos_you_have_liked:(id)sender{
    Photos_you_liked *obj =[[Photos_you_liked alloc]initWithNibName:@"Photos_you_liked" bundle:nil];
    [self.navigationController pushViewController:obj animated:YES];
    
}

-(IBAction)switchprivatebtnClick:(id)sender {
    if (btnswitchprivateflg == 1) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy off. This means from now on. anyone will be able to follow your photos. Are you sure you want to turn privacy off?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
        [alert show];
    }
    else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy on. This means from now on only people you approve will be able to follow your photos. Are you sure you want to turn privacy on?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
        [alert show];
    }
}

#pragma mark - private switch click
-(IBAction)btn_private_valuechange:(id)sender {
    if (is_private_flag==0) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy on. This means from now on only people you approve will be able to follow your photos. Are you sure you want to turn privacy on?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
        [alert show];
    }
    else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Are you sure?" message:@"You're about to turn privacy off. This means from now on. anyone will be able to follow your photos. Are you sure you want to turn privacy off?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes, I'm sure", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (btnswitchprivateflg == 0) {
        if (buttonIndex == 0) {
            imgswitchprivateoff.hidden = NO;
            imgswitchprivateon.hidden = YES;
            is_private_flag=0;
            btnswitchprivateflg=0;
        }
        else {
            imgswitchprivateoff.hidden = YES;
            imgswitchprivateon.hidden = NO;
            is_private_flag=1;
            btnswitchprivateflg=1;
            [self setPrivatePhotoFlg];
        }
    }
    else {
        if (buttonIndex == 0) {
            imgswitchprivateoff.hidden = YES;
            imgswitchprivateon.hidden = NO;
            btnswitchprivateflg=1;
        }
        else{
            imgswitchprivateoff.hidden = NO;
            imgswitchprivateon.hidden = YES;
            btnswitchprivateflg=0;
            [self setPrivatePhotoFlg];
        }
    }
}


-(void)setPrivatePhotoFlg {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14233" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postPrivateFlagResponce:) name:@"14233" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14233" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostPrivateFlagResponce:) name:@"-14233" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@post_private_flag.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"user_id",
                            [NSString stringWithFormat:@"%ld",(long)btnswitchprivateflg],@"flag"
                            ,nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14233" :params];
}

-(void)postPrivateFlagResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14233" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14233" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [[Singleton sharedSingleton] setCurrent_time:[result valueForKey:@"curr_utc"]];
        NSDictionary *dataDict=[result objectForKey:@"data"];
        if ([dataDict count]>0) {
            NSString *strIsPrivate=[dataDict objectForKey:@"is_private"];
            [StaticClass saveToUserDefaults:strIsPrivate :@"ISPRIVATEPHOTOORNOT"];
        }
    }
}

-(void)FailpostPrivateFlagResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14233" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14233" object:nil];
}

-(IBAction)btn_privacy_policy_click:(id)sender{
    webview_viewcontroller *obj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
   // obj.web_url =@"http://instagram.com/legal/privacy/";
     obj.web_url =[[Singleton sharedSingleton]get_privacy_police_url];
    [self.navigationController pushViewController:obj animated:YES];
}

-(IBAction)btn_terms_of_service_click:(id)sender{
    webview_viewcontroller *obj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
   // obj.web_url =@"http://instagram.com/legal/privacy/";
     obj.web_url =[[Singleton sharedSingleton]get_terms_of_use_url];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - Photo save 

-(IBAction)switchphotosbtnClick:(id)sender{
    if (btnswitchphotosflg == 0) {
        btnswitchphotosflg = 1;
        imgswitchphotoson.hidden = NO;
        imgswitchphotosoff.hidden = YES;
        [StaticClass saveToUserDefaults:@"1" :PHOTOS_SAVE_LIBRARY];
    }
    else{
        btnswitchphotosflg = 0;
        imgswitchphotoson.hidden = YES;
        imgswitchphotosoff.hidden = NO;

        [StaticClass saveToUserDefaults:@"0" :PHOTOS_SAVE_LIBRARY];
    }
}

-(IBAction)btn_photo_save_valuechange:(id)sender{

    if (swith_photos_save.isOn) {
        [StaticClass saveToUserDefaults:@"1" :PHOTOS_SAVE_LIBRARY];
    }else{
        [StaticClass saveToUserDefaults:@"0" :PHOTOS_SAVE_LIBRARY];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
