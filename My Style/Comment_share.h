//
//  Comment_share.h
//  My Style
//
//  Created by Tis Macmini on 4/20/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment_share : NSObject{

    NSString *comment_desc;
    NSString *datecreated;
    NSString *comment_id;
    NSString *image_url;
    NSString *name;
    NSString *uid;
    NSString *username;
    NSString *OwnPost;
    NSString *description;
}
@property(nonatomic,strong) NSString *comment_desc;
@property(nonatomic,strong) NSString *datecreated;
@property(nonatomic,strong) NSString *comment_id;
@property(nonatomic,strong) NSString *image_url;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *OwnPost;
@property(nonatomic,strong) NSString *description;
@end
