//
//  Notification_setting_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "RequestFriend_ViewController.h"
#import "Request_Friend_cell.h"
#import "Profile_share.h"
#import "AJNotificationView.h"

@interface RequestFriend_ViewController ()

@end

@implementation RequestFriend_ViewController

@synthesize tbl_notification,array_reqeust,dict_notification,denayAlertViewObj;
@synthesize tbl_footer,lblheader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.array_reqeust =[[NSMutableArray alloc]init];
    self.dict_notification =[[NSMutableDictionary alloc]init];
    
    self.tbl_notification.tableFooterView=self.tbl_footer;
    
    // Do any additional setup after loading the view from its nib.
}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
//    if (is_iPhone_5) {
//        self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
//        self.tbl_notification.frame = CGRectMake(0,71, 320, 445);
//    }else{
//        self.tbl_notification.frame = CGRectMake(0,71, 320, 360);
//    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_notification_info];
}

-(void)get_notification_info {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMutualFriendResponce:) name:@"14224" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetMutualFriendResponce:) name:@"-14224" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_mutual_friend.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14224" :params];
}

-(void)getMutualFriendResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSString *str=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"str:%@",str);
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    [array_reqeust removeAllObjects];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        if([[result valueForKey:@"success"] isEqualToString:@"1"]) {
            for (int i = 0; i< [dict count]; i++) {
                NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
                Profile_share *tempShare = [[Profile_share alloc] init];
                tempShare.username = [temp objectForKey:@"username"];
                tempShare.user_id = [temp objectForKey:@"id"];
                tempShare.name = [temp objectForKey:@"name"];
                tempShare.image = [temp objectForKey:@"image"];
                [array_reqeust addObject:tempShare];
            }
        }
    }
    [tbl_notification reloadData];
}

-(void)FailgetMutualFriendResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14224" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14224" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_reqeust.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Request_Friend_cell";
	Request_Friend_cell *cell = (Request_Friend_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Request_Friend_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        [cell.btnAccept addTarget:self action:@selector(btnAcceptClick :) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnNotNow addTarget:self action:@selector(btnNotNowClick :) forControlEvents:UIControlEventTouchUpInside];
	}
    cell.btnAccept.tag = indexPath.row;
    cell.btnNotNow.tag = indexPath.row;
    
    Profile_share *tempShare = [array_reqeust objectAtIndex:indexPath.row];
    
    cell.lbl_title.text = tempShare.username ;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{}

-(IBAction)btnAcceptClick :(id)sender {
    UIButton *temp = (UIButton *)sender;
    
    Profile_share *tempShare = [array_reqeust objectAtIndex:temp.tag];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14225" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestResponce:) name:@"14225" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14225" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestResponce:) name:@"-14225" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig,@"sign",
                            salt,@"salt",
                            @"1",@"flag",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"to_id",
                            tempShare.user_id,@"from_id",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14225" :params];
}

-(void)postAcceptRequestResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14225" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14225" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSLog(@"result:%@",result);
    [self get_notification_info];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Accept Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailpostAcceptRequestResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14225" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14225" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        Profile_share *tempShare = [array_reqeust objectAtIndex:alertView.tag];
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14226" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestResponce2:) name:@"14226" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14226" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestResponce2:) name:@"-14226" object:nil];
        
        NSString *requestStr = [NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
        NSLog(@"requestStr:%@",requestStr);
        

        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig,@"sign",
                                salt,@"salt",
                                @"2",@"flag",
                                tempShare.user_id,@"to_id",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14226" :params];
    }
    else {
        
    }
}

-(void)postAcceptRequestResponce2:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14226" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14226" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSLog(@"result:%@",result);
    [self get_notification_info];
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Not Now Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailpostAcceptRequestResponce2:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14226" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14226" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];

}


-(IBAction)btnNotNowClick :(id)sender {
    UIButton *temp = (UIButton *)sender;
    
    denayAlertViewObj = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"are you sure !! you want to reject this request ??" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok",@"cancel", nil];
    
    denayAlertViewObj.tag = temp.tag;
    [denayAlertViewObj show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
