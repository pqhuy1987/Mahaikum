//
//  Notification_setting_share.h
//  My Style
//
//  Created by Tis Macmini on 5/25/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification_setting_share : NSObject{

    NSString *title;
    NSString *is_checked;
    NSString *notification_type;
}
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *is_checked;
@property(nonatomic,strong) NSString *notification_type;
@end
