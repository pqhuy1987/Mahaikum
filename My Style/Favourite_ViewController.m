//
//  Favourite_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Favourite_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"

@interface Favourite_ViewController () {
    
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
}
@end

@implementation Favourite_ViewController
@synthesize array_news_feed,array_follow,tbl_feed,segment,image_detailViewObj,image_detail_viewObj,tbl_news,lbl_temp,lbl_temp2,userStyle1,imgsegment1,imgsegment2,btnreload,find_inviteViewObj;
@synthesize activity,lblSpinnerBg;
@synthesize imgArrow;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refresh_notification_view_data:(NSNotification *)notification {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refresh_notification_view_data" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh_notification_view_data:) name:@"refresh_notification_view_data" object:nil];
    
    [self.tbl_feed setHidden:YES];
    imgArrow.frame=CGRectMake(110,48,15,12);
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    [btnreload addTarget:self action:@selector(btnreloadClick:) forControlEvents:UIControlEventTouchUpInside];

    self.array_follow =[[NSMutableArray alloc]init];
    self.array_news_feed =[[NSMutableArray alloc]init];
    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    self.image_detailViewObj=[[Image_detail_url alloc]initWithNibName:@"Image_detail_url" bundle:nil];
    self.image_detail_viewObj=[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    

    self.lbl_temp = [[LORichTextLabel alloc] initWithWidth:205];
    [self.lbl_temp setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
    
    self.lbl_temp2 = [[LORichTextLabel alloc] initWithWidth:265];
    [self.lbl_temp2 setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
    
    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
    
    self.userStyle1 = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [self.userStyle1 addTarget:self action:@selector(userSelected:)];
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [atStyle addTarget:self action:@selector(atSelected:)];
    
    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
    [urlStyle addTarget:self action:@selector(urlSelected:)];
    [self.lbl_temp addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp addStyle:atStyle forPrefix:@"@"];

    [self.lbl_temp addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"WWW."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Www."];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"https://"];
    [self.lbl_temp addStyle:urlStyle forPrefix:@"Https://"];
    
//    [self.lbl_temp addStyle:urlStyle forPrefix:@"http://"];
//    [self.lbl_temp addStyle:urlStyle forPrefix:@"Http://"];
    
    [self.lbl_temp2 addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_temp2 addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"http://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"www."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"WWW."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Www."];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Http://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"https://"];
    [self.lbl_temp2 addStyle:urlStyle forPrefix:@"Https://"];
    
    is_following = is_news = 0;
    
    self.find_inviteViewObj =[[Find_invite_friend alloc]initWithNibName:@"Find_invite_friend" bundle:nil];
    
//    self.view.layer.shouldRasterize = YES;
    
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(newsGesture:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.tbl_news addGestureRecognizer:gesture];

    UISwipeGestureRecognizer *gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(feedGesture:)];
    [gesture1 setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.tbl_feed addGestureRecognizer:gesture1];
}

- (void)newsGesture:(id)sender
{
    [self.segment setSelectedSegmentIndex:1];
    [self btn_segment_value_change:self];
}

- (void)feedGesture:(id)sender
{
    [self.segment setSelectedSegmentIndex:0];
    [self btn_segment_value_change:self];
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;

//    imgsegment1.hidden = NO;
//    imgsegment2.hidden = YES;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_news_feed];
    [self get_categories_list];
}

-(IBAction)btnreloadClick:(id)sender{
    [self get_news_feed];
    [self get_following_feed];
}

- (void)get_news_feed
{
    [SVProgressHUD show];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_news_feed.php?sign=%@&salt=%@&uid=%@&off=0",[[Singleton sharedSingleton] getBaseURL], sig, salt, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            NSLog(@"There is no news!");
//            [self.tbl_news setHidden:YES];
//            [self.tbl_feed setHidden:YES];
//            [self.lbl_noResult setHidden:NO];
//            [self.btn_find_following setHidden:YES];
//            self.lbl_noResult.text=@"Tab on the camera to add your first post and receive you first activity";
            is_news = 0;
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            [self.array_news_feed removeAllObjects];
            
            //[self.tbl_news setHidden:NO];
//            [self.lbl_noResult setHidden:YES];
//            [self.btn_find_following setHidden:YES];

            is_news = 1;
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict_main in array) {
                
                News_feed_like_Share *shareObj_main =[[News_feed_like_Share alloc]init];
                
                shareObj_main.user_id = [dict_main valueForKey:@"user_id"];
                shareObj_main.owner_id = [dict_main valueForKey:@"owner_id"];
                shareObj_main.username =[StaticClass urlDecode:[dict_main valueForKey:@"username"]];
                
                shareObj_main.feed =[StaticClass urlDecode:[dict_main valueForKey:@"feed"]];
                shareObj_main.userimage =[StaticClass urlDecode:[dict_main valueForKey:@"userimage"]];
                shareObj_main.name =[StaticClass urlDecode:[dict_main valueForKey:@"name"]];
                shareObj_main.image =[StaticClass urlDecode:[dict_main valueForKey:@"image"]];
                shareObj_main.status_date=[StaticClass urlDecode:[dict_main valueForKey:@"status_date"]];
                shareObj_main.read_status=[dict_main valueForKey:@"read_status"];
                shareObj_main.entity_id=[dict_main valueForKey:@"entity_id"];
                
                shareObj_main.userAction=[StaticClass urlDecode:[dict_main valueForKey:@"user_action"]];
                
                NSDictionary *dict =[dict_main valueForKey:@"image_data"];
                
                if ([dict isKindOfClass:[NSDictionary class] ]){
                    Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                    
                    shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                    shareObj.image_id =[dict valueForKey:@"id"];
                    shareObj.image_owner=[dict valueForKey:@"image_owner"];
                    shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                    shareObj.lat =[dict valueForKey:@"lat"];
                    shareObj.lng=[dict valueForKey:@"lng"];
                    shareObj.price = dict[@"price"];
                    shareObj.currency = dict[@"currency"];
                    shareObj.sold = [dict[@"sold"] integerValue];
                    shareObj.likes=[dict valueForKey:@"likes"];
                    shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                    shareObj.uid=[dict valueForKey:@"uid"];
                    shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    
                    shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                    shareObj.category_id = [dict valueForKey:@"category_id"];
                    
                    shareObj.lat=[dict valueForKey:@"lat"];
                    shareObj.lng=[dict valueForKey:@"lng"];
                    
                    shareObj.liked=[dict valueForKey:@"user_liked"];
                    shareObj.comment_count =[dict valueForKey:@"total_comment"];
                    
                    shareObj.array_liked_by=[[NSMutableArray alloc]init];
                    
                    if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                        NSArray *tempArray =[dict valueForKey:@"liked_by"];
                        
                        for (NSDictionary *tempdict in tempArray) {
                            [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                        }
                        
                    }
                    else{
                        // NSLog(@"NSString");
                        
                    }
                    
                    shareObj.array_comments=[[NSMutableArray alloc]init];
                    shareObj.array_comments_tmp=[[NSMutableArray alloc]init];
                    
                    if (![[dict valueForKey:@"description"] isEqualToString:@""])
                    {
                        Comment_share *obj =[[Comment_share alloc]init];
                        obj.uid =shareObj.uid;
                        obj.username =shareObj.username;
                        obj.name =shareObj.name;
                        obj.image_url=shareObj.user_image;
                        obj.comment_desc=shareObj.description;
                        obj.datecreated=shareObj.datecreated;
                        [shareObj.array_comments_tmp addObject:obj];
                    }
                    
                    if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                        NSArray *tempArray =[dict valueForKey:@"comments"];
                        
                        for (NSDictionary *tempdict in tempArray) {
                            Comment_share *obj =[[Comment_share alloc]init];
                            
                            obj.comment_id =[tempdict valueForKey:@"id"];
                            obj.uid =[tempdict valueForKey:@"uid"];
                            obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                            obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                            obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                            obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                            obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                            
                            [shareObj.array_comments addObject:obj];
                            [shareObj.array_comments_tmp addObject:obj];
                            
                        }
                        
                    }else{
                        // NSLog(@"NSString");
                        
                    }
                    
                    shareObj.rating=[dict valueForKey:@"rating"];
                    shareObj.my_rating=[dict valueForKey:@"my_rating"];
                    
                    shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                    shareObj.totalUser=[dict valueForKey:@"tot_user"];
                    
                    shareObj_main.image_data =shareObj;
                    
                    //  [shareObj release];
                }
                [self.array_news_feed addObject:shareObj_main];
            }
            
        }
        
        [self.tbl_news reloadData];
        [self updateNewFeedsReadStatus];
        [self get_following_feed];
    } failure:^(NSString *errorString) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
        [self get_following_feed];
    }];
}

- (void)get_following_feed
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];

    NSString *requestStr = [NSString stringWithFormat:@"%@get_following_feeds.php?sign=%@&salt=%@&uid=%@&off=0",[[Singleton sharedSingleton] getBaseURL], sig, salt, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"] || [[responseObject valueForKey:@"success"] isEqualToString:@"-3"]) {
            NSLog(@"There is no following user!");
//            [self.tbl_feed setHidden:YES];
//            [self.tbl_news setHidden:YES];
//            [self.lbl_noResult setHidden:NO];
//            [self.btn_find_following setHidden:NO];
//             self.lbl_noResult.text=@"Follow people to see there activities";
            is_following = 0;
        }
        else if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            [self.array_follow removeAllObjects];
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            //[self.tbl_feed setHidden:NO];
//            [self.lbl_noResult setHidden:YES];
//            [self.btn_find_following setHidden:YES];
            is_following = 1;
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            for (NSDictionary *dict in array)
            {
                Favourite_news_following_share *shareobj =[[Favourite_news_following_share alloc]init];
                shareobj.user_action =[dict valueForKey:@"user_action"];
                
                //NSLog(@"========>owner_id=%@, user_id=%@, login_id=%@", [StaticClass urlDecode:[dict valueForKey:@"owner_id"]],[StaticClass urlDecode:[dict valueForKey:@"user_id"]],[StaticClass urlDecode:[dict valueForKey:@"login_id"]]);
                
                if ([shareobj.user_action isEqualToString:@"follow"])
                {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    [self.array_follow addObject:shareobj];
                }
                else if ([shareobj.user_action isEqualToString:@"comment"])
                {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.img_url =[StaticClass urlDecode:[dict valueForKey:@"comment_on_image"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"comment_on_username"]];
                    shareobj.image_id =[StaticClass urlDecode:[dict valueForKey:@"image_id"]];
                    [self.array_follow addObject:shareobj];

                }
                else if ([shareobj.user_action isEqualToString:@"follow_category"])
                {
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"following_name"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"category_on_username"]];
                    [self.array_follow addObject:shareobj];
//                    NSLog(@"--------------->%@, %@, %@, %@, %@",shareobj.feed, shareobj.following_name, shareobj.username, shareobj.user_img_url, shareobj.following_name);
                    
                }
                else if ([shareobj.user_action isEqualToString:@"like"]) {
                    
                    shareobj.feed =[StaticClass urlDecode:[dict valueForKey:@"feed"]];
                    shareobj.following_name =[StaticClass urlDecode:[dict valueForKey:@"liked_image_owner"]];
                    shareobj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
                    shareobj.user_img_url =[StaticClass urlDecode:[dict valueForKey:@"userimage"]];
                    shareobj.status_date =[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                    shareobj.img_count =[dict valueForKey:@"image_likes"];
                    
                    
                    shareobj.array_img =[dict valueForKey:@"image"];
                    [self.array_follow addObject:shareobj];
                    
                }
            }
        }
        
        [self.tbl_feed reloadData];
        
        if (self.segment.selectedSegmentIndex==1) {
            imgsegment1.hidden = NO;
            imgsegment2.hidden = YES;
            imgArrow.frame=CGRectMake(192,48,15,12);
            
            self.lbl_noResult.text=@"Follow people to see there activities";
            if (is_following == 0)
            {
                [self.tbl_news setHidden:YES];
                [self.tbl_feed setHidden:YES];
                
                [self.lbl_noResult setHidden:NO];
                [self.btn_find_following setHidden:NO];
            }
            else
            {
                [self.tbl_news setHidden:YES];
                [self.tbl_feed setHidden:NO];
                [self.tbl_feed reloadData];
                
                [self.lbl_noResult setHidden:YES];
                [self.btn_find_following setHidden:YES];
            }
        }
        else{
            imgsegment1.hidden = YES;
            imgsegment2.hidden = NO;
            imgArrow.frame=CGRectMake(110,48,15,12);
            
            [self.btn_find_following setHidden:YES];
            self.lbl_noResult.text=@"Tab on the camera to add your first post and receive you first activity";
            if (is_news == 0)
            {
                [self.tbl_news setHidden:YES];
                [self.tbl_feed setHidden:YES];
                
                [self.lbl_noResult setHidden:NO];
            }
            else
            {
                [self.tbl_news setHidden:NO];
                [self.tbl_feed setHidden:YES];
                [self.tbl_news reloadData];
                
                [self.lbl_noResult setHidden:YES];
            }
        }

        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
        [SVProgressHUD dismiss];
    }];
}

-(IBAction)btn_segment_value_change:(id)sender{
    if (self.segment.selectedSegmentIndex==1) {
        imgsegment1.hidden = NO;
        imgsegment2.hidden = YES;
        imgArrow.frame=CGRectMake(192,48,15,12);

        self.lbl_noResult.text=@"Follow people to see there activities";
        if (is_following == 0)
        {
            [self.tbl_news setHidden:YES];
            [self.tbl_feed setHidden:YES];
            
            [self.lbl_noResult setHidden:NO];
            [self.btn_find_following setHidden:NO];
        }
        else
        {
            [self.tbl_news setHidden:YES];
            [self.tbl_feed setHidden:NO];
            [self.tbl_feed reloadData];
            
            [self.lbl_noResult setHidden:YES];
            [self.btn_find_following setHidden:YES];
        }
    }
    else{
        imgsegment1.hidden = YES;
        imgsegment2.hidden = NO;
        imgArrow.frame=CGRectMake(110,48,15,12);

        [self.btn_find_following setHidden:YES];
         self.lbl_noResult.text=@"Tab on the camera to add your first post and receive you first activity";
        if (is_news == 0)
        {
            [self.tbl_news setHidden:YES];
            [self.tbl_feed setHidden:YES];
            
            [self.lbl_noResult setHidden:NO];
        }
        else
        {
            [self.tbl_news setHidden:NO];
            [self.tbl_feed setHidden:YES];
            [self.tbl_news reloadData];

            [self.lbl_noResult setHidden:YES];
        }
    }
}

#pragma mark UITableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.segment.selectedSegmentIndex==1) {
        return self.array_follow.count;
    }
    else{
        return self.array_news_feed.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.segment.selectedSegmentIndex==1) {
        Favourite_news_following_share *shareobj =[self.array_follow objectAtIndex:indexPath.row];
        if ([shareobj.user_action isEqualToString:@"follow"] || [shareobj.user_action isEqualToString:@"follow_category"]) {

            if ([shareobj.user_action isEqualToString:@"follow"])
                [self.lbl_temp2 setText:[NSString stringWithFormat:@"%@ %@ %@",shareobj.username,shareobj.feed,shareobj.following_name ]];
            else
                [self.lbl_temp2 setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
            
            if (shareobj.following_name.length !=0) {
                [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.following_name];
            }
            [self.lbl_temp2 addStyle:self.userStyle1 forPrefix:shareobj.username];
            
            if (self.lbl_temp2.height <21) {
              //  NSLog(@"row:%d, height:%f",indexPath.row,55.0f+20.0f);
                return 55.0f+20.0f;
                
            }
            else{
               // NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp2.height +34.0f+10.0f+5);
                return self.lbl_temp2.height +34.0f+10.0f+5;
            }
        }
        else if([shareobj.user_action isEqualToString:@"comment"]){
            
            [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
            
            [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];
            

            if (self.lbl_temp.height <29) {
                //NSLog(@"row:%d, height:%d",indexPath.row,55);
                return 55;//  55.0f+15.0f+5;
            }
            else {
               // NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp.height +28.0f+10.0f+5);
                return self.lbl_temp.height +28.0f+10.0f+5;
            }
        }
        else if([shareobj.user_action isEqualToString:@"like"]){
            if ([shareobj.img_count isEqualToString:@"1"]) {
                [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareobj.username,shareobj.feed]];
                [self.lbl_temp addStyle:self.userStyle1 forPrefix:shareobj.username];

                if (self.lbl_temp.height <29) {
                    //NSLog(@"row:%d, height:%d",indexPath.row,55);
                    return 55.0f;
                }
                else {
                    //NSLog(@"row:%d, height:%f",indexPath.row,self.lbl_temp.height +28.0f+5.0f+10.0f+5);
                    return self.lbl_temp.height +28.0f+5.0f+10.0f+5;
                }
            } else {
                int count =(([shareobj.img_count intValue])/3);
                int height =count*105;
                if((([shareobj.img_count intValue])%3) >0) {
                    height+=105;
                }
                
               // NSLog(@"row:%d, height:%f",indexPath.row,55.0f + height+10.0f+5);
                return 55.0f + height+10.0f+5;
            }
        }
       // NSLog(@"row:%d, height:%d",indexPath.row,55);
        return 55.0f;
    }
    else{
        News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];

        if (![shareObj.feed isEqualToString:@"started following you."]) {
            [self.lbl_temp setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            if (self.lbl_temp.height <29) {
                //NSLog(@"row:%d, height:%d",indexPath.row,75);
                return 75.0f;
            }else {
                NSLog(@"row:%ld, height:%f, label height:%f", (long)indexPath.row,self.lbl_temp.height +28.0f+10.0f+15+5,self.lbl_temp.height);
                return self.lbl_temp.height +28.0f+10.0f+15+5;
            }
        }
        //NSLog(@"row:%d, height:%f",indexPath.row,70.0f+5);
        return 70.0f+5;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.segment.selectedSegmentIndex==1) {
        Favourite_news_following_share *shareObj =[self.array_follow objectAtIndex:indexPath.row];
        if ([shareObj.user_action isEqualToString:@"follow"] || [shareObj.user_action isEqualToString:@"follow_category"]) {
            static NSString *identifier =@"News_feed_following_cell";
            News_feed_following_cell *cell=(News_feed_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell draw_in_cell];
            }
            cell.imgRateImage.hidden=YES;
            
            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            // following table username
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            
            [userStyle addTarget:self action:@selector(userSelected:)];
            
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
            if ([shareObj.user_action isEqualToString:@"follow_category"])
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            else
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@ %@",shareObj.username,shareObj.feed,shareObj.following_name]];

            cell.lbl_time.font = [UIFont fontWithName:@"Helvetica" size:10.0];
            
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];

            if (cell.lbl_dec.height <21) {
                cell.lbl_dec.frame = CGRectMake(58,13,265,20);
                cell.lbl_time.frame =CGRectMake(58,33,105,21);
            }
            else {
                cell.lbl_dec.frame = CGRectMake(58,13,265,cell.lbl_dec.height);
                cell.lbl_time.frame =CGRectMake(58,cell.lbl_dec.height+13,105,21);
            }
            return cell;
        }
        else if([shareObj.user_action isEqualToString:@"comment"]){
            static NSString *identifier =@"News_feed_comment_cell";
            News_feed_comment_cell *cell=(News_feed_comment_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_comment_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
                [cell draw_dec_lbl];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                
                
//                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
//                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            }
            
            cell.btn_img1.tag=indexPath.row;
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.feed];

            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
            
            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:shareObj.img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_img.image = image;
                }
            }];
            
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            if (cell.lbl_dec.height <29) {
                cell.lbl_dec.frame = CGRectMake(63,5,205,29);
                cell.lbl_time.frame = CGRectMake(63,34,105,19);
                cell.imgBottomLine.frame=CGRectMake(0,53,kViewWidth,1);
            } else {
                cell.lbl_dec.frame = CGRectMake(63,5,205,cell.lbl_dec.height);
                cell.lbl_time.frame = CGRectMake(63,cell.lbl_dec.height+5, 105, 19);
                cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+5+19,kViewWidth,1);
            }
            return cell;
        }
        else if([shareObj.user_action isEqualToString:@"like"]){
            if ([shareObj.img_count isEqualToString:@"1"]) {
                static NSString *identifier =@"News_feed_like_following_cell";
                News_feed_like_following_cell *cell=(News_feed_like_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (cell==nil) {
                    NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_following_cell" owner:self options:nil];
                    cell =[nib objectAtIndex:0];
                    cell.backgroundColor =[UIColor clearColor];
                    cell.showsReorderControl = NO;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_following_click:) forControlEvents:UIControlEventTouchUpInside];
                    [cell draw_dec_lbl];
                    
                    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                    
                    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    [hashStyle addTarget:self action:@selector(hashSelected:)];
                    
                    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    [atStyle addTarget:self action:@selector(atSelected:)];
                    
                    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    
                    [urlStyle addTarget:self action:@selector(urlSelected:)];
                    [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                    [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                    
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                    
//                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
//                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                }
                
                cell.btn_img1.tag = indexPath.row;
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [userStyle addTarget:self action:@selector(userSelected:)];
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.following_name];
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
                [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil)
                    {
                        cell.btn_user_img.image = image;
                    }
                }];

                cell.btn_user_img.layer.cornerRadius = 22.0;
                cell.btn_user_img.layer.masksToBounds=YES;
                
                for (NSDictionary *dict in shareObj.array_img) {
                    [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (error == nil)
                        {
                            cell.btn_img.image = image;
                        }
                    }];
                }
                cell.lbl_time.text =[self get_time_different:shareObj.status_date];
                
                if (cell.lbl_dec.height <29) {
                    cell.lbl_dec.frame = CGRectMake(63,5,205,29);
                    cell.lbl_time.frame = CGRectMake(63,34,105,19);
                    cell.imgBottomLine.frame=CGRectMake(0,53,kViewWidth,1);
                    
                } else {
                    cell.lbl_dec.frame = CGRectMake(63,5,205,cell.lbl_dec.height);
                    cell.lbl_time.frame = CGRectMake(63,cell.lbl_dec.height+5, 105,19);
                    cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+5+19,kViewWidth,1);
                }
                return cell;
                
            }
            else {
                static NSString *identifier =@"Favourite_feed_following_collection_cell";
                Favourite_feed_following_collection_cell *cell=(Favourite_feed_following_collection_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
                if (cell==nil) {
                    NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"Favourite_feed_following_collection_cell" owner:self options:nil];
                    cell =[nib objectAtIndex:0];
                    cell.backgroundColor =[UIColor clearColor];
                    cell.showsReorderControl = NO;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cell draw_collectionview_in_cell];
                    
                    UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                    
                    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    [hashStyle addTarget:self action:@selector(hashSelected:)];
                    
                    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    [atStyle addTarget:self action:@selector(atSelected:)];
                   
                    LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                    [urlStyle addTarget:self action:@selector(urlSelected:)];
                    
                    [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                    [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                    
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                    
//                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
//                    [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                    
                }
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                
                LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                
                [userStyle addTarget:self action:@selector(userSelected:)];
                [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
                
                [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];
                
                cell.btn_user_img.layer.cornerRadius = 22.0;
                cell.btn_user_img.layer.masksToBounds=YES;
                
                [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.user_img_url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil)
                    {
                        cell.btn_user_img.image = image;
                    }
                }];
                cell.lbl_time.text =[self get_time_different:shareObj.status_date];
                
                int count =(([shareObj.img_count intValue])/3);
                int height =count*105;
                if((([shareObj.img_count intValue])%3) >0){
                    height+=105;
                }
                
                cell.collectionView.frame = CGRectMake(0,55,kViewWidth,height);
                
                cell.imgBottomImage.frame=CGRectMake(0,55+height+5,kViewWidth, 1);
                [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
                cell.collectionView.delegate = self;
                cell.collectionView.dataSource = self;
                return cell;
            }
        }
        return nil;
    }
    else {
        News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:indexPath.row];
        if (![shareObj.feed isEqualToString:@"started following you."]) {
            static NSString *identifier =@"News_feed_like_cell";
            News_feed_like_cell *cell=(News_feed_like_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_like_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btn_img1 addTarget:self action:@selector(btn_image_detail_like_click:) forControlEvents:UIControlEventTouchUpInside];
                [cell draw_dec_lbl];
                
                UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
                
                LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:highlightFont color: [UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [hashStyle addTarget:self action:@selector(hashSelected:)];
                
                LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [atStyle addTarget:self action:@selector(atSelected:)];
                
                LORichTextLabelStyle *urlStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
                [urlStyle addTarget:self action:@selector(urlSelected:)];
                [cell.lbl_dec addStyle:hashStyle forPrefix:@"#"];
                [cell.lbl_dec addStyle:atStyle forPrefix:@"@"];
                
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"WWW."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Www."];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"https://"];
                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Https://"];
                
//                [cell.lbl_dec addStyle:urlStyle forPrefix:@"http://"];
//                [cell.lbl_dec addStyle:urlStyle forPrefix:@"Http://"];
            }
            cell.btn_img1.tag = indexPath.row;
            
            if (![shareObj.userAction isEqualToString:@"rating"]) {
                cell.imgRateImage.hidden=YES;
            }
            else {
                cell.imgRateImage.hidden=NO;
                Home_tableview_data_share *homShr=shareObj.image_data;
                if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                    NSString *imgName=@"0star.png";
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
                else {
                    NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
            }
            
            // news table user
            Home_tableview_data_share *homShr=shareObj.image_data;
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:55.0/255.0 green:100.0/255.0 blue:136.0/255.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];

            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            
            [cell.btn_img sd_setImageWithURL:[NSURL URLWithString:homShr.image_path] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_img.image = image;
                }
            }];
            
           /* cell.btn_user_img.imageURL = [NSURL URLWithString:shareObj.userimage];
            cell.btn_img.imageURL = [NSURL URLWithString:shareObj.image];
            */
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            if (cell.lbl_dec.height <29) {
                cell.lbl_dec.frame = CGRectMake(58,13,205,29);
                cell.lbl_time.frame = CGRectMake(58,33,105,21);
                cell.imgRateImage.frame = CGRectMake(58,33+21+2,70,15);
                cell.imgBottomLine.frame=CGRectMake(0,33+21+2+15,kViewWidth,1);
            }
            else{
                cell.lbl_dec.frame = CGRectMake(58,13,205,cell.lbl_dec.height);
                cell.lbl_time.frame = CGRectMake(58,cell.lbl_dec.height+7, 105, 21);
                cell.imgRateImage.frame = CGRectMake(58,cell.lbl_dec.height+7+21,70,15);
                NSLog(@"indexpath row:%ld", (long)indexPath.row);
                NSLog(@"height:%f label:%f",cell.lbl_dec.height+7+21+15,cell.lbl_dec.height);
                cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+7+21+15,kViewWidth,1);
            }
            
            return cell;
        }
        else{
            static NSString *identifier =@"News_feed_following_cell";
            News_feed_following_cell *cell=(News_feed_following_cell *)[tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell==nil) {
                NSArray *nib=[[NSBundle mainBundle]loadNibNamed:@"News_feed_following_cell" owner:self options:nil];
                cell =[nib objectAtIndex:0];
                cell.backgroundColor =[UIColor clearColor];
                cell.showsReorderControl = NO;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                [cell draw_in_cell];
            }
            
            if (![shareObj.userAction isEqualToString:@"rating"]) {
                cell.imgRateImage.hidden=YES;
            }
            else {
                cell.imgRateImage.hidden=NO;
                Home_tableview_data_share *homShr=shareObj.image_data;
                if ([homShr.my_rating isEqualToString:@""]||homShr.my_rating==nil) {
                    NSString *imgName=@"0star.png";
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
                else {
                    NSString *imgName=[NSString stringWithFormat:@"%@star.png",homShr.my_rating];
                    cell.imgRateImage.image=[UIImage imageNamed:imgName];
                }
            }
            
            UIFont *highlightFont = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
            LORichTextLabelStyle *userStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor colorWithRed:112.0/255.0 green:112.0/255.0 blue:112.0/255.0 alpha:1.0]];
            [userStyle addTarget:self action:@selector(userSelected:)];
            [cell.lbl_dec addStyle:userStyle forPrefix:shareObj.username];
            
            [cell.lbl_dec setText:[NSString stringWithFormat:@"%@ %@",shareObj.username,shareObj.feed]];

            cell.btn_user_img.layer.cornerRadius = 22.0;
            cell.btn_user_img.layer.masksToBounds=YES;
            
            [cell.btn_user_img sd_setImageWithURL:[NSURL URLWithString:shareObj.userimage] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil)
                {
                    cell.btn_user_img.image = image;
                }
            }];
            cell.lbl_time.text =[self get_time_different:shareObj.status_date];
            
            if (cell.lbl_dec.height <21) {
                cell.lbl_dec.frame = CGRectMake(55,5,265,20);
                cell.lbl_time.frame =CGRectMake(58,33,105,21);
                cell.imgRateImage.frame = CGRectMake(53,33+5+21+2,70,15);
                cell.imgBottomLine.frame=CGRectMake(0,33+5+21+2+15,kViewWidth,1);
                
            }
            else{
                cell.lbl_dec.frame = CGRectMake(55,5,265,cell.lbl_dec.height);
                cell.lbl_time.frame =CGRectMake(58,cell.lbl_dec.height+13+5,105,21);
                
                cell.imgRateImage.frame = CGRectMake(53,cell.lbl_dec.height+13+5+21+2,70,15);
                cell.imgBottomLine.frame=CGRectMake(0,cell.lbl_dec.height+13+5+21+2+15,kViewWidth,1);
            }
            return cell;
        }
    }
}

#pragma mark - UICollectionViewDataSource Methods

- (NSInteger)numberOfSectionsInCollectionView:(Collectionview_delegate *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(Collectionview_delegate *)collectionView numberOfItemsInSection:(NSInteger)section {
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    
    return [obj.img_count integerValue];
}

- (CGSize)collectionView:(Collectionview_delegate *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}

-(UICollectionViewCell *)collectionView:(Collectionview_delegate *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"image_collection_cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:10000];
    
    if (imageView == nil)
    {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        imageView.userInteractionEnabled = NO;
        imageView.tag = 10000;
    }
    
//    image_collection_cell *cell = (image_collection_cell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
//    
//    cell.img_photo.layer.borderWidth =1.0f;
//    cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
//    cell.img_photo.layer.cornerRadius =1.5f;
//    cell.img_photo.layer.masksToBounds =YES;
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];
    //NSLog(@"***********>owner_id->%@", [StaticClass urlDecode:[dict valueForKey:@"owner_id"]]);

   // cell.img_photo.imageURL =[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[StaticClass urlDecode:[dict valueForKey:@"image"]]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error == nil)
        {
            @autoreleasepool {
                imageView.image = image;
            }
        }
    }];
    
    // cell.img_photo.image =[UIImage imageNamed:@"test1.png"];
    cell.tag = indexPath.row;
    [cell addSubview:imageView];
    
    return cell;
}

- (void)collectionView:(Collectionview_delegate *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:collectionView.index];
    
    NSDictionary *dict =[obj.array_img objectAtIndex:indexPath.row];

    self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
}

- (void)get_categories_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&login_id=%@", salt, sig, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_category_followers.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"])
        {
            self.array_categories = [[NSMutableArray alloc] init];
            
            NSArray *dataArray = responseObject[@"data"];
            for(NSDictionary *data in dataArray)
            {
                CategoryObject *object = [[CategoryObject alloc] init];
                object.categoryId = data[@"id"];
                object.categoryName = data[@"name"];
                object.categoryDescs = data[@"description"];
                object.categoryFollowers = data[@"followers"];
                object.categoryUserFollower = data[@"user_follower"];
                [self.array_categories addObject:object];
            }
        }
        else {
            [self.array_categories removeAllObjects];
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get categories" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark - Image Detail
-(IBAction)btn_image_detail_like_click:(id)sender {
    
    News_feed_like_Share *shareObj =[self.array_news_feed objectAtIndex:((UIButton *)sender).tag];
    NSLog(@"Action=%@, read_status=%@, userid=%@, ownerid=%@, username=%@, following_name=%@, feed=%@",shareObj.userAction, shareObj.read_status, shareObj.user_id, shareObj.owner_id, shareObj.username, shareObj.name, shareObj.feed);
    
    if ([shareObj.userAction isEqualToString:@"recive_request"])
    {
        return;
    }
    
    if ([shareObj.userAction isEqualToString:@"follow_category"])
    {
        categoryInfoView.user_id = shareObj.owner_id;
        for(NSDictionary *data in self.array_categories)
        {
            CategoryObject *object = (CategoryObject*)data;
            
            if (object == nil)
                continue;
            
            if ([object.categoryId isEqualToString:shareObj.entity_id])
            {
                categoryInfoView.category = object;
                break;
            }
        }

        [self.navigationController pushViewController:categoryInfoView animated:YES];

        return;
    }
    
//    self.image_detail_viewObj.shareObj =shareObj.image_data;
//        
//    self.image_detail_viewObj.arrayFeedArray=self.array_news_feed;
//    self.image_detail_viewObj.currentSelectedIndex=((UIButton *)sender).tag;
//    
//    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];

    self.image_detailViewObj.image_id =shareObj.image_data.image_id;
    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
}

- (void)updateNewFeedsReadStatus
{
    //[SVProgressHUD show];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(update_new_feeds_status:) name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-162" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_update_new_feeds.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"owner_id": [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"162" :params];
}

- (void)update_new_feeds_status:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
    {
        NSLog(@"Update new feeds success!");
        //self.navigationController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%lu", (unsigned long)alert_badge];
        ALTabBarController *tabbar = (ALTabBarController *)self.tabBarController;
        tabbar.badgeView.alertCount = 0;
        tabbar.badgeView.hidden = YES;

    }
    else
        NSLog(@"Can not update new feeds");
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"162" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-162" object:nil];
}

-(IBAction)btn_image_detail_like_following_click:(id)sender {
    Favourite_news_following_share *obj =[self.array_follow objectAtIndex:((UIButton *)sender).tag];
    
    if ([obj.user_action isEqualToString:@"comment"] || [obj.user_action isEqualToString:@"mention"]){
        self.image_detailViewObj.image_id =obj.image_id;
    }
    else{
        NSDictionary *dict =[obj.array_img objectAtIndex:0];
        self.image_detailViewObj.image_id =[dict valueForKey:@"image_id"];
    }

    [self.navigationController pushViewController:self.image_detailViewObj animated:YES];
    
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring {
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"just now"];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.f minutes ago",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.f hours ago",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.f days ago",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.f months ago",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.f year ago",diff/(3600*24*30*12)];
    }
}

#pragma mark Handler Methods

-(NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

-(void)hashSelected:(id)sender {
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)atSelected:(id)sender {
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)userSelected:(id)sender {
    user_info_view.user_id=[[self tagFromSender:sender] stringByReplacingOccurrencesOfString:@"'s" withString:@""];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)find_following:(id)sender {
   [self.navigationController pushViewController:self.find_inviteViewObj animated:YES];
}

@end
