//
//  GoogleMap_ViewController.m
//  Mahalkum
//
//  Created by user on 6/26/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import <Foundation/Foundation.h>


#import "GoogleMap_ViewController.h"
#import "OpenInGoogleMapsController.h"
#import "Enums.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MDDirectionService.h"
#import "MJCollectionViewCell.h"

@interface GoogleMap_ViewController ()<GMSMapViewDelegate>
{
    CLLocationCoordinate2D  startlocation;
    CLLocationCoordinate2D destinationlocation;
    GMSMapView  *googlemapview;
}

@end

@implementation GoogleMap_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setPlaceMark:self.str_latitude loStr:self.str_longitude];
    
    self.lbl_username.text = self.str_name;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setPlaceMark:self.str_latitude loStr:self.str_longitude];
}

#pragma mark Google Map Delegate

- (void)setPlaceMark:(NSString *)laStr loStr:(NSString *)loStr
{
    CGFloat latitude = [laStr floatValue];
    CGFloat longitude = [loStr floatValue];
    
    //    self.mapView.mapType = MKMapTypeStandard;
    //    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latitude, longitude);
    //    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
    //    MKCoordinateRegion regionToDisplay = MKCoordinateRegionMake(center, span);
    //    [self.mapView setRegion:regionToDisplay];
    //
    //    MyAnnotation *ann = [[MyAnnotation alloc] init];
    //    ann.title = self.profile_share_obj.username;
    //    ann.coordinate = center;
    //    [self.mapView addAnnotation:ann];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:13];
    GMSMapView *view = [GMSMapView mapWithFrame:CGRectMake(0, 0, kViewWidth, kViewHeight) camera:camera];
    //    view.myLocationEnabled = YES;
    view.delegate = self;
//    [self.mapView removeSubviews];
    [self.mapView addSubview:view];
    CLLocationCoordinate2D  position = CLLocationCoordinate2DMake(latitude, longitude);
    destinationlocation = position;
    GMSMarker   *marker = [GMSMarker markerWithPosition:position];
    marker.title = [NSString stringWithFormat:@"%@ >", self.str_name];

    marker.infoWindowAnchor = CGPointMake(0.5, 0.0);
    marker.map = view;
    //    marker.userData = marker;
    googlemapview = view;
}

- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidStartTileRendering");
}

- (void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidFinishTileRendering");
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    //    startlocation = googlemapview.myLocation.coordinate;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    startlocation = delegate.locationManager.location.coordinate;
    
    GMSMarker   *marker1 = [GMSMarker markerWithPosition:startlocation];
    marker1.title = @"Current Location";
    marker1.infoWindowAnchor = CGPointMake(0.5, 0.0);
    marker1.map = googlemapview;
    //    marker1.userData = marker1;
    marker1.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    bounds = [bounds includingCoordinate:destinationlocation];
    bounds = [bounds includingCoordinate:startlocation];
    
    [googlemapview animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    
    //    GMSMutablePath *path = [GMSMutablePath path];
    //    [path addCoordinate:googlemapview.myLocation.coordinate];
    //    [path addCoordinate:destinationlocation];
    //
    //    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
    //    rectangle.strokeWidth = 2.f;
    //    rectangle.map = googlemapview;
    
    NSMutableArray *waypointStrings = [NSMutableArray array];
    NSString *startPositionString = [NSString stringWithFormat:@"%f,%f",startlocation.latitude,startlocation.longitude];
    [waypointStrings addObject:startPositionString];
    NSString *endPositionString = [NSString stringWithFormat:@"%f,%f",destinationlocation.latitude,destinationlocation.longitude];
    [waypointStrings addObject:endPositionString];
    
    NSLog(@"Map start=%@, end=%@", startPositionString, endPositionString);
    MDDirectionService *mds = [[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    NSDictionary *query = @{ @"sensor" : @"false",
                             @"waypoints" : waypointStrings };
    
    [mds setDirectionsQuery:query
               withSelector:selector
               withDelegate:self];
    
    
    return NO;
}

-(void)addDirections:(NSDictionary *)json{
    if ( [json[@"routes"] count] > 0 )
    {
        NSDictionary *routes = json[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 2.f;
        polyline.map = googlemapview;
    }
}


- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSLog(@"didTapInfoWindowOfMarker");
    [self openDirectionsInGoogleMaps];
}

- (void)openDirectionsInGoogleMaps
{
    NSLog(@"openDirectionsInGoogleMaps");
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    
    //    GoogleDirectionsWaypoint *start = [[GoogleDirectionsWaypoint alloc] init];
    //    start.queryString = @"";
    //    start.location = startlocation;
    //    directionsDefinition.startingPoint = start;
    
    directionsDefinition.startingPoint = nil;
    
    GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
    destination.queryString = @"";
    destination.location = destinationlocation;
    directionsDefinition.destinationPoint = destination;
    
    directionsDefinition.travelMode = kTravelModeDriving;
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}

- (IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btn_dirction_clicked:(id)sender {
    NSLog(@"btn_direction_click");
    [self openDirectionsInGoogleMaps];
}
@end
