//
//  Notification_setting_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Notification_setting_cell : UITableViewCell{

    UILabel *lbl_title;
    UIImageView *img_checkmark;
    UIImageView *img_bg;
}
- (void)addShadowToCellInTableView:(UITableView *)tableView
                       atIndexPath:(NSIndexPath *)indexPath;

@property(nonatomic,strong)IBOutlet UILabel *lbl_title;
@property(nonatomic,strong)IBOutlet UIImageView *img_checkmark;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg;
@end
