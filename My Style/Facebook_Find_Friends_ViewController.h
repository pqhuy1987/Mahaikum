//
//  Facebook_Find_Friends_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "Facebook_follow_frnd_list.h"
#import "Contact_list_friend.h"
#import "Suggested_friend_list_reg.h"

@interface Facebook_Find_Friends_ViewController : UIViewController{

    UIView *view_find_friend;
    UITableView *tbl_friend;
    NSMutableArray *array_friend;
    NSMutableArray *array_frnd;
    
    UIImageView *img_cell_bg;
    
    UIButton *btnContact,*btnFacebook;
    
    Suggested_friend_list_reg *suggested_viewObj;
}

@property(nonatomic,strong) Suggested_friend_list_reg *suggested_viewObj;
@property(nonatomic,strong) NSMutableArray *array_frnd;
@property(nonatomic,strong) NSMutableArray *array_friend;
@property(nonatomic,strong) IBOutlet UIView *view_find_friend;
@property(nonatomic,strong) IBOutlet UITableView *tbl_friend;
@property(nonatomic,strong) IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,strong) IBOutlet UIButton *btnContact,*btnFacebook;
@end
