//
//  ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/8/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sign_ViewController.h"

@interface ViewController : UIViewController{

    UIImageView *img_bg;
    UIImageView *img_logo;
    Sign_ViewController *sign_in_ViewObj;
    
}

@property(nonatomic,strong)IBOutlet UIImageView *img_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_logo;

@end
