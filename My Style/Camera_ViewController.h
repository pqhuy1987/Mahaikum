//
//  Camera_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/16/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
//#import "AFPhotoEditorController.h"
#import "Share_photo_ViewController.h"
#import "aCameraMoreViewController.h"

@interface Camera_ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{

    UIButton *btn_camera;
    UIButton *btn_laibray;
    UIImagePickerController *photoPickerController;
    UIImage *img_photo;
    
    Share_photo_ViewController *share_photo_viewObj;
    aCameraMoreViewController *image_effect_viewObj;
}
@property(nonatomic,strong) aCameraMoreViewController *image_effect_viewObj;
@property(nonatomic,strong) Share_photo_ViewController *share_photo_viewObj;
@property(nonatomic,strong) UIImage *img_photo;
@property(nonatomic,strong) IBOutlet UIButton *btn_camera;
@property(nonatomic,strong) IBOutlet UIButton *btn_laibray;

//@property(nonatomic, retain) AFPhotoEditorController *editorViewController;
@property(nonatomic,strong)UIImagePickerController *photoPickerController;


@end
