//
//  Singleton.m
//  NinjaDrop
//
//  Created by Piyush on 1/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton
@synthesize strdbpath;
@synthesize fbID;
@synthesize DBPath;
@synthesize currentLat,currentLong;
@synthesize current_time;
@synthesize locationname,location_lat,location_lng;
@synthesize imgCaptured;
@synthesize dictFollowing;

+(Singleton *) sharedSingleton {
	static Singleton* theInstance = nil;
	if (theInstance == nil) {
		theInstance = [[self alloc] init];
	}
	return theInstance;
}
-(NSString *)get_current_time{
    return self.current_time;
}
- (CGFloat) getCurrentLat {
    return currentLat;
}

- (CGFloat) getCurrentLong {
    return currentLong;
}

- (NSString *) getBucketName {
    return @"tstbuckets";
}

- (NSString *) getBaseURL {
    return @"http://178.62.125.207/api/";
}

-(NSString *)getDBPath {
	return self.strdbpath;
}

- (NSString *) getFbID
{
	return self.fbID;
}

-(NSString *)get_location_name{
    return self.locationname;
}
-(NSString *)get_location_lat{
    return self.location_lat;
}
-(NSString *)get_location_lng{
    return self.location_lng;
}
-(NSString *)get_terms_of_use_url{
    NSString *path =[[NSBundle mainBundle]pathForResource:@"term_use" ofType:@"txt"];
    NSURL *url =[NSURL fileURLWithPath:path];
    return [NSString stringWithFormat:@"%@",url];
}

-(NSString *)get_privacy_police_url{
    NSString *path =[[NSBundle mainBundle]pathForResource:@"policy" ofType:@"txt"];
    NSURL *url =[NSURL fileURLWithPath:path];
    return [NSString stringWithFormat:@"%@",url];
}

-(void)setCurrentCapturedImage:(UIImage *)imgs {
    self.imgCaptured=imgs;
}

-(UIImage *)getCurrentCapturedImage {
    return self.imgCaptured;
}

-(void)setCurrentDictFollowing:(NSMutableArray *)dictTemp {
    dictFollowing=dictTemp;
}

-(NSMutableArray *)getCurrentDictFollowing {
    return dictFollowing;
}

@end
