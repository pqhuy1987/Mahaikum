//
//  NSString+Encoding.h
//  EagleEye
//
//  Created by Tech Integrity on 23/01/13.
//  Copyright (c) 2013 Tech Integrity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encoding)

- (NSString *)urlencode;
@end



