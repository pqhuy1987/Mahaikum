//
//  Singleton.h
//  NinjaDrop
//
//  Created by Piyush on 1/4/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SHOW_INTRO_STATUS @"show_intro_status"
#define SHOW_SIGN_IN_FIRSTTIME @"show_sign_in_firsttime"
#define USERNAME @"username"
#define API_TOKEN @"api_token"
#define USER_ID @"user_id"
#define FB_LOGIN @"facebook_login"
#define FACEBOOK_ACCESS_TOKEN @"facebook_accessToken"
#define FACEBOOK_TIMELINE_POST @"facebook_timeline_post"
#define BY_FACEBOOK_LOGIN @"by_facebook_login"
#define FACEBOOK_ID @"facebook_ID"
#define USER_IMAGE @"user_image"
#define USER_NAME @"user_name"

#define USER_IS_LOGIN @"user_is_login"

#define PHOTOS_PRIVATE @"photos_private"
#define PHOTOS_SAVE_LIBRARY @"photos_save_library"
#define IS_HOME_PAGE_REFRESH @"is_home_page_refresh"

#define SIGNSALTAPIKEY @"tattoogram"

@interface Singleton : NSObject{
    NSString *strdbpath;
    NSString *fbID;
    NSString *DBPath;
    
    CGFloat currentLat;
    CGFloat currentLong;
    
    NSString *current_time;
    
    NSString *locationname;
    NSString *location_lat;
    NSString *location_lng;
    UIImage *imgCaptured;
    NSMutableArray *dictFollowing;
}

@property(nonatomic,strong) NSMutableArray *dictFollowing;
-(void)setCurrentDictFollowing:(NSMutableArray *)dictTemp;
-(NSMutableArray *)getCurrentDictFollowing;

@property(nonatomic,strong) UIImage *imgCaptured;
-(void)setCurrentCapturedImage:(UIImage *)imgs;
-(UIImage *)getCurrentCapturedImage;

@property(nonatomic,strong)NSString *current_time;
-(NSString *)get_current_time;

@property (nonatomic, assign) CGFloat currentLat;
@property (nonatomic, assign) CGFloat currentLong;
- (CGFloat) getCurrentLat;
- (CGFloat) getCurrentLong;

@property(nonatomic,strong)NSString *DBPath;
-(NSString *)getDBPath;

@property(nonatomic,strong)NSString *fbID;
@property (nonatomic, strong) NSString *strdbpath;
+(Singleton *) sharedSingleton;
- (NSString *) getBaseURL;
- (NSString *) getFbID;

@property(nonatomic,strong) NSString *locationname;
@property(nonatomic,strong)NSString *location_lat;
@property(nonatomic,strong)NSString *location_lng;

-(NSString *)get_location_name;
-(NSString *)get_location_lat;
-(NSString *)get_location_lng;
-(NSString *)getBucketName;
-(NSString *)get_terms_of_use_url;
-(NSString *)get_privacy_police_url;

@end
