//
//  LORichTextLabelStyle.m
//  RichTextLabel
//
//  Created by Locassa on 19/06/2011.
//  Copyright 2011 Locassa Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LORichTextLabelStyle : NSObject {
	UIFont *font;
	UIColor *color;	
	id __weak target;
	SEL action;
}

@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *color;
@property (weak, nonatomic, readonly) id target;
@property (nonatomic, readonly) SEL action;

+ (LORichTextLabelStyle *)styleWithFont:(UIFont *)aFont color:(UIColor *)aColor;
- (void)addTarget:(id)target action:(SEL)action;

@end
