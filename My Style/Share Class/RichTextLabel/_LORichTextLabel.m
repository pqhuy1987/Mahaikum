//
//  LORichTextLabel.m
//  RichTextLabel
//
//  Created by Locassa on 19/06/2011.
//  Copyright 2011 Locassa Ltd. All rights reserved.
//

#import "LORichTextLabel.h"
#import "UIView+Layout.h"


@implementation LORichTextLabel

- (id)initWithWidth:(CGFloat)aWidth {
	self = [super initWithFrame:CGRectMake(0.0, 0.0, aWidth, 0.0)];
	
	if(self != nil) {
		highlightStyles = [[NSMutableDictionary alloc] init];
		elements = [[NSMutableArray alloc] init];		

		font = [UIFont fontWithName:@"Helvetica" size:12.0];
		
		textColor = [UIColor blackColor];
	}
	
	return self;
}


#pragma mark -
#pragma mark Mutators

- (void)addStyle:(LORichTextLabelStyle *)aStyle forPrefix:(NSString *)aPrefix {	
	if((aPrefix == nil) || (aPrefix.length == 0)) {
		/*[NSException raise:NSInternalInconsistencyException
				format:@"Prefix must be specified in %@", NSStringFromSelector(_cmd)];
        */
        return;
	}
	
	[highlightStyles setObject:aStyle forKey:aPrefix];
}

- (void)setFont:(UIFont *)value {
	if([font isEqual:value]) {
		return;
	}
	
	font = value;
	
	[self setNeedsDisplay];	
}

- (void)setTextColor:(UIColor *)value {
	if([textColor isEqual:value]) {
		return;
	}
	
	textColor = value;
	
	[self setNeedsLayout];
}

- (void)setText:(NSString *)value {
    elements = [NSMutableArray array];
       
    
    while ([value rangeOfString:@"  "].location != NSNotFound) {
        value = [value stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }

    for (NSString *element in [value componentsSeparatedByString:@" "])
    {
        // NSLog(@"%@",element);
        if ([element rangeOfString:@"\n"].location != NSNotFound)
        {
            NSArray *temp = [element componentsSeparatedByString:@"\n"];
            for (NSString *tempEl in temp)
            {
                [elements addObject: ([tempEl length] == 1 ? @"\n" : tempEl)];
            }
        }
        else
        {
            [elements addObject:[NSString stringWithString:element]];
        }
    }
    if (elements.count>0) {
        if ([[elements objectAtIndex:0] isEqualToString:@""]) {
            [elements removeObjectAtIndex:0];
        }
    }
   	[self removeSubviews];
	
	NSUInteger maxHeight = 999999;
	CGPoint position = CGPointZero;
	CGSize measureSize = CGSizeMake(self.size.width, maxHeight);
	
	for(NSString *element in elements) {
		LORichTextLabelStyle *style = nil;
		
		// Find suitable style
		for(NSString *prefix in [highlightStyles allKeys]) {
			if([element hasPrefix:prefix]) {
				style = [highlightStyles objectForKey:prefix];
				break;
			}
		}
        
		UIFont *styleFont = style.font == nil ? font : style.font;
		UIColor *styleColor = style.color == nil ? textColor : style.color;
		
		// Get size of content (check current line before starting new one)
		CGSize remainingSize = CGSizeMake(measureSize.width - position.x, maxHeight);
		CGSize singleLineSize = CGSizeMake(remainingSize.width, 0.0);
		
		CGSize controlSize = [element sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
        //   CGSize controlSize = [element sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
		CGSize elementSize = [element sizeWithFont:styleFont constrainedToSize:remainingSize];
        
        
        
        if(elementSize.height > controlSize.height || elementSize.width == 0) {
            
            if (controlSize.width==0.0) {
                CGSize controlSize1 = [@"test" sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
                controlSize.height=controlSize1.height;
            }
            //  position.y += controlSize.height;
            if (isiPhone) {
                position.y += 12.0f;//9.0f
                position.x = 0.0;
            }else{
                position.y += 18.0f;
                position.x = 0.0;
            }
            
        }
		elementSize = [element sizeWithFont:styleFont constrainedToSize:measureSize];
        
        
		CGRect elementFrame = CGRectMake(position.x, position.y, elementSize.width, elementSize.height);
		
		// Add button or label depending on whether we have a target
		if(style.target != nil) {
			UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
			[button addTarget:style.target action:style.action forControlEvents:UIControlEventTouchUpInside];
			[button setTitle:element forState:UIControlStateNormal];
			[button setTitleColor:styleColor forState:UIControlStateNormal];
			[button setFrame:elementFrame];
			[button.titleLabel setFont:styleFont];
			[self addSubview:button];
		} else {
			UILabel *label = [[UILabel alloc] initWithFrame:elementFrame];
			[label setBackgroundColor:[UIColor clearColor]];
			[label setNumberOfLines:maxHeight];
			[label setFont:styleFont];
			[label setTextColor:styleColor];
			[label setText:element];
           

            
			[self addSubview:label];
		}
		
	
        CGSize spaceSize = elementSize.width == 0 ? (CGSize) {0,0} : [@" " sizeWithFont:styleFont];
        position.x += elementSize.width + spaceSize.width;
        
        
	}
    
    CGSize controlSize1 = [@"test" sizeWithFont:font constrainedToSize:CGSizeMake(self.size.width, maxHeight) lineBreakMode:NSLineBreakByWordWrapping];
    position.y += controlSize1.height;
    _height= position.y;
	[self setSize:CGSizeMake(self.size.width, position.y)];
    
    
	[self setNeedsLayout];
}

#pragma mark -
#pragma mark Drawing Methods

- (void)layoutSubviews {
	[self removeSubviews];
	
	NSUInteger maxHeight = 999999;
	CGPoint position = CGPointZero;
	CGSize measureSize = CGSizeMake(self.size.width, maxHeight);
	
	for(NSString *element in elements) {
		LORichTextLabelStyle *style = nil;
		
		// Find suitable style
		for(NSString *prefix in [highlightStyles allKeys]) {
			if([element hasPrefix:prefix]) {
				style = [highlightStyles objectForKey:prefix];
				break;
			}
		}
			
		UIFont *styleFont = style.font == nil ? font : style.font;
		UIColor *styleColor = style.color == nil ? textColor : style.color;
		
		// Get size of content (check current line before starting new one)
		CGSize remainingSize = CGSizeMake(measureSize.width - position.x, maxHeight);
		CGSize singleLineSize = CGSizeMake(remainingSize.width, 0.0);
		
		CGSize controlSize = [element sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
     //   CGSize controlSize = [element sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
		CGSize elementSize = [element sizeWithFont:styleFont constrainedToSize:remainingSize]; 		


       
        if(elementSize.height > controlSize.height || elementSize.width == 0) {

            if (controlSize.width==0.0) {
                CGSize controlSize1 = [@"test" sizeWithFont:styleFont constrainedToSize:singleLineSize lineBreakMode:NSLineBreakByWordWrapping];
                controlSize.height=controlSize1.height;
            }
          //  position.y += controlSize.height;
            if (isiPhone) {
                position.y += 12.0f;
                position.x = 0.0;
            }else{
                position.y += 18.0f;
                position.x = 0.0;
            }

        }
		elementSize = [element sizeWithFont:styleFont constrainedToSize:measureSize];
//        NSLog(@"%@",element);
//        NSLog(@"%f",position.y);
//        NSLog(@"%f",position.x);
    //    elementSize =[element sizeWithFont:styleFont constrainedToSize:CGSizeMake(self.size.width, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
        
		CGRect elementFrame = CGRectMake(position.x, position.y, elementSize.width, elementSize.height);
		
		// Add button or label depending on whether we have a target
		if(style.target != nil) {
			UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
			[button addTarget:style.target action:style.action forControlEvents:UIControlEventTouchUpInside];
			[button setTitle:element forState:UIControlStateNormal];
			[button setTitleColor:styleColor forState:UIControlStateNormal];
			[button setFrame:elementFrame];
			[button.titleLabel setFont:styleFont];
			[self addSubview:button];			
		} else {
			UILabel *label = [[UILabel alloc] initWithFrame:elementFrame];
			[label setBackgroundColor:[UIColor clearColor]];
			[label setNumberOfLines:maxHeight];
			[label setFont:styleFont];
			[label setTextColor:styleColor];
			[label setText:element];
			[self addSubview:label];
		}
		
		//CGSize spaceSize = [@" " sizeWithFont:styleFont];
		//position.x += elementSize.width + spaceSize.width;
		
        CGSize spaceSize = elementSize.width == 0 ? (CGSize) {0,0} : [@" " sizeWithFont:styleFont];
        position.x += elementSize.width + spaceSize.width;
        
        /*
		if([element isEqual:[elements lastObject]]) {
			position.y += controlSize.height;	
		}
         */
     //   NSUInteger index = [elements indexOfObject:element];
    //    NSLog(@"%d",index);
	}
    
    CGSize controlSize1 = [@"test" sizeWithFont:font constrainedToSize:CGSizeMake(self.size.width, maxHeight) lineBreakMode:NSLineBreakByWordWrapping];
    position.y += controlSize1.height;
    _height= position.y;
	[self setSize:CGSizeMake(self.size.width, position.y)];

}

@end
