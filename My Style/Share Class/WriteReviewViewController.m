//
//  WriteReviewViewController.m
//  
//
//  Created by Charles Moon on 1/27/16.
//
//

#import "WriteReviewViewController.h"

@interface WriteReviewViewController ()
{
    HCSStarRatingView *starRatingView;
}

@end

@implementation WriteReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewTitle.text = self.profile_share_obj.username;
    
    starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.ratingView.frame.size.width, self.ratingView.frame.size.height)];
    starRatingView.opaque = NO;
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 1;
    starRatingView.value = 0;
    starRatingView.tintColor = [GlobalDefine colorWithHexString:@"1fae38"];
    //    [starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.ratingView addSubview:starRatingView];
    self.ratingView.backgroundColor = [UIColor clearColor];
}

- (IBAction)backButtonClicked:(id)sender
{
    [self back];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitButtonClicked:(id)sender
{
    [self postReview];
}

- (void)postReview
{
    [SVProgressHUD show];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"70" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(postReviewResponse:) name:@"70" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-70" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-70" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@", salt, sig, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_reviews.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.profile_share_obj.user_id, @"toid",
                            self.reviewDescription.text, @"descs",
                            @(starRatingView.value), @"rating",
                            nil];
    [networkQueue queueItems :requestStr:@"70":params];
    
    NSLog(@"params:%@",params);
}

- (void)postReviewResponse:(NSNotification *)notification
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"70" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-70" object:nil];
    [SVProgressHUD dismiss];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
        [self back];
    else
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not add category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"70" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-70" object:nil];
    [SVProgressHUD dismiss];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not add category" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

@end
