//
//  AFNetworkingQueue.h
//  SampleProject
//
//  Created by Rupen Makhecha on 19/01/13.
//
//

#import <Foundation/Foundation.h>

@interface AFNetworkingQueue : NSObject {
    NSOperationQueue *queue;    
}

@property(nonatomic,retain) NSOperationQueue *queue;

+(AFNetworkingQueue*) sharedSingleton;
-(void) queueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)deleteQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)getQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)putQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)uploadImageAndData :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;

-(void)cancelAllQueueOperation;

@end
