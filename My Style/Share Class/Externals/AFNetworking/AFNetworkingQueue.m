//
//  AFNetworkingQueue.m
//  SampleProject
//
//  Created by Rupen Makhecha on 19/01/13.
//
//

#import "AFNetworkingQueue.h"

@implementation AFNetworkingQueue
@synthesize queue;

+(AFNetworkingQueue*) sharedSingleton {
	static AFNetworkingQueue* theInstance = nil;
	if (theInstance == nil) 	{
		theInstance = [[self alloc] init];        
        [theInstance initQueue];
	}
	return theInstance;
}

-(void)initQueue {
    self.queue = [[[NSOperationQueue alloc] init] autorelease];
}

-(void)queueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
        
    }];
    
   // [manager.operationQueue addOperation:<#(NSOperation *)#>]
    
    /*
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
   
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"POST" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:requesting] autorelease];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
       // NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
   // [operation start];
    
    [self.queue addOperation:operation];
    [httpClient release];
    */
}

-(void)putQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager PUT:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    /*
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"PUT" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:requesting] autorelease];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
        //NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
    [httpClient release];
    */
}

-(void)deleteQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager DELETE:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    /*
    
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"DELETE" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:requesting] autorelease];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
        //NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
    [httpClient release];
    */
}

-(void)getQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    
    [manager GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject:%@",responseObject);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    /*
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"GET" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:requesting] autorelease];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       // NSString *response = [operation responseString];
       // NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
    [httpClient release];
    */
}

-(void)uploadImageAndData :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    
    
//    AFHTTPRequestOperationManager *manager=[AFHTTPRequestOperationManager manager];
//    manager.responseSerializer =[AFJSONResponseSerializer serializer];
//    
//    [manager GET:requestUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
//        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
//        NSLog(@"error: %@", [operation error]);
//    }];
    

    /*
    
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];

    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"img.png"];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
    [formData appendPartWithFileData:imageData name:@"image" fileName:fName mimeType:@"image/jpeg"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[[AFHTTPRequestOperation alloc] initWithRequest:myRequest] autorelease];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
    [httpClient release];
    */
}

-(void)cancelAllQueueOperation {
    [self.queue cancelAllOperations];
}

/*
-(void)temp:(NSString *) requestUrl:(int) _tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:requestUrl parameters:params];
    AFHTTPRequestOperation *operation = [AFHTTPRequestOperation operationWithRequest:request
        completion:^(NSURLRequest *req, NSHTTPURLResponse *response, NSData *data, NSError *error) {
            BOOL HTTPStatusCodeIsAcceptable = [[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 100)] containsIndex:[response statusCode]];
            if (HTTPStatusCodeIsAcceptable) {
                NSLog(@"Request Successful");
            } else {
        NSLog(@"[Error]: (%@ %@) %@", [request HTTPMethod], [[request URL] relativePath], error);
        }
    }];
}
*/

-(void)dealloc {
    [super dealloc];
}

@end
