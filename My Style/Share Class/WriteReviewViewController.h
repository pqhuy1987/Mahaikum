//
//  WriteReviewViewController.h
//  
//
//  Created by Charles Moon on 1/27/16.
//
//

#import <UIKit/UIKit.h>
#import "Profile_share.h"

@interface WriteReviewViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *viewTitle;
@property (strong, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) IBOutlet UITextView *reviewDescription;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;

@property(nonatomic,strong) Profile_share *profile_share_obj;

@end
