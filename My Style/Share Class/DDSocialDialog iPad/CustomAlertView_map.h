//
//  CustomAlertView_map.h
//  My Style
//
//  Created by Tis Macmini on 7/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "DDSocialDialog_iPad.h"
#import <Foundation/Foundation.h>

@protocol CustomAlertViewDelegate;

@interface CustomAlertView_map : DDSocialDialog_iPad{

    UIView * myContentView ;
	id <CustomAlertViewDelegate> __weak delegate_;
    
	UIImageView *imgView;
}
@property (nonatomic, weak) id <CustomAlertViewDelegate> delegate;
@property (nonatomic) UIView * myContentView ;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img ;
@end
