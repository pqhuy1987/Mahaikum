//
//  CustomAlertView_map.m
//  My Style
//
//  Created by Tis Macmini on 7/11/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "CustomAlertView_map.h"

@implementation CustomAlertView_map
@synthesize delegate = delegate_;
@synthesize myContentView;
@synthesize imgView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img {
	
    
    if (isiPhone) {
        if(is_iPhone_5){
            myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 580)];
        }else{
            myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 480)];
        }
    }else{
        myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0,768, 1014)];
    }

    
	[myContentView setBackgroundColor:[UIColor clearColor]];
    
    
    
    UIImageView *backImage;
    if (isiPhone) {
        if (is_iPhone_5) {
            backImage= [[UIImageView alloc] initWithFrame:CGRectMake(-130, 0,580+130,580+130)];
        }else{
            backImage= [[UIImageView alloc] initWithFrame:CGRectMake(-70, 0,580,580)];
        }
    }else {
        backImage= [[UIImageView alloc] initWithFrame:CGRectMake(-138,0, 1150, 1150)];
    }
    myContentView.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
   // backImage.image = [UIImage imageNamed:@"popupbg@2x.png"];
   // [myContentView addSubview:backImage];
   // [backImage release];
    
    UIImageView *img_header;
    if (isiPhone) {
        if (is_iPhone_5) {
            img_header =[[UIImageView alloc]initWithFrame:CGRectMake(0,15, 320,44)];
        }else{
            img_header =[[UIImageView alloc]initWithFrame:CGRectMake(0,5, 320,44)];
        }
    }else{
        img_header =[[UIImageView alloc]initWithFrame:CGRectMake(0,5,768,107)];
    }
    
    
    img_header.image =[UIImage imageNamed:@"header_map@2x.png"];
    [myContentView addSubview:img_header];
    
    
    UIImageView *bg ;
    if (isiPhone) {
        if (is_iPhone_5) {
            bg= [[UIImageView alloc] initWithFrame:CGRectMake(8,85, 304,458)];
            
        }else{
            bg= [[UIImageView alloc] initWithFrame:CGRectMake(8,85, 304,458-88)];
            
        }
    }else{
        bg= [[UIImageView alloc] initWithFrame:CGRectMake(16,135, 768-32,1004-170)];
    }

    
    bg.image = [UIImage imageNamed:@"background_map@2x.png"];
    [myContentView addSubview:bg];
    
    
    UIImageView *img_map ;
    
    if (isiPhone) {
        img_map= [[UIImageView alloc] initWithFrame:CGRectMake(18,95, 284,97)];
    }else{
            img_map= [[UIImageView alloc] initWithFrame:CGRectMake(100,150, 568,194)];
    }
    
    img_map.image = [UIImage imageNamed:@"mapbg@2x.png"];
    [myContentView addSubview:img_map];
    
    UILabel *lbl_big_header;
    if (isiPhone) {
        lbl_big_header=[[UILabel alloc]initWithFrame:CGRectMake(54, 95+97+15, 212, 30)];
        lbl_big_header.font =[UIFont fontWithName:@"Verdana-Bold" size:11];

    }else{
        lbl_big_header=[[UILabel alloc]initWithFrame:CGRectMake(172, 370, 424,60)];
        lbl_big_header.font =[UIFont fontWithName:@"Verdana-Bold" size:22];

    }
    lbl_big_header.numberOfLines=0;
    lbl_big_header.backgroundColor =[UIColor clearColor];

    lbl_big_header.lineBreakMode = NSLineBreakByWordWrapping;
    lbl_big_header.textAlignment =NSTextAlignmentCenter;
    lbl_big_header.text =@"A FUN, NEW WAY OF BROWSING PHOTOS ON MAP";
    lbl_big_header.textColor =[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    [myContentView addSubview:lbl_big_header];
    
    
    UILabel *lbl_big_second_header;
    if (isiPhone) {
        lbl_big_second_header =[[UILabel alloc]initWithFrame:CGRectMake(18, 95+97+15+40, 284, 80)];
    lbl_big_second_header.font =[UIFont fontWithName:@"Verdana" size:11];
    }else{
        lbl_big_second_header =[[UILabel alloc]initWithFrame:CGRectMake(100, 480, 568, 160)];
            lbl_big_second_header.font =[UIFont fontWithName:@"Verdana" size:22];
    }
    
    lbl_big_second_header.numberOfLines=0;
    lbl_big_second_header.backgroundColor =[UIColor clearColor];

    lbl_big_second_header.lineBreakMode = NSLineBreakByWordWrapping;
    lbl_big_second_header.textAlignment =NSTextAlignmentCenter;
    lbl_big_second_header.text =@"Your Photo Map is currently empty because you haven't geotagged any photos. \n Choose \"Add to your Photo Map\" when sharing photos to browse them here.";
    lbl_big_second_header.textColor =[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    [myContentView addSubview:lbl_big_second_header];
    
    UIButton *btn_ok;
    if (isiPhone) {
        if (is_iPhone_5) {
            btn_ok=[[UIButton alloc]initWithFrame:CGRectMake(20,458+85-53+9,279,35)];
        }else{
            btn_ok=[[UIButton alloc]initWithFrame:CGRectMake(20,458+85-53+9-84,279,35)];
        }
        [btn_ok setImage:[UIImage imageNamed:@"btn_ok_map.png"] forState:UIControlStateNormal];

    }else{
        btn_ok=[[UIButton alloc]initWithFrame:CGRectMake(104,883,560,70)];
        [btn_ok setImage:[UIImage imageNamed:@"btn_ok_map@2x.png"] forState:UIControlStateNormal];

    }

    [btn_ok addTarget:self action:@selector(btn_ok_click:) forControlEvents:UIControlEventTouchUpInside];
    [myContentView addSubview:btn_ok];
    
    if (isiPhone) {
        if (is_iPhone_5){
            if ((self = [super initWithFrame:CGRectMake(0,0,320,580) theme:theme])) {
                [super setCustomView : myContentView] ;
                delegate_ = delegate;
                self.dialogDelegate = delegate;
            }
        }else{
            if ((self = [super initWithFrame:CGRectMake(0,0,320,480) theme:theme])) {
                [super setCustomView : myContentView] ;
                delegate_ = delegate;
                self.dialogDelegate = delegate;
            }
        }
    }else{
        if ((self = [super initWithFrame:CGRectMake(0,0,768,1024) theme:theme])) {
            [super setCustomView : myContentView] ;
            delegate_ = delegate;
            self.dialogDelegate = delegate;
        }
    }
    
        
    
	return self;
}

-(IBAction)btn_ok_click:(id)sender{
    
    [super dismiss:YES];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
@end
