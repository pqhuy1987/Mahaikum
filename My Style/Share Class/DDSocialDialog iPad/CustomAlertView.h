//
//  CustomAlertView.h
//  Gazillion
//
//  Created by Rupen Makhecha on 11/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "DDSocialDialog_iPad.h"
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"
#import "StaticClass.h"

@protocol CustomAlertViewDelegate;

@interface CustomAlertView : DDSocialDialog_iPad<UITextFieldDelegate,NSURLConnectionDelegate> {
	UIView * myContentView ;
	id <CustomAlertViewDelegate> __weak delegate_;
    
	UIImageView *imgView;		
	UIImageView *imgLogo;
	
	UILabel *lblName;
	
	UIButton *btnClose;
	UIButton *btnShake;
    
    UITextField *txtDataLimit;
    
    UIButton *btnCheckMB;
    UIButton *btnCheckGB;
    
    UITextField *txt_username;
    UITextField *txt_password;
    
    UIImageView *signicon1,*signicon1true,*signicon2,*signicon2true;
    
     NSMutableData *loin_data;
    
    UIActivityIndicatorView *activity;
    UILabel *lblSpinnerBg;
}
@property(nonatomic,strong)IBOutlet NSMutableData *loin_data;
@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)IBOutlet UITextField *txt_password;
@property (nonatomic, weak) id <CustomAlertViewDelegate> delegate;
@property (nonatomic, strong) UIView * myContentView ;

@property (nonatomic, strong) UIButton *btnClose;
@property (nonatomic, strong) UIButton *btnShake;
@property (nonatomic, strong) UIImageView *imgLogo;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) UILabel *lblName;
@property (nonatomic, strong) IBOutlet UIImageView *signicon1,*signicon2,*signicon1true,*signicon2true;

@property (nonatomic, strong) UIActivityIndicatorView *activity;
@property (nonatomic, strong) UILabel *lblSpinnerBg;

-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img ;

@end

