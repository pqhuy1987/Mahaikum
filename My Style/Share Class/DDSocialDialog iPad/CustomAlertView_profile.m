//
//  CustomAlertView_profile.m
//  NinjaDrop
//
//  Created by Tis Macmini on 3/29/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "CustomAlertView_profile.h"
#import "QuartzCore/QuartzCore.h"

@implementation CustomAlertView_profile

@synthesize delegate = delegate_;
@synthesize myContentView;
@synthesize imgView,btnShake;
@synthesize btnClose,imgLogo;
@synthesize lblName;
@synthesize txt_username,txt_password;
@synthesize loin_data;

-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img {
	
    self.loin_data =[[NSMutableData alloc]init];
    
    if(is_iPhone_5){
        myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 580)];
    }else{
        myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 480)];
    }
    
	[myContentView setBackgroundColor:[UIColor clearColor]];
    
    //UIImageView *backImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    UIImageView *backImage;
    
    if (is_iPhone_5) {
        backImage= [[UIImageView alloc] initWithFrame:CGRectMake(-130, 0,580+130,580+130)];
    }else{
        backImage= [[UIImageView alloc] initWithFrame:CGRectMake(-70, 0,580,580)];
    }
    
    
    backImage.image = [UIImage imageNamed:@"popupbg@2x.png"];
    [myContentView addSubview:backImage];
    
    UIImageView *bg;
    
    bg= [[UIImageView alloc] initWithFrame:CGRectMake(10,90, 299,287)];
    bg.image = [UIImage imageNamed:@"popupbackground@2x.png"];
    
    [myContentView addSubview:bg];
    
    
    UIButton *btn_close;
    btn_close =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_close setFrame:CGRectMake(280,98,18,19)];
    btn_close.layer.masksToBounds = YES;
    [btn_close setBackgroundImage:[UIImage imageNamed:@"closebtn_pop.png"] forState:UIControlStateNormal];
    
    [btn_close addTarget:self action:@selector(btnCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    [myContentView addSubview:btn_close];
    
    UIImageView *img_login =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"logintoninjatax@2x.png"]];
    img_login.frame = CGRectMake(72, 105, 175, 21);
    [myContentView addSubview:img_login];
    
    UIButton *btn_fb_login =[UIButton buttonWithType:UIButtonTypeCustom];
    btn_fb_login.frame =CGRectMake(64,130,192,39);
    btn_fb_login.layer.masksToBounds = YES;
    [btn_fb_login setImage:[UIImage imageNamed:@"facebookbtn_pop.png"] forState:UIControlStateNormal];
    [btn_fb_login addTarget:self action:@selector(btn_facebook_click:) forControlEvents:UIControlEventTouchUpInside];
    [myContentView addSubview:btn_fb_login];
    

    
    
    UIImageView *img_or_line =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"divdidershadow.png"]];
    [img_or_line setFrame:CGRectMake(14,175, 289, 28)];
    [myContentView addSubview:img_or_line];
    
    UIImageView *img_signup=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sign_profile@2x.png"]];
    img_signup.frame = CGRectMake(45,205, 230,18);
    [myContentView addSubview:img_signup];
    
    txt_username=[[UITextField alloc]initWithFrame:CGRectMake(40,230,240,45)];
    txt_username.borderStyle = UITextBorderStyleNone;
    txt_username.font = [UIFont systemFontOfSize:15];
    txt_username.placeholder = @"Email";
    //  txt_username.text=@"bzitzow@seatninja.com";
    txt_username.delegate =self;
    
    txt_username.returnKeyType = UIReturnKeyNext;
    txt_username.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_username.backgroundColor = [UIColor clearColor];
    
    txt_username.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UILabel *lbl_username =[[UILabel alloc]initWithFrame:CGRectMake(35,230,250,45)];
    lbl_username.layer.borderColor =[UIColor colorWithRed:126.0/255.0 green:174.0/255.0 blue:207.0/255.0 alpha:1].CGColor;
    lbl_username.layer.borderWidth=2.0f;
    lbl_username.layer.cornerRadius =5.0f;
    lbl_username.backgroundColor =[UIColor clearColor];
    [myContentView addSubview:lbl_username];
    [myContentView addSubview:txt_username];
    
    txt_password=[[UITextField alloc]initWithFrame:CGRectMake(40,280,240,45)];
    txt_password.borderStyle = UITextBorderStyleNone;
    txt_password.font = [UIFont systemFontOfSize:15];
    txt_password.placeholder = @"Password";
    // txt_password.text =@"testpass";
    txt_password.delegate =self;
    txt_password.secureTextEntry = YES;
    txt_password.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_password.backgroundColor = [UIColor clearColor];
    
    txt_password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UILabel *lbl_password =[[UILabel alloc]initWithFrame:CGRectMake(35,280,250,45)];
    lbl_password.layer.borderColor =[UIColor colorWithRed:126.0/255.0 green:174.0/255.0 blue:207.0/255.0 alpha:1].CGColor;
    lbl_password.layer.borderWidth=2.0f;
    lbl_password.layer.cornerRadius =5.0f;
    lbl_password.backgroundColor =[UIColor clearColor];
    [myContentView addSubview:lbl_password];
    [myContentView addSubview:txt_password];
    
    UIButton *btn_create_account =[UIButton buttonWithType:UIButtonTypeCustom];
    [btn_create_account setFrame:CGRectMake(82,330,155,34)];
    [btn_create_account addTarget:self action:@selector(btn_create_click:) forControlEvents:UIControlEventTouchUpInside];
    [btn_create_account setImage:[UIImage imageNamed:@"createaccountbtn@2x.png"] forState:UIControlStateNormal];
    btn_create_account.layer.masksToBounds = YES;
    [myContentView addSubview:btn_create_account];
    

    
    
    
    
    if (is_iPhone_5){
        if ((self = [super initWithFrame:CGRectMake(0,0,320,580) theme:theme])) {
            [super setCustomView : myContentView] ;
            delegate_ = delegate;
            self.dialogDelegate = delegate;
        }
    }else{
        if ((self = [super initWithFrame:CGRectMake(0,0,320,480) theme:theme])) {
            [super setCustomView : myContentView] ;
            delegate_ = delegate;
            self.dialogDelegate = delegate;
        }
    }
    
    
	
	return self;
}




-(IBAction)btnCancelClick:(id)sender {
	[super dismiss:YES];
}
-(IBAction)btn_create_click:(id)sender{
    
    if(self.txt_username.text.length==0){
    
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(self.txt_password.text.length==0){
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    

}
-(IBAction)btn_skip_click:(id)sender{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"skip_click" object:nil];
    [super dismiss:YES];
}


#pragma marak Login

-(IBAction)btn_login_click:(id)sender{

    
    
    
}


#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txt_username) {
        [txt_password becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}



@end
