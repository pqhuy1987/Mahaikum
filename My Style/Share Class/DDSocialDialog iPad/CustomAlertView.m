
#import "CustomAlertView.h"
#import "QuartzCore/QuartzCore.h"
#import "HyTransitions.h"
#import "HyLoginButton.h"

@implementation CustomAlertView

@synthesize delegate = delegate_;
@synthesize myContentView;
@synthesize imgView,btnShake;
@synthesize btnClose,imgLogo;
@synthesize lblName;
@synthesize txt_username,txt_password;
@synthesize loin_data,signicon1,signicon2,signicon1true,signicon2true;
@synthesize activity,lblSpinnerBg;

-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img {

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"close_popup" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(close_popup:) name:@"close_popup" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stop_spinner" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stop_spinner:) name:@"stop_spinner" object:nil];
    
    self.loin_data =[[NSMutableData alloc]init];
    if (isiPhone) {
        if(is_iPhone_5){
            myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 580)];
        }else{
            myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, 0, 320, 480)];
        }}
    else {
            myContentView=[[UIView alloc]initWithFrame:CGRectMake(-10, -10, 768,1024)];
        }
    
	[myContentView setBackgroundColor:[UIColor clearColor]];

    UIImageView *backImage;
    if (isiPhone) {
        if (is_iPhone_5) {
            backImage= [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,320,568)];
        }
        else{
            backImage= [[UIImageView alloc] initWithFrame:CGRectMake(0, -10,320,480)];
        }
    }
    else {
        backImage= [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 768, 1024)];
    }
    
    backImage.image = [UIImage imageNamed:@"background.png"];
    [myContentView addSubview:backImage];
    
    UIImageView *bg ;
    if (isiPhone) {
        bg= [[UIImageView alloc] initWithFrame:CGRectMake(5,50, 315,378)];
        bg.image = [UIImage imageNamed:@"popupbackground.png"];
    }
    else{
        bg= [[UIImageView alloc] initWithFrame:CGRectMake(133,194, 502,636)];
        bg.image = [UIImage imageNamed:@"Popup Background_iPad.png"];
    }
    
    [myContentView addSubview:bg];

    UIButton *btn_close;
    btn_close =[UIButton buttonWithType:UIButtonTypeCustom];

    if (isiPhone) {
        [btn_close setBackgroundImage:[UIImage imageNamed:@"closebtn_pop.png"] forState:UIControlStateNormal];
        [btn_close setFrame:CGRectMake(277,40,43,42)];

    }else{
        [btn_close setBackgroundImage:[UIImage imageNamed:@"Close Button_iPad.png"] forState:UIControlStateNormal];
        [btn_close setFrame:CGRectMake(615,170,52,52)];
    }
    btn_close.layer.masksToBounds = YES;
    [btn_close addTarget:self action:@selector(btnCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    HyLoginButton *btn_login =[HyLoginButton buttonWithType:UIButtonTypeCustom];
    [btn_login setBackgroundColor:[UIColor colorWithRed:1 green:0.f/255.0f blue:128.0f/255.0f alpha:1]];
    [btn_login setTitle:@"Login" forState:UIControlStateNormal];
    
    if (isiPhone) {
        btn_login.frame =CGRectMake(73,220,173,36);
//        [btn_login setImage:[UIImage imageNamed:@"signinbtn.png"] forState:UIControlStateNormal];
    }else{
        btn_login.frame =CGRectMake(208,515,337,75);
//        [btn_login setImage:[UIImage imageNamed:@"SignIn Button_iPad.png"] forState:UIControlStateNormal];
    }
    btn_login.layer.masksToBounds = YES;
    [btn_login addTarget:self action:@selector(btn_login_click:) forControlEvents:UIControlEventTouchUpInside];
    [myContentView addSubview:btn_login];
    
    UIButton *btn_forget =[UIButton buttonWithType:UIButtonTypeCustom];
    if (isiPhone) {
        btn_forget.frame =CGRectMake(115,280,89,21);
        btn_forget.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:10.0];
        
    }else{
        btn_forget.frame =CGRectMake(460,613,80,24);
        btn_forget.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16.0];
    }
    btn_forget.layer.masksToBounds = YES;
    
    [btn_forget setImage:[UIImage imageNamed:@"forgotpasswordbtn.png"] forState:UIControlStateNormal];
    [btn_forget setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [btn_forget addTarget:self action:@selector(btn_forget_password_click:) forControlEvents:UIControlEventTouchUpInside];
    [myContentView addSubview:btn_forget];
    
    UIImageView *img_or_line;
    UIImageView *img_or_line1;
    if (isiPhone) {
        img_or_line=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginuser.png"]];
        [img_or_line setFrame:CGRectMake(80,130, 15, 14)];

        img_or_line1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginpass.png"]];
        [img_or_line1 setFrame:CGRectMake(80,180, 14, 17)];
    }else{
        img_or_line=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginuser.png"]];
        [img_or_line setFrame:CGRectMake(123,631, 30, 28)];

        img_or_line1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"loginpass.png"]];
        [img_or_line1 setFrame:CGRectMake(35,290, 28, 34)];
    }
    UIImageView *img_signup;
    
    UIImageView *imgTextImage;
    if (isiPhone) {
        img_signup=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"BG.png"]];
        imgTextImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Popup Head_iPad.png"]];
        
        img_signup.frame = CGRectMake(32,78, 260,37);
        imgTextImage.frame = CGRectMake(32,78, 260,37);
    }
    else {
        img_signup=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"BG.png"]];
        imgTextImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Popup Head_iPad.png"]];
        
        img_signup.frame = CGRectMake(195,265, 352,36);
        imgTextImage.frame = CGRectMake(195,265, 352,36);
    }
    //[myContentView addSubview:img_signup];
    //[myContentView addSubview:imgTextImage];
    
    [myContentView addSubview:btn_close];
    
    if(isiPhone){
        txt_username=[[UITextField alloc]initWithFrame:CGRectMake(100,120,150,36)];
    }
    else{
        txt_username=[[UITextField alloc]initWithFrame:CGRectMake(233,310,480,80)];
    }
    txt_username.borderStyle = UITextBorderStyleNone;
    txt_username.placeholder = @"Username";
    txt_username.text =@"";
    txt_username.delegate =self;
    txt_username.returnKeyType = UIReturnKeyNext;
    txt_username.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_username.backgroundColor = [UIColor clearColor];
    txt_username.textColor = [UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];

    txt_username.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    UILabel *lbl_username;
    UIImageView *textboximg1;
    if (isiPhone) {
        textboximg1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"signinTextbox BG.png"]];
        signicon1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Nothing.png"]];
        signicon1true=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Entered.png"]];
    }
    else{
        textboximg1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"signinTextbox BG_iPad.png"]];
        signicon1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Nothing_iPad.png"]];
        signicon1true=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Entered_iPad.png"]];
    }
    
    if (isiPhone) {
        lbl_username = [[UILabel alloc]initWithFrame:CGRectMake(75,120,170,36)];
        lbl_username.layer.borderWidth=2.0f;
        [textboximg1 setFrame:CGRectMake(75,120, 168, 37)];
        [signicon1 setFrame:CGRectMake(220,130, 13, 14)];
        signicon1true.hidden = YES;
        [signicon1true setFrame:CGRectMake(220, 130, 13, 14)];
    }
    else{
        lbl_username = [[UILabel alloc]initWithFrame:CGRectMake(230,338,490,80)];
        lbl_username.layer.borderWidth=4.0f;
        [textboximg1 setFrame:CGRectMake(208, 314, 335, 73)];
        [signicon1 setFrame:CGRectMake(496, 335, 25, 28)];
        signicon1true.hidden = YES;
        [signicon1true setFrame:CGRectMake(496, 335, 25, 28)];
    }
    
     lbl_username.layer.borderColor =[UIColor colorWithRed:126.0/255.0 green:174.0/255.0 blue:207.0/255.0 alpha:1].CGColor;
     lbl_username.layer.cornerRadius =5.0f;
    lbl_username.backgroundColor =[UIColor clearColor];
    [myContentView addSubview:textboximg1];
    [myContentView addSubview:signicon1true];
    [myContentView addSubview:signicon1];
    [myContentView addSubview:txt_username];
    
    
    if (isiPhone) {
        txt_password=[[UITextField alloc]initWithFrame:CGRectMake(100,170,150,36)];
            txt_password.font = [UIFont systemFontOfSize:15];
    }else{
        txt_password=[[UITextField alloc]initWithFrame:CGRectMake(233,410,480,80)];
        txt_password.font = [UIFont systemFontOfSize:30];
    }
    txt_password.borderStyle = UITextBorderStyleNone;

    txt_password.placeholder = @"Password";
    txt_password.text =@"";
    txt_password.delegate =self;
    txt_password.secureTextEntry = YES;
    txt_password.autocorrectionType = UITextAutocorrectionTypeNo;
    txt_password.backgroundColor = [UIColor clearColor];
    txt_password.textColor = [UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    txt_password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    UILabel *lbl_password;
    UIImageView *textboximg2;
    if (isiPhone) {
        textboximg2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"signinTextbox BG.png"]];
        signicon2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Nothing.png"]];
        signicon2true=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Entered.png"]];
    }
    else{
        textboximg2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"signinTextbox BG_iPad.png"]];
        signicon2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Nothing.png_iPad"]];
        signicon2true=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Entered.png_iPad"]];
    }
    
    if (isiPhone) {
        lbl_password=[[UILabel alloc]initWithFrame:CGRectMake(75,170,170,36)];
        lbl_password.layer.borderWidth=2.0f;
        [textboximg2 setFrame:CGRectMake(75,170, 168, 37)];
        [signicon2 setFrame:CGRectMake(220,180, 13, 14)];
        signicon2true.hidden = YES;
        [signicon2true setFrame:CGRectMake(220, 180, 13, 14)];
    }
    else{
        lbl_password=[[UILabel alloc]initWithFrame:CGRectMake(139,280-50+40,490,80)];
        lbl_password.layer.borderWidth=4.0f;
        [textboximg2 setFrame:CGRectMake(208,415, 335, 73)];
        [signicon2 setFrame:CGRectMake(495,438, 25, 28)];
        signicon2true.hidden = YES;
        [signicon2true setFrame:CGRectMake(495, 438, 25, 28)];
    }
    
    lbl_password.layer.borderColor =[UIColor colorWithRed:126.0/255.0 green:174.0/255.0 blue:207.0/255.0 alpha:1].CGColor;
    lbl_password.layer.cornerRadius =5.0f;
    lbl_password.backgroundColor =[UIColor clearColor];

    [myContentView addSubview:textboximg2];
    [myContentView addSubview:signicon2true];
    [myContentView addSubview:signicon2];
    [myContentView addSubview:txt_password];

    [myContentView addSubview:img_or_line];
    [myContentView addSubview:img_or_line1];

    UIButton *btn_signin_facebook =[UIButton buttonWithType:UIButtonTypeCustom];
    if (isiPhone) {
        [btn_signin_facebook setFrame:CGRectMake(70,370,177,35)];
        [btn_signin_facebook setImage:[UIImage imageNamed:@"signinwithfacebookbtn.png"] forState:UIControlStateNormal];
    }
    else{
        [btn_signin_facebook setFrame:CGRectMake(220,695,321,66)];
        [btn_signin_facebook setImage:[UIImage imageNamed:@"signinwithfacebookbtn_iPad.png"] forState:UIControlStateNormal];
    }

    [btn_signin_facebook addTarget:self action:@selector(btn_login_with_facebook:) forControlEvents:UIControlEventTouchUpInside];
     btn_signin_facebook.layer.masksToBounds = YES;
    [myContentView addSubview:btn_signin_facebook];
    
    if (isiPhone) {
        txt_username.font = [UIFont fontWithName:@"Century Gothic" size:10.0];
        txt_password.font = [UIFont fontWithName:@"Century Gothic" size:10.0];
    }else{
        txt_username.font = [UIFont fontWithName:@"Century Gothic" size:20.0];
        txt_password.font = [UIFont fontWithName:@"Century Gothic" size:20.0];
    }
    
    if (isiPhone) {
        if (is_iPhone_5){
            if ((self = [super initWithFrame:CGRectMake(0,0,320,580) theme:theme])) {
                [super setCustomView : myContentView] ;
                delegate_ = delegate;
                self.dialogDelegate = delegate;
            }
        }
        else{
            if ((self = [super initWithFrame:CGRectMake(0,0,320,480) theme:theme])) {
                [super setCustomView : myContentView];
                delegate_ = delegate;
                self.dialogDelegate = delegate;
            }
        }
    }
    else {
        if ((self = [super initWithFrame:CGRectMake(0,0,768,1024) theme:theme])) {
            [super setCustomView : myContentView] ;
            delegate_ = delegate;
            self.dialogDelegate = delegate;
        }
    }
	return self;
}

-(IBAction)btnCancelClick:(id)sender {
    [SVProgressHUD dismiss];
	[super dismiss:YES];
    
}

-(IBAction)btn_forget_password_click:(id)sender{
//forgot_password_click
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"forgot_password_click" object:nil];
    [super dismiss:YES];
}

-(IBAction)btn_login_click:(id)sender {
    if(self.txt_username.text.length==0){
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter username" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(self.txt_password.text.length==0){        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }

    [SVProgressHUD show];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
	
	NSString *myRequestString = [NSString stringWithFormat:@"salt=%@&sign=%@&username=%@&password=%@&device_token=%@",salt,sig,self.txt_username.text,self.txt_password.text,[StaticClass retrieveFromUserDefaults:@"GETDEVICETOKEN"]];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_login.php?%@",[[Singleton sharedSingleton] getBaseURL],[myRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    	NSLog(@"%@",requestStr);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"start_user_login" object:nil userInfo:@{@"requestStr": requestStr}];
}

-(IBAction)btn_login_with_facebook:(id)sender{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"login_with_facebook_click" object:nil];
    [super dismiss:YES];
}
-(IBAction)btn_login_with_twitter:(id)sender{
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"login_with_twitter_click" object:nil];
    [super dismiss:YES];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField==txt_username) {
        if ([txt_username.text isEqualToString:@""]) {
            signicon1.hidden = NO;
            signicon1true.hidden = YES;
        }
        else{
            signicon1.hidden = YES;
            signicon1true.hidden = NO;
        }
        [txt_password becomeFirstResponder];
    }else{
        if ([txt_password.text isEqualToString:@""]) {
            signicon2.hidden = NO;
            signicon2true.hidden = YES;
        }
        else{
            signicon2.hidden = YES;
            signicon2true.hidden = NO;
        }
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)close_popup:(NSNotification *)notification{
    [SVProgressHUD dismiss];
    NSString *strUserName=txt_username.text;
    NSString *strPassword=txt_password.text;
    
    [StaticClass saveToUserDefaults:strUserName :@"STOREUSERNAME"];
    [StaticClass saveToUserDefaults:strPassword :@"STOREPASSWORD"];
    [super dismiss:YES];
}

-(void)stop_spinner:(NSNotification *)notification {
    [SVProgressHUD dismiss];
}

@end
