//
//  CustomAlertView_profile.h
//  NinjaDrop
//
//  Created by Tis Macmini on 3/29/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "DDSocialDialog_iPad.h"
#import <Foundation/Foundation.h>
#import "DDSocialDialog_iPad.h"
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import "Singleton.h"
#import "StaticClass.h"

@protocol CustomAlertViewDelegate;

@interface CustomAlertView_profile : DDSocialDialog_iPad<UITextFieldDelegate,NSURLConnectionDelegate>{

    UIView * myContentView ;
	id <CustomAlertViewDelegate> __weak delegate_;
    
	UIImageView *imgView;
	UIImageView *imgLogo;
	
	UILabel *lblName;
	
	UIButton *btnClose;
	UIButton *btnShake;
    
    UITextField *txtDataLimit;
    
    UIButton *btnCheckMB;
    UIButton *btnCheckGB;
    
    UITextField *txt_username;
    UITextField *txt_password;
    
    NSMutableData *loin_data;
}
@property(nonatomic,strong)IBOutlet NSMutableData *loin_data;
@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)IBOutlet UITextField *txt_password;
@property (nonatomic, weak) id <CustomAlertViewDelegate> delegate;
@property (nonatomic) UIView * myContentView ;

@property (nonatomic, strong) UIButton *btnClose;
@property (nonatomic, strong) UIButton *btnShake;
@property (nonatomic, strong) UIImageView *imgLogo;
@property (nonatomic, strong) IBOutlet UIImageView *imgView;
@property (nonatomic, strong) UILabel *lblName;


-(id)initWithDelegate:(id)delegate theme:(DDSocialDialogTheme)theme alertImage:(NSString *)img ;


@end
