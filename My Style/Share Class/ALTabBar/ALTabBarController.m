//
//  ALTabBarController.m
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//

#import "ALTabBarController.h"
#import "iosMacroDefine.h"


@implementation ALTabBarController

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    self.delegate = self;
    self.transitioningDelegate = ((AppDelegate *)[UIApplication sharedApplication].delegate).transition;
    
    if (isiPhone)
    {
        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TabBarView" owner:self options:nil];
        self.customTabBarView = [nibObjects objectAtIndex:0];
        [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
            vc.title = nil;
            vc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
            vc.transitioningDelegate = ((AppDelegate *)[UIApplication sharedApplication].delegate).transition;
            ((UINavigationController *)vc).delegate = ((AppDelegate *)[UIApplication sharedApplication].delegate).transition;

        }];
    }
    else
    {
//        NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TabBarView_iPad" owner:self options:nil];
//        self.customTabBarView = [nibObjects objectAtIndex:0];
    }
	
    [self setupTabBarItems];
    
    self.customTabBarView.delegate = self;
    
    self.customTabBarView.frame = CGRectMake(0, kViewHeight - 45, kViewWidth, 50);
    [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
        vc.title = nil;
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }];
    
    [self.view addSubview:self.customTabBarView];
    
    self.badgeView = [[CHBadgeView alloc] init];
    self.badgeView.frame = CGRectMake(kViewWidth*3/5 + 17, kViewHeight - 92, 40, 40);
    [self.view addSubview:_badgeView];
    _badgeView.badgeBorderColor = Rgb2UIColor(255,71,73);
    _badgeView.badgeColor = Rgb2UIColor(255,72,74);
    _badgeView.badgeBorderWidth =0.5f;
    _badgeView.drawBadgeBorder = YES;
    _badgeView.badgeCornerRadius = 4.0;
    _badgeView.bottomArrowHeight = 10.0f;
    _badgeView.badgeLabel.text = @"1";
    _badgeView.badgeLabel.font = [UIFont systemFontOfSize:14];
    _badgeView.badgeLabel.shadowColor   = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
    _badgeView.badgeLabel.shadowOffset  = CGSizeMake(0.0,-0.2);
    _badgeView.showArrow = YES;
    _badgeView.hidden = YES;
}

#pragma mark Tab bar controller delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (viewController.tabBarItem.tag == 3)
    {
        return NO;
    }
    
    return YES;
}

- (void)setupTabBarItems
{
    UIImage *selectedImage1 = [UIImage imageNamed:@"home"];
    selectedImage1 = [selectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *delectedImage1 = [UIImage imageNamed:@"homedeselected"];
    delectedImage1 = [delectedImage1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.viewControllers[0] setTabBarItem:[[UITabBarItem alloc] initWithTitle:nil image:delectedImage1 selectedImage:selectedImage1]];
    
    UIImage *selectedImage2 = [UIImage imageNamed:@"search"];
    selectedImage2 = [selectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *delectedImage2 = [UIImage imageNamed:@"searchdeselected"];
    delectedImage2 = [delectedImage2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.viewControllers[1] setTabBarItem:[[UITabBarItem alloc] initWithTitle:nil image:delectedImage2 selectedImage:selectedImage2]];
    
    UIImage *selectedImage3 = [UIImage imageNamed:@"camera"];
    selectedImage3 = [selectedImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *delectedImage3 = [UIImage imageNamed:@"camera"];
    delectedImage3 = [delectedImage3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.viewControllers[2] setTabBarItem:[[UITabBarItem alloc] initWithTitle:nil image:delectedImage3 selectedImage:selectedImage3]];
    
    UIImage *selectedImage4 = [UIImage imageNamed:@"notifications"];
    selectedImage4 = [selectedImage4 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *delectedImage4 = [UIImage imageNamed:@"notificationsdeselected"];
    delectedImage4 = [delectedImage4 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.viewControllers[3] setTabBarItem:[[UITabBarItem alloc] initWithTitle:nil image:delectedImage4 selectedImage:selectedImage4]];
    
    UIImage *selectedImage5 = [UIImage imageNamed:@"profile"];
    selectedImage5 = [selectedImage5 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage *delectedImage5 = [UIImage imageNamed:@"profiledeselected"];
    delectedImage5 = [delectedImage5 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.viewControllers[4] setTabBarItem:[[UITabBarItem alloc] initWithTitle:nil image:delectedImage5 selectedImage:selectedImage5]];
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"maintabbar-bg.png"]];
    view.frame = CGRectMake(kViewWidth * 2.0 / 5.0, 2, kViewWidth / 5.0, 48);
    [self.tabBar addSubview:view];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

#pragma mark ALTabBarDelegate
- (void)tabWasSelected:(NSInteger)index
{
    self.selectedIndex = index;
    
    if (index == 0)
    {
        self.customTabBarView.imgObj.image = [UIImage imageNamed:@"maintabbar"];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"refresh_home_view_data" object:nil];
    }
    else if (index == 1)
    {
        self.customTabBarView.imgObj.image = [UIImage imageNamed:@"maintabbar"];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"refresh_search_view_data" object:nil];

    }
    else if (index == 2)
    {
        if(isiPhone)
        {
            self.customTabBarView.imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        else
        {
            
        }
    }
    else if(index == 3)
    {
        self.customTabBarView.imgObj.image = [UIImage imageNamed:@"maintabbar"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refresh_notification_view_data" object:nil];

    }
    else if(index == 4)
    {
        self.customTabBarView.imgObj.image = [UIImage imageNamed:@"maintabbar"];
        //[[NSNotificationCenter defaultCenter]postNotificationName:@"refresh_profile_view_data" object:nil];
    }
}
@end

