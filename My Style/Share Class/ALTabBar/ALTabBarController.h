//
//  ALTabBarController.h
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//
//  Custom TabBarController that hides the iOS TabBar view and displays a custom
//  UI defined in TabBarView.xib.  By customizing TabBarView.xib, you can
//  create a tab bar that is unique to your application, but still has the tab
//  switching functionality you've come to expect out of UITabBarController.
 

#import <Foundation/Foundation.h>
#import "ALTabBarView.h"
#import "aCameraMoreViewController.h"
//#import "aCameraMoreViewController_iPad.h"
#import "CHBadgeView.h"

@interface ALTabBarController : UITabBarController <ALTabBarDelegate, UITabBarControllerDelegate>

@property (nonatomic, strong) ALTabBarView *customTabBarView;
@property (nonatomic, strong) CHBadgeView *badgeView;
@property (strong, nonatomic) IBOutlet UITabBar *tabbar;

@end
