//
//  ALTabBarView.m
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//

#import "ALTabBarView.h"
#import "Singleton.h"

@implementation ALTabBarView

@synthesize delegate;
@synthesize selectedButton,imgObj;
@synthesize home,search,cemera,favourite,profile;

@synthesize img_bg;

- (void)dealloc
{
    delegate = nil;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

//Let the delegate know that a tab has been touched
- (IBAction) touchButton:(id)sender
{
    if (delegate != nil && [delegate respondsToSelector:@selector(tabWasSelected:)])
    {
        NSInteger tagid = ((UIButton *)sender).tag;
        if (tagid == 0) {
            imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        else if(tagid == 1) {
            imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        else if(tagid == 2) {
            imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        else if(tagid == 3) {
            imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        else if(tagid == 4) {
            imgObj.image = [UIImage imageNamed:@"maintabbar"];
        }
        [delegate tabWasSelected:tagid];
    }
}

@end
