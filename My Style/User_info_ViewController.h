//
//  User_info_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 5/28/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "image_collection_cell.h"
#import "image_collection_header.h"
#import "image_collection_footer.h"
#import "Likers_list_cell.h"
#import "Show_map_ViewController.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "MyControl.h"

#import "AJNotificationView.h"

#import "Home_tableview_data_share.h"
#import "Comment_share.h"

#import "EGOImageButton.h"
#import "Home_tableview_cell.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "BlockActionSheet.h"
#import "Likers_list_ViewController.h"
#import "Comments_list_ViewController.h"
#import "Share_photo.h"
#import "Image_detail.h"

#import "Followers_list.h"
#import "Profile_share.h"
#import "Edit_your_profile.h"
#import "Setting_profile.h"
#import "LORichTextLabel.h"
#import "CategoriesViewController.h"
#import "MXScrollViewController.h"

#import "Chat_ViewController.h"
#import "GoogleMap_ViewController.h"
#import "Inbox_list_ViewController.h"

@interface User_info_ViewController : UIViewController<MFMailComposeViewControllerDelegate,DLStarRatingDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    //News feed
    NSInteger which_image_liked;
    NSInteger which_image_delete;
    NSInteger image_upload_flag;
    
    NSInteger fastCategoryFollow;
}

@property(nonatomic,strong)IBOutlet UIImageView *imgRequestSent;
@property(nonatomic,strong)IBOutlet UIButton *btn_edit;
@property(nonatomic,strong)IBOutlet UIButton *btn_edit_tbl;
@property(nonatomic,strong) LORichTextLabelStyle *username_style;

@property(nonatomic,strong) UIView *imgfullscreenviewobj;
@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow;
@property(nonatomic,strong)IBOutlet UIImageView *img_down_arrow_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lblheader,*lblphotos,*lblfollowing,*lblfollowers;
@property(nonatomic,strong)IBOutlet UILabel *lblphotos_tbl,*lblfollowing_tbl,*lblfollowers_tbl;
@property(nonatomic,strong)IBOutlet  UIView *view_secondcell;
@property(nonatomic,strong)IBOutlet  UIView *view_secondcell_tbl;

@property(nonatomic,strong)UIImagePickerController *photoPickerController;

@property(nonatomic,strong) Profile_share *profile_share_obj;
@property(nonatomic,strong) Followers_list *followers_list_viewObj;
@property(nonatomic,strong) Image_detail *image_detail_viewObj;
@property(nonatomic,strong) Likers_list_ViewController *liker_list_viewObj;
@property(nonatomic,strong) Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,strong) Edit_your_profile *edit_your_profile_viewObj;
@property(nonatomic,strong) Setting_profile *setting_profileViewObj;
@property(nonatomic,strong) CategoriesViewController *category_list_view;

@property(nonatomic,strong) Share_photo *share_photo_view;
@property(nonatomic,strong)IBOutlet UIImageView *img_like_heart;
@property(nonatomic,strong) NSMutableArray *array_feeds;
@property (nonatomic, strong) NSMutableArray *composed_feeds;
@property (nonatomic, strong) NSMutableArray *array_categories;
@property (nonatomic, strong) NSMutableArray *array_promotions;
@property (nonatomic, strong) NSMutableArray *array_reviews;
@property (nonatomic, strong) NSMutableDictionary *feeds_temp_dic;

//Footer view
@property(nonatomic,strong)IBOutlet UIView *view_footer;
@property(nonatomic,strong)IBOutlet UIView *view_footer_tbl;

@property(nonatomic,strong)Show_map_ViewController *mapviewObj;

@property(nonatomic,strong)IBOutlet  UICollectionView *collectionViewObj;
@property (strong, nonatomic) IBOutlet UILabel *lbl_no_promotions;

//Header view
@property(nonatomic,strong)IBOutlet UIView *view_header;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo;
@property(nonatomic,strong)IBOutlet UIView *view_img_photo;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_scondcell;

@property(nonatomic,strong)IBOutlet UIView *view_header_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl_bg;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl_bg_list;
@property(nonatomic,strong)IBOutlet UIView *view_img_photo_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_firstcell_tbl;
@property(nonatomic,strong)IBOutlet UIImageView *img_bg_scondcell_tbl;

@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followers;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followings;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_photos;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_categories;

@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followers_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followings_tbl;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_photos_tbl;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_categories_tbl;

// seller info view
@property (strong, nonatomic) IBOutlet UIView *sellerInfoView;
@property (strong, nonatomic) IBOutlet UIView *view_header_siv;
@property (strong, nonatomic) IBOutlet UIView *btn_edit_siv;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_siv;
@property(nonatomic,strong)IBOutlet UIView *view_img_photo_siv;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followers_siv;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_followings_siv;
@property(nonatomic,strong)IBOutlet UILabel *lbl_total_photos_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_categories_siv;
@property(nonatomic,strong)IBOutlet UIImageView *img_photo_tbl_bg_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_bio_siv;

//@property (strong, nonatomic) IBOutlet UITextView *lbl_bio_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mobile_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_email_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_website_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mobile_title_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_email_title_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_website_title_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_phone_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_mail_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_web_siv;
@property (strong, nonatomic) IBOutlet UIButton *btn_phone_siv;
@property (strong, nonatomic) IBOutlet UIButton *btn_mail_siv;
@property (strong, nonatomic) IBOutlet UIButton *btn_web_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_delivery_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_delivery_title_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_delivery_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_hours_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_hours_title_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_hours_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_fee_siv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_fee_title_siv;
@property (strong, nonatomic) IBOutlet UIImageView *img_fee_siv;

@property (strong, nonatomic) IBOutlet UIImageView *selectionTapImageView_siv;
@property (weak, nonatomic) IBOutlet UIButton *socialFacebookButton;
@property (weak, nonatomic) IBOutlet UIButton *socialTwitterButton;
@property (weak, nonatomic) IBOutlet UIButton *socialPinterestButton;
@property (weak, nonatomic) IBOutlet UIButton *socialGoogleButton;
@property (weak, nonatomic) IBOutlet UIButton *socialInstagramButton;
@property (weak, nonatomic) IBOutlet UIButton *socialYoutubeButton;
//@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) IBOutlet UIImageView *about_bg_siv;
@property (strong, nonatomic) IBOutlet UIImageView *contact_bg_siv;
@property (strong, nonatomic) IBOutlet UIImageView *store_bg_siv;
// review info view
@property (strong, nonatomic) IBOutlet UIView *reviewInfoView;
@property (strong, nonatomic) IBOutlet UIView *totalReview_riv;
@property (strong, nonatomic) IBOutlet UIButton *btn_write_review_riv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_reviews_riv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_total_reviews_value_riv;
@property (strong, nonatomic) ReviewTableView *tbl_review_riv;
@property (strong, nonatomic) IBOutlet UILabel *lbl_no_reviews_riv;

@property(nonatomic,strong)IBOutlet LORichTextLabel *lbl_name;
@property(nonatomic,strong)IBOutlet LORichTextLabel *lbl_name_tbl;

//UITableview
//@property(nonatomic,strong)IBOutlet UITableView *tbl_news;
@property(nonatomic,strong)IBOutlet UICollectionView *tbl_news;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow_tbl;
@property(nonatomic,strong)IBOutlet UIButton *btn_follow_siv;
@property(nonatomic,strong)IBOutlet UIButton *btn_friend_tbl;
@property(nonatomic,strong)IBOutlet UIButton *btn_friend;
@property(nonatomic,strong)IBOutlet UIButton *btn_friend_siv;
@property (strong, nonatomic) IBOutlet UIButton *btn_chat1;
@property (strong, nonatomic) IBOutlet UIButton *btn_chat2;
@property (strong, nonatomic) IBOutlet UIButton *btn_chat3;

- (IBAction)show_chatting:(id)sender;
- (IBAction)gotoBigMap:(id)sender;

@property(nonatomic,strong) IBOutlet UIButton *btnAccept;
@property(nonatomic,strong) IBOutlet UIButton *btnNotNow;
@property(nonatomic,strong) IBOutlet UIButton *btnAcceptTbl;
@property(nonatomic,strong) IBOutlet UIButton *btnNotNowTbl;
@property(nonatomic,strong) IBOutlet UIButton *btnNotNowSiv;

@property(nonatomic,strong) UIAlertView *denayAlertViewObj;
@property(nonatomic,strong) IBOutlet UIImageView *imgRequestSentTbl;
@property(nonatomic,strong) IBOutlet UILabel *lblPhotoPrivate;
@property(nonatomic,strong) IBOutlet UILabel *lblFriendRequest;
@property(nonatomic,strong) IBOutlet UILabel *lblFriendRequestTbl;
@property (strong, nonatomic) IBOutlet UILabel *lbl_user_not_found;

@property(nonatomic,strong) IBOutlet UIImageView *img_PostBG;
@property(nonatomic,strong) IBOutlet UIImageView *img_tbl_PostBG;
@property(nonatomic,strong) IBOutlet UIButton *btn_postButton;
@property(nonatomic,strong) IBOutlet UIButton *btn_tbl_postButton;

@property(nonatomic,strong)IBOutlet UIButton*BlockUser_Btn;

@property(nonatomic,strong)IBOutlet UILabel*objUserName_lbl;
@property (strong, nonatomic) IBOutlet UIView *view_content;
@property(nonatomic,strong) Share_photo_ViewController *share_photo_viewObj;
@property(nonatomic,strong) Chat_ViewController *chat_viewObj;
@property(nonatomic,strong) Inbox_list_ViewController *inbox_viewObj;
@property(nonatomic,strong) GoogleMap_ViewController *map_viewObj;

-(IBAction)btn_	:(id)sender;

@end
