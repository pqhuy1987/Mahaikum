//
//  Twitter_login_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>

@interface Twitter_login_ViewController : UIViewController{

    UITextField *txt_username;
    UITextField *txt_password;
}
@property(nonatomic,strong)IBOutlet UITextField *txt_username;
@property(nonatomic,strong)IBOutlet UITextField *txt_password;
@end
