//
//  Find_invite_friend.m
//  My Style
//
//  Created by Tis Macmini on 6/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Find_invite_friend.h"
#import "AppDelegate.h"
@interface Find_invite_friend ()

@end

@implementation Find_invite_friend

@synthesize img_cell_bg,facebook_friendViewObj,contact_friendViewObj,suggested_friend_list;
@synthesize btnFacebook,btnContact,btnUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.img_cell_bg.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    self.img_cell_bg.layer.cornerRadius =4.0f;
    
    self.facebook_friendViewObj =[[Facebook_friend_list alloc]initWithNibName:@"Facebook_friend_list" bundle:nil];
    
        self.contact_friendViewObj =[[Contact_friend_list alloc]initWithNibName:@"Contact_friend_list" bundle:nil];
    self.suggested_friend_list=[[Suggested_friend_list alloc]initWithNibName:@"Suggested_friend_list" bundle:nil];
    // Do any additional setup after loading the view from its nib.
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    if (isiPhone) {
        btnUser.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnContact.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    }else{
        btnUser.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        btnContact.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30.0];
        btnFacebook.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:30.0];
    }
}

-(IBAction)btn_facebook_friend_click:(id)sender{

    AppDelegate *appdelegate =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appdelegate.session closeAndClearTokenInformation];
    facebook_friendViewObj.loadFriends = YES;
    [self.navigationController pushViewController:self.facebook_friendViewObj animated:YES];
}

-(IBAction)btn_contact_click:(id)sender{
    
    [self.navigationController pushViewController:self.contact_friendViewObj animated:YES];
}

-(IBAction)btn_suggested_click:(id)sender{
    [self.navigationController pushViewController:self.suggested_friend_list animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
