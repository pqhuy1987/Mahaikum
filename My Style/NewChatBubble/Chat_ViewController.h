//
//  Chat_ViewController.h
//  NewChatBubble
//
//  Created by Siba Prasad Hota  on 1/3/15.
//  Copyright (c) 2015 Wemakeappz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Edit_your_profile_share.h"

@interface Chat_ViewController : UIViewController<UICollectionViewDelegate,
    UICollectionViewDataSource,
    UITextFieldDelegate>

@property    Edit_your_profile_share *profile_shareObj;

@property (strong, nonatomic) IBOutlet UICollectionView *sphChatTable;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UITextField *messageField;
@property (weak, nonatomic) IBOutlet UIView *msgInPutView;
@property (weak, nonatomic) IBOutlet UIButton *sendChatBtn;
@property NSString *chat_type; //0: Message, 1: Image
@property NSString *fromUserID;
@property NSString *toUserID;
@property NSString *toUserName;
@property NSString *toUserFullName;
@property NSString *message;
@property NSString *readStatus;
@property NSString *sentTime;
@property NSString *receivedTime;
@property NSString *toUserImage;
@property NSString *chat_image;
@property BOOL isImageUploading;

@property UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIButton *btn_upload;
- (IBAction)upload_image:(id)sender;

@property NSString *getType; // 0:init load, 1:get chat info when click more button,  2:get chat info when receive message, 3: get chat info for only login user
@property NSString *boundaryTime;
@property NSString *lastTime;

@property (nonatomic, strong) AmazonS3Client *s3;

- (IBAction)sendMessageNow:(id)sender;
- (IBAction)loadMoreMessage:(id)sender;

@end

