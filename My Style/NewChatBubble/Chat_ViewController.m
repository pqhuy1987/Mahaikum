//
//  Chat_ViewController.m
//  NewChatBubble
//
//  Created by Siba Prasad Hota  on 1/3/15.
//  Copyright (c) 2015 Wemakeappz. All rights reserved.
//

#import "Chat_ViewController.h"
#import "SPHTextBubbleCell.h"
#import "SPHMediaBubbleCell.h"
#import "SPH_PARAM_List.h"
#import "TWPhotoPickerController.h"
#import "SPHCollectionViewcell.h"
#import "iosMacroDefine.h"

static NSString *CellIdentifier = @"cellIdentifier";

@interface Chat_ViewController ()<TextCellDelegate,MediaCellDelegate>
{
    NSMutableArray *sphBubbledata;
    BOOL isfromMe;
}

@end

@implementation Chat_ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"ChatView is viewDidLoad!");

    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];

    isfromMe=YES;
    sphBubbledata =[[NSMutableArray alloc]init];
    
    UINib *cellNib = [UINib nibWithNibName:@"View" bundle:nil];
    [self.sphChatTable registerNib:cellNib forCellWithReuseIdentifier:CellIdentifier];

    //[self SetupDummyMessages];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.sphChatTable addGestureRecognizer:tap];
    self.sphChatTable.backgroundColor =[UIColor clearColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.messageField.leftView = paddingView;
    self.messageField.leftViewMode = UITextFieldViewModeAlways;
    
    //self.fromUserID = self.toUserID = self.message = self.toUserImage = @"";
    //self.getType = @"0";
    //self.boundaryTime = self.sentTime = self.receivedTime = self.lastTime = @"0000-00-00 00:00:00";

    self.isImageUploading = FALSE;
    
    // Do any additional setup after loading the view, typically from a nib.
    self.profile_shareObj=[[Edit_your_profile_share alloc]init];
    
    [self get_current_user_info];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMessages) name:@"reloadMessages" object:nil];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Load more..." attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont systemFontOfSize:17]}];
    [refreshControl addTarget:self action:@selector(loadMoreOldMessage) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor redColor];
    self.refreshControl = refreshControl;
    [self.sphChatTable addSubview:refreshControl];
}

-(void)loadMoreOldMessage
{
    NSLog(@"loadMoreOldMessage...");
    
    self.getType = @"1";
    
    [self get_chat_info];
}

-(void)reloadMessages
{
    NSLog(@"reloadMessages: get chat info received from user.");
    
    self.getType = @"2";

    [self get_chat_info];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (self.isImageUploading)
        return;
    
    self.fromUserID = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];

    NSLog(@"ChatView will appear!->[%@, %@, %@]", self.fromUserID, self.toUserID, self.toUserName);
    
    if (self.fromUserID == self.toUserID)
    {
        self.msgInPutView.hidden = YES;
        [self.sphChatTable setFrame:CGRectMake(0, 70,self.sphChatTable.frame.size.width, self.sphChatTable.frame.size.height + 50)];
    }
    else
    {
        //self.msgInPutView.hidden = NO;
    }

    if ([sphBubbledata count])
        [sphBubbledata removeAllObjects];
        
    self.boundaryTime = self.lastTime = @"0000-00-00 00:00:00";
    self.getType = @"0";
       
    self.lbl_title.text = self.toUserName;

    ((AppDelegate *)[UIApplication sharedApplication].delegate).isChattingRoom = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_chat_info];
}

- (void)viewWillDisappear:(BOOL)animated
{
    ((AppDelegate *)[UIApplication sharedApplication].delegate).isChattingRoom = NO;
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//=========***************************************************=============
#pragma mark - CELL CLICKED  PROCEDURE
//=========***************************************************=============


-(void)textCellDidTapped:(SPHTextBubbleCell *)tesxtCell AndGesture:(UIGestureRecognizer*)tapGR;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tesxtCell.textLabel.tag inSection:0];
    NSLog(@"Forward Pressed =%@ and IndexPath=%@",tesxtCell.textLabel.text,indexPath);
    [tesxtCell showMenu];
}
// 7684097905

-(void)cellCopyPressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"copy Pressed =%@",tesxtCell.textLabel.text);
    
}

-(void)cellForwardPressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"Forward Pressed =%@",tesxtCell.textLabel.text);
    
}
-(void)cellDeletePressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"Delete Pressed =%@",tesxtCell.textLabel.text);
    
}

//=========*******************  BELOW FUNCTIONS FOR IMAGE  **************************=============

-(void)mediaCellDidTapped:(SPHMediaBubbleCell *)mediaCell AndGesture:(UIGestureRecognizer*)tapGR;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:mediaCell.messageImageView.tag inSection:0];
    NSLog(@"Media cell Pressed  and IndexPath=%@",indexPath);

    [mediaCell showMenu];
}

-(void)mediaCellCopyPressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"copy Pressed =%@",mediaCell.messageImageView.image);
    
}

-(void)mediaCellForwardPressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"Forward Pressed =%@",mediaCell.messageImageView.image);
   
}
-(void)mediaCellDeletePressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"Delete Pressed =%@",mediaCell.messageImageView.image);
   
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark               KEYBOARD UPDOWN EVENT
/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (sphBubbledata.count>2) {
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
    }
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height-=210;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.msgInPutView.frame=CGRectMake(0,self.view.frame.size.height-265, self.view.frame.size.width, 50);
        self.sphChatTable.frame=tableviewframe;
    }];
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect tableviewframe=self.sphChatTable.frame;
    tableviewframe.size.height+=210;
    [UIView animateWithDuration:0.25 animations:^{
        self.msgInPutView.frame=CGRectMake(0,self.view.frame.size.height-50,  self.view.frame.size.width, 50);
        self.sphChatTable.frame=tableviewframe;  }];
    if (sphBubbledata.count>2) {
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.25];
    }
    
    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark       SEND MESSAGE PRESSED   
/////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendMessageNow:(id)sender
{
    NSDate *date = [NSDate date];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString *localDateString = [formatter stringFromDate:date];
    
    if ([self.messageField.text length]>0) {
        
        if (isfromMe)
        {
            NSString *rowNum=[NSString stringWithFormat:@"%d",(int)sphBubbledata.count];
            [self adddMediaBubbledata:@"0" mediaType:kSTextByme mediaPath:self.messageField.text mtime:localDateString thumb:@"" downloadstatus:@"" sendingStatus:kSending msg_ID:[self genRandStringLength:7] savedTime: @"0000-00-00 00:00:00" avator:self.profile_shareObj.image chatImage:nil];
            
            [self performSelector:@selector(messageSent:) withObject:rowNum afterDelay:1];
            
            //isfromMe=NO;
            
            NSLog(@"isfromMe!->%@", [self genRandStringLength:7]);
        }
/*        else
        {
            [self adddMediaBubbledata:kTextByOther mediaPath:self.messageField.text mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
            isfromMe=YES;
            NSLog(@"isfromMe is not!");
        }
*/
        self.messageField.text=@"";
        [self.sphChatTable reloadData];
        [self scrollTableview];
    }
}

- (IBAction)loadMoreMessage:(id)sender {
    NSLog(@"load more old messages!");
    //NSLog(@"loadmore--------------[%@]-----------------", self.boundaryTime);
    
    self.getType = @"1";

    [self get_chat_info];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)messageSent:(NSString*)rownum
{
    int rowID=[rownum intValue];
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data=[sphBubbledata objectAtIndex:rowID];
    
    [sphBubbledata  removeObjectAtIndex:rowID];
    feed_data.chat_send_status=kSent;
    [sphBubbledata insertObject:feed_data atIndex:rowID];
    // [self.sphChatTable reloadData];
    
    NSArray *indexPaths = [NSArray arrayWithObjects:
                           [NSIndexPath indexPathForRow:rowID inSection:0],
                           // Add some more index paths if you want here
                           nil];
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.sphChatTable reloadItemsAtIndexPaths:indexPaths];

    [UIView setAnimationsEnabled:animationsEnabled];
    
    if ([self.toUserID length] == 0)
        return;
    
    self.chat_type = feed_data.chat_message_Type;
    self.message = feed_data.chat_message;
    self.sentTime = feed_data.chat_date_time;
    self.receivedTime = @"0000-00-00 00:00:00";
    NSLog(@"sent message!");
    [self sendMessage];
}

- (void)sendMessage
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"1120" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(post_message:) name:@"1120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-1120" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailNewsReson:) name:@"-1120" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@insert_chat_message.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    self.fromUserID = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];
    self.readStatus = @"0";

    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"type": self.chat_type, @"fromUserID": self.fromUserID, @"toUserID": self.toUserID, @"message": self.message, @"readStatus": self.readStatus, @"sentDate": self.sentTime, @"ReceivedDate": self.receivedTime};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"1120" :params];
}

- (void)post_message:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"1120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-1120" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([result[@"success"] isEqualToString:@"1"])
    {
        NSLog(@"post message success!");
        
        [self sendPushMessage]; // Send push message into other user
        
    }
    else
    {
        NSLog(@"can not post message!");
    }
}

-(void)FailNewsReson:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"1120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-1120" object:nil];
}

- (void)sendPushMessage
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"3120" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(send_push_message:) name:@"3120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-3120" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(FailSendPushMessage:) name:@"-3120" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@send_chat_message.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = @{@"sign": sig, @"salt": salt, @"type":self.chat_type, @"fromUserID": self.fromUserID, @"toUserID": self.toUserID, @"message": self.message};
    
    NSLog(@"params:%@",params);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"3120" :params];
}

- (void)send_push_message:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"3120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-3120" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
   
    NSLog(@"send_push_message=>result: %@", result[@"success"]);
    
    if ([result[@"success"] isEqualToString:@"1"])
    {
        NSLog(@"send push message success!");
    }
    else
    {
        NSLog(@"can not send push message!");
    }
}

-(void)FailSendPushMessage:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"3120" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-3120" object:nil];
}

#pragma mark - Get chat info from server

-(void)get_chat_info{
    if ([self.getType isEqualToString:@"0"] || [self.getType isEqualToString:@"2"])
    {
        [SVProgressHUD dismiss];
        [SVProgressHUD show];
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"15209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getChatInfoAPIResponce:) name:@"15209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-15209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildgetChatInfoAPIResponce:) name:@"-15209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_chatitems.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID], @"fromUserID",
                            self.toUserID, @"toUserID",
                            self.getType, @"getType",
                            self.boundaryTime, @"boundaryTime",
                            self.lastTime, @"lastTime",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"15209" :params];
}

-(NSString*)getLocalTimeFromUTC: (NSString*)utcTime
{
    NSString *localTime=@"0000-00-00 00:00:00";
    
    NSDateFormatter *serverFormatter = [[NSDateFormatter alloc] init];
    [serverFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [serverFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *serverTime = [serverFormatter dateFromString:utcTime];
    
    NSDateFormatter *userFormatter = [[NSDateFormatter alloc] init];
    [userFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [userFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    // convert to String
    localTime = [userFormatter stringFromDate:serverTime];
    
    return localTime;
}

-(void)getChatInfoAPIResponce:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"15209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-15209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        if ([dict count] == 0 || dict == nil)
        {
            [SVProgressHUD dismiss];
            [self.refreshControl endRefreshing];
            return;
        }
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        
        NSDictionary *firstTemp = (NSDictionary *)[dict objectAtIndex:0];
        
        if (firstTemp)
            self.boundaryTime = [StaticClass urlDecode:[firstTemp objectForKey:@"savedDate"]];
        
        NSDictionary *lastTemp = (NSDictionary *)[dict objectAtIndex:([dict count] - 1)];
        
        if (lastTemp)
            self.lastTime = [StaticClass urlDecode:[lastTemp objectForKey:@"savedDate"]];
        
        //NSLog(@"boundary time======>[%@, %@], lastMessage=%@", self.boundaryTime, self.lastTime, [StaticClass urlDecode:[lastTemp objectForKey:@"message"]]);
        
        if ([self.getType isEqualToString:@"1"] && self.boundaryTime != nil && ![self.boundaryTime  isEqualToString: @"0000-00-00 00:00:00"] )
        {
            //NSLog(@"There is boundary time!.");
            
            for (int i = [dict count] - 1; i >= 0; i--) {
                NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
                
                NSString *chat_type = [temp objectForKey:@"type"];
                NSString *fromUserID = [temp objectForKey:@"fromUserID"];
                //NSString *toUserID = [temp objectForKey:@"toUserID"];
                NSString *message = [StaticClass urlDecode:[temp objectForKey:@"message"]];
                //NSString *time = [StaticClass urlDecode:[temp objectForKey:@"sentDate"]];
                NSString *time = [self getLocalTimeFromUTC:[StaticClass urlDecode:[temp objectForKey:@"sentDate"]]];
                NSString *saved_time = [StaticClass urlDecode:[temp objectForKey:@"savedDate"]];
                NSString *fromUserImage=[StaticClass urlDecode:[temp objectForKey:@"fromUserImage"]];
                //NSString *toUserImage=[StaticClass urlDecode:[temp objectForKey:@"toUserImage"]];
                NSString *chatMediaType = kSTextByme;
                
                if ([fromUserID isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]])
                {
                    isfromMe = YES;
                }
                else/* if ([fromUserID isEqualToString:self.toUserID])*/
                {
                    isfromMe = NO;
                }
                
                if (isfromMe)
                {
                    if ([chat_type isEqualToString:@"0"])
                        chatMediaType = kSTextByme;
                    else if ([chat_type isEqualToString:@"1"])
                        chatMediaType = kSImagebyme;
                        
                    [self insertMediaBubbledata:chat_type mediaType:chatMediaType mediaPath:message mtime:time thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7] savedTime:saved_time avator:fromUserImage chatImage:nil];
                    
                    //NSLog(@"isfromMe!->%@", [self genRandStringLength:7]);
                }
                else
                {
                    if ([chat_type isEqualToString:@"0"])
                        chatMediaType = kSTextByOther;
                    else if ([chat_type isEqualToString:@"1"])
                        chatMediaType = kSImagebyOther;
                    
                    [self insertMediaBubbledata:chat_type mediaType:chatMediaType mediaPath:message mtime:time thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7] savedTime:saved_time avator:fromUserImage chatImage:nil];
                    //NSLog(@"isfromMe is not!");
                }
            }
        }
        else if (([self.getType isEqualToString:@"0"]) || ([self.getType isEqualToString:@"2"] && self.lastTime != nil && ![self.lastTime  isEqualToString: @"0000-00-00 00:00:00"]) )
        {
            //NSLog(@"There is no boundary time!.");
            
            for (int i = 0; i< [dict count]; i++) {
                NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
                
                NSString *chat_type = [temp objectForKey:@"type"];
                NSString *fromUserID = [temp objectForKey:@"fromUserID"];
                //NSString *toUserID = [temp objectForKey:@"toUserID"];
                NSString *message = [StaticClass urlDecode:[temp objectForKey:@"message"]];
                NSString *time = [self getLocalTimeFromUTC:[StaticClass urlDecode:[temp objectForKey:@"sentDate"]]];
                //NSString *time = [StaticClass urlDecode:[temp objectForKey:@"sentDate"]];
                NSString *saved_time = [StaticClass urlDecode:[temp objectForKey:@"savedDate"]];
                NSString *fromUserImage=[StaticClass urlDecode:[temp objectForKey:@"fromUserImage"]];
                //NSString *toUserImage=[StaticClass urlDecode:[temp objectForKey:@"toUserImage"]];
                NSString *chatMediaType = kSTextByme;

                //NSLog(@"%@,-----[from:%@, to:%@]--", self.profile_shareObj.image, fromUserImage, toUserImage);
                BOOL isDouble = NO;
                
                if ([sphBubbledata count])
                for (int j = 0; j < [sphBubbledata count]; j++)
                {
                    SPH_PARAM_List *feed_data = [sphBubbledata objectAtIndex:j];
                    if (feed_data)
                    {
                        //if ([saved_time isEqualToString:feed_data.chat_savedTime] && [message isEqualToString:feed_data.chat_message])
                        if ([time isEqualToString:feed_data.chat_date_time] && [message isEqualToString:feed_data.chat_message])
                            
                        {
                            isDouble = YES;
                            break;
                        }
                    }
                }
                
                if (isDouble)
                    continue;
                
                if ([fromUserID isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]])
                {
                    isfromMe = YES;
                }
                else /*if ([fromUserID isEqualToString:self.toUserID])*/
                {
                    isfromMe = NO;
                }
                
                NSLog(@"There is no boundary time!.");
                
                if (isfromMe)
                {
                    if ([chat_type isEqualToString:@"0"])
                        chatMediaType = kSTextByme;
                    else if ([chat_type isEqualToString:@"1"])
                        chatMediaType = kSImagebyme;

                    [self adddMediaBubbledata:chat_type mediaType:chatMediaType mediaPath:message mtime:time thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7] savedTime:saved_time avator:fromUserImage chatImage:nil];
                    
                    //NSLog(@"[isfromMe!->%@, %@]", [self genRandStringLength:7], fromUserImage);
                }
                else
                {
                    if ([chat_type isEqualToString:@"0"])
                        chatMediaType = kSTextByOther;
                    else if ([chat_type isEqualToString:@"1"])
                        chatMediaType = kSImagebyOther;

                    [self adddMediaBubbledata:chat_type mediaType:chatMediaType mediaPath:message mtime:time thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7] savedTime:saved_time avator:fromUserImage chatImage:nil];
                    //NSLog(@"[isfromMe is not!, %@]", toUserImage);
                }
            }
        }
        
        isfromMe=YES;
        
        [self.sphChatTable reloadData];
    }
    
    [self.refreshControl endRefreshing];
    
    [SVProgressHUD dismiss];
}

-(void)FaildgetChatInfoAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"15209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-15209" object:nil];
    
    [self.refreshControl endRefreshing];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}

#pragma mark - Get user info

-(void)get_current_user_info{

    if ([self.getType isEqualToString:@"0"] || [self.getType isEqualToString:@"1"])
    {
        //[SVProgressHUD dismiss];
        //[SVProgressHUD show];
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disUserProfileAPIResponce:) name:@"14209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildisUserProfileAPIResponce:) name:@"-14209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@dis_user_profile.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14209" :params];
}

-(void)disUserProfileAPIResponce:(NSNotification *)notification {
    //[SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        NSDictionary *dict =[result valueForKey:@"data"];
        
        self.profile_shareObj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_shareObj.name =[StaticClass urlDecode:[dict valueForKey:@"name"]];
        self.profile_shareObj.photos_status =[StaticClass urlDecode:[dict valueForKey:@"photos_status"]];
        self.profile_shareObj.user_id =[dict valueForKey:@"user_id"];
        self.profile_shareObj.username =[StaticClass urlDecode:[dict valueForKey:@"username"]];
    }
}

-(void)FaildisUserProfileAPIResponce:(NSNotification *)notification {
    //[SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14209" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)scrollTableview
{
    NSInteger item = [self collectionView:self.sphChatTable numberOfItemsInSection:0] - 1;
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:0];
    [self.sphChatTable
     scrollToItemAtIndexPath:lastIndexPath
     atScrollPosition:UICollectionViewScrollPositionBottom
     animated:NO];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)adddMediaBubbledata:(NSString*)messageType mediaType:(NSString*)mediaType  mediaPath:(NSString*)mediaPath mtime:(NSString*)messageTime thumb:(NSString*)thumbUrl  downloadstatus:(NSString*)downloadstatus sendingStatus:(NSString*)sendingStatus msg_ID:(NSString*)msgID savedTime:(NSString*)savedTime avator:userAvator chatImage:(UIImage*)img
{
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data.chat_message_Type=messageType;
    feed_data.chat_message=mediaPath;
    feed_data.chat_date_time=messageTime;
    feed_data.chat_media_type=mediaType;
    feed_data.chat_send_status=sendingStatus;
    feed_data.chat_Thumburl=thumbUrl;
    feed_data.chat_downloadStatus=downloadstatus;
    feed_data.chat_messageID=msgID;
    feed_data.chat_savedTime=savedTime;
    feed_data.chat_avator=userAvator;
    feed_data.chat_image=img;
    [sphBubbledata addObject:feed_data];
}

-(void)insertMediaBubbledata:(NSString*)messageType mediaType:(NSString*)mediaType  mediaPath:(NSString*)mediaPath mtime:(NSString*)messageTime thumb:(NSString*)thumbUrl  downloadstatus:(NSString*)downloadstatus sendingStatus:(NSString*)sendingStatus msg_ID:(NSString*)msgID savedTime:(NSString*)savedTime avator:(NSString*)userAvator chatImage:(UIImage*)img
{
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data.chat_message_Type=messageType;
    feed_data.chat_message=mediaPath;
    feed_data.chat_date_time=messageTime;
    feed_data.chat_media_type=mediaType;
    feed_data.chat_send_status=sendingStatus;
    feed_data.chat_Thumburl=thumbUrl;
    feed_data.chat_downloadStatus=downloadstatus;
    feed_data.chat_messageID=msgID;
    feed_data.chat_savedTime=savedTime;
    feed_data.chat_avator=userAvator;
    feed_data.chat_image=img;
    
    [sphBubbledata insertObject:feed_data atIndex:0];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark  GENERATE RANDOM ID to SAVE IN LOCAL
/////////////////////////////////////////////////////////////////////////////////////////////////////


-(NSString *) genRandStringLength: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

- (void)postImage : (UIImage *)newImage rowNumber:(NSString*)rowNum
{
    NSLog(@"s3 Start Time:%@",[NSDate date]);

//    if (newImage.size.width > 200 || newImage.size.height > 200)
//        newImage = [newImage resizedImage:CGSizeMake(200,200) interpolationQuality:3];
    
    NSData *img_data = UIImageJPEGRepresentation(newImage, 1.0);
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSString *dateString1 = [GlobalDefine toDateTimeStringFromDateWithFormat:[NSDate date] formatString:@"yyyy-MM-dd hh:mm:ss"];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                [SVProgressHUD dismiss];
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
            }
            else {
                NSLog(@"s3 End Time:%@",[NSDate date]);
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;

                int rowID=[rowNum intValue];
                SPH_PARAM_List *feed_data=[sphBubbledata objectAtIndex:rowID];
                feed_data.chat_message = imageKeys;

                NSDate *date = [NSDate date];
                NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setTimeZone:timeZone];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                
                NSString *localDateString = [formatter stringFromDate:date];
                feed_data.chat_date_time = localDateString;
                
                [self performSelector:@selector(messageSent:) withObject:rowNum afterDelay:1];
            }
        });
    });
}

- (IBAction)upload_image:(id)sender {
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    self.isImageUploading = TRUE;

    photoPicker.cancelBlock = ^() {
        self.isImageUploading = FALSE;
        ((AppDelegate *)[UIApplication sharedApplication].delegate).isChattingRoom = YES;
    };
    
    photoPicker.cropBlock = ^(UIImage *image) {
        //do something
        if (image == nil) return;
        NSLog(@"image uploading(%f, %f)...", image.size.width, image.size.height);
        
        NSString *rowNum=[NSString stringWithFormat:@"%d",(int)sphBubbledata.count];
        
        NSDate *date = [NSDate date];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSString *localDateString = [formatter stringFromDate:date];
        
        [self adddMediaBubbledata: @"1" mediaType:kSImagebyme mediaPath:@"imageURL" mtime:localDateString thumb:@"" downloadstatus:@"" sendingStatus:kSending msg_ID:@"ABFCXYZ" savedTime:@"0000-00-00 00:00:00" avator:self.profile_shareObj.image chatImage:image];
        
        [self.sphChatTable reloadData];
        [self scrollTableview];

        [self postImage:image rowNumber:rowNum];
        
        self.isImageUploading = FALSE;
        ((AppDelegate *)[UIApplication sharedApplication].delegate).isChattingRoom = YES;
    };
    
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    [navCon setNavigationBarHidden:YES];
    
    [self presentViewController:navCon animated:YES completion:NULL];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////******* COLLECTION VIEW DELEGATE METHODS           **************/////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    
    if ([feed_data.chat_media_type isEqualToString:kSTextByme]||[feed_data.chat_media_type isEqualToString:kSTextByOther])
    {
        
        NSStringDrawingContext *ctx = [NSStringDrawingContext new];
        NSAttributedString *aString = [[NSAttributedString alloc] initWithString:feed_data.chat_message];
        UITextView *calculationView = [[UITextView alloc] init];
        [calculationView setAttributedText:aString];
        CGRect textRect = [calculationView.text boundingRectWithSize: CGSizeMake(TWO_THIRDS_OF_PORTRAIT_WIDTH, 10000000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:calculationView.font} context:ctx];
        
        return CGSizeMake(306,textRect.size.height+40);
    }
    else if ([feed_data.chat_media_type isEqualToString:kSImagebyme]||[feed_data.chat_media_type isEqualToString:kSImagebyOther])
    {
        //return CGSizeMake(306, feed_data.chat_image.size.height);
    }

    
    
    return CGSizeMake(306, 100);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return sphBubbledata.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SPHCollectionViewcell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       for (UIView *v in [cell.contentView subviews])
                           [v removeFromSuperview];
                       
                       if ([self.sphChatTable.indexPathsForVisibleItems containsObject:indexPath])
                       {
                           [cell setFeedData:(SPH_PARAM_List*)[sphBubbledata objectAtIndex:indexPath.row]];
                       }
                   });
    return cell;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////******* COLLECTION VIEW DELEGATE ENDS     **************/////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
@end
