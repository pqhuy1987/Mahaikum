//
//  Inbox_list_ViewController.m
//  Mahalkum
//
//  Created by user on 7/12/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Inbox_list_ViewController.h"
#import "Inbox_info.h"
#import "Inbox_list_cell.h"

@interface Inbox_list_ViewController()<AVTagTextViewDelegate,Inbox_list_CellDelegate> {
    NSMutableArray *array_inboxs;
}

@end

@interface Inbox_list_ViewController (PrivateStuff)

@end

@implementation Inbox_list_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (IBAction)btn_back_click:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    array_inboxs = [[NSMutableArray alloc]init];
    
    self.chat_viewObj=[[Chat_ViewController alloc]initWithNibName:@"Chat_ViewController" bundle:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
   self.lbl_title.text = @"Me";
    
   if ([array_inboxs count])
        [array_inboxs removeAllObjects];

    ((AppDelegate *)[UIApplication sharedApplication].delegate).isChattingRoom = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self get_inbox_info];
}

#pragma mark - Get inbox info from server for current user

-(void)get_inbox_info{
    [SVProgressHUD dismiss];
    [SVProgressHUD show];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getInboxInfoAPIResponce:) name:@"19209" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19209" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildgetInboxInfoAPIResponce:) name:@"-19209" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_chatting_inbox.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            self.userID, @"uid",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"19209" :params];
}

-(void)getInboxInfoAPIResponce:(NSNotification *)notification {
    
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19209" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        if ([dict count] == 0 || dict == nil)
        {
            return;
        }
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        
        for (int i = 0; i< [dict count]; i++) {
            NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
            
            NSString *chat_id = [temp objectForKey:@"id"];
            NSString *fromUserName = [temp objectForKey:@"fromUserName"];
            NSString *fromUserImage=[StaticClass urlDecode:[temp objectForKey:@"fromUserImage"]];
            NSString *message = [StaticClass urlDecode:[temp objectForKey:@"message"]];
            NSString *time = [StaticClass urlDecode:[temp objectForKey:@"sentDate"]];
            NSString *fromUserID = [temp objectForKey:@"fromUserID"];
            
            NSLog(@"Inbox->%@", fromUserName);
            
            [self addInboxData:chat_id username:fromUserName image_url:fromUserImage chat_content:message datecreated:time uid:fromUserID];
        }
        
        [self.inboxChatTable reloadData];
    }
}

-(void)FaildgetInboxInfoAPIResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"19209" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-19209" object:nil];
    
    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
}

-(void)addInboxData:(NSString*)chat_id username:(NSString*)UserName image_url:(NSString*)imageURL chat_content:(NSString*)message datecreated:(NSString*)dateTime uid:(NSString*)user_id
{
    Inbox_info *inbox_data = [[Inbox_info alloc]init];
    inbox_data.chat_id = chat_id;
    inbox_data.username = UserName;
    inbox_data.image_url=imageURL;
    inbox_data.chat_content=message;
    inbox_data.datecreated=dateTime;
    inbox_data.uid=user_id;
    
    [array_inboxs addObject:inbox_data];
}

#pragma mark UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger indCount = indexPath.row;
    Inbox_info *obj = [array_inboxs objectAtIndex:indCount];
    
    CGSize size =[obj.chat_content sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0] constrainedToSize:CGSizeMake(255, HUGE_VALL) lineBreakMode:NSLineBreakByWordWrapping];
    
    if (size.height<25) {
        return 58.0f;
    }
    return 30.0f+size.height+20.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array_inboxs.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return Nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [self Inbox_list_CellForTableView:tableView atIndexPath:indexPath];
    
    return cell;
}

- (Inbox_list_cell *)Inbox_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Inbox_list_cell";
    self.objInbox_list_cell = (Inbox_list_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    self.objInbox_list_cell.customdelegate = self;
    if(self.objInbox_list_cell == nil)
    {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Inbox_list_cell" owner:self options:nil];
        self.objInbox_list_cell = [nib objectAtIndex:0];
        [self.objInbox_list_cell draw_in_cell];
        self.objInbox_list_cell.showsReorderControl = NO;
        self.objInbox_list_cell.selectionStyle = UITableViewCellSelectionStyleNone;
        self.objInbox_list_cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"ChatBack.png"]];
        
        [self.objInbox_list_cell.btn_user_name addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
        [self.objInbox_list_cell.img_user1 addTarget:self action:@selector(btn_user_info_click:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    Inbox_info *shareObj = [array_inboxs objectAtIndex:indexPath.row];
    
    self.objInbox_list_cell.img_user1.tag = indexPath.row;
    self.objInbox_list_cell.btn_user_name.tag = indexPath.row;
    [self.objInbox_list_cell.img_user sd_setImageWithURL:[NSURL URLWithString:shareObj.image_url] placeholderImage:nil];
    
    self.objInbox_list_cell.img_user.layer.cornerRadius =22.0f;
    self.objInbox_list_cell.img_user.layer.masksToBounds =YES;
    [self.objInbox_list_cell.btn_user_name setTitle:shareObj.username forState:UIControlStateNormal];
    
    self.objInbox_list_cell.lbl_date.text =[self get_time_different:shareObj.datecreated];
    self.objInbox_list_cell.lbl_desc.text=shareObj.chat_content;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:self.objInbox_list_cell.lbl_desc.text];
    
    [self.objInbox_list_cell.lbl_desc setAttributedText:string];
    CGSize size =[shareObj.chat_content sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0] constrainedToSize:CGSizeMake(255, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    if (size.height<24) {
        self.objInbox_list_cell.view_line.frame = CGRectMake(0,54, kViewWidth,1);
    } else {
        self.objInbox_list_cell.view_line.frame = CGRectMake(0,34.f+size.height,kViewWidth,1);
    }
    
    self.objInbox_list_cell.view_line.hidden=YES;
    
    if (size.height<24)
    {
        self.objInbox_list_cell.lbl_desc.frame=CGRectMake(58,30, 255,24);
    }
    else
    {
        self.objInbox_list_cell.lbl_desc.frame=CGRectMake(58,30, 255, size.height);
    }
    
    self.objInbox_list_cell.tag=indexPath.row;
    self.objInbox_list_cell.lbl_desc.delegate = self;
    
    return self.objInbox_list_cell;
}

- (void)btn_user_info_click:(id)sender {
    
    NSLog(@"go to chatting room!");
    
    Inbox_info *shareObj=[array_inboxs objectAtIndex:((UIButton *)sender).tag];
    
    self.chat_viewObj.toUserID = shareObj.uid;
    self.chat_viewObj.toUserFullName = shareObj.name;
    self.chat_viewObj.toUserName = shareObj.username;
    self.chat_viewObj.toUserImage = shareObj.image_url;
    
    [self presentViewController:self.chat_viewObj animated:YES completion:nil];
}

#pragma mark  Date Convert to s,m,h,d,M,y
- (NSString *)get_time_different:(NSString *)datestring {
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

@end
