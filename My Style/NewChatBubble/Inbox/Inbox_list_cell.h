//
//  Comments_list_cell.h
//  My Style
//
//  Created by Tis Macmini on 4/29/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "STXAttributedLabel.h"
#import "Inbox_info.h"

@class Inbox_list_cell;

@protocol Inbox_list_CellDelegate <NSObject>

@optional

-(void)didSlectInbox:(Inbox_list_cell*)cell;

@end

@interface Inbox_list_cell : MGSwipeTableCell <UITextViewDelegate>{

    UIImageView *img_user;
    UIButton *btn_user_name;
    UITextView *lbl_desc;
    
    UIView *view_line;
    UIButton *img_user1;
}

@property(nonatomic,strong) NSMutableArray *array_comments;

@property (nonatomic) NSInteger totalComments;
@property(nonatomic,strong)id <Inbox_list_CellDelegate> customdelegate;
@property(nonatomic,strong)IBOutlet UIView *view_line;
@property(nonatomic,strong)IBOutlet  UILabel *lbl_date;
@property (strong, nonatomic) Inbox_info *comment;
@property(nonatomic,strong)IBOutlet UIImageView *img_user;
@property(nonatomic,strong)IBOutlet UIButton *btn_user_name;
@property (strong, nonatomic) IBOutlet UITextView *lbl_desc;

@property (strong, nonatomic) IBOutlet UIImageView *img_back;

@property(nonatomic,strong)IBOutlet UIButton *img_user1;
-(void)draw_in_cell;


@end
