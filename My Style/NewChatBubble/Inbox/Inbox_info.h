//
//  Inbox_info.h
//  My Style
//
//  Created by Tis Macmini on 4/20/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Inbox_info : NSObject{

    NSString *chat_content;
    NSString *datecreated;
    NSString *chat_id;
    NSString *image_url;
    NSString *name;
    NSString *uid;
    NSString *username;
}
@property(nonatomic,strong) NSString *chat_content;
@property(nonatomic,strong) NSString *datecreated;
@property(nonatomic,strong) NSString *chat_id;
@property(nonatomic,strong) NSString *image_url;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *uid;
@property(nonatomic,strong) NSString *username;
@end
