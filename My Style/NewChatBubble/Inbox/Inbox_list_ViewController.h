//
//  Inbox_list_ViewController.h
//  Mahalkum
//
//  Created by user on 7/12/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#ifndef Inbox_list_ViewController_h
#define Inbox_list_ViewController_h

#import "Inbox_list_cell.h"
#import "Chat_ViewController.h"

#endif /* Inbox_list_ViewController_h */

@class Inbox_list_ViewController;

@protocol Inbox_list_viewcontrollerDelegate <NSObject>

@optional


@end

@interface Inbox_list_ViewController : UIViewController<AVTagTextViewDelegate,UITextViewDelegate>
{
}
@property NSString *userID;

@property Inbox_list_cell *objInbox_list_cell;

@property(nonatomic,strong) Chat_ViewController *chat_viewObj;

@property (strong, nonatomic) IBOutlet UITableView *inboxChatTable;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;

- (Inbox_list_cell *)Inbox_list_CellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;

@end
