//
//  Find_invite_friend.h
//  My Style
//
//  Created by Tis Macmini on 6/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Facebook_friend_list.h"
#import "Contact_friend_list.h"
#import "Suggested_friend_list.h"
@interface Find_invite_friend : UIViewController{

    UIImageView *img_cell_bg;
    Facebook_friend_list *facebook_friendViewObj;
    Contact_friend_list *contact_friendViewObj;
    Suggested_friend_list *suggested_friend_list;
    
    UIButton *btnFacebook,*btnContact,*btnUser;
}
@property(nonatomic,strong)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,strong)Facebook_friend_list *facebook_friendViewObj;
@property(nonatomic,strong)Contact_friend_list *contact_friendViewObj;
@property(nonatomic,strong) Suggested_friend_list *suggested_friend_list;
@property(nonatomic,strong) IBOutlet UIButton *btnFacebook,*btnContact,*btnUser;

@end
