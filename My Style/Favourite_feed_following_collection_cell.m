//
//  Favourite_feed_following_collection_cell.m
//  My Style
//
//  Created by Tis Macmini on 6/18/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Favourite_feed_following_collection_cell.h"

@implementation Favourite_feed_following_collection_cell
@synthesize btn_user_img,lbl_time,lbl_dec,imgBottomImage;


-(void)draw_collectionview_in_cell{
    
    self.lbl_dec = [[LORichTextLabel alloc] initWithWidth:kViewWidth];
	[self.lbl_dec setFont:[UIFont fontWithName:@"Helvetica" size:10.0]];
	[self.lbl_dec setTextColor:[UIColor blackColor]];
	[self.lbl_dec setBackgroundColor:[UIColor clearColor]];
	[self.lbl_dec positionAtX:63.0 andY:5.0];
    [self.contentView addSubview:self.lbl_dec];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    layout.minimumLineSpacing = 1.1f;
    layout.minimumInteritemSpacing =0.1f;
    layout.itemSize = CGSizeMake(50,50);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.collectionView = [[Collectionview_delegate alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    self.collectionView.backgroundColor = [UIColor clearColor];
  //  self.collectionView.showsHorizontalScrollIndicator = NO;
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    [self.contentView addSubview:self.collectionView];
}
/*
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.collectionView.frame = CGRectMake(0, 50, 320, 100);
}
*/
-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index
{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.index = index;
    
    [self.collectionView reloadData];
}

@end
