//
//  User_info_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/28/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "User_info_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"
#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"
#import "BlockUserRecord.h"
#import "ORGContainerCell.h"
#import "WriteReviewViewController.h"
#import "Category_info_ViewController.h"
#import "Chameleon.h"
#import "MXScrollView.h"
#import "OpenInGoogleMapsController.h"
#import "Enums.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MDDirectionService.h"
#import "MJCollectionViewCell.h"
#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopupPreview/STPopupPreview.h>

@implementation UILabel (UILabel_Auto)

- (void)adjustHeight {
    
    if (self.text == nil) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, 0);
        return;
    }
    
    CGSize aSize = self.bounds.size;
    CGSize tmpSize = CGRectInfinite.size;
    tmpSize.width = aSize.width;
    
    tmpSize = [self.text sizeWithFont:self.font constrainedToSize:tmpSize];
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, aSize.width, tmpSize.height);
}
@end

#define screen_width     [[UIScreen mainScreen] bounds].size.width
static NSInteger cellClickIndex=0;
static NSInteger selectedItem = 0;

@interface User_info_ViewController () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, UITableViewDelegate, UITableViewDataSource, ReviewTableViewDelegate, STXSectionHeaderViewDelegate, GMSMapViewDelegate, UIGestureRecognizerDelegate, STPopupPreviewRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSString *image_url;
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    BlockUserRecord*objBlockUserRecord;
    NSString*Block_User_Tittle_Str;
    HCSStarRatingView *starRatingView;
    UIScrollView *infoScrollView;
    CGSize reviewTableViewContentSize;
    Category_info_ViewController *categoryInfoView;
    CLLocationCoordinate2D  startlocation;
    CLLocationCoordinate2D destinationlocation;
    GMSMapView  *googlemapview;
    BOOL isUserPrivate;
    BOOL isOweNerorNot;
    int currentSelectedIndex;
}

@property(nonatomic,strong)NSString *image_url;

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;
@property (strong, nonatomic) IBOutlet UIView *headerView1;
@property (strong, nonatomic) IBOutlet UIView *headerView2;
@property (strong, nonatomic) IBOutlet UIView *headerView3;

@end

@implementation User_info_ViewController

@synthesize collectionViewObj,btn_friend_tbl,btn_friend,btn_friend_siv;
@synthesize view_header,img_photo,img_bg_firstcell,img_bg_scondcell,view_img_photo;
@synthesize view_header_tbl,img_photo_tbl,img_photo_tbl_bg,img_photo_tbl_bg_list,img_bg_firstcell_tbl,img_bg_scondcell_tbl,view_img_photo_tbl,btnAccept,btnNotNow,btnAcceptTbl,btnNotNowTbl,btnNotNowSiv;

@synthesize tbl_news,imgRequestSent,denayAlertViewObj;
@synthesize mapviewObj,lblFriendRequest,lblFriendRequestTbl;

@synthesize view_footer,view_footer_tbl;
@synthesize array_feeds,img_like_heart;
@synthesize liker_list_viewObj,comments_list_viewObj,share_photo_view,image_detail_viewObj,followers_list_viewObj,edit_your_profile_viewObj,setting_profileViewObj, category_list_view;

@synthesize profile_share_obj;
@synthesize lbl_total_followers,lbl_total_followings,lbl_total_photos,lbl_total_categories,lbl_total_followers_tbl,lbl_total_followings_tbl,lbl_total_photos_tbl,lbl_total_categories_tbl;
@synthesize lbl_name,lbl_name_tbl,image_url;
@synthesize photoPickerController;
@synthesize view_secondcell,view_secondcell_tbl;
@synthesize img_down_arrow,img_down_arrow_tbl,user_id;
@synthesize btn_follow,btn_follow_tbl, btn_follow_siv,imgRequestSentTbl,lblPhotoPrivate;

@synthesize username_style,imgfullscreenviewobj;
@synthesize btn_edit,btn_edit_tbl,lblfollowers,lblfollowers_tbl,lblfollowing,lblfollowing_tbl,lblheader,lblphotos_tbl,lblphotos;

@synthesize img_PostBG,img_tbl_PostBG,btn_postButton,btn_tbl_postButton;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentSelectedIndex = 0;
    
    self.composed_feeds = [[NSMutableArray alloc] init];
//    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.tbl_news registerClass:[ORGContainerCell class] forCellReuseIdentifier:@"ORGContainerCell"];
    
    infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 66, kViewWidth, kViewHeight)];
    
    self.tbl_news.parallaxHeader.view = self.headerView1;
    self.tbl_news.parallaxHeader.height = 177;
    self.tbl_news.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.tbl_news.parallaxHeader.minimumHeight = 0;
    
    self.collectionViewObj.parallaxHeader.view = self.headerView2;
    self.collectionViewObj.parallaxHeader.height = 177;
    self.collectionViewObj.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.collectionViewObj.parallaxHeader.minimumHeight = 0;
    

    infoScrollView.showsVerticalScrollIndicator = YES;
    infoScrollView.scrollEnabled = YES;
    infoScrollView.userInteractionEnabled = YES;
    infoScrollView.hidden = YES;
    [self.view addSubview:infoScrollView];
    infoScrollView.contentSize = CGSizeMake(kViewWidth, self.view.frame.size.height * 1.12f);
    
    [infoScrollView addSubview:self.sellerInfoView];
    self.sellerInfoView.hidden = YES;
    self.sellerInfoView.frame = CGRectMake(0.0f, 0.0, kViewWidth, self.view.frame.size.height - 64.0f - 50.0f);
    
    [infoScrollView addSubview:self.reviewInfoView];
    self.reviewInfoView.hidden = YES;
    self.reviewInfoView.frame = CGRectMake(0.0f, 40.0, kViewWidth, 200.0);
    
    infoScrollView.parallaxHeader.view = self.headerView3;
    infoScrollView.parallaxHeader.height = 177;
    infoScrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    infoScrollView.parallaxHeader.minimumHeight = 0;

    starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.totalReview_riv.frame.size.width, self.totalReview_riv.frame.size.height)];
    starRatingView.opaque = NO;
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 0;
    starRatingView.value = 0;
    starRatingView.tintColor = [GlobalDefine colorWithHexString:@"1fae38"];
    starRatingView.userInteractionEnabled = NO;
    [self.totalReview_riv addSubview:starRatingView];
    self.totalReview_riv.backgroundColor = [UIColor clearColor];
    
    self.tbl_review_riv = [[ReviewTableView alloc] initWithFrame:CGRectMake(8.0f, 41.0f, kViewWidth-10, 100) style:UITableViewStylePlain];
    self.tbl_review_riv.customeDelegate = self;
    [self.reviewInfoView addSubview:self.tbl_review_riv];
    
    categoryInfoView = [[Category_info_ViewController alloc] initWithNibName:@"Category_info_ViewController" bundle:nil];
    
//    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
//    self.tbl_news.dataSource = dataSource;
//    self.tableViewDataSource = dataSource;
//    
//    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
//    self.tbl_news.delegate = delegate;
//    self.tableViewDelegate = delegate;
    
    self.btn_edit.hidden = YES;
    self.btn_edit_tbl.hidden = YES;
    self.btn_edit_siv.hidden = YES;
    
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    lblphotos.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lblfollowing.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lblfollowers.font = [UIFont fontWithName:@"Helvetica" size:14.0];

    lblphotos_tbl.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lblfollowing_tbl.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lblfollowers_tbl.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    
    self.view.backgroundColor =[UIColor whiteColor];

    hash_tag_viewObj =[[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view=[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj =[[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    
    photoPickerController = [[UIImagePickerController alloc] init];
    photoPickerController.delegate = self;
    photoPickerController.allowsEditing=YES;
    
    self.profile_share_obj =[[Profile_share alloc]init];
    
    self.array_feeds =[[NSMutableArray alloc]init];
    self.array_promotions = [[NSMutableArray alloc] init];
    [self.img_like_heart setHidden:YES];
    
    self.img_down_arrow.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    self.img_down_arrow_tbl.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(45));
    
    self.followers_list_viewObj =[[Followers_list alloc]initWithNibName:@"Followers_list" bundle:nil];
    self.category_list_view = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil];
    self.share_photo_view=[[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    
    self.comments_list_viewObj =[[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    
    self.liker_list_viewObj =[[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    self.image_detail_viewObj=[[Image_detail alloc]initWithNibName:@"Image_detail" bundle:nil];
    
    self.edit_your_profile_viewObj =[[Edit_your_profile alloc]initWithNibName:@"Edit_your_profile" bundle:nil];
    self.setting_profileViewObj=[[Setting_profile alloc]initWithNibName:@"Setting_profile" bundle:nil];
    
    //   self.tbl_news.hidden =YES;
    self.mapviewObj =[[Show_map_ViewController alloc]initWithNibName:@"Show_map_ViewController" bundle:nil];
    
    UINib *cellNib = [UINib nibWithNibName:@"image_collection_cell" bundle:nil];
    [self.collectionViewObj registerNib:cellNib forCellWithReuseIdentifier:@"image_collection_cell"];
    
    UINib *cellNib_tbl = [UINib nibWithNibName:@"MJCollectionViewCell" bundle:nil];
    [self.tbl_news registerNib:cellNib_tbl forCellWithReuseIdentifier:@"MJCollectionViewCell"];

    UINib *cellNib_header = [UINib nibWithNibName:@"image_collection_header" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];
    [self.tbl_news registerNib:cellNib_header forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header"];

    UINib *cellNib_footer = [UINib nibWithNibName:@"image_collection_footer" bundle:nil];
    
    [self.collectionViewObj registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];
    [self.tbl_news registerNib:cellNib_footer forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer"];

   // self.img_photo.placeholderImage =[UIImage imageNamed:@"default_user_image"];
   // self.img_photo_tbl.placeholderImage =[UIImage imageNamed:@"default_user_image"];
    
    //Header Name and bio
//    self.lbl_name = [[LORichTextLabel alloc] initWithWidth:screen_width -20];
//    [self.lbl_name setBackgroundColor:[UIColor clearColor]];
//    [self.lbl_name setTextColor:[UIColor blackColor]];
//    [self.lbl_name positionAtX:10.0 andY:170.0];//15,90
//    [self.view_header addSubview:self.lbl_name];
    
//    self.lbl_name_tbl = [[LORichTextLabel alloc] initWithWidth:screen_width -20];
//    [self.lbl_name_tbl setBackgroundColor:[UIColor clearColor]];
//    [self.lbl_name_tbl setTextColor:[UIColor blackColor]];
//    [self.lbl_name_tbl positionAtX:10.0 andY:170.0];//15,90
//
//    [self.view_header_tbl addSubview:self.lbl_name_tbl];
    
//    self.tbl_news.tableHeaderView =self.view_header_tbl;
//    self.tbl_news.tableFooterView = self.view_footer_tbl;
    
    image_upload_flag=0;
    
    self.username_style = [LORichTextLabelStyle styleWithFont:[UIFont boldSystemFontOfSize:17.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    self.username_style.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    
    LORichTextLabelStyle *hashStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [hashStyle addTarget:self action:@selector(hashSelected:)];
    
    LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:121.0f/255.0f green:121.0f/255.0f blue:121.0f/255.0f alpha:1]];
    [atStyle addTarget:self action:@selector(atSelected:)];

    
    LORichTextLabelStyle  *url_style = [LORichTextLabelStyle styleWithFont:[UIFont systemFontOfSize:14.0f] color:[UIColor colorWithRed:41.0f/255.0f green:114.0f/255.0f blue:182.0f/255.0f alpha:1.0f]];
    
    [url_style addTarget:self action:@selector(urlSelected:)];
    
    [self.lbl_name addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"www."];
    [self.lbl_name addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name addStyle:url_style forPrefix:@"Https://"];
    
    [self.lbl_name addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name addStyle:atStyle forPrefix:@"@"];
    
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"WWW."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Www."];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Http://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"https://"];
    [self.lbl_name_tbl addStyle:url_style forPrefix:@"Https://"];
    
    [self.lbl_name_tbl addStyle:hashStyle forPrefix:@"#"];
    [self.lbl_name_tbl addStyle:atStyle forPrefix:@"@"];
    
//    [self.collectionViewObj addSubview:lblPhotoPrivate];
    [self block_user_list];
    
    self.chat_viewObj=[[Chat_ViewController alloc]initWithNibName:@"Chat_ViewController" bundle:nil];
    self.inbox_viewObj=[[Inbox_list_ViewController  alloc]initWithNibName:@"Inbox_list_ViewController" bundle:nil];
    
    self.map_viewObj=[[GoogleMap_ViewController alloc]initWithNibName:@"GoogleMap_ViewController" bundle:nil];
    
    UISwipeGestureRecognizer *gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureRight:)];
    [gesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:gesture];
    
    UISwipeGestureRecognizer *gesture1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(gestureLeft:)];
    [gesture1 setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:gesture1];

    
}

#pragma mark - STPopupPreviewRecognizerDelegate

- (UIViewController *)previewViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    if (![popupPreviewRecognizer.view isKindOfClass:[image_collection_cell class]]) {
        return nil;
    }
    
    image_collection_cell *cell = popupPreviewRecognizer.view;
    
    PreviewViewController *previewVC = [[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];;
    
    previewVC.showImage = cell.img_photo.image;
    previewVC.data = cell.data;
    return previewVC;
}

- (UIViewController *)presentingViewControllerForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    return self;
}

- (NSArray<STPopupPreviewAction *> *)previewActionsForPopupPreviewRecognizer:(STPopupPreviewRecognizer *)popupPreviewRecognizer
{
    image_collection_cell *cell = popupPreviewRecognizer.view;
    int nIndexCell = 0;
    for (Home_tableview_data_share *dataObj in self.image_detail_viewObj.arrayFeedArray) {
        if ([dataObj.image_id isEqualToString:cell.data.image_id]) {
            break;
        }
        nIndexCell++;
    }
    Home_tableview_data_share *cmpObj = [self.image_detail_viewObj.arrayFeedArray objectAtIndex:nIndexCell];
    BOOL isLiked = [cmpObj.liked isEqualToString:@"yes"] ? YES : NO;
    return @[ [STPopupPreviewAction actionWithTitle: isLiked ? @"Unlike":@"Like" style:STPopupPreviewActionStyleDefault handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        
        Home_tableview_data_share *obj = cell.data;
        obj.liked = @"yes";
        [self.image_detail_viewObj.arrayFeedArray replaceObjectAtIndex:nIndexCell withObject:obj];
        
        [[[UIAlertView alloc] initWithTitle:isLiked ? @"Unliked" :@"Liked" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];

    }], [STPopupPreviewAction actionWithTitle:@"Comment" style:STPopupPreviewActionStyleDestructive handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        
        image_collection_cell *cell = popupPreviewRecognizer.view;
        Home_tableview_data_share *obj = cell.data;
        //  self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
        self.comments_list_viewObj.image_id=obj.image_id;
        self.comments_list_viewObj.array_comments=obj.array_comments;
        [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
        
    }], [STPopupPreviewAction actionWithTitle:@"Cancel" style:STPopupPreviewActionStyleCancel handler:^(STPopupPreviewAction *action, UIViewController *previewViewController) {
        [[[UIAlertView alloc] initWithTitle:@"Cancelled" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }] ];
}


- (void)gestureLeft:(id)sender
{
    currentSelectedIndex = ( currentSelectedIndex + 1 ) % 4;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
    else if ( currentSelectedIndex == 2 )
    {
        [self btn_reviewinfo_click:self];
    }
    else if ( currentSelectedIndex == 3 )
    {
        [self btn_sellerinfo_click:self];
    }
}

- (void)gestureRight:(id)sender
{
    if ( currentSelectedIndex == 0 )
        currentSelectedIndex = 4;
    
    currentSelectedIndex = ( currentSelectedIndex - 1 ) % 4;
    
    if ( currentSelectedIndex == 0 )
    {
        [self btn_collectionview_click:self];
    }
    else if ( currentSelectedIndex == 1 )
    {
        [self btn_tableview_click:self];
    }
    else if ( currentSelectedIndex == 2 )
    {
        [self btn_reviewinfo_click:self];
    }
    else if ( currentSelectedIndex == 3 )
    {
        [self btn_sellerinfo_click:self];
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    lblPhotoPrivate.hidden=YES;
    isUserPrivate = NO;

    self.navigationController.navigationBarHidden = YES;
    
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;

    btnNotNowSiv.hidden = YES;

    imgRequestSent.hidden=YES;
    imgRequestSentTbl.hidden=YES;

    btn_follow.hidden=YES;
    btn_follow_tbl.hidden=YES;
    btn_follow_siv.hidden = YES;
    
    btn_friend.hidden=YES;
    btn_friend_tbl.hidden=YES;
    btn_friend_siv.hidden=YES;
    
    lblFriendRequest.hidden=YES;
    lblFriendRequestTbl.hidden=YES;
    
//    [self.collectionViewObj setContentOffset:CGPointZero];
//    [self.tbl_news setContentOffset:CGPointZero];

    img_photo.layer.cornerRadius =55.0f;
    img_photo.layer.masksToBounds =YES;
    img_photo_tbl.layer.cornerRadius =55.0f;
    img_photo_tbl.layer.masksToBounds =YES;
    self.img_photo_siv.layer.cornerRadius = 55.0f;
    self.img_photo_siv.layer.masksToBounds =YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSelectItemFromCollectionView:) name:@"didSelectItemFromCollectionView" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self get_user_info];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didSelectItemFromCollectionView" object:nil];
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObj=[array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked = (int)userActionCell.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}

-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}

-(void)userWillComment:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:userActionCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)userWillShare:(STXUserActionCell *)userActionCell {
    
    NSInteger tag = (int)userActionCell.tag;// ((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            //  [alert release];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
                //  [controller release];
            } else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                //  [alert release];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        //report_id : 1= i dont like this photo,  2=this photo is spam or a scam,  3=this photo puts people at risk,  4=this photo shouldn't be on mystyle
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        //[alert release];
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
            //  [controller release];
        } else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            //  [alert release];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
    
    NSLog(@"OTHER CLICKED");
}

-(void)commentCellWillShowAllComments:(STXCommentCell *)commentCell {
    Home_tableview_data_share *obj =[array_feeds objectAtIndex:commentCell.tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    [self presentViewController:comments_list_viewObj animated:YES completion:nil];
}

-(void)likesCellWillShowLikes:(STXLikesCell *)likesCell {
    NSInteger tag = (int)likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked = tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url {
    NSLog(@"URL");
    
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;//[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag {
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag {
    //pushPopFlg=1;
//    hash_tag_viewObj.str_title = hashtag;//[[self tagFromSender:sender ] substringFromIndex:1];
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention {
   // pushPopFlg=1;
    user_info_view.user_id=mention;//[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)likesCellDidSelectLiker:(NSString *)liker {
    NSLog(@"USERLIKESSSED");
    
    user_info_view.user_id=liker;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url {
    NSLog(@"URL:fdasf");
}

- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter {
  //  pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;//[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (IBAction)btnProfileClicked:(id)sender {
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld", (long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:posterLike.tag];
    which_image_liked = (int)posterLike.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];}

-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:YES];
    [self.lbl_name setText:@""];
    self.lbl_total_followers.text =@"0";
    self.lbl_total_followings.text = @"0";
    self.lbl_total_photos.text = @"0";
    self.lbl_total_categories.text = @"0";
    self.img_photo.image = [UIImage imageNamed:@"default_user_image"];
    
    [self.lbl_name_tbl setText:@""];
    self.lbl_total_followers_tbl.text =@"0";
    self.lbl_total_followings_tbl.text = @"0";
    self.lbl_total_photos_tbl.text = @"0";
    self.lbl_total_categories_tbl.text = @"0";
    self.img_photo_tbl.image = [UIImage imageNamed:@"default_user_image"];
    
    self.btn_follow.hidden =YES;
    self.btn_follow_tbl.hidden =YES;
    self.btn_follow_siv.hidden = YES;
}

#pragma mark - Social buttons
- (IBAction)socialFacebookClicked:(id)sender {
    NSString *url = self.profile_share_obj.facebook_url;
    [self openUrl:url];
}

- (IBAction)socialTwitterClicked:(id)sender {
    NSString *url = self.profile_share_obj.twitter_url;
    [self openUrl:url];
}

- (IBAction)socialPinterestClicked:(id)sender {
    NSString *url = self.profile_share_obj.pinterest_url;
    [self openUrl:url];
}

- (IBAction)socialGoogleClicked:(id)sender {
    NSString *url = self.profile_share_obj.google_url;
    [self openUrl:url];
}
- (IBAction)socialInstagramClicked:(id)sender {
    NSString *url = self.profile_share_obj.instagram_url;
    [self openUrl:url];
}
- (IBAction)socialYoutubeClicked:(id)sender {
    NSString *url = self.profile_share_obj.youtube_url;
    [self openUrl:url];
}

- (void)openUrl:(NSString *)urlString
{
    NSURL *webpageUrl;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        webpageUrl = [NSURL URLWithString:urlString];
    } else {
        webpageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
    KINWebBrowserViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
    //     [webBrowser setDelegate:self];
    webBrowser.showsURLInNavigationBar = YES;
    webBrowser.tintColor = [UIColor whiteColor];
    webBrowser.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"colarge_ipad"]];
    webBrowser.showsPageTitleInNavigationBar = NO;
    webBrowser.showsURLInNavigationBar = NO;
    [self presentViewController:webBrowserNavigationController animated:YES completion:nil];
    
    [webBrowser loadURL:webpageUrl];
}

#pragma mark - Photos count click

- (IBAction)btn_categories_count_click:(id)sender {
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    
    if ( isUserPrivate )
        return;
    
    self.category_list_view.user_id =self.profile_share_obj.user_id;
    self.category_list_view.editable = NO;
    self.category_list_view.nav_title = @"Categories";
    
    [self.navigationController pushViewController:self.category_list_view animated:YES];
}

-(IBAction)btn_photos_count_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    
    [self.collectionViewObj setContentOffset:CGPointMake(100, self.view_header.frame.size.height-68)];
    [self.tbl_news setContentOffset:CGPointMake(0, self.view_header.frame.size.height-68)];
}

#pragma mark - Followes count click
-(IBAction)btn_followers_count_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"Followers";
    
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

#pragma mark - Following count click
-(IBAction)btn_following_count_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    self.followers_list_viewObj.user_id =self.profile_share_obj.user_id;
    self.followers_list_viewObj.nav_title = @"Following";
    [self.navigationController pushViewController:self.followers_list_viewObj animated:YES];
}

#pragma mark - Edit your profile click
-(IBAction)btn_edit_your_profile_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;	
    self.edit_your_profile_viewObj.hidesBottomBarWhenPushed = YES;
    self.edit_your_profile_viewObj.is_reload_data=1;
    
    [self.navigationController pushViewController:self.edit_your_profile_viewObj animated:YES];
    
}

#pragma mark - Setting Button click
-(IBAction)btn_setting_click:(id)sender{
    [self.navigationController pushViewController:self.setting_profileViewObj animated:YES];
}

#pragma mark - Refresh
-(IBAction)btn_refresh_click:(id)sender{
    [self get_news_feed];
    [self block_user_list];
}

#pragma mark - Get user Profile Info
-(void)get_user_info {
    if (fastCategoryFollow != 1)
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"605" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(get_user_info_Responce:) name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-605" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-605" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login=%@&to_id=%@&flag=1",salt,sig,[self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],[self.user_id stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_profile.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"605":nil];
    
    fastCategoryFollow = 0;
}

- (void)get_user_info_Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"605" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-605" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    [self.lbl_user_not_found setHidden:NO];
    self.btn_chat1.hidden = YES;
    self.btn_chat2.hidden = YES;
    self.btn_chat3.hidden = YES;

    self.objUserName_lbl.text = @"";

    if([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        [self.lbl_user_not_found setHidden:YES];

        NSDictionary *dict = [result valueForKey:@"data"];
        self.profile_share_obj.user_id = [dict valueForKey:@"user_id"];
        
        self.btn_chat1.hidden = NO;
        self.btn_chat2.hidden = NO;
        self.btn_chat3.hidden = NO;
 
        self.profile_share_obj.username = [StaticClass urlDecode:[dict valueForKey:@"username"]];
        self.profile_share_obj.name = [StaticClass urlDecode:[dict valueForKey:@"name"]];
        
        self.profile_share_obj.email = [StaticClass urlDecode:[dict valueForKey:@"email"]];
        self.profile_share_obj.facebook_id = [dict valueForKey:@"facebook_id"];
        self.profile_share_obj.total_followers = [dict valueForKey:@"total_followers"];
        self.profile_share_obj.total_following = [dict valueForKey:@"total_following"];
        self.profile_share_obj.total_categories = [dict valueForKey:@"total_categories"];

        self.profile_share_obj.total_photos =[dict valueForKey:@"total_images"];
        self.profile_share_obj.image =[StaticClass urlDecode:[dict valueForKey:@"image"]];
        self.profile_share_obj.phone = [dict valueForKey:@"phone"];
        self.profile_share_obj.bio=[StaticClass urlDecode:[dict valueForKey:@"bio"]];
        self.profile_share_obj.web_url=[StaticClass urlDecode:[dict valueForKey:@"weburl"]];
        self.profile_share_obj.facebook_url =[StaticClass urlDecode:[dict valueForKey:@"facebook_url"]];
        self.profile_share_obj.twitter_url =[StaticClass urlDecode:[dict valueForKey:@"twitter_url"]];
        self.profile_share_obj.pinterest_url =[StaticClass urlDecode:[dict valueForKey:@"pinterest_url"]];
        self.profile_share_obj.google_url =[StaticClass urlDecoding:[dict valueForKey:@"google_url"]];
        self.profile_share_obj.instagram_url =[StaticClass urlDecode:[dict valueForKey:@"instagram_url"]];
        self.profile_share_obj.youtube_url =[StaticClass urlDecode:[dict valueForKey:@"youtube_url"]];
        self.profile_share_obj.latitude =[StaticClass urlDecode:[dict valueForKey:@"latitude"]];
        self.profile_share_obj.longitude =[StaticClass urlDecode:[dict valueForKey:@"longitude"]];
        
        self.profile_share_obj.is_user_frined = [StaticClass urlDecode:[dict valueForKey:@"user_friend"]];
        self.profile_share_obj.is_user_follow=[StaticClass urlDecode:[dict valueForKey:@"user_following"]];
        
        self.profile_share_obj.delivery_time = [StaticClass urlDecode:[dict valueForKey:@"delivery_time"]];
        
        self.profile_share_obj.working_hours = [StaticClass urlDecode:[dict valueForKey:@"working_hours"]];
        
        self.profile_share_obj.shipping_fee = [StaticClass urlDecode:[dict valueForKey:@"shipping_fee"]];

        self.profile_share_obj.is_private_flg=[dict valueForKey:@"is_private"];
        
        self.objUserName_lbl.text=profile_share_obj.name;
        [self.img_photo sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_tbl_bg sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl_bg_list sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
        [self.img_photo_siv sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        [self.img_photo_tbl_bg_siv sd_setImageWithURL:[NSURL URLWithString:self.profile_share_obj.image] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        
//        self.img_photo.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
//        self.img_photo_tbl.imageURL =[NSURL URLWithString:self.profile_share_obj.image];
        
        if (self.profile_share_obj.total_followers.length==0) {
            self.lbl_total_followers.text=@"0";
            self.lbl_total_followers_tbl.text=@"0";
            self.lbl_total_followers_siv.text = @"0";
        }
        else {
            self.lbl_total_followers.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_tbl.text=self.profile_share_obj.total_followers;
            self.lbl_total_followers_siv.text=self.profile_share_obj.total_followers;
        }
        
        if (self.profile_share_obj.total_following.length==0) {
            self.lbl_total_followings.text=@"0";
            self.lbl_total_followings_tbl.text=@"0";
            self.lbl_total_followings_siv.text = @"0";
        }
        else {
            self.lbl_total_followings.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_tbl.text=self.profile_share_obj.total_following;
            self.lbl_total_followings_siv.text=self.profile_share_obj.total_following;
        }
        
        if (self.profile_share_obj.total_photos.length==0) {
            self.lbl_total_photos.text=@"0";
            self.lbl_total_photos_tbl.text=@"0";
            self.lbl_total_photos_siv.text = @"0";
        } else {
            self.lbl_total_photos.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_tbl.text=self.profile_share_obj.total_photos;
            self.lbl_total_photos_siv.text = self.profile_share_obj.total_photos;
        }
        
        if (self.profile_share_obj.total_categories.length == 0) {
            self.lbl_total_categories.text = @"0";
            self.lbl_total_categories_tbl.text = @"0";
            self.lbl_total_categories_siv.text = @"0";
        } else {
            self.lbl_total_categories.text = self.profile_share_obj.total_categories;
            self.lbl_total_categories_tbl.text = self.profile_share_obj.total_categories;
            self.lbl_total_categories_siv.text = self.profile_share_obj.total_categories;
        }
    
        NSMutableString *str =[[NSMutableString alloc]init];
        
        if (self.profile_share_obj.name.length==0) {
            [self.lbl_name setText:@""];
            [self.lbl_name setText:@""];
        }
        else {
           // [str appendFormat:@"%@ \n\n",self.profile_share_obj.name];
            
            for (NSString *name in [self.profile_share_obj.name componentsSeparatedByString:@" "]) {
                if (name.length>0) {
                    [self.lbl_name addStyle:self.username_style forPrefix:name];
                    [self.lbl_name_tbl addStyle:self.username_style forPrefix:name];
                }
            }
        }

        NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
        NSString *uid2=[NSString stringWithFormat:@"%ld",(long)[self.profile_share_obj.user_id integerValue]];
        
        isOweNerorNot=NO;
        if (![uid1 isEqualToString:uid2]) {
            isOweNerorNot=NO;
            if ([self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
                
                [self.btn_follow setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_siv setImage:[UIImage imageNamed:@"FollowBtn.png"] forState:UIControlStateNormal];
            } else {
                [self.btn_follow setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_siv setImage:[UIImage imageNamed:@"FollowingBtn.png"] forState:UIControlStateNormal];
            }
            
            if ([self.profile_share_obj.is_private_flg isEqualToString:@"1"])
            {
                [self.btn_follow setImage:[UIImage imageNamed:@"RequestBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"RequestBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_siv setImage:[UIImage imageNamed:@"RequestBtn.png"] forState:UIControlStateNormal];
            }
            
            if ( [self.profile_share_obj.is_private_flg isEqualToString:@"1"] && [self.profile_share_obj.is_user_frined isEqualToString:@"request sent"] )
            {
                [self.btn_follow setImage:[UIImage imageNamed:@"RequestSentBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_tbl setImage:[UIImage imageNamed:@"RequestSentBtn.png"] forState:UIControlStateNormal];
                [self.btn_follow_siv setImage:[UIImage imageNamed:@"RequestSentBtn.png"] forState:UIControlStateNormal];
            }

            if ([self.profile_share_obj.is_user_frined isEqualToString:@"no"]||[self.profile_share_obj.is_user_frined isEqualToString:@"request not sent"] ) {
                
                [self.btn_friend_tbl setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];

                [self.btn_friend setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
                [self.btn_friend_siv setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
            } else {
                [self.btn_friend_tbl setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];

                [self.btn_friend setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
                [self.btn_friend_siv setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
            }
            
            
            if([profile_share_obj.is_user_frined isEqualToString:@"request not sent"]) {
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=NO;
                btn_friend_tbl.hidden=NO;
                btn_friend_siv.hidden=NO;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                btnNotNowSiv.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                if ([self.profile_share_obj.is_private_flg isEqualToString:@"1"])
                {
                    btn_follow_tbl.hidden = NO;
                    btn_follow.hidden = NO;
                    btn_follow_siv.hidden = NO;
                }
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
                
                
            }
            else if([profile_share_obj.is_user_frined isEqualToString:@"request sent"]) {
                imgRequestSent.hidden = NO;
                imgRequestSentTbl.hidden =NO;
                
                btn_friend.hidden=YES;
                btn_friend_tbl.hidden=YES;
                btn_friend_siv.hidden=YES;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                btnNotNowSiv.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                if ([self.profile_share_obj.is_private_flg isEqualToString:@"1"])
                {
                    btn_follow_tbl.hidden = NO;
                    btn_follow.hidden = NO;
                    btn_follow_siv.hidden = NO;
                }
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
            }
            else if([profile_share_obj.is_user_frined isEqualToString:@"request pending"]) {
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=YES;
                btn_friend_siv.hidden=YES;
                btn_friend_tbl.hidden=YES;
                
                btnNotNow.hidden=NO;
                btnNotNowTbl.hidden=NO;
                btnNotNowSiv.hidden=NO;
                
                btnAccept.hidden=NO;
                btnAcceptTbl.hidden=NO;
                
                btn_follow_tbl.hidden = YES;
                btn_follow.hidden = YES;
                btn_follow_siv.hidden = YES;
                
                lblFriendRequest.hidden=NO;
                lblFriendRequestTbl.hidden=NO;
            }
            else {
                
                imgRequestSent.hidden = YES;
                imgRequestSentTbl.hidden =YES;
                
                btn_friend.hidden=NO;
                btn_friend_siv.hidden=NO;
                btn_friend_tbl.hidden=NO;
                
                btnNotNow.hidden=YES;
                btnNotNowTbl.hidden=YES;
                btnNotNowSiv.hidden=YES;
                
                btnAccept.hidden=YES;
                btnAcceptTbl.hidden=YES;
                
                if ([self.profile_share_obj.is_private_flg isEqualToString:@"1"])
                {
                    btn_follow_tbl.hidden = NO;
                    btn_follow.hidden = NO;
                    btn_follow_siv.hidden = NO;
                }
                
                lblFriendRequest.hidden=YES;
                lblFriendRequestTbl.hidden=YES;
            }
            
            self.btn_edit_tbl.hidden = YES;
            self.btn_edit.hidden = YES;
            self.btn_edit_siv.hidden = YES;
            self.btn_write_review_riv.hidden = NO;
            
            [lbl_name setWidth:screen_width -20];
            [lbl_name_tbl setWidth:screen_width -20];
        }
        else {
            isOweNerorNot=YES;
            self.btn_follow_tbl.hidden = YES;
            self.btn_follow.hidden = YES;
            self.btn_follow_siv.hidden = YES;
            
            self.btn_friend_tbl.hidden = YES;
            self.btn_friend.hidden = YES;
            self.btn_friend_siv.hidden = YES;
            
            self.imgRequestSentTbl.hidden=YES;
            self.imgRequestSent.hidden=YES;
            
            self.btnAcceptTbl.hidden=YES;
            self.btnAccept.hidden=YES;
            
            self.btnNotNowTbl.hidden=YES;
            self.btnNotNow.hidden=YES;
            self.btnNotNowSiv.hidden=YES;
            
            self.btn_edit_tbl.hidden = NO;
            self.btn_edit.hidden = NO;
            self.btn_edit_siv.hidden = NO;
            self.btn_write_review_riv.hidden = YES;
            
            lblFriendRequest.hidden=YES;
            lblFriendRequestTbl.hidden=YES;
            
            [lbl_name setWidth:screen_width-20];
            [lbl_name_tbl setWidth:screen_width-20];
        }
        
//        if (!self.profile_share_obj.web_url.length==0) {
//            [str appendFormat:@"%@",self.profile_share_obj.web_url];
//        }
        
        int fl=0;
        if (!(self.profile_share_obj.bio.length == 0)) {
            [str appendFormat:@"%@",self.profile_share_obj.bio];
        }
        else {
            fl=1;
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
        }
        self.lbl_bio_siv.text = str;
        
        [self.lbl_bio_siv adjustHeight];
        
        if (fl==1) {
            [str appendFormat:@"\n"];
        }
        
        if (!(self.profile_share_obj.email.length == 0))
        {
            self.lbl_email_siv.text = self.profile_share_obj.email;
        }
        if (!(self.profile_share_obj.phone.length == 0))
        {
            self.lbl_mobile_siv.text = self.profile_share_obj.phone;
        }
        
        if (!(self.profile_share_obj.web_url.length == 0)) {
            self.lbl_website_siv.text = self.profile_share_obj.web_url;
            [str appendFormat:@"%@",self.profile_share_obj.web_url];
        }
        else {
            str=(NSMutableString *)[str stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        }
        
        if (!(self.profile_share_obj.delivery_time.length == 0))
        {
            self.lbl_delivery_siv.text = self.profile_share_obj.delivery_time;
        }
        else
            self.lbl_delivery_siv.text = @"";
        
        if (!(self.profile_share_obj.working_hours.length == 0))
        {
            self.lbl_hours_siv.text = self.profile_share_obj.working_hours;
        }
        else
            self.lbl_hours_siv.text = @"";
        
        if (!(self.profile_share_obj.shipping_fee.length == 0))
        {
            self.lbl_fee_siv.text = self.profile_share_obj.shipping_fee;
        }
        else
            self.lbl_fee_siv.text = @"";

        [self.lbl_delivery_siv adjustHeight];
        [self.lbl_hours_siv adjustHeight];
        [self.lbl_fee_siv adjustHeight];
        
        self.lbl_hours_title_siv.frame = CGRectMake(self.lbl_hours_title_siv.frame.origin.x, self.lbl_delivery_siv.origin.y+self.lbl_delivery_siv.size.height+11, self.lbl_hours_title_siv.size.width, self.lbl_hours_title_siv.size.height);
        
        self.lbl_hours_siv.frame = CGRectMake(self.lbl_hours_siv.frame.origin.x, self.lbl_hours_title_siv.origin.y+self.lbl_hours_title_siv.size.height+3, self.lbl_hours_siv.size.width, self.lbl_hours_siv.size.height);
        
        self.lbl_fee_title_siv.frame = CGRectMake(self.lbl_fee_title_siv.frame.origin.x, self.lbl_hours_siv.origin.y+self.lbl_hours_siv.size.height+11, self.lbl_fee_title_siv.size.width, self.lbl_fee_title_siv.size.height);
        
        self.lbl_fee_siv.frame = CGRectMake(self.lbl_fee_siv.frame.origin.x, self.lbl_fee_title_siv.origin.y+self.lbl_fee_title_siv.size.height+3, self.lbl_fee_siv.size.width, self.lbl_fee_siv.size.height);
        
        self.img_delivery_siv.frame = CGRectMake(self.img_delivery_siv.frame.origin.x, self.lbl_delivery_title_siv.frame.origin.y+1, self.img_delivery_siv.frame.size.width, self.img_delivery_siv.frame.size.height);
        
        self.img_hours_siv.frame = CGRectMake(self.img_hours_siv.frame.origin.x, self.lbl_hours_title_siv.frame.origin.y-2, self.img_hours_siv.frame.size.width, self.img_hours_siv.frame.size.height);
        
        self.img_fee_siv.frame = CGRectMake(self.img_fee_siv.frame.origin.x, self.lbl_fee_title_siv.frame.origin.y-2, self.img_fee_siv.frame.size.width, self.img_fee_siv.frame.size.height);
        
        self.view_content.frame = CGRectMake(self.view_content.frame.origin.x, self.lbl_bio_siv.frame.origin.y+_lbl_bio_siv.frame.size.height + 10, self.view_content.frame.size.width, self.lbl_fee_siv.origin.y+self.lbl_fee_siv.size.height + 50);
        
        [self autoChangeScrollViewContentSize];
        
        NSString *tem=[NSString stringWithFormat:@"%@",str];
        [self.lbl_name setText:tem];
        [self.lbl_name_tbl setText:tem];
        
        if (self.lbl_name.height>22) {
            //  CGSize size = [[NSString stringWithFormat:@"%@",tem]  sizeWithFont:[UIFont boldSystemFontOfSize:17.0f]];
            
//            CGRect frame_header =  self.view_header.frame;
//            frame_header.size.height = 160.0f + self.lbl_name.height -22;
//            CGRect frame_header_tbl =  self.view_header_tbl.frame;
//            frame_header_tbl.size.height = 160.0f + self.lbl_name_tbl.height -22;
//            [self.tbl_news setTableHeaderView:self.view_header_tbl];
        }
       /* else {
            self.img_bg_firstcell.frame = CGRectMake(5,10,310, 135);
            self.view_secondcell.frame = CGRectMake(0,160,320,60);
            self.view_header.frame = CGRectMake(0,0,320,225);
            self.img_bg_firstcell_tbl.frame = CGRectMake(5,10,310, 135);
            self.view_secondcell_tbl.frame = CGRectMake(0,160,320,60);
            self.view_header_tbl.frame = CGRectMake(0,0,320,225);
            [self.tbl_news setTableHeaderView:self.view_header_tbl];
        }
        */
        
        [self.array_feeds removeAllObjects];
        [self.collectionViewObj reloadData];
        
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        
        [self.tbl_news reloadData];

        if (isOweNerorNot) {
            [self resetAllViews];
            [self get_categories_list];
            lblPhotoPrivate.hidden=YES;
            isUserPrivate = NO;
        }
        else {
            if ([[dict valueForKey:@"is_private"] isEqualToString:@"1"] && [[dict valueForKey:@"user_following"] isEqualToString:@"no"]) {
//                lblPhotoPrivate.frame=CGRectMake(80,tbl_news.tableHeaderView.frame.origin.y+tbl_news.tableHeaderView.frame.size.height+15 ,160 ,42 );
                [self resetAllViews];
                lblPhotoPrivate.hidden=NO;
                isUserPrivate = YES;
                
                btn_follow_tbl.hidden = NO;
                btn_follow.hidden = NO;
                btn_follow_siv.hidden = NO;

//                lblPhotoPrivate.frame=CGRectMake(80,_headerView1.frame.size.height + tbl_news.frame.origin.y+self.view_header_tbl.frame.size.height+15 ,160 ,42 );
                [self btn_collectionview_click:nil];
                [SVProgressHUD dismiss];
            }
            else {
                [self resetAllViews];
                
                [self get_categories_list];
                lblPhotoPrivate.hidden=YES;
                isUserPrivate = NO;
                
                btn_follow_tbl.hidden = YES;
                btn_follow.hidden = YES;
                btn_follow_siv.hidden = YES;
            }
        }
        
        [self setPlaceMark:self.profile_share_obj.latitude loStr:self.profile_share_obj.longitude];
        
        if ( self.lbl_bio_siv.hidden == NO )
            [self showsSellerInfoSubViews];
    }
    else {
        [self resetAllViews];
        [SVProgressHUD dismiss];
    }
}

- (void)resetAllViews
{
    lblPhotoPrivate.hidden=YES;
    isUserPrivate = NO;
    [self.array_feeds removeAllObjects];
    [self.collectionViewObj reloadData];
    
    self.tableViewDataSource.posts =self.array_feeds;// copy];
    self.tableViewDelegate.posts =self.array_feeds;// copy];
    [self.composed_feeds removeAllObjects];
    [self.tbl_news reloadData];
    
    self.array_promotions = [[NSMutableArray alloc] init];
    [self.collectionViewObj reloadData];
    self.array_reviews = [[NSMutableArray alloc] init];
    [self.tbl_review_riv setTblDataSource:self.array_reviews];
    [self.tbl_review_riv reloadData];
    
    starRatingView.value = 0.0;
    self.lbl_total_reviews_riv.text = @"0 reviews";
    self.lbl_total_reviews_value_riv.text = @"(0)";
}

- (void)setPlaceMark:(NSString *)laStr loStr:(NSString *)loStr
{
    CGFloat latitude = [laStr floatValue];
    CGFloat longitude = [loStr floatValue];
    
//    self.mapView.mapType = MKMapTypeStandard;
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latitude, longitude);
//    MKCoordinateSpan span = MKCoordinateSpanMake(0.2, 0.2);
//    MKCoordinateRegion regionToDisplay = MKCoordinateRegionMake(center, span);
//    [self.mapView setRegion:regionToDisplay];
//    
//    MyAnnotation *ann = [[MyAnnotation alloc] init];
//    ann.title = self.profile_share_obj.username;
//    ann.coordinate = center;
//    [self.mapView addAnnotation:ann];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:10];
    GMSMapView *view = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.mapView.frame.size.width, self.mapView.frame.size.height) camera:camera];
    
    view.settings.scrollGestures = NO;
    view.settings.zoomGestures = NO;
    
//    view.myLocationEnabled = YES;
    view.delegate = self;
    [self.mapView removeSubviews];
    [self.mapView addSubview:view];
    CLLocationCoordinate2D  position = CLLocationCoordinate2DMake(latitude, longitude);
    destinationlocation = position;
    GMSMarker   *marker = [GMSMarker markerWithPosition:position];
    marker.title = [NSString stringWithFormat:@"%@ >", profile_share_obj.name];
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.map = view;
    googlemapview = view;
    
    // Watch for pan
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget: self action:@selector(didPan:)];
    view.gestureRecognizers = @[panRecognizer];
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    self.map_viewObj.str_latitude=self.profile_share_obj.latitude;
    self.map_viewObj.str_longitude=self.profile_share_obj.longitude;
    self.map_viewObj.str_name = self.profile_share_obj.name;

    [self.navigationController pushViewController:self.map_viewObj animated:YES];
    
}

- (void) didPan:(UIPanGestureRecognizer*) gestureRecognizer
{
    NSLog(@"DID PAN");
    // React here to user's pan
}

- (IBAction)btn_write_review_riv_Clicked:(id)sender
{
    WriteReviewViewController *wrvc =[[WriteReviewViewController alloc]initWithNibName:@"WriteReviewViewController" bundle:nil];
    wrvc.profile_share_obj = self.profile_share_obj;
    [self.navigationController pushViewController:wrvc animated:YES];
}

- (IBAction)lbl_email_sivTapped:(id)sender {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    [picker setToRecipients:[NSArray arrayWithObject:self.lbl_email_siv.text]];
    [picker setSubject:@"Hello"];
    [self presentViewController:picker animated:NO completion:nil];
}

- (IBAction)lbl_website_sivTapped:(id)sender {
    NSString *urlString = self.lbl_website_siv.text;
    NSURL *webpageUrl;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        webpageUrl = [NSURL URLWithString:urlString];
    } else {
        webpageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    if ([[UIApplication sharedApplication] canOpenURL:webpageUrl]) {
        [[UIApplication sharedApplication] openURL:webpageUrl];
    }
    else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invaild Url!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)lbl_mobile_sivTapped:(id)sender {
    NSString *phNo = self.lbl_mobile_siv.text;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}


-(IBAction)btnAcceptClick :(id)sender {
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIResponceFlag:) name:@"14245" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIResponceFlag:) name:@"-14245" object:nil];

    NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            @"1",@"flag",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"to_id",
                            self.profile_share_obj.user_id,@"from_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14245" :params];
}

-(void)postAcceptRequestAPIResponceFlag:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    NSLog(@"result:%@",result);
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    btnNotNowSiv.hidden = YES;
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;
    [self get_user_info];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Accept Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)FailpostAcceptRequestAPIResponceFlag:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14245" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14245" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(IBAction)btnNotNowClick :(id)sender {
    UIButton *temp = (UIButton *)sender;
    
    denayAlertViewObj = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"are you sure !! you want to reject this request ??" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok",@"cancel", nil];
    
    denayAlertViewObj.tag = temp.tag;
    [denayAlertViewObj show];
}

- (void)get_categories_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&login_id=%@", salt, sig, self.profile_share_obj.user_id, [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_category_followers.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"])
        {
            self.array_categories = [[NSMutableArray alloc] init];
            
            NSArray *dataArray = responseObject[@"data"];
            for(NSDictionary *data in dataArray)
            {
                CategoryObject *object = [[CategoryObject alloc] init];
                object.categoryId = data[@"id"];
                object.categoryName = data[@"name"];
                object.categoryDescs = data[@"description"];
                object.categoryFollowers = data[@"followers"];
                object.categoryUserFollower = data[@"user_follower"];
                [self.array_categories addObject:object];
            }
            [self get_news_feed];
        }
        else {
            [self.array_categories removeAllObjects];
            [self get_news_feed];
        }
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get categories" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark - Get News Feed
- (void)get_news_feed
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&profile_handle=%@&login_id=%@",salt,sig,self.profile_share_obj.user_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_user_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        [self.array_feeds removeAllObjects];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            self.feeds_temp_dic = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.price = dict[@"price"];
                shareObj.currency = dict[@"currency"];
                shareObj.sold = [dict[@"sold"] integerValue];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                }
                else{
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        if (![shareObj.username isEqualToString:obj.username] || ![shareObj.description isEqualToString:obj.comment_desc])
                            [shareObj.array_comments addObject:obj];

                        [shareObj.array_comments addObject:obj];
                    }
                }
                else
                {
                    
                }
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (shareObj.image_path.length !=0) {
                    [self.array_feeds addObject:shareObj];
                }
                
                NSMutableArray *tempArray = [self.feeds_temp_dic[shareObj.category_id] mutableCopy];
                if (!tempArray)
                    tempArray = [[NSMutableArray alloc] init];
                [tempArray addObject:shareObj];
                self.feeds_temp_dic[shareObj.category_id] = [tempArray copy];
            }
        }
        
        [self composeCategoryPosts];
        [self get_promotion_list];
    } failure:^(NSString *errorString) {
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        [SVProgressHUD dismiss];
    }];
}

- (void)composeCategoryPosts
{
//    self.tbl_news.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SoldImage.png"]];
//    self.tbl_news.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SoldImage.png"]];
    self.composed_feeds = [[NSMutableArray alloc] initWithCapacity:self.array_categories.count];
    for (NSInteger index = 0; index < self.array_categories.count; index ++)
    {
        CategoryObject *obj = self.array_categories[index];
        NSMutableArray *tempArray = [self.feeds_temp_dic[obj.categoryId] mutableCopy];
        if (!tempArray)
            tempArray = [[NSMutableArray alloc] init];
        self.composed_feeds[index] = [tempArray copy];
    }
    
    [self.tbl_news reloadData];
}

- (void)get_promotion_list
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&flag=1&off=0&login_id=%@", salt, sig, self.profile_share_obj.user_id, LOGINEDUSERID];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_promotion_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@", requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        [self.array_promotions removeAllObjects];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"]) {
            
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO promotion images found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            
            NSArray *array =[responseObject valueForKey:@"data"];
            
            self.feeds_temp_dic = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *dict in array) {
                
                Home_tableview_data_share *shareObj =[[Home_tableview_data_share alloc]init];
                shareObj.datecreated=[StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                
                shareObj.description=[StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id =[dict valueForKey:@"id"];
                shareObj.image_owner=[dict valueForKey:@"image_owner"];
                shareObj.image_path=[StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat =[dict valueForKey:@"lat"];
                shareObj.lng=[dict valueForKey:@"lng"];
                shareObj.likes=[dict valueForKey:@"likes"];
                shareObj.location=[StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid=[dict valueForKey:@"uid"];
                shareObj.username=[StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image=[StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                
                shareObj.liked=[dict valueForKey:@"user_liked"];
                shareObj.comment_count =[dict valueForKey:@"total_comment"];
                
                shareObj.array_liked_by=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                    }
                }
                else{
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]]) {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray) {
                        Comment_share *obj =[[Comment_share alloc]init];
                        
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[tempdict valueForKey:@"username"];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        if ([shareObj.username isEqualToString:obj.username] && [shareObj.description isEqualToString:obj.comment_desc])
                            continue;
                        
                        [shareObj.array_comments addObject:obj];
                    }
                }
                else
                {
                    
                }
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (shareObj.image_path.length !=0) {
                    [self.array_promotions addObject:shareObj];
                }
            }
        }
        [self get_reviews];
        
        [self.collectionViewObj reloadData];
        
        if (selectedItem == 1)
        {
        if ([self.array_promotions count] > 0)
           [self.lbl_no_promotions setHidden:YES];
         else
           [self.lbl_no_promotions setHidden:NO];
        }
        else
        {
           [self.lbl_no_promotions setHidden:YES];
        }
        
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get promotions" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (void)get_reviews
{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@", salt, sig, self.profile_share_obj.user_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_reviews.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@", requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"-2"])
        {
//            [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"NO reviews found" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
        }
        
        self.array_reviews = [[NSMutableArray alloc] init];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            starRatingView.value = [responseObject[@"total_rating"] floatValue];
            self.lbl_total_reviews_riv.text = [NSString stringWithFormat:@"%@ reviews", responseObject[@"total_amount"]];
            self.lbl_total_reviews_value_riv.text = [NSString stringWithFormat:@"(%.1f)", [responseObject[@"total_rating"] floatValue]];
            
            for (NSDictionary *dic in (NSArray *)responseObject[@"data"])
            {
                RatingObject *object = [[RatingObject alloc] init];
                object.user_bio = [StaticClass urlDecode:dic[@"bio"]];
                object.review_datecreated = [StaticClass urlDecode:dic[@"datecreated"]];
                object.review_descs = [StaticClass urlDecode:dic[@"descs"]];
                object.review_id = dic[@"review_id"];
                object.user_id = dic[@"id"];
                object.user_image = [StaticClass urlDecode:dic[@"image"]];
                object.user_name = dic[@"name"];
                object.user_username = dic[@"username"];
                object.review_rating = dic[@"rating"];
                [self.array_reviews addObject:object];
            }
            
        }
        else
        {
            starRatingView.value = 0.0;
            self.lbl_total_reviews_riv.text = @"0 reviews";
            self.lbl_total_reviews_value_riv.text = @"(0)";
        }
        
        if ([self.lbl_total_reviews_riv.text isEqualToString:@"0 reviews"]) {
            [self.lbl_no_reviews_riv setHidden:NO];
            
//            [self.lbl_total_reviews_riv setHidden:YES];
//            [self.lbl_total_reviews_value_riv setHidden:YES];
//            [self.totalReview_riv setHidden:YES];
//            [self.reviewInfoView setHidden:YES];
        }
        else
        {
            [self.lbl_no_reviews_riv setHidden:YES];
            
//            [self.lbl_total_reviews_riv setHidden:NO];
//            [self.lbl_total_reviews_value_riv setHidden:NO];
//            [self.totalReview_riv setHidden:NO];
//            [self.reviewInfoView setHidden:NO];
        }

        
        [self.tbl_review_riv setTblDataSource:self.array_reviews];
        [self.tbl_review_riv reloadData];
        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
//        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Can not get reviews" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark - Collectionview & Tableview  choose for show
-(IBAction)btn_collectionview_click:(id)sender{
    [self.collectionViewObj setContentOffset:self.tbl_news.contentOffset];
    self.tbl_news.hidden = NO;
    self.lbl_no_promotions.hidden = YES;
    self.collectionViewObj.hidden = YES;
//    [self.collectionViewObj addSubview:lblPhotoPrivate];
    infoScrollView.hidden = YES;
    selectedItem = 0;
}

-(IBAction)btn_tableview_click:(id)sender{
    if ( isUserPrivate || [self.objUserName_lbl.text isEqualToString:@""])
        return;
    [self.tbl_news setContentOffset:self.collectionViewObj.contentOffset];
    self.tbl_news.hidden = YES;
    
    if ([self.array_promotions count] > 0)
       [self.lbl_no_promotions setHidden:YES];
    else
       [self.lbl_no_promotions setHidden:NO];

    self.collectionViewObj.hidden = NO;
//    [self.tbl_news addSubview:lblPhotoPrivate];
    infoScrollView.hidden = YES;
    
    selectedItem = 1;
}

- (IBAction)btn_sellerinfo_click:(id)sender
{
    if ( isUserPrivate || [self.objUserName_lbl.text isEqualToString:@""])
        return;
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden = YES;
    self.selectionTapImageView_siv.image = [UIImage imageNamed:@"Seller Info View Selected.png"];
    [self showsSellerInfoSubViews];
    infoScrollView.hidden = NO;
    self.sellerInfoView.hidden = NO;
    self.reviewInfoView.hidden = YES;
    [self autoChangeScrollViewContentSize];
    
    selectedItem = 3;
}

- (IBAction)btn_reviewinfo_click:(id)sender
{
    if ( isUserPrivate ||[self.objUserName_lbl.text isEqualToString:@""] )
        return;
    //selectionTapImageView_siv
    self.tbl_news.hidden = YES;
    self.collectionViewObj.hidden = YES;
    self.selectionTapImageView_siv.image = [UIImage imageNamed:@"Review Selected.png"];
    [self hiddenSellerInfoSubViews];
    infoScrollView.hidden = NO;
    self.sellerInfoView.hidden = NO;
    self.reviewInfoView.hidden = NO;
    [self autoChangeScrollViewContentSize];
    
    selectedItem = 2;
}

#pragma mark - Review Table View Delegate
- (void)reviewTableViewContentSizeChanged:(ReviewTableView *)tableView contentSize:(CGSize)contentSize
{
    reviewTableViewContentSize = contentSize;
    [self autoChangeScrollViewContentSize];
}

- (void)autoChangeScrollViewContentSize
{
    if (self.reviewInfoView.hidden)
    {
        infoScrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height +self.lbl_fee_siv.frame.size.height +self.lbl_hours_siv.frame.size.height +self.lbl_delivery_siv.frame.size.height +self.lbl_bio_siv.frame.size.height);
    }
    else
    {
        CGFloat newContentHeight = self.reviewInfoView.frame.origin.y + self.tbl_review_riv.frame.origin.y + self.tbl_review_riv.frame.size.height * 1.5f;
        infoScrollView.contentSize = CGSizeMake(self.view.frame.size.width, newContentHeight > self.view.frame.size.height * 1.12f ? newContentHeight : self.view.frame.size.height * 1.12f);
    }
    
    CGRect frame = self.reviewInfoView.frame;
    frame.size.height = infoScrollView.contentSize.height - 230.0;
    self.reviewInfoView.frame = frame;
    
    frame = self.sellerInfoView.frame;
    frame.size.height = infoScrollView.contentSize.height;
    self.sellerInfoView.frame = frame;
}

- (void)hiddenSellerInfoSubViews
{
    self.lbl_bio_siv.hidden = YES;
    self.lbl_mobile_siv.hidden = YES;
    self.lbl_email_siv.hidden = YES;
    self.lbl_website_siv.hidden = YES;
    self.lbl_mobile_title_siv.hidden = YES;
    self.lbl_email_title_siv.hidden = YES;
    self.lbl_website_title_siv.hidden = YES;
    self.img_phone_siv.hidden = YES;
    self.img_mail_siv.hidden = YES;
    self.img_web_siv.hidden = YES;
    self.btn_phone_siv.hidden = YES;
    self.btn_mail_siv.hidden = YES;
    self.btn_web_siv.hidden = YES;
    self.about_bg_siv.hidden=YES;
    self.contact_bg_siv.hidden = YES;
    self.store_bg_siv.hidden = YES;
    self.img_delivery_siv.hidden=YES;
    self.img_hours_siv.hidden=YES;
    self.img_fee_siv.hidden=YES;
    self.lbl_delivery_title_siv.hidden=YES;
    self.lbl_hours_title_siv.hidden=YES;
    self.lbl_fee_title_siv.hidden=YES;
    self.lbl_delivery_siv.hidden=YES;
    self.lbl_hours_siv.hidden=YES;
    self.lbl_fee_siv.hidden=YES;

    self.socialFacebookButton.hidden = YES;
    self.socialGoogleButton.hidden = YES;
    self.socialPinterestButton.hidden = YES;
    self.socialTwitterButton.hidden = YES;
    self.socialInstagramButton.hidden = YES;
    self.socialYoutubeButton.hidden = YES;
    self.mapView.hidden = YES;
}

- (void)showsSellerInfoSubViews
{
    self.lbl_bio_siv.hidden = NO;
//    self.lbl_mobile_siv.hidden = self.profile_share_obj.phone.length==0;
//    self.lbl_email_siv.hidden = self.profile_share_obj.email.length==0;
//    self.lbl_website_siv.hidden = self.profile_share_obj.web_url.length==0;
/*    self.lbl_mobile_title_siv.hidden = self.profile_share_obj.phone.length==0;
    self.lbl_email_title_siv.hidden = self.profile_share_obj.email.length==0;
    self.lbl_website_title_siv.hidden = self.profile_share_obj.web_url.length==0;
    self.img_phone_siv.hidden = self.profile_share_obj.phone.length==0;
    self.img_mail_siv.hidden = self.profile_share_obj.email.length==0;
    self.img_web_siv.hidden = self.profile_share_obj.web_url.length==0;
    self.btn_phone_siv.hidden = self.profile_share_obj.phone.length==0;
    self.btn_mail_siv.hidden = self.profile_share_obj.email.length==0;
    self.btn_web_siv.hidden = self.profile_share_obj.web_url.length==0;
    
    self.img_delivery_siv.hidden=self.profile_share_obj.delivery_time.length==0;
    self.img_hours_siv.hidden=self.profile_share_obj.working_hours.length==0;
    self.img_fee_siv.hidden=self.profile_share_obj.shipping_fee.length==0;
    self.lbl_delivery_title_siv.hidden=self.profile_share_obj.delivery_time.length==0;
    self.lbl_hours_title_siv.hidden=self.profile_share_obj.working_hours.length==0;
    self.lbl_fee_title_siv.hidden=self.profile_share_obj.shipping_fee.length==0;
    self.lbl_delivery_siv.hidden=self.profile_share_obj.delivery_time.length==0;
    self.lbl_hours_siv.hidden=self.profile_share_obj.working_hours.length==0;
    self.lbl_fee_siv.hidden=self.profile_share_obj.shipping_fee.length==0;
*/
    self.lbl_mobile_title_siv.hidden = NO;
    self.lbl_email_title_siv.hidden = NO;
    self.lbl_website_title_siv.hidden = NO;
    self.img_phone_siv.hidden = NO;
    self.img_mail_siv.hidden = NO;
    self.img_web_siv.hidden = NO;
    self.btn_phone_siv.hidden = NO;
    self.btn_mail_siv.hidden = NO;
    self.btn_web_siv.hidden = NO;
    self.about_bg_siv.hidden=NO;
    self.contact_bg_siv.hidden = NO;
    self.store_bg_siv.hidden = NO;
    self.img_delivery_siv.hidden=NO;
    self.img_hours_siv.hidden=NO;
    self.img_fee_siv.hidden=NO;
    self.lbl_delivery_title_siv.hidden=NO;
    self.lbl_hours_title_siv.hidden=NO;
    self.lbl_fee_title_siv.hidden=NO;
    self.lbl_delivery_siv.hidden=NO;
    self.lbl_hours_siv.hidden=NO;
    self.lbl_fee_siv.hidden=NO;

    self.lbl_mobile_title_siv.enabled = self.profile_share_obj.phone.length!=0;
    self.lbl_email_title_siv.enabled = self.profile_share_obj.email.length!=0;
    self.lbl_website_title_siv.enabled = self.profile_share_obj.web_url.length!=0;
    self.btn_phone_siv.enabled = self.profile_share_obj.phone.length!=0;
    self.btn_mail_siv.enabled = self.profile_share_obj.email.length!=0;
    self.btn_web_siv.enabled = self.profile_share_obj.web_url.length!=0;
    
    self.lbl_delivery_title_siv.enabled=self.profile_share_obj.delivery_time.length!=0;
    self.lbl_hours_title_siv.enabled=self.profile_share_obj.working_hours.length!=0;
    self.lbl_fee_title_siv.enabled=self.profile_share_obj.shipping_fee.length!=0;
    self.lbl_delivery_siv.enabled=self.profile_share_obj.delivery_time.length!=0;
    self.lbl_hours_siv.enabled=self.profile_share_obj.working_hours.length!=0;
    self.lbl_fee_siv.enabled=self.profile_share_obj.shipping_fee.length!=0;


/*
    self.socialFacebookButton.hidden = self.profile_share_obj.facebook_url.length==0;
    self.socialGoogleButton.hidden = self.profile_share_obj.google_url.length==0;
    self.socialPinterestButton.hidden = self.profile_share_obj.pinterest_url.length==0;
    self.socialTwitterButton.hidden = self.profile_share_obj.twitter_url.length==0;
    self.socialInstagramButton.hidden = self.profile_share_obj.instagram_url.length==0;
    self.socialYoutubeButton.hidden = self.profile_share_obj.youtube_url.length==0;
  */
    
    self.socialFacebookButton.hidden = NO;
    self.socialGoogleButton.hidden = NO;
    self.socialPinterestButton.hidden = NO;
    self.socialTwitterButton.hidden = NO;
    self.socialInstagramButton.hidden = NO;
    self.socialYoutubeButton.hidden = NO;

    self.socialFacebookButton.enabled = self.profile_share_obj.facebook_url.length!=0;
    self.socialGoogleButton.enabled = self.profile_share_obj.google_url.length!=0;
    self.socialPinterestButton.enabled = self.profile_share_obj.pinterest_url.length!=0;
    self.socialTwitterButton.enabled = self.profile_share_obj.twitter_url.length!=0;
    self.socialInstagramButton.enabled = self.profile_share_obj.instagram_url.length!=0;
    self.socialYoutubeButton.enabled = self.profile_share_obj.youtube_url.length!=0;
    self.mapView.hidden = NO;
}

#pragma mark - UICollectionView
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ( collectionView == collectionViewObj )
    {
        UICollectionReusableView *reusableview = nil;
        
        if (kind == UICollectionElementKindSectionHeader) {
            
            image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
            headerView.frame =self.view_header.frame;
            [headerView addSubview:self.view_header];
            
            UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionViewObj.collectionViewLayout;
            
            collectionViewLayout.headerReferenceSize = CGSizeMake(-10, self.view_header.frame.size.height );
            
            reusableview = headerView;
            NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
            return reusableview;
        }
        if (kind == UICollectionElementKindSectionFooter) {
           
            image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
            footerview.frame =self.view_footer.frame;
            [footerview addSubview:self.view_footer];
            reusableview = footerview;
            
            return reusableview;
        }
        
        return reusableview;
    }

    
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        NSLog(@"33333333333333333");
        
        image_collection_header *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"image_collection_header" forIndexPath:indexPath];
        headerView.frame =self.view_header_tbl.frame;
        [headerView addSubview:self.view_header_tbl];
        
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
        
        collectionViewLayout.headerReferenceSize = CGSizeMake(-10, self.view_header_tbl.frame.size.height );
        
        reusableview = headerView;
        NSLog(@"headerframe %@",NSStringFromCGRect(headerView.frame));
        return reusableview;
    }
    if (kind == UICollectionElementKindSectionFooter) {
        
        image_collection_footer *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"image_collection_footer" forIndexPath:indexPath];
        footerview.frame =self.view_footer_tbl.frame;
        [footerview addSubview:self.view_footer_tbl];
        reusableview = footerview;
        
        return reusableview;
    }
    
    return reusableview;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    if ( view == collectionViewObj )
        return self.array_promotions.count;
    
    return self.composed_feeds.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( collectionView == collectionViewObj )
        return CGSizeMake((collectionView.frame.size.width-5)/4, (collectionView.frame.size.width-5)/4);
    
    return CGSizeMake(collectionView.frame.size.width, 150);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if ( cv == collectionViewObj )
    {
        static NSString *CellIdentifier = @"image_collection_cell";
        image_collection_cell *cell = (image_collection_cell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.img_photo.layer.borderWidth =0.0f;
        cell.img_photo.layer.borderColor =[UIColor colorWithRed:154.0f/255.0f green:153.0f/255.0f blue:151.0f/255.0f alpha:1].CGColor;
        cell.img_photo.layer.cornerRadius =0.5f;
        cell.img_photo.layer.masksToBounds =YES;
        
        if (!cell.popupPreviewRecognizer) {
            cell.popupPreviewRecognizer = [[STPopupPreviewRecognizer alloc] initWithDelegate:self];
        }
        
        Home_tableview_data_share *shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        [cell.img_photo sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:[UIImage imageNamed:@"default_user_image"]];
        cell.data = shareObj;
        NSString *strPrice = @"";
        
        if ( shareObj.price.length != 0 )
            strPrice = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:shareObj.currency], shareObj.price];
        
        if ([strPrice isEqualToString:@"(null)(null)"])
            cell.lbl_price.text = @"";
        else
            cell.lbl_price.text = strPrice;
        
        if ( [cell.lbl_price.text isEqualToString:@""] )
            cell.lbl_price.hidden = YES;
        else
            cell.lbl_price.hidden = NO;

        cell.tag = indexPath.row;
        
        for (id gesture in cell.gestureRecognizers)
        {
            if ([gesture isKindOfClass:[MyLongPressGestureRecognizer class]])
            {
                [cell removeGestureRecognizer:gesture];
            }
        }
        
        MyLongPressGestureRecognizer *lpgr = [[MyLongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editPromotion:)];
        lpgr.indexPath = indexPath;
        lpgr.delaysTouchesBegan = YES;
        [cell addGestureRecognizer:lpgr];
        
        return cell;
    }
    
    static NSString *CellIdentifier = @"MJCollectionViewCell";
    MJCollectionViewCell *cell = (MJCollectionViewCell *)[cv dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.tag = indexPath.row;
    
    //set offset accordingly
    CGFloat yOffset = ((self.tbl_news.contentOffset.y - cell.frame.origin.y) / IMAGE_HEIGHT) * IMAGE_OFFSET_SPEED;
    cell.imageOffset = CGPointMake(0.0f, yOffset);
    
    NSArray *cellData = self.composed_feeds[indexPath.row];
    
    if ( cellData.count > 0 )
    {
        [cell.MJImageView sd_setImageWithURL:[NSURL URLWithString:((Home_tableview_data_share *)cellData[0]).image_path] placeholderImage:[UIImage imageNamed:@"background"]];
    }
    else
    {
        cell.MJImageView.image = [UIImage imageNamed:@"background"];
    }
    
    CategoryObject *obj = ((CategoryObject *)self.array_categories[indexPath.row]);
    
    cell.lblCategoryName.text = obj.categoryName;
    cell.lblFollowers.text = obj.categoryFollowers;
    cell.lblPosts.text = [NSString stringWithFormat:@"%ld", (long)cellData.count];

    if ( !isOweNerorNot )
    {
        if ( [obj.categoryUserFollower isEqualToString:@"no"] )
            [cell.btnFollow setSelected:NO];
        else
            [cell.btnFollow setSelected:YES];
        cell.btnFollow.hidden = NO;

        cell.btnFollow.tag = indexPath.row;
        [cell.btnFollow addTarget:self action:@selector(btn_category_follow_click:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        cell.btnFollow.hidden = YES;
    }
    
    return cell;
}

-(IBAction)btn_category_follow_click:(id)sender{

    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    UIButton    *btn = sender;
    CategoryObject *obj = ((CategoryObject *)self.array_categories[btn.tag]);

    NSLog(@"=======[%@, %@, %@, %@, %@]", obj.categoryId, obj.categoryName, obj.categoryDescs, obj.categoryFollowers, obj.categoryUserFollower);
    
    int currentFollowers = [[obj.categoryFollowers stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    
    BOOL categoryFollowed = NO;
    
    if ( !isOweNerorNot )
    {
        if ( [obj.categoryUserFollower isEqualToString:@"no"] )
        {
            categoryFollowed = NO;
        }
        else
        {
            categoryFollowed = YES;
        }
    }
    else
    {
        return;
    }

    
    if (categoryFollowed) {
        url=@"post_user_unfollow.php";
        currentFollowers--;
        obj.categoryUserFollower = @"no";
    }else{
        url=@"post_user_following.php";
        currentFollowers++;
        obj.categoryUserFollower = @"yes";
    }
    
    fastCategoryFollow = 1;
    obj.categoryFollowers =[NSString stringWithFormat:@"%d", currentFollowers];
    self.lbl_total_followers_tbl.text = obj.categoryFollowers;
    NSLog(@"------>current followers:%d", currentFollowers);
    [self.tbl_news reloadData];
    
    NSLog(@"%@%@",[[Singleton sharedSingleton] getBaseURL],url);
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserCategoryFollowingAPIResponce:) name:@"14242" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserCategoryFollowingAPIResponce:) name:@"-14242" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            LOGINEDUSERID, @"uid",
                            self.profile_share_obj.user_id, @"following_id",
                            obj.categoryId, @"category_ids",
                            nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14242" :params];
}

-(void)postUserCategoryFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
    }
}

-(void)FailpostUserCategoryFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if ( collectionView == collectionViewObj )
    {
        self.image_detail_viewObj.shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        self.image_detail_viewObj.arrayFeedArray = self.array_promotions;
        self.image_detail_viewObj.currentSelectedIndex = indexPath.row;
        self.image_detail_viewObj.promotion = YES;
        
        [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
        
        return;
    }
    
    categoryInfoView.user_id = self.profile_share_obj.user_id;
    categoryInfoView.category = ((CategoryObject *)self.array_categories[indexPath.row]);
    [self.navigationController pushViewController:categoryInfoView animated:YES];
}

- (void)editPromotion:(MyLongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    NSIndexPath *indexPath = gestureRecognizer.indexPath;
    
    if (indexPath.row != -1 && indexPath.row < self.array_promotions.count)
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Promotion Options"];
        
        NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
        NSString *uid2=[NSString stringWithFormat:@"%ld",(long)[self.profile_share_obj.user_id integerValue]];
        
        Home_tableview_data_share *shareObj = [self.array_promotions objectAtIndex:indexPath.row];
        
        if ([uid1 isEqualToString:uid2])
        {
            [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
                
            }];
            
            [sheet addButtonWithTitle:@"Edit" block:^{
                
            }];
        }
        else
        {
            [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
                
            }];
        }
        
        [sheet addButtonWithTitle:@"Share" block:^{
            NSString *textToShare = shareObj.description;
            NSString *myWebsite = shareObj.image_path;
            
            NSArray *objectsToShare = @[textToShare, myWebsite];
            
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
            
            NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                           UIActivityTypePrint,
                                           UIActivityTypeAssignToContact,
                                           UIActivityTypeSaveToCameraRoll,
                                           UIActivityTypeAddToReadingList,
                                           UIActivityTypePostToFlickr,
                                           UIActivityTypePostToVimeo,
                                           UIActivityTypePostToFacebook,
                                           UIActivityTypePostToTwitter,
                                           UIActivityTypePostToWeibo,
                                           UIActivityTypeMessage,
                                           UIActivityTypeMail];
            
            activityVC.excludedActivityTypes = excludeActivities;
            
            [self presentViewController:activityVC animated:YES completion:nil];
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        
        [sheet showInView:self.view];
    }
}

#pragma mark UITableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.composed_feeds.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 40.0)];
    
    MyControl *control = [[MyControl alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.frame.size.width, 40.0)];
    control.backgroundColor = [UIColor whiteColor];
    control.section = section;
    [control addTarget:self action:@selector(viewHeaderTouched:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, tableView.frame.size.width, 40.0)];
    label.text = ((CategoryObject *)self.array_categories[section]).categoryName;
    label.textColor = [UIColor flatSkyBlueColorDark];
    label.font = [UIFont boldSystemFontOfSize:15.0];
    UILabel *detaillabel = [[UILabel alloc] initWithFrame:CGRectMake(-10.0, 0.0, tableView.frame.size.width, 40.0)];
    detaillabel.text = @"+ Follow";
    [detaillabel setTextAlignment:NSTextAlignmentRight];
    detaillabel.textColor = [UIColor flatRedColor];
    detaillabel.font = [UIFont boldSystemFontOfSize:15.0];
    
    [view addSubview:control];
    [view addSubview:label];
    [view addSubview:detaillabel];
    view.userInteractionEnabled = YES;
    
    return view;
}

- (void)viewHeaderTouched:(MyControl *)control
{
    categoryInfoView.user_id = self.user_id;
    categoryInfoView.category = ((CategoryObject *)self.array_categories[control.section]);
    [self.navigationController pushViewController:categoryInfoView animated:YES];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *header = ((CategoryObject *)self.array_categories[section]).categoryName;
//    return header;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ORGContainerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ORGContainerCell"];
    NSArray *cellData = self.composed_feeds[indexPath.section];
    [cell setCollectionData:cellData section:indexPath.section];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)didSelectItemFromCollectionView:(NSNotification *)notification
{
    NSIndexPath *indexPath = notification.userInfo[@"IndexPath"];
    
    self.image_detail_viewObj.shareObj = self.composed_feeds[indexPath.section][indexPath.row];
    
    self.image_detail_viewObj.arrayFeedArray = self.composed_feeds[indexPath.section];
    self.image_detail_viewObj.currentSelectedIndex = indexPath.row;
    self.image_detail_viewObj.promotion = NO;
    
    [self.navigationController pushViewController:self.image_detail_viewObj animated:YES];
}

#pragma mark Method for Stop Header scrolling in UITableview
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ( scrollView == tbl_news )
    {
        for(MJCollectionViewCell *view in self.tbl_news.visibleCells) {
            CGFloat yOffset = ((self.tbl_news.contentOffset.y - view.frame.origin.y) / IMAGE_HEIGHT) * IMAGE_OFFSET_SPEED;
            view.imageOffset = CGPointMake(0.0f, yOffset);
        }
    }

//    CGFloat sectionHeaderHeight = 15;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
}

#pragma mark Action sheet
-(IBAction)btn_photo_option_click:(id)sender {
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;

    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    
    if ([shareObj.image_owner isEqualToString:@"yes"]) {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
            
        }];
        
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        NSLog(@"Report Inappropriate");
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
        
    }];
    
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
}

-(void)report_inappropriate:(int )index {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReportInappropriateResponce:) name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetReportInappropriateResponce:) name:@"-77703" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

-(void)getReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)FailgetReportInappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"77703" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-77703" object:nil];
}

#pragma mark List Of Likers
-(IBAction)btn_show_likers_click:(id)sender {
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;

    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

#pragma mark Go to Profile
-(IBAction)btn_profile_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;

    NSInteger tag = ((UIButton *)sender).tag;
    NSLog(@"%ld",(long)tag);
    /*
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
     */
}

#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView == denayAlertViewObj) {
        if(buttonIndex == 0) {
            NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
            NSString *key = SIGNSALTAPIKEY;
            NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
            NSString *sig = [StaticClass returnMD5Hash :tempStr];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIResponce:) name:@"14243" object:nil];
            
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIResponce:) name:@"-14243" object:nil];
            
            NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
            NSLog(@"requestStr:%@",requestStr);
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sig, @"sign",
                                    salt, @"salt",
                                    @"2",@"flag",
                                    [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                    self.profile_share_obj.user_id,@"to_id",nil];
            NSLog(@"params:%@",params);
            AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
            [networkQueue queueItems:requestStr :@"14243" :params];
        }

    }
    else {
        if (alertView.tag==1) {
            if (buttonIndex!=0) {
                NSLog(@"DELETE");
                [self delete_photo];
            }
        }
        
        if (alertView.tag==2) {
            if (buttonIndex==0) {
                return;
            }
            [self report_inappropriate:(int)buttonIndex];
            
        }
    }
}

#pragma mark Delete Photo
-(void)delete_photo {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getdeleteResponce:) name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-602" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    self.tableViewDataSource.posts = self.array_feeds;// copy];
    self.tableViewDelegate.posts = self.array_feeds;// copy];
    [self.collectionViewObj reloadData];
    [self.tbl_news reloadData];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"602":nil];
    
}

-(void)getdeleteResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"602" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-602" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
    }
}

#pragma mark Star Rating

-(void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRatingResponce:) name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-603" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    NSLog(@"TAG:%ld",(long)control.tag);
    cellClickIndex=control.tag;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"603":nil];
    
    obj.my_rating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
}

-(void)getRatingResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"603" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-603" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"]isEqualToString:@"1"]) {
        NSLog(@"%@",[result valueForKey:@"success"]);
        NSDictionary *dataDict = [result objectForKey:@"data"];
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex];
        
        shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
        shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
        shareObj.my_rating = [NSString stringWithFormat:@"%ld",(long)[[dataDict objectForKey:@"rate"] integerValue]];

        [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:cellClickIndex inSection:0]]];
//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark
#pragma mark like unlike call
-(IBAction)btn_like_click:(id)sender{
    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;

    NSInteger tag = (int)((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    /////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        
    }else{
        
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];
}

-(void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    //[SVProgressHUD show];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlikeResponce:) name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-601" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"601":nil];
    
}

-(void)FailNewsReson:(NSNotification *)notification {
    [SVProgressHUD dismiss];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"62" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"-62" object:nil];
}

-(void)getlikeResponce:(NSNotification *)notification {
    [SVProgressHUD dismiss];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"601" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-601" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
//        
//        if ([shareObj.liked isEqualToString:@"no"]) {
//            shareObj.liked=@"yes";
//            int count =[shareObj.likes intValue];
//            count++;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            
//        }else{
//            
//            shareObj.liked=@"no";
//            int count =[shareObj.likes intValue];
//            count--;
//            shareObj.likes=[NSString stringWithFormat:@"%d",count];
//            
//            if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//        }
//        [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
//        [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];

//        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark

// Single tap for fullscreen image
-(IBAction)handleSingleTap:(UITapGestureRecognizer *)gesture {
//    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
//    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
//    which_image_liked=tappedRow.section;
//    
//    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
//    UIImageView *nprfullscreenimg = [[UIImageView alloc] init];
//    
//    [nprfullscreenimg sd_setImageWithURL:[NSURL URLWithString:shareObj.image_path] placeholderImage:Nil];
//    
//    nprfullscreenimg.contentMode = UIViewContentModeScaleToFill;
//    
//    nprfullscreenimg.frame = CGRectMake(0, 20, 320, 418);
//    
//    UIButton *btnclose = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnclose setFrame:CGRectMake(290, 30, 26, 26)];
//    [btnclose setImage:[UIImage imageNamed:[NSString stringWithFormat:@"closebtn_pop.png"]] forState:UIControlStateNormal];//with image
//    [btnclose addTarget:self action:@selector(btnclosefullscreenClick) forControlEvents:UIControlEventTouchUpInside];
//    
//    imgfullscreenviewobj = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
//    [imgfullscreenviewobj setBackgroundColor:[UIColor blackColor]];
//    imgfullscreenviewobj.alpha =1.0;
//    
//    [self.view addSubview:imgfullscreenviewobj];
//    [imgfullscreenviewobj addSubview:nprfullscreenimg];
//    [imgfullscreenviewobj addSubview:btnclose];
}

-(IBAction)btnclosefullscreenClick{
    [imgfullscreenviewobj removeFromSuperview];
    [self get_news_feed];
    [self block_user_list];
}

#pragma mark Double Tab for like
-(IBAction)handleLikeTap:(UITapGestureRecognizer *)gesture {
//    CGPoint touchLocation = [gesture locationOfTouch:0 inView:self.tbl_news];
//    NSIndexPath *tappedRow = [self.tbl_news indexPathForRowAtPoint:touchLocation];
//    
//    which_image_liked=tappedRow.section;
//    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tappedRow.section];
//    
//    if ([shareObj.liked isEqualToString:@"no"]) {
//        [self btn_like_heart_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
//    }
//    
//    [self.img_like_heart setAlpha:0.0];
//    [self.img_like_heart setHidden:NO];
//    
//    [UIView animateWithDuration:0.7 animations:^{
//        [self.img_like_heart setAlpha:1.0];
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.7 animations:^{
//            [self.img_like_heart setAlpha:0.0];
//        } completion:^(BOOL finished) {
//            [self.img_like_heart setHidden:YES];
//        }];
//    }];
}

-(void)btn_like_heart_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getlike_heart_Responce:) name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailNewsReson:) name:@"-604" object:nil];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"604":nil];
}

-(void)getlike_heart_Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"604" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-604" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
        if ([shareObj.liked isEqualToString:@"no"]) {
            shareObj.liked=@"yes";
            int count =[shareObj.likes intValue];
            count++;
            shareObj.likes=[NSString stringWithFormat:@"%d",count];
            // NSLog(@"%@",shareObj.array_liked_by);
            
            [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
            
            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
            [self.tbl_news reloadItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:which_image_liked inSection:0]]];

//            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];
            // [[Singleton sharedSingleton]setArray_news:self.array_feeds];
        }
    }
}

#pragma mark
#pragma mark Handler Methods

-(NSString *)tagFromSender:(id)sender {
	return ((UIButton *)sender).titleLabel.text;
}

-(void)hashSelected:(id)sender {
    
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)atSelected:(id)sender {
    
    user_info_view.user_id=[[self tagFromSender:sender ] substringFromIndex:1];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)urlSelected:(id)sender {
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

-(void)userSelected:(id)sender {
    user_info_view.user_id=[self tagFromSender:sender];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

-(void)exclamationSelected:(id)sender {
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mrak
#pragma mark Comments Method
-(IBAction)btn_comment_click:(id)sender{
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    self.comments_list_viewObj.hidesBottomBarWhenPushed = YES;
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments;
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.comments_list_viewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
-(NSString *)get_time_different:(NSString *)datestring{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

#pragma mark - Show Mapview
-(IBAction)btn_mapview_click:(id)sender{
    self.mapviewObj.array_data =self.array_feeds;
    UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.mapviewObj];
    [self presentViewController:nav animated:YES completion:nil];
}

-(IBAction)btn_friend_click:(id)sender{
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    
    if ([self.profile_share_obj.is_user_frined isEqualToString:@"no"]||[self.profile_share_obj.is_user_frined isEqualToString:@"request not sent"]) {
        
        [btn_friend_tbl setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
        [btn_friend_siv setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];
        
        [btn_friend setImage:[UIImage imageNamed:@"AddFrndBtn.png"] forState:UIControlStateNormal];

        url=@"post_mutual_friend.php";
        
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postMutualFriendAPIResponce:) name:@"14244" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostMutualFriendAPIResponce:) name:@"-14244" object:nil];
        
        NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
        NSLog(@"requestStr:%@",requestStr);
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                self.profile_share_obj.user_id,@"to_id",nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14244" :params];
    }
    else{
       [btn_friend_tbl setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
        
        [self.btn_friend setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];
        [self.btn_friend_siv setImage:[UIImage imageNamed:@"UnfrndBtn.png"] forState:UIControlStateNormal];

        url=@"post_mutual_friend.php";
        
        NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
        NSString *key = SIGNSALTAPIKEY;
        NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
        NSString *sig = [StaticClass returnMD5Hash :tempStr];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAcceptRequestAPIFlag2Responce:) name:@"14246" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostAcceptRequestAPIFlag2Responce:) name:@"-14246" object:nil];
        
        NSString *requestStr =[NSString stringWithFormat:@"%@post_accept_request.php",[[Singleton sharedSingleton] getBaseURL]];
        NSLog(@"requestStr:%@",requestStr);
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                sig, @"sign",
                                salt, @"salt",
                                @"2",@"flag",
                                [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"from_id",
                                self.profile_share_obj.user_id,@"to_id",nil];
        NSLog(@"params:%@",params);
        AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
        [networkQueue queueItems:requestStr :@"14246" :params];
    }
}

-(void)postAcceptRequestAPIFlag2Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    NSLog(@"result:%@",result);
    
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    
    [self get_user_info];
    
    btnAccept.hidden = YES;
    btnNotNow.hidden = YES;
    btnNotNowSiv.hidden = YES;
    
    btnAcceptTbl.hidden = YES;
    btnNotNowTbl.hidden = YES;
    
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Not Now Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];

}

-(void)FailpostAcceptRequestAPIFlag2Responce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14246" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14246" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)postMutualFriendAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    
    NSString *strMsg=[[NSString alloc] initWithData:[response responseData] encoding:NSUTF8StringEncoding];
    NSLog(@"strMsg:%@",strMsg);
    
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        imgRequestSent.hidden = NO;
        imgRequestSentTbl.hidden=NO;
        [self get_user_info];
    }
    
    if ( result == nil || [[result valueForKey:@"success"] isEqualToString:@"2"]) {
        [self get_user_info];
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Sent Request Successfully" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
}

-(void)FailpostMutualFriendAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14244" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14244" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

-(void)postAcceptRequestAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
    }
}

-(void)FailpostAcceptRequestAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14243" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14243" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma mark - Follow and Following
-(IBAction)btn_follow_click:(id)sender{

    NSLog(@"%@", NSStringFromSelector(_cmd));

    if ([self.objUserName_lbl.text isEqualToString:@""])
        return;
    
    if ( isUserPrivate )
    {
        [self btn_friend_click:sender];
        return;
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *url;
    if (![self.profile_share_obj.is_user_follow isEqualToString:@"no"]) {
        [self.btn_follow setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_siv setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        url=@"post_user_unfollow.php";
    }else{
        [self.btn_follow setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_tbl setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
        [self.btn_follow_siv setImage:[UIImage imageNamed:@"Followingbtn.png"] forState:UIControlStateNormal];
         url=@"post_user_following.php";
    }
    NSLog(@"%@%@",[[Singleton sharedSingleton] getBaseURL],url);
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postUserFollowingAPIResponce:) name:@"14242" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailpostUserFollowingAPIResponce:) name:@"-14242" object:nil];
    
    NSString *requestStr =[NSString stringWithFormat:@"%@%@",[[Singleton sharedSingleton] getBaseURL],url];
    NSLog(@"requestStr:%@",requestStr);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            self.profile_share_obj.user_id,@"following_id",nil];
    NSLog(@"params:%@",params);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems:requestStr :@"14242" :params];
}

-(void)postUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"-2"]) {
        
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeRed title:@"Please retry" linedBackground:AJLinedBackgroundTypeDisabled hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        [self get_user_info];
        
        
        //akshay
       // if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
            NSDictionary *dataDict=[result objectForKey:@"data"];
            if ([dataDict count]>0) {
                NSDictionary *tempFollwingDict=[dataDict objectForKey:@"following_username"];
                NSMutableArray *tempArray=[[NSMutableArray alloc] init];
                for (NSDictionary *key in tempFollwingDict) {
                    NSString *str=[StaticClass urlDecode: [key objectForKey:@"following_uname"]];
                    [tempArray addObject:str];
                }
                [[Singleton sharedSingleton] setCurrentDictFollowing:tempArray];
            }
       // }
    }
}

-(void)FailpostUserFollowingAPIResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14242" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14242" object:nil];
    [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Please cheack your internet connection." linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
}

#pragma Block_User_list
-(void) block_user_list
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getBlockUserResponce:) name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailBlockUserResponce:) name:@"-1424" object:nil];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_list.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1424":nil];
}
-(void)DidClicktoBlockUser
{
    if([self.BlockUser_Btn.currentTitle isEqualToString:@"Block User"])
    {
        NSString *buttonTitle = self.BlockUser_Btn.currentTitle;
        if ([buttonTitle length] > 0) {
            Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
        }
        [self get_block_user];
    }
    else if ([self.BlockUser_Btn.currentTitle isEqualToString:@"Unblock User"])
    {
        NSString *buttonTitle = self.BlockUser_Btn.currentTitle;

        if ([buttonTitle length] > 0) {
            Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
        }
        [self get_unblock_user];
    }

}
-(void)getBlockUserResponce:(NSNotification*)notification
{
   
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSMutableArray *tempArray =[result valueForKey:@"data"];
        
        for (BlockUserRecord * obj in tempArray)
        {
            if([self.profile_share_obj.user_id isEqualToString:(NSString *)obj])
            {
     
                NSString *buttonTitle = self.BlockUser_Btn.currentTitle;
                NSLog(@"comparision successful");
                Block_User_Tittle_Str = [[buttonTitle substringToIndex:[buttonTitle length] - 5]lowercaseString ];
                [self get_unblock_user];
                [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];

            }
            
        }
    }
   }
-(void)FailBlockUserResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)get_block_user {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1454" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_blockuser_get:) name:@"1454" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1454" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailblockuserReson:) name:@"-1454" object:nil];
    
    //http://www.techintegrity.in/mystyle/get_comments.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&block_user_id=%@&action=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],profile_share_obj.user_id,Block_User_Tittle_Str];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_for_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1454":nil];
}
-(void)response_blockuser_get:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1454" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1454" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
   if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
      
       [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"2"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
      
        [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
        

    }

}
-(void)FailblockuserReson :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)get_unblock_user
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1455" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(response_unblockuser_get:) name:@"1455" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1455" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailUnblockuserReson:) name:@"-1455" object:nil];
    
    //http://www.techintegrity.in/mystyle/get_comments.php?salt=123&sign=6d940cad513a80ee051f3c349852cc63&id=5
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&block_user_id=%@&action=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],profile_share_obj.user_id,Block_User_Tittle_Str];
    NSString *requestStr = [NSString stringWithFormat:@"%@block_user_for_comments.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"1455":nil];
  
}
-(void)response_unblockuser_get:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1455" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1455" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
        [self.BlockUser_Btn setTitle:@"Block User" forState:UIControlStateNormal];
    }
    if ([[result valueForKey:@"success"] isEqualToString:@"2"]) {
//        NSMutableArray *tempArray =[result valueForKey:@"data"];
     
        [self.BlockUser_Btn setTitle:@"Unblock User" forState:UIControlStateNormal];
    }
    }

-(void)FailUnblockuserReson :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1424" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1424" object:nil];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@" Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(IBAction)DidClickToShare:(id)sender
{
    if([profile_share_obj.is_user_follow isEqualToString:@"yes"])
    {
    
    NSInteger tag =((UIButton *)sender).tag;
    which_image_delete =tag;
    
        
    //Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    // if ([shareObj.image_owner isEqualToString:@"yes"]) {
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:nil];
    
    [sheet setDestructiveButtonWithTitle:self.BlockUser_Btn.currentTitle block:^{
        [self DidClicktoBlockUser];
        
    }];
    
    [sheet addButtonWithTitle:@"Report Inappropriate" block:^{
        
        
        [self report_Inappropriate];
    }];
    [sheet addButtonWithTitle:@"Copy Profile URL" block:^{
        
         NSString*objCopyProfileUrl=[NSString stringWithFormat:@"http://www.mahalkum.com/user_profile.html?uname=%@",self.profile_share_obj.username];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =objCopyProfileUrl;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"The User profile sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        //[alert release];
        
    }];
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    [sheet showInView:self.view];
        return;
    
    }
    else{
       

        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:nil];
        
        [sheet setDestructiveButtonWithTitle:self.BlockUser_Btn.currentTitle block:^{
            NSLog(@"%@",self.BlockUser_Btn.currentTitle);
             [self DidClicktoBlockUser];
            
            
        }];
        [sheet addButtonWithTitle:@"Report Inappropriate" block:^{
            
            
            [self report_Inappropriate];
        }];
        [sheet addButtonWithTitle:@"Copy Profile URL" block:^{
            
            NSString*objCopyProfileUrl=[NSString stringWithFormat:@"http://www.mahalkum.com/user_profile.html?uname=%@",self.profile_share_obj.username];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =objCopyProfileUrl;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"The User profile sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            
        }];
        
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
    }
    
}
-(void)report_Inappropriate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getreport_InappropriateResponce:) name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Failgetreport_InappropriateResponce:) name:@"-7773" object:nil];
    
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%@&uid=%@",salt,sig,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_user.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"7773":nil];
}

-(void)getreport_InappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        
        Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_delete];
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        //        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
    }
}

-(void)Failgetreport_InappropriateResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7773" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7773" object:nil];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Google Map Delegate
- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidStartTileRendering");
}

- (void)mapViewDidFinishTileRendering:(GMSMapView *)mapView {
    NSLog(@"mapViewDidFinishTileRendering");
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
//    startlocation = googlemapview.myLocation.coordinate;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    startlocation = delegate.locationManager.location.coordinate;

    GMSMarker   *marker1 = [GMSMarker markerWithPosition:startlocation];
    marker1.title = @"Current Location";
    marker1.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker1.map = googlemapview;
//    marker1.userData = marker1;
    marker1.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    bounds = [bounds includingCoordinate:destinationlocation];
    bounds = [bounds includingCoordinate:startlocation];
    
    [googlemapview animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];

//    GMSMutablePath *path = [GMSMutablePath path];
//    [path addCoordinate:googlemapview.myLocation.coordinate];
//    [path addCoordinate:destinationlocation];
//    
//    GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
//    rectangle.strokeWidth = 2.f;
//    rectangle.map = googlemapview;
    
    NSMutableArray *waypointStrings = [NSMutableArray array];
    NSString *startPositionString = [NSString stringWithFormat:@"%f,%f",startlocation.latitude,startlocation.longitude];
    [waypointStrings addObject:startPositionString];
    NSString *endPositionString = [NSString stringWithFormat:@"%f,%f",destinationlocation.latitude,destinationlocation.longitude];
    [waypointStrings addObject:endPositionString];

    NSLog(@"Map start=%@, end=%@", startPositionString, endPositionString);
    MDDirectionService *mds = [[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    NSDictionary *query = @{ @"sensor" : @"false",
                             @"waypoints" : waypointStrings };

    [mds setDirectionsQuery:query
               withSelector:selector
               withDelegate:self];


    return NO;
}

-(void)addDirections:(NSDictionary *)json{
    if ( [json[@"routes"] count] > 0 )
    {
        NSDictionary *routes = json[@"routes"][0];
        NSDictionary *route = routes[@"overview_polyline"];
        NSString *overview_route = route[@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.strokeWidth = 2.f;
        polyline.map = googlemapview;
    }
}


- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSLog(@"didTapInfoWindowOfMarker");
    [self openDirectionsInGoogleMaps];
}

- (void)openDirectionsInGoogleMaps
{
    NSLog(@"openDirectionsInGoogleMaps");
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];

//    GoogleDirectionsWaypoint *start = [[GoogleDirectionsWaypoint alloc] init];
//    start.queryString = @"";
//    start.location = startlocation;
//    directionsDefinition.startingPoint = start;
    
    directionsDefinition.startingPoint = nil;

    GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
    destination.queryString = @"";
    destination.location = destinationlocation;
    directionsDefinition.destinationPoint = destination;
    
    directionsDefinition.travelMode = kTravelModeDriving;
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}

- (IBAction)show_chatting:(id)sender {
    
    NSString *loginUserID = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];

    if ([loginUserID isEqualToString:self.profile_share_obj.user_id])
    {
        self.inbox_viewObj.userID = loginUserID;
        
        [self presentViewController:self.inbox_viewObj animated:YES completion:nil];
    }
    else
    {
        self.chat_viewObj.toUserID = self.profile_share_obj.user_id;
        self.chat_viewObj.toUserFullName = self.profile_share_obj.name;
        self.chat_viewObj.toUserName = self.profile_share_obj.username;
        self.chat_viewObj.toUserImage = self.profile_share_obj.image;
        
        [self presentViewController:self.chat_viewObj animated:YES completion:nil];
    }
    
    //NSLog(@"show_chatting:====>%@, %@, %@, %@", self.profile_share_obj.user_id, self.profile_share_obj.image, self.profile_share_obj.username, self.profile_share_obj.name);
}

- (IBAction)gotoBigMap:(id)sender {
    self.map_viewObj.str_latitude=self.profile_share_obj.latitude;
    self.map_viewObj.str_longitude=self.profile_share_obj.longitude;
    self.map_viewObj.str_name = self.profile_share_obj.name;
    
    [self.navigationController pushViewController:self.map_viewObj animated:YES];
}
@end
