//
//  CustomImageEditor.m
//  My Style
//
//  Created by Tis Macmini on 7/19/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "CustomImageEditor.h"

@interface CustomImageEditor ()

@end

@implementation CustomImageEditor
@synthesize image;
@synthesize scrollview,img_photoview;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)btn_done_click:(id)sender{
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(0,101, 640, 640));
    UIImage *image1 = [UIImage imageWithCGImage:imageRef];
    self.img_photoview.image =image1;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scrollview.zoomScale =1;
    self.scrollview.minimumZoomScale =1;
    self.scrollview.maximumZoomScale =10.0f;

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:YES];
    //[[UIApplication sharedApplication]setStatusBarHidden:YES];
    self.img_photoview.image = self.image;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.img_photoview;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
