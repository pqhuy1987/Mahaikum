////////////////////////////////////////
//  Created by Aenea for codecanyon.net
//  01/10/11 updated 29/08/12
////////////////////////////////////////

#import <UIKit/UIKit.h>

#import "Share_photo_ViewController.h"
#import "CustomImageEditor.h"

@interface aCameraMoreViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
	UIImage *workingImage;              // im use this for filters&save
    UIImage *sourceImage;               // this is a selected image or camera image
	IBOutlet UIImageView *imageView;    // UIImageView
    IBOutlet UIScrollView *scrollView; // scroll buttons
    
    Share_photo_ViewController *share_photo_viewObj;
    int flag;
    
    UIImagePickerController *imagepicker;
    
}

@property(nonatomic,strong) UIImagePickerController *imagepicker;
@property(nonatomic,strong) Share_photo_ViewController *share_photo_viewObj;

@property (nonatomic, strong) UIImage *workingImage;             // im use this for filters&save
@property (nonatomic, strong) UIImage *sourceImage;              // this is a selected image or camera image
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property ( nonatomic , strong )  IBOutlet UIScrollView *scrollView; // scroll buttons


- (IBAction) chooseImageFromAlbum:(id) sender;      // activate Album
- (IBAction) takeImageFromCamera:(id) sender;       // activate Camera only works on iphone
- (IBAction) SendingImage:(id)sender;               // activate Share module, remember read readme for configuration

@property(nonatomic,strong)IBOutlet UIBarButtonItem *btn_done;
@property(nonatomic,strong)IBOutlet UIBarButtonItem *btn_camera;
@property(nonatomic,strong)IBOutlet UIBarButtonItem *btn_photo_libary;

@end

