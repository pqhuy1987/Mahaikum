////////////////////////////////////////
//  Created by Aenea for codecanyon.net
//  01/10/11 updated 29/08/12
////////////////////////////////////////

#import "aCameraMoreViewController.h"
#import "Share_photo_ViewController.h"
#import "StaticClass.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "DemoImageEditor.h"
//#import "AFPhotoEditorControllerOptions.h"
//#import "AFPhotoEditorController.h"
//#import "YCameraViewController.h"

#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "ALTabBarController.h"
#import "TWPhotoPickerController.h"
#import "TGCameraViewController.h"

@interface aCameraMoreViewController () <DBCameraViewControllerDelegate, CLImageEditorDelegate>  {
    DBCameraContainerViewController *cameraContainer;
    UINavigationController *nav;
}

@property(nonatomic,strong) DemoImageEditor *imageEditor;
@property(nonatomic,strong) ALAssetsLibrary *library;

@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btn_take_photo;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btn_cancel;;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btn_photo_library;
@property (nonatomic, strong) IBOutlet UIButton *btn_photo_library1;

@end

static NSInteger flashFlg=0;
static NSInteger cameraLibraryFlg=0;

//static NSInteger libraryFlg=0;
//static NSInteger shareViewPresent=0;

@implementation aCameraMoreViewController

@synthesize imageView;
@synthesize workingImage;
@synthesize sourceImage;
@synthesize scrollView,share_photo_viewObj;
@synthesize btn_done,btn_camera,btn_photo_libary,btn_photo_library1,imagepicker;
@synthesize library = _library;
@synthesize imageEditor = _imageEditor;

-(IBAction)btn_back_clike:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];    
}

-(void)gotohome {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
        NSLog(@"Callls");
        [StaticClass saveToUserDefaults:@"1" :IS_HOME_PAGE_REFRESH];
        [StaticClass saveToUserDefaults:@"0" :@"IMAGEUPLOADEDSUCCESSFULLY"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
   
    
}

-(void)go_to_home_view1:(NSNotification *)notification {
    @try {
        [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(gotohome) userInfo:self repeats:NO];

    }
    @catch (NSException * ex) {
        NSLog(@"Exception: [%@]:%@",[ex  class], ex );
        
    }
    @finally {
        NSLog(@"finally");
    }
}

-(void) viewDidLoad {
    [super viewDidLoad];
    flag=0;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    
    cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"go_to_home_view_from_share" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_home_view1:) name:@"go_to_home_view_from_share" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {    
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    //shareViewPresent=0;
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGEUPLOADEDSUCCESSFULLY"] isEqualToString:@"1"]) {
        return;
    }
    
    [imageView setImage:workingImage];
    self.sourceImage =self.workingImage;
    self.navigationController.navigationBar.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    //shareViewPresent=0;
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGEUPLOADEDSUCCESSFULLY"] isEqualToString:@"1"]) {
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(gotohome) userInfo:self repeats:NO];
        return;
    }
    
    if (flag==1) {
        flag=0;
        [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        }];
        return;
    }
    else if (flag==2) {
        flag=0;
        self.share_photo_viewObj.img_photo =self.workingImage;
        [self presentViewController:self.share_photo_viewObj animated:YES completion:^{
            
        }];
        return;
    }
    
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGECAPTUREDORNOT"] isEqualToString:@"0"]) {
        [StaticClass saveToUserDefaults:@"-1" :@"IMAGECAPTUREDORNOT"];
        [self dismissViewControllerAnimated:YES completion:^{
            //[self.tabBarController setSelectedIndex:0];
        }];
    }
    else {
        flag=1;
        
        
        imagepicker = [[UIImagePickerController alloc] init];
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            flashFlg=1;
            
            TGCameraNavigationController *navigationController = [TGCameraNavigationController newWithCameraDelegate:self];
            [self presentViewController:navigationController animated:YES completion:nil];

//            flashFlg=1;
//            imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
//            imagepicker.allowsEditing = YES;
//            imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
//            imagepicker.delegate = self;
//            imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
//            
//            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
//            [button setTitle:@"Library" forState:UIControlStateNormal];
//            [button setBackgroundColor:[UIColor darkGrayColor]];
//            [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
//            [imagepicker.view addSubview:button];
//            
//            UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
//            [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
//            [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
//            [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
//            [imagepicker.view addSubview:buttonFlashOnOff];
//            
//            [self presentViewController:imagepicker animated:YES completion:^{
//                
//            }];
        }
        else {
            flashFlg=0;
            cameraLibraryFlg=1;
//            imagepicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
//            imagepicker.delegate = self;
//            imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
            
            TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
            
            photoPicker.cropBlock = ^(UIImage *image) {
                //do something
                cameraLibraryFlg=0;
                
                self.sourceImage = image;
                self.workingImage = image;
                [self.imageView setImage:self.sourceImage];
                
                [self presentImageEditorWithImage:self.workingImage];
            };
            
            photoPicker.cancelBlock = ^() {
                if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    flag = 1;
                    imagepicker = [[UIImagePickerController alloc] init];
                    imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                    imagepicker.allowsEditing = YES;
                    imagepicker.delegate = self;
                    if (flashFlg==1) {
                        imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                    }
                    else {
                        imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                    }
                    imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
                    
                    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
                    [button setTitle:@"Library" forState:UIControlStateNormal];
                    [button setBackgroundColor:[UIColor darkGrayColor]];
                    [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
                    [imagepicker.view addSubview:button];
                    
                    flashFlg=1;
                    UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
                    [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
                    [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
                    [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
                    [imagepicker.view addSubview:buttonFlashOnOff];
                    
                    [self presentViewController:imagepicker animated:YES completion:^{
                        
                    }];
                }
                else
                {
                    [[UIApplication sharedApplication] setStatusBarHidden:NO];
                    ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
                    [altab tabWasSelected:0];
                    [self.tabBarController setSelectedIndex:0];
                }
                
                cameraLibraryFlg=0;
            };
            
            UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
            [navCon setNavigationBarHidden:YES];
            
            [self presentViewController:navCon animated:YES completion:NULL];

        }
        
        
//        [self presentViewController:imagepicker animated:YES completion:^{
//            
//        }];
    }
    
    return;
}

- (void)gotoLibrary:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        flag=1;
        cameraLibraryFlg=1;
//        imagepicker = [[UIImagePickerController alloc] init];
//        imagepicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
//        imagepicker.delegate = self;
//        imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
//        [self presentViewController:imagepicker animated:YES completion:^{
//            
//        }];
        TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
        
        photoPicker.cropBlock = ^(UIImage *image) {
            //do something
            cameraLibraryFlg=0;
            
            self.sourceImage = image;
            self.workingImage = image;
            [self.imageView setImage:self.sourceImage];
            
            [self presentImageEditorWithImage:self.workingImage];
        };
        
        photoPicker.cancelBlock = ^() {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                flag = 1;
                imagepicker = [[UIImagePickerController alloc] init];
                imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                imagepicker.allowsEditing = YES;
                imagepicker.delegate = self;
                if (flashFlg==1) {
                    imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                }
                else {
                    imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                }
                imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
                
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
                [button setTitle:@"Library" forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor darkGrayColor]];
                [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
                [imagepicker.view addSubview:button];
                
                flashFlg=1;
                UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
                [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
                [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
                [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
                [imagepicker.view addSubview:buttonFlashOnOff];
                
                [self presentViewController:imagepicker animated:YES completion:^{
                    
                }];
            }
            else
            {
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
                ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
                [altab tabWasSelected:0];
                [self.tabBarController setSelectedIndex:0];
            }
            
            cameraLibraryFlg=0;
        };

        UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
        [navCon setNavigationBarHidden:YES];
        
        [self presentViewController:navCon animated:YES completion:NULL];

    }];
}

- (void)gotoCamera:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        flag=1;
        imagepicker = [[UIImagePickerController alloc] init];
        imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        imagepicker.allowsEditing = YES;
        imagepicker.delegate = self;
        if (flashFlg==1) {
            imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
        }
        else {
            imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
        }
        imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
        [button setTitle:@"Library" forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor darkGrayColor]];
        [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
        [imagepicker.view addSubview:button];
        
        flashFlg=1;
        UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
        [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
        [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
        [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
        [imagepicker.view addSubview:buttonFlashOnOff];
        
        [self presentViewController:imagepicker animated:YES completion:^{
            
        }];
    }];
}

- (void)buttonFlashOnOffClick:(id)sender {
    UIButton *btn=(UIButton *)sender;
    if (flashFlg==1) {
        flashFlg=0;
        [btn setTitle:@"Flash On" forState:UIControlStateNormal];
        imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOff;
    }
    else {
        flashFlg=1;
        [btn setTitle:@"Flash Off" forState:UIControlStateNormal];
        imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
    }
}

- (void) dismissCamera:(id)cameraViewController{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];
    [cameraViewController restoreFullScreenMode];
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    [[Singleton sharedSingleton] setCurrentCapturedImage:image];
    
    self.workingImage=image;
    self.sourceImage=image;
    [self.imageView setImage:self.sourceImage];
    
    [cameraViewController restoreFullScreenMode];
    
    flag=2;
}

//////////// SHAREKIT ////////////
#pragma mark - Go to share image view
- (IBAction) SendingImage:(id) sender {
    if(self.workingImage){
        self.share_photo_viewObj.img_photo =self.workingImage;
        UINavigationController *nav1 =[[UINavigationController alloc]initWithRootViewController:self.share_photo_viewObj];
        [self presentViewController:nav1 animated:YES completion:nil];
    }
}

//////////// TAKE PICTURE ////////////
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    cameraLibraryFlg=0;
    
    self.sourceImage = info[UIImagePickerControllerEditedImage];
    self.workingImage = info[UIImagePickerControllerEditedImage];
    if (!info[UIImagePickerControllerEditedImage]) {
        self.sourceImage = info[UIImagePickerControllerOriginalImage];
        self.workingImage = info[UIImagePickerControllerOriginalImage
                                 ];
    }
    [self.imageView setImage:self.sourceImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:self.workingImage];
    }];
}

- (void)presentImageEditorWithImage:(UIImage*)image
{
    //begin
    //[[CLImageEditorTheme theme] setBackgroundColor:[UIColor blackColor]];
    //[[CLImageEditorTheme theme] setToolbarColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    //[[CLImageEditorTheme theme] setToolbarTextColor:[UIColor whiteColor]];
    //[[CLImageEditorTheme theme] setToolIconColor:@"white"];
    
    //[[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    //[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //end
    
    CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
    editor.delegate = self;
    
    [self presentViewController:editor animated:YES completion:nil];
}

#pragma mark- CLImageEditor delegate

- (void)imageEditor:(CLImageEditor *)editor didFinishEdittingWithImage:(UIImage *)image
{
    [[Singleton sharedSingleton] setCurrentCapturedImage:image];
    
    self.workingImage=image;
    self.sourceImage=image;
    [self.imageView setImage:self.sourceImage];
    
//    [cameraViewController restoreFullScreenMode];
    
    flag=2;
    [editor dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (cameraLibraryFlg==1) {
        [self dismissViewControllerAnimated:YES completion:^{
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                flag = 1;
                imagepicker = [[UIImagePickerController alloc] init];
                imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                imagepicker.allowsEditing = YES;
                imagepicker.delegate = self;
                if (flashFlg==1) {
                    imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                }
                else {
                    imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
                }
                imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
                
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
                [button setTitle:@"Library" forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor darkGrayColor]];
                [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
                [imagepicker.view addSubview:button];
                
                flashFlg=1;
                UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
                [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
                [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
                [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
                [imagepicker.view addSubview:buttonFlashOnOff];
                
                [self presentViewController:imagepicker animated:YES completion:^{
                    
                }];
            }
            else
            {
                [[UIApplication sharedApplication] setStatusBarHidden:NO];
                ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
                [altab tabWasSelected:0];
                [self.tabBarController setSelectedIndex:0];
            }
        }];
    }
    else {
        [picker dismissViewControllerAnimated:NO completion:^{
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
            [altab tabWasSelected:0];
            [self.tabBarController setSelectedIndex:0];
            
        }];
    }
    cameraLibraryFlg=0;
}

- (IBAction) takeImageFromCamera:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.imagepicker.allowsEditing = YES;
        [self presentViewController:self.imagepicker animated:YES completion:nil];
    }
    
}

- (IBAction) chooseImageFromAlbum:(id) sender {
    cameraLibraryFlg=1;
    //libraryFlg=1;
//    self.imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    [self presentViewController:self.imagepicker animated:YES completion:nil];
    
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    
    photoPicker.cropBlock = ^(UIImage *image) {
        //do something
        cameraLibraryFlg=0;
        
        self.sourceImage = image;
        self.workingImage = image;
        [self.imageView setImage:self.sourceImage];
        
        [self presentImageEditorWithImage:self.workingImage];
    };
    
    photoPicker.cancelBlock = ^() {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            flag = 1;
            imagepicker = [[UIImagePickerController alloc] init];
            imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagepicker.allowsEditing = YES;
            imagepicker.delegate = self;
            if (flashFlg==1) {
                imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
            }
            else {
                imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
            }
            imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
            [button setTitle:@"Library" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor darkGrayColor]];
            [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
            [imagepicker.view addSubview:button];
            
            flashFlg=1;
            UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
            [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
            [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
            [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
            [imagepicker.view addSubview:buttonFlashOnOff];
            
            [self presentViewController:imagepicker animated:YES completion:^{
                
            }];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
            [altab tabWasSelected:0];
            [self.tabBarController setSelectedIndex:0];
        }
        
        cameraLibraryFlg=0;
    };

    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    [navCon setNavigationBarHidden:YES];
    
    [self presentViewController:navCon animated:YES completion:NULL];
}

#pragma mark - Camera
-(void)open_photo_library {
    cameraLibraryFlg=1;
    //libraryFlg=1;
//    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
//    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    imagePickerController.delegate = self;
//    imagePickerController.allowsEditing =NO;
//    [self presentViewController:imagePickerController animated:YES completion:nil];
    
    TWPhotoPickerController *photoPicker = [[TWPhotoPickerController alloc] init];
    
    photoPicker.cropBlock = ^(UIImage *image) {
        //do something
        cameraLibraryFlg=0;
        
        self.sourceImage = image;
        self.workingImage = image;
        [self.imageView setImage:self.sourceImage];
        
        [self presentImageEditorWithImage:self.workingImage];
    };
    
    photoPicker.cancelBlock = ^() {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            flag = 1;
            imagepicker = [[UIImagePickerController alloc] init];
            imagepicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagepicker.allowsEditing = YES;
            imagepicker.delegate = self;
            if (flashFlg==1) {
                imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
            }
            else {
                imagepicker.cameraFlashMode=UIImagePickerControllerCameraFlashModeOn;
            }
            imagepicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,nil];
            
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 30)];
            [button setTitle:@"Library" forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor darkGrayColor]];
            [button addTarget:self action:@selector(gotoLibrary:) forControlEvents:UIControlEventTouchUpInside];
            [imagepicker.view addSubview:button];
            
            flashFlg=1;
            UIButton *buttonFlashOnOff = [[UIButton alloc] initWithFrame:CGRectMake(120, 10, 100, 30)];
            [buttonFlashOnOff setTitle:@"Flash Off" forState:UIControlStateNormal];
            [buttonFlashOnOff setBackgroundColor:[UIColor darkGrayColor]];
            [buttonFlashOnOff addTarget:self action:@selector(buttonFlashOnOffClick:) forControlEvents:UIControlEventTouchUpInside];
            [imagepicker.view addSubview:buttonFlashOnOff];
            
            [self presentViewController:imagepicker animated:YES completion:^{
                
            }];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarHidden:NO];
            ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
            [altab tabWasSelected:0];
            [self.tabBarController setSelectedIndex:0];
        }
        
        cameraLibraryFlg=0;
    };

    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:photoPicker];
    [navCon setNavigationBarHidden:YES];
    
    [self presentViewController:navCon animated:YES completion:NULL];
}

- (IBAction)btn_photo_library_click:(id)sender
{
    [self.imagepicker dismissViewControllerAnimated:YES completion:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(open_photo_library) userInfo:self repeats:NO];
}

-(IBAction)btn_cancle_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];
}
- (IBAction)takePhoto:(id)sender {
    [self.imagepicker takePicture];
    
    CustomImageEditor *obj =[[CustomImageEditor alloc]initWithNibName:@"CustomImageEditor" bundle:nil];
    obj.image=self.sourceImage;
    [self presentViewController:obj animated:YES completion:^{
        
    }];
}

#pragma mark Aviary SDK Delegate

- (void)displayEditorForImage:(UIImage *)imageToEdit
{
    //flag=1;
}

#pragma mark - Edit image click
-(IBAction)btn_edit_click:(id)sender{
    if(self.workingImage){
        [self displayEditorForImage:self.workingImage];
    }
}

- (void)viewDidUnload {
	self.imageView = nil;
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
    return (orientation == UIInterfaceOrientationLandscapeLeft ||
            orientation == UIInterfaceOrientationLandscapeRight);
}


#pragma mark -
#pragma mark - TGCameraDelegate required

- (void)cameraDidCancel
{
    [self dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
        [altab tabWasSelected:0];
        [self.tabBarController setSelectedIndex:0];
    }];
}

- (void)cameraDidTakePhoto:(UIImage *)image
{
    cameraLibraryFlg=0;
    
    self.sourceImage = image;
    self.workingImage = image;
    
    [self.imageView setImage:self.sourceImage];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:self.workingImage];
    }];
}

- (void)cameraDidSelectAlbumPhoto:(UIImage *)image
{
    cameraLibraryFlg=0;
    
    self.sourceImage = image;
    self.workingImage = image;
    
    [self.imageView setImage:self.sourceImage];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self presentImageEditorWithImage:self.workingImage];
    }];
}

#pragma mark -
#pragma mark - TGCameraDelegate optional

- (void)cameraWillTakePhoto
{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)cameraDidSavePhotoAtPath:(NSURL *)assetURL
{
    NSLog(@"%s album path: %@", __PRETTY_FUNCTION__, assetURL);
}

- (void)cameraDidSavePhotoWithError:(NSError *)error
{
    NSLog(@"%s error: %@", __PRETTY_FUNCTION__, error);
}

@end
