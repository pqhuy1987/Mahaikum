////////////////////////////////////////
//  Created by Aenea for codecanyon.net
//  01/10/11 updated 29/08/12
////////////////////////////////////////

#import "aCameraMoreViewController.h"
#import "Share_photo_ViewController.h"
#import "StaticClass.h"
#import "GPUImage.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "DemoImageEditor.h"
//#import "AFPhotoEditorControllerOptions.h"
//#import "AFPhotoEditorController.h"
//#import "YCameraViewController.h"

#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "CustomCamera.h"
#import "DBCameraGridView.h"

@interface aCameraMoreViewController () <DBCameraViewControllerDelegate>  {
    DBCameraContainerViewController *cameraContainer;
    UINavigationController *nav;
}

@property(nonatomic,retain) DemoImageEditor *imageEditor;
@property(nonatomic,retain) ALAssetsLibrary *library;

@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *btn_take_photo;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *btn_cancel;;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *btn_photo_library;
@property (nonatomic, retain) IBOutlet UIButton *btn_photo_library1;

@end

static NSInteger libraryFlg=0;

static NSInteger shareViewPresent=0;

@implementation aCameraMoreViewController

@synthesize imageView;
@synthesize workingImage;
@synthesize sourceImage;
@synthesize scrollView,share_photo_viewObj;
@synthesize btn_done,btn_camera,btn_photo_libary,btn_photo_library1,imagepicker;
@synthesize library = _library;
@synthesize imageEditor = _imageEditor;

-(IBAction)btn_back_clike:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];    
}

-(void)gotohome {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
        NSLog(@"Callls");
        [StaticClass saveToUserDefaults:@"1" :IS_HOME_PAGE_REFRESH];
        [StaticClass saveToUserDefaults:@"0" :@"IMAGEUPLOADEDSUCCESSFULLY"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"go_to_home_view" object:self];
   
    
}

-(void)go_to_home_view1:(NSNotification *)notification {
    @try {
        [NSTimer scheduledTimerWithTimeInterval:0.7 target:self selector:@selector(gotohome) userInfo:self repeats:NO];

    }
    @catch (NSException * ex) {
        NSLog(@"Exception: [%@]:%@",[ex  class], ex );
        
    }
    @finally {
        NSLog(@"finally");
    }
}

-(void) viewDidLoad {
    [super viewDidLoad];
    flag=0;
    self.view.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_fullscreen.png"]];
    
    cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"go_to_home_view_from_share" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(go_to_home_view1:) name:@"go_to_home_view_from_share" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {    
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    shareViewPresent=0;
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGEUPLOADEDSUCCESSFULLY"] isEqualToString:@"1"]) {
        return;
    }
    
    [imageView setImage:workingImage];
    self.sourceImage =self.workingImage;
    self.navigationController.navigationBar.hidden=YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    shareViewPresent=0;
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGEUPLOADEDSUCCESSFULLY"] isEqualToString:@"1"]) {
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(gotohome) userInfo:self repeats:NO];
        return;
    }
    
    if (flag==1) {
        flag=0;
        [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        }];
        return;
    }
    
    if ([[StaticClass retrieveFromUserDefaults:@"IMAGECAPTUREDORNOT"] isEqualToString:@"0"]) {
        [StaticClass saveToUserDefaults:@"-1" :@"IMAGECAPTUREDORNOT"];
        [self dismissViewControllerAnimated:YES completion:^{
            [self.tabBarController setSelectedIndex:0];
        }];
    }
    else {
        flag=1;
        
        nav=[[UINavigationController alloc] init];
        nav.viewControllers=[NSArray arrayWithObject:cameraContainer];
        [cameraContainer setFullScreenMode];
        [nav setNavigationBarHidden:YES];
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }
    
    return;
}

- (void) dismissCamera:(id)cameraViewController{
    if (shareViewPresent==1) {
        return;
    }
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];
    [cameraViewController restoreFullScreenMode];
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata
{
    [[Singleton sharedSingleton] setCurrentCapturedImage:image];
    
    self.workingImage=image;
    self.sourceImage=image;
    [self.imageView setImage:self.sourceImage];
    
    [cameraViewController restoreFullScreenMode];
   
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        self.share_photo_viewObj.img_photo =self.workingImage;
        [self presentViewController:self.share_photo_viewObj animated:YES completion:^{
            shareViewPresent=1;
        }];
    }];
}

//////////// SHAREKIT ////////////
#pragma mark - Go to share image view
- (IBAction) SendingImage:(id) sender {
    if(self.workingImage){
        self.share_photo_viewObj.img_photo =self.workingImage;
        UINavigationController *nav =[[UINavigationController alloc]initWithRootViewController:self.share_photo_viewObj];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

//////////// TAKE PICTURE ////////////
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.sourceImage = info[UIImagePickerControllerEditedImage];
    self.workingImage = info[UIImagePickerControllerEditedImage];
    if (!info[UIImagePickerControllerEditedImage]) {
        self.sourceImage = info[UIImagePickerControllerOriginalImage];
        self.workingImage = info[UIImagePickerControllerOriginalImage
                                 ];
    }
    [self.imageView setImage:self.sourceImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self displayEditorForImage:self.workingImage];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:NO completion:^{
        
    }];
}

- (IBAction) takeImageFromCamera:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.imagepicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.imagepicker animated:YES completion:nil];
    }
    
}

- (IBAction) chooseImageFromAlbum:(id) sender {
    libraryFlg=1;
    self.imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagepicker animated:YES completion:nil];
}

#pragma mark - Camera
-(void)open_photo_library {
    libraryFlg=1;
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing =NO;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (IBAction)btn_photo_library_click:(id)sender
{
    [self.imagepicker dismissViewControllerAnimated:YES completion:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(open_photo_library) userInfo:self repeats:NO];
}

-(IBAction)btn_cancle_click:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];
}
- (IBAction)takePhoto:(id)sender {
    [self.imagepicker takePicture];
    
    CustomImageEditor *obj =[[CustomImageEditor alloc]initWithNibName:@"CustomImageEditor" bundle:nil];
    obj.image=self.sourceImage;
    [self presentViewController:obj animated:YES completion:^{
        
    }];
}

#pragma mark Aviary SDK Delegate

- (void)displayEditorForImage:(UIImage *)imageToEdit
{
    flag=1;
}

#pragma mark - Edit image click
-(IBAction)btn_edit_click:(id)sender{
    if(self.workingImage){
        [self displayEditorForImage:self.workingImage];
    }
}

- (void)viewDidUnload {
	self.imageView = nil;
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
    return (orientation == UIInterfaceOrientationLandscapeLeft ||
            orientation == UIInterfaceOrientationLandscapeRight);
}

- (void)dealloc {
    [_library release];
    [_imageEditor release];
    
    [imagepicker release];
    [btn_done release];
    [btn_camera release];
    [btn_photo_libary release];
    
    [share_photo_viewObj release];
	[workingImage release];
    [sourceImage release];
	[imageView release];
    [super dealloc];
}

@end
