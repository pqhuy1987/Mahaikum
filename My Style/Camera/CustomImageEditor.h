//
//  CustomImageEditor.h
//  My Style
//
//  Created by Tis Macmini on 7/19/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//



@interface CustomImageEditor : UIViewController{
    UIImage *image;

    UIScrollView *scrollview;
    UIImageView *img_photoview;
}
@property(nonatomic,strong)UIImage *image;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong)IBOutlet UIImageView *img_photoview;
@end
