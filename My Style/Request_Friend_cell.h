//
//  Notification_setting_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Request_Friend_cell : UITableViewCell{
    UILabel *lbl_title;
    UIButton *btnAccept;
    UIButton *btnNotNow;
}
@property(nonatomic,strong)IBOutlet UILabel *lbl_title;
@property(nonatomic,strong)IBOutlet UIButton *btnAccept;
@property(nonatomic,strong)IBOutlet UIButton *btnNotNow;

@end
