//
//  Facebook_friend_list.h
//  My Style
//
//  Created by Tis Macmini on 6/13/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StaticClass.h"
#import "Singleton.h"
#import "Faceabook_find_friend_Share.h"
#import "Faceabook_find_friend_cell.h"
#import "DejalActivityView.h"

@interface Facebook_friend_list : UIViewController{
    NSMutableArray *array_friend;
    UITableView *tbl_friend;
}

@property(nonatomic,strong)NSMutableArray *array_friend;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_friend;
@property(nonatomic,strong)NSMutableArray *array_fb_id;
@property BOOL loadFriends;

@end
