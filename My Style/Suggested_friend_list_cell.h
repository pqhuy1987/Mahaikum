//
//  Suggested_friend_list_cell.h
//  My Style
//
//  Created by Tis Macmini on 6/21/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Collectionview_delegate.h"
#import "EGOImageButton.h"

static NSString *CollectionViewCellIdentifier1 = @"CollectionViewCellIdentifier";

@interface Suggested_friend_list_cell : UITableViewCell{

    EGOImageButton *btn_user_img;
    UILabel *lbl_name;
    UILabel *lbl_username;
    UILabel *lbl_dec;
    UILabel *lbl_private;
    
    UIButton *btn_follow;
}
@property(nonatomic,strong)IBOutlet UIButton *btn_follow;
@property(nonatomic,strong)IBOutlet UILabel *lbl_dec;
@property(nonatomic,strong)IBOutlet EGOImageButton *btn_user_img;
@property(nonatomic,strong)IBOutlet UILabel *lbl_name;
@property(nonatomic,strong)IBOutlet UILabel *lbl_username;

-(void)draw_collectionview_in_cell;
@property(nonatomic,strong)IBOutlet Collectionview_delegate *collectionView;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate index:(NSInteger)index;

@end
