//
//  Share_photo_ViewController.h
//  My Style
//
//  Created by Tis Macmini on 4/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Show_location_list.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "DCRoundSwitch.h"
//#import "AFPhotoEditorController.h"
#import "UITextView+AVTagTextView.h"

@interface Share_photo_ViewController : UIViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,AmazonServiceRequestDelegate,AVTagTextViewDelegate>{

    UIImage *img_photo;
    UIScrollView *scrollview;
    UIImageView *img_bg_firstcell;
    UIImageView *img_bg_secondcell;
    
    UILabel *lblheader,*lblsubheader1,*lblsubheader2,*lbladdtomap,*lblshareto;
    
    UIImageView *img_show_photo;
    UIView *view_bg_show_photo;
    UITextView *txt_desc;
    
    UIButton *btn_location;
    UILabel *lbl_optional;
    UIView *view_second_cell;
    
    UIButton *btnswitch;
    UIImageView *imgswitchon,*imgswitchoff;
    
    UIView *view_third_cell;
    UIImageView *img_bg_third_cell;
    
    Show_location_list *show_location_listViewObj;
    UIActivityIndicatorView *activity;
    
    UILabel *lblSpinnerBg;
    UITableView *tblSearch;
    NSMutableArray *searchArray;
}

@property(nonatomic,retain) UILabel *lblSpinnerBg;
@property(nonatomic,retain) IBOutlet DCRoundSwitch *switch_map;

@property(nonatomic,retain)IBOutlet UITextView *txt_desc;
@property(nonatomic,retain)IBOutlet UIImageView *img_show_photo;
@property(nonatomic,retain)IBOutlet UIView *view_bg_show_photo;
@property(nonatomic,retain)IBOutlet UILabel *lblheader,*lblsubheader1,*lblsubheader2,*lbladdtomap,*lblshareto;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_firstcell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_secondcell;
@property(nonatomic,retain)IBOutlet UIButton *btnswitch;
@property(nonatomic,retain)IBOutlet UIImageView *imgswitchon,*imgswitchoff;

@property(nonatomic,retain)UIImage *img_photo;
@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;

@property(nonatomic,retain)IBOutlet UIButton *btn_location;
@property(nonatomic,retain)IBOutlet UILabel *lbl_optional;
@property(nonatomic,retain)IBOutlet UIView *view_second_cell;

@property(nonatomic,retain)IBOutlet UIView *view_third_cell;
@property(nonatomic,retain)IBOutlet UIImageView *img_bg_third_cell;

@property(nonatomic,retain)Show_location_list *show_location_listViewObj;

@property (nonatomic, retain) AmazonS3Client *s3;
@property (nonatomic, retain) UIActivityIndicatorView *activity;

@property (nonatomic, retain) IBOutlet UITableView *tblSearch;
@property (nonatomic, retain) NSMutableArray *searchArray;
@property (nonatomic, strong) NSMutableArray *tagsArray;

-(IBAction)editBtnClick :(id)sender ;
-(IBAction)btnswitchClick:(id)sender;

@end
