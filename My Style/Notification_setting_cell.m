//
//  Notification_setting_cell.m
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Notification_setting_cell.h"

@implementation Notification_setting_cell
@synthesize lbl_title,img_checkmark,img_bg;

- (void)addShadowToCellInTableView:(UITableView *)tableView
                       atIndexPath:(NSIndexPath *)indexPath
{
    BOOL isFirstRow = !indexPath.row;
    BOOL isLastRow = (indexPath.row == [tableView numberOfRowsInSection:indexPath.section] - 1);
    
    // the shadow rect determines the area in which the shadow gets drawn
    CGRect shadowRect = CGRectInset(self.backgroundView.bounds, 0, -10);
    if(isFirstRow)
        shadowRect.origin.y += 10;
    else if(isLastRow)
        shadowRect.size.height -= 10;

    // the mask rect ensures that the shadow doesn't bleed into other table cells
    CGRect maskRect = CGRectInset(self.backgroundView.bounds, -20, 0);
    if(isFirstRow) {
        maskRect.origin.y -= 10;
        maskRect.size.height += 10;
    }
    else if(isLastRow)
        maskRect.size.height += 10;
    
    CALayer *layer = self.backgroundView.layer;
    layer.shadowColor = [UIColor whiteColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0.1);
    layer.shadowRadius = 4.0;
    layer.shadowOpacity = 0.4;
    layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:shadowRect cornerRadius:5].CGPath;
    layer.masksToBounds = NO;
    
    // and finally add the shadow mask
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRect:maskRect].CGPath;
    layer.mask = maskLayer;
    self.img_bg.layer.masksToBounds = YES;
    self.img_bg.layer.cornerRadius=4.0f;
}
@end
