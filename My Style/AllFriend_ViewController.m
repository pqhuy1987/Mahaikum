//
//  Notification_setting_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 5/23/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "AllFriend_ViewController.h"
#import "Faceabook_find_friend_cell.h"
#import "Profile_share.h"
#import "AJNotificationView.h"

@interface AllFriend_ViewController ()

@end

@implementation AllFriend_ViewController

@synthesize tbl_notification,array_reqeust,dict_notification;
@synthesize tbl_footer,lblheader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lblheader.font = [UIFont fontWithName:@"Roboto" size:13.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    self.array_reqeust =[[NSMutableArray alloc]init];
    self.dict_notification =[[NSMutableDictionary alloc]init];
    
    self.tbl_notification.tableFooterView=self.tbl_footer;
    
    // Do any additional setup after loading the view from its nib.
}
//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
//    if (is_iPhone_5) {
//        self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
//        self.tbl_notification.frame = CGRectMake(0,71, 320, 445);
//    }else{
//        self.tbl_notification.frame = CGRectMake(0,71, 320, 360);
//    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self get_notification_info];
}

-(void)get_notification_info{
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14201" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAllFriendResponce:) name:@"14201" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14201" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FailgetAllFriendResponce:) name:@"-14201" object:nil];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&user_id=%@",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@get_all_friend.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"14201":nil];
}

-(void)getAllFriendResponce :(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14201" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14201" object:nil];
    
    NSDictionary* dict = [notification userInfo];
    NSError *err = nil;
    AFHTTPRequestOperation *response=[dict objectForKey:@"index"];
    NSDictionary *result = (NSDictionary *) [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:&err];
    
    [array_reqeust removeAllObjects];
    
    if ([[result valueForKey:@"success"] isEqualToString:@"1"]) {
        NSLog(@"%@",NSStringFromClass([[result valueForKey:@"data"] class]));
        NSMutableArray *dict =(NSMutableArray *)[result valueForKey:@"data"];
        
        for (int i = 0; i< [dict count]; i++) {
            NSDictionary *temp = (NSDictionary *)[dict objectAtIndex:i];
            Profile_share *tempShare = [[Profile_share alloc] init];
            tempShare.username = [temp objectForKey:@"username"];
            tempShare.user_id = [temp objectForKey:@"id"];
            tempShare.name = [temp objectForKey:@"name"];
            tempShare.image = [StaticClass urlDecode:[temp objectForKey:@"image"]];
            
            [array_reqeust addObject:tempShare];
        }
    }
    [self.tbl_notification reloadData];
}

-(void)FailgetAllFriendResponce:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14201" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14201" object:nil];
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Network Failure!" message:@"Check your internet connection and try again later!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array_reqeust.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Faceabook_find_friend_cell";
	Faceabook_find_friend_cell *cell = (Faceabook_find_friend_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell == nil)	{
		NSArray* nib = [[NSBundle mainBundle] loadNibNamed:@"Faceabook_find_friend_cell" owner:self options:nil];
		cell = [nib objectAtIndex:0];
		cell.showsReorderControl = NO;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
	}
    cell.btn_follow.hidden = YES;
//    
    Profile_share *tempShare = [array_reqeust objectAtIndex:indexPath.row];
    cell.lbl_username.textColor = [UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    cell.lbl_username.text = tempShare.username ;
    cell.lbl_name.hidden = YES;
    
    [cell.img_user sd_setImageWithURL:[NSURL URLWithString:tempShare.image] placeholderImage:nil];
    //cell.img_user.imageURL = [NSURL URLWithString:tempShare.image];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
