//
//  Favourite_news_following_share.h
//  My Style
//
//  Created by Tis Macmini on 6/17/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Favourite_news_following_share : NSObject{
/*
    feed = "started+following";
    "following_name" = demo2;
    id = 437;
    image = "";
    name = test6;
    "user_action" = follow;
    "user_id" = 30;
    username = test6;
 */
    NSString *following_name;
    NSString *username;
    NSString *user_action;
    NSString *user_img_url;
    NSString *status_date;
    NSString *feed;
    NSString *img_url;
    NSString *img_count;
    NSMutableArray *array_img;
    NSString *image_id;
}
@property(nonatomic,strong)  NSString *image_id;
@property(nonatomic,strong)  NSMutableArray *array_img;
@property(nonatomic,strong) NSString *following_name;
@property(nonatomic,strong) NSString *username;
@property(nonatomic,strong) NSString *user_action;
@property(nonatomic,strong) NSString *user_img_url;
@property(nonatomic,strong) NSString *status_date;
@property(nonatomic,strong) NSString *feed;
@property(nonatomic,strong) NSString *img_url;
@property(nonatomic,strong) NSString *img_count;
@end
