//
//  Rest_password_throw_fb.h
//  My Style
//
//  Created by Tis Macmini on 6/5/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AJNotificationView.h"
#import "Singleton.h"
#import "StaticClass.h"

@interface Rest_password_throw_fb : UIViewController{

    UITextField *txt_password;
    UITextField *txt_password_again;
    
    NSString *str_email;
    NSString *str_username;
    NSString *str_image_url;
    UIImageView *img_cell_bg;
    
    UILabel *lbl_username;
    UILabel *lbl_dec;
    UIImageView *img_image;
}

@property(nonatomic,strong)IBOutlet UIImageView *img_image;

@property(nonatomic,strong)IBOutlet UIImageView *img_cell_bg;
@property(nonatomic,strong)IBOutlet UITextField *txt_password;
@property(nonatomic,strong)IBOutlet UITextField *txt_password_again;
@property(nonatomic,strong) NSString *str_username;
@property(nonatomic,strong) NSString *str_email;
@property(nonatomic,strong) NSString *str_image_url;

@property(nonatomic,strong)IBOutlet UILabel *lbl_username;
@property(nonatomic,strong)IBOutlet UILabel *lbl_dec;

@end
