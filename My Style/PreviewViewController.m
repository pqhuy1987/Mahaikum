//
//  PreviewViewController.m
//  STPopupPreviewExample
//
//  Created by Kevin Lin on 22/5/16.
//  Copyright © 2016 Sth4Me. All rights reserved.
//

#import "PreviewViewController.h"
#import "ImageLoader.h"
#import <STPopup/STPopup.h>

@interface PreviewViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *headerViewHeightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *footerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;

@end

@implementation PreviewViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width-5, self.view.frame.size.height-150);
        self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Resize content size based on image dimensions
    CGFloat imageWidth = self.view.frame.size.width;
    CGFloat imageHeight = self.view.frame.size.height - 50;
    CGFloat contentWidth = [UIScreen mainScreen].bounds.size.width - 20;
    CGFloat contentHeight = contentWidth * imageHeight / imageWidth + _headerViewHeightConstraint.constant + _footerViewHeightConstraint.constant;
    self.contentSizeInPopup = CGSizeMake(contentWidth, contentHeight);
    
    self.avatarImageView.layer.cornerRadius = 16;
    NSURL *avatarImageURL = [NSURL URLWithString:self.data.user_image];
    if (!avatarImageURL) {
        self.avatarImageView.image = [UIImage imageNamed:@"default_user_image"];
    } else if ([ImageLoader cachedImageForURL:avatarImageURL]) {
        self.avatarImageView.image = [ImageLoader cachedImageForURL:avatarImageURL];
    } else {
        [ImageLoader loadImageForURL:avatarImageURL completion:^(UIImage *image) {
            self.avatarImageView.image = image;
        }];
    }

    self.nameLabel.text = self.data.username;
    self.captionLabel.text = self.data.description;
    self.imageView.image = self.showImage;
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@", [StaticClass urlDecode:self.data.currency], self.data.price];
    self.categoryLabel.text = self.data.location;
    if (self.data.sold) {
        UIImageView *soldImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"SoldImage.png"]];
        soldImg.center = CGPointMake(self.imageView.frame.size.width  / 2,
                                         self.imageView.frame.size.height / 2);
        [self.imageView addSubview:soldImg];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.popupController.navigationBarHidden = YES;
}

@end
