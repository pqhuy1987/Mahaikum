//
//  Image_detail_url.h
//  My Style
//
//  Created by Tis Macmini on 6/19/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "Comment_share.h"
#import "Home_tableview_cell.h"
#import "DLStarRatingControl.h"
#import <MessageUI/MFMailComposeViewController.h>

#import "BlockActionSheet.h"
#import "Likers_list_ViewController.h"
#import "Comments_list_ViewController.h"
#import "Share_photo.h"

@interface Image_detail_url : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate>{
    NSString *image_id;
    Home_tableview_data_share *shareObj;
    NSInteger which_image_liked;
    NSInteger which_image_delete;
    UIImageView *img_like_heart;
    
    Likers_list_ViewController *liker_list_viewObj;
    Comments_list_ViewController *comments_list_viewObj;
    Share_photo *share_photo_view;
    
    UITableView *tbl_news;
    
    UIView *imgfullscreenviewobj;
    
    NSMutableArray *arrayFeedArray;
    NSInteger currentSelectedIndex;
}
@property(nonatomic,strong) NSString *image_id;
@property(nonatomic,strong)IBOutlet  UITableView *tbl_news;
@property(nonatomic,strong) Likers_list_ViewController *liker_list_viewObj;
@property(nonatomic,strong) Comments_list_ViewController *comments_list_viewObj;
@property(nonatomic,strong) Share_photo *share_photo_view;

@property(nonatomic,strong) Home_tableview_data_share *shareObj;
@property(nonatomic,strong) Share_photo_ViewController *share_photo_viewObj;
@property(nonatomic,strong)IBOutlet UIImageView *img_like_heart;

@property(nonatomic,strong)IBOutlet UIView *imgfullscreenviewobj;
@property(nonatomic,strong)NSMutableArray *arrayFeedArray;
@property(nonatomic,assign)NSInteger currentSelectedIndex;
@property (nonatomic, assign) BOOL promotion;

@end
