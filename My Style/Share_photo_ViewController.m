//
//  Share_photo_ViewController.m
//  My Style
//
//  Created by Tis Macmini on 4/26/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import "Share_photo_ViewController.h"
//#import "AFPhotoEditorControllerOptions.h"
//#import "AFPhotoEditorController.h"
#import "UIImage+Resize.h"
#import "NSString+AVTagAdditions.h"

#import "DBCameraViewController.h"
#import "DBCameraContainerViewController.h"
#import "DBCameraLibraryViewController.h"
#import "DBCameraGridView.h"
#import "ALTabBarController.h"
#import "CategoriesViewController.h"
#import "ActionSheetStringPicker.h"

NSInteger btnswitchsharephotoflg = 0;

@interface Share_photo_ViewController () <DBCameraViewControllerDelegate, CategoriesViewControllerDelegate, CZPickerViewDataSource, CZPickerViewDelegate>
{
    CategoriesViewController *category_view;
    CategoryObject *selectedCategory;
    BOOL selectedCurrency;
    NSString *selectedCategoryId;
}

@end

#define CURRENCYS   @[@" ", @"$ ", @"£ ", @"€ ", @"KWD ", @"AED ", @"BHD ", @"OMR ", @"QAR ", @"SAR ", @"SALE ", @"Call for price"]

@implementation Share_photo_ViewController
@synthesize img_photo,scrollview,img_bg_firstcell;
@synthesize img_show_photo,view_bg_show_photo,txt_desc,img_bg_secondcell,btnswitch,imgswitchon,imgswitchoff,lblSpinnerBg;

@synthesize btn_location,lbl_optional,view_second_cell;
@synthesize view_third_cell,img_bg_third_cell;
@synthesize switch_map,lbladdtomap,lblheader,lblshareto,lblsubheader1,lblsubheader2,activity;
@synthesize btn_category;

@synthesize tblSearch,searchArray,tagsArray;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)btn_back_click:(id)sender {
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
    if (!self.editing)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)selectCurrenyButtonClicked:(id)sender
{
    selectedCurrency = NO;
    
    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Select a unit"
                                                   cancelButtonTitle:nil
                                                  confirmButtonTitle:nil];
    picker.delegate = self;
    picker.dataSource = self;
    NSInteger selectedIndex = [CURRENCYS indexOfObject:self.selectCurrencyButton.titleLabel.text];
    if (selectedIndex != NSNotFound)
    {
        selectedCurrency = YES;
        [picker setSelectedRows:[NSArray arrayWithObject:@(selectedIndex)]];
    }
    [picker show];
}

/* number of items for picker */
- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView
{
    return CURRENCYS.count;
}

/* picker item title for each row */
- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row
{
    return CURRENCYS[row];
}

/** delegate method for picking one item */
- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row
{
    [self.selectCurrencyButton setTitle:CURRENCYS[row] forState:UIControlStateNormal];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    selectedCategory = nil;
    selectedCategoryId = @"1";

    searchArray=[[NSMutableArray alloc] init];
    tagsArray=[[NSMutableArray alloc] init];
    txt_desc.hashTagsDelegate = self;
    txt_desc.hashTagsTableViewHeight = 120.0f;
    
    
    self.s3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    self.s3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
    lblheader.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    lblsubheader1.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lblsubheader2.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    lbladdtomap.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    lblshareto.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    btn_location.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    btn_category.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    txt_desc.font = [UIFont fontWithName:@"Helvetica" size:16.0];
    
    self.view.backgroundColor =[UIColor whiteColor];
    category_view = [[CategoriesViewController alloc] initWithNibName:@"CategoriesViewController" bundle:nil];
    
    self.img_bg_firstcell.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellbg.png"]];
    self.img_bg_firstcell.layer.cornerRadius =4.0f;
    
    [switch_map setOn:YES];
    switch_map.onTintColor =[UIColor colorWithRed:12.0f/255.0f green:53.0f/255.0f blue:85.0f/255.0f alpha:1];
    
    if (self.editing)
    {
        [self.img_show_photo sd_setImageWithURL:[NSURL URLWithString:self.shareObjs.image_path]];
        self.itemPriceEdit.text = self.shareObjs.price;
        [self.selectCurrencyButton setTitle:[StaticClass urlDecode:self.shareObjs.currency] forState:UIControlStateNormal];
        self.txt_desc.text = self.shareObjs.description;
        self.markAsSoldView.hidden = NO;
        [self.soldSwitch setOn:self.shareObjs.sold animated:YES];
        [self.btn_category setTitle:self.shareObjs.location forState:UIControlStateNormal];
        selectedCategoryId = self.shareObjs.category_id;
        
        if (self.selectCurrencyButton.titleLabel.text.length == 0)
            [self.selectCurrencyButton setTitle:@"Select currency" forState:UIControlStateNormal];
    }
    else
        self.markAsSoldView.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    self.img_show_photo.layer.cornerRadius = 42.0;
    self.img_show_photo.layer.masksToBounds=YES;
    
    if (!self.editing)
        self.img_show_photo.image=self.img_photo;
    
//    if (is_iPhone_5) {
//        self.scrollview.contentSize=CGSizeMake(kViewWidth,500);
//    }
//    else{
//        self.scrollview.contentSize=CGSizeMake(kViewWidth,500);
//        self.view.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        self.scrollview.frame = CGRectMake(0,71, kViewWidth, 498);
//    }
    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }
    else {
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
    
    
    NSMutableArray *tempFollowing=[[Singleton sharedSingleton] getCurrentDictFollowing];
    
    [tagsArray removeAllObjects];
    for (int i=0;i<[tempFollowing count];i++) {
        NSString *strName=[StaticClass urlDecode:[tempFollowing objectAtIndex:i]];
        [tagsArray addObject:strName];
    }
}

- (NSArray *)tagsForQuery:(NSString *)query{
    NSPredicate *bPredicate =
    [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@", query];
    NSArray *array =[tagsArray filteredArrayUsingPredicate:bPredicate];
    
    [searchArray removeAllObjects];
    for ( int i=0;i<[array count];i++) {
        [searchArray addObject:[array objectAtIndex:i]];
    }
    
    if ([searchArray count]==0) {
        tblSearch.hidden=YES;
    }
    else {
        tblSearch.hidden=NO;
    }
    
    [tblSearch reloadData];
    return array;
}

- (BOOL)textView:(UITextView *)textViewsss shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [txt_desc resignFirstResponder];
        //list all the contained tags:
        NSLog(@"Entered tags: %@", [txt_desc.hashTags componentsJoinedByString:@" "]);
    }
    return YES;
}

#pragma mark UITableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 41.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIImageView *strip = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    strip.frame =CGRectMake(0,0,kViewWidth,41);
        
    UILabel *lbl=[[UILabel alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
    lbl.textColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    lbl.backgroundColor=[UIColor clearColor];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.text=@"Suggestion";
        
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,kViewWidth,41)];
    [view setBackgroundColor:[UIColor clearColor]];
    [view addSubview:strip];
    [view addSubview:lbl];
    return view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
        
    NSString *tag = [searchArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"@%@", tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:16.0f];
    cell.backgroundColor=[UIColor whiteColor];
    cell.textLabel.textColor=[UIColor colorWithRed:77.0f/255.0f green:15.0f/255.0f blue:26.0f/255.0f alpha:1];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView==tblSearch) {
        NSString *str=[searchArray objectAtIndex:indexPath.row];
        
        NSString *replaceString = [NSString stringWithFormat:@"@%@ ", str];
        NSMutableString *mutableText = [NSMutableString stringWithString:txt_desc.text];
        
        [[NSString endOfStringHashtagRegex] replaceMatchesInString:mutableText options:0 range:[txt_desc.text wholeStringRange] withTemplate:replaceString];
        txt_desc.text = mutableText;
        tblSearch.hidden=YES;
    }
}

// category view controller delegate
- (void)dismissButtonClicked:(CategoriesViewController *)viewController
{
    viewController.delegate = nil;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)categorySelected:(CategoriesViewController *)viewController category:(CategoryObject *)category
{
    selectedCategory = category;
    selectedCategoryId = category.categoryId;
    [self.btn_category setTitle:category.categoryName forState:UIControlStateNormal];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)btn_category_click:(id)sender
{
    category_view.user_id = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID];
    category_view.editable = YES;
    category_view.postable = YES;
    category_view.delegate = self;
    category_view.nav_title = @"Categories";
    [self presentViewController:category_view animated:NO completion:nil];
}

-(IBAction)editBtnClick :(id)sender {
//    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:self.img_photo];
//    [editorController setDelegate:self];
//    [self presentViewController:editorController animated:YES completion:nil];
    
    
    DBCameraContainerViewController *cameraContainer = [[DBCameraContainerViewController alloc] initWithDelegate:self];
    [cameraContainer setFullScreenMode];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraContainer];
    [nav setNavigationBarHidden:YES];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
    
}

- (void) dismissCamera:(id)cameraViewController{
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
    [cameraViewController restoreFullScreenMode];
}

- (void) camera:(id)cameraViewController didFinishWithImage:(UIImage *)image withMetadata:(NSDictionary *)metadata {
    [[Singleton sharedSingleton] setCurrentCapturedImage:image];
    self.img_photo=image;
    self.img_show_photo.image=image;
    [self.img_show_photo setImage:self.img_photo];
    
    [cameraViewController restoreFullScreenMode];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//-(void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image {
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                           self.img_photo = image;
//                           self.img_show_photo.image=self.img_photo;
//                           
//                       });
//    }];
//    
//}
//
//-(void)photoEditorCanceled:(AFPhotoEditorController *)editor {
//    [editor dismissViewControllerAnimated:YES completion:^{
//        dispatch_async( dispatch_get_main_queue(),
//                       ^{
//                       });
//    }];
//}


#pragma mark - ToggleViewDelegate
-(IBAction)btnswitchClick:(id)sender{
    if (btnswitchsharephotoflg == 0) {
        
        btnswitchsharephotoflg = 1;
        imgswitchoff.hidden = YES;
        imgswitchon.hidden = NO;
        
        [self performSelector:@selector(selectRightButton) withObject:self afterDelay:0.1];
    }
    else{
        btnswitchsharephotoflg = 0;
        imgswitchoff.hidden = NO;
        imgswitchon.hidden = YES;
        
        [self performSelector:@selector(selectLeftButton) withObject:self afterDelay:0.1];
    }
}

-(IBAction)btn_switch_map_click:(id)sender {
    if (!switch_map.isOn) {
        [self performSelector:@selector(selectLeftButton) withObject:self afterDelay:0.1];
    }else{
        [self performSelector:@selector(selectRightButton) withObject:self afterDelay:0.1];
    }
}

-(void)selectLeftButton {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(5,125+25,310,52);
    self.view_second_cell.frame = CGRectMake(5,177+25,310,0);
    self.view_third_cell.frame =CGRectMake(5, 235-52+25, 310, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=YES;
    
    [[Singleton sharedSingleton]setLocationname:@""];
    [[Singleton sharedSingleton]setLocation_lat:@""];
    [[Singleton sharedSingleton]setLocation_lng:@""];
    
}

-(void)selectRightButton {
    self.view_second_cell.layer.masksToBounds = YES;
    
    self.view_second_cell.frame = CGRectMake(5,177+25,310,0);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    self.img_bg_secondcell.frame = CGRectMake(5,125+25,310,104);
    self.view_second_cell.frame = CGRectMake(5,177+25,310,51);
    self.view_third_cell.frame =CGRectMake(5, 235+25, 310, 252);
    [UIView commitAnimations];
    
    self.view_second_cell.hidden=NO;
    
    if ([[Singleton sharedSingleton]get_location_name].length ==0) {
        [self.btn_location setTitle:@"Name this location" forState:UIControlStateNormal];
        self.lbl_optional.hidden =NO;
    }else{
        [self.btn_location setTitle:[[Singleton sharedSingleton]get_location_name] forState:UIControlStateNormal];
        self.lbl_optional.hidden =YES;
    }
    
    
}

#pragma mark UIScrollview Delegate 
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
   [self.txt_desc resignFirstResponder];
}

#pragma mark UITextview Delegate

//-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    return TRUE;
//}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."]) {
        self.txt_desc.text=@"";
    }
    
    return TRUE;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if (self.txt_desc.text.length==0) {
        self.txt_desc.text=@"Write a caption...";
    }
}

-(IBAction)btn_share_click:(id)sender {
    if (!self.editing)
        [self postImage];
    else
        [self postEditImage];
}

- (void)postEditImage
{
//    if (self.itemPriceEdit.text.length == 0)
//    {
//        [ProgressHUD showError:@"Please input item price" Interaction:NO];
//        return;
//    }
    if (self.itemPriceEdit.text.length!=0&&[self.selectCurrencyButton.titleLabel.text isEqualToString:@"Select currency"])
    {
        [ProgressHUD showError:@"Please select a currency" Interaction:NO];
        return;
    }
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."] || self.txt_desc.text.length == 0)
    {
        [ProgressHUD showError:@"Please input item description" Interaction:NO];
        return;
    }
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"14211" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editImageAPIResponce:) name:@"14211" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-14211" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(FaildiseditImageAPIResponce:) name:@"-14211" object:nil];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@post_edit_images.php",[[Singleton sharedSingleton] getBaseURL]];
    NSLog(@"requestStr:%@",requestStr);
    
    self.shareObjs.price = self.itemPriceEdit.text;
    self.shareObjs.currency = self.selectCurrencyButton.titleLabel.text;
    self.shareObjs.description = self.txt_desc.text;
    self.shareObjs.sold = self.soldSwitch.on ? 1 : 0;
    self.shareObjs.location = self.btn_category.titleLabel.text;
    self.shareObjs.category_id = selectedCategoryId;
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            sig, @"sign",
                            salt, @"salt",
                            self.itemPriceEdit.text, @"price",
                            self.selectCurrencyButton.titleLabel.text, @"currency",
                            self.txt_desc.text, @"description",
                            selectedCategoryId, @"category_id",
                            @(self.soldSwitch.on ? 1 : 0), @"sold",
                            self.shareObjs.image_id, @"imageid",
                            [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                            nil];
    NSLog(@"params:%@",params);
    
    [SVProgressHUD show];
    [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
        [SVProgressHUD dismiss];
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(shareObjsChanged:shareObjs:)])
            {
                [self.delegate shareObjsChanged:self shareObjs:self.shareObjs];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } failure:^(NSString *errorString) {
        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
        [SVProgressHUD dismiss];
    }];
}

#pragma mark Post Image To server
- (void)postImage
{
//    if (self.itemPriceEdit.text.length == 0)
//    {
//        [ProgressHUD showError:@"Please input item price" Interaction:NO];
//        return;
//    }
    if (self.itemPriceEdit.text.length!=0&&[self.selectCurrencyButton.titleLabel.text isEqualToString:@"Select currency"])
    {
        [ProgressHUD showError:@"Please select a currency" Interaction:NO];
        return;
    }
    if ([self.txt_desc.text isEqualToString:@"Write a caption..."] || self.txt_desc.text.length == 0)
    {
        [ProgressHUD showError:@"Please input item description" Interaction:NO];
        return;
    }
    
    NSLog(@"s3 Start Time:%@",[NSDate date]);
    CGSize size = CGSizeMake(PostImageWidth, PostImageWidth * self.img_photo.size.height / self.img_photo.size.width);
    UIImage *newImage = [GlobalDefine imageWithImage:self.img_photo scaledToSize:size];
    NSData *img_data = UIImageJPEGRepresentation(newImage, 1.0);
    
    [SVProgressHUD show];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSString *dateString1 = [GlobalDefine toDateTimeStringFromDateWithFormat:[NSDate date] formatString:@"yyyy-MM-dd hh:mm:ss"];
        dateString1=[dateString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *imageKeys=[NSString stringWithFormat:@"%@%@.jpeg",[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],dateString1];
        
        imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
        
        por.contentType = @"image/jpeg";
        por.data        = img_data;
        
        // Put the image data into the specified s3 bucket and object.
        S3PutObjectResponse *putObjectResponse = [self.s3 putObject:por];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(putObjectResponse.error != nil)
            {
                [SVProgressHUD dismiss];
                NSLog(@"Error: %@", putObjectResponse.error);
                //ERRORWHILEUPLOADINGANIMAGES
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:imageKeys message:putObjectResponse.error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            else {
                NSLog(@"s3 End Time:%@",[NSDate date]);
                NSLog(@"KEY:%@",imageKeys);
                //SuccessAlertMessage;
                
                NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
                NSString *key = SIGNSALTAPIKEY;
                NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
                NSString *sig = [StaticClass returnMD5Hash :tempStr];
                
                NSString *requestStr =[NSString stringWithFormat:@"%@post_images.php",[[Singleton sharedSingleton] getBaseURL]];
                NSLog(@"requestStr:%@",requestStr);
                
                NSString *strDesc=@"";
                if ([self.txt_desc.text isEqualToString:@"Write a caption..."])
                {
                    strDesc=@"";
                }
                else
                {
                    strDesc=self.txt_desc.text;
                }
                
                NSString *strLocation=@"";
                NSString *strLatitude=@"";
                NSString *strLongitude=@"";
                
                if (!self.view_second_cell.hidden) {
                    if ([[Singleton sharedSingleton]get_location_name].length !=0) {
                        NSLog(@"Share");
                        strLocation=[[Singleton sharedSingleton]get_location_name];
                        strLatitude=[[Singleton sharedSingleton]get_location_lat];
                        strLongitude=[[Singleton sharedSingleton]get_location_lng];
                    }
                }
                
                if ([[[NSUserDefaults standardUserDefaults]objectForKey:PHOTOS_SAVE_LIBRARY] isEqualToString:@"1"]) {
                    
                }
                
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                        sig, @"sign",
                                        salt, @"salt",
                                        [[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],@"uid",
                                        strDesc,@"description",
                                        imageKeys,@"image",
                                        strLocation,@"location",
                                        strLatitude,@"lat",
                                        strLongitude,@"lng",
                                        self.itemPriceEdit.text,@"price",
                                        self.selectCurrencyButton.titleLabel.text,@"currency",
                                        nil];
                
                NSLog(@"params:%@",params);
                
                if (selectedCategory != nil)
                {
                    NSMutableDictionary *tempDic = [params mutableCopy];
                    tempDic[@"category_id"] = selectedCategory.categoryId;
                    params = tempDic;
                }
                
                [[AFNetworkingQueue sharedSingleton] postRequest:params requestUrl:requestStr success:^(id responseObject) {
                    if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
                        NSLog(@"Product Image Successfully Uploaded");
                        
                        [SVProgressHUD dismiss];
                        
                        ALTabBarController *altab = (ALTabBarController *)self.tabBarController;
                        [altab tabWasSelected:0];
                        
                        self.txt_desc.text=@"Write a caption...";
                        [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Success!" subtitle:@"Photo upload successfully." hideAfter:2];
                        
                        [StaticClass saveToUserDefaults:@"1" :@"IMAGEUPLOADEDSUCCESSFULLY"];
                        [self dismissViewControllerAnimated:NO completion:^{
                        }];
                    }
                } failure:^(NSString *errorString) {
                    [SVProgressHUD dismiss];
                    [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeError title:@"Network Failure!" subtitle:@"Check your internet connection and try again later!" hideAfter:2];
                }];
            }
        });
    });
}

#pragma mark - Facebook Sharing
-(IBAction)btn_facebook_share_click:(id)sender{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Facebook Accounts" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark - Twitter Sharing
-(IBAction)btn_twitter_share_click:(id)sender{
    
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if (![self.txt_desc.text isEqualToString:@"Write a caption..."]) {
            [tweetSheet setInitialText:self.txt_desc.text];
        }
        [tweetSheet addImage:self.img_show_photo.image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Twitter Accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in setting" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark - Messaage Sharing
-(IBAction)btn_message_share_click:(id)sender{
    
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.persistent = YES;
    pasteboard.image = self.img_show_photo.image;
    
    NSString *phoneToCall = @"sms:";
    NSString *phoneToCallEncoded = [phoneToCall stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL *url = [[NSURL alloc] initWithString:phoneToCallEncoded];
    [[UIApplication sharedApplication] openURL:url];
    
    if([MFMessageComposeViewController canSendText]) {
        NSMutableString *emailBody = [[NSMutableString alloc] initWithString:@"Your Message Body"];
        picker.messageComposeDelegate = self;
        [picker setBody:emailBody];// your recipient number or self for testing
        picker.body = emailBody;
        NSLog(@"Picker -- %@",picker.body);
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
    /*    MessageComposeResultCancelled,
     MessageComposeResultSent,
     MessageComposeResultFailed*/
    switch (result) {
        case MessageComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MessageComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent failed!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MessageComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
    }
}

#pragma mark - Email Sharing
-(IBAction)btn_email_share_click:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        NSData *photoData=UIImagePNGRepresentation(self.img_show_photo.image);
        [controller addAttachmentData:photoData mimeType:@"image/png" fileName:[NSString stringWithFormat:@"photo.png"]];
        controller.mailComposeDelegate = self;
        if (controller)
            [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
            
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            break;
        }
            
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
