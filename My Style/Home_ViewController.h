
#import <UIKit/UIKit.h>
#import "Home_tableview_cell.h"
#import "BlockActionSheet.h"
#import "Likers_list_ViewController.h"
#import "StaticClass.h"
#import "Singleton.h"
#import "AJNotificationView.h"
#import "Home_tableview_data_share.h"
#import "EGOImageButton.h"
#import "EGOCache.h"
#import "Comment_share.h"
#import "Comments_list_ViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Share_photo.h"
#import "Suggested_friend_list.h"

@interface Home_ViewController : UIViewController<DLStarRatingDelegate,MFMailComposeViewControllerDelegate,UITabBarControllerDelegate>
{
    NSInteger which_image_liked;
    NSInteger which_image_delete;
}

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityINd;
@property (nonatomic, strong) IBOutlet UILabel *lblLoadingText;
@property (nonatomic, strong) IBOutlet UIView *view_footer_tbl;
@property (nonatomic, strong) IBOutlet  UIImageView *img_like_heart;
@property (nonatomic, strong) IBOutlet UITableView *tbl_news;
@property (nonatomic, strong) IBOutlet UIButton *btnSuggestedUser;

@property (nonatomic, strong) UIImageView *img_spinner_tbl;
@property (nonatomic, strong) Share_photo_ViewController *share_photo_viewObj;
@property (nonatomic, strong) Home_tableview_data_share *shareObjs;
@property (nonatomic, strong) NSMutableArray *arrayFeedArray;
@property (nonatomic) NSInteger currentSelectedIndex;
@property (nonatomic, strong) Share_photo *share_photo_view;
@property (nonatomic, strong) Comments_list_ViewController *comments_list_viewObj;
@property (nonatomic, strong) Suggested_friend_list *suggestedList;
@property (nonatomic, strong) Likers_list_ViewController *liker_list_viewObj;
@property (nonatomic, strong) NSMutableArray *array_feeds;
@property (nonatomic, strong) UIButton *btn_reload;
@property (nonatomic, strong) UIActivityIndicatorView *activity;
@property (nonatomic, strong) NSArray *array_spinner;

@property (strong, nonatomic) IBOutlet UIView *bodyView;
@end
