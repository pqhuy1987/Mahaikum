//
//  search_view_cell.h
//  My Style
//
//  Created by Tis Macmini on 5/28/13.
//  Copyright (c) 2013 Tis Macmini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface search_view_cell : UITableViewCell{
    UIImageView *img_user;
    UILabel *lbl_name;
    UILabel *lbl_username;
}
@property(nonatomic,strong)IBOutlet UIImageView *img_user;
@property(nonatomic,strong)IBOutlet UILabel *lbl_name;
@property(nonatomic,strong)IBOutlet UILabel *lbl_username;

@end
