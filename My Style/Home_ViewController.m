
#import "Home_ViewController.h"
#import "User_info_ViewController.h"
#import "webview_viewcontroller.h"
#import "Hash_tag_ViewController.h"

#import "STXFeedViewController.h"

#import "STXFeedPhotoCell.h"
#import "STXLikesCell.h"
#import "STXCaptionCell.h"
#import "STXCommentCell.h"
#import "STXUserActionCell.h"
#import "STXFeedTableViewDataSource.h"
#import "STXFeedTableViewDelegate.h"

#import "LMPullToBounceWrapper.h"
#import "NSTimer+PullToBounce.h"
#import "UIView+PullToBounce.h"

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

@interface Home_ViewController () <STXFeedPhotoCellDelegate, STXLikesCellDelegate, STXCaptionCellDelegate, STXCommentCellDelegate, STXUserActionDelegate, Share_photo_ViewControllerDelegate, STXSectionHeaderViewDelegate>
{
    User_info_ViewController *user_info_view;
    webview_viewcontroller *web_viewObj;
    Hash_tag_ViewController *hash_tag_viewObj;
    AmazonS3Client *tempS3;
    BOOL isRefreshing;
}

@property (strong, nonatomic) STXFeedTableViewDataSource *tableViewDataSource;
@property (strong, nonatomic) STXFeedTableViewDelegate *tableViewDelegate;

@property (nonatomic, strong) LMPullToBounceWrapper *pushToBounceWrapper;
@end

@implementation Home_ViewController

NSInteger cellClickIndex = 0;
NSInteger tblRefresOffset = 0;

NSInteger afterCommentRefresh = -1;

static NSInteger pushPopFlg=0;
static NSInteger queueKillFlg=0;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)refresh_home_view_data:(NSNotification *)notification {
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self.tbl_news setContentOffset:CGPointZero animated:YES];
    //[self get_news_feed:0];
}

// temp code
- (void)tempFunc
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        tempS3 = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
        tempS3.endpoint = [AmazonEndpoints s3Endpoint:US_WEST_2];
        @try {
            
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            S3ListObjectsRequest *req = [[S3ListObjectsRequest alloc] initWithName:@"tstbuckets"];
            
            S3ListObjectsResponse *resp = [tempS3 listObjects:req];
            
            NSMutableArray* objectSummaries = resp.listObjectsResult.objectSummaries;
            
            for (NSUInteger x = [objectSummaries count] - 1; x > 0 ; x--) {
                
                NSString *imageName = ((S3ObjectSummary *)objectSummaries[x]).key;
                @try {
                    imageName = [imageName stringByReplacingOccurrencesOfString:@".jpeg" withString:@""];
                }
                @catch (NSException *exception) {
                    continue;
                }
                @finally {
                    
                }
                
                BOOL flag = NO;
                if ([GlobalDefine toDateFromDateTimeStringWithFormat:[imageName substringFromIndex:1] formatString:@"yyyy-MM-ddhh:mm:ss"])
                {
                    flag = YES;
                }
                if ([GlobalDefine toDateFromDateTimeStringWithFormat:[imageName substringFromIndex:2] formatString:@"yyyy-MM-ddhh:mm:ss"])
                {
                    flag = YES;
                }
                
                if (flag)
                {
                    NSString *imageUrl = [NSString stringWithFormat:@"https://s3-us-west-2.amazonaws.com/tstbuckets/%@.jpeg", imageName];
                    UIImageView *imageView = [[UIImageView alloc] init];
                    __weak NSString *weakImageName = imageName;
                    
                    [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (!error)
                        {
                            if (image.size.width > PostImageWidth)
                            {
                                CGSize size = CGSizeMake(PostImageWidth, PostImageWidth * image.size.height / image.size.width);
                                UIImage *newImage = [GlobalDefine imageWithImage:image scaledToSize:size];
                                NSData *img_data = UIImagePNGRepresentation(newImage);
                                
                                __weak NSString *imageKeys=[NSString stringWithFormat:@"%@.jpeg",weakImageName];
                                
                                imageKeys = [imageKeys stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
                                S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:imageKeys inBucket:[[Singleton sharedSingleton] getBucketName]];
                                
                                por.contentType = @"image/jpeg";
                                por.data        = img_data;
                                
                                // Put the image data into the specified s3 bucket and object.
                                S3PutObjectResponse *putObjectResponse = [tempS3 putObject:por];
                                
                                if(putObjectResponse.error == nil)
                                {
                                    NSLog(@"////////////////////////////////////Successfully updated: %@", imageUrl);
                                }
                            }
                        }
                        dispatch_semaphore_signal(semaphore);
                    }];
                    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Cannot list S3 %@",exception);
        }
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tbl_news.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.suggestedList=[[Suggested_friend_list alloc] initWithNibName:@"Suggested_friend_list" bundle:nil];
    
    STXFeedTableViewDataSource *dataSource = [[STXFeedTableViewDataSource alloc] initWithController:self tableView:self.tbl_news];
    self.tbl_news.dataSource = dataSource;
    self.tableViewDataSource = dataSource;
    
    STXFeedTableViewDelegate *delegate = [[STXFeedTableViewDelegate alloc] initWithController:self];
    self.tbl_news.delegate = delegate;
    self.tableViewDelegate = delegate;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refresh_home_view_data" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh_home_view_data:) name:@"refresh_home_view_data" object:nil];
    
    self.array_spinner = @[[UIImage imageNamed:@"feedstate-spinner-frame01.png"],[UIImage imageNamed:@"feedstate-spinner-frame02.png"],[UIImage imageNamed:@"feedstate-spinner-frame03.png"],[UIImage imageNamed:@"feedstate-spinner-frame04.png"],[UIImage imageNamed:@"feedstate-spinner-frame05.png"],[UIImage imageNamed:@"feedstate-spinner-frame06.png"],[UIImage imageNamed:@"feedstate-spinner-frame07.png"],[UIImage imageNamed:@"feedstate-spinner-frame08.png"]];
    
    self.img_spinner_tbl.animationImages = self.array_spinner;
    self.img_spinner_tbl.animationRepeatCount = HUGE_VAL;
    self.img_spinner_tbl.animationDuration=1.0f;
    
    self.view.backgroundColor =[UIColor whiteColor];
    
    hash_tag_viewObj = [[Hash_tag_ViewController alloc]initWithNibName:@"Hash_tag_ViewController" bundle:nil];
    user_info_view = [[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    web_viewObj = [[webview_viewcontroller alloc]initWithNibName:@"webview_viewcontroller" bundle:nil];
    
    self.share_photo_view = [[Share_photo alloc]initWithNibName:@"Share_photo" bundle:nil];
    self.comments_list_viewObj = [[Comments_list_ViewController alloc]initWithNibName:@"Comments_list_ViewController" bundle:nil];
    self.array_feeds = [[NSMutableArray alloc]init];
    self.liker_list_viewObj = [[Likers_list_ViewController alloc]initWithNibName:@"Likers_list_ViewController" bundle:nil];
    
    UIView *tableview_header =[[UIView alloc]initWithFrame:CGRectMake(0, 20, kViewWidth, 44)];
    
    
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 44)];
    lbl.text = @"Mahalkum";
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    
//    _btn_reload = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_btn_reload setImage:[UIImage imageNamed:@"refreshbtn.png"] forState:UIControlStateNormal];
//    [_btn_reload addTarget:self action:@selector(btn_refresh_click:) forControlEvents:UIControlEventTouchUpInside];
//    [_btn_reload setFrame:CGRectMake(kViewWidth - 46.0, 10, 26, 26)];
    
    [tableview_header addSubview:lbl];
//    [tableview_header addSubview:_btn_reload];
    [self.view addSubview:tableview_header];
    self.tbl_news.tableFooterView = self.view_footer_tbl;
    
    [self.img_like_heart setHidden:YES];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"colarge_iPad.png"]];
    
//    CGRect bodyViewFrame = self.tbl_news.frame;
//    NSLog(@"----------bodyViewFrame=%f,%f,%f,%f", bodyViewFrame.origin.x, bodyViewFrame.origin.y, bodyViewFrame.size.width, bodyViewFrame.size.height);
//    UIView *bodyView = [[UIView alloc] initWithFrame:bodyViewFrame];
//    [self.view addSubview:bodyView];
    self.tbl_news.frame = CGRectMake(0, 0, self.tbl_news.frame.size.width, self.tbl_news.frame.size.height);
    self.pushToBounceWrapper = [[LMPullToBounceWrapper alloc] initWithScrollView:self.tbl_news];
    
        self.pushToBounceWrapper.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    [self.bodyView addSubview:self.pushToBounceWrapper];
    WS(weakSelf);
    [self.pushToBounceWrapper setDidPullTorefresh:^(){
        isRefreshing = YES;
        [weakSelf btn_refresh_click:nil];
        //        [NSTimer schedule:2.0 handler:^(CFRunLoopTimerRef timer) {
        //            [weakSelf.pushToBounceWrapper stopLoadingAnimation];
        //        }];
    }];

    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadCommentViewCell" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refeshReloadCommentViewCell:) name:@"ReloadCommentViewCell" object:nil];
    
    
}

- (void)refeshReloadCommentViewCell :(NSNotification *)notification
{
    NSDictionary* dict = [notification userInfo];
    NSDictionary *responseDict= [dict objectForKey:@"index"];
    
    NSMutableArray *responseArray=(NSMutableArray *)[responseDict objectForKey:@"CommentArray"];
    
    if ([self.array_feeds count]>0) {
        NSString *commentCount=[responseDict objectForKey:@"CommentCount"];
       
        Home_tableview_data_share *shareObj = [self.array_feeds objectAtIndex:cellClickIndex];
        
        NSMutableArray *tmpArray =[[NSMutableArray alloc]init];
        for (id item in responseArray)
        {
            Comment_share *obj = (Comment_share*)item;
            if ([shareObj.username isEqualToString:obj.username] && [shareObj.description isEqualToString:obj.comment_desc])
                continue;
            [tmpArray addObject:obj];
        }
        
        //shareObj.array_comments=responseArray;
        shareObj.array_comments=tmpArray;

        shareObj.comment_count=commentCount;
        
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
    }
}

//- (UIStatusBarStyle)preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBarHidden = YES;
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, kViewWidth, 22)];
    statusBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"colarge_iPad.png"]];
    [self.view addSubview:statusBarView];
    
    self.tbl_news.tag = 11001;
    
    if (afterCommentRefresh>-1) {
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:afterCommentRefresh] withRowAnimation:UITableViewRowAnimationNone];
        afterCommentRefresh=-1;
        return;
    }
    
    if (pushPopFlg==1) {
        pushPopFlg=0;
        return;
    }
    else if(pushPopFlg==2) {
        pushPopFlg=0;
        
        [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
        return;
    }
    
    if ([[StaticClass retrieveFromUserDefaults:@"USERNOWLOGOUT"] isEqualToString:@"1"]) {
        tblRefresOffset=0;
        [StaticClass saveToUserDefaults:@"0" :@"USERNOWLOGOUT"];
        [self.array_feeds removeAllObjects];
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        [self.tbl_news setContentOffset:CGPointMake(0,0)];
        [self.tbl_news reloadData];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:IS_HOME_PAGE_REFRESH] isEqualToString:@"1"]) {
        [StaticClass saveToUserDefaults:@"0" :IS_HOME_PAGE_REFRESH];
        tblRefresOffset = 0;
        [self get_news_feed:0];
    }
    else {
        [self get_news_feed:0];
    }
}

-(void)userDidLike:(STXUserActionCell *)userActionCell {
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:userActionCell.tag];
    which_image_liked = userActionCell.tag;

    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }

////////////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }
    else{
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

    
    if (![shareObj.liked isEqualToString:@"no"]) {
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likebtn.png"] forState:UIControlStateNormal];
    }
    else{
        [userActionCell.likeButton setImage:[UIImage imageNamed:@"likedbtn.png"] forState:UIControlStateNormal];
    }
}

-(void)userDidUnlike:(STXUserActionCell *)userActionCell {
    NSLog(@"UN LIKE CLICKED");
}

-(void)userWillComment:(STXUserActionCell *)userActionCell {
    cellClickIndex=userActionCell.tag;
    pushPopFlg=2;
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:userActionCell.tag];
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.objOwnPost=obj.image_owner;
    self.comments_list_viewObj.imageCaption_Tittle=obj.description;
    self.comments_list_viewObj.post_user_id=obj.uid;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    
    
    afterCommentRefresh=userActionCell.tag;
    [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
    
}

// share photo view controller delegate
- (void)shareObjsChanged:(Share_photo_ViewController *)viewController shareObjs:(Home_tableview_data_share *)shareObjz
{
    NSMutableArray *tempArrayFeedArray = [self.array_feeds mutableCopy];
    tempArrayFeedArray[which_image_delete] = shareObjz;
    self.array_feeds = tempArrayFeedArray;
}

- (void)userWillShare:(STXUserActionCell *)userActionCell
{
    NSInteger tag = userActionCell.tag;
    which_image_delete =tag;
    
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    
    if ([shareObj.image_owner isEqualToString:@"yes"])
    {
        BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
        
        [sheet setDestructiveButtonWithTitle:@"Delete" block:^{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Confirm Deletion" message:@"Delete this photo" delegate:self cancelButtonTitle:@"Don't delete" otherButtonTitles:@"Delete", nil];
            alert.tag=1;
            [alert show];
        }];
        [sheet addButtonWithTitle:@"Edit" block:^{
            self.share_photo_viewObj=[[Share_photo_ViewController alloc]initWithNibName:@"Share_photo_ViewController" bundle:nil];
            self.share_photo_viewObj.shareObjs = shareObj;
            self.share_photo_viewObj.editing = YES;
            self.share_photo_viewObj.delegate = self;
            [self.navigationController pushViewController:self.share_photo_viewObj animated:YES];
        }];
        [sheet addButtonWithTitle:@"Share Photo" block:^{
            pushPopFlg=1;
            self.share_photo_view.str_img_url = shareObj.image_path;
            [self.navigationController pushViewController:self.share_photo_view animated:YES];
            
        }];
        [sheet addButtonWithTitle:@"Copy Share URL" block:^{
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string =shareObj.image_path;
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
        [sheet addButtonWithTitle:@"Email Photo" block:^{
            if ([MFMailComposeViewController canSendMail]) {
                
                MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
                
                NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
                
                [controller setMessageBody:htmlStr isHTML:YES];
                controller.mailComposeDelegate = self;
                if (controller)
                    [self presentViewController:controller animated:YES completion:nil];
            }else {
                UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
        [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
        [sheet showInView:self.view];
        return;
    }
    
    BlockActionSheet *sheet = [BlockActionSheet sheetWithTitle:@"Photo Options"];
    
    [sheet setDestructiveButtonWithTitle:@"Report Inappropriate" block:^{
        
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"Report Inappropriate" message:@"" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"This post shouldn't be on this category",@"This post is spam or a scam",@"This post puts people at risk",@"This post shouldn't be on Mahalkum", nil];
        alert.tag =2;
        [alert show];
    }];
    [sheet addButtonWithTitle:@"Share Photo" block:^{
        pushPopFlg=1;
        self.share_photo_view.str_img_url = shareObj.image_path;
        [self.navigationController pushViewController:self.share_photo_view animated:YES];
        
    }];
    [sheet addButtonWithTitle:@"Copy Share URL" block:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string =shareObj.image_path;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Copied" message:@"This photo's sharable link has been copied to the clipboard." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        
    }];
    [sheet addButtonWithTitle:@"Email Photo" block:^{
        if ([MFMailComposeViewController canSendMail]) {
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            
            NSString *htmlStr=[NSString stringWithFormat:@"<html><body> <img border=\"0\" src=\"%@\" alt=\"Mahalkum\"></body></html>",shareObj.image_path];
            
            [controller setMessageBody:htmlStr isHTML:YES];
            controller.mailComposeDelegate = self;
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }else {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up a mail account in order to send email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
}

- (void)commentCellWillShowAllComments:(STXCommentCell *)commentCell
{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:commentCell.tag];
    self.comments_list_viewObj.image_id=obj.image_id;
    self.comments_list_viewObj.array_comments=obj.array_comments_tmp;
    afterCommentRefresh=commentCell.tag;
    [self presentViewController:self.comments_list_viewObj animated:YES completion:nil];
}

- (void)likesCellWillShowLikes:(STXLikesCell *)likesCell
{
    pushPopFlg=1;
    NSInteger tag = likesCell.tag;// ((UIButton *)sender).tag;
    which_image_liked=tag;
    Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:tag];
    self.liker_list_viewObj.image_id =shareObj.image_id;
    [self.navigationController pushViewController:self.liker_list_viewObj animated:YES];
}

- (void)commentCell:(STXCommentCell *)commentCell didSelectURL:(NSURL *)url
{
    NSLog(@"URL");
    pushPopFlg=1;
    NSString *str=[NSString stringWithFormat:@"%@",url];
    web_viewObj.web_url =str;
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)commentCell:(STXCommentCell *)commentCell didSelectHashtag:(NSString *)hashtag
{
    pushPopFlg=1;
    
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];
    
    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

-(void)commentCell:(STXCommentCell *)commentCell didSelectMention:(NSString *)mention {
    pushPopFlg=1;
    user_info_view.user_id=mention;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)captionCell:(STXCaptionCell *)commentCell didSelectHashtag:(NSString *)hashtag
{
    pushPopFlg=1;
    //hash_tag_viewObj.str_title = hashtag;
    hash_tag_viewObj.str_title = [NSString stringWithFormat:@"#%@", hashtag];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)captionCell:(STXCaptionCell *)commentCell didSelectMention:(NSString *)mention
{
    pushPopFlg=1;
    user_info_view.user_id=mention;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)likesCellDidSelectLiker:(NSString *)liker
{
    pushPopFlg=1;
    user_info_view.user_id=liker;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    
}

- (void)commentCell:(STXCommentCell *)commentCell willShowCommenter:(NSString *)commenter
{
    pushPopFlg=1;
    NSLog(@"TESTING - username:%@",commenter);
    user_info_view.user_id=commenter;
    [self.navigationController pushViewController:user_info_view animated:YES];    
}

- (void)btnProfileClicked:(id)sender
{
    pushPopFlg=1;
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:((UIButton *)sender).tag];
    user_info_view.user_id=obj.uid;
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)feedCellWillShowLikeUnlike:(STXFeedPhotoCell *)posterLike {
    NSLog(@"Tag:%ld",(long)posterLike.tag);
    [self.img_like_heart setHidden:NO];
    [self.img_like_heart setAlpha:0.0];
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.img_like_heart setAlpha:1.0];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.0 animations:^{
            
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.img_like_heart setAlpha:0.0];
            } completion:^(BOOL finished) {
                [self.img_like_heart setHidden:YES];
            }];
        }];
        
        
    }];
    
    Home_tableview_data_share *shareObj=[self.array_feeds objectAtIndex:posterLike.tag];
    which_image_liked=posterLike.tag;
    if (![shareObj.liked isEqualToString:@"no"]) {
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"unlike"];
    }else{
        [self btn_like_click:shareObj.uid imageid:shareObj.image_id action:@"like"];
    }
    
    ////////////////
    if ([shareObj.liked isEqualToString:@"no"]) {
        shareObj.liked=@"yes";
        int count =[shareObj.likes intValue];
        count++;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
    }
    else{
        shareObj.liked=@"no";
        int count =[shareObj.likes intValue];
        count--;
        shareObj.likes=[NSString stringWithFormat:@"%d",count];
        
        if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
            [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
        }
    }
    [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
    [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

#pragma mark Refresh
- (void)btn_refresh_click:(id)sender
{
    tblRefresOffset = 0;
    [self get_news_feed:0];
}

#pragma mark UITableview Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ( scrollView.tag != 11001 )
        return;
    
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height-150)
    {
        queueKillFlg=1;
        [[AFNetworkingQueue sharedSingleton] cancelAllQueueOperation];
        [self get_news_feed:1];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat sectionHeaderHeight = 50;
//    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
//        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
//    } else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
//        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
//    }
}

#pragma mark Get News Feed
- (void)get_news_feed :(NSInteger)fl
{
    if ( !isRefreshing )
    {
    if (fl == 0)
        [SVProgressHUD dismiss];
    }
    
    [self.activityINd startAnimating];
    
    self.lblLoadingText.text = @"Loading...";
    self.activityINd.hidden = NO;
    self.btnSuggestedUser.hidden = YES;
//    self.btn_reload.enabled = NO;
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&uid=%@&off=%ld&flag=1",salt,sig,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],(long)tblRefresOffset];
    
    NSString *requestStr = [NSString stringWithFormat:@"%@get_images.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URL:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];
        
        [SVProgressHUD dismiss];
        isRefreshing = NO;
        [self.activityINd stopAnimating];
        
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            [[Singleton sharedSingleton]setCurrent_time:[responseObject valueForKey:@"curr_utc"]];
            NSDictionary *array =[responseObject valueForKey:@"data"];
            
            if(tblRefresOffset == 0) {
                [self.array_feeds removeAllObjects];
            }
            for (id dict in array) {
                Home_tableview_data_share *shareObj = [[Home_tableview_data_share alloc]init];
                shareObj.datecreated = [StaticClass urlDecode:[dict valueForKey:@"datecreated"]];
                shareObj.description = [StaticClass urlDecode:[dict valueForKey:@"description"]];
                shareObj.image_id = [dict valueForKey:@"id"];
                shareObj.image_owner = [dict valueForKey:@"image_owner"];
                shareObj.image_path = [StaticClass urlDecode:[dict valueForKey:@"image_path"]];
                shareObj.lat = [dict valueForKey:@"lat"];
                shareObj.lng = [dict valueForKey:@"lng"];
                shareObj.likes = [dict valueForKey:@"likes"];
                shareObj.location = [StaticClass urlDecode:[dict valueForKey:@"category_name"]];
                shareObj.uid = [dict valueForKey:@"uid"];
                shareObj.username = [StaticClass urlDecode:[dict valueForKey:@"username"]];
                shareObj.user_image = [StaticClass urlDecode:[dict valueForKey:@"uimage"]];
                shareObj.category_id = [dict valueForKey:@"category_id"];
                shareObj.lat = [dict valueForKey:@"lat"];
                shareObj.lng = [dict valueForKey:@"lng"];
                shareObj.price = dict[@"price"];
                shareObj.currency = dict[@"currency"];
                shareObj.sold = [dict[@"sold"] integerValue];
                shareObj.liked = [dict valueForKey:@"user_liked"];
                shareObj.comment_count = [dict valueForKey:@"total_comment"];
                shareObj.array_liked_by = [[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"liked_by"]isKindOfClass:[NSArray class]])
                {
                    NSArray *tempArray =[dict valueForKey:@"liked_by"];
                    
                    for (NSDictionary *tempdict in tempArray)
                        [shareObj.array_liked_by addObject:[StaticClass urlDecode:[tempdict valueForKey:@"username"]]];
                }
                else
                {
                    
                }
                
                shareObj.array_comments=[[NSMutableArray alloc]init];
                shareObj.array_comments_tmp=[[NSMutableArray alloc]init];
                
                if ([[dict valueForKey:@"comments"]isKindOfClass:[NSArray class]])
                {
                    NSArray *tempArray =[dict valueForKey:@"comments"];
                    
                    for (NSDictionary *tempdict in tempArray)
                    {
                        Comment_share *obj =[[Comment_share alloc]init];
                        obj.comment_id =[tempdict valueForKey:@"id"];
                        obj.uid =[tempdict valueForKey:@"uid"];
                        obj.username =[StaticClass urlDecode:[tempdict valueForKey:@"username"]];
                        obj.name =[StaticClass urlDecode:[tempdict valueForKey:@"name"]];
                        obj.image_url=[StaticClass urlDecode:[tempdict valueForKey:@"image"]];
                        obj.comment_desc=[StaticClass urlDecode:[tempdict valueForKey:@"comment_desc"]];
                        obj.datecreated=[StaticClass urlDecode:[tempdict valueForKey:@"datecreated"]];
                        
                        if (![shareObj.username isEqualToString:obj.username] || ![shareObj.description isEqualToString:obj.comment_desc])
                            [shareObj.array_comments addObject:obj];
                        
                        [shareObj.array_comments_tmp addObject:obj];
                    }
                }
                else
                {
                    
                }
                
                shareObj.rating=[dict valueForKey:@"rating"];
                shareObj.my_rating=[dict valueForKey:@"my_rating"];
                shareObj.avgrating=[dict valueForKey:@"avrage_rating"];
                shareObj.totalUser=[dict valueForKey:@"tot_user"];
                
                if (shareObj.image_path.length !=0)
                    [self.array_feeds addObject:shareObj];
            }
        }
        else
        {
            self.lblLoadingText.text=@"No more ads.";
            self.activityINd.hidden=YES;
            self.btnSuggestedUser.hidden=YES;
        }
        
        if (self.array_feeds.count==0)
        {
            self.lblLoadingText.text=@"You can follow some of our suggested users and get updated with new Ads.";
            self.activityINd.hidden=YES;
            self.btnSuggestedUser.hidden=NO;
            self.img_spinner_tbl.frame=CGRectMake(125,20,70,70);
//            self.activityINd.frame=CGRectMake(151,43,20,20);
//            self.lblLoadingText.frame=CGRectMake(10,98,300,42);
        }
        else
        {
            self.btnSuggestedUser.hidden=YES;
            self.img_spinner_tbl.frame=CGRectMake(125,40,70,70);
//            self.activityINd.frame=CGRectMake(151,63,20,20);
//            self.lblLoadingText.frame=CGRectMake(10,122,300,21);
        }
        
        self.tableViewDataSource.posts =self.array_feeds;// copy];
        self.tableViewDelegate.posts =self.array_feeds;// copy];
        [self.tbl_news reloadData];
        tblRefresOffset =[[responseObject valueForKey:@"offset"] integerValue];
//        _btn_reload.enabled = YES;
    } failure:^(NSString *errorString) {
        [self.activityINd stopAnimating];
        self.activityINd.hidden = YES;
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];

        [SVProgressHUD dismiss];
        isRefreshing = NO;
//        _btn_reload.enabled = YES;
        
        if (queueKillFlg==1) {
            queueKillFlg=0;
            return;
        }
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

- (IBAction)btnSuggestedUserClick
{
    NSLog(@"Callsss");
    [self.navigationController pushViewController:self.suggestedList animated:YES];
}

#pragma mark UIAlertview Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1)
    {
        if (buttonIndex!=0)
            [self delete_photo];
    }
    else if (alertView.tag == 2)
    {
        if(buttonIndex==0)
            return;
        [self report_inappropriate:(int)buttonIndex];
    }
}

- (void)report_inappropriate:(int )index
{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&report_id=%d&image_id=%@&uid=%@",salt,sig,index,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_report_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"requestStr:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            [MKInfoPanel showPanelInView:self.view type:MKInfoPanelTypeInfo title:@"Thank you!" subtitle:@"Thank you for your report. We will remove this photo if it violates our Community Guidelines." hideAfter:2];
        }
    } failure:^(NSString *errorString) {
        
    }];
    
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"77703":nil];
}

#pragma mark Delete Photo
- (void)delete_photo
{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:which_image_delete];
    [self.array_feeds removeObjectAtIndex:which_image_delete];
    
    self.tableViewDataSource.posts = self.array_feeds;// copy];
    self.tableViewDelegate.posts = self.array_feeds;// copy];
    [self.tbl_news reloadData];
    
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@",salt,sig,obj.image_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_image.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URL:%@",requestStr);
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
            NSLog(@"Deleted successfully");
        }
    } failure:^(NSString *errorString) {
        [self.activityINd stopAnimating];
        self.activityINd.hidden = YES;
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];

        [SVProgressHUD dismiss];
        isRefreshing = NO;
//        _btn_reload.enabled = YES;
        
        if (queueKillFlg==1) {
            queueKillFlg=0;
            return;
        }
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark Star Rating

- (void)newRating:(DLStarRatingControl *)control :(NSUInteger)rating
{
    Home_tableview_data_share *obj =[self.array_feeds objectAtIndex:control.tag];
    cellClickIndex=control.tag;
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&rate=%lu&item_id=%@&uid=%@",salt,sig,(unsigned long)rating,obj.image_id,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_rating.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"URLString:%@",requestStr);
    AFNetworkingQueue *networkQueue=[AFNetworkingQueue sharedSingleton];
    [networkQueue queueItems :requestStr:@"103":nil];
    obj.my_rating = [NSString stringWithFormat:@"%lu",(unsigned long)rating];
    [self.array_feeds replaceObjectAtIndex:control.tag withObject:obj];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"]isEqualToString:@"1"]) {
            NSDictionary *dataDict = [responseObject objectForKey:@"data"];
            
            Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:cellClickIndex];
            
            shareObj.avgrating=[dataDict objectForKey:@"avrage_rating"];
            shareObj.totalUser=[dataDict objectForKey:@"tot_user"];
            shareObj.my_rating = [NSString stringWithFormat:@"%ld",(long)[[dataDict objectForKey:@"rate"] integerValue]];
            
            [self.array_feeds replaceObjectAtIndex:cellClickIndex withObject:shareObj];
            
            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:cellClickIndex] withRowAnimation:UITableViewRowAnimationNone];
        }
    } failure:^(NSString *errorString) {
        [self.activityINd stopAnimating];
        self.activityINd.hidden = YES;
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];

        [SVProgressHUD dismiss];
        isRefreshing = NO;
//        _btn_reload.enabled = YES;
        
        if (queueKillFlg==1) {
            queueKillFlg=0;
            return;
        }
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark
#pragma mark like unlike call

- (void)btn_like_click:(NSString *)userid imageid:(NSString *)imageid action:(NSString *)like
{
//    [SVProgressHUD show];

    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
	NSString *key = SIGNSALTAPIKEY;
	NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
	NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&action=%@&id=%@&uid=%@",salt,sig,like,imageid,[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_like_unlike.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        if ([[responseObject valueForKey:@"success"] isEqualToString:@"1"]) {
//            Home_tableview_data_share *shareObj =[self.array_feeds objectAtIndex:which_image_liked];
//            
//            if ([shareObj.liked isEqualToString:@"no"]) {
//                shareObj.liked=@"yes";
//                int count =[shareObj.likes intValue];
//                count++;
//                shareObj.likes=[NSString stringWithFormat:@"%d",count];
//                [shareObj.array_liked_by addObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//            }
//            else{
//                shareObj.liked=@"no";
//                int count =[shareObj.likes intValue];
//                count--;
//                shareObj.likes=[NSString stringWithFormat:@"%d",count];
//                
//                if ([shareObj.array_liked_by containsObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]]) {
//                    [shareObj.array_liked_by removeObject:[[NSUserDefaults standardUserDefaults]objectForKey:USERNAME]];
//                }
//            }
//            [self.array_feeds replaceObjectAtIndex:which_image_liked withObject:shareObj];
//            [self.tbl_news reloadSections:[NSIndexSet indexSetWithIndex:which_image_liked] withRowAnimation:UITableViewRowAnimationNone];

            if ( isRefreshing )
                [self.pushToBounceWrapper stopLoadingAnimation];

            [SVProgressHUD dismiss];
            isRefreshing = NO;
        }
    } failure:^(NSString *errorString) {
        [self.activityINd stopAnimating];
        self.activityINd.hidden = YES;
        if ( isRefreshing )
            [self.pushToBounceWrapper stopLoadingAnimation];

        [SVProgressHUD dismiss];
        isRefreshing = NO;
//        _btn_reload.enabled = YES;
        
        if (queueKillFlg==1) {
            queueKillFlg=0;
            return;
        }
        [AJNotificationView showNoticeInView:self.view type:AJNotificationTypeDefault title:@"Couldn't refresh feed" linedBackground:AJLinedBackgroundTypeAnimated hideAfter:2.0f offset:50.0f delay:0.01f response:nil];
    }];
}

#pragma mark
#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender
{
	return ((UIButton *)sender).titleLabel.text;
}

- (void)hashSelected:(id)sender
{
    pushPopFlg=1;
    hash_tag_viewObj.str_title =[[self tagFromSender:sender ] substringFromIndex:1];

    [self.navigationController pushViewController:hash_tag_viewObj animated:YES];
}

- (void)atSelected:(id)sender
{
    pushPopFlg=1;
    NSLog(@"Ids:%@",[[self tagFromSender:sender ] substringFromIndex:1]);
    user_info_view.user_id=[StaticClass urlDecode:[[self tagFromSender:sender ] substringFromIndex:1]];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)urlSelected:(id)sender
{
    pushPopFlg=1;
    web_viewObj.web_url =[self tagFromSender:sender];
    [self.navigationController pushViewController:web_viewObj animated:YES];
}

- (void)userSelected:(id)sender
{
    pushPopFlg=1;
    user_info_view.user_id=[StaticClass urlDecode:[self tagFromSender:sender]];
    [self.navigationController pushViewController:user_info_view animated:YES];
}

- (void)exclamationSelected:(id)sender
{
    NSLog(@"%@", [self tagFromSender:sender]);
}

#pragma mark
#pragma mark Share Photo to email

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email Cancelled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultSaved:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email save successfully!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultSent:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent successfully! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        case MFMailComposeResultFailed:{
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Email sent failed! " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
        default:
        {
            UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"" message:@"Sending Failed - Unknown Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            break;
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark  Date Convert to s,m,h,d,M,y
- (NSString *)get_time_different:(NSString *)datestring
{
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *current_date1 = [dateFormatter dateFromString:[[Singleton sharedSingleton]get_current_time]];
    NSDate *created_date = [dateFormatter dateFromString:datestring];
    NSTimeInterval diff = [current_date1 timeIntervalSinceDate:created_date];
    
    
    if (diff<60) {
        return [NSString stringWithFormat:@"%.fs",diff];
    }else if(diff/60<60){
        return [NSString stringWithFormat:@"%.fm",diff/60];
    }else if(diff/3600<24){
        return [NSString stringWithFormat:@"%.fh",diff/3600];
    }else if(diff/(3600*24)<31){
        return [NSString stringWithFormat:@"%.fd",diff/(3600*24)];
    }else if(diff/(3600*24*30)<12){
        return [NSString stringWithFormat:@"%.fM",diff/(3600*24*30)];
    }else{
        return [NSString stringWithFormat:@"%.fy",diff/(3600*24*30*12)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
