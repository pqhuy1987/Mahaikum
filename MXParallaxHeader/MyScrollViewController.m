//
//  MyScrollViewController.m
//  Mahalkum
//
//  Created by user on 4/20/16.
//  Copyright © 2016 Kuwait E-Gate. All rights reserved.
//

#import "MyScrollViewController.h"
#import "MXScrollView.h"

@interface MyScrollViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MyScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrollView.parallaxHeader.view = [NSBundle.mainBundle loadNibNamed:@"StarshipHeader" owner:self options:nil].firstObject;
    self.scrollView.parallaxHeader.height = 200;
    self.scrollView.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.scrollView.parallaxHeader.minimumHeight = 20;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
