
#import "ReviewTableView.h"
#import "RatingObject.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "User_info_ViewController.h"

@interface ReviewTableView () {
    User_info_ViewController *user_info_view;
}
@end

static NSString *CellIdentifier = @"ReviewTableViewCell";

@implementation ReviewTableView

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    if (self = [super initWithFrame:frame style:style])
    {
        self.backgroundColor = [UIColor clearColor];
        self.tblDataSource = [[NSMutableArray alloc] init];
        self.allowsSelection = NO;
        self.rowHeight = UITableViewAutomaticDimension;
        self.estimatedRowHeight = 44.0f;
        self.dataSource = self;
        self.delegate = self;
        self.separatorColor = [UIColor clearColor];
        
        user_info_view =[[User_info_ViewController alloc]initWithNibName:@"User_info_ViewController" bundle:nil];
    }
    
    return self;
}

- (void)setReviewData:(NSArray *)array
{
    self.tblDataSource = [array mutableCopy];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tblDataSource.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RatingObject *data = self.tblDataSource[indexPath.row];
    
    ReviewTableViewCell *cell = (ReviewTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        [cell.userIcon addTarget:self action:@selector(btn_user_image_click:) forControlEvents:UIControlEventTouchUpInside];
        cell.userIcon.tag = indexPath.row;
        
        [cell.userName addTarget:self action:@selector(btn_user_name_click:) forControlEvents:UIControlEventTouchUpInside];
        cell.userName.tag = indexPath.row;
    }
    
    cell.indexPath = indexPath;
    
    // Configure the cell for this indexPath
    [cell updateFonts];
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    //[cell.userIcon sd_setImageWithURL:[NSURL URLWithString:data.user_image]];
    [cell.userIcon sd_setImageWithURL:[NSURL URLWithString:data.user_image] forState:UIControlStateNormal];
    cell.userIcon.layer.cornerRadius = 15.0f;
    cell.userIcon.clipsToBounds = YES;
    
    //cell.userName.text = data.user_name;
    [cell.userName setTitle:data.user_name forState:UIControlStateNormal];
    cell.ratingView.value = [data.review_rating floatValue];
    cell.date.text = [GlobalDefine toXmlNameFromDate:[GlobalDefine getTimeFromDateString:data.review_datecreated]];
    cell.descs.text = data.review_descs;
    
    NSString *uid1=[NSString stringWithFormat:@"%ld",(long)[LOGINEDUSERID integerValue]];
    NSString *uid2 = data.user_id;
    
    MGSwipeButton *button;
    if ([uid1 isEqualToString:uid2])
    {
        button = [MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]];
        button.indexPath = indexPath;
        [button addTarget:self action:@selector(deleteButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        button = [MGSwipeButton buttonWithTitle:@"Report" backgroundColor:[UIColor redColor]];
        button.indexPath = indexPath;
        [button addTarget:self action:@selector(reportButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.rightButtons = @[button];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    
    return cell;
}

-(IBAction)btn_user_image_click:(id)sender{
    RatingObject *data = self.tblDataSource[((UIButton *)sender).tag];
    user_info_view.user_id=data.user_username;
    
    [((UIViewController *)self.customeDelegate).navigationController pushViewController:user_info_view animated:YES];
}

-(IBAction)btn_user_name_click:(id)sender{
    RatingObject *data = self.tblDataSource[((UIButton *)sender).tag];
    user_info_view.user_id=data.user_username;
    
    [((UIViewController *)self.customeDelegate).navigationController pushViewController:user_info_view animated:YES];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //This code will run in the main thread:
        CGRect frame = self.frame;
        frame.size.height = self.contentSize.height;
        self.frame = frame;
        
        if (self.customeDelegate && [self.customeDelegate respondsToSelector:@selector(reviewTableViewContentSizeChanged:contentSize:)])
        {
            [self.customeDelegate reviewTableViewContentSizeChanged:self contentSize:self.contentSize];
        }
    });
}

- (void)deleteButtonClicked:(MGSwipeButton *)sender
{
    RatingObject *data = self.tblDataSource[sender.indexPath.row];
    NSString *salt = [NSString stringWithFormat:@"%d",rand() % 10000];
    NSString *key = SIGNSALTAPIKEY;
    NSString *tempStr = [NSString stringWithFormat:@"%@%@",key,salt];
    NSString *sig = [StaticClass returnMD5Hash :tempStr];
    
    NSString *postString =[NSString stringWithFormat:@"salt=%@&sign=%@&id=%@", salt, sig, data.review_id];
    NSString *requestStr = [NSString stringWithFormat:@"%@post_delete_review.php?%@",[[Singleton sharedSingleton] getBaseURL],postString];
    NSLog(@"%@", requestStr);
    
    [SVProgressHUD show];
    [[AFNetworkingQueue sharedSingleton] postRequest:nil requestUrl:requestStr success:^(id responseObject) {
        NSLog(@"%@", responseObject);
        if ([responseObject[@"success"] isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tblDataSource removeObjectAtIndex:sender.indexPath.row];
                [self deleteRowsAtIndexPaths:@[sender.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            });
        }
        [SVProgressHUD dismiss];
    } failure:^(NSString *errorString) {
        [SVProgressHUD dismiss];
        NSLog(@"%@", errorString);
    }];
}

- (void)reportButtonClicked:(MGSwipeButton *)sender
{
    
}

@end
