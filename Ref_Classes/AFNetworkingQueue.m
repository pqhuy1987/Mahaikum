//
//  AFNetworkingQueue.m
//  SampleProject
//
//  Created by Rupen Makhecha on 19/01/13.
//
//

#import "AFNetworkingQueue.h"
#import "AFHTTPClient.h"

@implementation AFNetworkingQueue
@synthesize queue;


+(AFNetworkingQueue*) sharedSingleton {
	static AFNetworkingQueue* theInstance = nil;
	if (theInstance == nil) 	{
		theInstance = [[self alloc] init];        
        [theInstance initQueue];
	}
	return theInstance;
}

-(void)initQueue {
    self.queue = [[NSOperationQueue alloc] init];
}

-(void)queueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
   
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"POST" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:requesting];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
       // NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
   // [operation start];
    [self.queue addOperation:operation];
}

-(void)putQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"PUT" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:requesting];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
        //NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
}

-(void)deleteQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"DELETE" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:requesting];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSString *response = [operation responseString];
        //NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
}

-(void)getQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"GET" path:requestUrl parameters:params];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:requesting];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
       // NSString *response = [operation responseString];
       // NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    // [operation start];
    [self.queue addOperation:operation];
}

-(void)uploadImageDrawing :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"test.xml"];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"image" fileName:fName mimeType:@"text/xml"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)uploadImageAndData :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];

    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"img.png"];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
    [formData appendPartWithFileData:imageData name:@"image" fileName:fName mimeType:@"image/jpeg"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)uploadImageAndDataViaPUT :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"%@img.png",[DateFormatter stringFromDate:[NSDate date]]];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"PUT" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"product[image]" fileName:fName mimeType:@"image/jpeg"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)uploadImageAndDataViaPUTPost :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
    //    [formData appendPartWithFileData:imageData name:@"seller_settings[profile_picture]" fileName:@"HomeBackground1.png" mimeType:@"image/jpeg"];
        
        NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
        [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
        NSString *fName=[NSString stringWithFormat:@"%@img.png",[DateFormatter stringFromDate:[NSDate date]]];
        
        [formData appendPartWithFileData:imageData name:@"seller_settings[seller_profile_picture]" fileName:fName mimeType:@"image/jpeg"];
        
        
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)uploadImageForSignUp :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"%@img.png",[DateFormatter stringFromDate:[NSDate date]]];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"user[profile_picture]" fileName:fName mimeType:@"image/jpeg"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)uploadImageForViaPUTInAccountSetting :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSLog(@"%@",[DateFormatter stringFromDate:[NSDate date]]);
    NSString *fName=[NSString stringWithFormat:@"%@img.png",[DateFormatter stringFromDate:[NSDate date]]];
    
    NSMutableURLRequest *myRequest = [httpClient multipartFormRequestWithMethod:@"PUT" path:requestUrl parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"user[profile_picture]" fileName:fName mimeType:@"image/jpeg"];
    }];
    NSLog(@"myRequest:%@",myRequest);
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = [operation responseString];
        NSLog(@"response: [%@]",response);
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:_tag object:self  userInfo:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *tStr=[NSString stringWithFormat:@"-%@",_tag];
        NSDictionary* dict = [NSDictionary dictionaryWithObject: operation forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:tStr object:self  userInfo:dict];
        NSLog(@"error: %@", [operation error]);
    }];
    
    [self.queue addOperation:operation];
}

-(void)cancelAllQueueOperation {
    [self.queue cancelAllOperations];
}

//- (AFHTTPSessionManager*)getAFHTTPSessionManager
//{
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:@"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36" forHTTPHeaderField:@"User-Agent"];
//    [manager.requestSerializer setValue:@"*/*" forHTTPHeaderField:@"Accept"];
//    [manager.securityPolicy setAllowInvalidCertificates:YES];
//    
//    return manager;
//}

#pragma mark - APIs

- (void)postRequest:(NSDictionary *)parameters requestUrl:(NSString *)requestUrl success:(void (^)(id responseObject))success failure:(void (^)(NSString *errorString))failure
{
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *requesting = [httpClient requestWithMethod:@"POST" path:requestUrl parameters:parameters];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:requesting];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        if (!error)
        {
            success(json);
        }
        else
        {
            failure(@"error");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error.localizedDescription);
    }];
    [operation start];
}

//- (void)getRequest:(NSDictionary *)parameters requestUrl:(NSString *)requestUrl success:(void (^)(id responseObject))success
//           failure:(void (^)(NSString *errorString))failure
//{
//    AFHTTPSessionManager *manager = [self getAFHTTPSessionManager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    
//    [manager GET:requestUrl parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
//        
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        success(responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        failure(error.localizedDescription);
//    }];
//}

/*
-(void)temp:(NSString *) requestUrl:(int) _tag :(NSDictionary *)params {
    NSURL *baseURL = [NSURL URLWithString:requestUrl];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [httpClient defaultValueForHeader:@"Accept"];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:requestUrl parameters:params];
    AFHTTPRequestOperation *operation = [AFHTTPRequestOperation operationWithRequest:request
        completion:^(NSURLRequest *req, NSHTTPURLResponse *response, NSData *data, NSError *error) {
            BOOL HTTPStatusCodeIsAcceptable = [[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(200, 100)] containsIndex:[response statusCode]];
            if (HTTPStatusCodeIsAcceptable) {
                NSLog(@"Request Successful");
            } else {
        NSLog(@"[Error]: (%@ %@) %@", [request HTTPMethod], [[request URL] relativePath], error);
        }
    }];
}
*/

@end
