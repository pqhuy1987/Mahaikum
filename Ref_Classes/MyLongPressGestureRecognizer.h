//
//  MyLongPressGestureRecognizer.h
//  
//
//  Created by Charles Moon on 3/12/16.
//
//

#import <UIKit/UIKit.h>

@interface MyLongPressGestureRecognizer : UILongPressGestureRecognizer

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
