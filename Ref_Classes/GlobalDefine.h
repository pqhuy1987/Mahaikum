
typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
} ScrollDirection;

@interface GlobalDefine : NSObject

@property(nonatomic,strong) UILabel *lblSpinnerBg;
@property(nonatomic,strong) UIActivityIndicatorView *activity;

+ (void)setRounded:(UIView *)view color:(UIColor *)color rate:(CGFloat)rate;
+ (NSString *)hexStringFromColor:(UIColor *)color;
+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (NSString *)toXmlNameFromDate:(NSDate *)date;
+ (NSDate *)getTimeFromDateString:(NSString *)datestring;
+ (UIImage *)getImageWithName:(NSString *)imageName;
+ (NSString *)getImageUrl:(NSString *)imageName;
+ (NSString *)getVoiceUrl:(NSString *)voiceName;
+ (BOOL)checkImageExist:(NSString *)imageName;
+ (BOOL)saveImageToAppDocument:(UIImage *)image name:(NSString *)name;
+ (BOOL)deleteImageInAppDocument:(NSString *)name;
+ (BOOL)deleteVoiceInAppDocument:(NSString *)name;
+ (void)createImagesResourceDirectory;
+ (void)createVoicesResourceDirectory;
+ (NSString *)toStringFromDate:(NSDate *)date;
+ (NSString *)toDayStringFromDate:(NSDate *)date;
+ (NSString *)toTimeStringFromDate:(NSDate *)date;
+ (NSDate *)toDateFromString:(NSString *)dateString;
+ (NSString *)toDatabaseDateStringFromDate:(NSDate *)date;
+ (NSDate *)toDateFromDatabaseDateString:(NSString *)dateString;
+ (NSDate *)toDateFromDateTimeStringWithFormat:(NSString *)dateString formatString:(NSString *)formatString;
+ (NSString *)toDateTimeStringFromDateWithFormat:(NSDate *)date formatString:(NSString *)formatString;
+ (NSInteger)getDurationAsNumber:(NSDate *)startDate endDate:(NSDate *)endDate;
+ (NSString *)getDurationAsString:(NSTimeInterval)secs;
+ (NSString *)getDurationAsString:(NSDate *)startDate endDate:(NSDate *)endDate;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (void)showAnimationFromBottom:(UIView *)view;
+ (void)removeAnimationToBottom:(UIView *)view;
+ (void)showAnimationFromTop:(UIView *)view;
+ (void)removeAnimationToTop:(UIView *)view;
+ (void)showAnimationFromTop1:(UIView *)view;
+ (void)removeAnimationToTop1:(UIView *)view;
+ (void)showAnimationFromRight:(UIView *)view;
+ (void)removeAnimationToRight:(UIView *)view;
+ (void)showAnimationWithAlpha:(UIView *)view;
+ (void)removeAnimationWithAlpha:(UIView *)view;
+ (UIControlContentHorizontalAlignment)convertTextAlignmentIntoHorizontal:(NSTextAlignment)alignment;
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;
+ (UIImage *)thumbnailImageFromURL:(NSURL *)videoURL;
+ (NSString*)bv_jsonStringWithPrettyPrint:(NSDictionary *)dic prettyPrint:(BOOL)prettyPrint;
+ (NSString *)randomStringWithLength:(int)len;
+ (NSString*)getRandomEmail;
+ (void)performBlock:(void(^)())block afterDelay:(NSTimeInterval)delay;
+ (NSString *)setLocalNotification:(NSDate *)fireDate alertBody:(NSString *)alertBody alertTitle:(NSString *)alertTitle;
+ (NSInteger)getSecond:(NSDate *)date;

- (id)init;

@end
