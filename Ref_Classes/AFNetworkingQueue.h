//
//  AFNetworkingQueue.h
//  SampleProject
//
//  Created by Rupen Makhecha on 19/01/13.
//
//

#import <Foundation/Foundation.h>

@interface AFNetworkingQueue : NSObject {
    NSOperationQueue *queue;    
}

@property(nonatomic,retain) NSOperationQueue *queue;

+(AFNetworkingQueue*) sharedSingleton;
-(void) queueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)deleteQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)getQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)putQueueItems :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params;
-(void)uploadImageAndData :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;
-(void)uploadImageAndDataViaPUT :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;
-(void)uploadImageAndDataViaPUTPost :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;
-(void)uploadImageForSignUp :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;
-(void)uploadImageForViaPUTInAccountSetting :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData;
-(void)cancelAllQueueOperation;
-(void)uploadImageDrawing :(NSString *)requestUrl :(NSString *)_tag :(NSDictionary *)params :(NSData *)imageData ;
- (void)postRequest:(NSDictionary *)parameters requestUrl:(NSString *)requestUrl success:(void (^)(id responseObject))success failure:(void (^)(NSString *errorString))failure;

@end
