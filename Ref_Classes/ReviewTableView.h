
#import <UIKit/UIKit.h>
#import "ReviewTableViewCell.h"

@protocol ReviewTableViewDelegate;

@interface ReviewTableView : UITableView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) id<ReviewTableViewDelegate> customeDelegate;
@property (nonatomic, strong) NSMutableArray *tblDataSource;
@end

@protocol ReviewTableViewDelegate <NSObject>

@optional
- (void)reviewTableViewContentSizeChanged:(ReviewTableView *)tableView contentSize:(CGSize)contentSize;

@end