
#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface ReviewTableViewCell : MGSwipeTableCell

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UIButton *userIcon;
@property (nonatomic, strong) UIButton *userName;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) HCSStarRatingView *ratingView;
@property (nonatomic, strong) UILabel *descs;

- (void)updateFonts;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
