
#import "FindLocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface FindLocationViewController ()<GMSAutocompleteTableDataSourceDelegate>
{
    CLLocationManager *locationManager;
    NSMutableArray *dataSource;
    BOOL localSearchFlag;
    GMSAutocompleteTableDataSource *_tableDataSource;

    NSTimer *searchDelayer;
}

@end

@implementation FindLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
    
    self.searchString.text = self.mapItemName;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    localSearchFlag = YES;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [locationManager startUpdatingLocation];
    
    _tableDataSource = [[GMSAutocompleteTableDataSource alloc] init];
    _tableDataSource.delegate = self;

    self.tableView.delegate = _tableDataSource;
    self.tableView.dataSource = _tableDataSource;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 40, 0, 0);
    
    if (![self.searchString.text isEqualToString:@""])
    {
        [self getLocationInformation:self.searchString.text];
    }
    
    self.navigationItem.title = @"Location";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestAlwaysAuthorization];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
}

// CLLocationManager delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Failed to get current location: %@", error.localizedDescription);
}

- (void)getLocationInformation:(NSString *)searchKey
{
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = locationManager.location.coordinate.latitude;
    region.center.longitude = locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    // [mapView setRegion:region animated:YES];
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.naturalLanguageQuery = searchKey;
    request.region = region;
    
    localSearchFlag = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSMutableArray *tempDataSource;
        tempDataSource = [[NSMutableArray alloc] init];
        
        if (error != nil) {
            NSLog(@"Map error: %@", error.localizedDescription);
        }
        else if ([response.mapItems count] == 0) {
            NSLog(@"Map no result");
        }
        else
        {
            [response.mapItems enumerateObjectsUsingBlock:^(MKMapItem *item, NSUInteger idx, BOOL *stop) {
                [tempDataSource addObject:item];
            }];
        }
        dataSource = tempDataSource;
        [self.tableView reloadData];
        localSearchFlag = YES;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return dataSource.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//    
//    static NSUInteger const imgTag = 2;
//    static NSUInteger const objTag = 2;
//    
//    CGRect objFrame;
//    UILabel *objLable;
//    UIImageView *imgView;
//    
//    objFrame = CGRectMake(13, 9, 10, 26);
//    imgView = [[UIImageView alloc] initWithFrame:objFrame];
//    imgView.tag = imgTag;
//    imgView.backgroundColor = [UIColor clearColor];
//    imgView.image = [UIImage imageNamed:@"Placemark2.png"];
//    [cell.contentView addSubview:imgView];
//    
//    objFrame = CGRectMake(40, 3, 280, 25);
//    objLable = [[UILabel alloc] initWithFrame:objFrame];
//    objLable.tag = objTag;
//    objLable.font = [UIFont systemFontOfSize:17.0f];
//    objLable.backgroundColor = [UIColor clearColor];
//    objLable.textColor = [UIColor blackColor];
//    objLable.textAlignment = NSTextAlignmentLeft;
//    objLable.text = ((MKMapItem *)dataSource[indexPath.row]).name;
//    [cell.contentView addSubview:objLable];
//    
//    objFrame = CGRectMake(40, 30, 280, 12);
//    objLable = [[UILabel alloc] initWithFrame:objFrame];
//    objLable.tag = objTag;
//    objLable.font = [UIFont systemFontOfSize:10.0f];
//    objLable.backgroundColor = [UIColor clearColor];
//    objLable.textColor = [UIColor blackColor];
//    objLable.textAlignment = NSTextAlignmentLeft;
//    objLable.text = [((MKMapItem *)dataSource[indexPath.row]).placemark.addressDictionary[@"FormattedAddressLines"] componentsJoinedByString:@", "];
//    [cell.contentView addSubview:objLable];
//    
//    return cell;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(mapItemSelected:mapItem:selectedLocationKind:)])
//    {
//        [self.delegate mapItemSelected:self mapItem:(MKMapItem *)dataSource[indexPath.row] selectedLocationKind:self.selectedLocationKind];
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
//}

#pragma mark - GMSAutocompleteTableDataSourceDelegate

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [self.searchString resignFirstResponder];
//    NSMutableAttributedString *text =
//    [[NSMutableAttributedString alloc] initWithString:[place description]];
//    if (place.attributions) {
//        [text appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
//        [text appendAttributedString:place.attributions];
//    }
//    self.searchString.text = place.name;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapItemSelected:place:selectedLocationKind:)])
    {
        [self.delegate mapItemSelected:self place:place selectedLocationKind:self.selectedLocationKind];
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [self.searchString resignFirstResponder];
    self.searchString.text = @"";
}

- (void)didRequestAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.tableView reloadData];
}

- (void)didUpdateAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.tableView reloadData];
}


- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

// UISearchBar delegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [_tableDataSource sourceTextHasChanged:searchText];
//    [searchDelayer invalidate], searchDelayer = nil;
//    searchDelayer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(doDelayedSearch:) userInfo:searchText repeats:YES];
}

// UIScrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchString resignFirstResponder];
}

- (void)doDelayedSearch:(NSTimer *)t
{
    if (localSearchFlag)
    {
        [self getLocationInformation:searchDelayer.userInfo];
        [searchDelayer invalidate], searchDelayer = nil;
    }
}

@end
