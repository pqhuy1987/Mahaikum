
#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol FindLocationViewControllerDelegate;

@interface FindLocationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchString;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *mapItemName;
@property (strong, nonatomic) NSString *selectedLocationKind;

@property (nonatomic, strong) id<FindLocationViewControllerDelegate> delegate;

@end

@protocol FindLocationViewControllerDelegate <NSObject>

@optional
- (void)mapItemSelected:(FindLocationViewController *)locationViewController mapItem:(MKMapItem *)mapItem selectedLocationKind:(NSString *)selectedLocationKind;
- (void)mapItemSelected:(FindLocationViewController *)locationViewController place:(GMSPlace *)mapItem selectedLocationKind:(NSString *)selectedLocationKind;

@end