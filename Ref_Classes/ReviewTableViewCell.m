
#import "ReviewTableViewCell.h"

#define kLabelHorizontalInsets      10.0f
#define kLabelVerticalInsets        10.0f

@interface ReviewTableViewCell ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation ReviewTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.userIcon = [UIButton newAutoLayoutView];
        self.userIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.userIcon.userInteractionEnabled = YES;
        
        self.userName = [UIButton newAutoLayoutView];
        //self.userName.textAlignment = NSTextAlignmentLeft;
        //self.userName.textColor = [UIColor blackColor];
        self.userName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.userName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.userName.backgroundColor = [UIColor clearColor];
        
        self.ratingView = [HCSStarRatingView newAutoLayoutView];
        self.ratingView.opaque = NO;
        self.ratingView.maximumValue = 5;
        self.ratingView.minimumValue = 0;
        self.ratingView.userInteractionEnabled = NO;
        self.ratingView.tintColor = [GlobalDefine colorWithHexString:@"1fae38"];
        
        self.date = [UILabel newAutoLayoutView];
        self.date.textAlignment = NSTextAlignmentLeft;
        self.date.textColor = [UIColor lightGrayColor];
        self.date.backgroundColor = [UIColor clearColor];
        
        self.descs = [UILabel newAutoLayoutView];
        [self.descs setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.descs setNumberOfLines:0];
        [self.descs setTextAlignment:NSTextAlignmentLeft];
        [self.descs setTextColor:[UIColor lightGrayColor]];
        self.descs.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:self.userIcon];
        [self.contentView addSubview:self.userName];
        [self.contentView addSubview:self.ratingView];
        [self.contentView addSubview:self.date];
        [self.contentView addSubview:self.descs];
        
        [self updateFonts];
    }
    
    return self;
}

- (void)updateConstraints
{
    if (!self.didSetupConstraints) {
        // Note: if the constraints you add below require a larger cell size than the current size (which is likely to be the default size {320, 44}), you'll get an exception.
        // As a fix, you can temporarily increase the size of the cell's contentView so that this does not occur using code similar to the line below.
        //      See here for further discussion: https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
        // self.contentView.bounds = CGRectMake(0.0f, 0.0f, 99999.0f, 99999.0f);
        
        CGFloat imageWidthHeight = 30.0f;
        
        [self.userIcon autoSetDimension:ALDimensionWidth toSize:imageWidthHeight relation:NSLayoutRelationEqual];
        [self.userIcon autoSetDimension:ALDimensionHeight toSize:imageWidthHeight relation:NSLayoutRelationEqual];
        [self.userIcon autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:kLabelVerticalInsets];
        [self.userIcon autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
        
        [self.ratingView autoSetDimension:ALDimensionWidth toSize:80.0f relation:NSLayoutRelationEqual];
        [self.ratingView autoSetDimension:ALDimensionHeight toSize:imageWidthHeight / 2.0f relation:NSLayoutRelationEqual];
        [self.ratingView autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelVerticalInsets];
        [self.ratingView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.userIcon withOffset:0.0];
        
        [self.userName autoSetDimension:ALDimensionHeight toSize:imageWidthHeight / 2.0f relation:NSLayoutRelationEqual];
        [self.userName autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:self.userIcon withOffset:0.0];
        [self.userName autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:self.userIcon withOffset:5.0];
        [self.userName autoPinEdge:ALEdgeRight toEdge:ALEdgeLeft ofView:self.ratingView withOffset:-5.0];
        
        [self.date autoSetDimension:ALDimensionHeight toSize:imageWidthHeight / 2.0f relation:NSLayoutRelationEqual];
        [self.date autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.userName withOffset:0.0];
        [self.date autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:self.userIcon withOffset:5.0];
        [self.date autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelVerticalInsets];
        
        [self.descs autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.userIcon withOffset:kLabelVerticalInsets];
        
        [UILabel autoSetPriority:UILayoutPriorityRequired forConstraints:^{
            [self.descs autoSetContentCompressionResistancePriorityForAxis:ALAxisVertical];
        }];
        [self.descs autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:kLabelVerticalInsets];
        [self.descs autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:kLabelHorizontalInsets];
        [self.descs autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:kLabelHorizontalInsets];
        
        self.didSetupConstraints = YES;
    }
    
    [super updateConstraints];
}

- (void)updateFonts
{
    self.userName.font = [UIFont boldSystemFontOfSize:16.0];
    self.date.font = [UIFont boldSystemFontOfSize:14.0];
    self.descs.font = [UIFont boldSystemFontOfSize:14.0];
}

@end
